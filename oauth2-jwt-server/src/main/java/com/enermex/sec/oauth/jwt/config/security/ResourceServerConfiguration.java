package com.enermex.sec.oauth.jwt.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.util.Arrays;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;



@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private final TokenStore tokenStore;

    public ResourceServerConfiguration(final TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Override
    public void configure(final ResourceServerSecurityConfigurer resources) {
        resources.tokenStore(tokenStore);
    }
    
    @Override
   	public void configure(HttpSecurity http) throws Exception {
//   		 Cualquier solicitud se autentica y se habilita para CORS
   		http.authorizeRequests()
   		.anyRequest().authenticated()
   		.antMatchers("/users/**").permitAll()
   		.and()
   		.cors().configurationSource(corsConfigurationSource());
    	
//    	  http
//          .authorizeRequests()
//             .antMatchers("/users/register").permitAll()
//             .anyRequest().authenticated();
         
   	}
   	
   	@Bean
   	public CorsConfigurationSource corsConfigurationSource() {
   		// Configuración de Cors
   		CorsConfiguration config = new CorsConfiguration();
   		config.setAllowedOrigins(Arrays.asList("*"));
   		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
   		config.setAllowCredentials(true);
   		config.setAllowedHeaders(Arrays.asList("*"));
   		
   		// Activación de Cors para cualquier ruta
   		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
   		source.registerCorsConfiguration("/**", config);
   		return source;
   	}
   	
   	@Bean
   	public FilterRegistrationBean<CorsFilter> corsFilter(){
   		// Precedencia del filtro del Cors; para que se añada siempre (Como filter de Web.xml)
   		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
   		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
   		return bean;
   	}
       
    
   

}
