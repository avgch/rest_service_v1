package  com.enermex.enumerable;
public enum UsuarioEstatus {
  ACTIVO("A"), 
  INACTIVO("I"), 
  BLOQUEADO("B"), 
  POR_VERIFICAR("V");
  
  private String estatus;

  /**
   * 
   * @param estatus
   * estatus
   */
  private UsuarioEstatus(String estatus) {
      this.estatus = estatus;
  }

  /**
   * 
   * @return estatus
   */
  public String getEstatus() {
      return estatus;
  }
}
