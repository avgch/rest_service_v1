package  com.enermex.enumerable;

public enum QuejaEstatus {
  DESATENDIDA("D"), 
  SEGUIMIENTO("S"),
  ACLARADA("A");
  
  private String estatus;

  /**
   * 
   * @param estatus
   * estatus
   */
  private QuejaEstatus(String estatus) {
      this.estatus = estatus;
  }

  /**
   * 
   * @return estatus
   */
  public String getEstatus() {
      return estatus;
  }
}
