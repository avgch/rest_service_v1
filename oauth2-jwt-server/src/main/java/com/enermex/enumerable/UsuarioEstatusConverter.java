package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UsuarioEstatusConverter implements AttributeConverter<UsuarioEstatus, String> {
  
    @Override
    public String convertToDatabaseColumn(UsuarioEstatus estatus) {
        if (estatus == null) {
            return null;
        }
        return estatus.getEstatus();
    }
 
    @Override
    public UsuarioEstatus convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (UsuarioEstatus estatus : UsuarioEstatus.values()) {
          if(estatus.getEstatus().equals(code)){
            return estatus;
          }
        }

        return null;
    }
}
