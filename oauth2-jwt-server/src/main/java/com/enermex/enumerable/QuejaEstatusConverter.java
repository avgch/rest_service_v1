package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class QuejaEstatusConverter implements AttributeConverter<QuejaEstatus, String> {
  
    @Override
    public String convertToDatabaseColumn(QuejaEstatus estatus) {
        if (estatus == null) {
            return null;
        }
        return estatus.getEstatus();
    }
 
    @Override
    public QuejaEstatus convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (QuejaEstatus estatus : QuejaEstatus.values()) {
          if(estatus.getEstatus().equals(code)){
            return estatus;
          }
        }

        return null;
    }
}
