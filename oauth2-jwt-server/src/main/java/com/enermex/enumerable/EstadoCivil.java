package com.enermex.enumerable;

public enum EstadoCivil {
  SOLTERO("S"),
  CASADO("C"),
  DIVORCIADO("D"),
  UNION_LIBRE("U");

  private String estadoCivil;

  /**
   * 
   * @param estadoCivil
   * estadoCivil
   */
  private EstadoCivil(String estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  /**
   * 
   * @return estadoCivil
   */
  public String getEstadoCivil() {
    return estadoCivil;
  }
}
