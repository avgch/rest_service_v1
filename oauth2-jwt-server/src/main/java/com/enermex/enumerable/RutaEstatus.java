package  com.enermex.enumerable;

public enum RutaEstatus {
  ACTIVO("A"), 
  INACTIVO("I");
  
  private String estatus;

  /**
   * 
   * @param estatus
   * estatus
   */
  private RutaEstatus(String estatus) {
      this.estatus = estatus;
  }
  
  /**
   * 
   * @return estatus
   */
  public String getEstatus() {
      return estatus;
  }
}
