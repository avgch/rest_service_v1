package com.enermex.enumerable;

public enum DireccionEstatus {
  ACTIVO(1),
  ELIMINADO(2);

  private int estatus;

  /**
   * 
   * @param estadoCivil
   * estadoCivil
   */
  private DireccionEstatus(int estadoCivil) {
    this.estatus = estadoCivil;
  }

  /**
   * 
   * @return estatus
   */
  public int getDireccionEstatus() {
    return estatus;
  }
}
