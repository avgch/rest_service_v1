package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RutaEstatusConverter implements AttributeConverter<RutaEstatus, String> {
  
    @Override
    public String convertToDatabaseColumn(RutaEstatus estatus) {
        if (estatus == null) {
            return null;
        }
        return estatus.getEstatus();
    }
 
    @Override
    public RutaEstatus convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (RutaEstatus estatus : RutaEstatus.values()) {
          if(estatus.getEstatus().equals(code)){
            return estatus;
          }
        }

        return null;
    }
}
