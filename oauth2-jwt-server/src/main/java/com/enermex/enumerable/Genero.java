package com.enermex.enumerable;

public enum Genero {
  MASCULINO("M"),
  FEMENINO("F");

  private String genero;

  /**
   * 
   * @param genero
   * genero
   */
  private Genero(String genero) {
      this.genero = genero;
  }
  
  /**
   * 
   * @return genero
   */
  public String getGenero() {
      return genero;
  }
}
