package com.enermex.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

// Antonio González
// TODO: verificar los datos
@Entity
@Table(name = "t_usuario")
public class Usuario {
	
	
	@Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name = "id_usuario")
	private Long idUsuario;
	private String correo;
  private String contrasena;
  private String estatus;
	
	/**
	 * 
	 * @return idUsuario
	 */
	public Long getIdUsuario() {
		return idUsuario;
	}
	
	/**
	 * 
	 * @param idUsuario
	 * idUsuario
	 */
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	/**
	 * 
	 * @return correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * 
	 * @param correo
	 * correo
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * 
	 * @return contrasena
	 */
	public String getContrasena() {
		return contrasena;
	}
	/**
	 * 
	 * @param contrasena
	 * contrasena
	 */
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	   /**
	    * 
	    * @return estatus
	    */
	  public String getEstatus() {
	    return estatus;
	  }
	  /**
	   * 
	   * @param estatus
	   * estatus
	   */
	  public void setEstatus(String estatus) {
	    this.estatus = estatus;
	  }
}
