package com.enermex.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import com.enermex.modelo.UsuarioEntity;
import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.repository.UsuarioCompletoRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Servicio de operaciones de usuarios
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 16 de enero de 2020
 */
@Service
public class UsuarioService {
  Logger logger = LoggerFactory.getLogger(UsuarioService.class);

  @Autowired
  private UsuarioCompletoRepository repo;
  
  @Resource(name="passwordEncoder")
  private PasswordEncoder passwordEncoder;

  /**
   * Creación de un usuario
   * 
   * @param usuario Usuario por insertar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  public boolean create(UsuarioEntity usuario) {
    if (exists(usuario)) {
      logger.error("Trata de añadir un usuario existente - ID: " + usuario.getIdUsuario());

      return false;
    }

    if(exists(usuario.getCorreo())){
      return false;
    }

    // Se establece el estado y la fecha de creación
    Date date = new Date();
    usuario.setFechaCreacion(date);
    usuario.setEstatus(UsuarioEstatus.POR_VERIFICAR);

    return save(usuario);
  }

  /**
   * Actualización de un usuario
   * 
   * @param usuario Usuario por actualizar
   * @return Bandera que indica el resultado de la operación
   */
  public boolean update(UsuarioEntity usuario) {
    if (!exists(usuario)) {
      logger.error("Trata de actualizar un usuario inexistente - ID: " + usuario.getIdUsuario());

      return false;
    }

    return save(usuario);
  }

  /**
   * Eliminación lógica del usuario
   * 
   * @param usuario Usuario por eliminar
   * @return Bandera que indica el resultado de la operación
   */
  public boolean delete(UsuarioEntity usuario) {
    if (!exists(usuario)) {
      logger.error("Trata de eliminar un usuario inexistente - ID: " + usuario.getIdUsuario());

      return false;
    }

    usuario.setEstatus(UsuarioEstatus.INACTIVO);
    return save(usuario);
  }

  /**
   * Consulta de los usuario registrados
   * 
   * @return Lista con usuario registrados
   */
  @Transactional(readOnly = true)
  public List<UsuarioEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener usuarioes: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un usuario por su identificador
   * 
   * @param id Identificador del usuario
   * @return El usuario, si se encuentra
   */
  @Transactional(readOnly = true)
  public UsuarioEntity queryById(Long id) {
    try {
      Optional<UsuarioEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener un usuario: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un usuario por su identificador
   * 
   * @param id Identificador del usuario
   * @return El usuario, si se encuentra
   */
  @Transactional(readOnly = true)
  public UsuarioEntity queryByCorreo(String correo) {
    try {
      UsuarioEntity usuario = repo.findByCorreo(correo);
      return usuario;
    } catch (Exception e) {
      logger.error("Error al obtener un usuario: " + e.getMessage());
      return null;
    }
  }

  /**
   * Comprueba si existe un usuario
   * 
   * @param usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  private boolean exists(UsuarioEntity usuario) {
    try {
      return usuario != null && usuario.getIdUsuario() != null && repo.existsById(usuario.getIdUsuario());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un usuario
   * 
   * @param id Identificador del usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  public boolean exists(Long id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un usuario
   * 
   * @param correo Correo electrónico del usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  public boolean exists(String correo) {
    try {
      return repo.existsByCorreo(correo);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un usuario con un teléfono
   * 
   * @param telefono Teléfono del usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  public boolean existsTelefono(String telefono) {
    try {
      return repo.existsByTelefono(telefono);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Guardado del usuario
   * 
   * @param usuario Usuario por guardar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional(propagation = Propagation.NESTED)
  private boolean save(UsuarioEntity usuario) {
    try {
      repo.save(usuario);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar un usuario: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Comprobación de la contraseña
   * 
   * @param id Identificador del usuario
   * @param password Problable contraseña del usuario
   * @return Bandera que indica si la contraseña corresponde
   */
  @Transactional
  public boolean testPassword(Long id, String password) {
    try {
      return repo.findWithPassword(id,passwordEncoder.encode( password)) != null;
    } catch (Exception e) {
      logger.error("Error al consultar un usuario por su contraseña: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Actualización de la contraseña
   * 
   * @param id Identificador del usuario
   * @param password Problable contraseña del usuario
   * @return Bandera que indica si la contraseña corresponde
   */
  @Transactional
  public boolean updatePassword(String correo, String password) {
    try {
      repo.updatePassword(correo, passwordEncoder.encode(password));
      return true;
    } catch (Exception e) {
      logger.error("Error al actualizar la contraseña de un usuario: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}
