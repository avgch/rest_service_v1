package com.enermex.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.enermex.repository.UsuarioRepository;
import com.enermex.dto.Usuario;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.PermisoEntity;
import com.enermex.modelo.RolEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.PermisoRepository;
import com.enermex.repository.UsuarioCompletoRepository;

@Service
public class CustomDetailService implements UserDetailsService {

  @Autowired
  private UsuarioRepository usuarioRepository;

  @Autowired
  private UsuarioCompletoRepository usuarioCompletoRepository;

  @Autowired
  private PermisoRepository permisoRepository;

  @Autowired
  private ClienteRepository clienteRepository;

  @Override
  public UserDetails loadUserByUsername(String userLogin) throws UsernameNotFoundException {
    UserDetails userDet = null;

    try {
      // Datos del usuario y rol
      String correo = null;
      String role   = null;

      // Se intenta obtener el split de la aplicación
      String[] userWithRole = userLogin.split("&");

      // Si el usuario indica a qué rol está asociado, se obtiene. En caso contrario, se trata de un refresco de Web
      if(userWithRole.length > 1) {
        correo = userWithRole[0];
        role   = userWithRole[1].split("=")[1];
      } else {
        role = "REFRESH";
      }

      // Login de dispositivo móvil
      if (role.equalsIgnoreCase("MOVIL")) {
        Cliente cliente = clienteRepository.findByEmail(correo);

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
        userDet = new User(cliente.getIdCliente().toString(), cliente.getContrasena(), authorities);

      }
      
      // Login de aplicación Web
      else if (role.equalsIgnoreCase("WEB")) {
        // Se busca al usuario determinado
        Usuario usuario = usuarioRepository.findByCorreo(correo);
        UsuarioEntity usuarioEntity = usuarioCompletoRepository.findByCorreo(correo);

        // Se verifica que el usuario cuente con el estatus requerido (Activo)
        if (usuarioEntity != null && usuario != null && usuario.getEstatus().equalsIgnoreCase("A")) {

          // Verificar que el usuario tenga permiso para entrar a la aplicación (cuando se integren las aplicaciones)
          List<PermisoEntity> permisos = permisoRepository.findAll();
          boolean permitido = false;

          for(RolEntity rol : usuarioEntity.getRoles()) {
            for(PermisoEntity perm : permisos){
              if(perm.getId().getPantalla().equals("BACKOFFICE")
                  && rol.getIdRol() == perm.getId().getIdRol()
                  && perm.getAsignado()) {
                permitido = true;
              }
            }
          }

          if(!permitido) {
            throw new IllegalArgumentException("ROL");
          }

          List<GrantedAuthority> authorities = new ArrayList<>();

          authorities.add(new SimpleGrantedAuthority("ROLE_WEB"));
          userDet = new User(String.valueOf(usuario.getIdUsuario()), usuario.getContrasena(), authorities);
        }
      }
      
      // Login de aplicación de despachador
      else if (role.equalsIgnoreCase("DESPACHADOR")) {
        // Se busca al usuario determinado
        Usuario usuario = usuarioRepository.findByCorreo(correo);
        UsuarioEntity usuarioEntity = usuarioCompletoRepository.findByCorreo(correo);

        // Se verifica que el usuario cuente con el estatus requerido (Activo)
        if (usuarioEntity != null && usuario != null && usuario.getEstatus().equalsIgnoreCase("A")) {

          // Verificar que el usuario tenga el rol para entrar a la aplicación
          boolean permitido = false;

          for(RolEntity rol : usuarioEntity.getRoles()) {
            if (rol.getRol().equalsIgnoreCase("Despachador")) {
              permitido = true;
            }
          }

          if(!permitido) {
            throw new IllegalArgumentException("ROL");
          }

          List<GrantedAuthority> authorities = new ArrayList<>();

          authorities.add(new SimpleGrantedAuthority("ROLE_DESPACHADOR"));
          userDet = new User(String.valueOf(usuario.getIdUsuario()), usuario.getContrasena(), authorities);
        }
      }

      // Refresh token
      else if (role.equalsIgnoreCase("REFRESH")) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_WEB"));
        
        userDet = new User(userLogin, "", authorities);
      }
    }
    catch(Exception e) {
    	
    	//e.printStackTrace();
    }
    

    return userDet;
  }
}
