package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.PermisoEntity;
import com.enermex.modelo.PermisoId;

import org.springframework.data.repository.CrudRepository;

/**
 * Repositorio de permisos
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 22 de enero de 2020
 */
public interface PermisoRepository extends CrudRepository<PermisoEntity, PermisoId>{
  /**
   * Devuelve la lista con todos los permisos
   * 
   * @return Lista de permisos
   */
  List<PermisoEntity> findAll();

  /**
   * Devuelve los permisos de un rol y pantallas específicos
   * 
   * @param idRol Identificador del rol
   * @param pantalla Nombre de la pantalla
   * @return
   */
  List<PermisoEntity> findByIdIdRolAndIdPantalla(Integer idRol, String pantalla);

  /**
   * Devuelve los permisos de un rol específico
   * 
   * @param idRol Identificador del rol
   * @return
   */
  List<PermisoEntity> findByIdIdRol(Integer idRol);
}
