package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.UsuarioEntity;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Repositorio de UsuarioEntity
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 16 de enero de 2020
 */
public interface UsuarioCompletoRepository extends CrudRepository<UsuarioEntity, Long> {
  /**
   * Consulta de todos los usuarios
   * @return Lista de usuarios registrados
   */
  public List<UsuarioEntity> findAll();

  /**
   * Búsqueda de usuario por correo electrónico
   * @param correo Dirección de correo electrónico
   * @return
   */
  public UsuarioEntity findByCorreo(String correo);

  /**
   * Búsqueda de usuario por número telefónico
   * @param correo Dirección de correo electrónico
   * @return
   */
  public UsuarioEntity findByTelefono(String telefono);

  /**
   * Indica si existe un usuario con la dirección de correo específica
   * @param correo Dirección de correo electrónico
   * @return
   */
  public boolean existsByCorreo(String correo);

  /**
   * Indica si existe un usuario con el teléfono específico
   * @param correo Dirección de correo electrónico
   * @return
   */
  public boolean existsByTelefono(String telefono);

  /**
   * Búsqueda de usuario por identificador y contraseña
   * 
   * @param id Identificador del usuario
   * @param password Contraseña del usuario
   * @return
   */
  @Query(value = "SELECT * FROM t_usuario WHERE id_usuario = ?1 AND contrasena = ?2 AND estatus = 'A'", nativeQuery = true)
  public UsuarioEntity findWithPassword(Long id, String password);

  /**
   * Búsqueda de usuario por correo y contraseña
   * 
   * @param correo Dirección de correo electrónico
   * @param password Contraseña del usuario
   * @return
   */
  @Query(value = "SELECT * FROM t_usuario WHERE correo = ?1 AND contrasena = ?2 AND estatus = 'A'", nativeQuery = true)
  public UsuarioEntity findWithPassword(String correo, String password);

  /**
   * Actualización de la contraseña (y activación) del usuario
   * 
   * @param id Identificador del usuario
   * @param password 
   */
  @Modifying
  @Query(value = "UPDATE t_usuario SET contrasena = ?2, estatus = 'A' WHERE correo = ?1", nativeQuery = true)
	public void updatePassword(String correo, String password);
}
