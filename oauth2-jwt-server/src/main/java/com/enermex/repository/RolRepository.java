package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.RolEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface RolRepository  extends JpaRepository<RolEntity, Integer> {
	
	List<RolEntity> findAll();
//	@Modifying
//	@Query("update UsuarioDto u set u.nombre=?1,u.apellidos=?2, u.correo = ?3, u.telefono = ?4  where u.idUsuario = ?3")
//	void updateUsuario(String nombre,String apellidos, String correo, String telefono, Integer usuarioId);	
	
	@Query("select r.idRol from RolEntity r where r.rol = ?1")
	Integer findByRol(String rol);
	

}
