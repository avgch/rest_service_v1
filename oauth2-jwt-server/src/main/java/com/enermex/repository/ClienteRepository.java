package com.enermex.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.Cliente;



@Repository
@Transactional
public interface ClienteRepository  extends JpaRepository<Cliente, BigInteger>{

	
	
	/**
	 * 
	 * @param id
	 * id
	 */
	@Modifying
	@Query("update Cliente c set c.estatus=2 where c.idCliente = ?1")
	public void delete(BigInteger id) ;
	
	
	/**
	 * 
	 * @param nombre
	 * nombre
	 * @param correo
	 * correo
	 * @param telefono
	 * telefono
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set c.nombre=?1 , c.correo = ?2, c.telefono = ?3  where c.idCliente = ?4")
	void update(String nombre, String correo, String telefono, BigInteger idCliente);
	
	
	/**
	 * 
	 * @param email
	 * email
	 * @return email
	 */
	@Query("Select c from Cliente c where c.correo = ?1 and c.acceso=true")
	 Cliente findByEmail(String email);
}
