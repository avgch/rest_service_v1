package com.enermex.modelo;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.enermex.enumerable.EstadoCivil;
import com.enermex.enumerable.Genero;
import com.enermex.enumerable.UsuarioEstatus;

/**
 * Entidad JPA de tabla t_usuario
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 16 de enero de 2020
 */
@Entity
@Table(name = "t_usuario")
public class UsuarioEntity{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_usuario")
  private Long idUsuario;

  @Column(name = "nombre")
  private String nombre;

  @Column(name = "apellido_paterno")
  private String apellidoPaterno;

  @Column(name = "apellido_materno")
  private String apellidoMaterno;

  @Column(name = "correo")
  private String correo;

  @Column(name = "telefono")
  private String telefono;

  @Column(name = "fecha_creacion")
  private Date fechaCreacion;

  @Column(name = "estatus")
  private UsuarioEstatus estatus;

  @Column(name = "fecha_nacimiento")
  private Date fechaNacimiento;

  @Column(name = "genero")
  private Genero genero;

  @Column(name = "estado_civil")
  private EstadoCivil estadoCivil;

  @Column(name = "foto_uri")
  private String fotoUri;

  @JoinTable(
    name = "t_usuarios_rol",
    joinColumns = @JoinColumn(name = "id_usuario"),
    inverseJoinColumns = @JoinColumn(name = "id_rol"))
  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
  private List<RolEntity> roles;

  /**
   * 
   * @return idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * 
   * @param idUsuario
   * idUsuario
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * 
   * @return nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * 
   * @param nombre
   * nombre
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * 
   * @return apellidoPaterno
   */
  public String getApellidoPaterno() {
    return apellidoPaterno;
  }

  /**
   * 
   * @param apellidoPaterno
   * apellidoPaterno
   */
  public void setApellidoPaterno(String apellidoPaterno) {
    this.apellidoPaterno = apellidoPaterno;
  }

  /**
   * 
   * @return apellidoMaterno
   */
  public String getApellidoMaterno() {
    return apellidoMaterno;
  }

  /**
   * 
   * @param apellidoMaterno
   * apellidoMaterno
   */
  public void setApellidoMaterno(String apellidoMaterno) {
    this.apellidoMaterno = apellidoMaterno;
  }

  /**
   * 
   * @return correo
   */
  public String getCorreo() {
    return correo;
  }

  /**
   * 
   * @param correo
   * correo
   */
  public void setCorreo(String correo) {
    this.correo = correo;
  }

  /**
   * 
   * @return telefono
   */
  public String getTelefono() {
    return telefono;
  }

  /**
   * 
   * @param telefono
   * telefono
   */
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  /**
   * 
   * @return fechaCreacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * 
   * @param fechaCreacion
   * fechaCreacion
   */
  public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * 
   * @return estatus
   */
  public UsuarioEstatus getEstatus() {
    return estatus;
  }

  /**
   * 
   * @param estatus
   * estatus
   */
  public void setEstatus(UsuarioEstatus estatus) {
    this.estatus = estatus;
  }

  /**
   * 
   * @return fechaNacimiento
   */
  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  /**
   * 
   * @param fechaNacimiento
   * fechaNacimiento
   */
  public void setFechaNacimiento(Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  /**
   * 
   * @return genero
   */
  public Genero getGenero() {
    return genero;
  }

  /**
   * 
   * @param genero
   * genero
   */
  public void setGenero(Genero genero) {
    this.genero = genero;
  }

  /**
   * 
   * @return estadoCivil
   */
  public EstadoCivil getEstadoCivil() {
    return estadoCivil;
  }

  /**
   * 
   * @param estadoCivil
   * estadoCivil
   */
  public void setEstadoCivil(EstadoCivil estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  /**
   * 
   * @return fotoUri
   */
  public String getFotoUri() {
    return fotoUri;
  }

  /**
   * 
   * @param fotoUri
   * fotoUri
   */
  public void setFotoUri(String fotoUri) {
    this.fotoUri = fotoUri;
  }

  /**
   * 
   * @return roles
   */
  public List<RolEntity> getRoles() {
    return roles;
  }

  /**
   * 
   * @param roles
   * roles
   */
  public void setRoles(List<RolEntity> roles) {
    this.roles = roles;
  }
}
