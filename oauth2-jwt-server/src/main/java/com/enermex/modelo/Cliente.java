package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;


@Entity
@Table(name="t_cliente")
@Component
public class Cliente implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_cliente")
	private BigInteger idCliente;
	
	
	@Column(name = "nombre")
	private String nombre;

	
	@Column(name = "correo")
	private String correo;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "contrasena")
	private String contrasena;
	
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name = "id_titular")
	private BigInteger idTitular;
	
	@Column(name = "foto_uri")
	private String fotoUri;
	
	@Column(name = "id_estatus")
	private int estatus; 
	
	@Column(name = "acceso")
	private boolean acceso ;
	
	
	public Cliente(){}
	
	
	
	
	
    /**
     * 
     * @param idCliente
     * idCliente
     * @param nombre
     * nombre
     * @param correo
     * correo
     * @param telefono
     * telefono
     * @param contrasena
     * contrasena
     * @param fechaCreacion
     * fechaCreacion
     * @param idTitular
     * idTitular
     * @param fotoUri
     * fotoUri
     * @param estatus
     * estatus
     */
	public Cliente(BigInteger idCliente, String nombre, String correo,
			String telefono, String contrasena, Date fechaCreacion, BigInteger idTitular, String fotoUri,
			Integer estatus) {
		super();
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.correo = correo;
		this.telefono = telefono;
		this.contrasena = contrasena;
		this.fechaCreacion = fechaCreacion;
		this.idTitular = idTitular;
		this.fotoUri = fotoUri;
		this.estatus = estatus;

	}

    /**
     * 
     * @return idCliente
     */
	public BigInteger getIdCliente() {
		return idCliente;
	}
    /**
     * 
     * @param idCliente
     * idCliente
     */
	public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * 
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre
	 * nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return correo
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * 
	 * @param correo
	 * correo
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}

	/**
	 * 
	 * @return telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * 
	 * @param telefono
	 * telefono
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * 
	 * @return contrasena
	 */
	public String getContrasena() {
		return contrasena;
	}

	/**
	 * 
	 * @param contrasena
	 * contrasena
	 */
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	/**
	 * 
	 * @return fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * 
	 * @param fechaCreacion
	 * fechaCreacion
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * 
	 * @return idTitular
	 */
	public BigInteger getIdTitular() {
		return idTitular;
	}

	/**
	 * 
	 * @param idTitular
	 * idTitular
	 */
	public void setIdTitular(BigInteger idTitular) {
		this.idTitular = idTitular;
	}

	/**
	 * 
	 * @return fotoUri
	 */
	public String getFotoUri() {
		return fotoUri;
	}

	/**
	 * 
	 * @param fotoUri
	 * fotoUri
	 */
	public void setFotoUri(String fotoUri) {
		this.fotoUri = fotoUri;
	}

	/**
	 * 
	 * @return estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}

	/**
	 * 
	 * @param estatus
	 * estatus
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	/**
	 * 
	 * @return acceso
	 */
	public boolean isAcceso() {
		return acceso;
	}

	/**
	 * 
	 * @param acceso
	 * acceso
	 */
	public void setAcceso(boolean acceso) {
		this.acceso = acceso;
	}

	/**
	 * 
	 * @param estatus
	 * estatus
	 */
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
    


	
	

	

}
