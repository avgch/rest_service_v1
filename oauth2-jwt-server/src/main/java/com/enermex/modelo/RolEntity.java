/*##############################################################################
# Nombre del Programa : RolDto.java                                          #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               : Antonio González                                       #
# Compania            : Enermex                                                #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/



package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="t_rol")
public class RolEntity implements Serializable{

  private static final long serialVersionUID = 1L;

  @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_rol")
	private Integer idRol;
	
	@Column(name="rol")
	private String rol;
	
	@Column(name="descripcion")
	private String descripcion;
	

	
	/**
	 * Constructor de clase
	 */
	public RolEntity() {
		
	}
   /**
    * 
    * @return idRol
    */
	public Integer getIdRol() {
		return idRol;
	}
    /**
     * 
     * @param idRol
     * idRol
     */
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	/**
	 * 
	 * @return rol
	 */
	public String getRol() {
		return rol;
	}

	/**
	 * 
	 * @param rol
	 * rol
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}

	/**
	 * 
	 * @return descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * 
	 * @param descripcion
	 * descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
    
    
	

}
