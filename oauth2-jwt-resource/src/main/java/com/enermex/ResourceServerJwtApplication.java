package com.enermex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author abraham
 */
@SpringBootApplication
@EnableScheduling
public class ResourceServerJwtApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ResourceServerJwtApplication.class);
	}

    /**
     *
     * @param args
     */
    public static void main(String... args) {
        SpringApplication.run(ResourceServerJwtApplication.class, args);
    }

}
