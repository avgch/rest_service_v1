package com.enermex.avg.exceptions;

/**
 *
 * @author abraham
 */
public class ConectaException  extends Exception{

	 /**
	  * Constructor de clase
	  * @param errorMessage
	  */
	 public ConectaException(String errorMessage) {
	        super(errorMessage);
	    }
}
