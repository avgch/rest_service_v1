package com.enermex.avg.exceptions;

/**
 *
 * @author abraham
 */
public class FastGasException extends Exception {
	
	
	private String codeError;
	
	/**
	 * Constructor de clase
	 * @param errorMessage
	 * @param code
	 */
	public FastGasException(String errorMessage,String code) {
        super(errorMessage);
        this.codeError = code;
    }

    /**
     *
     * @return
     */
    public String getCodeError() {
		return codeError;
	}

    /**
     *
     * @param codeError
     */
    public void setCodeError(String codeError) {
		this.codeError = codeError;
	}
    

}
