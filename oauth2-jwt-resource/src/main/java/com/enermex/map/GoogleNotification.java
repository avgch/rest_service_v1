package com.enermex.map;

/**
 *
 * @author abraham
 */
public class GoogleNotification {
  private String body;
  private String title;

  GoogleNotification(String title, String body) {
    this.title = title;
    this.body = body;
  }

    /**
     *
     * @return
     */
    public String getBody() {
    return body;
  }

    /**
     *
     * @param value
     */
    public void setBody(String value) {
    this.body = value;
  }

    /**
     *
     * @return
     */
    public String getTitle() {
    return title;
  }

    /**
     *
     * @param value
     */
    public void setTitle(String value) {
    this.title = value;
  }
}
