package com.enermex.map;

import java.util.List;

/**
 *
 * @author abraham
 */
public class GoogleFcm {
  private List<String> registration_ids;
  private GoogleNotification notification;
  private GoogleNotification data;

    /**
     *
     * @param fcms
     * @param title
     * @param body
     */
    public GoogleFcm(List<String> fcms, String title, String body) {
    registration_ids = fcms;

    notification = new GoogleNotification(title, body);
    data = new GoogleNotification(title, body);
  }

    /**
     *
     * @return
     */
    public List<String> getRegistration_ids() {
    return registration_ids;
  }

    /**
     *
     * @param value
     */
    public void setRegistration_ids(List<String> value) {
    this.registration_ids = value;
  }

    /**
     *
     * @return
     */
    public GoogleNotification getNotification() {
    return notification;
  }

    /**
     *
     * @param value
     */
    public void setNotification(GoogleNotification value) {
    this.notification = value;
  }

    /**
     *
     * @return
     */
    public GoogleNotification getData() {
    return data;
  }

    /**
     *
     * @param value
     */
    public void setData(GoogleNotification value) {
    this.data = value;
  }
}