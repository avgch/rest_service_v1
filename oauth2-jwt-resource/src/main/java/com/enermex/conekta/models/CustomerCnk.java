package com.enermex.conekta.models;

/**
 *
 * @author abraham
 */
public class CustomerCnk {
	
	
	private String name;
	private String email;
	private String phone;
	
	/**
	 * 
	 * @return name
	 * getName
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param name
	 * setName
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return email
	 * getEmail
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * 
	 * @param email
	 * setEmail
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * 
	 * @return phone
	 * getPhone
	 */
	public String getPhone() {
		return phone;
	}
	
	/**
	 * 
	 * @param phone
	 * setPhone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	

}
