package com.enermex.conekta.models;

/**
 *
 * @author abraham
 */
public class Servicio {
	
	private  String name;
	private  String unit_price;
	private  String quantity;
	
	/**
	 * 
	 * @return name
	 * getName
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param name
	 * setName
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return  unit_price
	 * getUnit_price
	 */
	public String getUnit_price() {
		return unit_price;
	}
	
	/**
	 * 
	 * @param unit_price
	 * setUnit_price
	 */
	public void setUnit_price(String unit_price) {
		this.unit_price = unit_price;
	}
	/**
	 * 
	 * @return quantity
	 * getQuantity
	 */
	public String getQuantity() {
		return quantity;
	}
	/**
	 * 
	 * @param quantity
	 * setQuantity
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	        
	

}
