package com.enermex.conekta.models;

/**
 * 
 * @author abraham
 *
 */
public class CardCnk {

	
	private String tokenId;
	private String type;
	private String exp_month;
	private String exp_year;
	private String name;
	
	/**
	 *  Constructo de clase
	 * @param tokenId
	 * tokenId
	 * @param type
	 * type
	 * @param exp_month
	 * exp_month
	 * @param exp_year
	 * exp_year
	 * @param name
	 * name
	 * 
	 */
	public CardCnk(String tokenId, String type, String exp_month, String exp_year,String name) {
		super();
		this.tokenId = tokenId;
		this.type = type;
		this.exp_month = exp_month;
		this.exp_year = exp_year;
		this.name = name;
	
	}

    /**
     * 
     * @return name
     * getName
     */
    public String getName() {
		return name;
	}

    /**
     * 
     * @param name
     * setName
     */
    public void setName(String name) {
		this.name = name;
	}

    /**
     * 
     * @return tokenId
     * getTokenId
     */
    public String getTokenId() {
		return tokenId;
	}

    /**
     * 
     * @param tokenId
     * setTokenId
     */
    public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

    /**
     * 
     * @return type
     * getType
     */
    public String getType() {
		return type;
	}

    /**
     * 
     * @param type
     * setType
     */
    public void setType(String type) {
		this.type = type;
	}

    /**
     * 
     * @return exp_month
     * getExp_month
     */
    public String getExp_month() {
		return exp_month;
	}

    /**
     * 
     * @param exp_month
     * setExp_month
     */
    public void setExp_month(String exp_month) {
		this.exp_month = exp_month;
	}

    /**
     * 
     * @return exp_year
     * getExp_year
     */
    public String getExp_year() {
		return exp_year;
	}

    /**
     * 
     * @param exp_year
     * setExp_year
     */
    public void setExp_year(String exp_year) {
		this.exp_year = exp_year;
	}
	
	
	
}
