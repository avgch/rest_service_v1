package com.enermex.json;

import java.util.List;

import com.enermex.modelo.DespachadorFaqEntity;
import com.enermex.modelo.DespachadorIncidenciaEntity;

/**
 * Formato de incidencias y preguntas frecuentes
 */
public class DespachadorFaqsIncidencias {
  private List<DespachadorIncidenciaEntity> incidencias;
  private List<DespachadorFaqEntity> faqs;

    /**
     *
     * @return
     */
    public List<DespachadorIncidenciaEntity> getIncidencias() {
    return incidencias;
  }

    /**
     *
     * @param incidencias
     */
    public void setIncidencias(List<DespachadorIncidenciaEntity> incidencias) {
    this.incidencias = incidencias;
  }

    /**
     *
     * @return
     */
    public List<DespachadorFaqEntity> getFaqs() {
    return faqs;
  }

    /**
     *
     * @param faqs
     */
    public void setFaqs(List<DespachadorFaqEntity> faqs) {
    this.faqs = faqs;
  }
}
