package com.enermex.json;

import java.util.List;

import com.enermex.modelo.FaqEntity;
import com.enermex.modelo.IncidenciaEntity;

/**
 * Formato de incidencias y preguntas frecuentes
 */
public class FaqsIncidencias {
  private List<IncidenciaEntity> incidencias;
  private List<FaqEntity> faqs;

    /**
     *
     * @return
     */
    public List<IncidenciaEntity> getIncidencias() {
    return incidencias;
  }

    /**
     *
     * @param incidencias
     */
    public void setIncidencias(List<IncidenciaEntity> incidencias) {
    this.incidencias = incidencias;
  }

    /**
     *
     * @return
     */
    public List<FaqEntity> getFaqs() {
    return faqs;
  }

    /**
     *
     * @param faqs
     */
    public void setFaqs(List<FaqEntity> faqs) {
    this.faqs = faqs;
  }
}
