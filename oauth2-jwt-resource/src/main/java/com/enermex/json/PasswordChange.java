package com.enermex.json;

/**
 * Clase que permitirá recibir información para el cambio de contraseña
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 6 de enero de 2020
 */
public class PasswordChange {
  private String token;
  private String password;

    /**
     *
     * @return
     */
    public String getToken() {
      return token;
  }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
      this.token = token;
  }

    /**
     *
     * @return
     */
    public String getPassword() {
      return password;
  }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
      this.password = password;
  }
}
