package com.enermex.utilerias;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.impl.PromocionRepositoryImpl;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.enumerable.PlanesEnum;
import com.enermex.modelo.Parametro;
import com.enermex.modelo.PromocionCliente;
import com.enermex.repository.ParametroRepository;
import com.enermex.repository.PromocionClienteRepository;
import com.enermex.service.ClienteFcmService;

/**
 *
 * @author abraham
 */
@Service
public class CalculoPromocion {
	
	@Autowired
	PromocionRepositoryImpl promocionRepositoryImpl;
	
	@Autowired
	ParametroRepository parametroRepository;
	
	@Autowired
	PromocionClienteRepository promocionClienteRepository;
	
	@Autowired
	ClienteFcmService clienteFcmService;
	
    /**
     *
     * @return
     */
    public List<String> saveAlgunosClientes(){
	   
	   return null;
   }
   
   
     
  /**
   * Enviar notificacion a clientes frecuentes de la promocion
   * @param planes
   * @param idPromocion
     * @param push
   * @param body
   * @throws FastGasException
   */
   public void clientesFrecuentes(List<Integer> planes,Long idPromocion,String push,String body) throws FastGasException{
	   
	   List<BigInteger> clienteIds = null;
	   List<String> tokens=null;
	   List<PromocionCliente> promoClientes = new ArrayList<>();
	   PromocionCliente promoCliente;
	   Parametro clienteFreq = parametroRepository.clienteFrecuenteValue();
	   MulticastMessage.Builder msg=  MulticastMessage.builder();		
	   
	   clienteIds = promocionRepositoryImpl.getClientesFrecuentes(clienteFreq.getValor(),planes);
	   
	   //notificamos y guardamos en t_promocion_cliente
	   for(BigInteger id : clienteIds) {
		   promoCliente = new PromocionCliente();
		   promoCliente.setIdCliente(id);
		   promoCliente.setIdPromocion(idPromocion);
		   
		   promoClientes.add(promoCliente);
	   }
	   promocionClienteRepository.saveAll(promoClientes);
	   
	   if(clienteIds!=null) {
		   if(clienteIds.size()>0)
			   tokens  = clienteFcmService.getTokensClientes(clienteIds);
	   }
	     
	   
	   if(tokens!=null){
		    //Notificacion
	    	if(tokens.size()>0) {
			    Notification.Builder msNot = Notification.builder();
			    
			    msNot.setBody(body);
			    msNot.setTitle(push);
			    msg.addAllTokens(tokens);
			    
			    msg.setNotification(msNot.build());
			    
			    FirebaseMessaging fm = FirebaseMessaging.getInstance();
			    
			    try {
					fm.sendMulticast(msg.build());
				} catch (FirebaseMessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					 throw new FastGasException("Contacte al administrador del sistema","ADMERR");
					
				}
			    
	    	}   
	    }
	   
   }
   /**
    * Obtiene el id de los productos de la promocion
    * @param esFamiliar
    * @param esParticular
    * @param esEmpresarial
    * @return
    */
   public List<Integer> getPlanes(Boolean esFamiliar, Boolean esParticular, Boolean esEmpresarial){
	   
	   List<Integer> planes = new ArrayList<Integer>();
	   
	   
	   if( (esParticular==null) ? false : esParticular )
		    planes.add(PlanesEnum.PARTICULAR.getValor());
	   
	   if( (esFamiliar==null) ? false : esFamiliar )
	    planes.add(PlanesEnum.FAMILIAR.getValor());
	   
	   if((esEmpresarial==null) ? false : esEmpresarial)
		    planes.add(PlanesEnum.EMPRESARIAL.getValor());
	   
	   return planes;
   } 
   
   

}
