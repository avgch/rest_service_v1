package com.enermex.utilerias;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import com.enermex.config.*;

import java.io.IOException;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 *
 * @author abraham
 */
@Configuration
@EnableResourceServer
@EnableConfigurationProperties(SecurityProperties.class)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String ROOT_PATTERN = "/**";

    private final SecurityProperties securityProperties;

    private TokenStore tokenStore;

    /**
     *
     * @param securityProperties
     */
    public ResourceServerConfiguration(final SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

    @Override
    public void configure(final ResourceServerSecurityConfigurer resources) {
        resources.tokenStore(tokenStore());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
    	
    	
    	  
    	    
    	    

    	
    	  http
          .authorizeRequests()
             .antMatchers("/api/clientes/register","/api/clientes/recovery","/api/clientes/validaRegistro","/api/clientes/hellow",
            		      "/api/marcas/**","/api/modelos/**","/api/trim/**","/api/serie/**","/api/generation/**","/api/especificacion/**","/api/precios/combustibles"
                      ,"/api/usuarios/account/recovery","/api/usuarios/account/password","/api/usuarios/account/test", "/api/usuarios/**/foto",
                      "/api/quejas/foto/**", "/api/quejasd/foto/**", "/api/despachadores/account/recovery","/api/pedido/prueba", "/api/clientes/**/foto", "/api/clientes/**/upload/foto","/api/clientes/esCtaActiva",
                      "/api/pedidos/evidencias/**", "/api/facturas/excel", "/api/website", "/api/website/correos", "/api/website/forms", "/api/legales/terminos", "/api/legales/privacidad",
                      "/api/legales/terminos/text", "/api/legales/privacidad/text", "/api/colores")
             .permitAll()
             .antMatchers("/tipoPlan/**")
             .permitAll()
             .anyRequest().authenticated();
    	
        http.authorizeRequests()
                .antMatchers("/users/register").permitAll()
                .antMatchers(HttpMethod.GET, ROOT_PATTERN).access("#oauth2.hasScope('read')")
                .antMatchers(HttpMethod.POST, ROOT_PATTERN).access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PATCH, ROOT_PATTERN).access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PUT, ROOT_PATTERN).access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.DELETE, ROOT_PATTERN).access("#oauth2.hasScope('write')")
                .and().cors().configurationSource(corsConfigurationSource());
                
    }

    /**
     *
     * @param tokenStore
     * @return
     */
    @Bean(name = "tokenServicesR")
    public DefaultTokenServices tokenServices(final TokenStore tokenStore) {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore);
        return tokenServices;
    }

    /**
     *
     * @return
     */
    @Bean(name = "tokenStoreR")
    public TokenStore tokenStore() {
        if (tokenStore == null) {
            tokenStore = new JwtTokenStore(jwtAccessTokenConverter());
        }
        return tokenStore;
    }

    /**
     *
     * @return
     */
    @Bean(name = "converterR")
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setVerifierKey(getPublicKeyAsString());
        return converter;
    }

    private String getPublicKeyAsString() {
        try {
            return IOUtils.toString(securityProperties.getJwt().getPublicKey().getInputStream(), UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    
//    @Override
//   	public void configure(HttpSecurity http) throws Exception {
//   		// Cualquier solicitud se autentica y se habilita para CORS
//   		http.authorizeRequests()
//   		.anyRequest().authenticated()
//   		.and()
//   		.cors().configurationSource(corsConfigurationSource());
//   	}

    /**
     *
     * @return
     */
   	
   	@Bean(name ="sourceR")
   	public CorsConfigurationSource corsConfigurationSource() {
   		// Configuración de Cors
   		CorsConfiguration config = new CorsConfiguration();
   		config.setAllowedOrigins(Arrays.asList("*"));
   		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
   		config.setAllowCredentials(true);
   		config.setAllowedHeaders(Arrays.asList("*"));
   		
   		// Activación de Cors para cualquier ruta
   		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
   		source.registerCorsConfiguration("/**", config);
   		return source;
   	}
   	
    /**
     *
     * @return
     */
    @Bean(name = "FilterRegistrationBeanR")
   	public FilterRegistrationBean<CorsFilter> corsFilter(){
   		// Precedencia del filtro del Cors; para que se añada siempre (Como filter de Web.xml)
   		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
   		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
   		return bean;
   	}

}
