package com.enermex.utilerias;

import org.springframework.stereotype.Component;

/**
 *
 * @author abraham
 */
@Component
public class SqlOperations {
	
	
	/**
	 * Dues backup from t_gasolina_estado_municipio to t_gasolina_estado_municipio_historico
	 * @return
	 */
	public String saveHistoricPricesFuels() {
		
		StringBuffer query = new StringBuffer();
		query.append(" INSERT INTO t_gasolina_estado_municipio_historico ");
		query.append("   (id, id_estado, id_municipio,fecha_registro,combustible,minimo,maximo,promedio,tot_gas_prom,tot_gas,fecha_creacion) ");
		query.append("     SELECT * from t_gasolina_estado_municipio; ");
		//query.append("     commit; ");
	
		return query.toString();
	}

}
