package com.enermex.utilerias;

import java.math.BigInteger;
import com.enermex.modelo.Column;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.enermex.modelo.DataTableFilter;
import com.enermex.modelo.DataTableFilter.ColumnCriterias;
import com.enermex.modelo.DataTableFilter.OrderCriterias;
import com.enermex.modelo.Order;
import com.enermex.modelo.DataTableFilter;

/**
 *
 * @author abraham
 */
@Component
public class DAOUtil {

    /**
     *
     * @param filtro
     * @return
     */
    public String getCondiciones(DataTableFilter filtro) {
		StringBuffer condiciones = new StringBuffer();
		String searchText="",columnOrder="";
		
		int i=0;
		for(Column column : filtro.getColumns()){
			
			if(i==0) {
				condiciones.append(" WHERE  ");
				condiciones.append(column.getName());
				condiciones.append(" LIKE '%");
				condiciones.append(filtro.getSearch().getValue());
				condiciones.append("%'");
			}else {
				
				condiciones.append(" OR ");
				condiciones.append(column.getName());
				condiciones.append(" LIKE '%");
				condiciones.append(filtro.getSearch().getValue());
				condiciones.append("%'");
				
			}
			i++;
			
		};

		Order orden =filtro.getOrder().get(0);
		if(orden.getColumn()==0)
			columnOrder="ID";
			
		if(orden.getColumn()==1)
			columnOrder="ESTADO";
		
		if(orden.getColumn()==2)
			columnOrder="MUNICIPIO";
			
		if(orden.getColumn()==3)
			columnOrder="CP";
		
		if(orden.getColumn()==4)
			columnOrder="COMBUSTIBLE";
		
		if(orden.getColumn()==5)
			columnOrder="PROMEDIO";
		
		if(orden.getColumn()==6)
			columnOrder="FECHA";
		
			
		condiciones.append(" ORDER BY ");
		condiciones.append(columnOrder);
		condiciones.append(" ").append(orden.getDir());
	
		
		return condiciones.toString();
	}
	
    /**
     *
     * @param query
     * @return
     */
    public String getCountQuery(String query){
		return " SELECT COUNT(*) FROM (" + query + ")PRECIOS)T ";
	}
	
    /**
     *
     * @param filter
     * @param query
     * @param recordsTotal
     * @return
     */
    public String createFinalQuery(DataTableFilter filter, String query, BigInteger recordsTotal){
		StringBuffer finQuery = new StringBuffer(" SELECT * FROM ( SELECT  *  FROM (  ");
		finQuery.append(query.toString());
		finQuery.append(")PRECIOS)T1)T2  ");
		
		finQuery.append("  WHERE T2.IDSEQ BETWEEN ");
		finQuery.append(filter.getStart());
		finQuery.append(" AND ");
		if(filter.getLength() == -1){
			finQuery.append(recordsTotal);						
		}else{
			finQuery.append(filter.getStart() + filter.getLength());			
		}
		return finQuery.toString();
	}
}
