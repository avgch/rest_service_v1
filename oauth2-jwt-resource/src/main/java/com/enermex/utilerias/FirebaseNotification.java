package com.enermex.utilerias;

import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import com.enermex.map.GoogleFcm;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteFcmEntity;
import com.enermex.modelo.DespachadorFcmEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.ClienteFcmService;
import com.enermex.service.DespachadorFcmService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author abraham
 */
@Component
public class FirebaseNotification {

  @Autowired
  ClienteFcmService fcmClienteService;

  @Autowired
  DespachadorFcmService fcmDespachadorService;

  private boolean send(List<String> fcms, String titulo, String cuerpo) {
    try {
      // Construcción del cuerpo del servicio
      GoogleFcm notification = new GoogleFcm(fcms, titulo, cuerpo);
      ObjectMapper jsonMapper = new ObjectMapper();
      String body = jsonMapper.writeValueAsString(notification);

      // Conexión Https
      URL url = new URL("https://fcm.googleapis.com/fcm/send");
      HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
      connection.setRequestProperty("Authorization", "key=AIzaSyCb-5GkN0m2dD3mThIXeZoOmttuYGeJmVk");
      connection.setRequestProperty("Content-Type", "application/json; UTF-8");
      connection.setDoOutput(true);

      // Escritura de los datos de salida
      OutputStream outputStream = connection.getOutputStream();
      outputStream.write(body.getBytes("UTF-8"));
      outputStream.flush();
      outputStream.close();

      return connection.getResponseCode() == 200;
    } catch (Exception e) {

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param cliente
     * @param titulo
     * @param respuesta
     * @return
     */
    public boolean notificar(Cliente cliente, String titulo, String respuesta) {
    List<ClienteFcmEntity> fcmsDb = fcmClienteService.queryByIdCliente(cliente.getIdCliente());
    List<String> fcms = fcmsDb.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());

    return send(fcms, titulo, respuesta);
  }

    /**
     *
     * @param despachador
     * @param titulo
     * @param respuesta
     * @return
     */
    public boolean notificar(UsuarioEntity despachador, String titulo, String respuesta) {
    List<DespachadorFcmEntity> fcmsDb = fcmDespachadorService.queryByIdDespachador(despachador.getIdUsuario());
    List<String> fcms = fcmsDb.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());

    return send(fcms, titulo, respuesta);
  }

    /**
     *
     * @param idCliente
     * @param titulo
     * @param respuesta
     * @return
     */
    public boolean notificarCliente(BigInteger idCliente, String titulo, String respuesta) {
    List<ClienteFcmEntity> fcmsDb = fcmClienteService.queryByIdCliente(idCliente);
    List<String> fcms = fcmsDb.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());

    return send(fcms, titulo, respuesta);
  }

    /**
     *
     * @param idDespachador
     * @param titulo
     * @param respuesta
     * @return
     */
    public boolean notificarDespachador(Long idDespachador, String titulo, String respuesta) {
    List<DespachadorFcmEntity> fcmsDb = fcmDespachadorService.queryByIdDespachador(idDespachador);
    List<String> fcms = fcmsDb.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());

    return send(fcms, titulo, respuesta);
  }
}