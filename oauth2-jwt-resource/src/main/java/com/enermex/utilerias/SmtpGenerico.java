package com.enermex.utilerias;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.springframework.core.io.ClassPathResource;
import com.sendgrid.*;

// TODO: Gestionar correctamente la memoria

/**
 *
 * @author abraham
 */
@Component
public class SmtpGenerico {
  final String username = "apikey";
  final String password = "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";

  private String getHtml(String title, String data) {
    try {
      Resource resource = new ClassPathResource("email.html");
      File file = resource.getFile();
      BufferedReader br = new BufferedReader(new FileReader(file));

      StringBuilder html = new StringBuilder();
      String replacement = data;
      String line;

      while ((line = br.readLine()) != null) {
        html.append(line.replace("[!data!]", replacement).replace("[!title!]", title));
      }

      br.close();
      return html.toString();
    } catch (Exception e) {
      return "";
    }
  }

    /**
     *
     * @param subject
     * @param email
     * @param title
     * @param data
     * @return
     */
    public boolean sendEmail(String subject, String email, String title, String data) {
    try {
      System.out.println(subject);
      System.out.println(email);
      System.out.println(title);
      System.out.println(data);

      
      // Configuración de Azure SendGrid
      SendGrid sg = new SendGrid(password);
      
      // Datos  del correo electrónico
      Email from = new Email("abraham900925@gmail.com", username);
      Email to = new Email(email);
      
      // Contenido del correo
      Content content = new Content();
      content.setType("text/html");
      content.setValue(getHtml(title, data));
      
      // Solicitud de envío
      Mail mail = new Mail(from, subject, to, content);
      Request request = new Request();
      request.setMethod(Method.POST);
      request.setEndpoint("mail/send");
      request.setBody(mail.build());

      Response response = sg.api(request);

      // Finalización correcta
      return true;
    } catch (Exception e) {
      e.printStackTrace();

      return false;
    }
  }

  private String hr() {
    return "<hr style=\"max-width: 600px;\">";
  }

    /**
     *
     * @return
     */
    public String br() {
    return "<br>";
  }

  /**
   * Devuelve una tabla en formato HTML para incluir en el correo
   * @param title
   * @param tabData
   * @return
   */
  public String table(String title, ArrayList<BasicTabulator> tabData) {
    String table = 
      "<div style=\"margin-top: 15px; margin-bottom: 15px;\">" +
        "<div style=\"font-size: 0.9em; margin-bottom: 5px;\">" +
          "<strong>" + title + "</strong>" +
        "</div>" +
        "<table style=\"margin-left: auto; margin-right: auto; font-size: 0.9em; background-color: #eaeaea;\">";
    
    for(BasicTabulator tab : tabData) {
      table += 
        "<tr>" +
          "<td>" + tab.title + "&nbsp;</td>" +
          "<td>" + tab.data  + "</td>" +
        "</tr>";
    }

    table += 
        "</table>" +
      "</div>";
    
    return table;
  }

    /**
     *
     * @param content
     * @return
     */
    public String text(String content) {
    return 
      "<div style=\"display: inline-block; max-width: 600px; width: 100%\">" +
        content +
      "</div>";
  }

    /**
     *
     * @param title
     * @param tabData
     * @return
     */
    public String lines(String title, ArrayList<BasicTabulator> tabData) {
    String lines = 
      "<div style=\"margin-top: 15px; margin-bottom: 15px;\">" +
        "<div style=\"font-size: 0.9em; margin-bottom: 5px;\">" +
          "<strong>" + title + "</strong>" +
        "</div>" +
        "<div style=\"font-size: 0.9em; margin-bottom: 5px;\">";
    
    for(BasicTabulator tab : tabData) {
      lines += 
        "<strong>" + tab.title + "&nbsp;</strong><br>" +
        tab.data  + "<br>";
    }

    lines += 
        "</div>" +
      "</div>";
    
    return lines;
  }

    /**
     *
     * @param parts
     * @return
     */
    public String join(String ...parts) {
    String joined = "";

    if(parts.length > 0) {
      for(int i = 0; i < parts.length - 1; i++) {
        joined += parts[i] + hr();
      }

      joined += parts[parts.length - 1];
    }

    return joined;
  }

    /**
     *
     */
    public static class BasicTabulator {
    private String title;
    private String data;

      /**
       *
       * @param title
       * @param data
       */
      public BasicTabulator(String title, String data) {
      this.title = title;
      this.data  = data;
    }
  }

}
