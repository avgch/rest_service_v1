package com.enermex.utilerias;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

/**
 * Clase para la gestión de errores en peticiones de servicios REST
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 30 de diciembre de 2019
 * @param <T>
 */
@Component
public class RestResponse<T> {
    private boolean success;
    private T data;
    private String code;
    private String message;

    /**
     *
     * @return
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     *
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     *
     * @return
     */
    public T getData() {
        return data;
    }

    /**
     *
     * @param data
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     *
     * @return
     */
    public String getCode() {
		return code;
	}

    /**
     *
     * @param code
     */
    public void setCode(String code) {
		this.code = code;
	}

    /**
     *
     * @return
     */
    public String getMessage() {
		return message;
	}

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
		this.message = message;
	}

   
    
}
