package com.enermex.utilerias;

import java.util.Base64;
import java.util.Map;

import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.ClienteService;
import com.enermex.service.UsuarioService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Obtención de datos de Tokens oAuth del sistema
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 5 de febrero de 2020
 * Última modificación: 27 de febrero de 2020
 */
@Component
public class Token {
  private Logger logger = LoggerFactory.getLogger(Token.class);

  @Autowired
  private UsuarioService usuarioService;

  @Autowired
  private ClienteService clienteService;

  /**
   * Obtención de un usuario desde el token oAuth
   * 
   * @param oauth Token oAuth de cuenta de usuario
   * @param response Referencia de objeto de respuesta en controladores
   * @return
   */
  public UsuarioEntity getUsuarioFromToken(String oauth, RestResponse<?> response) {
    try {
      // Consulta del usuario
      UsuarioEntity usuario = usuarioService.queryById(Long.parseLong(getId(oauth)));

      // Token sin usuario asociado
      if(usuario == null) {
        logger.error("El usuario asociado al token oAuth no exite: " + oauth);

        response.setSuccess(false);
        response.setCode("USR_OAUTH");
        return null;
      }

      // Validar que el usuario sea activo
      if(usuario.getEstatus() == UsuarioEstatus.ACTIVO) {
        return usuario;
      } else {

        // TODO: Auditoría
        response.setSuccess(false);
        response.setCode("USR_OAUTH");
        return null;
      }
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Obtención de un usuario desde el token oAuth
   * 
   * @param oauth Token oAuth de cuenta de usuario
   * @return
   * 
   * @deprecated Debería gestionar los posibles errores en esta operación, use getUsuarioFromToken(String oauth, RestResponse response) en su lugar
   */
  @Deprecated
  public UsuarioEntity getUsuarioFromToken(String oauth) {
    try {
      UsuarioEntity usuario = usuarioService.queryById(Long.parseLong(getId(oauth)));

      if(usuario == null) {
        return null;
      }

      // Validar que el usuario sea activo
      if(usuario.getEstatus() == UsuarioEstatus.ACTIVO) {
        return usuario;
      } else {
        return null;
      }
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Obtención de un usuario desde el token oAuth
   * 
   * @param oauth Token oAuth de cuenta del cliente
   * @return
   * 
   * @deprecated Debería gestionar los posibles errores en esta operación, use getClienteFromToken(String oauth, RestResponse response) en su lugar
   */
  public Cliente getClienteFromToken(String oauth) {
    try {
      Cliente cliente = clienteService.getClienteById(getId(oauth));

      if(cliente == null) {
        return null;
      }

      // Validar que, el cliente tenga acceso y que sea activo
      if(cliente.getEstatus() == 1 && cliente.isAcceso()) {
        return cliente;
      } else {
        return null;
      }
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Obtención de un cliente desde el token oAuth
   * 
   * @param oauth Token oAuth de cuenta del cliente
   * @param response Referencia de objeto de respuesta en controladores
   * @return
   */
  public Cliente getClienteFromToken(String oauth, RestResponse<?> response) {
    try {
      Cliente cliente = clienteService.getClienteById(getId(oauth));

      // Token sin cliente asociado
      if(cliente == null) {
        logger.error("El cliente asociado al token oAuth no exite: " + oauth);

        response.setSuccess(false);
        response.setCode("CLI_OAUTH");
        return null;
      }

      // Validar que el cliente sea activo
      if(cliente.getEstatus() == 1 && cliente.isAcceso()) {
        return cliente;
      } else {

        // TODO: Auditoría
        response.setSuccess(false);
        response.setCode("CLI_OAUTH");
        return null;
      }
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  private String getId(String oauth) {
    try {
      // Se obtiene el usuario desde el token
      String dataPart = oauth.split("\\.")[1];
      String jsonPart = new String(Base64.getDecoder().decode(dataPart));
      
      // Deserialización del token
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      Map<String, Object> jsonMap = objectMapper.readValue(jsonPart, new TypeReference<Map<String,Object>>(){});
      
      // Se eliminan posibles caracteres extras
      return (String)jsonMap.get("user_name");
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}