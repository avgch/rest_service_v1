package com.enermex.utilerias;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Random;

import org.springframework.core.io.ClassPathResource;

/**
 * José Antonio González Hernández
 * 
 * Clase que permite firmar, cifrar y verificar cadenas de URL
 */
@Component
public class UrlSigner {
    private RSAPublicKey PUBLIC_KEY;
    private RSAPrivateKey PRIVATE_KEY;

    // Lectura del archivo de clave pública o privada en formato PEM
    private String getPemKey(boolean isPrivate) throws IOException {
        // Discriminación del archivo de clave pública o privada
        String resourceName = isPrivate ? "url_private.pem" : "url_public.pem";
        Resource resource = new ClassPathResource(resourceName);
        File file = resource.getFile();
        BufferedReader br = new BufferedReader(new FileReader(file));

        // NOTE: Los archivos ya no deben contener los marcadores de certificado
        String pem = "";
        String line;
        while ((line = br.readLine()) != null) {
            pem += line;
        }
        br.close();

        return pem;
    }

    
    // Obtiene la clave pública RSA; no está en un certificado X509
    private RSAPublicKey getPublicKey() throws IOException, GeneralSecurityException {
        // Si la clave ya se obtuvo, se omite el proceso
        if (PUBLIC_KEY != null) {
            return PUBLIC_KEY;
        }

        // Obtención de la clave pública, en formato DER
        byte[] encoded = Base64.getDecoder().decode(getPemKey(false));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec spec = new X509EncodedKeySpec(encoded);
        PUBLIC_KEY = (RSAPublicKey) kf.generatePublic(spec);
        return PUBLIC_KEY;
    }

    // Obtiene la clave privada; debe estar en formato PKCS8 sin contraseña
    private RSAPrivateKey getPrivateKey() throws IOException, GeneralSecurityException {
        // Si la clave ya se obtuvo, se omite el proceso
        if (PRIVATE_KEY != null) {
            return PRIVATE_KEY;
        }

        // Obtención de la clave privada, en formato DER (PKCS8)
        byte[] encoded = Base64.getDecoder().decode(getPemKey(true));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        PRIVATE_KEY = (RSAPrivateKey) kf.generatePrivate(keySpec);
        return PRIVATE_KEY;
    }

    /**
     * Empaqueta una cadena, incluyendo su firma digital
     * 
     * @param data Cadena por empaquetar
     * @return
     */
    public String sign(String data) {
        try {
            // Salt sencillo para cambiar la firma
            Random r = new Random();
            String c1 = String.valueOf((char)(r.nextInt(26) + 'a'));
            String c2 = String.valueOf((char)(r.nextInt(26) + 'a'));
            String c3 = String.valueOf((char)(r.nextInt(26) + 'a'));
            String c4 = String.valueOf((char)(r.nextInt(26) + 'a'));
            data = c1 + c2 + c3 + c4 + data;
            
            // Firma de los datos
            Signature sign = Signature.getInstance("SHA1withRSA");
            sign.initSign(getPrivateKey());
            sign.update(data.getBytes("UTF-8"));

            // Se realiza una asignación de los datos
            String dotted = data + "|||"
                + new String(Base64.getEncoder().encode(sign.sign()));

            return new String(
                Base64.getEncoder().encode(dotted.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Desempaquetado y validación de una cadena envuelta con el método sign
     * @param data
     * @return
     */
    public String unbox(String data) {
        try {
            String undotted = new String(Base64.getDecoder().decode(data.getBytes()), "UTF-8");
            String[] parts = undotted.split("\\|\\|\\|");

            if (parts.length == 2 && verify(parts[0], parts[1])) {
                return parts[0].substring(4);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Verifica que una firma digital sea correcta
    private boolean verify(String message, String signature) throws GeneralSecurityException, IOException,
            SignatureException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        Signature sign = Signature.getInstance("SHA1withRSA");
        sign.initVerify(getPublicKey());
        sign.update(message.getBytes("UTF-8"));
        return sign.verify(Base64.getDecoder().decode(signature));
    }
}
