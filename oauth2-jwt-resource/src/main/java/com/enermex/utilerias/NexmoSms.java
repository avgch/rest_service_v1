package com.enermex.utilerias;


import java.math.BigInteger;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.service.ClienteSmsRegistroService;
import com.google.gson.Gson;
import com.nexmo.client.NexmoClient;
import com.nexmo.client.sms.SmsSubmissionResponse;
import com.nexmo.client.sms.SmsSubmissionResponseMessage;
import com.nexmo.client.sms.messages.TextMessage;
import java.util.Calendar;
import java.util.Date;
import java.util.Random; 
import com.enermex.modelo.ClienteSmsRegistro;

/**
 *
 * @author abraham
 */
@Service
@ConfigurationProperties("nexmo")
public class NexmoSms {
	
	private String from;
	private String user;
	private String pass;
	
	@Autowired
	ClienteSmsRegistroService clienteSmsRegistroService;
	
	@Autowired
    private Environment env;
	
	/**
	 * Envia validacion sms
	 * @param cellPhone
     * @param id
	 * @throws FastGasException 
	 */
	public void sendSms(String cellPhone,BigInteger id) throws FastGasException {
		recoverEnviroment();
		String codigoSms="";
		Calendar vigencia;
		//SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		//String currentDate = sdf.format(new Date());
		//esValido = clienteSmsRegistroService.esCodigoVigente(currentDate, codigoSms, id);
		
		int addCurrent = 5;
		//fecha actual 
		vigencia = Calendar.getInstance();
		vigencia.add(Calendar.MINUTE, addCurrent);
		  //GENERAMOS EL CODIGO DE ACCESO SMS
		  codigoSms  = String.valueOf(Math.abs(new Random(System.currentTimeMillis()).nextInt(90000)+10000));
		  //validamos en bd
	   	  //VALIDAMOS SI HAY UN CODIGO VIGENTE AL QUE GENERAMOS
		  builderMessage(cellPhone,codigoSms);
		 //guardamos el codigo
		  ClienteSmsRegistro sms =new ClienteSmsRegistro();
		  
		  sms.setIdCliente(id);
		  sms.setCodigo(codigoSms);
		  sms.setFechaVigencia(vigencia);
		  sms.setActivo(true);
		  
		  clienteSmsRegistroService.save(sms);		
		
	}
	/**
	 * 
     * @param codigo
	 * @param id
     * @return 
	 * @throws FastGasException
	 */
	public boolean esCodigoValido(String codigo,BigInteger id) throws FastGasException {
	
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String currentDate = sdf.format(new Date());
		
		return clienteSmsRegistroService.esCodigoVigente(currentDate, codigo, id);
		
	}
	
	
	
	
    /**
     * Construye el sms a enviar
     * @param toCellPhone
     */
	private void builderMessage(String toCellPhone,String codigoRegistro) {
		
		
		NexmoClient client = NexmoClient.builder()
		        .apiKey(this.user)
		        .apiSecret(this.pass)
		        .build();

		SmsSubmissionResponse responses = client.getSmsClient().submitMessage(new TextMessage(
		        this.from,
		        toCellPhone,
		        "FAST-GAS TU C\u00D3DIGO DE REGISTRO ES: "+codigoRegistro,true));
		for (SmsSubmissionResponseMessage response : responses.getMessages()) {
		    System.out.println(response);
		}
		
		//GENERAMOS CODIGO DE REGISTRO
		recoverEnviroment(); 
		
	}
	
	/**
	 * Recupera el from(cellphone to send sms)
	 */
	private void recoverEnviroment() {
		
		this.from = env.getProperty("nexmo.from");
		this.user = env.getProperty("nexmo.user");
		this.pass = env.getProperty("nexmo.pass");
		
		
	}
	
}
