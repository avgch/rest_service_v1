package com.enermex.utilerias;

import com.enermex.modelo.Parametro;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.ProductoPromocion;
import com.enermex.modelo.Promocion;
import com.enermex.modelo.RutaEntity;
import com.enermex.modelo.TipoPedido;
import com.google.gson.Gson;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.avg.exceptions.PedidoException;
import com.enermex.conekta.models.Servicio;
import com.enermex.dto.CancelacionEmailDto;
import com.enermex.dto.DisponibilidadPedidoDto;
import com.enermex.dto.PedidoDto;
import com.enermex.enumerable.TipoCombustible;
import com.enermex.enumerable.PedidoVarios;
import com.enermex.enumerable.PipaEstatusEnum;
import com.enermex.enumerable.ProductosEnum;
import com.enermex.repository.ParametroRepository;
import com.enermex.repository.PedidoRepository;
import com.enermex.repository.PipasRepository;
import com.enermex.modelo.TipoPlan;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.TipoPedidoRepository;
import com.enermex.repository.impl.PedidoRepositoryImpl;
import com.enermex.service.RutaService;
import com.enermex.enumerable.TipoPedidoEnum;
import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.repository.CancelacionRepository;
import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.Cancelacion;
import com.enermex.modelo.DireccionCliente;
import com.enermex.service.PromocionService;

/**
 *
 * @author abraham
 */
@Service
public class CalculoPedido {
	
	
	//Injectamos nuestro catalogo de parametros
	@Autowired
	ParametroRepository parametroRepository;
	
	@Autowired
	PedidoRepository pedidoRepository;
	
	@Autowired
	TipoPedidoRepository tipoPedidoRepository;
	
	@Autowired
	CancelacionRepository cancelacionRepository;
	
	@Autowired
	RutaService rutaService;
	
	@Autowired
	PedidoRepositoryImpl pedidoRepositoryImpl;
	
	@Autowired
	PipasRepository pipaRepository;
	
	@Autowired
	PromocionService promocionService;
	
	
    private static final Logger logger = LogManager.getLogger(CalculoPedido.class);

	/**
	 * Author: Abraham Vargas
	 * Obtiene el importe total a pagar
	 * @param precioPorLitro
	 * @param litros
	 * @return
	 */
	public float calculaImporteTotalPorLitros(double precioPorLitro, String litros) {
		
		//obtener el iva
		double importeIva,importe, iva=.16;
		importe=(precioPorLitro*(Float.parseFloat(litros)));
		importeIva = importe * iva;
		importe +=importeIva;
		
		return (float)importe;
		
	}
	
	/**
	 * Author: Abraham Vargas
	 * Calcula importe sin iva
	 * @param precioPorLitro
	 * @param litros
	 * @return
	 */
	public Double servicioGas(double precioPorLitro, String litros) {
		
		//obtener el iva
		Double importe;
		importe=(precioPorLitro*(Double.parseDouble(litros)));

		return importe;
		
	}
	
	/**
	 * Author: Abraham Vargas
	 * Calcula importe con iva	
	 * @param precioPorLitro
	 * @param litros
	 * @return
	 */
    public float calculaImporteIvaPorLitros(double precioPorLitro, String litros) {
		
		//obtener el iva
		double importeIva,importe, iva=.16;
		importe=(precioPorLitro*(Float.parseFloat(litros)));
		importeIva = importe * iva;
		return (float)importeIva;
		
	}
    
    /**
     * Author: Abraham Vargas
     * Calcula litros de carga con base al monto pedido del cliente
     * @param precioPorLitro
     * @param monto
     * @return
     */
    public Double calculaLitrosCarga(Double precioPorLitro, Double monto) {
	
    	
    	Double litros = (monto/precioPorLitro);
    
 		
    	return litros;
 	}
    
    /**
     * Author: Abraham Vargas
     * Se utilizara al final de la compra cuando el despachador finaliza el pedido
     * @param p
     * @return
     */
    @Transactional
    public String buildServiciosOrder(Pedido p) {
    	List<Servicio> servicios = new ArrayList<>();
    	
    	List<Parametro> tarifas = parametroRepository.costosPorServicioAndTimepoProrroga();
    	Promocion promo =promocionService.findById(p.getIdPromocion());
    	double cargoExtra=0;
    	
    	Servicio serv;
    	//DETERMINAMOS EL COSTO POR SERVICIO
    	
    	if(p.isCargaGas()) {
    		serv = new Servicio();
    		
    		if(TipoCombustible.DIESEL.getCombustible().compareTo(p.getIdCombustible())==0){
    			Double monto=null;
    			//validamos si tiene descuento
    			if(promo!=null)
    				monto=this.tienePromo(promo, p,true,false,false);
    			
    			if(monto==null) {
    				serv.setName("Carga de Gasolina Diesel");
        			//Consultamos el precio actual
        			serv.setUnit_price(this.formatMonto(p.getMontoCarga()));
        			serv.setQuantity("1");
        			cargoExtra+=p.getMontoCarga();
    			}
    			else {
    				serv.setName("Carga de Gasolina Diesel");
        			//Consultamos el precio actual
        			serv.setUnit_price(this.formatMonto(monto));
        			serv.setQuantity("1");
        			cargoExtra+=monto;
    			}
    			
    		
    		}
    		if(TipoCombustible.PREMIUM.getCombustible().compareTo(p.getIdCombustible())==0){
    			
    			Double monto=null;
    			//validamos si tiene descuento
    			if(promo!=null)
    				monto=this.tienePromo(promo, p,true,false,false);
    			
    			if(monto==null) {
	    			//validamos si tiene descuento
	    			serv.setName("Carga de Gasolina Premium");
	    			//Consultamos el precio actual
	    			serv.setUnit_price(this.formatMonto(p.getMontoCarga()));
	    			serv.setQuantity("1");
	    			cargoExtra+=p.getMontoCarga();
    			}
    			else {
    				//validamos si tiene descuento
	    			serv.setName("Carga de Gasolina Premium");
	    			//Consultamos el precio actual
	    			serv.setUnit_price(this.formatMonto(monto));
	    			serv.setQuantity("1");
	    			cargoExtra+=monto;
    				
    			}
			}
    		if(TipoCombustible.REGULAR.getCombustible().compareTo(p.getIdCombustible())==0){
    			
    			
    			Double monto=null;
    			//validamos si tiene descuento
    			if(promo!=null)
    				monto=this.tienePromo(promo, p,true,false,false);
    			
    			
    			if(monto==null) {
	    			//validamos si tiene descuento
	    			serv.setName("Carga de Gasolina Regular");
	    			//Consultamos el precio actual
	    			serv.setUnit_price(this.formatMonto(p.getMontoCarga()));
	    			serv.setQuantity("1");
	    			cargoExtra+=p.getMontoCarga();
    			}else {
    				//validamos si tiene descuento
	    			serv.setName("Carga de Gasolina Regular");
	    			//Consultamos el precio actual
	    			serv.setUnit_price(this.formatMonto(monto));
	    			serv.setQuantity("1");
	    			cargoExtra+=monto;
    				
    			}
    			
			}
    		servicios.add(serv);
    		
    		
    	}
    	
    	if(p.isLavadoAuto()) {
    		
			Double monto=null;
			//validamos si tiene descuento
			if(promo!=null)
				monto=this.tienePromo(promo, p,false,true,false);
    		
    		if(monto==null) {
				//validamos si tiene descuento
	    		serv = new Servicio();
	    		serv.setName("Servicio de lavado de automóvil");
				serv.setUnit_price(this.formatMonto(p.getPrecioLavadoAuto()));
				serv.setQuantity("1");
				servicios.add(serv);
				cargoExtra+=p.getPrecioLavadoAuto();
    		}else {
    			//validamos si tiene descuento
	    		serv = new Servicio();
	    		serv.setName("Servicio de lavado de automóvil");
				serv.setUnit_price(this.formatMonto(monto));
				serv.setQuantity("1");
				servicios.add(serv);
				cargoExtra+=monto;

    		}
			
			
			
    	}
    	//sin costo
    	
    	if(p.isRevisionNeumaticos()) {
    		
    		Double monto=null;
			//validamos si tiene descuento
			if(promo!=null)
				monto=this.tienePromo(promo, p,false,false,true);
			
			if(monto==null) {
				//validamos si tiene descuento
	    		serv = new Servicio();
	    		serv.setName("Servicio de revisión de neumáticos");
				serv.setUnit_price(this.formatMonto(p.getPrecioPresionNeumaticos()));
				serv.setQuantity("1");
				servicios.add(serv);
				cargoExtra+=p.getPrecioPresionNeumaticos();
			}else {
				
				//validamos si tiene descuento
	    		serv = new Servicio();
	    		serv.setName("Servicio de revisión de neumáticos");
				serv.setUnit_price(this.formatMonto(monto));
				serv.setQuantity("1");
				servicios.add(serv);
				cargoExtra+=monto;

			}
			
			
    	}
    	
    	
    	buildCostoServiciosOrderFinal(p,servicios,cargoExtra);
    	Gson gson = new Gson();
    	
    	return gson.toJson(servicios);
    	
    }
    
    /**
     * Author: Abraham Vargas
     * Agregamos el costo del servicio de acuerdo al tipo de pedido 
     * @param p
     * @param servicios
     * @param cargoExtra
     * @return
     */
    public List<Servicio> buildCostoServiciosOrderFinal(Pedido p,List<Servicio> servicios,double cargoExtra) {
    	
    	
		
    	Servicio serv  = new Servicio();
    	serv.setName("Costo por servicio FAST-GAS");
    	serv.setUnit_price(this.formatMonto(p.getCostoServicio()));
    	serv.setQuantity("1");
    	servicios.add(serv);
    	cargoExtra+=p.getCostoServicio();
    	
    	System.out.println("Total de cargo:");
    	System.out.println(cargoExtra);
    	
    	System.out.println("Porcentaje extra:");
    	System.out.println(cargoExtra);
    	double porcentajeExtra= cargoExtra*.15;
    	
//    	//agregamos el cargofinal
    	Servicio servFinal  = new Servicio();
    	servFinal.setName("Cargo extra por servicio FAST-GAS");
    	servFinal.setUnit_price(this.formatMonto(porcentajeExtra));
    	servFinal.setQuantity("1");
    	servicios.add(servFinal);
    	
    
    	return servicios;
    	
    }
    
    /**
     * Author: Abraham Vargas 
     * Obtiene el tipo de pedido (programado,mismo dia o urgente)
     * @param dias
     * @param horas
     * @param minutos
     * @return
     */
    public TipoPedido tipoPedido(int dias,int horas,int minutos) {
    	
    	 horas+=dias*24;
    	 String minutosS="";
    	List<Parametro> paramtetros = parametroRepository.costosPorServicioAndTimepoProrroga();

        TipoPedido tp = null;
        if(minutos<10)
        	minutosS="0"+minutos;
        else
        	minutosS=String.valueOf(minutos);
        
    	String timpoToPedido = String.valueOf(horas)+"."+minutosS;
        
        float tiempoParaPedido=Float.parseFloat(timpoToPedido);
    	//Obtenemos  el tiempo x para considerar si es un pedido programado
    	Parametro programadoProrroga = paramtetros.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_PROGRAMADO_EN_TIEMPO.getValor()))
    	.findAny()
    	.orElse(null);
    	
    	//Obtenemos  el tiempo x para considerar si es un pedido mismo dia limite inferior
    	Parametro mismoDiaProrrogaInf = paramtetros.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_MISMO_DIA_EN_TIEMPO_LI_INF.getValor()))
    	    	.findAny()
    	    	.orElse(null);
    	
    	//Obtenemos  el tiempo x para considerar si es un pedido mismo dia limite superior
    	Parametro mismoDiaProrrogaSup = paramtetros.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_MISMO_DIA_EN_TIEMPO_LI_SUP.getValor()))
    	    	.findAny()
    	    	.orElse(null);
    	//Obtenemos  el tiempo x para considerar si es un pedido urgente limite inferior
    	Parametro urgenteProrrogaLimiteInf = paramtetros.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_URGENTE_EN_TIEMPO_LI_INF.getValor()))
    	    	.findAny()
    	    	.orElse(null);
    	
    	//Obtenemos  el tiempo x para considerar si es un pedido urgente limite superior
    	Parametro urgenteProrrogaLimiteSuperior = paramtetros.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_URGENTE_EN_TIEMPO_LI_SUP.getValor()))
    	    	.findAny()
    	    	.orElse(null);
    	
    	//validamos si es pedido programado con base a fecha creacion y fecha pedido
    	if(tiempoParaPedido>=Float.parseFloat(programadoProrroga.getValor())) {
    		
        	
    		tp =tipoPedidoRepository.getTipoPedido(TipoPedidoEnum.PROGRAMADO.getId());
    		
    	}
    	//validamos si es pedido mismo dia con base a fecha creacion y fecha pedido
    	else if(Float.parseFloat(mismoDiaProrrogaInf.getValor())<=tiempoParaPedido &&  Float.parseFloat(mismoDiaProrrogaSup.getValor())>=tiempoParaPedido) {
    		System.out.println("Mismo dia");
  
    		tp =tipoPedidoRepository.getTipoPedido(TipoPedidoEnum.MISMO_DIA.getId());
    		
    	}
    	//validamos si es pedido urgente con base a fecha creacion y fecha pedido
    	else if(Float.parseFloat(urgenteProrrogaLimiteInf.getValor())<=tiempoParaPedido &&  Float.parseFloat(urgenteProrrogaLimiteSuperior.getValor())>=tiempoParaPedido) {
    		System.out.println("Pedido urgente");
    		tp =tipoPedidoRepository.getTipoPedido(TipoPedidoEnum.URGENTE.getId());
    
    	}
    	
    	return tp;
    }
    /**
     * 
     * @param p
     * @param cancelacionDto
     * @return
     */
    public CancelacionEmailDto getDatosCancelacion(Pedido p,CancelacionEmailDto cancelacionDto ) {
    	
    	String pattern = "dd/MM/yy HH:mm";
    	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    	sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    	
    	Cancelacion cancelacion = cancelacionRepository.getById(p.getIdPedido());
    	
    	Set<AutomovilCliente> autos = p.getCliente().getAutos();
    	Set<DireccionCliente> dirs = p.getCliente().getDirecciones();  
    	
    	AutomovilCliente autoE =autos.stream().filter(auto -> auto.getIdAutomovil().equals(p.getIdAutomovil()))
    			.findAny()
    			.orElse(null);
          
    	DireccionCliente dirE =dirs.stream().filter(dir -> dir.getIdDireccion().equals(p.getIdDireccion()))
    			.findAny()
    			.orElse(null);
          
         		
 		
    			
    	
    	
    	//Pasamos la fecha de pedido a string 
    	
    	Date fechaPedido = p.getFechaPedido().getTime();
    	String fechaPedidoS = sdf.format(fechaPedido);
    	
    	Date fechaCreacion = cancelacion.getFechaCreacion().getTime();
    	
    	sdf.setTimeZone(TimeZone.getTimeZone("Mexico/General"));
    	String fechaCreacionS = sdf.format(fechaCreacion);
    	
    	cancelacionDto.setFechaServicio(fechaPedidoS);
    	cancelacionDto.setMotivoCancelacion(cancelacion.getMotivo().getMotivo());
    	cancelacionDto.setFechaCancelacion(fechaCreacionS);
    	cancelacionDto.setDireccion(dirE.getColonia()+" Calle:"+dirE.getCalle()+" "+dirE.getCp());
    	cancelacionDto.setAutomovil("Marca: "+autoE.getMarca()+ " Modelo: "+autoE.getModelo()+" Placas: "+autoE.getPlacas());
    	
    	cancelacionDto.setIdServicio(p.getUuid());
    	if(cancelacionDto.isPenalizacion()) {
    	 Double montoDevuelto = p.getTotalPagar()- Float.parseFloat(cancelacionDto.getMontoPenalizacion());	
    	  cancelacionDto.setMontoDevuelto(String.valueOf(montoDevuelto));
    	  
    		
    	}else {
    		cancelacionDto.setMontoDevuelto(String.valueOf(p.getTotalPagar()));
    		cancelacionDto.setMontoPenalizacion("0");
    	}
    	
    	
    	return cancelacionDto;
    }
    
    /**
     * 
     * @param montoP
     * @return
     */
    public String formatMonto(Double montoP) {
    	 
    	 Double monto = (montoP==null) ? 0 : montoP;
    	
    	
    	 String montoF = String.format("%.2f",monto);
    	 
    	 montoF=montoF.replace(".", "");
    	
    	return montoF;
    }
    
    /**
     *
     * @param idHorario
     * @param fecha
     * @param p
     * @return
     * @throws PedidoException
     * @throws ParseException
     */
    public DisponibilidadPedidoDto validaDisponibilidad(BigInteger idHorario,String fecha,PedidoDto p) throws PedidoException, ParseException {
    	/*FECHA PEDIDO FORMATEO*/
    	SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
    	SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
    	TimeZone tz = TimeZone.getDefault();
    	sdf1.setTimeZone(tz);
    	//fecha pedido
    	Date pedido = sdf1.parse(fecha.split(" ")[0]);
    	String pedidoS = sdf2.format(pedido);
    	/*END PEDIDO FORMATEO*/
    	
    	/*FECHA HOY FORMATEO*/
    	Date fechaHoyD = new Date();
    	String fechaHoyS = sdf2.format(fechaHoyD);
    	/*END FECHA HOY FORMATEO*/
    	
    	RutaEntity ruta =null;
    	DisponibilidadPedidoDto disponible= null;
    	List<DisponibilidadPedidoDto> disponibles= null;
    	List<UsuarioEntity> hayDespachadores= new ArrayList<>();
	    UsuarioEntity  despachador= null;
	    List<RutaEntity> rutas = this.rutaService.getRutasActivas(p.getCp());
	    List<UsuarioEntity> despachadoresActivos=null ;
	    //obtenemos la ruta al azar
	    //int idRuta =(int) (Math.random() * rutas.size()); 
	    //obtenemos la ruta
		//ruta = rutas.get(idRuta);
		//Validamos que al menos tengamos un despachador
		for(RutaEntity r  :rutas) {
			
			if(r.getDespachadores()!=null) {
				for(UsuarioEntity d : r.getDespachadores()) {
					hayDespachadores.add(d);		
				}
			}
		}
		
		
		
		if(hayDespachadores==null)
			throw new  PedidoException("FG021","La ruta no cuenta con despachadores en este momento");
		
		
		//SI FECHA DE PEDIDO ES IGUAL A LA FECHA DE CONSULTA
		if(pedidoS.equalsIgnoreCase(fechaHoyS)==false) {
			//Validamos que solo sean despachadores activos y autotanques activos
			//Depende del tipo de combustible que valide si tiene o no gasolina
			if(p.getIdCombustible().compareTo(TipoCombustible.DIESEL.getCombustible())==0) {
				despachadoresActivos= hayDespachadores.stream().filter(des ->  des.getEstatus().getEstatus().equalsIgnoreCase(UsuarioEstatus.ACTIVO.getEstatus())
						&& des.getPipa().getEstatus().equalsIgnoreCase(PipaEstatusEnum.ACTIVO.getEstatus()) )
				.collect(Collectors.toList());	
				
			}
			if(p.getIdCombustible().compareTo(TipoCombustible.REGULAR.getCombustible())==0) {
				despachadoresActivos= hayDespachadores.stream().filter(des ->  des.getEstatus().getEstatus().equalsIgnoreCase(UsuarioEstatus.ACTIVO.getEstatus())
						&& des.getPipa().getEstatus().equalsIgnoreCase(PipaEstatusEnum.ACTIVO.getEstatus()))
				.collect(Collectors.toList());	
				
			}
			
			if(p.getIdCombustible().compareTo(TipoCombustible.PREMIUM.getCombustible())==0) {
				despachadoresActivos= hayDespachadores.stream().filter(des ->  des.getEstatus().getEstatus().equalsIgnoreCase(UsuarioEstatus.ACTIVO.getEstatus())
						&& des.getPipa().getEstatus().equalsIgnoreCase(PipaEstatusEnum.ACTIVO.getEstatus()) )
				.collect(Collectors.toList());	
				
			}
			
		}
		else {
			//Validamos que solo sean despachadores activos y autotanques activos
			//Depende del tipo de combustible que valide si tiene o no gasolina
			if(p.getIdCombustible().compareTo(TipoCombustible.DIESEL.getCombustible())==0) {
				despachadoresActivos= hayDespachadores.stream().filter(des ->  des.getEstatus().getEstatus().equalsIgnoreCase(UsuarioEstatus.ACTIVO.getEstatus())
						&& des.getPipa().getEstatus().equalsIgnoreCase(PipaEstatusEnum.ACTIVO.getEstatus())  && ((des.getPipa().getLitrosRestantesDiesel()==null) ? 0 :  des.getPipa().getLitrosRestantesDiesel()) >= new Double(p.getLitroCarga()) )
				.collect(Collectors.toList());	
				//update LitrosRestantesDiesel
				
				//this.pipaRepository.updateLitrosRestantesDiesel(diferenciaLitros, pedido.getIdPipa());
			}
			if(p.getIdCombustible().compareTo(TipoCombustible.REGULAR.getCombustible())==0) {
				despachadoresActivos= hayDespachadores.stream().filter(des ->  des.getEstatus().getEstatus().equalsIgnoreCase(UsuarioEstatus.ACTIVO.getEstatus())
						&& des.getPipa().getEstatus().equalsIgnoreCase(PipaEstatusEnum.ACTIVO.getEstatus()) && ((des.getPipa().getLitrosRestantesRegular()==null) ? 0 : des.getPipa().getLitrosRestantesRegular()) >= new Double(p.getLitroCarga())    )
				.collect(Collectors.toList());	
			
				//update LitrosRestantesRegular
				//this.pipaRepository.updateLitrosRestantesDiesel(diferenciaLitros, pedido.getIdPipa());
			}
			
			if(p.getIdCombustible().compareTo(TipoCombustible.PREMIUM.getCombustible())==0) {
				despachadoresActivos= hayDespachadores.stream().filter(des ->  des.getEstatus().getEstatus().equalsIgnoreCase(UsuarioEstatus.ACTIVO.getEstatus())
						&& des.getPipa().getEstatus().equalsIgnoreCase(PipaEstatusEnum.ACTIVO.getEstatus()) && ((des.getPipa().getLitrosRestantesPremium()==null) ? 0 :  des.getPipa().getLitrosRestantesPremium()) >= new Double(p.getLitroCarga()) )
				.collect(Collectors.toList());	
				
				//update LitrosRestantesPremium
				//this.pipaRepository.updateLitrosRestantesDiesel(diferenciaLitros, pedido.getIdPipa());
				
			}
		}
		
		if(despachadoresActivos==null)
			throw new  PedidoException("La ruta no cuenta con despachadores en este momento","FG021");
		
		if(despachadoresActivos.size()==0)
			throw new  PedidoException("La ruta no cuenta con despachadores en este momento","FG021");
		
		disponibles=this.pedidoRepositoryImpl.getOcupabilidadCierraPedido(pedidoS,despachadoresActivos);
		
		
		disponible=disponibles.stream().filter(aviable -> aviable.getIdHorario().compareTo(idHorario)==0)
		.findAny().orElse(null);
		
		if(disponible==null)
			throw new  PedidoException("No disponible, por favor eliga otra opción","FG022");
		
		//Determinamos la ruta
       for(RutaEntity r  :rutas) {
			
			if(r.getDespachadores()!=null) {
				for(UsuarioEntity d : r.getDespachadores()) {
					 if(d.getIdUsuario().compareTo(disponible.getIdDespachador())==0) {
						 disponible.setIdRuta(r.getIdRuta());
						 
							if(p.getIdCombustible().compareTo(TipoCombustible.DIESEL.getCombustible())==0 && (p.getTipoPedidoP().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
								
								if(((d.getPipa().getLitrosRestantesDiesel()==null)? 0 : d.getPipa().getLitrosRestantesDiesel())< new Double(p.getLitroCarga())) 
									throw new  PedidoException("La pipa no cuenta con suficiente combustible para atender su pedido.","FG036");
							
								Double litrosRestantes;
								litrosRestantes = d.getPipa().getLitrosRestantesDiesel()-new Double(p.getLitroCarga());
								this.pipaRepository.updateLitrosRestantesDiesel(litrosRestantes,d.getPipa().getIdPipa());
								
							}
							if(p.getIdCombustible().compareTo(TipoCombustible.REGULAR.getCombustible())==0 && (p.getTipoPedidoP().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
								
								if(((d.getPipa().getLitrosRestantesRegular()==null)? 0 : d.getPipa().getLitrosRestantesRegular())< new Double(p.getLitroCarga())) 
									throw new  PedidoException("La pipa no cuenta con suficiente combustible para atender su pedido.","FG036");
								
								Double litrosRestantes;	
								litrosRestantes = d.getPipa().getLitrosRestantesRegular()-new Double(p.getLitroCarga());
								this.pipaRepository.updateLitrosRestantesRegular(litrosRestantes,d.getPipa().getIdPipa());
							}
							
							if(p.getIdCombustible().compareTo(TipoCombustible.PREMIUM.getCombustible())==0 && (p.getTipoPedidoP().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
								if(((d.getPipa().getLitrosRestantesPremium()==null)? 0 : d.getPipa().getLitrosRestantesPremium())< new Double(p.getLitroCarga())) 
									throw new  PedidoException("La pipa no cuenta con suficiente combustible para atender su pedido.","FG036");
								
								Double litrosRestantes;
								litrosRestantes = d.getPipa().getLitrosRestantesPremium()-new Double(p.getLitroCarga());
								this.pipaRepository.updateLitrosRestantesPremium(litrosRestantes,d.getPipa().getIdPipa());
								
							}
						 
						 
						 break;
					 }
						  
						 
				}
			}
		}
		
				 
		return disponible;
    }
    
    /**
     * 
     * @param promo
     * @param pedido
     * @return
     */
    private Double tienePromo(Promocion promo,Pedido pedido,boolean gas ,boolean esLavado,boolean esRevision) {
    	
       boolean esDescuentoresult =false;
       Double  monto=null;
       double descuento,precioLavado,precioRevision;
      // Promocion promo =promocionService.getPromoById(id);
       
        
        esDescuentoresult = (promo.getDescuento()!=null) ? true : false;
   	    //BUSCAMOS PROMO PARA CARGA DE GASOLINA
		 ProductoPromocion promoGas= promo.getProductosPromo().stream().filter(prod -> prod.getProductoServicio().getId().compareTo(new Long(pedido.getIdCombustible()))==0)
		    .findAny()
		    .orElse(null);
		
		 if(promoGas!=null && gas) {
			  
			 if(esDescuentoresult) {
			  descuento = Double.parseDouble(promo.getDescuento());
			  descuento = (descuento*pedido.getMontoCarga())/100; 
			  monto     = pedido.getMontoCarga() - descuento;
		    }
		  else {
			  if(new Double(pedido.getMontoCarga())>promo.getMonto()) {
				  descuento = promo.getMonto();
				  monto = pedido.getMontoCarga() - descuento;
			  }
		   }
			 return monto;
		 }
		 
		 
		 
		 
			//BUSCAMOS PROMO LAVADO DE AUTO
		 ProductoPromocion promoLavado= promo.getProductosPromo().stream().filter(prod -> prod.getProductoServicio().getId().compareTo(new Long(ProductosEnum.LAVADO_AUTO.getValor()))==0)
		    .findAny()
		    .orElse(null);
		
		 //SI TIENE PROMO VALIDAMOS SI ES POR DESCUENTO O POR MONTO
		 if(promoLavado!=null && esLavado) {
			  //descuento %
			 if(esDescuentoresult) {
				 
				 if(pedido.isLavadoAuto()) {
					  precioLavado = (pedido.getPrecioLavadoAuto()==null) ? 0 : pedido.getPrecioLavadoAuto() ;
						 //Validamos si existe descuento para lavado de auto solicitado
					  descuento = Double.parseDouble(promo.getDescuento());
					  descuento = ((descuento*precioLavado)/100); 
					  monto     = precioLavado-descuento;
			    }
		    }
			//descuento por monto
		  else {
				 if(pedido.isLavadoAuto()) {
					 precioLavado = (pedido.getPrecioLavadoAuto()==null) ? 0 : pedido.getPrecioLavadoAuto() ;
					 if(precioLavado>promo.getMonto()) {
					 //Validamos si existe descuento para lavado de auto solicitado
					  descuento = promo.getMonto();
					  monto     = precioLavado - descuento;
					 }
			    }
		   }
			 return monto;
		 }
		 
		 
		//BUSCAMOS PROMO LAVADO DE AUTO
		 ProductoPromocion promoNeumaticos= promo.getProductosPromo().stream().filter(prod -> prod.getProductoServicio().getId().compareTo(new Long(ProductosEnum.REVISION_NEUMATICOS.getValor()))==0)
		    .findAny()
		    .orElse(null);
		 
		 //SI TIENE PROMO VALIDAMOS SI ES POR DESCUENTO O POR MONTO
		 if(promoNeumaticos!=null && esRevision) {
			 double neumaticosD= (pedido.getPrecioPresionNeumaticos()==null) ? 0 : pedido.getPrecioPresionNeumaticos();
			  //descuento %
			 if(esDescuentoresult) {
				 
				 if(pedido.isRevisionNeumaticos() && neumaticosD>0) {
					 precioRevision = (pedido.getPrecioPresionNeumaticos()==null) ? 0 : pedido.getPrecioPresionNeumaticos() ;
						 //Validamos si existe descuento para lavado de auto solicitado
					  descuento = Double.parseDouble(promo.getDescuento());
					  descuento = ((descuento*precioRevision)/100); 
					  monto     = precioRevision - descuento;
			    }
		    }
			//descuento por monto
		  else {
				 if(pedido.isRevisionNeumaticos() && neumaticosD>0 ) {
					 if(neumaticosD>promo.getMonto()) {
						 precioRevision = (pedido.getPrecioPresionNeumaticos()==null) ? 0 : pedido.getPrecioPresionNeumaticos() ;
						 //Validamos si existe descuento para lavado de auto solicitado
						  descuento = promo.getMonto();
						  monto     =  precioRevision - descuento;
					 }
			    }
		   }
			 return monto;
		 }
		 
		 
		 return null;
    	
    	
    }
    
    
    
    

    

}
