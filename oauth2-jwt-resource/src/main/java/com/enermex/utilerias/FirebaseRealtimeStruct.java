package com.enermex.utilerias;

import java.util.UUID;

import com.google.cloud.firestore.annotation.IgnoreExtraProperties;

/**
 * FirebaseRealtimeStruct
 */
@IgnoreExtraProperties
public class FirebaseRealtimeStruct {

    /**
     *
     */
    public Long user;

    /**
     *
     */
    public Object data;

    /**
     *
     */
    public String uuid;

  FirebaseRealtimeStruct(Long user, Object data) {
    this.user = user;
    this.data = data;
    this.uuid = UUID.randomUUID().toString();
  }
}
