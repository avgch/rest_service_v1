package com.enermex.utilerias;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import com.enermex.enumerable.MsjBloqueCuentaEnum;
import com.enermex.modelo.Cliente;
import com.sendgrid.*;

// TODO: Extender bien

/**
 *
 * @author abraham
 */
@Component
public class SmtpQueja {
  

  @Autowired
  private Environment env;
  
  private String username;
  private String password;
  String error ="Ok";
  private String urlCorreo;
  private String urlSitio;
  
  private String correoEnvio;
  
  private static final Logger logger = LogManager.getLogger(SmtpGmailSsl.class);

  private String getHtml(String title, String event, String data) {
    try {
    	recoverEnviroment();
      Resource resource = new ClassPathResource("bloqueo.html");
      File file = resource.getFile();
      BufferedReader br = new BufferedReader(new FileReader(file));

      StringBuilder html = new StringBuilder();
      String replacement = event + "<br>" + data;
      String line;

      while ((line = br.readLine()) != null) {
        html.append(line.replace("[!data!]", replacement).replace("[!title!]", title));
      }

      br.close();
      return html.toString();
    } catch (Exception e) {
      return "";
    }
  }

    /**
     *
     * @param email
     * @param asunto
     * @return
     */
    public boolean sendEmailAsunto(String email, String asunto) {
    return sendEmail(email, "Asunto:", asunto);
  }

    /**
     *
     * @param email
     * @param asunto
     * @return
     */
    public boolean sendEmailAtendido(String email, String asunto) {
    return sendEmail(email, "Resolución:", asunto);
  }

  private boolean sendEmail(String email, String event, String asunto) {
    try {
    	
    	recoverEnviroment();
      // Configuración de Azure SendGrid
      SendGrid sg = new SendGrid(password);
      
      // Datos  del correo electrónico
      String subject = "FAST GAS RECLAMACIONES";
      Email from = new Email(correoEnvio, username);
      Email to = new Email(email);
      
      // Contenido del correo
      Content content = new Content();
      content.setType("text/html");
      content.setValue(getHtml("Tu reclamo ser&aacute; procesado a la brevedad", event, asunto));
      
      // Solicitud de envío
      Mail mail = new Mail(from, subject, to, content);
      Request request = new Request();
      request.setMethod(Method.POST);
      request.setEndpoint("mail/send");
      request.setBody(mail.build());

      Response response = sg.api(request);

      // Finalización correcta
      return true;
    } catch (Exception e) {
      e.printStackTrace();

      return false;
    }
  }

    /**
     *
     * @param email
     * @param nombre
     * @param bloqueo
     * @param hayServicios
     * @return
     */
    public boolean sendEmailBloqueado(String email, String nombre,boolean bloqueo,boolean hayServicios) {
	  
    try {
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    	String fechaBaja = sdf.format(new Date()); 
    	
    	recoverEnviroment();
    	
    	//precio por litro
    	
       String mensaje = MsjBloqueCuentaEnum.MSJ_BLOQUEO_CUENTA_FAMILIAR.getValor().replace("NOMBRE_FAMILIAR",nombre);//
       mensaje = mensaje.replace("FECHA_CAMBIO",fechaBaja);
       if(bloqueo)
    	   mensaje = mensaje.replace("BLOQUE_DESBLOQUEO", "bloqueo");
       else
    	   mensaje = mensaje.replace("BLOQUE_DESBLOQUEO", "desbloqueo");
       
       if(hayServicios)
    	   mensaje = mensaje.replace("CUENTA_O_NO_CUENTA", "cuenta");
       else
    	   mensaje = mensaje.replace("CUENTA_O_NO_CUENTA", "no cuenta");
       
      // Configuración de Azure SendGrid
      SendGrid sg = new SendGrid(password);
      
      // Datos  del correo electrónico
      String subject = "FAST GAS "+((bloqueo==true) ? "BLOQUEO" : "DESBLOQUEO" )+" DE CUENTA";
      Email from = new Email(correoEnvio, username);
      Email to = new Email(email);
      
      // Contenido del correo
      Content content = new Content();
      content.setType("text/html");
      content.setValue(getHtml(((bloqueo==true) ? "Bloqueo" : "Desbloqueo" )+" de cuenta " + email, "",mensaje));
      
      // Solicitud de envío
      Mail mail = new Mail(from, subject, to, content);
      Request request = new Request();
      request.setMethod(Method.POST);
      request.setEndpoint("mail/send");
      request.setBody(mail.build());

      Response response = sg.api(request);

      // Finalización correcta
      return true;
    } catch (Exception e) {
      e.printStackTrace();

      return false;
    }
  }
  
    /**
     *
     * @param titular
     * @param familiar
     * @param bloqueo
     * @param hayServicios
     * @return
     */
    public boolean sendEmailBloqueador(Cliente titular, String familiar,boolean bloqueo,boolean hayServicios) {
	    try {
	    	
	    	recoverEnviroment();
	    	
	    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    	String fechaBaja = sdf.format(new Date()); 
	      // Configuración de Azure SendGrid
	      SendGrid sg = new SendGrid(password);
	      //NOMBRE_TITULAR
	      String nombreCompleto = titular.getNombre();
	      nombreCompleto        +=" " +(titular.getApellidoMaterno()==null ? "" : titular.getApellidoMaterno());
	      nombreCompleto        +=" " + (titular.getApellidoPaterno()==null ? "" : titular.getApellidoPaterno());
	      
	   	
	       String mensaje = MsjBloqueCuentaEnum.MSJ_BLOQUEO_CUENTA.getValor().replace("NOMBRE_FAMILIAR",familiar);
	       mensaje = mensaje.replace("NOMBRE_TITULAR", nombreCompleto);
	       mensaje = mensaje.replace("FECHA_CAMBIO",fechaBaja);
	       if(bloqueo)
	    	   mensaje = mensaje.replace("BLOQUE_DESBLOQUEO", "bloqueo");
	       else
	    	   mensaje = mensaje.replace("BLOQUE_DESBLOQUEO", "desbloqueo");
	       
	       if(hayServicios)
	    	   mensaje = mensaje.replace("CUENTA_O_NO_CUENTA", "cuenta");
	       else
	    	   mensaje = mensaje.replace("CUENTA_O_NO_CUENTA", "no cuenta");
	      
	      
	      
	      // Datos  del correo electrónico
	      String subject = "FAST GAS "+((bloqueo==true) ? "BLOQUEO" : "DESBLOQUEO" )+" DE CUENTA";
	      Email from = new Email(correoEnvio, username);
	      Email to = new Email(titular.getCorreo());
	      
	      // Contenido del correo
	      Content content = new Content();
	      content.setType("text/html");
	      content.setValue(getHtml(((bloqueo==true) ? "Bloqueo" : "Desbloqueo" )+" de cuenta " + familiar, "",mensaje));
	      
	      // Solicitud de envío
	      Mail mail = new Mail(from, subject, to, content);
	      Request request = new Request();
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());

	      Response response = sg.api(request);

	      // Finalización correcta
	      return true;
	    } catch (Exception e) {
	      e.printStackTrace();

	      return false;
	    }
	  }
	  
  
  
   //"Bloqueo de cuenta " + email,  --> motivo
  //"Tu cuenta ha sido bloqueada " + email,  --> detalle

    /**
     *
     * @param email
     * @param nombre
     * @param titulo
     * @param motivo
     * @param detalle
     * @return
     */
  
  public boolean sendEmailBloqueadoSys(String email, String nombre,String titulo,String motivo,String detalle) {
    try {
    	
    	recoverEnviroment();
      // Configuración de Azure SendGrid
      SendGrid sg = new SendGrid(password);
      
      // Datos  del correo electrónico
      String subject = titulo;
      Email from = new Email(correoEnvio, username);
      Email to = new Email(email);
      
      // Contenido del correo
      Content content = new Content();
      content.setType("text/html");
      content.setValue(getHtml(motivo, "Hola " + nombre, detalle));
      
      // Solicitud de envío
      Mail mail = new Mail(from, subject, to, content);
      Request request = new Request();
      request.setMethod(Method.POST);
      request.setEndpoint("mail/send");
      request.setBody(mail.build());

      Response response = sg.api(request);

      // Finalización correcta
      return true;
    } catch (Exception e) {
      e.printStackTrace();

      return false;
    }
  }
  
	private void recoverEnviroment() {
		//cuenta de la c
		this.correoEnvio = env.getProperty("correo.envio");
		this.logger.info("correo de envio: "+correoEnvio);
		this.password  = env.getProperty("sendgrid.password");
		this.logger.info("correo de envio: "+password);
		this.username  = env.getProperty("sendgrid.username");
		this.logger.info("correo de envio: "+username);
		this.urlSitio  = env.getProperty("azureEnviroments.web.prod");
		this.logger.info("correo de envio: "+urlSitio);
		this.urlCorreo = env.getProperty("azureEnviroments.correo.prod.rs");
		this.logger.info("correo de envio: "+urlCorreo);
		
		
	}
	  
  
}
