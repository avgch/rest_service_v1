package com.enermex.utilerias;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.enermex.dto.CancelacionEmailDto;
import com.enermex.dto.TicketFinalDto;
import com.enermex.enumerable.PedidoEstatus;
import com.enermex.enumerable.TipoCombustible;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.Promocion;
import com.enermex.repository.PromocionRepository;
import com.enermex.service.PromocionService;
import com.enermex.service.TokenService;
import com.enermex.dto.AppStoreDto;
import com.sendgrid.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

/**
 *
 * @author abraham
 */
@Component
@ConfigurationProperties("envios")
public class SmtpGmailSsl {
	
	private String username;
    private String password;
    String error ="Ok";
    private String urlCorreo;
    private String urlSitio;
    
    private SendGrid sendGrida;
    
    @Autowired
    PromocionService  promocionService;
    
    @Autowired
    ResourceLoader resourceLoader;
    
    @Autowired
    private Environment env;
    
    @Autowired
    CalculoPedido calculoPedido;
    
    private String correoEnvio;
    
    private static final Logger logger = LogManager.getLogger(SmtpQueja.class);
    
    /**
     *
     * @param toEmail
     * @param token
     * @param tipoEmail
     * @param app
     * @return
     */
    public String sendEmail(String toEmail,String token,String tipoEmail,String app) {
		
    try {
      
    	recoverEnviroment();
		Email from = new Email(correoEnvio,username);
	    Email to = new Email(toEmail);
	    //Attachments attachment = new Attachments();
	   // Content content = new Content("text/plain", "and easy to do anywhere, even with Java");

	    SendGrid sg = new SendGrid(password);
	    Request request = new Request();

	   	    	
	  if(tipoEmail.equalsIgnoreCase("recoveryPass")) {
		  
		  String subject = "FAST GAS RECUPERACIÓN DE CONTRASEÑA";
		  
		  
	      Content content = new Content();
	      content.setType("text/html");
	      content.setValue(buildBodyEmailRecupera(app,token));
	      Mail mail = new Mail(from, subject, to, content);
	        
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      
	      Response response = sg.api(request);
      }
      if(tipoEmail.equalsIgnoreCase("registroDespachador")) {
		  
		  String subject = "VALIDACIÓN DE REGISTRO";
		  
		  
	      Content content = new Content();
	      content.setType("text/html");
	      content.setValue(buildBodyEmailRegistroDespachador(app,token));
	      Mail mail = new Mail(from, subject, to, content);
	        
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      
	      Response response = sg.api(request);
      }
      if(tipoEmail.equalsIgnoreCase("registroInvitado")) {
		  
  		  String subject = "VALIDACIÓN DE REGISTRO";
  		  
  		  
  	      Content content = new Content();
  	      content.setType("text/html");
  	      content.setValue(buildBodyEmailRegistroInvitado(app,token));
  	      Mail mail = new Mail(from, subject, to, content);
  	        
  	      request.setMethod(Method.POST);
  	      request.setEndpoint("mail/send");
  	      request.setBody(mail.build());
  	      
  	      Response response = sg.api(request);
        }
      if(tipoEmail.equalsIgnoreCase("registro")) {
    	  
    	  String subject = "VALIDACIÓN DE REGISTRO";
    	  Content content = new Content();
    	  content.setType("text/html");
    	  content.setValue(buildBodyEmail(toEmail,token));
    	  Mail mail = new Mail(from, subject, to, content);
	         
	      
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
    
	        
      }
     if(tipoEmail.equalsIgnoreCase("registroUsuarioWeb")) {
    	  
    	  String subject = "VALIDACIÓN DE REGISTRO";
    	  Content content = new Content();
    	  content.setType("text/html");
    	  content.setValue(registroUsuarioWeb(toEmail,token));
    	  Mail mail = new Mail(from, subject, to, content);
	         
	      
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
    
	        
      }
  
     
	      
	    } catch (IOException ex) {
	      System.out.println(ex);
	    }

	
	    return error;
		
	}

	/**
	 * Notifica Registro de familiar
	 * @param toEmail
	 * @param token
	 * @param tipoEmail
	 * @param app
	 * @return
	 */
	public String sendEmailNotificaFamiliarTitular(String toEmail,String tipoEmail) {
		
	    try {
	    	recoverEnviroment();
		    
		    String[] emailInvitado =  tipoEmail.split("&"); 
	    	

			Email from = new Email(correoEnvio,username);
		    
		    Email to = new Email(toEmail);

		    SendGrid sg = new SendGrid(password);
		    Request request = new Request();
		    	
	      if(emailInvitado[0].equalsIgnoreCase("notificaFamiliar")) {
	    	
	    	  
	    	  String subject = "USTED ACABA DE REGISTRAR A UN FAMILIAR EXITOSAMENTE";
	    	  Content content = new Content();
	    	  content.setType("text/html");
	    	  content.setValue(buildRegistroFamiliarNotificaTitular(toEmail,emailInvitado[1]));
	    	  Mail mail = new Mail(from, subject, to, content);
		         
		      
		      request.setMethod(Method.POST);
		      request.setEndpoint("mail/send");
		      request.setBody(mail.build());
		      Response response = sg.api(request);
	    
		        
	      }
		     
	      if(emailInvitado[0].equalsIgnoreCase("bajaInvitado")) {
			  
	 		  String subject = "FAST GAS BAJA DE FAMILIAR";
	 		  
	 		  
	 	      Content content = new Content();
	 	      content.setType("text/html");
	 	      content.setValue(buildBajaFamiliarNotificaTitular(emailInvitado[1]));
	 	      Mail mail = new Mail(from, subject, to, content);
	 	        
	 	      request.setMethod(Method.POST);
	 	      request.setEndpoint("mail/send");
	 	      request.setBody(mail.build());
	 	      
	 	      Response response = sg.api(request);
	       }
	      
		      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
	/**
	 * Notifica Registro de familiar
     * @param pedido
	 * @param tipoEmail
	 * @return
	 */
	public String sendEmailCancelar(Pedido pedido,CancelacionEmailDto cancelacion) {
		
	    try {
	    	  String toEmail=pedido.getCliente().getCorreo();
	    	  
	    	  cancelacion = calculoPedido.getDatosCancelacion(pedido,cancelacion);
		    	recoverEnviroment();
			    
				Email from = new Email(correoEnvio,username);
			    
				
				
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS USTED ACABA DE CANCELAR EL SERVICIO: "+pedido.getUuid();
		    	Content content = new Content();
		    	content.setType("text/html");
		    	content.setValue(cancelarServicio(cancelacion,pedido));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	/**
	 * Email de cancelacion por parte de despachador
	 * @param pedido
	 * @param cancelacion
	 * @return
	 */
	public String sendEmailCancelarDespachador(Pedido pedido,CancelacionEmailDto cancelacion) {
		
	    try {
	    	  String toEmail=pedido.getCliente().getCorreo();
	    	  
	    	  cancelacion = calculoPedido.getDatosCancelacion(pedido,cancelacion);
		    	recoverEnviroment();
			    
				Email from = new Email(correoEnvio,username);
			    
				
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS HA CANCELADO SU SERVICIO: "+pedido.getUuid();
		    	Content content = new Content();
		    	content.setType("text/html");
		    	content.setValue(cancelarServicio(cancelacion,pedido));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
	/**
	 * 
	 * @param ticket
	 * @param pedido
	 * @return
	 */
	public String sendEmailTicket(TicketFinalDto ticket, Pedido pedido) {
		
	    try {
	    	  
	    	  String toEmail=pedido.getCliente().getCorreo();
		       recoverEnviroment();
			    
				Email from = new Email(correoEnvio,username);
			    
				
				
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS TICKET DEL SERVICIO: "+pedido.getUuid();
		    	Content content = new Content();
		    	content.setType("text/html");
		    	content.setValue(this.ticketServicio(ticket,pedido));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
	
    /**
     * 
     * @param pedido
     * @param fechaHoraPedido
     * @param titular
     * @param nombreInvitado
     * @return
     * @throws ParseException 
     */
	public String sendEmailTicketPreCompra(Pedido pedido,String fechaHoraPedido,Cliente titular,String nombreInvitado) throws ParseException {
		
	    try {
	    
	    	
	    	  String toEmail="";
	    	  if(titular==null)
	    		  toEmail=pedido.getCliente().getCorreo();
	    	  else
	    		  toEmail=titular.getCorreo();
	    		  
		    	recoverEnviroment();
			    
				Email from = new Email(correoEnvio,username);
			    
				
				
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS TICKET DE COMPRA SERVICIO: "+pedido.getUuid();
		    	Content content = new Content();
		    	content.setType("text/html");
		    	content.setValue(ticketServicioPreCompra(pedido,fechaHoraPedido,titular,nombreInvitado));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
    /**
     * 
     * @param toEmail
     * @param Baja Plan Familiares
     * @param pathAppStore
     * @return
     */
	public String sendEmailBajaPlanFamiliares(String toEmail, List<AppStoreDto> pathAppStore) {
		
	    try {
		    	recoverEnviroment();
				Email from = new Email(correoEnvio,username);
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS BAJA PLAN FAMILIAR ";
		    	
		    	Content content = new Content();
		    	content.setType("text/html");
		    	String operacion = "el acceso al Plan Familiar";
		    	content.setValue(bajaPlanFamiliares(operacion, pathAppStore));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
    /**
     * 
     * @param toEmail
     * @param Baja Plan Familiares al Administrador
     * @return
     */
	public String sendEmailBajaPlanFamiliaresAdmin(String toEmail) {
		
	    try {
		    	recoverEnviroment();
				Email from = new Email(correoEnvio,username);
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS BAJA PLAN FAMILIAR ";
		    	
		    	Content content = new Content();
		    	content.setType("text/html");
		    	String operacion = "el Plan Familiar que tenía con nosotros de manera exitosa.";
		    	content.setValue(bajaPlanFamiliarCuentaAdmin(operacion));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
    /**
     * 
     * @param toEmail
     * @param Baja Cuenta - Email para Familiares
     * @param pathAppStore
     * @return
     */
	public String sendEmailBajaCuentaFamiliares(String toEmail,List<AppStoreDto> pathAppStore) {
		
	    try {
		    	recoverEnviroment();
				Email from = new Email(correoEnvio,username);
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS BAJA PLAN FAMILIAR ";
		    	
		    	Content content = new Content();
		    	content.setType("text/html");
		    	String operacion = "la cuenta";
		    	content.setValue(bajaPlanFamiliares(operacion, pathAppStore));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
    /**
     * 
     * @param toEmail
     * @param Baja Cuenta - Email para Administrador
     * @param plan
     * @return
     */
	public String sendEmailBajaCuentaAdmin(String toEmail, String plan) {
		
	    try {
		    	recoverEnviroment();
				Email from = new Email(correoEnvio,username);
			    Email to = new Email(toEmail);
	
			    SendGrid sg = new SendGrid(password);
			    Request request = new Request();
			    	
		    	String subject = "FAST-GAS BAJA PLAN " + plan.toUpperCase() + " ";
		    	
		    	Content content = new Content();
		    	content.setType("text/html");
		    	
		    	String operacion = "su cuenta exitosamente.";
		    	content.setValue(bajaPlanFamiliarCuentaAdmin(operacion, plan));
		    	Mail mail = new Mail(from, subject, to, content);
			         

			     request.setMethod(Method.POST);
			     request.setEndpoint("mail/send");
			     request.setBody(mail.build());
			     Response response = sg.api(request);
		    
			      
		    } catch (IOException ex) {
		      System.out.println(ex);
		    }

		
		    return error;
			
		}
	
	/**
	 * 
	 * @param email
	 * @param token
	 * @return
	 */
	private String buildBodyEmail(String email,String token) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html xmlns:th=\"http://www.thymeleaf.org\">\n" + 
	    		"\n" + 
	    		"<head>\n" + 
	    		"\n" + 
	    		"<title>Enermex</title>\n" + 
	    		"\n" + 
	    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" + 
	    		"\n" + 
	    		"<style>\n" + 
	    		"\n" + 
	    		"table {\n" + 
	    		"  margin: auto;\n" + 
	    		"\n" + 
	    		"}\n" + 
	    		"\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"</head>\n" + 
	    		"\n" + 
	    		"<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"\n" + 
	    		"	<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
	    		"		<tr>\n" + 
	    		"			<th><img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\"></th>\n" + 
	    		"		</tr>\n" + 
	    		"	\n" + 
	    		"		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	        <th style='color:#F7901D;' >Para Terminar tu registro</th>\n" + 
	    		"	    </tr>\n" + 
	    		"	    <tr>\n" + 
	    		"	        <th>\n" + 
	    		"	          <a  clicktracking=off href='"+this.urlCorreo+"/api/clientes/validaRegistro?correo="+email+"' style=\"background-color: #00A3D7;padding: 10px 5%; cursor: pointer;margin-bottom: 0px;border: none; color: #ffffff;border-radius: 20px;font-size: 80%\">DAR CLICK AQUí</a>  \n" + 
	    		"	        </th>\n" + 
	    		"	    </tr><br><br>\n" + 
	    		"		<tr>\n" + 
	    		"			<th style=\"background-color:#001C33;\">\n" + 
	    		"				<p style=\"width: 80%; margin: auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">nombre@ Targa Fuels S.A. de C.V. 2020 Derechos Reservados</p>\n" + 
	    		"		</tr>\n" + 
	    		"	</table>\n" + 
	    		"\n" + 
	    		"</body>\n" + 
	    		"\n" + 
	    		"</html>");
		 
		
		return body.toString();
	}
	/**
	 * 
	 * @param email
	 * @param token
	 * @return
	 */
	private String registroUsuarioWeb(String app,String token) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html xmlns:th=\"http://www.thymeleaf.org\">\n" + 
	    		"\n" + 
	    		"<head>\n" + 
	    		"\n" + 
	    		"<title>Enermex</title>\n" + 
	    		"\n" + 
	    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" + 
	    		"\n" + 
	    		"<style>\n" + 
	    		"\n" + 
	    		"table {\n" +
	    		"  margin:0 auto;\n" + 
	    		"}\n" + 
	    		"\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"</head>\n" + 
	    		"\n" + 
	    		"<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"\n" + 
	    		"	<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
	    		"		<tr>\n" + 
	    		"			<th><img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\"></th>\n" + 
	    		"		</tr>\n" + 
	    		"	\n" + 
	    		"		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	        <th style='color:#F7901D;' >Para terminar tu registro ingresa al siguiente link</th>\n" + 
	    		"	    </tr>\n" + 
	    		"	    <tr>\n" + 
	    		"	        <th>\n" + 
	    		"	          <a  clicktracking=off href='"+this.urlSitio+"/recovery?token="+token+"&app="+app+"' style=\"background-color: #00A3D7;padding: 10px 5%; cursor: pointer;margin-bottom: 20px;border: none; color: #ffffff;border-radius: 20px;font-size: 100%\">DAR CLICK AQUí</a>  \n" + 
	    		"	        </th>\n" + 
	    		"	    </tr><br><br>\n" + 
	    		"		<tr>\n" + 
	    		"			<th style=\"background-color:#001C33; \">\n" + 
	    		"				<p style=\"width: 80%; margin:auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">nombre@ Targa Fuels S.A. de C.V. 2020 Derechos Reservados</p>\n" + 
	    		"		</tr>\n" + 
	    		"	</table>\n" + 
	    		"\n" + 
	    		"</body>\n" + 
	    		"\n" + 
	    		"</html>");
		 
		
		return body.toString();
	}
	
	/**
	 * 
	 * @param email
	 * @param token
	 * @return
	 */
	private String buildBodyEmailRegistroDespachador(String app,String token) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html xmlns:th=\"http://www.thymeleaf.org\">\n" + 
	    		"\n" + 
	    		"<head>\n" + 
	    		"\n" + 
	    		"<title>Enermex</title>\n" + 
	    		"\n" + 
	    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" + 
	    		"\n" + 
	    		"<style>\n" + 
	    		"\n" + 
	    		"table {\n" + 
	    		"  display: block;\n" + 
	    		"  margin-left: auto;\n" + 
	    		"}\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"</head>\n" + 
	    		"\n" + 
	    		"<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"\n" + 
	    		"	<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
	    		"		<tr>\n" + 
	    		"			<th><img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"600\" height=\"144\" alt=\"\"></th>\n" + 
	    		"		</tr>\n" + 
	    		"	\n" + 
	    		"		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	        <th style='color:#F7901D;' >Para Terminar tu registro</th>\n" + 
	    		"	    </tr>\n" + 
	    		"	    <tr>\n" + 
	    		"	        <th>\n" + 
	    		"	          <a  clicktracking=off href='"+this.urlSitio+"/recovery?token="+token+"&app="+app+"&tipo=invitado' style=\"background-color: #00A3D7;padding: 10px 5%; cursor: pointer;margin-bottom: 20px;border: none; color: #ffffff;border-radius: 20px;font-size: 100%\">DAR CLICK AQUí</a>  \n" + 
	    		"	        </th>\n" + 
	    		"	    </tr><br><br>\n" + 
	    		"		<tr>\n" + 
	    		"			<th style=\"background-color:#001C33;\">\n" + 
	    		"				<p style=\"width: 80%; margin: auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">nombre@ Targa Fuels S.A. de C.V. 2020 Derechos Reservados</p>\n" + 
	    		"		</tr>\n" + 
	    		"	</table>\n" + 
	    		"\n" + 
	    		"</body>\n" + 
	    		"\n" + 
	    		"</html>");
		 
		
		return body.toString();
	}

	
	/**
	 * 
	 * @param email
	 * @param token
	 * @return
	 */
	private String buildBodyEmailRegistroInvitado(String app,String token) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html xmlns:th=\"http://www.thymeleaf.org\">\n" + 
	    		"\n" + 
	    		"<head>\n" + 
	    		"\n" + 
	    		"<title>Enermex</title>\n" + 
	    		"\n" + 
	    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" + 
	    		"\n" + 
	    		"<style>\n" + 
	    		"\n" + 
	    		"table {\n" + 
	    		"  margin:0 auto;\n" + 
	    		"\n" + 
	    		"}\n" + 
	    		"\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"</head>\n" + 
	    		"\n" + 
	    		"<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"\n" + 
	    		"	<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
	    		"		<tr>\n" + 
	    		"			<th><img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\"></th>\n" + 
	    		"		</tr>\n" + 
	    		"	\n" + 
	    		"		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	        <th style='color:#F7901D;' >Para Terminar tu registro</th>\n" + 
	    		"	    </tr>\n" + 
	    		"	    <tr>\n" + 
	    		"	        <th>\n" + 
	    		"	          <a  clicktracking=off href='"+this.urlSitio+"/recovery?token="+token+"&app="+app+"&tipo=invitado' style=\"background-color: #00A3D7;padding: 10px 5%; cursor: pointer;margin-bottom: 20px;border: none; color: #ffffff;border-radius: 20px;font-size: 100%\">DAR CLICK AQUí</a>  \n" + 
	    		"	        </th>\n" + 
	    		"	    </tr><br><br>\n" + 
	    		"		<tr>\n" + 
	    		"			<th style=\"background-color:#001C33;\">\n" + 
	    		"				<p style=\"width: 80%; margin: auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">nombre@ Targa Fuels S.A. de C.V. 2020 Derechos Reservados</p>\n" + 
	    		"		</tr>\n" + 
	    		"	</table>\n" + 
	    		"\n" + 
	    		"</body>\n" + 
	    		"\n" + 
	    		"</html>");
		 
		
		return body.toString();
	}
	
	/**
	 * 
	 * @param email
	 * @param token
	 * @return
	 */
	private String buildRegistroFamiliarNotificaTitular(String email,String invitado) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html xmlns:th=\"http://www.thymeleaf.org\">\n" + 
	    		"\n" + 
	    		"<head>\n" + 
	    		"\n" + 
	    		"<title>Enermex</title>\n" + 
	    		"\n" + 
	    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" + 
	    		"\n" + 
	    		"<style>\n" + 
	    		"\n" + 
	    		"table {\n" + 
	    		"  margin:0 auto;\n" + 
	    		"\n" + 
	    		"}\n" + 
	    		"\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"</head>\n" + 
	    		"\n" + 
	    		"<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"\n" + 
	    		"	<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
	    		"		<tr>\n" + 
	    		"			<th><img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\"></th>\n" + 
	    		"		</tr>\n" + 
	    		"	\n" + 
	    		"		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	        <th style='color:#F7901D;' >Haz agregado a: "+invitado+"!</th>\n" + 
	    		"	    </tr>\n" + 
	    		"		<tr>\n" + 
	    		"			<th style=\"background-color:#001C33;\">\n" + 
	    		"				<p style=\"width: 80%;  margin: auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">nombre@ Targa Fuels S.A. de C.V. 2020 Derechos Reservados</p>\n" + 
	    		"		</tr>\n" + 
	    		"	</table>\n" + 
	    		"\n" + 
	    		"</body>\n" + 
	    		"\n" + 
	    		"</html>");
		 
		
		return body.toString();
	}
	/**
	 * 
	 * @param email
	 * @param token
	 * @return
	 */
	private String buildBodyEmailRecupera(String app,String token) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html xmlns:th=\"http://www.thymeleaf.org\">\n" + 
	    		"\n" + 
	    		"<head>\n" + 
	    		"\n" + 
	    		"<title>Enermex</title>\n" + 
	    		"\n" + 
	    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" + 
	    		"\n" + 
	    		"<style>\n" + 
	    		"\n" + 
	    		"table {\n" + 
	    		"  margin:0 auto;\n" + 
	    		"\n" + 
	    		"}\n" + 
	    		"\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"</head>\n" + 
	    		"\n" + 
	    		"<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"\n" + 
	    		"	<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
	    		"		<tr>\n" + 
	    		"			<th><img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\"></th>\n" + 
	    		"		</tr>\n" + 
	    		"	\n" + 
	    		"		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	        <th style='color:#F7901D;' >Para recuperar tu contraseña</th>\n" + 
	    		"	    </tr>\n" + 
	    		"	    <tr>\n" + 
	    		"	        <th>\n" + 
	    		"	          <a  clicktracking=off href='"+this.urlSitio+"/recovery?token="+token+"&app="+app+"' style=\"background-color: #00A3D7;padding: 10px 5%; cursor: pointer;margin-bottom: 20px;border: none; color: #ffffff;border-radius: 20px;font-size: 100%\">DAR CLICK AQUí</a>  \n" + 
	    		"	        </th>\n" + 
	    		"	    </tr><br><br>\n" + 
	    		"		<tr>\n" + 
	    		"			<th style=\"background-color:#001C33;\">\n" + 
	    		"				<p style=\"width: 80%; margin: auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">nombre@ Targa Fuels S.A. de C.V. 2020 Derechos Reservados</p>\n" + 
	    		"		</tr>\n" + 
	    		"	</table>\n" + 
	    		"\n" + 
	    		"</body>\n" + 
	    		"\n" + 
	    		"</html>");
		 
		
		return body.toString();
	}
	
	/**
	 * Notificacion baja familiar
	 * @param invitado
	 * @return
	 */
	private String buildBajaFamiliarNotificaTitular(String invitado) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html xmlns:th=\"http://www.thymeleaf.org\">\n" + 
	    		"\n" + 
	    		"<head>\n" + 
	    		"\n" + 
	    		"<title>Enermex</title>\n" + 
	    		"\n" + 
	    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" + 
	    		"\n" + 
	    		"<style>\n" + 
	    		"\n" + 
	    		"table {\n" + 
	    		"\n" + 
	    		"  margin:0 auto;\n" + 
	    		"\n" + 
	    		"}\n" + 
	    		"\n" + 
	    		"</style>\n" + 
	    		"\n" + 
	    		"</head>\n" + 
	    		"\n" + 
	    		"<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"\n" + 
	    		"	<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
	    		"		<tr>\n" + 
	    		"			<th><img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\"></th>\n" + 
	    		"		</tr>\n" + 
	    		"	\n" + 
	    		"		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	        <th style='color:#F7901D;' >Se ha dado de baja a: "+invitado+" :(</th>\n" + 
	    		"	    </tr>\n" + 
	    		"		<tr>\n" + 
	    		"			<th style=\"background-color:#001C33; \">\n" + 
	    		"				<p style=\"width: 80%; margin: auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">nombre@ Targa Fuels S.A. de C.V. 2020 Derechos Reservados</p></th> \n" + 
	    		"		</tr>\n" + 
	    		"	</table>\n" + 
	    		"\n" + 
	    		"</body>\n" + 
	    		"\n" + 
	    		"</html>");
		 
		
		return body.toString();
	}
	
	
    /**
     * Ticket de cancelacion
     * @param invitado
     * @return
     */
	private String cancelarServicio(CancelacionEmailDto cancelacion,Pedido p) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		/*CUERPO DEL CORREO*/
	    body.append("<html xmlns:th='http://www.thymeleaf.org'>\n" + 
	    		"	    	<head>\n" + 
	    		"	    	<title>Enermex</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n" + 
	    		"                <style>" + 
	    		"                  .tblCancelacion" + 
	    		"                   {"+
	    		"                     margin:0 auto;\n" + 
	    		"	    		    }\n" + 
	    		"                </style>\n" + 
	    		"                </head>\n" + 
	    		"	    		<body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>\n" + 
	    		"	    		<table id='Tabla_01' width='100%' height='490' border='0' cellpadding='0' cellspacing='0'>"+
	    	    "                   <tr><th><img src='https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download' width='100%' height='144' alt=''></th>\n" + 
	    		"                </tr>\n" + 
	    		"	    		<tr width='80%' height='100'>\n" + 
	    		"	    		   <th style='color:red;' >El servicio:"+cancelacion.getIdServicio()+" ha sido cancelado </th>\n" + 
	    		"               </tr>\n" + 
	    		"                <tr width='80%' height='100'>\n" + 
		    		"                <table cellspacing='10' class=\"tblCancelacion\"   style='margin:0 auto;'>\n" + 
							    		"  <tr colspan='2'>\n" + 
							    		"    <td style='font-weight:bold;text-decoration: underline; margin:0 auto;' >Detalle de servicio </td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr>\n" + 
							    		"    <td style='font-weight:bold'>Fecha de servicio:</td>\n" + 
							    		"    <td>"+cancelacion.getFechaServicio()+"</td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr>\n" + 
							    		"    <td style='font-weight:bold'>Dirección:</td>\n" + 
							    		"    <td>"+cancelacion.getDireccion()+"</td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr>\n" + 
							    		"    <td style='font-weight:bold'>Vehículo:</td>\n" + 
							    		"    <td>"+cancelacion.getAutomovil()+"</td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr>\n" + 
							    		"     <td style='font-weight:bold'>Carga de combustible:</td>\n" + 
							    		"     <td>"+p.getCombustible().getCombustible()+"</td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr>\n" + 
							    		"     <td style='font-weight:bold'>Lavado de auto</td>\n" + 
							    		"     <td>$"+ ((p.isLavadoAuto()) ? (String.format("%.2f", p.getPrecioLavadoAuto())) : "0.00") +"</td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr colspan=\"2\">\n" + 
							    		"    <td style=\"font-weight:bold;text-decoration: underline; margin:0 auto;\" >Detalle de cancelación </td>\n" + 
							    		"  </tr>\n" + 
							    		"   <tr>\n" + 
							    		"    <td style=\"font-weight:bold\">Fecha y hora:</td>\n" + 
							    		"    <td>"+cancelacion.getFechaCancelacion()+"</td>\n" + 
							    		"  </tr>\n" + 
							    		"   <tr>\n" + 
							    		"    <td style=\"font-weight:bold\">Motivo:</td>\n" + 
							    		"    <td>"+cancelacion.getMotivoCancelacion()+"</td>\n" + 
							    		"  </tr>\n" + 
							    		"   <tr>\n" + 
							    		"    <td style=\"font-weight:bold\">Comentario:</td>\n" + 
							    		"    <td>"+cancelacion.getComentarioCancelacion()+"</td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr>\n" + 
							    		"    <td style=\"font-weight:bold\">Monto de devolución:</td>\n" + 
							    		"     <td>$"+ ((cancelacion.isPenalizacion()==true) ? (String.format("%.2f",(p.getTotalPagar()-Double.parseDouble(cancelacion.getMontoPenalizacion())))) : (String.format("%.2f", p.getTotalPagar())))  +"</td>\n" + 
							    		"  </tr>\n" + 
							    		"  <tr>\n" + 
							    		"    <td style=\"font-weight:bold\">Penalización:</td>\n" + 
							    		"        <td>$"+  ((cancelacion.isPenalizacion()==true) ? (String.format("%.2f",Double.parseDouble(cancelacion.getMontoPenalizacion()))) : 0 ) +"</td>\n" + 
							    		"  </tr>\n" + 
							    	"</table>\n" + 
					    		"    </tr>\n" +  
							    		"  <th style=\"background-color:#001C33; \">\n" + 
							    		"	    		<p style=\"width: 80%; text-align:center; margin:0 auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">Fast gas Targa Fuels S.A de C.V 2020 Derechos reservados</p></th> </tr>	</table>\n" 
							    		+ "</th> "
					    		+ "</tr>"
	                    	+ "</table>\n" + 
	    		" </body>\n" + 
	    		"</html>");
		 
	    System.out.println(body.toString());
		
		return body.toString();
	}
	
	  /**
     * Ticket de cancelacion
     * @param invitado
     * @return
     */
	private String ticketServicio(TicketFinalDto ticket,Pedido p) {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		
		
		
		/*CUERPO DEL CORREO*/
		
	    body.append("<html>\n" + 
	    		"	    	<head>\n" + 
	    		"	    	<title>Enermex</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n" + 
	    		"                <style>\n" + 
	    		"                table\n" + 
	    		"                {"
	    		+ "                 margin:0 auto;\n" + 
	    		"	    		}\n" + 
	    		"                </style>\n" + 
	    		"                </head>\n" + 
	    		"	    		<body bgcolor='#FFFFFF' leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"	    		<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"
	    		+ "              <tr>"
	    		+ "                  <th>"
	    		+ "                    <img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\" />"
	    		+ "                  </th>\n" + 
	    		"                </tr>\n" + 
	    		"	    		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	    		<th style='color:red;' >Servicio finalizado servicio: "+p.getUuid()+"</th>\n" + 
	    		"                </tr>\n" + 
	    		"                <tr width=\"80%\" height=\"100\">\n" +  
	    		"                <table cellspacing=\"10\"   style='margin:0 auto;'>\n" + 
						    		"  <tr colspan=\"2\">\n" + 
						    		"    <td style=\"font-weight:bold;text-decoration: underline; margin:0 auto;\" >Detalle de servicio </td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Código de promoción</td>\n" + 
						    		"    <td>"+( (ticket.getCodigoPromo()==null) ? "NA" : ticket.getCodigoPromo())+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Cargo por servicio ("+p.getAlcaldiaMunicipio()+"):</td>\n" + 
						    		"    <td>$"+String.format("%.2f",ticket.getCostoServicio())+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Servicio de gasolina:</td>\n" + 
						    		"    <td>$"+String.format("%.2f",p.getMontoCarga())+"</td>\n" + 
						    		"  </tr>\n" +
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Precio por litro:</td>\n" + 
						    		"     <td>$"+String.format("%.2f", new Double(ticket.getPrecioPorlitro()))+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Litros:</td>\n" + 
						    		"     <td>"+String.format("%.2f", new Double(ticket.getLitros()))+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"   <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Servicio de lavado:</td>\n" + 
						    		"    <td>$"+((p.isLavadoAuto()) ? String.format("%.2f",ticket.getMontoLavado()) : "0.00"  )+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"   <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Servicio de Neumáticos:</td>\n" + 
						    		"    <td >"+((p.isRevisionNeumaticos()) ? ( (p.getPrecioPresionNeumaticos()==null) ? "GRATIS" : ((p.getPrecioPresionNeumaticos().compareTo(new Double("0"))==0 ) ? "GRATIS" : p.getPrecioPresionNeumaticos()  ))  : "$0.00"  )+"</td>\n" +
						    		"  </tr>\n"	+
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Total de importe :</td>\n" + 
						    		"     <td>$"+ String.format("%.2f",ticket.getTotalImporte()) +"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"         <td style=\"font-weight:bold\">Descuento:</td>\n" + 
						    		"        <td>$"+ String.format("%.2f", ticket.getDescuento())+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Total con Descuento:</td>\n" + 
						    		"     <td>$"+ String.format("%.2f", ticket.getTotalConDescuento())+"</td>\n" +
						    		"  </tr>\n" + 
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Subtotal:</td>\n" + 
						    		"     <td>$"+ String.format("%.2f", ticket.getSubTotal())+"</td>\n" +
						    		"  </tr>\n" + 
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Impuesto:</td>\n" + 
						    		"     <td>$"+ String.format("%.2f", ticket.getIvaPagar())+"</td>\n" +
						    		"  </tr>\n" + 
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Total a Pagar:</td>\n" + 
						    		"     <td>$"+ String.format("%.2f", ticket.getTotalPagar())+"</td>\n" +
						    		"  </tr>\n" +
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Método de pago:</td>\n" + 
						    		"     <td>"+p.getTarjeta().getNumeroTarjeta()+"</td>\n" +
						    		"  </tr>\n" +  
						    	"</table>\n" + 
	    		"                </tr>\n" + 
	    		"\n" + 
	    		"                <th style=\"background-color:#001C33; \">\n" + 
	    		"	    		<p style=\"width: 80%; text-align:center; margin:0 auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">Fast gas Targa Fuels S.A de C.V 2020 Derechos reservados</p></th> </tr>	</table>\n" + 
	    		"	    		</body>\n" + 
	    		"	    		</html>");
		 
		System.out.println(body.toString());
		return body.toString();
	}
	
	  /**
     * Ticket de cancelacion
     * @param invitado
     * @return
	 * @throws ParseException 
     */
	private String ticketServicioPreCompra(Pedido p,String fechaHoraPedido,Cliente titular,String nombreInvitado) throws ParseException {
		// TODO: Mejorar la estética del mensaje
		StringBuilder body = new StringBuilder();
		String[] fechaHrInc = fechaHoraPedido.split("-");
		
		StringBuilder sbTit = new StringBuilder();
		
		if(titular!=null) {
			sbTit.append("Estimado cliente su invitado "+nombreInvitado+"," );
		    sbTit.append(" ha solicitado el servicio "+p.getUuid()+", que ha sido cargado a su tarjeta "+p.getTarjeta().getNumeroTarjeta()+", con el siguiente detalle:");	
		}else {
			sbTit.append("Ticket de compra servicio: "+p.getUuid());
		    
		}
		
		
		SimpleDateFormat sdfD = new SimpleDateFormat("dd/MM/yy HH:mm");
		Date fecha  = sdfD.parse(fechaHoraPedido);
		String fechaS = sdfD.format(fecha);
		
		String combustible="";
		String promocion="N/A";
		
		//DETERMINAMOS EL TIPO DE COBUSTIBLE ADQUIRIDO
		if(p.getIdCombustible().compareTo(TipoCombustible.DIESEL.getCombustible())==0)
			combustible ="Diesel";
	    
		if(p.getIdCombustible().compareTo(TipoCombustible.REGULAR.getCombustible())==0)
			combustible ="Regular";
	
	   if(p.getIdCombustible().compareTo(TipoCombustible.PREMIUM.getCombustible())==0)
		   combustible ="Premium";
	   
	   
	   if(p.getIdPromocion()!=null) {
		    Promocion promo =  promocionService.findById(p.getIdPromocion());
		    promocion =promo.getCodigoPromocion();
	   }
	   
//	   System.out.println(fechaHoraPedido);
//	   System.out.println(((p.isLavadoAuto()) ? ( (p.getPrecioLavadoAuto()==null) ? 0 : p.getPrecioLavadoAuto()) : "0.00"  ));
//	   System.out.println(((p.isRevisionNeumaticos()) ? ( (p.getPrecioPresionNeumaticos()==null) ? "GRATIS" : ((p.getPrecioPresionNeumaticos().compareTo(new Double("0"))==0 ) ? "GRATIS" : p.getPrecioPresionNeumaticos()  ))  : "$0.00"  ));
//	   System.out.println(p.getCostoServicio());
//	   System.out.println(p.getMontoCarga());
//	   System.out.println(p.getLitroCarga());
//	   System.out.println(p.getTotalImporte());
//	   System.out.println(p.getDescuento());
//	   System.out.println(p.getTotalConDescuento());
//	   System.out.println(p.getSubTotal());
//	   System.out.println(p.getTarjeta().getNumeroTarjeta());
//	   System.out.println(p.getSubTotal());
//	   System.out.println(p.getTotalImporte());
//	   System.out.println(p.getIva());
//	   System.out.println(p.getTotalPagar());
		
		//+((ticket.getCodigoPromo().isEmpty()) ? "NA" : ticket.getCodigoPromo())+
		/*CUERPO DEL CORREO*/
		
	    body.append("<html>\n" + 
	    		"	    	<head>\n" + 
	    		"	    	<title>Enermex</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n" + 
	    		"                <style>\n" + 
	    		"                table\n" + 
	    		"                {"
	    		+ "                 margin:0 auto;\n" + 
	    		"	    		}\n" + 
	    		"                </style>\n" + 
	    		"                </head>\n" + 
	    		"	    		<body bgcolor='#FFFFFF' leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n" + 
	    		"	    		<table id=\"Tabla_01\" width=\"100%\" height=\"490\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"
	    		+ "              <tr>"
	    		+ "                  <th>"
	    		+ "                    <img src=\"https://drive.google.com/uc?id=1F8Mqxu-w8YsZVFgwz42319eG0LtAP9j5&export=download\" width=\"100%\" height=\"144\" alt=\"\" />"
	    		+ "                  </th>\n" + 
	    		"                </tr>\n" + 
	    		"	    		<tr width=\"80%\" height=\"100\">\n" + 
	    		"	    		<th style='color:red;' >"+sbTit.toString()+"</th>\n" + 
	    		"                </tr>\n" + 
	    		"                <tr width=\"80%\" height=\"100\">\n" +  
	    		"                <table cellspacing=\"10\"   style='margin:0 auto;'>\n" + 
						    		"  <tr colspan=\"2\">\n" + 
						    		"    <td style=\"font-weight:bold;text-decoration: underline; margin:0 auto;\" >Detalle de servicio </td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Fecha y hora: </td>\n" + 
						    		"    <td>"+fechaS+"</td>\n" + 
						    		"  </tr>\n" +
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Código de promoción:</td>\n" + 
						    		"    <td>"+promocion+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Cargo por servicio ("+p.getAlcaldiaMunicipio()+"):</td>\n" + 
						    		"    <td>$"+String.format("%.2f",p.getCostoServicio())+" </td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Servicio de gasolina:</td>\n" + 
						    		"    <td>$"+String.format("%.2f",p.getMontoCarga())+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Litros:</td>\n" + 
						    		"     <td>"+String.format("%.2f", Float.parseFloat(p.getLitroCarga()))+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"   <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Servicio de lavado:</td>\n" + 
						    		"    <td>$"+((p.isLavadoAuto()) ? ( (p.getPrecioLavadoAuto()==null) ? 0 : String.format("%.2f",p.getPrecioLavadoAuto())) : "0.00"  )+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"   <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Servicio de neumáticos:</td>\n" + 
						    		"    <td >"+((p.isRevisionNeumaticos()) ? ( (p.getPrecioPresionNeumaticos()==null) ? "GRATIS" : ((p.getPrecioPresionNeumaticos().compareTo(new Double("0"))==0 ) ? "GRATIS" : ("$"+String.format("%.2f",p.getPrecioPresionNeumaticos()))))  : "$0.00"  )+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"    <td style=\"font-weight:bold\">Total de importe :</td>\n" + 
						    		"     <td>$"+ String.format("%.2f",p.getTotalImporte()) +"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"         <td style=\"font-weight:bold\">Descuento:</td>\n" + 
						    		"        <td>$"+ String.format("%.2f", ((p.getDescuento()==null)  ? 0 : p.getDescuento() ))+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" + 
						    		"         <td style=\"font-weight:bold\">Total con Descuento: </td>\n" + 
						    		"        <td>$"+ String.format("%.2f", ((p.getTotalConDescuento()==null)  ? 0 : p.getTotalConDescuento() ))+"</td>\n" + 
						    		"  </tr>\n" + 
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Subtotal:</td>\n" + 
						    		"     <td>$"+ String.format("%.2f", ((p.getSubTotal()==null)  ? 0 : p.getSubTotal() ))+"</td>\n" +
						    		"  </tr>\n" + 
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Impuesto:</td>\n" + 
						    		"     <td>$"+ String.format("%.2f", ((p.getIva()==null)  ? 0 : p.getIva() ))+"</td>\n" +
						    		"  </tr>\n" + 
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Total a Pagar:</td>\n" + 
						    		"     <td>$"+ String.format("%.2f", ((p.getTotalPagar()==null)  ? 0 : p.getTotalPagar() ))+"</td>\n" +
						    		"  </tr>\n" +
						    		"  <tr>\n" +
						    		"     <td style=\"font-weight:bold\">Método de pago:</td>\n" + 
						    		"     <td>"+p.getTarjeta().getNumeroTarjeta()+"</td>\n" +
						    		"  </tr>\n" + 
						    	"</table>\n" + 
	    		"                </tr>\n" + 
	    		"\n" + 
	    		"                <th style=\"background-color:#001C33; \">\n" + 
	    		"	    		<p style=\"width: 80%; text-align:center; margin:0 auto; background-color:#04345a; margin-top: 10px; color:#ffffff; font-size: 80%;\">Fast gas Targa Fuels S.A de C.V 2020 Derechos reservados</p></th> </tr>	</table>\n" + 
	    		"	    		</body>\n" + 
	    		"	    		</html>");
	    
	    System.out.println(body.toString());
		return body.toString();
	}
	
	  /**
     * Baja Plan Familiar o Cuenta --  Correo a Familiares
     * @param invitado
     * @return
     */
	private String bajaPlanFamiliares(String operacion, List<AppStoreDto> appStore) {
		
		StringBuilder body = new StringBuilder();
		String ios = "";
		String android = "";
		
		for(AppStoreDto app : appStore)
		{
			if(app.getIdAppStore() == 1){
				ios = app.getValor();
			}else {
				android = app.getValor();
			}
		}
		body.append(bajaPlanFamiliarAInvitado(operacion,ios,android));

		return body.toString();
	}
	
	  /**
     * Baja Plan Familiar o Cuenta --- Correo a Administrador
     * @param invitado
     * @return
     */
	private String bajaPlanFamiliarCuentaAdmin(String operacion) {
		
		
		return getHtmlBajaPlanFamiliar("FastGas: Baja plan familiar",
				                       "Estimado cliente usted ha dado de baja: "+operacion,
				                       "Gracias por su confianza.");
  }
  
  // Antonio González; baja de cuenta administrativa para plan
	private String bajaPlanFamiliarCuentaAdmin(String operacion, String plan) {
		
		
		return getHtmlBajaPlanFamiliar("FastGas: Baja plan " + plan,
				                       "Estimado cliente usted ha dado de baja: "+operacion,
				                       "Gracias por su confianza.");
	}
	/**
	 * 
	 * @param operacion
	 * @param ios
	 * @param android
	 * @return
	 */
    private String bajaPlanFamiliarAInvitado(String operacion,String ios, String android) {
		
		
		return getHtmlBajaPlanFamiliarInvitado("FastGas: Baja plan familiar",
				                       "Estimado cliente su Administrador ha eliminado "+operacion+", con todo gusto lo invitamos a Registrarse en nuestra Aplicación, para no perder los servicios que le ofrecemos.",
				                       "Gracias por su confianza.",ios,android);
	}
    /**
     * 
     * @param title
     * @param body
     * @param footer
     * @return
     */
	private String getHtmlBajaPlanFamiliar(String title, String body, String footer) {
		    try {
		    	recoverEnviroment();
		      Resource resource = new ClassPathResource("bajaCuenta.html");
		      File file = resource.getFile();
		      BufferedReader br = new BufferedReader(new FileReader(file));

		      StringBuilder html = new StringBuilder();
		      String line;

		      while ((line = br.readLine()) != null) {
		    	  
		    	  if(line.contains("TITULO_FG"))
		    		  line=line.replace("TITULO_FG", title);
		    	  
		    	  if(line.contains("BODY_FG"))
		    		  line=line.replace("BODY_FG", body);
		    	  
		    	  if(line.contains("PIE_PAGINA_FG"))
		    		  line=line.replace("PIE_PAGINA_FG", footer);
		    	  
		        html.append(line);
		      }
		      br.close();
		      System.out.println(html.toString());
		      return html.toString();
		    } catch (Exception e) {
		      return "";
		    }
		}
	  
	  private String getHtmlBajaPlanFamiliarInvitado(String title, String body, String footer,String ios,String android) {
		    try {
		    	recoverEnviroment();
		      Resource resource = new ClassPathResource("bajaCuentaFamiliar.html");
		      File file = resource.getFile();
		      BufferedReader br = new BufferedReader(new FileReader(file));

		      StringBuilder html = new StringBuilder();
		      String line;

		      while ((line = br.readLine()) != null) {
		    	  
		    	  if(line.contains("TITULO_FG"))
		    		  line=line.replace("TITULO_FG", title);
		    	  
		    	  if(line.contains("BODY_FG"))
		    		  line=line.replace("BODY_FG", body);
		    	  
		      	  if(line.contains("LINK_IOS"))
		    		  line=line.replace("LINK_IOS", ios);
		    	  
		    	  if(line.contains("LINK_ANDROID"))
		    		  line=line.replace("LINK_ANDROID", android);
		    	  
		    	  if(line.contains("PIE_PAGINA_FG"))
		    		  line=line.replace("PIE_PAGINA_FG", footer);
		    	  
		        html.append(line);
		      }
		      br.close();
		      System.out.println(html.toString());
		      return html.toString();
		    } catch (Exception e) {
		      return "";
		    }
		}
	
	private void recoverEnviroment() {
		//cuenta de la c
		this.correoEnvio = env.getProperty("correo.envio");
		this.logger.info("correo de envio: "+correoEnvio);
		this.password  = env.getProperty("sendgrid.password");
		this.logger.info("correo de envio: "+password);
		this.username  = env.getProperty("sendgrid.username");
		this.logger.info("correo de envio: "+username);
		this.urlSitio  = env.getProperty("azureEnviroments.web.prod");
		this.logger.info("correo de envio: "+urlSitio);
		this.urlCorreo = env.getProperty("azureEnviroments.correo.prod.rs");
		this.logger.info("correo de envio: "+urlCorreo);
		
		
	}
	  
	  
	  

}
