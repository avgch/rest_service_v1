package com.enermex.utilerias;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * FirebaseRealtime
 */
@Component
public class FirebaseRealtime {
  //private DatabaseReference rootReference;
  private FirebaseDatabase database;

  @Autowired
  private FirebaseNotification notification;

    /**
     *
     */
    public FirebaseRealtime() {
    try {
      FirebaseOptions options = new FirebaseOptions.Builder()
          .setCredentials(GoogleCredentials.getApplicationDefault())
          .setDatabaseUrl("https://enermex-fastgas.firebaseio.com/")
          .build();
      
      // En desarrollo se ejecuta la instanciación

      FirebaseApp app = null;

      try {
        app = FirebaseApp.getInstance();
      } catch(Exception e) {
        app = FirebaseApp.initializeApp(options);
      }

      // Write a message to the database
      database = FirebaseDatabase.getInstance(app);

      // Activamos nodo a la escucha para realizar el chat
      database.getReference("chat").addValueEventListener(new ValueEventListener(){
      
        @Override
        public void onDataChange(DataSnapshot snapshot) {

          // System.out.println("Snapshot recibido");
          // Nivel de pedidos
          for(DataSnapshot pedido : snapshot.getChildren()) {
            String pedidoKey = pedido.getKey();

            // System.out.println("Pedido: " + pedidoKey );

            // Nivel de texto
            for(DataSnapshot texto :  pedido.getChildren()) {
              Chat chat = texto.getValue(Chat.class);
              String textoKey = texto.getKey();

              // System.out.println("Llave: " + textoKey);
              // System.out.println("Texto: " + chat.getText());
              // System.out.println("Ntf: "   + chat.getNtf());

              if(chat.getNtf() == null || !chat.getNtf().equals("1")) {
                // Se actualiza el nodo de firebase

                database.getReference("chat/" + pedidoKey + "/" + textoKey + "/ntf").setValueAsync("1");

                // Se detecta si es usuario o despachador
                if(chat.getToId() != null) {
                  String[] despachador = chat.getToId().split("driver");
                  String[] cliente = chat.getToId().split("user");

                  if(despachador.length > 1) {
                    notification.notificarDespachador(Long.parseLong(despachador[1]), "Chat activo", chat.getText());
                  } else if (cliente.length > 1) {
                    notification.notificarCliente(new BigInteger(cliente[1]), "Chat activo", chat.getText());
                  }
                }
              }
            }
          }
        }
      
        @Override
        public void onCancelled(DatabaseError error) {
          // TODO Auto-generated method stub
          
        }

        
      });

      // TODO: ELiminar
      // queja(2L); 
    } catch(Exception e) {
      // Logger
      e.printStackTrace();
    }
  }

    /**
     *
     * @param cantidad
     */
    public void queja(Long cantidad) {
    try {
      if(database != null) {
        DatabaseReference rootReference = database.getReference("queja");
        rootReference.setValue(new FirebaseRealtimeStruct(null, cantidad), (DatabaseError err, DatabaseReference ref) -> { });
      }
    } catch (Exception e) {
      
    }
  }
  
    /**
     *
     * @param cantidad
     */
    public void quejad(Long cantidad) {
    try {
      if(database != null) {
        DatabaseReference rootReference = database.getReference("quejad");
        rootReference.setValue(new FirebaseRealtimeStruct(null, cantidad), (DatabaseError err, DatabaseReference ref) -> { });
      }
    } catch (Exception e) {
      
    }
  }
  
  /**
   * Método que debe ejecutarse cada vez que se actualicen pipas y usuarios
   */
  public void seguimiento() {
    try {
      if(database != null) {
        DatabaseReference rootReference = database.getReference("seguimiento");
        rootReference.setValue(new FirebaseRealtimeStruct(null, null), (DatabaseError err, DatabaseReference ref) -> { });
      }
    } catch (Exception e) {
      
    }
  }

    /**
     *
     */
    public static class Chat {
    private String toId;
    private String text;
    private String ntf;

    // Getters

      /**
       *
       * @return
       */
    public String getToId() { return toId; }

      /**
       *
       * @return
       */
      public String getText() { return text; }

      /**
       *
       * @return
       */
      public String getNtf()  { return ntf; }

    // Setters

      /**
       *
       * @param toId
       */
    public void setToId(String toId) { this.toId = toId; }

      /**
       *
       * @param text
       */
      public void setText(String text) { this.text = text; }

      /**
       *
       * @param ntf
       */
      public void setNtf(String ntf)   { this.ntf = ntf; }
  }
}
