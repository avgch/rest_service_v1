package com.enermex.utilerias;


import io.conekta.*;
import io.conekta.Error;
import java.io.Serializable;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.conekta.models.*;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.Pedido;
import com.google.gson.Gson;
import com.enermex.repository.PedidoConektaRepository;
import com.enermex.modelo.PedidoConekta;
import com.enermex.avg.exceptions.*;

/**
 *
 * @author abraham
 */
@Service
@ConfigurationProperties("conekta")
public class ConektaOperation  {
	
	@Autowired(required = true)
	PedidoConektaRepository pedidoConektaRepository;
	
	@Autowired
	CalculoPedido itemsCompraService;
	
	@Autowired
	CalculoPedido calculoPedido;
	
	private String apiKey;
	
    /**
     *
     */
    public ConektaOperation() {
	
	}
	
	/**
	 * 
	 * @param tokenCard
     * @param tokenCustomer
	 * @param idCustomer
     * @return 
	 * @throws ConectaException 
	 */
	public String cardhealthy(String tokenCard, String tokenCustomer) throws ConectaException {
		
		JSONObject completeOrderJSON = new JSONObject("{" +
		        "'currency': 'mxn',"
		        + "'pre_authorize':true," +
		        "'metadata': {" +
		        "    'test': true"+
		        "}," +
		        "'line_items': [{"
		        + "name: 'FAST GAS'," + 
		        "    unit_price: 1500," + 
		        "    quantity: 1"
		        + "}]," +
		        "'customer_info': { " +
		        "   'customer_id' : '"+tokenCustomer+"' "+
		        "}," +
		        "'charges': [{" +
		        "    'payment_method' : {" +
		        "        'type' : 'card'," +
		        "        'payment_source_id' : '"+tokenCard+"'" +
		        "    } " +
		        "}]"+
		        "}");

		Order orden = null;
		
		try {
			orden=Order.create(completeOrderJSON);
		
			
		} catch (ErrorList e) {
			// TODO Auto-generated catch block
			
			for(Error error: e.details) {
				System.out.println(error.message);
				throw new ConectaException(error.message);
			}
					
		} catch (Error error) {
			// TODO Auto-generated catch block
			System.out.println(error.message);
			throw new ConectaException(error.message);
			
		} 
		
		return orden.getId();
	}
	
	
	/**
	 * Author: Abraham Vargas
	 * @param tokenCustomer
	 * @param tokenCard
	 * @param pedido
     * @param idPedido
	 * @return
	 * @throws ConectaException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws JSONException 
	 */
	@Transactional
	public String creaCompra(String tokenCustomer,String tokenCard,Pedido pedido,BigInteger idPedido) throws ConectaException, JSONException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		
	
		PedidoConekta pedidoConkt = new PedidoConekta();
		
		String itemsJSON=itemsCompraService.buildServiciosOrder(pedido);
		
		JSONObject completeOrderJSON = new JSONObject("{" +
		        "'currency': 'mxn',"+
		        "'metadata': {" +
		        "    'test': true"+
		        "}," +
		        "'line_items': "+itemsJSON+"," +
		        "'customer_info': { " +
		        "   'customer_id' : '"+tokenCustomer+"' "+
		        "}," +
		        "'charges': [{" +
		        "    'payment_method' : {" +
		        "        'type' : 'card'," +
		        "        'payment_source_id' : '"+tokenCard+"'" +
		        "    } " +
		        "}],"
		        + "'discount_lines': [{"
				  + "'code': 'Codigo de promocion',"
				  + "'amount': "+  calculoPedido.formatMonto(pedido.getDescuento())+","
				  + "'type': 'coupon'"
		        + "}]"+
		        "}");

		Order orden = null;
		
		try {
			orden = Order.create(completeOrderJSON);
			
			orden.charges.get(0);
			
			pedidoConkt.setIdPedido(idPedido);
			pedidoConkt.setIdOrden(orden.getId());
			pedidoConektaRepository.deleteById(idPedido);
			pedidoConektaRepository.save(pedidoConkt);
			
		  // System.out.println(orden.charges.get);
		} catch (ErrorList e) {
			// TODO Auto-generated catch block
			
			for(Error error: e.details) {
				System.out.println(error.message);
				throw new ConectaException(error.message);
			}
					
		} catch (Error error) {
			// TODO Auto-generated catch block
			System.out.println(error.message);
			throw new ConectaException(error.message);
			
		} 
		
	   return orden.getId();	
	}
	/**
	 * Author: Abraham Vargas
	 * @param tokenCustomer
	 * @param tokenCard
	 * @param pedido
	 * @return
	 * @throws ConectaException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws JSONException 
	 */
	public String creaPreCompra(String tokenCustomer,String tokenCard,Pedido pedido) throws ConectaException, JSONException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		
	
		PedidoConekta pedidoConkt = new PedidoConekta();
		
		//CalculoPedido itemsCompra = new CalculoPedido();
		String itemsJSON=itemsCompraService.buildServiciosOrder(pedido);
		
		JSONObject completeOrderJSON = new JSONObject("{" +
		        "'currency': 'mxn',"
		        + "'pre_authorize':true," +
		        "'line_items': "+itemsJSON+"," +
		        "'customer_info': { " +
		        "   'customer_id' : '"+tokenCustomer+"' "+
		        "}," +
		        "'charges': [{" +
		        "    'payment_method' : {" +
		        "        'type' : 'card'," +
		        "        'payment_source_id' : '"+tokenCard+"'" +
		        "    } " +
		        "}]"+
		        "}");

		Order orden = null;
		
		try {
			orden = Order.create(completeOrderJSON);
			
//			if(pedido.getIdPromocion()!=null) {
//			//Validamos si aplica descuento
//			orden.createDiscountLine(new JSONObject("{"
//					  + "'code': 'Codigo de promocion',"
//					  + "'amount': "+  calculoPedido.formatMonto(pedido.getDescuento())+","
//					  + "'type': 'coupon'"
//					+ "}"));
//			
//			}
			//Agregamos el iva a pagar el iva se encuentra incluido en los precios
//			orden.createTaxLine(new JSONObject("{"
//					  + "'description': 'IVA',"
//					  + "'amount': "+calculoPedido.formatMonto(pedido.getIva())+""
//					+ "}"));
			
			//Guardamos el order id en nuestra bd
			
			pedidoConkt.setIdPedido(pedido.getIdPedido());
			pedidoConkt.setIdOrden(orden.getId());
			pedidoConektaRepository.save(pedidoConkt);
			
		  // System.out.println(orden.charges.get);
		} catch (ErrorList e) {
			// TODO Auto-generated catch block
			
			for(Error error: e.details) {
				System.out.println(error.message);
				throw new ConectaException(error.message);
			}
					
		} catch (Error error) {
			// TODO Auto-generated catch block
			System.out.println(error.message);
			throw new ConectaException(error.message);
			
		} 
		
	   return orden.getId();	
	}
	
	/**
	 * Cierra una orden pre-autorizada
	 * @param idOrdenConekta
	 * @return
	 * @throws ConectaException 
	 */
	public boolean cierraOrden(String idOrdenConekta) throws ConectaException {
		
		Order orden;
		try {
			orden = Order.find(idOrdenConekta);
			orden.capture();
			
			
		} catch (ErrorList e) {
			
			for(Error error: e.details) {
				System.out.println(error.message);
				throw new ConectaException(error.message);
			}
				
		} catch (Error e) {
			throw new ConectaException(e.message);
		} 
		
		return true;
	} 
	
	/**
	 * Cierra una orden pre-autorizada
	 * @param idOrdenConekta
	 * @return
	 * @throws Exception 
	 */
	public boolean devolucionOrden(String idOrdenConekta) throws Exception {
		
		Order orden;
		try {
			orden = Order.find(idOrdenConekta);
			JSONObject validRefund = new JSONObject("{"
			  + "'amount': 8200,"
			  + "'reason': 'requested_by_client'"
			+ "}");

			orden.refund(validRefund);

			
			
		} catch (ErrorList erros) {
			// TODO Auto-generated catch block
			for(Error e :erros.details) {
				System.out.println(e.message);
				throw new ConectaException(e.message);
				
			}
			
		} catch (Error e) {
			// TODO Auto-generated catch block
			System.out.println(e.message);
			throw new ConectaException(e.message);
		} 
		return true;
	} 

	/**
	 * Cierra una orden pre-autorizada
	 * @param idOrdenConekta
     * @param totalOrden
     * @param penalizacion
	 * @return
	 * @throws Exception 
	 */
	public boolean devolucionOrdenPenazila(String idOrdenConekta,double totalOrden,double penalizacion) throws Exception {
		
		Order orden;
		
		try {
			orden = Order.find(idOrdenConekta);
			
			//tomamos el monto total de la orden - monto penalizacion
			double extra = totalOrden*.15;
			double rembolso = (totalOrden+extra)-penalizacion;
			String rembolsoFormat=String.format("%.2f", rembolso).replace(".", "");	
			
			
			JSONObject validRefund = new JSONObject("{"
			  + "'amount':"+rembolsoFormat+","
			  + "'reason': 'requested_by_client'"
			+ "}");

			orden.refund(validRefund);
			
			
			
		} catch (ErrorList erros) {
			// TODO Auto-generated catch block
			for(Error e :erros.details) {
				System.out.println(e.message);
				throw new ConectaException(e.message);
				
			}
			
		} catch (Error e) {
			// TODO Auto-generated catch block
			System.out.println(e.message);
			throw new ConectaException(e.message);
		} 
		return true;
	} 
	
	
	/**
	 * Author: Ing. Abraham Vargas Garcia
	 * Id cliente y num tarjeta tokenizados
	 * @param idCustumner
	 * @param token
	 * @return
	 * @throws ConectaException 
	 */
	public PaymentSource creaTarjeta(String idCustumner,String token) throws ConectaException {
		
		Customer customer =null;
		PaymentSource paymentSource=null;
		try {
			 //id conekta
			 customer = Customer.find(idCustumner);
			
			JSONObject paymentSourceJSON = new JSONObject("{"
					  + "'token_id': '"+token+"',"
					  + "'type': 'card'"
					+ "}");
			paymentSource = customer.createPaymentSource(paymentSourceJSON);
//			
//			System.out.println(Customer.find(idCustumner));
//			System.out.println(paymentSource);
//			System.out.println(paymentSource.getVal("last4"));
//			System.out.println(paymentSource.getVal("id"));
		} catch (Error error) {
			// TODO Auto-generated catch block
			throw new ConectaException(error.message);
		} catch (ErrorList errorList) {
			// TODO Auto-generated catch block
			
			for(Error error : errorList.details) {
				
				throw new ConectaException(error.message);
			}
			
			
		} catch (JSONException error) {
			error.printStackTrace();
			throw new ConectaException("No se pudo agregar la tarjeta correctamente");
			
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConectaException("No se pudo agregar la tarjeta correctamente");
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConectaException("No se pudo agregar la tarjeta correctamente");
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConectaException("No se pudo agregar la tarjeta correctamente");
		}
		
		
		
		return paymentSource;
		
	}
	/**
	 * Author: Ing. Abraham Vargas Garcia
	 * Crea cliente conekta
	 * @param clienteP
	 * @return
	 * @throws ConectaException 
	 */
	public String createCustomerConekta(Cliente clienteP) throws ConectaException {
			
        
		JSONObject customerJSON = new JSONObject("{"
			    + "'name': '"+clienteP.getNombre()+"', "
			    + "'email': '"+clienteP.getCorreo()+"'"
			     + "}");

			Customer customer=null;
			try {
				customer = Customer.create(customerJSON);
			} catch (Error e) {
				// TODO Auto-generated catch block
				throw new ConectaException("No se pudo agregar la tarjeta correctamente");
			} catch (ErrorList e) {
				// TODO Auto-generated catch block
				throw new ConectaException("No se pudo agregar la tarjeta correctamente");
			}
			System.out.println("token:"+customer.getId());
		
			clienteP.setTokenConekta(customer.getId());
			 
		
		return clienteP.getTokenConekta();
	}
	
	/**
	 * Author: Ing. Abraham Vargas Garcia
	 * Buscamos Customer
	 * @param idTokenCustomer
	 * @return
	 * @throws Error
	 * @throws ErrorList
	 */
	public Customer buscamosCliente(String idTokenCustomer) throws Error, ErrorList {
		
		Customer customer = Customer.find(idTokenCustomer);
		  
		return customer;
	}
	
	
	/**
	 * Author: Ing. Abraham Vargas Garcia
	 * @param tarjetaToken
	 * @param idConektaCustumer
	 * @throws ConectaException 
	 */
	public void borraTarjeta(String tarjetaToken,String idConektaCustumer) throws ConectaException {
		
		
		Customer customer;
		try {
			customer = Customer.find(idConektaCustumer);
			
			//buscamos el idTokenTarjeta del usuario
			
			for(Object ps : customer.payment_sources) {
				Card tarj = (Card)ps;
				//eliminamos la tarjeta
				if(tarj.getId().equalsIgnoreCase(tarjetaToken))
					tarj.delete();
				
			}
			
		} catch (Error e) {
			// TODO Auto-generated catch block
			throw new ConectaException("No se pudo eliminar la tarjeta correctamente");
		} catch (ErrorList e) {
			// TODO Auto-generated catch block
			throw new ConectaException("No se pudo eliminar la tarjeta correctamente");
		}
		 
		
	}
	/**
	 * Author: Ing. Abraham Vargas Garcia
     * @param tarjetaToken
     * @param titular
     * @param mesExp
     * @param idConektaCustumer
     * @param anioExp
	 * @throws ConectaException 
	 */
	public void updateCard(String tarjetaToken,String idConektaCustumer,String mesExp,String anioExp,String titular) throws ConectaException {
		
       CardCnk card = new CardCnk(null,null,mesExp,anioExp,titular);
		Customer customer;
		try {
			customer = Customer.find(idConektaCustumer);
			
			//buscamos el idTokenTarjeta del usuario
			
			Gson gson = new Gson();
			String cardJson=gson.toJson(card);
			for(Object ps : customer.payment_sources) {
				Card tarj = (Card)ps;
				JSONObject paymentSourceUpdateJSON = new JSONObject(cardJson);
			    
				//actualizamos card
				if(tarj.getId().equalsIgnoreCase(tarjetaToken))
					tarj.update(paymentSourceUpdateJSON);
				
			}

		} catch (Error e) {
			// TODO Auto-generated catch block
			throw new ConectaException("No se pudo actualizar la tarjeta correctamente");
		} catch (ErrorList e) {
			// TODO Auto-generated catch block
			throw new ConectaException("No se pudo actualizar la tarjeta correctamente");
		}
		
	}
	/**
	 * 
     * @param orden
	 * @param customer
	 * @return
	 * @throws ConectaException 
	 */
	public String updateOrden(Order orden) throws ConectaException {
		
		ConektaList cargos = orden.line_items;
		
		
		for(Object o  :cargos) {
			
			LineItems lineItem = (LineItems) o;
			try {
				if(cargos.size()>1)
				lineItem.delete();
				else {
					
					JSONObject data = new JSONObject("{'amount':1400}");
					orden.update(data);
					orden.capture();
				}
					
			} catch (Error e) {
				// TODO Auto-generated catch block
				throw new ConectaException(e.message);	
			} catch (ErrorList e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				for(Error err : e.details) {
					throw new ConectaException(err.message);					
				}
			}
			
		}
		
		//cargos.
		System.out.println("sakmdkms");
		//Charge c =orden.charges;
		
		return "";
	
	}
	
	/**
	 * 
     * @param idConketaOrder
	 * @param customer
	 * @return
	 * @throws ConectaException 
	 */
	public Order findOrden(String idConketaOrder) throws ConectaException {
		
		Order orden=null;
		
			try {
				orden = Order.find(idConketaOrder);
				
			} catch (Error e) {
				// TODO Auto-generated catch block
				throw new ConectaException(e.message);	
			} catch (ErrorList e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				for(Error err : e.details) {
					throw new ConectaException(err.message);					
				}

			}
		
		
		return orden;
	
	}
	
	/**
	 * 
     * @param idConketaOrder
     * @param totalPagarInc
	 * @param customer
     * @param totalPagarFin
	 * @return
	 * @throws ConectaException 
	 */
	public boolean ajustaOrdenFinal(String idConketaOrder,double totalPagarInc,double totalPagarFin) throws ConectaException {
		
	Order orden;
		
		try {
			orden = Order.find(idConketaOrder);
			//cerramos la orden 
			
			//aplicamos rembolso
			//tomamos el monto total de la orden - monto penalizacion
			double extra =totalPagarInc*.15;
			
			double rembolso = (totalPagarInc+extra)-totalPagarFin;
			String rembolsoFormat=String.format("%.2f", rembolso).replace(".", "");	
			
			
			JSONObject validRefund = new JSONObject("{"
			  + "'amount':"+rembolsoFormat+","
			  + "'reason': 'requested_by_client'"
			+ "}");


			orden.refund(validRefund);
			
		} catch (ErrorList erros) {
			// TODO Auto-generated catch block
			for(Error e :erros.details) {
				System.out.println(e.message);
				throw new ConectaException(e.message);
				
			}
			
		} catch (Error e) {
			// TODO Auto-generated catch block
			System.out.println(e.message);
			throw new ConectaException(e.message);
		} 
		catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	    }
		return true;
	
	}
	
	/**
	 * 
	 * @param idConketaOrder
	 * @throws ConectaException
	 */
	public void cancelarOrdenPreAutorizada(String idConketaOrder) throws ConectaException  {
		
		  Requestor requestor = new Requestor();
		  
              String url = "https://api.conekta.io/orders/" + idConketaOrder + "/void";

              JSONObject jsonObject;
			try {
				jsonObject = (JSONObject) requestor.request("POST", url, null);
			} catch (Error e) {
				// TODO Auto-generated catch block
				throw new ConectaException(e.message);	
			} catch (ErrorList e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				for(Error err : e.details) {
					throw new ConectaException(err.message);					
				}

			}
          
	}
	
    /**
     *
     */
    public void setApiKey() {
		
		Conekta.setApiKey(apiKey);
		
	}
	
    /**
     *
     * @return
     */
    public String getApiKey() {
		return apiKey;
	}

    /**
     *
     * @param apiKey
     */
    public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	
	
}
