package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.enumerable.ClienteFiscalEstatus;
import com.enermex.modelo.ClienteFiscalEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * ClienteFiscalRepository
 */
public interface ClienteFiscalRepository extends CrudRepository<ClienteFiscalEntity, BigInteger>  {

    /**
     *
     * @param idCliente
     * idCliente
     * @param estatus
     * estatus
     * @return List
     */
    List<ClienteFiscalEntity> findByIdClienteAndEstatus(BigInteger idCliente, ClienteFiscalEstatus estatus);

    /**
     *
     * @param idFiscal
     * idFiscal
     * @return Objeto
     */
    ClienteFiscalEntity findByIdFiscal(BigInteger idFiscal);
}