package com.enermex.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.enermex.modelo.PrecioGasolina;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface PrecioGasolinaRepository  extends JpaRepository<PrecioGasolina, Long> {
	
	 

}
