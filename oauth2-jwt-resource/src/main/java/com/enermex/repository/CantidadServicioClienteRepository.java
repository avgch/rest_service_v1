package com.enermex.repository;

import java.math.BigInteger;

import com.enermex.view.CantidadServicioClienteView;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface CantidadServicioClienteRepository extends CrudRepository<CantidadServicioClienteView, BigInteger> {

}
