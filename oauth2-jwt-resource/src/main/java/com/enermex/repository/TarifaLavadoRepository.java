package com.enermex.repository;

import com.enermex.modelo.TarifaLavadoEntity;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * TarifaLavadoRepository
 */
@Repository
@Transactional
public interface TarifaLavadoRepository extends CrudRepository<TarifaLavadoEntity, String> {
	
    /**
     *
     * @param cp
     * @return
     */
    @Query("Select tl.precio  from TarifaLavadoEntity tl where tl.idCodigo =?1 ")
	public String getTarifaLavadoByCp(String cp);

}