package com.enermex.repository;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CatMarca;
import com.enermex.modelo.Promocion;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface  PromocionRepository extends JpaRepository<Promocion, Long> {
		
	//@Query("Select p  from Promocion p where p.estatus = true and p.fechaHoraVigencia>=?1 order by p.fechaCreacion desc ")

    /**
     *
     * @return
     */
	@Query("Select p  from Promocion p  order by p.fechaCreacion desc ")
	List<Promocion> getAll();
	
	//@Query("Select p  from Promocion p where p.estatus = true and p.fechaHoraVigencia>=?1 order by p.fechaCreacion desc ")

    /**
     *
     * @param currentDate
     * @return
     */
	@Query("Select p  from Promocion p  where p.estatus = true and p.fechaHoraVigencia>=?1 and p.paraTodos=true   order by p.fechaCreacion desc ")
	List<Promocion> getAllActivasForAllCustomers(Calendar currentDate);
	
    /**
     *
     * @param idPromocion
     * @return
     */
    @Query("Select p.codigoPromocion  from Promocion p where p.idPromocion=?1")
	String getCodPromoById(Long idPromocion);
	
    /**
     *
     * @param imagePromo
     * @param idPromocion
     */
    @Modifying
	@Query("update Promocion p set p.imagenPromo=?1 where p.idPromocion=?2")
	void updateImagePromo(byte[] imagePromo, Long idPromocion);
	
    /**
     *
     * @param codigoPromo
     * @param fecha
     * @return
     */
    @Query(value =  "Select id_promocion  from t_promocion where codigo_promocion like :codigoPromo and :fecha >= DATE_FORMAT(fecha_hora_inicio,'%d-%m-%Y %H:%i') ", nativeQuery = true)
	List<Long>  existeCodigoPromo(String codigoPromo,String fecha);
	
	//Find all ids from promotions for customers frecuents and  some customers

    /**
     *
     * @param idCliente
     * @return
     */
	@Query(value =  "Select id_promocion  from t_promocion_cliente where id_cliente= :idCliente  group by (id_promocion) ", nativeQuery = true)
	Long[]  promosClientesFreqSegmetado(BigInteger idCliente);
	
	//With ids find all active promotions

    /**
     *
     * @param currentDate
     * @param idsPromo
     * @return
     */
	@Query("Select p  from Promocion p  where p.estatus = true and  p.fechaHoraVigencia>= :currentDate and p.idPromocion in :idsPromo   order by p.fechaCreacion desc ")
	List<Promocion> getAllActiveByIds(Calendar currentDate, Long[] idsPromo);
	
    /**
     *
     * @param titulo
     * @param codigoPromocion
     * @param descuento
     * @param monto
     * @param detalle
     * @param estatus
     * @param esFrecuente
     * @param paraTodos
     * @param paraAlgunos
     * @param fechaHoraInicio
     * @param fechaHoraVigencia
     * @param idEstado
     * @param idAlcaldia
     * @param idPromocion
     */
    @Modifying
	@Query("update Promocion p set p.titulo=?1,p.codigoPromocion=?2,p.descuento=?3,p.monto=?4,p.detalle=?5,p.estatus=?6,"
			+ "p.esFrecuente=?7,p.paraTodos=?8,p.paraAlgunos=?9,p.fechaHoraInicio=?10,p.fechaHoraVigencia=?11,p.idEstado=?12,"
			+ " p.idAlcaldia=?13  where p.idPromocion=?14")
	void update(String titulo,String codigoPromocion,String descuento,Double monto,String detalle,Boolean estatus,Boolean esFrecuente,
			Boolean paraTodos,Boolean paraAlgunos,Calendar fechaHoraInicio,Calendar fechaHoraVigencia,Integer idEstado,Integer idAlcaldia,
			Long idPromocion);
	
    /**
     *
     * @param idPromocion
     * @return
     */
    @Query("Select p  from Promocion p where p.idPromocion=?1")
	Promocion getById(Long idPromocion);
	
}
