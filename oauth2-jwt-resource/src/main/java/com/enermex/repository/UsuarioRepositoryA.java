package com.enermex.repository;


import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.enermex.modelo.Usuario;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface UsuarioRepositoryA  extends JpaRepository<Usuario, Long>{
	
	
	
	List<Usuario> findAll();
	
    /**
     *
     * @param idUsuario
     * @return
     */
    Usuario findByIdUsuario(Integer idUsuario);
	
    /**
     *
     * @param nombre
     * @param apellidoPaterno
     * @param apellidoMaterno
     * @param estadoCivil
     * @param correo
     * @param telefono
     * @param usuarioId
     */
    @Modifying
	@Query("update Usuario u set u.nombre=?1,u.apellidoPaterno=?2, u.apellidoMaterno=?3, u.estadoCivil=?4, u.correo = ?5, u.telefono = ?6  where u.idUsuario = ?7")
	void updateUsuario(String nombre,String apellidoPaterno,String apellidoMaterno,String estadoCivil, String correo, String telefono, Integer usuarioId);
	
    /**
     *
     * @param usuarioId
     */
    @Modifying
	@Query("update Usuario u set u.estatus=2 where u.idUsuario = ?1")
	void delete(Integer usuarioId);
	
    /**
     *
     * @param isId
     * @param id
     */
    @Modifying
	@Query("update Usuario u set u.isId=?1  where u.idUsuario = ?2")
	void updateIsid(Integer isId,Integer id);
	
    /**
     *
     * @param email
     * @return
     */
    @Query("Select u from Usuario u where u.correo = ?1")
	Usuario findByEmail(String email);
	
    /**
     *
     * @param contrasena
     * @param id
     * @param correo
     */
    @Modifying
	@Query("update Usuario u set u.contrasena=?1  where u.idUsuario = ?2 and u.correo=?3")
	void updatePassword(String contrasena,Integer id,String correo);

}
