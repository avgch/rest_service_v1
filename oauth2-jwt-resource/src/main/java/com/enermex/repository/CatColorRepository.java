package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.CatColorEntity;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface CatColorRepository extends CrudRepository<CatColorEntity, Integer> {

    /**
     *
     * @return List
     */
    List<CatColorEntity> findByOrderByOrdenAsc();
}