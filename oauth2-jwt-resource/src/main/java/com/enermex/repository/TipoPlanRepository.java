/**
 * 
 */
package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.dto.TipoPlanDto;
import com.enermex.modelo.TipoPlan;

/**
 * @author wkn-40
 *
 */

@Repository
@Transactional(rollbackFor=Exception.class)
public interface TipoPlanRepository extends  JpaRepository<TipoPlan, Integer> {

}
