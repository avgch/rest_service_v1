package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.TarifaPresionHistorialEntity;
import com.enermex.modelo.TarifaPresionHistorialId;

import org.springframework.data.repository.CrudRepository;

/**
 * TarifaPresionHistorialRepository
 */
public interface TarifaPresionHistorialRepository extends CrudRepository<TarifaPresionHistorialEntity, TarifaPresionHistorialId> {

    /**
     *
     * @return
     */
    List<TarifaPresionHistorialEntity> findAllByOrderByFechaCreacionAsc();
}
