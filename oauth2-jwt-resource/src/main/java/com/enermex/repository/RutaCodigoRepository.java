package com.enermex.repository;

import org.springframework.data.repository.CrudRepository;

import com.enermex.modelo.RutaCodigoEntity;

/**
 *
 * @author abraham
 */
public interface RutaCodigoRepository extends CrudRepository<RutaCodigoEntity, Integer> {

}
