package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.DireccionCliente;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface AutomovilClienteRepository  extends JpaRepository<AutomovilCliente, BigInteger> {
	
	
	
	@Modifying
	@Query("select ac from AutomovilCliente ac  where ac.estatus = 1")
	List<AutomovilCliente> findAll();

    /**
     *
     * @param idCliente
     * idCliente
     * @return AutomovilCliente
     */
    @Modifying
	@Query("select ac from AutomovilCliente ac  where ac.estatus = 1 and ac.cliente.idCliente=?1")
	List<AutomovilCliente> findCarByIdCliente( BigInteger idCliente);
	
    /**
     *
     * @param id
     * id
     */
    @Modifying
	@Query("update AutomovilCliente a set a.estatus=0 where a.idAutomovil = ?1")
	public void delete(Integer id);
	
    /**
     *
     * @param color
     * color
     * @param placas
     * placas
     * @param anio
     * anio
     * @param capacidad
     * capacidad
     * @param tamanioTanque
     * tamanioTanque
     * @param idCombustible
     * idCombustible
     * @param version
     * version
     * @param serie
     * serie
     * @param idAutomovil
     * idAutomovil
     */
    @Modifying
	@Query("update AutomovilCliente ac set ac.color=?1, ac.placas = ?2, ac.anio=?3, ac.capacidad = ?4,ac.tamanioTanque=?5, ac.idCombustible=?6, "
			+ "  ac.version=?7, ac.serie=?8 where ac.idAutomovil = ?9")
	void update(String color, String placas,String anio, String capacidad,String tamanioTanque, Integer idCombustible,String version,String serie, Integer idAutomovil);
	
    /**
     *
     * @param idAutomovil
     * idAutomovil
     * @return AutomovilCliente
     */
    @Query("select ac from AutomovilCliente ac where ac.estatus = 1 and ac.idAutomovil = ?1")
	AutomovilCliente buscaAutoClienteById(Integer idAutomovil);
  
  // José Antonio González
  // Búsqueda de automóviles del cliente

    /**
     *
     * @param idCliente
     * idCliente
     * @return List
     */
  @Query(value = "SELECT * FROM t_automovil_cliente WHERE id_cliente = ?1", nativeQuery = true)
  List<AutomovilCliente> findByIdCliente(BigInteger idCliente);

  // Búsqueda de automóviles por número de placa

    /**
     *
     * @param placas
     * placas
     * @return List
     */
  List<AutomovilCliente> findTop5ByPlacasStartingWith(String placas);

  // Conteo de los automóviles del cliente

    /**
     *
     * @param idCliente
     * idCliente
     * @return Long
     */
  @Query(value = "SELECT COUNT(id_automovil), id_cliente FROM t_automovil_cliente WHERE id_cliente = ?1 GROUP BY id_cliente", nativeQuery = true)
  Long countByIdCliente(BigInteger idCliente);
}
