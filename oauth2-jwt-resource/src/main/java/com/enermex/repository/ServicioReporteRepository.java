package com.enermex.repository;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import com.enermex.view.ServicioReporteView;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface ServicioReporteRepository extends CrudRepository<ServicioReporteView, BigInteger> {
  // Consulta de filtrado más importante
  // Búsqueda por fechas

    /**
     *
     * @param inicio
     * @param fin
     * @return
     */
  List<ServicioReporteView> findByFechaPedidoGreaterThanEqualAndFechaPedidoLessThanEqual(Calendar inicio, Calendar fin);

}