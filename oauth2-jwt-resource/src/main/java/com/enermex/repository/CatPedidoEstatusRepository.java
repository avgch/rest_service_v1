package com.enermex.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CatEstatusPedido;
import com.enermex.modelo.ClienteDireccionEntity;


/**
 * Repo estatus pedido
 * @author wkn-40
 *
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CatPedidoEstatusRepository extends CrudRepository<CatEstatusPedido, Integer> {

}
