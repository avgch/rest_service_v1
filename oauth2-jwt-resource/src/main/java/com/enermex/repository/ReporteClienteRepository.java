package com.enermex.repository;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.enermex.dto.reporte.ReporteClienteFiltroV2;
import com.enermex.dto.reporte.ReporteClienteLineaV2;

import org.springframework.stereotype.Repository;

/**
 *
 * @author abraham
 */
@Repository
public class ReporteClienteRepository {
  @PersistenceContext
  private EntityManager em;

    /**
     *
     * @param filtro
     * @return
     */
    public List<ReporteClienteLineaV2> findAll(ReporteClienteFiltroV2 filtro) {
    try {
      String  inicio       = filtro.getInicio();
      String  fin          = filtro.getFin();
      Long    idCliente    = filtro.getIdCliente();
      Integer idEstado     = filtro.getIdEstado();
      Integer idMunicipio  = filtro.getIdMunicipio();
      String  cp           = filtro.getCp();
      Integer plan         = filtro.getPlan();
      Long    idAutomovil  = filtro.getIdAutomovil();
      Boolean hasLavado    = filtro.getHasLavado();
      Boolean hasPresion   = filtro.getHasPresion();
      Integer idTipoPedido = filtro.getIdTipoPedido();
      Integer estatus      = filtro.getEstatus();

      String consulta = 
        "SELECT " +
        "    c.id_cliente, " +
        "    c.id_titular, " +
        "    c.nombre, " +
        "    c.apellido_paterno, " +
        "    c.apellido_materno, " +
        "    c.id_estatus, " +
        "    c.bloqueo, " +
        "    c.acceso, " +
        "    c.fecha_creacion, " +
        "    c.fecha_baja, " +
        "    a.id_automovil, " +
        "    a.marca, " +
        "    a.modelo, " +
        "    a.anio, " +
        "    a.color, " +
        "    a.placas, " +
        "    count(p.id_pedido) as cantidad " +
        "  FROM t_pedido AS p " +
        "    INNER JOIN t_automovil_cliente AS a ON a.id_automovil = p.id_automovil " +
        "    INNER JOIN t_cliente           AS c ON c.id_cliente   = a.id_cliente " +
        "    LEFT  JOIN t_direccion_cliente AS d ON d.id_direccion = p.id_direccion " +
        "    LEFT  JOIN t_cat_codigo        AS g ON g.id_codigo    = d.cp " +
        "  WHERE p.fecha_pedido >= ? AND p.fecha_pedido <= ? ";

      // Filtros
      consulta += idCliente    != null ? "AND p.id_cliente = ? "          : "";
      consulta += idAutomovil  != null ? "AND p.id_automovil = ? "        : "";
      consulta += cp           != null ? "AND d.cp = ? "                  : "";
      consulta += idEstado     != null ? "AND g.id_estado = ? "           : "";
      consulta += idMunicipio  != null ? "AND g.id_municipio = ? "        : "";
      consulta += estatus      != null ? "AND p.estatus_pedido = ? "      : "";
      consulta += hasLavado    != null ? "AND p.lavado_auto = ? "         : "";
      consulta += hasPresion   != null ? "AND p.revision_neumaticos = ? " : "";
      consulta += idTipoPedido != null ? "AND p.id_tipo_pedido = ? "      : "";

      consulta += "GROUP BY (p.id_automovil);";

      Query query = em.createNativeQuery(consulta);

      // Posiciones 1 y  2 para las fechas
      int parm = 2;

      query.setParameter(1, inicio);
      query.setParameter(2, fin);

      if(idCliente != null) {
        parm++;
        query.setParameter(parm, idCliente);
      }

      if(idAutomovil != null) {
        parm++;
        query.setParameter(parm, idAutomovil);
      }

      // Filtrar por código postal
      if(cp != null) {
        parm++;
        query.setParameter(parm, cp);
      }

      // Filtrar por estado
      if(idEstado != null) {
        parm++;
        query.setParameter(parm, idEstado);
      }

      // Filtrar por municipio
      if(idMunicipio != null) {
        parm++;
        query.setParameter(parm, idMunicipio);
      }

      

      // Estatus
      if(estatus != null) {
        parm++;
        query.setParameter(parm, estatus);
      }

      if(hasLavado != null) {
        parm++;
        query.setParameter(parm, hasLavado);
      }
      if(hasPresion != null) {
        parm++;
        query.setParameter(parm, idAutomovil);
      }
      if(idTipoPedido != null) {
        parm++;
        query.setParameter(parm, idTipoPedido);
      }

      
      List<Object[]> results = query.getResultList();
      
      return results.stream().map(result -> {
        ReporteClienteLineaV2 linea = new ReporteClienteLineaV2();
        linea.setIdCliente(((BigInteger) result[0]).longValue());

        if(result[1] != null)
          linea.setIdTitular(((BigInteger) result[1]).longValue());

        linea.setNombre(         (String) result[2]);
        linea.setApellidoPaterno((String) result[3]);
        linea.setApellidoMaterno((String) result[4]);
        linea.setIdEstatus(      (int)    result[5]);

        if(result[6] != null)
          linea.setBloqueo((boolean) result[6]);

        if(result[7] != null)
          linea.setAcceso((boolean) result[7]);

        linea.setFechaCreacion(  (Timestamp)      result[8]);
        linea.setFechaBaja(      (Timestamp)      result[9]);
        linea.setIdAutomovil(    (int) result[10]);
        linea.setMarca(          (String)      result[11]);
        linea.setModelo(         (String)      result[12]);
        linea.setAnio(           (String)      result[13]);
        linea.setColor(          (String)      result[14]);
        linea.setPlacas(         (String)      result[15]);
        linea.setCantidad(       ((BigInteger) result[16]).longValue());

        return linea;
      }).collect(Collectors.toList());
    } catch(Exception e) {
      e.printStackTrace();

      return null;
    }
  }
}