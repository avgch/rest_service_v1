package com.enermex.repository;

import java.math.BigInteger;

import com.enermex.modelo.TokenEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * TokenRepository
 */
public interface TokenRepository extends CrudRepository<TokenEntity,  BigInteger>{
  
}
