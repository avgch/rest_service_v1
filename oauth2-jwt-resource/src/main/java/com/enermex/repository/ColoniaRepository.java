package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.ColoniaEntity;
import com.enermex.modelo.ColoniaId;

import org.springframework.data.repository.CrudRepository;

/**
 * ColoniaRepository
 */
public interface ColoniaRepository extends CrudRepository<ColoniaEntity, ColoniaId> {
  public List<ColoniaEntity> findAll();
}
