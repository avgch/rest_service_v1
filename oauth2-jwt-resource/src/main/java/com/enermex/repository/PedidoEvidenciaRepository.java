package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.modelo.PedidoEvidenciaEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * PedidoEvidenciaRepository
 */
public interface PedidoEvidenciaRepository extends CrudRepository<PedidoEvidenciaEntity, BigInteger> {

    /**
     *
     * @param idPedido
     * idPedido
     * @return List
     */
    List<PedidoEvidenciaEntity> findByIdPedido(BigInteger idPedido);
}
