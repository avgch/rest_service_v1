package com.enermex.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.Cancelacion;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CancelacionRepository extends JpaRepository<Cancelacion, Long> {
	
    /**
     *
     * @param id
     * id
     * @return Objecto
     */
    @Query("Select c from Cancelacion c where c.pedido.idPedido = ?1")
	Cancelacion getById(BigInteger id);
	

}
