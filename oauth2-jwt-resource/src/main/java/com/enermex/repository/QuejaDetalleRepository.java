package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.QuejaDetalleEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * QuejaRepository
 */
public interface QuejaDetalleRepository extends CrudRepository<QuejaDetalleEntity, Integer> {

    /**
     *
     * @param idQueja
     * @return
     */
    public List<QuejaDetalleEntity> findByIdQueja(Long idQueja);
}
