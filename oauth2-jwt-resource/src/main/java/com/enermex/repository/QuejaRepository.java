package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.enumerable.QuejaEstatus;
import com.enermex.modelo.QuejaEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * QuejaRepository
 */
public interface QuejaRepository extends CrudRepository<QuejaEntity, Long> {

    /**
     *
     * @return
     */
    public List<QuejaEntity> findByIdClienteNotNull();

    /**
     *
     * @return
     */
    public List<QuejaEntity> findByIdDespachadorNotNull();
  
    /**
     *
     * @return
     */
    @Query(value = "SELECT * FROM t_queja WHERE estatus = 'A' AND id_cliente IS NOT NULL LIMIT 5;", nativeQuery = true)
  public List<QuejaEntity> findLimitAsignadasCliente();

    /**
     *
     * @return
     */
    @Query(value = "SELECT * FROM t_queja WHERE estatus = 'A' AND id_despachador IS NOT NULL LIMIT 5;", nativeQuery = true)
  public List<QuejaEntity> findLimitAsignadasDespachador();
  
    /**
     *
     * @param idCliente
     * @return
     */
    public List<QuejaEntity> findByIdCliente(BigInteger idCliente);

    /**
     *
     * @param idUsuario
     * @return
     */
    public List<QuejaEntity> findByIdDespachador(Long idUsuario);
  
    /**
     *
     * @param estatus
     * @return
     */
    public Long countByEstatusAndIdClienteNotNull(QuejaEstatus estatus);

    /**
     *
     * @param estatus
     * @return
     */
    public Long countByEstatusAndIdDespachadorNotNull(QuejaEstatus estatus);

    /**
     *
     * @param idPedido
     * @return
     */
    public List<QuejaEntity> findByIdPedido(BigInteger idPedido);
}
