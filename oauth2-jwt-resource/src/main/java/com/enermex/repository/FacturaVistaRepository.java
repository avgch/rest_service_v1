package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.dto.factura.FacturaVista;
import com.enermex.enumerable.PedidoFacturaEstatus;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface FacturaVistaRepository extends CrudRepository<FacturaVista, BigInteger> {
  List<FacturaVista> findAll();

    /**
     *
     * @param estatus
     * estatus
     * @return List
     */
    List<FacturaVista> findByFacturaEstatus(PedidoFacturaEstatus estatus);
}