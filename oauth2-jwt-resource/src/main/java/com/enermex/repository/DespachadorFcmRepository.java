package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.modelo.DespachadorFcmEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abraham
 */
@Repository
public interface DespachadorFcmRepository  extends JpaRepository<DespachadorFcmEntity, BigInteger> {

    /**
     *
     * @param idDespachador
     * idDespachador
     * @return List
     */
    List<DespachadorFcmEntity> findByIdDespachador(Long idDespachador);
  
    /**
     *
     * @param fcm
     * fcm
     * @return Objeto
     */
    @Nullable
  DespachadorFcmEntity findByFcm(String fcm);
}
