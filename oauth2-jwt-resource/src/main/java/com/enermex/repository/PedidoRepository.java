package com.enermex.repository;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.enermex.modelo.Pedido;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface PedidoRepository  extends JpaRepository<Pedido, BigInteger>{
	
    /**
     *
     * @param id
     * id
     * @return Objeto
     */
    @Query("Select p from Pedido p where p.idPedido =?1")
	Pedido findPedidoById(BigInteger id);
	
    /**
     *
     * @param idCliente
     * @return
     */
    @Query("Select p from Pedido p where p.cliente.idCliente = ?1")
	List<Pedido> getPedidosCliente(BigInteger idCliente);
	
    /**
     *
     * @param idCliente
     * idCliente
     * @param fechaPedido
     * fechaPedido
     * @return List
     */
    @Query("Select p from Pedido p where p.cliente.idCliente = ?1 and DATE(p.fechaPedido)=?2  order by p.fechaPedido ASC")
	List<Pedido> getPedidosCliente(BigInteger idCliente,Date fechaPedido);
	
    /**
     *
     * @param idCliente
     * idCliente
     * @return List
     */
    @Query("Select p from Pedido p where p.cliente.idCliente = ?1  order by p.fechaPedido DESC")
	List<Pedido> getPedidosClienteDesc(BigInteger idCliente);
	
    /**
     *
     * @param idDespachador
     * idDespachador
     * @param fechaPedido
     * fechaPedido
     * @return List
     */
    @Query("Select p from Pedido p where p.idDespachador = ?1 and DATE(p.fechaPedido)=?2  order by p.fechaPedido ASC")
	List<Pedido> getPedidosDespachador(Long idDespachador,Date fechaPedido);
	
    /**
     *
     * @param idPedido
     * idPedido
     * @return Long
     */
    @Query(value="Select id_despachador from t_pedido  where id_pedido = ?1",nativeQuery = true)
	Long getIdDespachadorById(BigInteger idPedido);
	
    /**
     *
     * @param idTarjeta
     * idTarjeta
     * @return BigInteger
     */
    @Query(value="Select count(*) from t_pedido  where id_tarjeta_cliente = ?1   and estatus_pedido in (1,2,3)",nativeQuery = true)
	BigInteger tarjetaTienePedido(Integer idTarjeta);
	
    /**
     *
     * @param idEstatus
     * idEstatus
     * @param idPedido
     * idPedido
     */
    @Modifying
	@Query("update Pedido p set p.estatusPedido=?1 where p.idPedido = ?2")
	void updatePedido(Integer idEstatus ,BigInteger idPedido);
	
    /**
     *
     * @param idPedido
     * idPedido
     * @return String
     */
    @Query(value="SELECT  CAST( timediff(fecha_creacion,fecha_pedido) as CHAR) as diferencia FROM t_pedido where id_pedido = ?1",nativeQuery = true)
	String difFechaCreacionPedido(BigInteger idPedido);
	
    /**
     *
     * @param calificado
     * calificado
     * @param idPedido
     * idPedido
     */
    @Modifying
	@Query("update Pedido p set p.calificado=?1 where p.idPedido = ?2")
	void calificaPedido(Boolean calificado ,BigInteger idPedido);
	
	/**
	 * Actualiza el tiempo de llegada de un pedido y la distancia a recorrer
     * @param tiempoTraslado
     * tiempoTraslado
     * @param distancia
     * distancia
	 * @param idEstatus
	 * idEstatus
	 * @param idPedido
	 * idPedido
     * @param horaEstimada
     * HoraEstimada
	 */
	@Modifying
	@Query("update Pedido p set p.tiempoTraslado=?1, p.distancia = ?2, p.horaEstimada = ?3 where p.idPedido = ?4")
	void updateDurationAndDistance(String tiempoTraslado ,String distancia ,Calendar horaEstimada,BigInteger idPedido);
	
	
	/**
	 * Actualizamos la hora de inicio del servicio
     * @param horaFechaInicio
     * horaFechaPedido
	 * @param tiempoTraslado
	 * TiempoTraslado
	 * @param distancia
	 * distancia
	 * @param idPedido
	 * idPedido
	 */
	@Modifying
	@Query("update Pedido p set p.fechaHoraInicio=?1 where p.idPedido = ?2")
	void updateHoraInicio(Calendar horaFechaInicio, BigInteger idPedido);
	
	
	/**
	 * Actualizamos la hora de fin del servicio
     * @param horaFechaFin
     * horaFechaFin
	 * @param tiempoTraslado
	 * tiempoTraslado
	 * @param distancia
	 * distancia
	 * @param idPedido
	 * idPedido
	 */
	@Modifying
	@Query("update Pedido p set p.fechaHoraFin=?1 where p.idPedido = ?2")
	void updateHoraFin(Calendar horaFechaFin, BigInteger idPedido);
	
	
	/**
	 * Actualizamos los litros despachador
     * @param litrosDespachados
     * litrosDespachados
	 * @param tiempoTraslado
	 * tiempoTraslado
	 * @param distancia
	 * distancia
	 * @param idPedido
	 * idPedido
	 */
	@Modifying
	@Query("update Pedido p set p.litrosDespachados=?1 where p.idPedido = ?2")
	void updateLitrosDespachados(String litrosDespachados, BigInteger idPedido);
	
    /**
     *
     * @param montoFinal
     * montoFinal
     * @param iva
     * iva
     * @param subTotal
     * subTotal
     * @param totalImporte
     * totalImporte
     * @param totalPagar
     * totalPagar
     * @param descuento
     * descuento
     * @param totalConDescuento
     * totalConDescuento
     * @param precioPorLitroFinal
     * precioPorLitroFinal
     * @param idPedido
     * idPedido
     */
    @Modifying
	@Query("update Pedido p set p.montoCarga=?1, p.iva=?2, p.subTotal=?3, p.totalImporte=?4,p.totalPagar=?5,p.descuento=?6,p.totalConDescuento=?7, p.precioPorLitroFinal=?8  where p.idPedido = ?9")
	void updateCompra(Double montoFinal,Double iva,Double subTotal,Double totalImporte,Double totalPagar,Double descuento,Double totalConDescuento,Double precioPorLitroFinal, BigInteger idPedido);
	
    /**
     *
     * @param latitudDespachador
     * latitudDespachador
     * @param longitudDespachador
     * longitudDespachador
     * @param idPedido
     * idPedido
     */
    @Modifying
	@Query("update Pedido p set p.latitudDespachador=?1, p.longitudDespachador=?2  where p.idPedido = ?3")
	void saveLatAndLong(String latitudDespachador,String longitudDespachador, BigInteger idPedido);
	
    /**
     *
     * @param idCliente
     * idCliente
     * @return {@link BigInteger}
     */
    @Query(value="Select count(*) from t_pedido  where id_cliente = ?1  and estatus_pedido in (1,2,3)",nativeQuery = true)
	BigInteger getNumPedidosCliente(BigInteger idCliente);
	
	


}
