package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.TarifaLavadoHistorialEntity;
import com.enermex.modelo.TarifaLavadoHistorialId;

import org.springframework.data.repository.CrudRepository;

/**
 * TarifaLavadoHistorialRepository
 */
public interface TarifaLavadoHistorialRepository extends CrudRepository<TarifaLavadoHistorialEntity, TarifaLavadoHistorialId> {

    /**
     *
     * @return
     */
    List<TarifaLavadoHistorialEntity> findAllByOrderByFechaCreacionAsc();

}
