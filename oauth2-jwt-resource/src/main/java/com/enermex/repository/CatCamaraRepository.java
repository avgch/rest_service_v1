package com.enermex.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.CatCamara;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CatCamaraRepository extends JpaRepository<CatCamara, Integer> {
	
    /**
     *
     * @param id
     * id
     * @return CatCamara
     */
    @Query("Select p from CatCamara p where p.idCamara = ?1 ")
	CatCamara findCamaraById(Integer id);
	
    /**
     *
     * @param nombre
     * nombre
     * @param numSerie
     * numSerie
     * @param modelo
     * modelo
     * @param estatus
     * estatus
     * @param motivoBaja
     * motivoBaja
     * @param idPipa
     * idPipa
     * @param idCamara
     * idCamara
     */
    @Modifying
	@Query("update CatCamara c set c.nombre=?1, c.numSerie=?2, c.modelo=?3, c.estatus=?4, c.motivoBaja=?5, c.idPipa=?6  where c.idCamara = ?7")
	void updateCamara(String nombre,String numSerie,String modelo,String estatus,String motivoBaja,Integer idPipa, Integer idCamara);
	
    /**
     *
     * @return List
     */
    @Query("Select p from CatCamara p ")
	List<CatCamara> getCamaras();
	
    /**
     *
     * @return List
     */
    @Query("Select p from CatCamara p where p.estatus like 'Activa' ")
	List<CatCamara> camarasActivas();
	
    /**
     *
     * @param estatus
     * estatus
     * @param nombrePipa
     * nombrePipa
     * @param idCamara
     * idCamara
     */
    @Modifying
	@Query("update CatCamara c set c.estatus=?1, c.pipaAsignada=?2  where c.idCamara = ?3")
	void updateCamaraStatus(String estatus,String nombrePipa, Integer idCamara);

}
