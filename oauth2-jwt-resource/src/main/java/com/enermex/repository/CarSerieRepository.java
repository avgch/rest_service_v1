package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CarSerie;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CarSerieRepository extends JpaRepository<CarSerie, Integer> {
	
    /**
     *
     * @param idModelo
     * idModelo
     * @param idGeneration
     * idGeneration
     * @return List
     */
    @Query("Select cs from CarSerie cs  where cs.idModelo = ?1 and cs.idCarGeneration=?2")
	List<CarSerie> getSeriesByModeloId(Integer idModelo,Integer idGeneration);

}
