package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.Parametro;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface ParametroRepository extends JpaRepository<Parametro, Integer> {
	
    /**
     *
     * @return list
     */
    @Query("Select p  from Parametro p where p.tipo = 'Tipo pedido'")
	List<Parametro> getParamsTipoPedo();
	
    /**
     *
     * @return List
     */
    @Query("Select p  from Parametro p where p.tipo = 'Tarifa' or p.tipo = 'Tipo pedido' or p.tipo='Tarifa cancelación' ")
	List<Parametro> costosPorServicioAndTimepoProrroga();
	
    /**
     *
     * @return List
     */
    @Query("Select p  from Parametro p where p.tipo = 'Tiempo cancelación'")
	List<Parametro> tiemposCancelacionService();
	
    /**
     *
     * @return
     */
    @Query("Select p  from Parametro p ")
	List<Parametro> getParametros();
	
    /**
     *
     * @return Objeto
     */
    @Query("Select p  from Parametro p where p.tipo = 'Cliente Frecuente' ")
	Parametro clienteFrecuenteValue();
	
    /**
     *
     * @param id
     * id
     * @return Objeto
     */
    @Query("Select p  from Parametro p where p.idParametro = ?1 ")
	Parametro getParametroById(Integer id);
	
    /**
     *
     * @param valor
     * valor
     * @param id
     * id
     */
    @Query("update Parametro p set p.valor=?1 where p.idParametro = ?2")
	@Modifying
	void updateParametro(String valor,Integer id);

}
