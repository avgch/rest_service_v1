package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CarSpecificationValue;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CarSpecificationValueRepository extends JpaRepository<CarSpecificationValue, Integer> {
	
    /**
     *
     * @param idTrim
     * idTrim
     * @return List
     */
    @Query("Select cs from CarSpecificationValue cs  where cs.idCarTrim = ?1 and  cs.idCarSpecification in (12,35) ")
	List<CarSpecificationValue> getSpecificationValueByTrimAndSpecification(Integer idTrim);

}
