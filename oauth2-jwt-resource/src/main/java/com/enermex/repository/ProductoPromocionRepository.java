package com.enermex.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enermex.modelo.ProductoPromocion;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface ProductoPromocionRepository extends JpaRepository<ProductoPromocion, Long>{

    /**
     *
     * @param idPromocion
     * @return
     */
    @Query("Select p  from ProductoPromocion p where p.promocion.idPromocion = ?1")
	@Modifying
	List<ProductoPromocion> getProductosPromo(Long idPromocion);
	
    /**
     *
     * @param idPromocion
     */
    @Query("Delete from ProductoPromocion pp where pp.promocion.idPromocion=?1")
	@Modifying
	void deleteByIdPromo(Long idPromocion);
}
