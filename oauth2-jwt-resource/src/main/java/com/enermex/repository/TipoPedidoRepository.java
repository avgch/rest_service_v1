package com.enermex.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.TipoPedido;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface TipoPedidoRepository extends JpaRepository<TipoPedido,Integer>{

    /**
     *
     * @param tipo
     * @return
     */
    @Query("Select tp from TipoPedido tp where tp.idTipoPedido = ?1")
	TipoPedido getTipoPedido(Integer tipo);
}
