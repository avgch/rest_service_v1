package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.DireccionCliente;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface DireccionClienteRepository extends JpaRepository<DireccionCliente, Integer> {
	
	/**
	 * Busca direccion cliente
	 * @param idCliente
	 * idCliente
	 * @return List
	 */
	@Modifying
	@Query("select dc from DireccionCliente dc  where dc.estatus = 1 and dc.cliente.idCliente=?1")
	List<DireccionCliente> findDireccionesByIdCliente( BigInteger idCliente);
	
	/**
	 * Busca direccion cliente
	 * @param idDireccion
	 * idDireccion
	 * @return Objeto
	 */
	@Query("select dc from DireccionCliente dc  where dc.estatus = 1 and dc.idDireccion = ?1")
	DireccionCliente buscaDireccionCliente(Integer idDireccion);
	
	/**
	 * Busca direccion despachador
	 * @param idDireccion
	 * idDireccion
	 * @return Objeto
	 */
	@Query("select dc from DireccionCliente dc  where  dc.idDireccion = ?1")
	DireccionCliente buscaDireccionClienteDespachador(Integer idDireccion);
	
	/**
	 * Busca direccion cliente
	 * @param idDireccion
	 * idDireccion
	 * @return Objeto
	 */
	@Query("select dc from DireccionCliente dc  where dc.estatus = 1 and dc.idDireccion = ?1")
	DireccionCliente buscaDireccionClienteById(Integer idDireccion);

}
