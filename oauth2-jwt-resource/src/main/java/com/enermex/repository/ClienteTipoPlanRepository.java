package com.enermex.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.ClienteTipoPlan;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface ClienteTipoPlanRepository extends JpaRepository<ClienteTipoPlan, BigInteger>{

}
