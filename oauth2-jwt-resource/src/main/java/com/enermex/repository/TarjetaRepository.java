package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.Tarjeta;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface TarjetaRepository extends  JpaRepository<Tarjeta, Integer> {

    /**
     *
     * @param idCliente
     * @return
     */
    @Query("Select t  from Tarjeta t where t.idCliente = ?1 and t.estatus=1 and t.borrado =0")
	List<Tarjeta> getTarjetasActivas(BigInteger idCliente);
	
    /**
     *
     * @param idTarjeta
     */
    @Modifying
	@Query("update Tarjeta t set t.borrado=true  where t.idTarjeta = ?1")
	void borrar(Integer idTarjeta);
	
    /**
     *
     * @param idTarjeta
     * @return
     */
    @Query("Select t  from Tarjeta t where t.idTarjeta = ?1 and t.estatus=1")
    Tarjeta  getTarjetasById(Integer idTarjeta);
	
	/**
	 * Author; Abraham Vargas
	 * Obtiene Tarjetas activas plan familiar
	 * @param idCliente
	 * @return
	 */
	@Query(value="select tc.id_tarjeta_cliente,tc.id_token_tarjeta,tc.numero_tarjeta,tc.id_cliente,tc.estatus,tc.borrado,tc.tipo_tarjeta\n" + 
			"    from t_cat_tarjeta_cliente tc,t_tarjeta_plan tp\n" + 
			"    where  tc.id_tarjeta_cliente = tp.id_tarjeta_cliente\n" + 
			"     and tc.borrado = 0\n" + 
			"     and tp.id_plan =2\n" + 
			"     and tc.id_cliente= ?1",nativeQuery = true)
	List<Tarjeta> getTarjetasAPlanFamiliar(BigInteger idCliente);
}
