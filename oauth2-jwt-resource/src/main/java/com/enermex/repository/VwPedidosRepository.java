package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.enermex.view.VwPedidos;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface VwPedidosRepository   extends JpaRepository<VwPedidos, Integer>{
	
    /**
     *
     * @return
     */
    @Query("Select vp from VwPedidos vp")
	List<VwPedidos> getPedidos();

}
