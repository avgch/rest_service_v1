package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.modelo.QuejaEvidenciaEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * QuejaEvidenciaRepository
 */
public interface QuejaEvidenciaRepository extends CrudRepository<QuejaEvidenciaEntity, Long> {
  public List<QuejaEvidenciaEntity> findAll();

    /**
     *
     * @param idPedido
     * @return
     */
    @Query(value = "SELECT e.* FROM t_queja_evidencia AS e INNER JOIN t_queja as q ON e.id_queja = q.id_queja INNER JOIN t_pedido as p ON p.id_pedido = ?1; ", nativeQuery = true)
  public List<QuejaEvidenciaEntity> findByIdPedido(BigInteger idPedido);
}
