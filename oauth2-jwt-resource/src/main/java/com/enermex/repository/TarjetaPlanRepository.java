package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.TarjetaPlan;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface TarjetaPlanRepository extends  JpaRepository<TarjetaPlan, Integer> {
	
    /**
     *
     * @return
     */
    @Query("Select tp from TarjetaPlan tp")
	List<TarjetaPlan> getTarjetaPlanes();
	
    /**
     *
     * @param idTarjeta
     * @return
     */
    @Query("Select tp.idPlan from TarjetaPlan tp Where tp.idTarjetaCliente = ?1")
	List<Integer> getTarjetaPlanesById(Integer idTarjeta);
	
    /**
     *
     * @param idTarjeta
     */
    @Modifying
	@Query("delete from TarjetaPlan tp Where tp.idTarjetaCliente = ?1")
	void deletePlanes(Integer idTarjeta);

}
