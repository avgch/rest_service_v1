package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.dto.RolDto;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface RolRepositoryA  extends JpaRepository<RolDto, Long> {
	
	List<RolDto> findAll();
//	@Modifying
//	@Query("update UsuarioDto u set u.nombre=?1,u.apellidos=?2, u.correo = ?3, u.telefono = ?4  where u.idUsuario = ?3")
//	void updateUsuario(String nombre,String apellidos, String correo, String telefono, Integer usuarioId);	
	
    /**
     *
     * @param rol
     * @return
     */
    @Query("select r.idRol from RolDto r where r.rol = ?1")
	Integer findByRol(String rol);
	

}
