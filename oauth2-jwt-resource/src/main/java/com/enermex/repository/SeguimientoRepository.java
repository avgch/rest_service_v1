package com.enermex.repository;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import com.enermex.modelo.SeguimientoPedidoEntity;
import com.enermex.modelo.SeguimientoPedidoEntity.SRutaEntity;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abraham
 */
@Repository
public interface SeguimientoRepository  extends JpaRepository<SeguimientoPedidoEntity, BigInteger> {
  // Paginación para rango de fechas

    /**
     *
     * @param inicio
     * @param fin
     * @return
     */
  Long countByFechaPedidoBetween(Calendar inicio, Calendar fin); 

    /**
     *
     * @param inicio
     * @param fin
     * @param pageable
     * @return
     */
    List<SeguimientoPedidoEntity> findByFechaPedidoBetweenOrderByIdPedidoAsc(
    Calendar inicio, Calendar fin, Pageable pageable); 
  
  // Paginación para rango de fechas y ruta

    /**
     *
     * @param inicio
     * @param fin
     * @param ruta
     * @return
     */
  Long countByFechaPedidoBetweenAndIdRuta(Calendar inicio, Calendar fin, Integer ruta); 

    /**
     *
     * @param inicio
     * @param fin
     * @param ruta
     * @param pageable
     * @return
     */
    List<SeguimientoPedidoEntity> findByFechaPedidoBetweenAndIdRutaOrderByIdPedidoAsc(
    Calendar inicio, Calendar fin, Integer ruta, Pageable pageable); 
  
  // Paginación para rango de fechas y estatus

    /**
     *
     * @param inicio
     * @param fin
     * @param estatus
     * @return
     */
  Long countByFechaPedidoBetweenAndEstatusPedido(Calendar inicio, Calendar fin, Integer estatus); 

    /**
     *
     * @param inicio
     * @param fin
     * @param estatus
     * @param pageable
     * @return
     */
    List<SeguimientoPedidoEntity> findByFechaPedidoBetweenAndEstatusPedidoOrderByIdPedidoAsc(
    Calendar inicio, Calendar fin, Integer estatus, Pageable pageable);
  
  // Paginación para rango de fechas y ruta y estatus

    /**
     *
     * @param inicio
     * @param fin
     * @param ruta
     * @param estatus
     * @return
     */
  Long countByFechaPedidoBetweenAndIdRutaAndEstatusPedido(Calendar inicio, Calendar fin, Integer ruta, Integer estatus); 

    /**
     *
     * @param inicio
     * @param fin
     * @param ruta
     * @param estatus
     * @param pageable
     * @return
     */
    List<SeguimientoPedidoEntity> findByFechaPedidoBetweenAndIdRutaAndEstatusPedidoOrderByIdPedidoAsc(
    Calendar inicio, Calendar fin, Integer ruta, Integer estatus, Pageable pageable); 
}
