package com.enermex.repository;

import java.util.List;

import com.enermex.dto.DespachadorDto;
import com.enermex.modelo.Cliente;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
/**
 * Repositorio de tabla Despachador
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 27 de diciembre de 2019
 */
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.UsuarioEntity;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface DespachadorRepository extends CrudRepository<DespachadorDto, Long> {
    /**
     * Permite obtener los despachadores sin cierto estado; con lo que se
     * pueden ignorar los pseudoeliminados.
     * 
     * @param status Estado ignorado para el conjunto de despachadores
     * @return Lista de despachadores
     */
    List<DespachadorDto> findAllByStatusNot(Integer status);

    /**
     *
     * @param despachador
     * despachador
     * @param status
     * estatus
     * @return Objeto
     */
    @Nullable
    DespachadorDto findByIdDespachadorAndStatusNot(Long despachador, Integer status);
    
    /**
     *
     * @param email
     * email
     * @return Objeto
     */
    @Query("Select u from UsuarioEntity u where u.correo = ?1")
    UsuarioEntity findByEmail(String email);
}