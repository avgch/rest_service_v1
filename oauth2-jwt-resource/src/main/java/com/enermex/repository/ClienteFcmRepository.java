package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.modelo.ClienteFcmEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abraham
 */
@Repository
public interface ClienteFcmRepository  extends JpaRepository<ClienteFcmEntity, BigInteger> {

    /**
     *
     * @param idCliente
     * idCliente
     * @return List
     */
    List<ClienteFcmEntity> findByIdCliente(BigInteger idCliente);
  
    /**
     *
     * @param fcm
     * fcm
     * @return ClienteFcmEntity
     */
    @Nullable
  ClienteFcmEntity findByFcm(String fcm);
  
    /**
     *
     * @param idsClientes
     * idClientes
     * @return List
     */
    @Query("Select cf.fcm from ClienteFcmEntity cf where cf.idCliente in ?1  ")
   List<String> getTokensClientes(List<BigInteger> idsClientes);
  
}
