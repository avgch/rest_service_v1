package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.EstadoEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * EstadoRepository
 */
public interface EstadoRepository extends CrudRepository<EstadoEntity, Integer> {
  public List<EstadoEntity> findAll();
}
