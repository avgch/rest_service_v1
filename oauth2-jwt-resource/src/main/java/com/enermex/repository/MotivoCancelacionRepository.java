package com.enermex.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.enermex.modelo.CatMotivoCancelacion;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface MotivoCancelacionRepository extends JpaRepository<CatMotivoCancelacion, Integer> {
	
    /**
     *
     * @return List
     */
    @Query("Select mc from CatMotivoCancelacion mc  where mc.tipo = 'Usuario' ")
	List<CatMotivoCancelacion> getAllCliente();
	
    /**
     *
     * @return List
     */
    @Query("Select mc from CatMotivoCancelacion mc  where mc.tipo = 'Despachador' ")
	List<CatMotivoCancelacion> getAllDespachador();
	
    /**
     *
     * @param idMotivoCancelacion
     * @return Objeto
     */
    @Query("Select mc from CatMotivoCancelacion mc  where mc.idMotivoCancelacion =?1 ")
	CatMotivoCancelacion getById(Integer idMotivoCancelacion);
	
	
	
}
