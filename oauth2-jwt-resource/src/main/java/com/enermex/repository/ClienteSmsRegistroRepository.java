package com.enermex.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.ClienteSmsRegistro;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface ClienteSmsRegistroRepository extends JpaRepository<ClienteSmsRegistro, BigInteger> {
	
    /**
     *
     * @param currentDate
     * currentDate
     * @param codigo
     * codigo
     * @param id
     * id
     * @return BigInteger
     */
    @Query(value=" select count(*) from t_cliente_sms_registro csr where  ?1 <= DATE_FORMAT(fecha_vigencia,'%d-%m-%Y %H:%i')   "
			+ " and csr.codigo like ?2 and csr.id_cliente = ?3 and  csr.activo = 1 ", nativeQuery=true)
	BigInteger esCodigoVigente(String currentDate,String codigo,BigInteger id);
	
    /**
     *
     * @param codigo
     * codigo
     * @param idCliente
     * idCliente
     */
    @Modifying
	@Query(value=" update t_cliente_sms_registro csr  set csr.activo=0 where "
			+ "  csr.codigo like ?1 and csr.id_cliente = ?2 and  csr.activo = 1 ", nativeQuery=true)
	void updateCodigoCliente(String codigo ,BigInteger idCliente);
	

}
