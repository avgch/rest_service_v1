package com.enermex.repository;

import com.enermex.view.TiempoTrasladoView;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface TiempoTrasladoRepository extends CrudRepository<TiempoTrasladoView, Long> {
  
}
