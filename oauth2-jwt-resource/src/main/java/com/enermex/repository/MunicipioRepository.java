package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.MunicipioEntity;
import com.enermex.modelo.MunicipioId;

import org.springframework.data.repository.CrudRepository;

/**
 * MunicipioRepository
 */
public interface MunicipioRepository extends CrudRepository<MunicipioEntity, MunicipioId> {
  public List<MunicipioEntity> findAll();
}
