package com.enermex.repository;

import com.enermex.view.DespachadorServicioView;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface DespachadorServicioRepository extends CrudRepository<DespachadorServicioView, Long> {
  
}