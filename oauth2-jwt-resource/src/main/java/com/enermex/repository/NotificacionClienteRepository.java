package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.enermex.modelo.NotificacionCliente;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface  NotificacionClienteRepository extends CrudRepository<NotificacionCliente, Integer> {

    /**
     *
     * @return List
     */
    @Query("Select nc from NotificacionCliente nc ")
	List<NotificacionCliente> getAll();
	
    /**
     *
     * @param idCliente
     * idCliente
     * @return List
     */
    @Query("Select nc.idNotificacion from NotificacionCliente nc where nc.idCliente=?1 ")
	List<BigInteger> getAllNotiClienteById(BigInteger idCliente);

}
