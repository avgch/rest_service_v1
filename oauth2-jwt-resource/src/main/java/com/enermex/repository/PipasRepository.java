package com.enermex.repository;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.dto.PipaDto;
import com.enermex.modelo.Pipa;;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface PipasRepository extends JpaRepository<Pipa, Long> {

    /**
     *
     * @param id
     * @return
     */
    @Query("Select p from Pipa p where p.idPipa = ?1 ")
	Pipa findPipaById(Integer id);
	
	//get all for camaras

    /**
     *
     * @return
     */
	@Query("Select p from Pipa p where p.estatus like 'Activo'")
	List<Pipa>  allPipasActivas();
	
	//get all for pipas

    /**
     *
     * @return
     */
	@Query("Select p from Pipa p ")
	List<Pipa>  allPipasActivasPipas();
	
    /**
     *
     * @param nombre
     * @param numSerie
     * @param placas
     * @param estatus
     * @param motivoBaja
     * @param diesel
     * @param regular
     * @param premium
     * @param tamanoTanque
     * @param totalRegular
     * @param totalPremium
     * @param totalDiesel
     * @param litrosRestantesDiesel
     * @param litrosRestantesRegular
     * @param litrosRestantesPremium
     * @param fechaModificacion
     * @param idPipa
     */
    @Modifying
	@Query("update Pipa p set p.nombre=?1, p.numSerie=?2, p.placas=?3, p.estatus=?4, p.motivoBaja=?5, p.diesel =?6, p.regular=?7, p.premium =?8,p.tamanoTotalTanque=?9"
	+ ",p.totalRegular=?10,p.totalPremium=?11,p.totalDiesel=?12,p.litrosRestantesDiesel=?13,p.litrosRestantesRegular=?14,"
	+ "p.litrosRestantesPremium=?15,p.fechaModificacion=?16   where p.idPipa = ?17")
	void updatePipa(String nombre,String numSerie,String placas,String estatus,String motivoBaja,boolean diesel,boolean regular,boolean premium,String tamanoTanque,
			Double totalRegular,Double totalPremium,Double totalDiesel,Double litrosRestantesDiesel,Double litrosRestantesRegular,Double litrosRestantesPremium,
			Calendar fechaModificacion,Integer idPipa);
	
    /**
     *
     * @param nombre
     * @param numSerie
     * @param placas
     * @param estatus
     * @param motivoBaja
     * @param diesel
     * @param regular
     * @param premium
     * @param tamanoTanque
     * @param totalRegular
     * @param totalPremium
     * @param totalDiesel
     * @param idPipa
     */
    @Modifying
	@Query("update Pipa p set p.nombre=?1, p.numSerie=?2, p.placas=?3, p.estatus=?4, p.motivoBaja=?5, p.diesel =?6, p.regular=?7, p.premium =?8,p.tamanoTotalTanque=?9"
	+ ",p.totalRegular=?10,p.totalPremium=?11,p.totalDiesel=?12   where p.idPipa = ?13")
	void updatePipaSinCarga(String nombre,String numSerie,String placas,String estatus,String motivoBaja,boolean diesel,boolean regular,boolean premium,String tamanoTanque,
			Double totalRegular,Double totalPremium,Double totalDiesel,Integer idPipa);
	
    /**
     *
     * @param litros
     * @param idPipa
     */
    @Modifying
	@Query(" update Pipa p set p.totalRegular= ?1 where p.idPipa =?2")
	void updateLitrosRegularPipa(Double litros,Integer idPipa);
	
    /**
     *
     * @param litros
     * @param idPipa
     */
    @Modifying
	@Query(" update Pipa p set p.totalDiesel= ?1 where p.idPipa =?2 ")
	void updateLitrosDieselPipa(Double litros,Integer idPipa);
	
    /**
     *
     * @param litros
     * @param idPipa
     */
    @Modifying
	@Query(" update Pipa p set p.totalPremium= ?1 where p.idPipa =?2 ")
	void updateLitrosPremiumPipa(Double litros,Integer idPipa);
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query("Select p.totalRegular from Pipa p where p.idPipa = ?1 ")
	Double getLitrosRegular(Integer idPipa);
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query("Select p.totalDiesel from Pipa p where p.idPipa = ?1 ")
	Double getLitrosDiesel(Integer idPipa);
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query("Select p.totalPremium from Pipa p where p.idPipa = ?1 ")
	Double getLitrosPremium(Integer idPipa);
	
    /**
     *
     * @param nombre
     * @param idPipa
     * @return
     */
    @Query(value = "select  case when count(*)>0 then 'false' else 'true' end from t_cat_pipa where nombre like :nombre and id_pipa!=:idPipa ",nativeQuery = true)
	boolean existeNombrePipa(String nombre,Integer idPipa);
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query(value = "select litros_restantes_regular from t_cat_pipa where  id_pipa=:idPipa ",nativeQuery = true)
	Double getLitrosRestantesRegular(Integer idPipa);
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query(value = "select litros_restantes_premium from t_cat_pipa where  id_pipa=:idPipa ",nativeQuery = true)
	Double getLitrosRestantesPremium(Integer idPipa);
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query(value = "select litros_restantes_diesel from t_cat_pipa where  id_pipa=:idPipa ",nativeQuery = true)
	Double getLitrosRestantesDiesel(Integer idPipa);
	
    /**
     *
     * @param litrosRestantesDiesel
     * @param idPipa
     */
    @Modifying
	@Query(" update Pipa p set p.litrosRestantesDiesel= ?1 where p.idPipa =?2")
	void updateLitrosRestantesDiesel(Double litrosRestantesDiesel,Integer idPipa);
	
    /**
     *
     * @param litrosRestantesPremium
     * @param idPipa
     */
    @Modifying
	@Query(" update Pipa p set p.litrosRestantesPremium= ?1 where p.idPipa =?2")
	void updateLitrosRestantesPremium(Double litrosRestantesPremium,Integer idPipa);
	
    /**
     *
     * @param litrosRestantesRegular
     * @param idPipa
     */
    @Modifying
	@Query(" update Pipa p set p.litrosRestantesRegular= ?1 where p.idPipa =?2")
	void updateLitrosRestantesRegular(Double litrosRestantesRegular,Integer idPipa);
	
    /**
     *
     * @param hoy
     * @param combustible
     * @param idPipa
     * @return
     */
    @Query(value = "select sum(litro_carga) from t_pedido p where date_format(p.fecha_pedido,'%Y-%m-%d')=:hoy and p.id_combustible=:combustible and estatus_pedido in(1) and p.id_pipa=:idPipa",nativeQuery = true)
	Double  getLitrosProgramadosDelDia(String hoy,Integer combustible,Integer idPipa);

}

