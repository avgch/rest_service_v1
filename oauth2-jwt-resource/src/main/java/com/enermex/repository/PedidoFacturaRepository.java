package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.modelo.PedidoFacturaEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * PedidoFacturaRepository
 */
public interface PedidoFacturaRepository extends CrudRepository<PedidoFacturaEntity, BigInteger> {

    /**
     *
     * @param idCliente
     * idCliente
     * @return List
     */
    @Query(value = "SELECT f.* FROM t_pedido_factura AS f JOIN t_cliente_fiscal AS c ON f.id_fiscal = c.id_fiscal WHERE c.id_cliente = ?1", nativeQuery = true)
  List<PedidoFacturaEntity> findByIdCliente(BigInteger idCliente);

  List<PedidoFacturaEntity> findAll();
  
    /**
     *
     * @param idFactura
     * idFactura
     * @return Objeto
     */
    PedidoFacturaEntity findByIdFactura(BigInteger idFactura);

    /**
     *
     * @param idPedido
     * idPedido
     * @return Objeto
     */
    PedidoFacturaEntity findByIdPedido(BigInteger idPedido);

    /**
     *
     * @param idPedido
     * idPedido
     * @return boolean
     */
    boolean existsByIdPedido(BigInteger idPedido);

    /**
     *
     * @param idFiscal
     * idFiscal
     * @return boolean
     */
    boolean existsByIdFiscal(BigInteger idFiscal);
}