package com.enermex.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enermex.dto.PipaDto;
import com.enermex.modelo.CatMarca;
import com.enermex.modelo.CatMarca;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CatMarcaRepository extends JpaRepository<CatMarca, Integer>{
	
    /**
     *
     * @return List
     */
    @Query("Select cm  from CatMarca cm ")
	List<CatMarca> getAll();
	
    /**
     *
     * @param idMarca
     * idMarca
     * @return CatMarca
     */
    @Query("Select cm  from CatMarca cm where  cm.idMarca = ?1")
	CatMarca getMarcaById(Integer idMarca);

}
