package com.enermex.repository;

import java.util.List;

import com.enermex.enumerable.FaqEstatus;
import com.enermex.modelo.FaqEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * TODO: Completar para aplicación angular
 */
public interface FaqRepository extends CrudRepository<FaqEntity, Integer> {
  public List<FaqEntity> findAll();

    /**
     *
     * @param estatus
     * estatus
     * @return List
     */
    public List<FaqEntity> findByEstatus(FaqEstatus estatus);
}
