package com.enermex.repository;

import com.enermex.modelo.AzureBsEntity;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface AzureBsRepository extends CrudRepository<AzureBsEntity, Integer> {

}