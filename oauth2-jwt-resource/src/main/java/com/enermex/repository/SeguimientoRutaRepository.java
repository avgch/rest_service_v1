package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.SeguimientoPedidoEntity.SRutaEntity;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface SeguimientoRutaRepository extends CrudRepository<SRutaEntity, Integer> {
  List<SRutaEntity> findAll();
}