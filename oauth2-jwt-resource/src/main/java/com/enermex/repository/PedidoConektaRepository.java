package com.enermex.repository;


import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.PedidoConekta;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface PedidoConektaRepository extends JpaRepository<PedidoConekta, Long>{
	
    /**
     *
     * @param idPedido
     * idPedido
     * @return Objeto
     */
    @Query("Select pc from PedidoConekta pc  where pc.idPedido= ?1")
   PedidoConekta finById(BigInteger idPedido);
   
    /**
     *
     * @param idPedido
     * idPedido
     */
    @Modifying
   @Query("Delete  from PedidoConekta pc  where pc.idPedido= :idPedido")
   void deleteById(BigInteger idPedido);

}
