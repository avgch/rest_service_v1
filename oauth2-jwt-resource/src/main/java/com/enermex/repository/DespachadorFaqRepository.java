package com.enermex.repository;

import java.util.List;

import com.enermex.enumerable.FaqEstatus;
import com.enermex.modelo.DespachadorFaqEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * TODO: Completar para aplicación angular
 */
public interface DespachadorFaqRepository extends CrudRepository<DespachadorFaqEntity, Integer> {
  public List<DespachadorFaqEntity> findAll();

    /**
     *
     * @param estatus
     * estatus
     * @return List
     */
    public List<DespachadorFaqEntity> findByEstatus(FaqEstatus estatus);
}
