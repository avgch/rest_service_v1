package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.PlanPromocion;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface PromocionPlanRepository extends JpaRepository<PlanPromocion, Long> {
	
    /**
     *
     * @param idPromocion
     */
    @Query("delete from PlanPromocion pc where pc.promocion.idPromocion = ?1")
	@Modifying
	void delete(Long idPromocion);

}
