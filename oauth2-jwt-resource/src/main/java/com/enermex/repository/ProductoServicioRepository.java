package com.enermex.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.enermex.modelo.ProductoServicio;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface ProductoServicioRepository extends CrudRepository<ProductoServicio, Integer> {

    /**
     *
     * @return
     */
    @Query("Select ps from ProductoServicio ps where ps.borrado = false ")
	List<ProductoServicio> getAll();
}
