package com.enermex.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.enermex.modelo.Cliente;
import com.enermex.modelo.GasolinaPrecio;

/**
 *
 * @author abraham
 */
@Repository
public interface GasolinaPrecioRepository extends CrudRepository<GasolinaPrecio, Integer> {
	
    /**
     *
     * @param cp
     * cp
     * @param idTipoCombustible
     * idTipoCombustible
     * @return Objeto
     */
    @Query("Select gp from GasolinaPrecio gp where gp.cp = ?1 and gp.idTipoCombustible = ?2")
	GasolinaPrecio getPrecioByCp(String cp, Integer idTipoCombustible );

}
