package com.enermex.repository;


import java.util.List;

import com.enermex.dto.tarifa.TarifaVista;

import org.springframework.data.repository.CrudRepository;

/**
 * TarifaWebRepository
 */
public interface TarifaVistaRepository extends CrudRepository<TarifaVista, String> {
  List<TarifaVista> findAll();
}
