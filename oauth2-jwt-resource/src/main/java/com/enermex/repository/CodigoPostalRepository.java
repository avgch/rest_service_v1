package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.CodigoPostalEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * CodigoPostalRepository
 */
public interface CodigoPostalRepository extends CrudRepository<CodigoPostalEntity, String> {
  public List<CodigoPostalEntity> findAll();

    /**
     *
     * @param idEstado
     * idEstado
     * @param idMunicipio
     * idMunicipio
     * @return List
     */
    public List<CodigoPostalEntity> findByIdEstadoAndIdMunicipio(Integer idEstado, Integer idMunicipio);
}
