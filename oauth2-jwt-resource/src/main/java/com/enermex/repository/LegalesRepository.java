package com.enermex.repository;

import com.enermex.modelo.LegalesEntity;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface LegalesRepository extends CrudRepository<LegalesEntity, Integer> {

}
