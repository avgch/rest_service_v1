package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.DireccionCliente;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface DireccionRepository extends JpaRepository<DireccionCliente, Long> {

    /**
     *
     * @param calle
     * calle
     * @param cp
     * cp
     * @param colonia
     * colonia
     * @param latitud
     * latitud
     * @param longitud
     * latitud
     * @param idDireccion
     * idDireccion
     */
    @Modifying
	@Query("update DireccionCliente d set d.calle=?1, d.cp=?2, d.colonia=?3, d.latitud=?4, d.longitud=?5   where d.idDireccion = ?6")
	void update(String calle,String cp,String colonia,double latitud, double longitud, Integer idDireccion);
}
