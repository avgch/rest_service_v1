package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.modelo.ClienteTipoPlan;
import com.enermex.modelo.ClienteTipoPlanKey;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface ClienteTipoPlanRepository2 extends CrudRepository<ClienteTipoPlan, ClienteTipoPlanKey> {

    /**
     *
     * @param idCliente
     * idCliente
     * @return List
     */
    List<ClienteTipoPlan> findByClienteTipoPlanKeyIdCliente(BigInteger idCliente);
}