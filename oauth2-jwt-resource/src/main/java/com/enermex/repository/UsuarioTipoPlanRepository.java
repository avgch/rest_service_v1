package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.UsuarioTipoPlan;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface UsuarioTipoPlanRepository extends JpaRepository<UsuarioTipoPlan, Long>{

}
