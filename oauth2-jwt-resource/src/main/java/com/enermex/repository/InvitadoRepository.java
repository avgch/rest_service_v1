package com.enermex.repository;

import java.math.BigInteger;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enermex.modelo.Cliente;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface InvitadoRepository  extends JpaRepository<Cliente, Integer>{
	
    /**
     *
     * @param id
     * id
     */
    @Modifying
	@Query("update Cliente c set  c.estatus=0  where c.idCliente = ?1 and c.estatus=1")
	void deleteCliente(BigInteger id);
	
    /**
     *
     * @param id
     * id
     * @param bloqueo
     * bloqueo
     */
    @Modifying
	@Query("update Cliente c set  c.bloqueo=?2  where c.idCliente = ?1 and c.estatus=1")
	void blockUnlock(BigInteger id,boolean bloqueo);
	
    /**
     *
     * @param nombre
     * nombre
     * @param apellidoPaterno
     * apellidoPaterno
     * @param apellidoMaterno
     * apellidoMaterno
     * @param telefono
     * telefono
     * @param idCliente
     * idCliente
     */
    @Modifying
	@Query("update Cliente c set c.nombre=?1, c.apellidoPaterno=?2, c.apellidoMaterno=?3, c.telefono = ?4  where c.idCliente = ?5")
	void update(String nombre,String apellidoPaterno,String apellidoMaterno, String telefono, BigInteger idCliente);

}
