package com.enermex.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.PromocionCliente;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface PromocionClienteRepository extends JpaRepository<PromocionCliente, Long>{
	
    /**
     *
     * @param idPromocion
     */
    @Query("delete from PromocionCliente pc where pc.idPromocion = ?1")
	@Modifying
	void delete(Long idPromocion);

}
