package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.RutaEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * RutaRepository
 */
@Repository
public interface RutaRepository extends CrudRepository<RutaEntity, Integer> {
  public List<RutaEntity> findAll();
  
    /**
     *
     * @param idRuta
     * @return
     */
    @Query(" Select r from RutaEntity r where r.idRuta = ?1 ")
  public RutaEntity findByIdRuta(Integer idRuta);
  
}