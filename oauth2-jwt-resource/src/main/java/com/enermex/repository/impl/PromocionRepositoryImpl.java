package com.enermex.repository.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.enermex.dto.CodigoPromocionResponseDto;
import com.enermex.dto.HorarioServicioDto;
import com.enermex.dto.ValidaPromoDto;
import com.enermex.modelo.CodigoPromoCliente;
import com.enermex.service.ClientePromocionService;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class PromocionRepositoryImpl {
	
	@PersistenceContext
    private EntityManager em;
	@Autowired
	ClientePromocionService clientePromocionService;
	
	

    /**
     * Valida el codigo de promocion 	
     * @param promo
     * @param idCliente
     * @return
     */
    public CodigoPromocionResponseDto  validaPromo(ValidaPromoDto promo,BigInteger idCliente) {
		     CodigoPromocionResponseDto response;		  
		     CodigoPromoCliente promoCli;
		     Long idPromo=null;
		     StringBuilder sb = new StringBuilder();
		     Boolean esFrecuente;

		     String descuento=null,monto=null;
		     Object[] resultados;
		     SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		     String dateHoy = sf.format(new Date());
             System.out.println(dateHoy);
                        
             
             //Validamos que sea valida en el mes que se quiere aplicar el descuento
		     sb.append(" select descuento,monto,id_promocion from t_promocion p ");
		     sb.append("  where STR_TO_DATE(:today,'%d-%m-%Y %H:%i') ");
		     sb.append(" between fecha_hora_inicio and  fecha_hora_vigencia ");
		     sb.append(" and p.codigo_promocion = :promo ");
		     sb.append(" and NOT EXISTS (Select id_cliente from  t_codigo_promo_cliente cpc ");
		     sb.append(" Where cpc.id_cliente=:idCliente and cpc.id_promocion=p.id_promocion)  ");
		     //Y los planes asociados a la promocion por lo menos un coincida con el de la promocion
		     sb.append(" and EXISTS (SELECT cp.id_plan FROM t_promocion_plan pp,t_cliente_cat_tipo_plan cp ");
		     sb.append("       where  pp.id_plan = cp.id_plan ");
		     sb.append("       and   pp.id_promocion = p.id_promocion ");
		     sb.append("       and   cp.id_cliente = :idCliente) ");
		     
		     //si esFrecuente agregamos
		     esFrecuente = esFrecuente(promo);
		     if(esFrecuente!=null) {
		    	 if(esFrecuente) {
		    	    sb.append(" and EXISTS ( ");
		    		  sb.append(" SELECT count(*) FROM t_promocion_cliente pp ");
		    		  sb.append("  Where  pp.id_cliente = :idCliente and pp.id_promocion=p.id_promocion ) ");
		    	 }
		    	 
		     }
		    	 
		     
		     System.out.println(sb.toString());
	    	Query q = em.createNativeQuery(sb.toString());
	    	  q.setParameter("today", dateHoy);
	    	  q.setParameter("promo", promo.getCodigo());
	    	  q.setParameter("idCliente", idCliente);
	    	  
	    	  List  result = q.getResultList() ;
	    	  
	    	  response = new CodigoPromocionResponseDto();
	    	  //Si tiene resultado entonces si aplica promo
	    	  if(!result.isEmpty()){
	    		// ignores multiple results
	    		  resultados =( Object[]) result.get(0);
	    		  if(resultados[0]!=null) {
	    			  if(!resultados[0].toString().contentEquals("")) {
	    			  response.setDescuento(descuento =  resultados[0].toString());
	    			  response.setEsDescuento(true);
	    			  }
	    		  }
	    		      
	    		  if(resultados[1]!=null) {
	    			  response.setMonto(Double.parseDouble(resultados[1].toString()));
	    			  response.setEsMonto(true);
	    		  }
	    		  
	    		  if(resultados[2]!=null) {
	    			//registramos el uso de la promo
		    		  
		    		  promoCli = new CodigoPromoCliente();
		    		  promoCli.setIdCliente(idCliente);
		    		  promoCli.setIdPromocion(idPromo = new Long(resultados[2].toString()));
		    		  response.setIdPromocion(promoCli.getIdPromocion());
		    		  //clientePromocionService.save(promoCli);
	    		  }
	    		  
	    		  
	    		  response.setMensaje("Código de promoción válido");
	    	  }
	    	  else {
	    		  
	    		  response.setEsInvalido(true);
	    		  response.setMensaje("Código de promoción inválido");
	    	  }
	  
	    	  
	    	return response;
	 }
	 /**
	  * Validamos si hay que considerar solo ha usuarios frecuentes
	  * aquienes se esegmento
	  * @return
	  */
	  private Boolean esFrecuente(ValidaPromoDto promo) {
		  StringBuilder sb = new StringBuilder();
		  SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		  String today = sf.format(new Date());		  
		  Object[] resultados;
		  Boolean esFrecuente=null;
		    sb.append(" select es_frecuente from t_promocion p ");
		     sb.append("  where STR_TO_DATE(:today,'%d-%m-%Y %H:%i') ");
		     sb.append(" between fecha_hora_inicio and  fecha_hora_vigencia ");
		     sb.append(" and p.codigo_promocion = :promo ");
		     
		     Query q = em.createNativeQuery(sb.toString());
		     q.setParameter("promo", promo.getCodigo());
		     q.setParameter("today", today);
		     
		     List  result = q.getResultList() ;
		     
		   //Si tiene resultado entonces si aplica promo
	    	  if(!result.isEmpty()){
	    		// ignores multiple results
	    		  if(result.get(0)!=null) {
	    			  esFrecuente = new Boolean( result.get(0).toString());
	    			  
	    		  }
	    	  }
		  
		  
		  return esFrecuente;
	  }
	  
	  /**
	   * Parametro de cliente frecuente 
	   * @param paramFreq
     * @param planes
	   * @return
	   */
	  public List<BigInteger> getClientesFrecuentes(String paramFreq, List<Integer> planes){
		  StringBuilder sb = new StringBuilder();
		  SimpleDateFormat sf = new SimpleDateFormat("MM/yyyy");
		  String fecha = sf.format(new Date());		  
		  Object[] resultados;
		  List<BigInteger> clienteIds = new ArrayList<BigInteger>();
		  
		  sb.append(" SELECT * FROM ( ");
		      sb.append(" SELECT count(*)as pedidos,p.id_cliente FROM t_pedido p ");
		         sb.append(" where DATE_FORMAT(p.fecha_pedido,'%m/%Y') = :fecha ");
		            sb.append(" and p.estatus_pedido = 4 ");
		            sb.append(" group by p.id_cliente)T1 where pedidos >= :frequente ");
		            sb.append(" and T1.id_cliente  in ( ");
		            sb.append("  Select distinct(cp.id_cliente) from t_cliente_cat_tipo_plan cp where ");
		            sb.append("   cp.id_cliente = T1.id_cliente and cp.id_plan in :planes ) ");
		            
		  
		  Query q = em.createNativeQuery(sb.toString());
		  q.setParameter("fecha", fecha);
		  q.setParameter("frequente", new Integer(paramFreq));
		  q.setParameter("planes", planes);
		  
    	 
		  for(Object o :  q.getResultList()) {
 		     Object[] hr = (Object[] )o; 
 		    clienteIds.add(new BigInteger(hr[1].toString()));
 		     
 	  }
		  
		  return clienteIds;
		  
	  }
	  
	

}
