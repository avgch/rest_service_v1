package com.enermex.repository.impl;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.hibernate.jdbc.Work;
import org.springframework.stereotype.Repository;
import com.enermex.dto.reportes.*;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class PreciosGasolinaHistoricoReporteImpl {
	
	
	@PersistenceContext
    private EntityManager em;
	
	
	/**
	 * Guarda el historico de precios
	 * @param query
	 */
    public void saveHistoricPrices(String query) {
    	
	    	
	   	    Session session = em.unwrap(Session.class);
	   	
			 session.doWork(new Work() {

				@Override

				public void execute(Connection conn) throws SQLException {
					
					PreparedStatement stmt = null;
					// TODO Auto-generated method stub
					stmt = conn.prepareStatement(query);

					
					stmt.executeUpdate(); 
					stmt.close();	 

				}});
			 
    	
    } 
    
    /**
     *
     * @param fechaInc
     * @param fechaFin
     * @return
     */
    public int numeroRows(String fechaInc, String fechaFin) {
         int numRows=0;
         BigInteger numRows2;
    	 StringBuilder sb = new StringBuilder();
    	 PreparedStatement sentencia=null;
 	     ResultSet  rs ;
		 Session session = em.unwrap(Session.class);
		 SessionImpl sessionImpl = (SessionImpl) session;
		 
		 Connection con =sessionImpl.connection();
    
    	 List<ReporteHistoricoDto> reporte = new ArrayList<ReporteHistoricoDto>();
    	 sb.append(" Select COUNT(*) from ");
	        sb.append("  ( ");
	        sb.append("   select (@rn := @rn + 1) as IDSEQ, T3.*   ");
	        sb.append("    FROM ( ");
	        sb.append("  SELECT * FROM ");
	        sb.append("   (SELECT * FROM ");
	        sb.append("    (SELECT g.id as ID, g.id_estado as ID_ESTADO, e.nombre as ESTADO ,g.id_municipio as ID_MUNICIPIO,   ");
	        sb.append("      m.nombre as MUNICIPIO, c.id_codigo as CP , g.combustible as COMBUSTIBLE,g.promedio as PROMEDIO,  ");
	        sb.append("       g.fecha_creacion as FECHA ");
	        sb.append("        from  t_gasolina_estado_municipio_historico g FORCE INDEX (t_gasolina_estado_municipio_historico_indx),  ");
	        sb.append("         t_cat_codigo c FORCE INDEX (t_cat_codigo_indx),t_cat_estado e FORCE INDEX (t_cat_estado_indx),  ");
	        sb.append("          t_cat_municipio m  FORCE INDEX(t_cat_municipio_indx)  ");
	        sb.append("           where    g.id_municipio = c.id_municipio ");
	        sb.append("            and    g.id_estado = c.id_estado   ");
		    sb.append("            and    g.id_estado = e.id_estado   ");
		    sb.append("            and    g.id_municipio = m.id_municipio  ");
	        sb.append("            and    e.id_estado    = m.id_estado  ");
	        sb.append("           )T1)T2)T3  join  ");
	        sb.append("          (select @rn := 0) vars  ");
	        sb.append("          )PRECIOS_H  ");
		 sb.append(" Where  date_format(FECHA,'%d/%m/%Y') ");
		 sb.append("   BETWEEN ");
		 sb.append(" '"+fechaInc+"'" );
		 sb.append("   AND ");
		 sb.append(" '"+fechaFin+"' ");
	     System.out.println(sb.toString());
		 try {
			 sentencia = con.prepareStatement(sb.toString());
			  rs=sentencia.executeQuery();
			  while(rs.next()) {
				  numRows2 = new BigInteger(String.valueOf(rs.getInt(1)));
				  numRows = Integer.parseInt((numRows2.toString()));
			  }
			  
			  
		   } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
		  
		 return numRows;
    	
    }
    /**
     * Crea tabla temporal para realizar los reportes
     */
    public void creaTablaTemporalParaReporte() {
    	
      	StringBuilder sb = new StringBuilder();
   	    Session session = em.unwrap(Session.class);
   	 
		 sb.append(" CREATE  TABLE t_gasolina_estado_municipio_historico_temp as  ");
		   sb.append("   select (@rn := @rn + 1) as IDSEQ, T3.* ");
          sb.append("    FROM ( ");
		     sb.append("   SELECT * FROM  ");
		     sb.append("     (SELECT * FROM ");
		          sb.append(" (SELECT g.id as ID, g.id_estado as ID_ESTADO, e.nombre as ESTADO ,g.id_municipio as ID_MUNICIPIO, ");
		          sb.append("   m.nombre as MUNICIPIO, c.id_codigo as CP , g.combustible as COMBUSTIBLE,g.promedio as PROMEDIO, ");
		            sb.append("  g.fecha_registro as FECHA    from  t_gasolina_estado_municipio_historico g FORCE INDEX (t_gasolina_estado_municipio_historico_indx), ");
		          sb.append("    t_cat_codigo c FORCE INDEX (t_cat_codigo_indx),t_cat_estado e FORCE INDEX (t_cat_estado_indx), ");
		          sb.append("    t_cat_municipio m  FORCE INDEX(t_cat_municipio_indx)  ");
		          sb.append("  where    g.id_municipio = c.id_municipio ");
		          sb.append("    and    g.id_estado = c.id_estado ");
		          sb.append("    and    g.id_estado = e.id_estado ");
		          sb.append("    and    g.id_municipio = m.id_municipio  )T1)T2)T3  join ");
		          sb.append("    (select @rn := 0) vars; ");
		          		       
//		          
//		          sb.append(" commit;");

		  
		   System.out.println(sb.toString());

		 session.doWork(new Work() {

			@Override

			public void execute(Connection conn) throws SQLException {
				
				PreparedStatement stmt = null;
				// TODO Auto-generated method stub
				stmt = conn.prepareStatement(sb.toString());

				
				stmt.executeUpdate(); 
				stmt.close();	 

			}});
		 
    }
    
    /**
     * Crea tabla temporal para realizar los reportes
     */
    public void creaIndexTemporalParaReporte() {
    	
      	StringBuilder sb = new StringBuilder();
   	    Session session = em.unwrap(Session.class);
   	 
   	     /** CREAMOS EL INDICE DE LA TABLA **/
        sb.append(" ALTER TABLE t_gasolina_estado_municipio_historico_temp ");
        sb.append(" ADD INDEX t_gasolina_estado_municipio_historico_temp_indx ");
        sb.append(" (ID,ESTADO,MUNICIPIO,CP,COMBUSTIBLE,PROMEDIO,FECHA); ");
		  
		   System.out.println(sb.toString());

		 session.doWork(new Work() {

			@Override

			public void execute(Connection conn) throws SQLException {
				
				PreparedStatement stmt = null;
				// TODO Auto-generated method stub
				stmt = conn.prepareStatement(sb.toString());

				
				stmt.executeUpdate(); 
				stmt.close();	 

			}});
		 
    }
    /**
     * Elimina tabla temporal para realizar los reportes
     */
    public void dropTablaTemporalParaReporte() {
    	
      	StringBuilder sb = new StringBuilder();
   	    Session session = em.unwrap(Session.class);
   	 
		 sb.append(" DROP TABLE IF EXISTS t_gasolina_estado_municipio_historico_temp ");

		  
		   System.out.println(sb.toString());

		 session.doWork(new Work() {

			@Override

			public void execute(Connection conn) throws SQLException {
				PreparedStatement stmt = null;
				// TODO Auto-generated method stub
				stmt = conn.prepareStatement(sb.toString());

				
				stmt.execute(); 
				stmt.close();	 

			}});
     }
    /**
     * 
     * @param fechaInc
     * @param fechaFin
     * @param startPos
     * @param endPos
     * @return
     */
      public List<ReporteHistoricoDto> consultaReporte(String fechaInc,String fechaFin, double startPos,double endPos){
    	  
    	  PreparedStatement sentencia=null;
    	   ResultSet  rs ;
			Session session = em.unwrap(Session.class);
			SessionImpl sessionImpl = (SessionImpl) session;
			Statement stmt = null;
			Connection con =sessionImpl.connection();
	
    	  //select count(*) from t_gasolina_estado_municipio_historico_temp
    	  //where date_format(FECHA,'%m/%Y') between '04/2020' AND '04/2020'
         StringBuilder sb = new StringBuilder();
     	 List<ReporteHistoricoDto> reporte = new ArrayList<ReporteHistoricoDto>();
     	sb.append(" Select ID,ESTADO,MUNICIPIO,CP,COMBUSTIBLE,PROMEDIO,FECHA from ");
	        sb.append("  ( ");
	        sb.append("   select (@rn := @rn + 1) as IDSEQ, T3.*   ");
	        sb.append("    FROM ( ");
	        sb.append("  SELECT * FROM ");
	        sb.append("   (SELECT * FROM ");
	        sb.append("    (SELECT g.id as ID, g.id_estado as ID_ESTADO, e.nombre as ESTADO ,g.id_municipio as ID_MUNICIPIO,   ");
	        sb.append("      m.nombre as MUNICIPIO, c.id_codigo as CP , g.combustible as COMBUSTIBLE,g.promedio as PROMEDIO,  ");
	        sb.append("       g.fecha_creacion as FECHA ");
	        sb.append("        from  t_gasolina_estado_municipio_historico g FORCE INDEX (t_gasolina_estado_municipio_historico_indx),  ");
	        sb.append("         t_cat_codigo c FORCE INDEX (t_cat_codigo_indx),t_cat_estado e FORCE INDEX (t_cat_estado_indx),  ");
	        sb.append("          t_cat_municipio m  FORCE INDEX(t_cat_municipio_indx)  ");
	        sb.append("           where    g.id_municipio = c.id_municipio ");
	        sb.append("            and    g.id_estado = c.id_estado   ");
		    sb.append("            and    g.id_estado = e.id_estado   ");
		    sb.append("            and    g.id_municipio = m.id_municipio  ");
	        sb.append("            and    e.id_estado    = m.id_estado  order by FECHA desc,ESTADO asc,MUNICIPIO asc ");
	        sb.append("           )T1)T2)T3  join  ");
	        sb.append("          (select @rn := 0) vars  ");
	        sb.append("          )PRECIOS_H  ");
 		 sb.append(" Where  date_format(FECHA,'%d/%m/%Y') ");
 		 sb.append("   BETWEEN ");
 		 sb.append(" '"+fechaInc+"' " );
 		 sb.append("   AND ");
 		 sb.append(" '"+fechaFin+"' ");
 		 sb.append("    AND  ");
 		sb.append(" IDSEQ >= "+startPos);
 		sb.append("    AND  ");
 		sb.append(" IDSEQ < "+endPos);
 		sb.append(" ");
 	     
 		  System.out.println(sb.toString());
		  try {
			  sentencia= con.prepareStatement(sb.toString());
			  sentencia.setFetchSize(500);
			  rs= sentencia.executeQuery();
			  
     	 
		  while(rs.next()) {
			  String fechaRegistro;
	 		    ReporteHistoricoDto r = new ReporteHistoricoDto();
	 		    
	 		    r.setId(new BigInteger(String.valueOf(rs.getInt(1))));
	 		    r.setEstado(rs.getString((2)));
	 		    r.setAlcaldia(rs.getString((3)));
	 		    r.setCp(rs.getString((4)));
	 		    r.setIdCombustible(rs.getInt((5)));
	 		    r.setPromedio(rs.getDouble(6));
	 		    r.setFechaRegistro(rs.getString(7));
	 		    
 		    
 		    
 		   reporte.add(r);
 	      }
		 }
		 catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
 		  
    	  return reporte;
      }
      /**
       * 
       * @param fechaInc
       * @param fechaFin
       * @param startPos
       * @param endPos
       * @return
       */
        public List<ReporteHistoricoDto> consultaReporteSinBloque(String fechaInc,String fechaFin){
      	  
        	  PreparedStatement sentencia=null;
       	   ResultSet  rs ;
   			Session session = em.unwrap(Session.class);
   			SessionImpl sessionImpl = (SessionImpl) session;
   			Statement stmt = null;
   			Connection con =sessionImpl.connection();
   	
       	  //select count(*) from t_gasolina_estado_municipio_historico_temp
       	  //where date_format(FECHA,'%m/%Y') between '04/2020' AND '04/2020'
            StringBuilder sb = new StringBuilder();
        	List<ReporteHistoricoDto> reporte = new ArrayList<ReporteHistoricoDto>();
    	 sb.append(" Select ID,ESTADO,MUNICIPIO,CP,COMBUSTIBLE,PROMEDIO,FECHA from ");
	        sb.append("  ( ");
	        sb.append("   select (@rn := @rn + 1) as IDSEQ, T3.*   ");
	        sb.append("    FROM ( ");
	        sb.append("  SELECT * FROM ");
	        sb.append("   (SELECT * FROM ");
	        sb.append("    (SELECT g.id as ID, g.id_estado as ID_ESTADO, e.nombre as ESTADO ,g.id_municipio as ID_MUNICIPIO,   ");
	        sb.append("      m.nombre as MUNICIPIO, c.id_codigo as CP , g.combustible as COMBUSTIBLE,g.promedio as PROMEDIO,  ");
	        sb.append("       g.fecha_creacion as FECHA ");
	        sb.append("        from  t_gasolina_estado_municipio_historico g FORCE INDEX (t_gasolina_estado_municipio_historico_indx),  ");
	        sb.append("         t_cat_codigo c FORCE INDEX (t_cat_codigo_indx),t_cat_estado e FORCE INDEX (t_cat_estado_indx),  ");
	        sb.append("          t_cat_municipio m  FORCE INDEX(t_cat_municipio_indx)  ");
	        sb.append("           where    g.id_municipio = c.id_municipio ");
	        sb.append("            and    g.id_estado = c.id_estado   ");
		    sb.append("            and    g.id_estado = e.id_estado   ");
		    sb.append("            and    g.id_municipio = m.id_municipio  ");
	        sb.append("            and    e.id_estado    = m.id_estado  order by FECHA asc,ESTADO desc,MUNICIPIO asc ");
	        sb.append("           )T1)T2)T3  join  ");
	        sb.append("          (select @rn := 0) vars  ");
	        sb.append("          )PRECIOS_H  ");
   		 sb.append(" Where  date_format(FECHA,'%d/%m/%Y') ");
   		 sb.append("   BETWEEN ");
   		 sb.append(" '"+fechaInc+"' " );
   		 sb.append("   AND ");
   		 sb.append(" '"+fechaFin+"'  ");
   	     
   	  System.out.println(sb.toString());
	  try {
		  sentencia= con.prepareStatement(sb.toString());
		  sentencia.setFetchSize(500);
		  rs= sentencia.executeQuery();
		  
 	 
	  while(rs.next()) {
		  String fechaRegistro;
 		    ReporteHistoricoDto r = new ReporteHistoricoDto();
 		    
 		    r.setId(new BigInteger(String.valueOf(rs.getInt(1))));
 		    r.setEstado(rs.getString((2)));
 		    r.setAlcaldia(rs.getString((3)));
 		    r.setCp(rs.getString((4)));
 		    r.setIdCombustible(rs.getInt((5)));
 		    r.setPromedio(rs.getDouble(6));
            r.setFechaRegistro(rs.getString(7));
		    
		    
		   reporte.add(r);
	      }
	 }
	 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
		  
	  return reporte;
        }
    
    
    

}
