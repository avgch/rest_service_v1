package com.enermex.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class CodigoPostalRepositoryImpl {
	
	
    @PersistenceContext
    private EntityManager em;
    
    /**
     * 
     * @param cpsP
     * @return
     */
    public List<String> notificarA(List<Integer> idMunicipio) {
    	Query q = em.createNativeQuery("select id_codigo from t_cat_codigo where id_municipio in :idMunicipio");
    	  q.setParameter("idMunicipio", idMunicipio);
    	  List<String> cps =  q.getResultList();
    	return cps;
    	
    }
    
    /**
     * 
     * @param cp
     * @return
     */
    public String getMunicipioByCp(String cp) {
    	
    	StringBuilder sb = new StringBuilder();
    	sb.append(" SELECT m.nombre FROM  t_cat_codigo c,t_cat_municipio m ");
    	  sb.append("  where ");
    	sb.append("     c.id_municipio = m.id_municipio  ");
    	sb.append("     and c.id_estado = m.id_estado  ");
    	sb.append("     and c.id_codigo like :cp  ");
   
    	  
    	 
    	 ;
    	
    	Query q = em.createNativeQuery(sb.toString());
  	    q.setParameter("cp", cp);
  	    String alcMunc =  (String) q.getSingleResult();
    	
    	return alcMunc;
    }
    

}
