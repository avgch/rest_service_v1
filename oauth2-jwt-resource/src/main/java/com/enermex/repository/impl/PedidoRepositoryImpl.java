package com.enermex.repository.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.enermex.dto.DisponibilidadPedidoDto;
import com.enermex.dto.HorarioServicioDto;
import com.enermex.dto.PedidoDto;
import com.enermex.dto.PedidoTicketRequestDto;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.UsuarioEntity;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class PedidoRepositoryImpl {

	@PersistenceContext
    private EntityManager em;
	
	/**
	 * Obtiene la disponibilidad del dia
	 * @param fecha
     * @param despachadores
	 * @return
	 */
	  public List<HorarioServicioDto> getOcupabilidad(String fecha, List<UsuarioEntity> despachadores) {
		     StringBuilder sb =null;
		     List<HorarioServicioDto> horarios= new ArrayList<>();
		     SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		     SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
		     Date today = new Date();
		     String timeQuery = sdf.format(today);
		     String todayS    = sdf2.format(today);
		     

             //a Por cada despachador buscamos su disponibilidad
		     
		     for(UsuarioEntity desp : despachadores) {
		    	 sb = new StringBuilder();
		     if(fecha.isEmpty()) {
		    	 
	    	  
	    	   sb.append(" select id_horario_servicio,DATE_FORMAT(hora_inicio,'%H:%i'),DATE_FORMAT(hora_fin,'%H:%i') from t_horario_servicio hs ");
			   sb.append("  where id_horario_servicio not in( select ");
			   sb.append(" COALESCE(id_horario_servicio,0)as id_horario_servicio  from t_pedido p where DATE(fecha_pedido) = STR_TO_DATE(:fecha,'%d-%m-%Y') ");
			   sb.append("  and  p.id_despachador  = :idDespachador ");
			   sb.append("  and  p.id_pipa         = :idPipa  and p.estatus_pedido !=5 )");
			   sb.append("  and DATE_FORMAT(hora_inicio,'%H:%i') >=  STR_TO_DATE(:timeQuery,'%H:%i')  order by id_horario_servicio asc");
	    	 
		     }else {
			     sb.append(" select id_horario_servicio,DATE_FORMAT(hora_inicio,'%H:%i'),DATE_FORMAT(hora_fin,'%H:%i') from t_horario_servicio hs ");
			     sb.append("  where id_horario_servicio not in( select ");
			     sb.append(" COALESCE(id_horario_servicio,0)as id_horario_servicio  from t_pedido p where DATE(fecha_pedido) = STR_TO_DATE(:fecha,'%d-%m-%Y') ");
			     sb.append("  and  p.id_despachador  = :idDespachador");
				 sb.append("  and  p.id_pipa         = :idPipa and p.estatus_pedido !=5 ) order by id_horario_servicio asc");
		     }
		     
		      Query q = em.createNativeQuery(sb.toString());
		      if(fecha.isEmpty())
	    	    q.setParameter("fecha", todayS);
		      else
		    	q.setParameter("fecha", fecha);
		      
	    	  q.setParameter("idDespachador", desp.getIdUsuario());
	    	  q.setParameter("idPipa", desp.getIdPipa());
	    	  
	    	  if(fecha.isEmpty())
	    	  q.setParameter("timeQuery", timeQuery);
	    	  
	    	  for(Object o :  q.getResultList()) {
	    		     Object[] hr = (Object[] )o; 
	    		     HorarioServicioDto  hrDto = new HorarioServicioDto();
	    		     hrDto.setIdHorario((int)hr[0]);
	    		     hrDto.setFrom(hr[1].toString());
	    		     hrDto.setTo(hr[2].toString());
	    		     System.out.println(hr[1].toString()+"-"+hr[2].toString());
	    		     //buscamos si ya existe idHorario a horarios
	    		     HorarioServicioDto haux =horarios.stream().filter(horario -> horario.getIdHorario().compareTo(hrDto.getIdHorario())==0).
	    		    		 findAny().orElse(null);
	    		     
	    		     if(haux==null)
	    		    	 horarios.add(hrDto);
	    		     
	    	  }
		     }
	    	  //a end for por cada despahcador
	    	  
		     
		     Comparator<HorarioServicioDto> compareById = (HorarioServicioDto o1, HorarioServicioDto o2) -> o1.getIdHorario().compareTo( o2.getIdHorario() );
		     
		     Collections.sort(horarios, compareById);
		     
		     return horarios;
	 }
	  
	  /**
		 * Obtiene la disponibilidad del dia
		 * @param fecha
     * @param despachadores
		 * @return
		 */
		  public List<DisponibilidadPedidoDto> getOcupabilidadCierraPedido(String fecha, List<UsuarioEntity> despachadores) {
			     StringBuilder sb;
			     List<DisponibilidadPedidoDto> horarios= new ArrayList<>();
			     
	             //a Por cada despachador buscamos su disponibilidad
			     
			     for(UsuarioEntity desp : despachadores) {
			
			    	 sb = new StringBuilder();
				     sb.append(" select id_horario_servicio from t_horario_servicio hs ");
				     sb.append("  where id_horario_servicio not in( select ");
				     sb.append(" COALESCE(id_horario_servicio,0)as id_horario_servicio  from t_pedido p where DATE(fecha_pedido) = STR_TO_DATE(:fecha,'%d-%m-%Y') ");
				     sb.append("  and  p.id_despachador  = :idDespachador");
					 sb.append("  and  p.id_pipa         = :idPipa and estatus_pedido!=5) order by id_horario_servicio asc");
			     
			     
			      Query q = em.createNativeQuery(sb.toString());
			      q.setParameter("fecha", fecha);
			      
		    	  q.setParameter("idDespachador", desp.getIdUsuario());
		    	  q.setParameter("idPipa", desp.getIdPipa());
		    	  
		    	  
		    	  for(Object o :  q.getResultList()) {
		    		     Object hr = (Object)o; 
		    		     DisponibilidadPedidoDto  hrDto = new DisponibilidadPedidoDto();
		    		     hrDto.setIdHorario(new BigInteger(hr.toString()));
		    		     hrDto.setIdDespachador(desp.getIdUsuario());
		    		     hrDto.setIdPipa(desp.getIdPipa());
		    		     //buscamos si ya existe idHorario a horarios
		    		     DisponibilidadPedidoDto haux =horarios.stream().filter(horario -> horario.getIdHorario().compareTo(hrDto.getIdHorario())==0).findAny().orElse(null);
		    		     
		    		     if(haux==null)
		    		    	 horarios.add(hrDto);
		    		     
		    	  }
			     }
		    	  //a end for por cada despahcador
		    	  
			     
			     return horarios;
		 }
	
	  /**
	   * OBtiene del id de servicio a utilizar
	   * @param fechaHoraP
	   * @return
	   */
	  public BigInteger getIdHorarioServicio(String fechaHoraP) {
		  StringBuilder sb = new StringBuilder();
		  Object idHrServicio;
		  String[] fechaHora  =  fechaHoraP.split(" ");
		  String[] horaIncFin =  fechaHora[1].split("-");
		  
		  sb.append(" select id_horario_servicio from t_horario_servicio hs ");
		     sb.append("  where hs.hora_inicio = STR_TO_DATE(:horaInc,'%H:%i') ");
		     sb.append("  and   hs.hora_fin     = STR_TO_DATE(:horaFin,'%H:%i') ");
		     
	    	Query q = em.createNativeQuery(sb.toString());
	    	  q.setParameter("horaInc", horaIncFin[0].toString());
	    	  q.setParameter("horaFin", horaIncFin[1].toString());
	    	  idHrServicio =  q.getSingleResult();
	    	  
	    	return new BigInteger(idHrServicio.toString());
		  
	  }
	  
	  /**
	   * Obtenemos hr del tipo de pedido que es
	   * @return
	   */
	  public String getTipoPedido() {
		  
		  
		  
		  return null;
		  
	  }
	  
	  /**
	   * 
	   * @param idPedido
	   * @param fechaPedido
	   */
	  public void updateFechaPedido(BigInteger idPedido, String fechaPedido) {
		 StringBuilder sb = new StringBuilder();
		  
		 String [] dateA= fechaPedido.split("-");
		 
		  //para la hora actual
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
		  TimeZone cdmx = TimeZone.getTimeZone("Mexico/General");
		  sdf.setTimeZone(cdmx);
		  
		  String dateHoy= sdf.format(new Date());
		  
		  
	     sb.append(" update t_pedido set fecha_creacion = STR_TO_DATE(:today,'%Y-%m-%d %H:%i'), fecha_pedido=STR_TO_DATE(:datePedido,'%d/%m/%Y %H:%i') ");
	     sb.append(" where  id_pedido=:idPedido  ");
	     
    	 Query q = em.createNativeQuery(sb.toString());
    	  q.setParameter("today", dateHoy);
    	  q.setParameter("datePedido", dateA[0].toString());
    	  q.setParameter("idPedido", idPedido);
    	  q.executeUpdate();
		    
	  }
	  
	  /**
	   * Valida si tiene un pedido en el mismo dia
     * @param idHorarioServicio
     * @param pedido
	   * @return
	   */
	  public BigInteger existeDuplicidadPedidoCliente(BigInteger idHorarioServicio,PedidoDto pedido){
		  
		  StringBuilder sb = new StringBuilder();
		  Object masDeUnServ;
		  String[] fechaHora  =  pedido.getFechaPedidoS().split(" ");
		  
		  System.out.println(pedido.getIdCliente());
		  System.out.println(pedido.getIdAutomovil());
		  System.out.println(idHorarioServicio);
		  
		  sb.append(" select count(*) from t_pedido ");
		     sb.append(" where id_cliente = :idCliente ");
		     sb.append(" and id_horario_servicio = :idHorario ");
		     sb.append(" and id_automovil = :idAutomovil ");
		     sb.append(" and DATE_FORMAT(fecha_pedido,'%d/%m/%Y') = '"+fechaHora[0].toString()+"' ");
		     //update 07052020 Abraham Vargas
		     sb.append(" and estatus_pedido!=5 ");
		     
	    	Query q = em.createNativeQuery(sb.toString());
	    	  q.setParameter("idCliente", pedido.getIdCliente());
	    	  q.setParameter("idAutomovil", pedido.getIdAutomovil());
	    	  q.setParameter("idHorario", idHorarioServicio);

	    	  
	    	  masDeUnServ =  q.getSingleResult();
	    	  
	    	return new BigInteger(masDeUnServ.toString());
		  
	  }
	  
	  /**
	   * Valida si tiene un pedido en el mismo dia
     * @param idHorarioServicio
     * @param ticket
	   * @return
	   */
	  public BigInteger existeDuplicidadPedidoCliente(BigInteger idHorarioServicio,PedidoTicketRequestDto ticket){
		  
		  StringBuilder sb = new StringBuilder();
		  Object masDeUnServ;
		  String[] fechaHora  =  ticket.getFechaPedido().split(" ");
		  
		  sb.append(" select count(*) from t_pedido ");
		     sb.append("  where id_cliente        = :idCliente   ");
		     sb.append("  and id_horario_servicio = :idHorario   ");
		     sb.append("  and id_automovil        = :idAutomovil   ");
		     sb.append("  and DATE(fecha_pedido) = STR_TO_DATE(:fehaPedido,'%d/%m/%Y') ");
		     sb.append(" and estatus_pedido!=5 ");
		     
	    	Query q = em.createNativeQuery(sb.toString());
	    	  q.setParameter("idCliente", ticket.getIdCliente());
	    	  q.setParameter("idAutomovil", ticket.getIdAutomovil());
	    	  q.setParameter("idHorario", idHorarioServicio);
	    	  q.setParameter("fehaPedido", fechaHora[0].toString());
	    	  
	    	  masDeUnServ =  q.getSingleResult();
	    	  
	    	return new BigInteger(masDeUnServ.toString());
		  
	  }

	
	
}
