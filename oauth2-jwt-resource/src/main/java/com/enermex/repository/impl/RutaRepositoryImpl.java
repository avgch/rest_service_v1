package com.enermex.repository.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.enermex.modelo.RutaEntity;
import com.enermex.repository.RutaRepository;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class RutaRepositoryImpl  {

	   
    @PersistenceContext
    private EntityManager em;
    
    /**
     *
     * @param cp
     * @return
     */
    public Integer validaCobertura(String cp) {
    	Query q = em.createNativeQuery("SELECT COUNT(*) FROM t_ruta_codigo rc,t_ruta r where id_codigo = ? and rc.id_ruta =r.id_ruta and r.estatus = 'A';");
    	  q.setParameter(1, cp);
    	  String hayCobertura =  q.getSingleResult().toString();
    	  
    	  Integer cobertura = new Integer(hayCobertura);
    	  
    	return cobertura;
    	
    }


	
}
