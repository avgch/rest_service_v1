package com.enermex.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.enermex.dto.PrecioGasolinaRequestDto;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class AlcaldiaEstadoRepositoryImpl {
	
	@PersistenceContext
	EntityManager  em ;
	
	/**
	 * 
	 * @param cp
	 * @return
	 */
	public PrecioGasolinaRequestDto getAlcEdoByCp(String cp) {
		
     PrecioGasolinaRequestDto request=null ;
	  boolean encontroCp=false;
	 StringBuilder sb = new StringBuilder();
			  
			   sb.append(" select m.id_estado,m.id_municipio from t_cat_municipio m,t_cat_estado e,t_cat_codigo c where  ");
			   sb.append("  m.id_estado=e.id_estado ");
			   sb.append("  and m.id_municipio = c.id_municipio  ");
			   sb.append("  and c.id_estado    = e.id_estado  ");
			   sb.append(" and c.id_codigo = :cp  ");

		      Query q = em.createNativeQuery(sb.toString());
		      q.setParameter("cp", cp);

			  for(Object o :  q.getResultList()) {				  
				     Object[] hr = (Object[] )o; 
				     request = new PrecioGasolinaRequestDto();
				     encontroCp =true;
				     request.setIdEstado((int)hr[0]);
				     request.setIdMunicipio((int)hr[1]);	
				     
			  }
			  //workaround
			  if(encontroCp==false) {
				  request = new PrecioGasolinaRequestDto();
				  request.setIdEstado(9);
				  request.setIdMunicipio(5);	
			  }
			  
			  
		      
		      return request;
	     }
	
	
	}
	
