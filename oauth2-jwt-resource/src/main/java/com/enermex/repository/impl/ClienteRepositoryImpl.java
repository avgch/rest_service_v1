package com.enermex.repository.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.enermex.dto.ClienteWebDto;
import com.enermex.dto.HorarioServicioDto;
import com.enermex.modelo.CodigoPostalEntity;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class ClienteRepositoryImpl {

	 @PersistenceContext
	 EntityManager  em ;
	 
	 
	 
	 /**
	  * Obtiene clientes ids a notificar app cliente con base al cp participante
	  * @param cpsNotify
	  * @return
	  */
	 public List<BigInteger> getAllClinetesNotifyNoti(List<String> cpsNotify) {
		 
		 StringBuilder sb = new StringBuilder();
				 
	      sb.append(" SELECT distinct(T.id_cliente) FROM (SELECT  ");
	      sb.append("   tf.fcm AS token,tc.id_cliente AS id_cliente,td.cp AS cp_cliente  ");
	      sb.append("  FROM ");
	      sb.append("    t_cliente tc JOIN t_direccion_cliente td JOIN t_cliente_fcm tf  ");
	      sb.append("  WHERE ");
	      sb.append("    tc.id_cliente = td.id_cliente ");
	      sb.append("     AND tf.id_cliente = tc.id_cliente ");
	      sb.append("     AND tf.id_cliente = td.id_cliente ) T ");
	      sb.append("    WHERE T.cp_cliente  in  :cpsNotify ");
		 
		 Query q = em.createNativeQuery(sb.toString());
   	     q.setParameter("cpsNotify", cpsNotify);
	   	 List<BigInteger> clientesId =  q.getResultList();
	   	return clientesId;
  
	 }
	 /**
	  * Obtiene clientes ids a notificar app cliente con base al cp participante
	  * @param cpsNotify
	  * @return
	  */
	 public List<BigInteger> getAllClinetesNotify(List<CodigoPostalEntity> cpsNotify) {
		 
		 StringBuilder sb = new StringBuilder();
		 
		 List<String> cps = new ArrayList<>();
		 for(CodigoPostalEntity c: cpsNotify ) {
			 cps.add(c.getIdCodigo());
			 
		 }
		 
		 
		 
	      sb.append(" SELECT distinct(T.id_cliente) FROM (SELECT  ");
	      sb.append("   tf.fcm AS token,tc.id_cliente AS id_cliente,td.cp AS cp_cliente  ");
	      sb.append("  FROM ");
	      sb.append("    t_cliente tc JOIN t_direccion_cliente td JOIN t_cliente_fcm tf  ");
	      sb.append("  WHERE ");
	      sb.append("    tc.id_cliente = td.id_cliente ");
	      sb.append("     AND tf.id_cliente = tc.id_cliente ");
	      sb.append("     AND tf.id_cliente = td.id_cliente ) T ");
	      sb.append("    WHERE T.cp_cliente  in  :cpsNotify ");
		 
		 Query q = em.createNativeQuery(sb.toString());
   	     q.setParameter("cpsNotify", cps);
	   	 List<BigInteger> clientesId =  q.getResultList();
	   	return clientesId;
  
	 }
	 
	 
	 /**
	  * Obtiene tokens a notificar app cliente con base al cp participante
	  * @param cpsNotify
	  * @return
	  */
	 public List<String> getAllTokensNotifyNoti(List<String> cpsNotify) {
		 
		 
		 
	      StringBuilder sb = new StringBuilder();  
	      sb.append(" SELECT distinct(T.token)  FROM (SELECT  ");
	      sb.append("   tf.fcm AS token,tc.id_cliente AS id_cliente,td.cp AS cp_cliente  ");
	      sb.append("  FROM ");
	      sb.append("    t_cliente tc JOIN t_direccion_cliente td JOIN t_cliente_fcm tf  ");
	      sb.append("  WHERE ");
	      sb.append("    tc.id_cliente = td.id_cliente ");
	      sb.append("     AND tf.id_cliente = tc.id_cliente ");
	      sb.append("     AND tf.id_cliente = td.id_cliente ) T ");
	      sb.append("    WHERE T.cp_cliente  in  :cpsNotify ");
	        
		 Query q = em.createNativeQuery(sb.toString());
   	     q.setParameter("cpsNotify", cpsNotify);
	   	 List<String> tokens =  q.getResultList();
	   	return tokens;
  
	 }
	 
	 /**
	  * Obtiene tokens a notificar app cliente con base al cp participante
	  * @param cpsNotify
	  * @return
	  */
	 public List<String> getAllTokensNotify(List<CodigoPostalEntity> cpsNotify) {
		 
		 List<String> cps = new ArrayList<>();
		 for(CodigoPostalEntity c: cpsNotify ) {
			 cps.add(c.getIdCodigo());
			 
		 }
		 
		 
		 
	      StringBuilder sb = new StringBuilder();  
	      sb.append(" SELECT distinct(T.token) FROM (SELECT  ");
	      sb.append("   tf.fcm AS token,tc.id_cliente AS id_cliente,td.cp AS cp_cliente  ");
	      sb.append("  FROM ");
	      sb.append("    t_cliente tc JOIN t_direccion_cliente td JOIN t_cliente_fcm tf  ");
	      sb.append("  WHERE ");
	      sb.append("    tc.id_cliente = td.id_cliente ");
	      sb.append("     AND tf.id_cliente = tc.id_cliente ");
	      sb.append("     AND tf.id_cliente = td.id_cliente  ) T ");
	      sb.append("    WHERE T.cp_cliente  in  :cpsNotify ");
	        
		 Query q = em.createNativeQuery(sb.toString());
   	     q.setParameter("cpsNotify", cps);
	   	 List<String> tokens =  q.getResultList();
	   	return tokens;
  
	 }
	 
	 /**
	  * Notifica a todos los usuarios activos de la app
	  * @return
	  */
	  public List<String> notificaClientesTodos(){
	    	StringBuilder sb = new StringBuilder();
	    	sb.append(" select fcm from t_cliente tc,t_cliente_fcm tf ");
	    	sb.append(" where tc.id_cliente = tf.id_cliente ");
	    	sb.append(" and tc.bloqueo = false  ");
	    	sb.append(" and tc.acceso = true ");
	    	
	    	
	    	Query q = em.createNativeQuery(sb.toString());
	    	  List<String> tokens =  q.getResultList();
	    	return tokens;
	    	
	    }
	  
		 /**
		  * Get nombre del cliente
		  * @param idCliente
		  * @return
		  */
		 public String geNombreCliente(BigInteger idCliente) {
			 

		    	Query q = em.createNativeQuery("SELECT CONCAT(nombre,' ',IFNULL(apellido_paterno,''),' ', IFNULL(apellido_materno,'')) AS fullname  from  t_cliente where id_cliente = :idCliente");
		    	q.setParameter("idCliente", idCliente);
		    	String nombreCliente = q.getSingleResult().toString();
		    	
		    return nombreCliente;
		 } 
		 
		 /**
		  * Elimina los tokens del cliente
		  * @param idCliente
		  */
		 public void deleteTokenCliente(BigInteger idCliente) {
			 
				Query q = em.createNativeQuery("delete  from  t_token where id_cliente = :idCliente");
		    	q.setParameter("idCliente", idCliente);
		        q.executeUpdate();
		 }
		 
		 /**
		  * Buscamos si algun invitado tiene pedido en curso
		  * @param ids
     * @param idTitular
     * @param isCuenta
		  * @return
		  */
		 public boolean seEliminaPlan(List<BigInteger> ids,BigInteger idTitular, Boolean isCuenta) {
			 
			 boolean seElimina = true;
			 if(ids!= null && ids.size()> 0){
				 Query q = em.createNativeQuery("Select count(*) from t_pedido p,t_cliente c  where p.id_cliente  in :ids  and"
						 + " c.id_titular = :idTitular and estatus_pedido in(1,2,3) and p.id_cliente=c.id_cliente and c.bloqueo=0");
				 q.setParameter("ids", ids);
				 q.setParameter("idTitular", idTitular);
				 BigInteger eliminaPlan =(BigInteger) q.getSingleResult();
				 seElimina = eliminaPlan.compareTo(new BigInteger("0"))==0;
			 }
		     if(isCuenta){
		    	 Query q2 = em.createNativeQuery("Select count(*) from t_pedido p,t_cliente c where p.id_cliente = :idTitular and"
					 		+ " p.estatus_pedido in(1,2,3) and p.id_cliente=c.id_cliente and c.bloqueo=0");
		    	 q2.setParameter("idTitular", idTitular);
		    	 BigInteger eliminaPlanCuenta = (BigInteger) q2.getSingleResult();
		    	 seElimina = seElimina && eliminaPlanCuenta.compareTo(new BigInteger("0"))==0;
		     }
		     
		    return seElimina;
		 }
		 
		 /**
		  * Elimina el id Titular y resetear banderas de los familiares del cliente
		  * @param ids
		  * @return
		  */
		 public boolean seEliminaTitularFamiliar(List<BigInteger> ids) {
			 if(ids!=null && ids.size()>0)
			 {
				 Query q = em.createNativeQuery("UPDATE t_cliente SET id_titular= NULL, id_estatus= 0, acceso = 0, bloqueo = 0, fecha_baja = :fechaBaja WHERE id_cliente in :ids");
				 q.setParameter("fechaBaja", (new Date()));
				 q.setParameter("ids", ids);
				 int updateFam = q.executeUpdate();
				 return updateFam == ids.size();
			 }
		     
			 return false;
		 }
		 
		 /**
		  * Elimina el relacion entre Titular y familiares
		  * @param ids
		  * @return
		  */
		 public boolean seEliminaRelacionTitularFamiliar(BigInteger ids) {
				 Query q = em.createNativeQuery("DELETE FROM t_cliente_cat_tipo_plan WHERE id_cliente = :ids AND id_plan=2");
				 q.setParameter("ids", ids);
				 int updateFam = q.executeUpdate();
				 return updateFam > 0;
		 }
		 
		 /**
		  * Elimina el id Titular y resetear banderas de los familiares del cliente
		  * @param ids
		  * @return
		  */
		 public boolean seEliminaCuenta(List<BigInteger> ids) {
			 Query q = em.createNativeQuery("UPDATE t_cliente SET id_titular= NULL, id_estatus= 0, acceso = 0, bloqueo = 0, fecha_baja = :fechaBaja WHERE id_cliente in :ids");
			 q.setParameter("fechaBaja", (new Date()));
			 q.setParameter("ids", ids);
			 int updateFam = q.executeUpdate();
		     
			 return updateFam == ids.size();
		 }
		 
		 /**
		  * Buscar cliente que fue dado de baja y se esta registrando
     * @param correoExistente
		  * @param ids
		  * @return
		  */
		 public boolean buscarClienteExistente(String correoExistente) {
			 
			 Query q = em.createNativeQuery("SELECT count(*) FROM t_cliente WHERE correo = :correoExistente AND fecha_baja IS NOT NULL");
			 q.setParameter("correoExistente", correoExistente);
		     BigInteger existe =(BigInteger) q.getSingleResult();
		     
		     return existe.compareTo(new BigInteger("0"))!=0;
		 }
		 
		 /**
		  * Realiza el update de datos personales para usuario que ya existia
     * @param nombre
		  * @param ids
     * @param telefono
     * @param idCliente
     * @param correo
     * @return 
		  * @returnel
		  */
		 public boolean updateCuentaExistente(String nombre, String correo, String telefono, BigInteger idCliente) {
			 Query q = em.createNativeQuery("UPDATE t_cliente SET nombre= :nombre, correo= :correo, telefono = :telefono,  id_estatus= 1, acceso = 1, fecha_baja = NULL WHERE id_cliente = :idCliente");
			 q.setParameter("nombre", nombre);
			 q.setParameter("correo", correo);
			 q.setParameter("telefono", telefono);
			 q.setParameter("idCliente", idCliente);
			 int updateFam = q.executeUpdate();
		     
			 return updateFam > 0;
		 }
		 
    /**
     *
     * @return
     */
    public List<ClienteWebDto> getAllClientes(){
			 StringBuilder sb = new StringBuilder();
			 sb.append(" Select id_cliente,nombre,apellido_paterno,apellido_materno,correo,fecha_creacion,telefono,'Particular',bloqueo ");
			 sb.append(" from t_cliente ");
			 
			 Query q = em.createNativeQuery(sb.toString());
			 
			 List<ClienteWebDto> clientes = new ArrayList<ClienteWebDto>();
			  for(Object o :  q.getResultList()) {
				  ClienteWebDto cli = new ClienteWebDto();
				  String nombre="";
	    		     Object[] hr = (Object[] )o; 
	    		     Date createOn;
	    		     
	    		     cli.setIdCliente(((BigInteger)hr[0]));
	    		     
	    		     nombre +=((String)hr[1])==null ? "" : (String)hr[1];
	    		     nombre +=" "+((hr[2])==null ? "" : (String)hr[2]);
	    		     nombre +=" "+((hr[3])==null ? "" : (String)hr[3]);
	    		     
	    		     cli.setNombreCompleto(nombre);
	    		     if(hr[4]!=null)
	    		    	 
	    		     cli.setEmail(((String)hr[4]));
	    		     if(hr[5]!=null) {
	    		      createOn =  (Date)hr[5];
	    		      cli.getFechaCreacion().setTime(createOn);
	    		     }
	    		     if(hr[6]!=null)
	    		     cli.setTelefonoCelular((String)hr[6]);
	    		     
	    		     if(hr[7]!=null)
	    		     cli.setTipoPlan((String)hr[7]);
	    		     
	    		     if(hr[8]!=null)
	    		     cli.setBloqueo((Boolean)hr[8]);
	    		     
	    		     clientes.add(cli);
	    	  }
			  
			  return clientes;

		 }
		 
}
