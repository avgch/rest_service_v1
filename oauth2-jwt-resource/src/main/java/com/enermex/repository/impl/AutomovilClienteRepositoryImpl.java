package com.enermex.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.enermex.modelo.AutomovilCliente;

/**
 *
 * @author abraham
 */
public class AutomovilClienteRepositoryImpl {

	
	 @PersistenceContext
	 EntityManager  em ;
	 
    /**
     *
     * @return
     */
    public List<AutomovilCliente> getAll() {
		 
		 return  (List<AutomovilCliente>) em.createNativeQuery("Select * from t_automovil_cliente").getResultList();
			     
	 }
}
