package com.enermex.repository.impl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import com.enermex.dto.NotificaDespachadorDto;
import com.enermex.modelo.UsuarioEntity;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class UsuarioRepositoryImpl {




@PersistenceContext
private EntityManager em;



/**
 * Notifica a todos los despachadores activos de la app
     * @param ids
 * @return
 */
 public List<String> notificaDespachadoresTodos(List<Long> ids){
    StringBuilder sb = new StringBuilder();
    sb.append(" select fcm from t_usuario tu, t_despachador_fcm tdf ");
    sb.append(" where tu.id_usuario = tdf.id_despachador ");
    sb.append(" and tu.estatus = 'A' and tu.id_usuario in(");
    sb.append(ids);
    sb.append(")");
    
    String query= sb.toString().replace("[","");
     query= query.replace("]","");
   
   
   
    Query q = em.createNativeQuery(query);
     List<String> tokens =  q.getResultList();
    return tokens;
   
   }
 
 

/**
 * Consulta todos los despachadores activos
 * @return
 */
 public List<NotificaDespachadorDto> getDespachadores(){
 List<NotificaDespachadorDto> despachadores = new ArrayList<>();
    StringBuilder sb = new StringBuilder();
    sb.append(" select distinct (nombre_completo),id_usuario FROM( ");
    sb.append("   select tu.id_usuario, CONCAT(tu.nombre,' ',tu.apellido_paterno,' ',tu.apellido_materno)as nombre_completo from t_usuario tu,  ");
    sb.append("    t_usuarios_rol tur,  ");
    sb.append("    t_rol tr,  ");
    sb.append("    t_usuario_ruta ur,  ");
    sb.append("    t_ruta r  ");
    sb.append(" Where tu.id_usuario = tur.id_usuario  ");
    sb.append("  and   tur.id_rol = tr.id_rol  ");
    sb.append("  and   ur.id_usuario = tu.id_usuario  ");
    sb.append("  and   ur.id_ruta    = r.id_ruta  ");
    sb.append("  and   tr.id_rol =3  ");
    sb.append("  and   tu.estatus = 'A')TBL1 ");

    System.out.println(sb.toString());
    Query q = em.createNativeQuery(sb.toString());
     List<Object[]> despachadoresO = q.getResultList();
     NotificaDespachadorDto ue;
      for(Object[] o : despachadoresO) {
     
     ue  = new NotificaDespachadorDto();
     ue.setIdUsuario(new Long(o[1].toString()));
     ue.setDespachador(o[0].toString());
     despachadores.add(ue);
     }
     
     
     
     
    return despachadores;
   
   }
   


}

