package com.enermex.repository.impl;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.enermex.dto.CoberturaDto;
import com.enermex.dto.EdoMunDto;
import com.enermex.dto.HorarioServicioDto;
import com.enermex.dto.ListadoCombustiblesDto;
import com.enermex.modelo.DataTableFilter;
import com.enermex.modelo.DataTableResult;
import com.enermex.modelo.PrecioGasolina;
import com.enermex.utilerias.DAOUtil;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public class PrecioGasolinaRepositoryImpl {
	
	@PersistenceContext
	EntityManager  em ;
	
	@Autowired
	private DAOUtil daoUtil;
	
	/**
	 * 
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCombustible
	 * @return
	 */
	public Double getPrecioPromedio(Integer idEstado,Integer idMunicipio,Integer idCombustible) {
		
		 StringBuilder sb = new StringBuilder();
		 Double promedio=null;
		 try {
		
			   sb.append(" Select pg.promedio from t_gasolina_estado_municipio pg  ");
			   sb.append("   where pg.id_estado =:idEstado and pg.id_municipio=:idMunicipio and pg.combustible=:combustible  ");
			   sb.append(" limit 1; ");

			      Query q = em.createNativeQuery(sb.toString());
			      q.setParameter("idEstado", idEstado);
			      q.setParameter("idMunicipio", idMunicipio);
			      q.setParameter("combustible", idCombustible);

			     Object o   = q.getSingleResult();
			     promedio  = new Double(o.toString());
		 }
		 catch(Exception e) {
			 
			 e.printStackTrace();
		 }
				  
       return promedio;
	}

	/**
	 * 
	 * @return
	 */
	public List<ListadoCombustiblesDto> getPrecios() {
		
		 StringBuilder sb = new StringBuilder();
		 List<ListadoCombustiblesDto> precios = new ArrayList<ListadoCombustiblesDto>();
		 try {
			 sb.append(" select   g.id, g.id_estado, e.nombre as edo,g.id_municipio,m.nombre as alc, c.id_codigo, g.combustible,g.promedio,g.fecha_registro  ");
			   sb.append("   from  t_gasolina_estado_municipio g FORCE INDEX (t_gasolina_estado_municipio_inx),t_cat_codigo c FORCE INDEX (t_cat_codigo_indx),t_cat_estado e FORCE INDEX (t_cat_estado_indx),t_cat_municipio m  FORCE INDEX(t_cat_municipio_indx) ");
			   sb.append(" where    g.id_municipio = c.id_municipio ");
			   sb.append("   and    g.id_estado = c.id_estado ");
			   sb.append("   and    g.id_estado = e.id_estado ");
			   sb.append("   and    g.id_municipio = m.id_municipio  ");
   
			   System.out.println(sb.toString());
			   
			      Query q    = em.createNativeQuery(sb.toString());
			    
				  for(Object o :  q.getResultList()) {
		    		     Object[] hr = (Object[] )o; 
		    		     ListadoCombustiblesDto dto = new ListadoCombustiblesDto();
		    		     Date fechaRegistro;
		    		     
		    		     dto.setId((new BigInteger(hr[0].toString())));
		    		     dto.setIdEstado(new Integer(hr[1].toString()));
		    		     dto.setEstado(hr[2].toString());
		    		     dto.setIdAlcaldia((new Integer(hr[3].toString())));
		    		     dto.setAlcaldia(hr[4].toString());
		    		     dto.setCp(hr[5].toString());
		    		     dto.setIdCombustible(new Integer(hr[6].toString()));
		    		     dto.setPromedio((new Double(hr[7].toString())));
		    		     fechaRegistro =  (Date)hr[8];
		    		     dto.getFechaRegistro().setTime(fechaRegistro);
		    		     precios.add(dto);
		    		     
		    	  }
      
		 }
		 catch(Exception e) {
			 
			 e.printStackTrace();
		 }
				  
      return precios;
	}

/**
 * 	Ingeniero Abraham Vargas Garcia
 * @param filter
 * @return
 */
public DataTableResult getPreciosCombustibles(DataTableFilter filter) {
		
		StringBuffer query = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		query.append("Select IDSEQ,ID,ID_ESTADO,ESTADO,ID_MUNICIPIO,MUNICIPIO,CP,COMBUSTIBLE,PROMEDIO,FECHA from ");
		query.append(" ( ");
		   query.append(" select (@rn := @rn + 1) as IDSEQ, T3.*  ");
		   query.append(" FROM ( ");
		   query.append("   SELECT * FROM ");
		   query.append("    (SELECT * FROM  ");
		   query.append("     (SELECT g.id as ID, g.id_estado as ID_ESTADO, e.nombre as ESTADO ,g.id_municipio as ID_MUNICIPIO,  ");
		   query.append("       m.nombre as MUNICIPIO, c.id_codigo as CP , g.combustible as COMBUSTIBLE,g.promedio as PROMEDIO, ");
		   query.append("       g.fecha_creacion as FECHA    from  t_gasolina_estado_municipio g FORCE INDEX (t_gasolina_estado_municipio_indx), ");
		   query.append("       t_cat_codigo c FORCE INDEX (t_cat_codigo_indx),t_cat_estado e FORCE INDEX (t_cat_estado_indx), ");
		   query.append("       t_cat_municipio m  FORCE INDEX(t_cat_municipio_indx) ");
		   query.append("       where    g.id_municipio = c.id_municipio ");
		   query.append("        and    g.id_estado = c.id_estado  ");
		   query.append("        and    g.id_estado = e.id_estado  ");
		   query.append("        and    g.id_municipio = m.id_municipio ");
	       query.append("        and    e.id_estado    = m.id_estado ");
	       query.append("         )T1)T2)T3  join  ");
		   query.append("        (select @rn := 0) vars ");
		   
		
	    System.out.println(query.toString());
		DataTableResult result = new DataTableResult();
		//Obtenemos el query del total de registros en la consulta
		String countQueryTotal = daoUtil.getCountQuery(query.toString()); 
		//Obtenemos y agregamos las condiciones al query
		String condiciones = daoUtil.getCondiciones(filter);
		query.append(condiciones);
		//Obtenemos el query del total registros filtrados
		String countQueryFiltered = daoUtil.getCountQuery(query.toString());

		BigInteger recordsTotal = recordsTotal(countQueryTotal);
		BigInteger recordsFiltered = recordsTotal(countQueryFiltered);	
		String finalQuery = daoUtil.createFinalQuery(filter, query.toString(), recordsTotal);

		 StringBuilder sb = new StringBuilder();
		 List<ListadoCombustiblesDto> precios = new ArrayList<ListadoCombustiblesDto>();
		 try {
  
			   System.out.println(finalQuery);
			   
				Session session = em.unwrap(Session.class);
				SessionImpl sessionImpl = (SessionImpl) session;
				Statement stmt = null;
				Connection con =sessionImpl.connection();
			
			      stmt = con.createStatement();
			      ResultSet rs = stmt.executeQuery(finalQuery);
			    
			      while(rs.next()) {

		    		     ListadoCombustiblesDto dto = new ListadoCombustiblesDto();
		    		     String fechaRegistro;
		    		     
		    		     dto.setId((new BigInteger(String.valueOf(rs.getInt(2)))));
		    		     
		    		     dto.setIdEstado((new Integer(String.valueOf(rs.getInt(3)))));
		    		     dto.setEstado(String.valueOf(rs.getString(4)));
		    		     dto.setIdAlcaldia((new Integer(String.valueOf(rs.getInt(5)))));
		    		     dto.setAlcaldia(rs.getString(6));
		    		     dto.setCp(rs.getString(7));
		    		     dto.setIdCombustible(new Integer(String.valueOf(rs.getInt(8))));
		    		     dto.setPromedio(rs.getDouble(9));
		    		     fechaRegistro =  rs.getString(10);
		    		     Date creacion = sdf.parse(fechaRegistro);
		    		     dto.setFechaVigencia(sdf2.format(creacion));
		    		     precios.add(dto);
		    		     
		    	  }
     
		 }
		 catch(Exception e) {
			 
			 e.printStackTrace();
		 }
		
		result.setRecordsTotal(recordsTotal);
		result.setRecordsFiltered(recordsFiltered);
		result.setDraw(filter.getDraw());
		result.setData(precios);
		
		return result;
	}



/**
 * Crea tabla de precio de combustibles 
 */
  public void createAndDropTablePreciosCombustiblesServerSide() {
		
		
    	StringBuilder sb = new StringBuilder();
 	    Session session = em.unwrap(Session.class);
 	 
		 sb.append(" CREATE  TABLE precios_combustibles as (  ");
		   sb.append("   select (@rn := @rn + 1) as IDSEQ, T3.* ");
        sb.append("    FROM ( ");
		     sb.append("   SELECT * FROM  ");
		     sb.append("     (SELECT * FROM ");
		          sb.append(" (SELECT g.id as ID, g.id_estado as ID_ESTADO, e.nombre as ESTADO ,g.id_municipio as ID_MUNICIPIO, ");
		          sb.append("   m.nombre as MUNICIPIO, c.id_codigo as CP , g.combustible as COMBUSTIBLE,g.promedio as PROMEDIO, ");
		            sb.append("  g.fecha_registro as FECHA    from  t_gasolina_estado_municipio g FORCE INDEX (t_gasolina_estado_municipio_indx), ");
		          sb.append("    t_cat_codigo c FORCE INDEX (t_cat_codigo_indx),t_cat_estado e FORCE INDEX (t_cat_estado_indx), ");
		          sb.append("    t_cat_municipio m  FORCE INDEX(t_cat_municipio_indx)  ");
		          sb.append("  where    g.id_municipio = c.id_municipio ");
		          sb.append("    and    g.id_estado = c.id_estado ");
		          sb.append("    and    g.id_estado = e.id_estado ");
		          sb.append("    and    g.id_municipio = m.id_municipio  )T1)T2)T3  join ");
		          sb.append("    (select @rn := 0) vars );");
		          
		          System.out.println(sb.toString());
				 session.doWork(new Work() {

					@Override

					public void execute(Connection conn) throws SQLException {
						PreparedStatement stmt = null;
						// TODO Auto-generated method stub
						stmt = conn.prepareStatement(sb.toString());

						
						stmt.executeUpdate(); 
						stmt.close();	 

					}});
				 
				 
	       System.out.println("*** END tabla para server side procesing ***");
		   
		
 }

    /**
     *
     */
    public void dropTablePreciosCombustiblesServerSide() {
		
		
  	StringBuilder sb = new StringBuilder();
	    Session session = em.unwrap(Session.class);
	 
	     sb.append(" DROP TABLE IF EXISTS precios_combustibles; ");
		          
		          System.out.println(sb.toString());
				 session.doWork(new Work() {

					@Override

					public void execute(Connection conn) throws SQLException {
						PreparedStatement stmt = null;
						// TODO Auto-generated method stub
						stmt = conn.prepareStatement(sb.toString());

						
						stmt.executeUpdate(); 
						stmt.close();	 

					}});
				 
				 
	       System.out.println("*** END tabla para server side procesing ***");
		   
		
}

/**
 * 
 * @param query
 * @return
 */
public BigInteger recordsTotal(String query) {
	
	Session session = em.unwrap(Session.class);
	SessionImpl sessionImpl = (SessionImpl) session;
	Statement stmt = null;
	Connection con =sessionImpl.connection();
	BigInteger recordsTotal=null;
	System.out.println("contando registros");
    System.out.println(query);
	 try {
		    PreparedStatement sentencia= con.prepareStatement(query);
		    ResultSet rs = sentencia.executeQuery(query);
		    while(rs.next()){
		         //Retrieve by column name
		    	recordsTotal  =  new BigInteger( String.valueOf(rs.getInt(1)));
		   
		    }

	 }
	 catch(Exception e) {
		 
		 e.printStackTrace();
	 }
			  
	 
   return recordsTotal;
}

/**
 * 
 */
public void deletePreciosGasolina() {
	    	
	      	StringBuilder sb = new StringBuilder();
	   	    Session session = em.unwrap(Session.class);
	   	 
			 sb.append(" DELETE FROM t_gasolina_estado_municipio; ");
			 
			  
			   System.out.println(sb.toString());

			 session.doWork(new Work() {

				@Override

				public void execute(Connection conn) throws SQLException {
					PreparedStatement stmt = null;
					// TODO Auto-generated method stub
					stmt = conn.prepareStatement(sb.toString());

					
					stmt.execute(); 
					stmt.close();	 

				}});
	
  }

/**
 * Obtiene los estados  acuerdo a la cobertura de las rutas para
 * cargar solo esa info
     * @return 
 */
  public List<EdoMunDto> getCoberturaEdoMun() {
	    List<EdoMunDto> cobertura = new ArrayList<EdoMunDto>();
  	    StringBuilder sb = new StringBuilder();
	    Session session = em.unwrap(Session.class);
	 
	 sb.append(" select cc.id_estado,cc.id_municipio from  ");
	 sb.append("   t_ruta_codigo rc,t_cat_codigo cc,t_ruta r ");
	 sb.append("    where rc.id_codigo = cc.id_codigo ");
	 sb.append("     and   r.id_ruta    = rc.id_ruta ");
	 sb.append("     and   r.estatus    = 'A' ");
	 sb.append("     group by  cc.id_estado,cc.id_municipio; ");
	 
	  
	   System.out.println(sb.toString());

	 session.doWork(new Work() {

		@Override

		public void execute(Connection conn) throws SQLException {
			PreparedStatement stmt = null;
			// TODO Auto-generated method stub
			stmt = conn.prepareStatement(sb.toString());

			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				EdoMunDto e = new EdoMunDto(); 
				e.setIdEdo(rs.getInt(1));
				e.setIdMun(rs.getInt(2));
				cobertura.add(e);
			}
			
			stmt.close();	 

		}});

	 return cobertura;
	  
  }
   
  
  
        
  /**
   * Obtiene los estados  acuerdo a la cobertura de las rutas para
   * cargar solo esa info
     * @return 
   */
    public List<Integer> getCoberturaMunAlc() {
  	    List mun = new ArrayList<>();
    	    StringBuilder sb = new StringBuilder();
  	    Session session = em.unwrap(Session.class);
  	 
  	 sb.append(" select cc.id_municipio from ");
  	 sb.append("   t_ruta_codigo rc,t_cat_codigo cc ");
  	 sb.append("   where rc.id_codigo = cc.id_codigo ");
  	 sb.append("   group by cc.id_municipio;  ");
  	 
  	  
  	   System.out.println(sb.toString());

  	 session.doWork(new Work() {

  		@Override

  		public void execute(Connection conn) throws SQLException {
  			PreparedStatement stmt = null;
  			// TODO Auto-generated method stub
  			stmt = conn.prepareStatement(sb.toString());

  			ResultSet rs = stmt.executeQuery();
  			while(rs.next()) {
  			
  				mun.add(rs.getInt(1));
  				
  			}
  			
  			stmt.close();	 

  		}});

  	 return mun;
  	  
    }




	
	
}
