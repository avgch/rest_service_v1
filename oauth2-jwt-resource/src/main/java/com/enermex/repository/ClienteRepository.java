package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.dto.UsuarioDto;
import com.enermex.modelo.AppStore;
import com.enermex.modelo.Cliente;


/**
 * @author abraham
 *
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface ClienteRepository  extends JpaRepository<Cliente, BigInteger>{

	
	
	
	/**
	 * @param id
	 * id
	 */
	@Modifying
	@Query("update Cliente c set c.estatus=2 where c.idCliente = ?1")
	public void delete(BigInteger id) ;
	
	
	/**
	 * @param nombre
	 * nombre
	 * @param correo
	 * correo
	 * @param telefono
	 * telefono
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set c.nombre=?1, c.correo = ?2, c.telefono = ?3  where c.idCliente = ?4")
	void update(String nombre, String correo, String telefono, BigInteger idCliente);
	
	/**
	 * @param contrasena
	 * contrasena
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set c.contrasena=?1, c.acceso=true   where c.idCliente = ?2")
	void updatePassword(String contrasena, BigInteger idCliente);
	
	
	/**
	 * @param email
	 * email 
	 * @return Objeto
	 */
	@Query("Select c from Cliente c where c.correo = ?1 ")
	//@Query("Select c from Cliente c join c.autos ac  where c.correo like ?1 and ac.estatus=1")
	Cliente findByEmail(String email);
	
	/**
	 * @param id
	 * id
	 * @return Objeto
	 */
	@Query("Select c from Cliente c where c.idCliente = ?1")
	Cliente findClienteById(BigInteger id);
	
	
	/**
	 * @param correo
	 * correo
	 * @param contrasena
	 * contrasena
	 * @return Objeto
	 */
	@Query("Select c from Cliente c where c.correo = ?1 and c.contrasena = ?2 ")
	Cliente login(String correo, String contrasena);
	
	
	/**
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set c.acceso=true  where c.idCliente = ?1")
	void updateAcces(BigInteger idCliente);
	
	/**
	 * @param idTitular
	 * idTitular
	 * @return Objeto
	 */
	@Query("Select c from Cliente c where c.idTitular = ?1 ")
	List<Cliente> getInvitados(BigInteger idTitular);
	
	//Actualiza el perfil (telefono)
	/**
	 * @param telefono
	 * telefono
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.telefono = ?1  where c.idCliente = ?2")
	void updatePerfilTel(String telefono,  BigInteger idCliente);
	
	//Actualiza el perfil (password)
	/**
	 * @param contrasena
	 * contrasena
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.contrasena = ?1  where c.idCliente = ?2")
	void updatePerfilPass(String contrasena,  BigInteger idCliente);
	
	//Obtiene el password del cliente para validarlo
	/**
	 * @param idCliente
	 * idCliente
	 * @return Objeto
	 */
	@Query("select c from Cliente c  where c.idCliente = ?1")
	Cliente getPassClienteBycript(BigInteger idCliente);
   
	/**
	 * @param apellidoPaterno
	 * apellidPaterno
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.apellidoPaterno = ?1  where c.idCliente = ?2")
	void updatePerfilApellidoP(String apellidoPaterno,  BigInteger idCliente);
	
	/**
	 * @param apellidoMaterno
	 * apellidoMaterno
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.apellidoMaterno = ?1  where c.idCliente = ?2")
	void updatePerfilApellidoM(String apellidoMaterno,  BigInteger idCliente);
	
	/**
	 * @param nombre
	 * nombre
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.nombre = ?1  where c.idCliente = ?2")
	void updatePerfilNombre(String nombre,  BigInteger idCliente);
	
	/**
	 * @param token
	 * token
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.tokenConekta = ?1  where c.idCliente = ?2")
	void updateTokenConektaCustumer(String token, BigInteger idCliente);
	
	/**
	 * @param idCliente
	 * idCliente
	 * @return String
	 */
	@Query("select c.tokenConekta from Cliente c  where c.idCliente = ?1")
	String getIdTokenCustumer(BigInteger idCliente);
	
    /**
     *
     * @return List
     */
    @Query("Select c from Cliente c ")
	List<Cliente> getClientes();
	
	/**
	 * @param bloqueo
	 * bloqueo
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.bloqueo = ?1  where c.idCliente = ?2")
	void bloqueoCliente(boolean bloqueo,  BigInteger idCliente);
	
	/**
	 * @param correo
	 * correo
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.correo = ?1  where c.idCliente = ?2")
	void updatePerfilCorreo(String correo,  BigInteger idCliente);
	
	/**
	 * @param estatus
	 * estatus
	 * @param idCliente
	 * idCliente
	 */
	@Modifying
	@Query("update Cliente c set  c.accesoSms = ?1  where c.idCliente = ?2")
	void updateAccesoSms(Boolean estatus ,BigInteger idCliente);
	
	//Regresa el id del titular a traves de id invitado
	/**
	 * @param idInvitado
	 * idInvitado
	 * @return BigInteger
	 */
	@Query("select c.idTitular from Cliente c  where c.idCliente = ?1")
	BigInteger getIdTitularByIdInvitado(BigInteger idInvitado);
	
	/**
	 * @return List
	 */
	@Query("Select a from AppStore a")
    List<AppStore> getAppStorePaths();
  
  // José Antonio González Hernández
  // Lista de clientes por su titular para reportes
    /**
     * @param idTitular
     * idTitular
     * @return List
     */
    List<Cliente> findByIdTitular(BigInteger idTitular);
	
	
	//Regresa el id del idCliente a traves de id invitado
	/**
	 * @param correo
	 * correo
	 * @return BigInteger
	 */
	@Query("select count(c) from Cliente c  where c.correo = ?1 and c.fechaBaja!=null ")
	BigInteger tieneBaja(String correo );
	
	
	/**
	 * @param email
	 * email
	 * @return Objeto
	 */
	@Query("Select c from Cliente c where c.correo = ?1  ")
	//@Query("Select c from Cliente c join c.autos ac  where c.correo like ?1 and ac.estatus=1")
	Cliente tieneAcceso(String email);
	
    /**
     *
     * @param idTitular
     * idTitular
     * @return Long
     */
    long countByIdTitular(BigInteger idTitular);
}
