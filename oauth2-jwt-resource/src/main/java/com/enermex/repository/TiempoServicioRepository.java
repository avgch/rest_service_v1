package com.enermex.repository;


import com.enermex.view.TiempoServicioView;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface TiempoServicioRepository extends CrudRepository<TiempoServicioView, Long> {
  
}
