package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CalificaPedido;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CalificaPedidoRepository extends JpaRepository<CalificaPedido, Integer> {
	
    /**
     *
     * @param idPedido
     * idPedido
     * @return Integer
     */
    @Query("Select cp.calificacion from CalificaPedido cp where cp.idPedido = ?1 and cp.califica like 'Cliente'")
  Integer getCalificacionByIdPedido(BigInteger idPedido);
  

  // INTER: José Antonio González Hernández
  // Búsqueda de calificaciones por identificador de pedido

    /**
     *
     * @param idPedido
     * idPedido
     * @return List
     */
  List<CalificaPedido> findByIdPedido(BigInteger idPedido);
}
