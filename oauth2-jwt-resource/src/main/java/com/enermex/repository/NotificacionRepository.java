package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enermex.modelo.Cliente;
import com.enermex.modelo.Notificacion;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface  NotificacionRepository  extends JpaRepository<Notificacion, Integer> {

    /**
     *
     * @return List
     */
    @Query("Select n from Notificacion n order by n.fechaCreacion desc")
	List<Notificacion> getNotificaciones();
	
    /**
     *
     * @return List
     */
    @Query("Select n from Notificacion n where n.enviadaA = 'Despachadores' order by n.fechaCreacion desc")
	List<Notificacion> getNotificacionesDespachadores();
	
    /**
     *
     * @return List
     */
    @Query("Select n from Notificacion n where n.enviadaA = 'Clientes' order by n.fechaCreacion desc")
	List<Notificacion> getNotificacionesClientes();
	
    /**
     *
     * @param idNotificacion
     * idNotificacion
     * @return Objeto
     */
    @Query("Select n from Notificacion n where n.idNotificacion=?1 order by n.fechaCreacion desc")
	Notificacion getNotificacionesByIdCliente(BigInteger idNotificacion);
	
}
