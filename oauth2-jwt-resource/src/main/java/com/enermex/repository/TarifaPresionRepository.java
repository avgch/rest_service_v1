package com.enermex.repository;

import com.enermex.modelo.TarifaPresionEntity;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * TarifaPresionRepository
 */
@Repository
@Transactional
public interface TarifaPresionRepository extends CrudRepository<TarifaPresionEntity, String> {
	
    /**
     *
     * @param cp
     * @return
     */
    @Query("Select tp.precio  from TarifaPresionEntity tp where tp.idCodigo =?1 ")
	public String getTarifaPresionByCp(String cp);

}