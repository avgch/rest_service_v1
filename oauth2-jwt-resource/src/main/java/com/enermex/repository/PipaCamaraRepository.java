package com.enermex.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enermex.modelo.CatMarca;
import com.enermex.modelo.PipaCamara;

/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface PipaCamaraRepository  extends  JpaRepository<PipaCamara, Integer>{
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query("Select pm  from PipaCamara pm where pm.pipaCamaraKey.idPipa = ?1")
	List<PipaCamara> getAll(Integer idPipa);
	
    /**
     *
     * @param idPipa
     */
    @Modifying
	@Query("delete  from PipaCamara pm where pm.pipaCamaraKey.idPipa = ?1")
	void deleteCurrentePipaCamara(Integer idPipa);

    /**
     *
     * @return
     */
    @Query("Select pm  from PipaCamara pm where pm.pipaCamaraKey.idPipa !=0")
	List<PipaCamara> camarasAsigandas();
	
    /**
     *
     * @param idPipa
     * @return
     */
    @Query("Select pm  from PipaCamara pm where pm.pipaCamaraKey.idPipa !=?1")
	List<PipaCamara> camarasAsigandasOtrasPipas(Integer idPipa);
	

	
}
