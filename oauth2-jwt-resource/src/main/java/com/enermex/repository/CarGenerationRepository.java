package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CarGeneration;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CarGenerationRepository  extends JpaRepository<CarGeneration, Integer> {
	
    /**
     *
     * @param idModelo
     * idModelo
     * @return List
     */
    @Query("Select cg from CarGeneration cg  where cg.idCarModel = ?1")
	List<CarGeneration> getGenerationByModeloId(Integer idModelo);

}
