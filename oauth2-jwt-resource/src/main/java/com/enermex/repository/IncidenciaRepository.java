package com.enermex.repository;

import java.util.List;

import com.enermex.enumerable.IncidenciaEstatus;
import com.enermex.modelo.IncidenciaEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * TODO: Completar para aplicación angular
 */
public interface IncidenciaRepository extends CrudRepository<IncidenciaEntity, Integer> {
  public List<IncidenciaEntity> findAll();

    /**
     *
     * @param estatus
     * estatus
     * @return List
     */
    public List<IncidenciaEntity> findByEstatus(IncidenciaEstatus estatus);
}
