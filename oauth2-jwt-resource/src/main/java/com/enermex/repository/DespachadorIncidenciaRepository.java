package com.enermex.repository;

import java.util.List;

import com.enermex.enumerable.IncidenciaEstatus;
import com.enermex.modelo.DespachadorIncidenciaEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * TODO: Completar para aplicación angular
 */
public interface DespachadorIncidenciaRepository extends CrudRepository<DespachadorIncidenciaEntity, Integer> {
  public List<DespachadorIncidenciaEntity> findAll();

    /**
     *
     * @param estatus
     * estatus
     * @return List
     */
    public List<DespachadorIncidenciaEntity> findByEstatus(IncidenciaEstatus estatus);
}
