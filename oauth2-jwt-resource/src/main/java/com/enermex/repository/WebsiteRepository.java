package com.enermex.repository;

import com.enermex.modelo.WebsiteEntity;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface WebsiteRepository extends CrudRepository<WebsiteEntity, Integer> {
  
}
