package com.enermex.repository;

import java.util.List;

import com.enermex.view.CodigoPostalUsoView;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface CodigoPostalUsoRepository extends CrudRepository<CodigoPostalUsoView, String> {
  List<CodigoPostalUsoView> findAll();
}