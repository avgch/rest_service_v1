package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CarTrim;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CarTrimRepository extends JpaRepository<CarTrim, Integer> {
	
    /**
     *
     * @param idModelo
     * idModelo
     * @param idSerie
     * idSerie
     * @return List
     */
    @Query("Select ct from CarTrim ct  where ct.idCarModel = ?1 and  ct.idCarSerie= ?2 ")
	List<CarTrim> getTrimByModeloAndSerie(Integer idModelo,Integer idSerie);


}
