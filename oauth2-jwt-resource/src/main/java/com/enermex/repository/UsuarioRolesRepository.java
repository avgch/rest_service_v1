package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enermex.modelo.UsuarioRoles;

/**
 *
 * @author abraham
 */
public interface UsuarioRolesRepository extends JpaRepository<UsuarioRoles, Long>{

}
