package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CatMarca;
import com.enermex.modelo.CatModelo;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CatModeloRepository extends JpaRepository<CatModelo, Integer>{
	
    /**
     *
     * @param idMarca
     * idMarca
     * @return List
     */
    @Query("Select cm  from CatModelo cm where  cm.idMarca = ?1")
	List<CatModelo> getModelosByIdMarca(Integer idMarca);

}
