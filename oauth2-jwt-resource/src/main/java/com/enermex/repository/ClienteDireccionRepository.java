package com.enermex.repository;

import java.util.List;

import com.enermex.enumerable.DireccionEstatus;
import com.enermex.modelo.ClienteDireccionEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * ClienteDireccionRepository
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface ClienteDireccionRepository extends CrudRepository<ClienteDireccionEntity, Long> {

  /**
  * Consulta de direcciones
  * @param idCliente
  * idCliente
  * @param idEstatus
  * idEstatus
  * @return List
  */
  public List<ClienteDireccionEntity> findAllByIdClienteAndIdEstatus(Long idCliente, DireccionEstatus idEstatus);
  
  
}