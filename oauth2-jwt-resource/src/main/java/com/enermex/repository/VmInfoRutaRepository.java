package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.Cliente;


/**
 *
 * @author abraham
 */
@Repository
@Transactional
public interface VmInfoRutaRepository  extends JpaRepository<Object, BigInteger>{
	
    /**
     *
     * @param cp
     * @return
     */
    //@Query("Select vr from VwInfoRuta vr where vr.cp = ?1")
	//List<VwInfoRuta> getInfRuta(String cp);

}
