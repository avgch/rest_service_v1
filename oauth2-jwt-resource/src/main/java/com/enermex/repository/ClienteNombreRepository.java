package com.enermex.repository;

import java.math.BigInteger;
import java.util.List;

import com.enermex.view.ClienteNombreView;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author abraham
 */
public interface ClienteNombreRepository extends CrudRepository<ClienteNombreView, BigInteger> {

    /**
     *
     * @param nombreCompleto
     * nombreCompleto
     * @return List
     */
    List<ClienteNombreView> findTop10ByNombreCompletoUpperContaining(String nombreCompleto);

    /**
     *
     * @param nombreCompleto
     * nombreCompleto
     * @return List
     */
    @Query(value = "SELECT * FROM v_cliente_nombre WHERE nombre_completo_upper LIKE CONCAT('%', ?1, '%') AND id_familiar IS NULL AND id_titular IS NULL LIMIT 10", nativeQuery = true)
  List<ClienteNombreView> findIndividual(String nombreCompleto);

    /**
     *
     * @param nombreCompleto
     * nombreCompleto
     * @return List
     */
    @Query(value = "SELECT * FROM v_cliente_nombre WHERE nombre_completo_upper LIKE CONCAT('%', ?1, '%') AND (id_familiar IS NOT NULL OR id_titular IS NOT NULL) LIMIT 10", nativeQuery = true)
  List<ClienteNombreView> findFamiliar(String nombreCompleto);
}
