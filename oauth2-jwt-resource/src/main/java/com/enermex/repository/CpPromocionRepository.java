package com.enermex.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.modelo.CpPromocion;

/**
 *
 * @author abraham
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public interface CpPromocionRepository extends JpaRepository<CpPromocion, BigInteger> {
	
    /**
     *
     * @param idPromocion
     * idPromocion
     */
    @Query("delete from CpPromocion cp where cp.promocion.idPromocion=?1")
	@Modifying
	void delete(Long idPromocion);

}
