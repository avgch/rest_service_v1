package com.enermex.repository;

import java.util.List;

import com.enermex.modelo.CatUsoCfdiEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * CatUsoCfdiRepository
 */
public interface CatUsoCfdiRepository extends CrudRepository<CatUsoCfdiEntity, String> {
  @Query(value = "SELECT * FROM t_cat_uso_cfdi WHERE estatus = 'A'", nativeQuery = true)
  List<CatUsoCfdiEntity> findAll();
}