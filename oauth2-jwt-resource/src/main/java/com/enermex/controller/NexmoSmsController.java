package com.enermex.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.modelo.Cliente;
import com.enermex.service.ClienteService;
import com.enermex.utilerias.NexmoSms;
import com.enermex.utilerias.RestResponse;
import com.enermex.service.ClienteSmsRegistroService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/sms")
public class NexmoSmsController {
	
	
	@Autowired
	NexmoSms  nexmoSms;
	
	@Autowired
	ClienteService clienteService;
	
	
	
	/**
	 * Valida SMS 
	 * @param oauth
     * @param codigo
	 * @return
	 */
	@PostMapping("/valido")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> validaSms(@RequestHeader(value="Authorization") String oauth,@RequestParam(name = "codigo")String codigo){
		
		 RestResponse response = new RestResponse();
		List<String>  tokensDevices=null;
		Cliente cliente;
		try {

   		      cliente = this.clienteService.getClienteFromToken(oauth);
		     //1a valido cliente
		     if(cliente == null){
		    	 
		    	 
		    	 if(!(cliente.getIdCliente().compareTo(cliente.getIdCliente())==0)) {
		    		 response.setSuccess(false);
			          response.setCode("FG007");
			          response.setMessage("Intentas acceder a recurso no permitido");
			          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	 
		    		 
		    	 }
		          
		     }
		     
		     response.setSuccess(true);
	    	 response.setCode("FG028");
	    	 response.setData(nexmoSms.esCodigoValido(codigo, cliente.getIdCliente()));
	         response.setMessage("Se ha concluido exitosamente la validación de Teléfono celular, puede continuar con la solicitud del servicio");
	
    	 }

       catch(FastGasException ex) {
			
			response.setSuccess(false);
    		response.setCode(ex.getCodeError());
    		response.setMessage(ex.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	catch(Exception e) {
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage()+e.getCause());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	/**
	 * Genera SMS
	 * @param oauth
	 * @return
	 */
	@GetMapping("/nuevo")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> generaSms(@RequestHeader(value="Authorization") String oauth){
		
		 RestResponse response = new RestResponse();
		List<String>  tokensDevices=null;
		Cliente cliente;
		try {

   		      cliente = this.clienteService.getClienteFromToken(oauth);
		     //1a valido cliente
		     if(cliente == null){
		    	 
		    	 
		    	 if(!(cliente.getIdCliente().compareTo(cliente.getIdCliente())==0)) {
		    		 response.setSuccess(false);
			          response.setCode("FG007");
			          response.setMessage("Intentas acceder a recurso no permitido");
			          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	 
		    		 
		    	 }
		          
		     }
		     
             if(cliente.getAccesoSms()==null) {
		    	 
		    	 nexmoSms.sendSms(cliente.getTelefono(),cliente.getIdCliente());
		    	 throw new FastGasException("Estimado cliente, aun no ha confirmado su teléfono celular, favor de colocar el código que le fue enviado por SMS","FG027");
		     }
             if(cliente.getAccesoSms()==false) {
		    	 
		    	 nexmoSms.sendSms(cliente.getTelefono(),cliente.getIdCliente());
		    	 throw new FastGasException("Estimado cliente, aun no ha confirmado su teléfono celular, favor de colocar el código que le fue enviado por SMS","FG027");
		     }
		     
		     
		     response.setSuccess(true);
	    	 response.setCode("FG032");
	    	 response.setMessage("Usted ha completado el proceso de registro vía sms");
	    	 response.setData(null);
	         
	
    	 }

       catch(FastGasException ex) {
			
			response.setSuccess(false);
    		response.setCode(ex.getCodeError());
    		response.setMessage(ex.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	catch(Exception e) {
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage()+e.getCause());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	
	
	
	

}
