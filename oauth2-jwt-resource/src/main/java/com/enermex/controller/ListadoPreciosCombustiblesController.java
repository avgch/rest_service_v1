package com.enermex.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.utilerias.RestResponse;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.dto.RequestReportePreciosDto;
import com.enermex.modelo.DataTableFilter;
import com.enermex.modelo.bo.BuildReportePreciosHistoricoBo;
import com.enermex.service.ListadoCombustiblesService;
import com.enermex.repository.impl.PreciosGasolinaHistoricoReporteImpl;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/precios/combustibles")
public class ListadoPreciosCombustiblesController {
	
	@Autowired
	ListadoCombustiblesService listadoCombustiblesService;
	
	@Autowired
	BuildReportePreciosHistoricoBo  buildReportePreciosHistoricoBo;
	
	@Autowired
	PreciosGasolinaHistoricoReporteImpl preciosGasolinaHistoricoReporteImpl;
	
	/**
	 * Obtiene los precios de combustibles de petrointelligence
     * @param filter
	 * @param oauth
	 * @return
	 */
	@PostMapping
	public ResponseEntity<RestResponse>  getAllPrecios(@RequestBody DataTableFilter	 filter ) {
		RestResponse response = new RestResponse();

		
	  try {
		  
	     System.out.println("Obteniendo  los precios de combustibles de petrointelligence del dia");
	 
	     response.setSuccess(true);
	     response.setCode("200");
	     response.setMessage("Consulta exitosa");
	     response.setData(listadoCombustiblesService.getCombustiblesTarifas(filter));
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	        response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
	/**
	 * Obtiene reporte historico
	 * @param responseServlet
	 * @param dto
	 * @return
	 */
	@RequestMapping(value = "/reporte", method = { RequestMethod.POST ,}, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	@PreAuthorize("hasRole('ROLE_WEB')")
	public ResponseEntity<InputStreamResource>  getReporteHistorico(HttpServletResponse responseServlet,@RequestBody RequestReportePreciosDto dto ) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        ByteArrayInputStream in=null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        String fechaIncF = "";
        String fechaFinF = "";
        
	  try {
		  
		   fechaIncF     = sdf.format(dto.getFechaInc());
		   fechaFinF     = sdf.format(dto.getFechaFin());
		   
		   
	       System.out.println("building report");
	       File                   archivo                = null;
	       InputStream            inputStream            = null;
	       byte[]                 bytes                  = null;

	        SXSSFWorkbook wb          = new SXSSFWorkbook(100);
            wb.setCompressTempFiles(true);               
            buildReportePreciosHistoricoBo.buildReporteExcel(wb,fechaIncF,fechaFinF);
            wb.write(out);
            in =new ByteArrayInputStream(out.toByteArray());
            out.close();
            wb.dispose();
            wb.close();
	        
	    
	       }
	      catch(FastGasException e) {
	    	  
	    	  System.out.println(e.getCodeError());
	    	  System.out.println(e.getMessage());
	      }
	       catch(Exception e) {
	       
	        System.out.println(e);
	     
	       }
	  
	  
	  
	     return ResponseEntity
	                  .ok()
	                  .body(new InputStreamResource(in));
	  
	  }
	

}
