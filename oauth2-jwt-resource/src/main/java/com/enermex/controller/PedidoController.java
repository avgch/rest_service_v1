package com.enermex.controller;
import com.google.api.client.util.Value;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.enermex.utilerias.ConektaOperation;
import com.enermex.utilerias.NexmoSms;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;

import java.math.BigInteger;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.azure.core.annotation.Delete;
import com.enermex.avg.exceptions.ConectaException;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.avg.exceptions.PedidoException;
import com.enermex.dto.CancelarServicioDto;
import com.enermex.dto.FinalizarCompraRequestDto;
import com.enermex.dto.PedidoDto;
import com.enermex.dto.PedidoTicketRequestDto;
import com.enermex.dto.PedidoTicketResponseDto;
import com.enermex.dto.PrecioGasolinaRequestDto;
import com.enermex.dto.UbicacionDespachadorRequestDto;
import com.enermex.enumerable.PedidoVarios;
import com.enermex.enumerable.TipoPedidoEnum;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.RutaEntity;
import com.enermex.modelo.TipoPedido;
import com.enermex.utilerias.CalculoPedido;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.service.PedidoService;
import com.enermex.service.PrecioGasolinaService;
import com.enermex.service.RutaService;
import com.enermex.service.TarifaService;
import com.enermex.service.VmInfoRutaService;
import com.enermex.service.CatEstatusPedidoService;
import com.enermex.repository.ParametroRepository;
import com.enermex.repository.PedidoRepository;
import com.enermex.repository.RutaRepository;
import com.enermex.repository.impl.CodigoPostalRepositoryImpl;
import com.enermex.repository.impl.RutaRepositoryImpl;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.GasolinaPrecioService;
import com.enermex.service.ClienteFcmService;
import com.enermex.service.ClienteService;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteFcmEntity;
import com.enermex.modelo.Parametro;
import com.enermex.service.CalificaPedidoService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/pedido")
public class PedidoController {

	
	@Autowired
	private PedidoService pedidoService;
	
	private final String QR_CODE_IMAGE_PATH = "./MyQRCode.png";
	
	@Autowired
	private RutaService rutaService;
	
	@Autowired
	private VmInfoRutaService vmInfoRutaService;
	
	@Autowired
	private CatEstatusPedidoService catEstatusPedidoService;
	
	@Autowired
	Environment env;
	
	@Autowired
	RutaRepository rutaRepository;
	
	@Autowired
	RutaRepositoryImpl  rutaRepositoryImpl;
	
	@Autowired
	GasolinaPrecioService gasolinaPrecioService;
	
	@Autowired
	CalculoPedido calulosPedido; 
	
	@Autowired
	ClienteFcmService clienteFcmService;
	
	@Autowired
	CalificaPedidoService calificaPedidoService;
	
	List<ClienteFcmEntity>  tokenClientes;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	TarifaService   tarifaService;
	
	@Autowired
	ParametroRepository parametroRepository;
	
	@Autowired
	SmtpGmailSsl sendMail;
	
	@Autowired
	PedidoRepository pedidoRepository;
	
	@Autowired
	CodigoPostalRepositoryImpl  codigoPostalRepositoryImpl;
	
	@Autowired
	NexmoSms  nexmoSms;
	
	@Autowired
	PrecioGasolinaService precioGasolinaService;
	
	/**
	 * Nuevo pedido
	 * @param pedido
	 * @param oauth
	 * @return
	 */
	@PostMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> nuevoPedido(@RequestBody PedidoDto pedido,@RequestHeader(value="Authorization") String oauth){
		
		 RestResponse response = new RestResponse();
		List<String>  tokensDevices=null;
		Cliente cliente;
		try {

   		      cliente = this.clienteService.getClienteFromToken(oauth);
		     //1a valido cliente
		     if(cliente == null){
		    	 
		    	 
		    	 if(!(cliente.getIdCliente().compareTo(pedido.getIdCliente())==0)) {
		    		 response.setSuccess(false);
			          response.setCode("FG007");
			          response.setMessage("Intentas acceder a recurso no permitido");
			          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	 
		    		 
		    	 }
		          
		     }
		    
		     
		     // 1a end
		     
		     //1. Necesitamos las pipas que se encuentran asiganadas a esa ruta
		     //2. Elegimos una al azar
		     //3. Ahora de esa pipa elegida obtenemos los despachadores 
		     
			response.setCode("FG006");
			pedidoService.nuevoPedido(pedido);
    		response.setSuccess(true);
    		response.setMessage("Tu pedido esta programado");
    		
    		 //Mensaje dependiedo del numero de device
		    MulticastMessage.Builder msg=  MulticastMessage.builder();			    
		    
		    tokenClientes = clienteFcmService.queryByIdCliente(pedido.getIdCliente());
		    
		    tokensDevices = tokenClientes.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());
		    
		    if(tokensDevices!=null)
		    {
		    	if(tokensDevices.size()>0) {
				    //Notificacion
				    Notification.Builder msNot = Notification.builder();
				    
				    msNot.setBody("¡Se ha programado tu pedido!");
				    msNot.setTitle("FAST GAS ");
				    msg.addAllTokens(tokensDevices);
				    
				    msg.setNotification(msNot.build());
				    
				    FirebaseMessaging fm = FirebaseMessaging.getInstance();
				    
				    fm.sendMulticast(msg.build());
		    	}
		    }

    		//generateQRCodeImage("This is my first QR Code", 350, 350, QR_CODE_IMAGE_PATH);
    	 }
		catch(ConectaException conekta) {
			response.setSuccess(false);
			response.setCode("ERRCKT");
    		response.setMessage(conekta.getMessage());
			
		}
       catch(FastGasException ex) {
			
			response.setSuccess(false);
    		response.setCode(ex.getCodeError());
    		response.setMessage(ex.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(PedidoException ex) {
			
			response.setSuccess(false);
    		response.setCode(ex.getCodeError());
    		response.setMessage(ex.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	catch(Exception e) {
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage()+e.getCause());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	/**
	 * Obtiene los pedidos de un cliente por id
	 * @param id
	 * @param fecha
	 * @return
	 */
	@GetMapping("/cliente/{id}")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> getAllPedidos(@PathVariable(name = "id")BigInteger id,@RequestParam(name = "fecha" )String fecha ){
		
		RestResponse response = new RestResponse();
		
		try {
			
			
    		response.setSuccess(true);
    		response.setCode("FG008");
    		response.setMessage("Consulta exitosa");
    		response.setData(this.pedidoService.getPedidosCliente(id,fecha));
    	 }
		catch(PedidoException e) {
			
		}
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	
	/**
	 * Detalle de pedido del cliente
	 * @param id
	 * @return
	 */
	@GetMapping("/cliente/datalle/{id}")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> getPedidoClienteDetail(@PathVariable(name = "id")BigInteger id ){
		
		RestResponse response = new RestResponse();
		
		try {
			
			
			response.setSuccess(true);
    		response.setData(this.pedidoService.getPedidoClienteDetail(id));
    		response.setCode("FG008");
    		response.setMessage("Consulta exitosa");
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	
	private  void generateQRCodeImage(String text, int width, int height, String filePath)
            throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
    }
	
	
	/**
	 * Califica pedido
     * @param cp
	 * @param calificacion
	 * @return
	 */
	@GetMapping("/cobertura")
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> cobertura(@RequestParam(name = "cp")String cp ){
		
		RestResponse response = new RestResponse();
		try {
    	    Integer hayCobertura = rutaRepositoryImpl.validaCobertura(cp);
		    List<RutaEntity> rutaCobertura ;
		    
			if(hayCobertura ==0) {
			response.setCode("FG005");
			response.setSuccess(false);
    		response.setMessage("Sin cobertura");
			return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		  }
		   response.setSuccess(true);
    	   response.setData(null);
    	   response.setCode("FG017");
    	   response.setMessage("Con cobertura");
    	
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}

    /**
     *
     * @return
     */
    @PostMapping("/prueba")
	public ResponseEntity<RestResponse> pedidoSandBox(){
		
		RestResponse response = new RestResponse();
		
		try {
			
			//pedidoService.nuevoPedidoSandBox();
    		response.setSuccess(true);
    		response.setMessage("Tu pedido esta programado");
    		//generateQRCodeImage("This is my first QR Code", 350, 350, QR_CODE_IMAGE_PATH);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	 
			
			
			return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		}
	
	/**
	 * Obtiene los pedidos del despachador historial
	 * @param id
	 * @param fecha
	 * @return
	 */
	@GetMapping("/despachador/{id}")
	@PreAuthorize("hasRole('ROLE_DESPACHADOR')")
	public ResponseEntity<RestResponse> getAllPedidosDespachador(@PathVariable(name = "id")Long id,@RequestParam(name = "fecha" )String fecha ){
		
		
		RestResponse response = new RestResponse();
		
		try {
			
			
    		response.setSuccess(true);
    		
    		response.setData(this.pedidoService.getPedidosDespachador(id,fecha));
    		
    		response.setMessage("Consulta exitosa");
    	
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	
	/**
	 * Obtiene lel detalle del pedido (id del pedido)
	 * @param id
	 * @return
	 */
	@GetMapping("/despachador/detalle/{id}")
	//@PreAuthorize("hasRole('ROLE_DESPACHADOR')")
	public ResponseEntity<RestResponse> getPedidoDetail(@PathVariable(name = "id")BigInteger id ){
		
		
		RestResponse response = new RestResponse();
		
		try {
			
			
    		response.setSuccess(true);
    		response.setData(this.pedidoService.getPedidoDetail(id));
    		response.setMessage("Consulta exitosa");
    	
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	
	/**
	 * Obtiene los estatus de los pedidos
	 * @param id
	 * @return
	 */
	@GetMapping("/estatus")
	@RolesAllowed({ "ROLE_DESPACHADOR", "ROLE_CLIENTE","ROLE_WEB" })
	public ResponseEntity<RestResponse> getEstatusPedidos(){
		
		
		RestResponse response = new RestResponse();
		
		try {
			
			
    		response.setSuccess(true);
    		response.setData(this.catEstatusPedidoService.getAll());
    		response.setMessage("Consulta exitosa");
    	
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	
	/**
	 * Obtiene el detalle del ticket
     * @param ticket
	 * @param id
	 * @return
	 */
	@PostMapping("/ticket")
	@RolesAllowed({ "ROLE_DESPACHADOR", "ROLE_CLIENTE","ROLE_WEB" })
	public ResponseEntity<RestResponse> getDetailTicket(@RequestBody PedidoTicketRequestDto ticket){
		
		
		RestResponse response = new RestResponse();
		PedidoTicketResponseDto responseTicket = new PedidoTicketResponseDto();
		Double servicioGasolinaAux=new Double("0");;
		Double totalImporte= new Double("0");
		
		//evitamos los nulls
		responseTicket.setPrecioRevisionLlantas(new Double("0"));
		responseTicket.setPrecioLavadoAuto(new Double("0"));
		
		try {
			
			    DateTime today = new DateTime();
			    DateTimeFormatter sdf = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
			    String[] fechaHoraAP = ticket.getFechaPedido().split("-");
			    
			    DateTime fechaPedido = sdf.parseDateTime(fechaHoraAP[0]);
			    
			    
				int dias    = Days.daysBetween(today, fechaPedido).getDays();
				int horas   = Hours.hoursBetween(today, fechaPedido).getHours() % 24;
				int minutos = Minutes.minutesBetween(today, fechaPedido).getMinutes() % 60;
			
				
		if(dias<=0) {		
		   //si horas > 2
		   if(horas<2)
			   throw new  PedidoException("Estimado cliente no se puede solicitar su servicio con menos de 2 horas, favor de intentar con otro horario","FG024");
		}
		   
		
		     //validamos duplicidad de pedido para el mismo auto misma fecha 
		     pedidoService.existeDuplicidadPedidoCliente(ticket);
				
			
			//	DETERMINAMOS EL COSTO POR SERVICIO
		    TipoPedido tp= calulosPedido.tipoPedido(dias, horas, minutos);
			
		    
		    /* CONSULTAMOS EL COSTO POR SERVICIO */
			 Parametro costoService=null;
			 
			 List<Parametro> costos=this.parametroRepository.costosPorServicioAndTimepoProrroga();
			 
			 
			 if(tp.getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())==0) {
				  costoService = costos.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_PROGRAMADO.getValor()))
					    	.findAny()
					    	.orElse(null);	 
				 
			 }
			 else if(tp.getIdTipoPedido().compareTo(TipoPedidoEnum.MISMO_DIA.getId())==0 ){
				 costoService = costos.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_MISMO_DIA.getValor()))
					    	.findAny()
					    	.orElse(null);	 
			 }
			 else if(tp.getIdTipoPedido().compareTo(TipoPedidoEnum.URGENTE.getId())==0){
				 costoService = costos.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_URGENTE.getValor()))
					    	.findAny()
					    	.orElse(null);	 
				 
			 }
			 
			 String alcMuncipio = codigoPostalRepositoryImpl.getMunicipioByCp(ticket.getCp());
			 responseTicket.setAlcMun(alcMuncipio);
			 
			 //obtenemos el id
			 PrecioGasolinaRequestDto alcEdo =precioGasolinaService.getAlcEdoByCp(ticket.getCp());
			 alcEdo.setIdCombustible(ticket.getIdCombustible());
			 
			 //consultamos el precio promedio
			 Double precioPorLitro =precioGasolinaService.getPrecioPromAlcMun(alcEdo.getIdEstado(),alcEdo.getIdMunicipio(),alcEdo.getIdCombustible());
			 
			 responseTicket.setCostoPorLitro(precioPorLitro);
			 
			if(ticket.isEsPorLitros()) {
				//consultamos costo por servicio y establcemos el valor
				
				//importe con iva 
				servicioGasolinaAux = calulosPedido.servicioGas(responseTicket.getCostoPorLitro(), ticket.getLitroCarga());
				responseTicket.setEsPorLitros(true);
				responseTicket.setLitroCarga(new Double( String.format("%.2f", Double.parseDouble(ticket.getLitroCarga()))));
				
			}else {
				
			     //primero determino los litros de carga con base al monto
				
				responseTicket.setLitroCarga(calulosPedido.calculaLitrosCarga(responseTicket.getCostoPorLitro(),ticket.getMontoCarga()));
				servicioGasolinaAux = ticket.getMontoCarga();
				responseTicket.setLitroCarga(new Double( String.format("%.2f", responseTicket.getLitroCarga())));
			}
			
			if(ticket.isLavadoAuto()) {
				
				 String precioLavado =this.tarifaService.queryLavadoPrecioByCp(ticket.getCp());
				responseTicket.setPrecioLavadoAuto((precioLavado==null) ? 0 : Double.parseDouble(precioLavado));
				responseTicket.setLavadoAuto(true);
				totalImporte+=responseTicket.getPrecioLavadoAuto();
				
			}
			if(ticket.isRevisionNeumaticos()) {
				
				 String precioRevisionNeum =this.tarifaService.queryPresionPrecioByCp(ticket.getCp());
				responseTicket.setRevisionNeumaticos(true);
				responseTicket.setPrecioRevisionLlantas((precioRevisionNeum==null) ? 0 : Double.parseDouble(precioRevisionNeum));
				totalImporte+=responseTicket.getPrecioRevisionLlantas();
			}
			
			//servicio de gasolina
			Double costoPorServicio    = Double.parseDouble(costoService.getValor());
			Double servicioGasolina    = servicioGasolinaAux;
			totalImporte               += costoPorServicio+servicioGasolina;
			Double subTotal            = totalImporte/1.16;
			Double iva                 = totalImporte-subTotal;
			Double totalAPagar         = subTotal+iva;
			
			
			
			//datos del pago
			responseTicket.setCostoPorLitro(new Double(String.format("%.2f", responseTicket.getCostoPorLitro())));
			responseTicket.setServicioGasolina(new Double(String.format("%.2f",servicioGasolina)));
			responseTicket.setTotalImporte(new Double(String.format("%.2f",totalImporte)));
			responseTicket.setTotalConDescuento(new Double(String.format("%.2f",totalImporte)));
			responseTicket.setIva(new Double(String.format("%.2f", iva)));
			responseTicket.setSubTotal(new Double(String.format("%.2f", subTotal)));
			responseTicket.setTotalAPagar(new Double(String.format("%.2f", totalAPagar)));
			responseTicket.setCostoServicio( new Double(String.format("%.2f",  Double.parseDouble(costoService.getValor()))));
			
			//otros
			responseTicket.setIdCombustible(ticket.getIdCombustible());
			responseTicket.setTipoPedidoP(tp.getIdTipoPedido());
		
			
			
			response.setCode("FG020");
    		response.setSuccess(true);
    		response.setData(responseTicket);
    		response.setMessage("Se genero su ticket exitosamente");
    	
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * Obtiene la disponibiliad
     * @param fecha
	 * @param id
     * @param cp
	 * @return
	 */
	@GetMapping("/disponibiliad")
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> disponibilidad(@RequestParam(name = "fecha")String fecha,@RequestParam(name="cp")String cp){
		
		RestResponse response = new RestResponse();
		try {
    		response.setSuccess(true);
    		response.setData(this.pedidoService.disponibilidad(fecha,cp));
    		response.setCode("FG008");
    		response.setMessage("Consulta exitosa");
    	
    	 }
		catch(PedidoException ex) {
			
			response.setSuccess(false);
    		response.setCode(ex.getCodeError());
    		response.setMessage(ex.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * Califica al despachador el cliente
     * @param idPedido
	 * @param calificacion
     * @param comentario
     * @param oauth
	 * @return
	 */
	@PutMapping("/califica/cliente")
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> califica(@RequestParam(name = "idPedido")BigInteger idPedido ,
			   @RequestParam(name = "calificacion")Integer calificacion,
			   @RequestParam(name = "comentario")String comentario,@RequestHeader(value="Authorization") String oauth){
		
		RestResponse response = new RestResponse();
		Cliente cliente;
		
		try {
			
			
		    cliente = this.clienteService.getClienteFromToken(oauth);
		     
		     if(cliente == null){
		          response.setSuccess(false);
		          response.setCode("FG007");
		          response.setMessage("Intentas acceder a recurso no permitido");
		          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		        }			
			
    		response.setSuccess(true);
    		this.calificaPedidoService.calificaClienteDespachador(idPedido, calificacion,comentario);
    		response.setData(null);
    		response.setMessage("Calificacion exitosa");
    	
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * Califica al cliente el despachador
     * @param idPedido
	 * @param calificacion
     * @param comentario
	 * @return
	 */
	@PutMapping("/califica/despachador")
	@RolesAllowed({"ROLE_DESPACHADOR"})
	public ResponseEntity<RestResponse> calificaDespachador(@RequestParam(name = "idPedido")BigInteger idPedido ,
			   @RequestParam(name = "calificacion")Integer calificacion,
			   @RequestParam(name = "comentario")String comentario){
		
		RestResponse response = new RestResponse();
		try {
    		response.setSuccess(true);
    		this.calificaPedidoService.calificaDespachadorCliente(idPedido, calificacion,comentario);
    		response.setData(null);
    		response.setMessage("Calificacion exitosa");
    	
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * Actualiza el estatus del pedido
	 * En Camino, Iniciado y Terminado
     * @param idPedido
     * @param estatus
	 * @param calificacion
     * @param ubicacion
	 * @return
	 */
	@PutMapping("/update/estatus")
	@RolesAllowed({"ROLE_DESPACHADOR","ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> updateEstatus(@RequestParam(name = "idPedido")BigInteger idPedido ,
			   @RequestParam(name = "estatus")Integer estatus,@RequestBody(required = false) UbicacionDespachadorRequestDto ubicacion ){
		
		RestResponse response = new RestResponse();
		try {
    		response.setSuccess(true);
    		this.pedidoService.updateStatus(estatus,idPedido,ubicacion);
    		response.setData(null);
    		response.setCode("FG014");
    		response.setMessage("Se actualizó el estatus del pedido");
    	
    	 }
		catch(ConectaException conkt) {
			
			response.setSuccess(false);
			response.setCode("ERRCKT");
    		response.setMessage(conkt.getMessage());
		}
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	
	/**
	 * Actualiza el estatus del pedido
	 * En Camino, Iniciado y Terminado
     * @param compra
	 * @param calificacion
	 * @return
	 */
	@PutMapping("/finalizar")
	@RolesAllowed({"ROLE_DESPACHADOR","ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> finalizaPedido(@RequestBody FinalizarCompraRequestDto compra){
		
		RestResponse response = new RestResponse();
		try {
    		response.setSuccess(true);
    		response.setData(this.pedidoService.finalizarPedido(compra));
    		response.setCode("FG014");
    		response.setMessage("Se actualizó el estatus del pedido");
    	
    	 }
		catch(ConectaException conkt) {
			
			response.setSuccess(false);
			response.setCode("ERRCKT");
    		response.setMessage(conkt.getMessage());
		}
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * AuThOr: Abraham VARGAS 
	 * @param cancelar
	 * @param oauth
	 * @return
	 */
	@PutMapping("/cliente/cancelacion")
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> cancelaCliente(@RequestBody CancelarServicioDto cancelar,
			@RequestHeader(value="Authorization") String oauth){
		
		RestResponse response = new RestResponse();
		Cliente cliente;
		
		try {
			
		    cliente = this.clienteService.getClienteFromToken(oauth);
		     
		     if(cliente == null){
		          response.setSuccess(false);
		          response.setCode("FG007");
		          response.setMessage("Intentas acceder a recurso no permitido");
		          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		        }			
	
    		response.setSuccess(true);
    		this.pedidoService.cancelaPedidoCliente(cancelar);
    		response.setData(null);
    		response.setMessage("Cancelación exitosa");
    	
    	 }
		catch(PedidoException ex) {
			
			
			response.setSuccess(false);
    		response.setCode(ex.getCodeError());
    		response.setMessage(ex.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * Califica pedido
     * @param cancelar
	 * @param calificacion
	 * @return
	 */
	@PutMapping("/despachador/cancelacion")
	@RolesAllowed({"ROLE_DESPACHADOR"})
	public ResponseEntity<RestResponse> cancelaDespachador(@RequestBody CancelarServicioDto cancelar ){
		
		RestResponse response = new RestResponse();
		try {
    		response.setSuccess(true);
    		this.pedidoService.cancelaPedidoDespachador(cancelar);
    		response.setData(null);
    		response.setMessage("Cancelacion exitosa");
    	
    	 }
		catch(ConectaException ex) {
			response.setSuccess(false);
    		response.setCode("ERRCKT");
    		response.setMessage(ex.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		catch(NumberFormatException e) {
			response.setSuccess(false);
    		response.setCode("ADMERR");
    		response.setMessage("Contacte al administrador del sistema");
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}

	
	
}
