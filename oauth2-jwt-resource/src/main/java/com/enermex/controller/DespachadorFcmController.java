package com.enermex.controller;

import java.util.List;

import com.enermex.modelo.UsuarioEntity;
import com.enermex.modelo.DespachadorFcmEntity;
import com.enermex.service.DespachadorFcmService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador de tokens FCM del usuario
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 24 de febrero de 2020
 * Última modificación: 28 de febrero de 2020
 */
@RestController
@RequestMapping("/api/despachadores/fcm")
public class DespachadorFcmController {
  private Logger logger = LoggerFactory.getLogger(DespachadorFcmController.class);

  @Autowired
  private DespachadorFcmService service;

  @Autowired
  Token token;

  /**
   * Añade un token FCM del dispositivo del usuario
   * 
   * @param oauth
   * @param fcm
   * @return
   */
  @PreAuthorize("hasRole('ROLE_DESPACHADOR')")
  @PostMapping()
  public RestResponse<Void> postDireccion(@RequestHeader(value="Authorization") String oauth, @RequestBody DespachadorFcmEntity fcm) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del usuario desde oAuth
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth, response);
    
    if(usuario == null) {
      return response;
    }

    // Se asocia al token FCM con el identificador del usuario
    fcm.setIdDespachador(usuario.getIdUsuario());

    // Bandera que indica si el token FCM se pudo almacenar
    boolean valido = service.create(fcm);

    // Preparación de la respuesta
    response.setSuccess(valido);

    return response;
  }

  /**
   * Elimina un token FCM del dispositivo del usuario
   * 
   * @param oauth
   * @param fcm
   * @return
   */
  @PreAuthorize("hasRole('ROLE_DESPACHADOR')")
  @DeleteMapping()
  public RestResponse<Void> deleteUsuario(@RequestHeader(value="Authorization") String oauth, @RequestBody DespachadorFcmEntity fcm) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del usuario desde oAuth
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth, response);
    
    if(usuario == null) {
      return response;
    }

    // Consulta de los tokens FCM del usuario
    List<DespachadorFcmEntity> fcms = service.queryByIdDespachador(usuario.getIdUsuario());

    // Bandera que indicará si el token FCM se pudo eliminar
    boolean valido = false;

    // Se busca el token FCM del usuario para eliminarlo
    if(fcms != null) {
      for(DespachadorFcmEntity row : fcms) {
        if(row.getFcm().equals(fcm.getFcm())) {
          valido = service.delete(row);
          break;
        }
      }
    }

    // Preparación de la respuesta
    response.setSuccess(valido);

    return response;
  }
}
