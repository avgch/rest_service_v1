package com.enermex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.utilerias.RestResponse;
import com.enermex.dto.PipaDto;
import com.enermex.modelo.CatCamara;
import com.enermex.modelo.Pipa;
import com.enermex.service.CatCamaraService;
import com.enermex.service.PipaCamaraService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/camaras")
public class CatCamaraController {
	
	
	RestResponse response;
	
	@Autowired
	CatCamaraService catCamaraService;
	
	@Autowired
	PipaCamaraService pipaCamaraService;
	
	
	/**
	 * Obtiene las camaras disponibles
	 * @return
	 */
	@GetMapping("/disponibles")
	@PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
	public ResponseEntity<RestResponse> getCamaras(){
	
		response = new RestResponse();
	try {
		response.setData(catCamaraService.camarasDisponibles());
		response.setCode("200");
		response.setSuccess(true);
		response.setMessage("Consulta exitosa");
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }
    	catch(Exception e) {
    	
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
	/**
	 * Obtiene las camaras activas no asignadas
	 * @return
	 */
	@GetMapping
	@PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
	public ResponseEntity<RestResponse> getCamarasActivasNoAsignadas(){
	
		response = new RestResponse();
	try {
		response.setData(catCamaraService.camarasExistentes());
		response.setCode("200");
		response.setSuccess(true);
		response.setMessage("Consulta exitosa");
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }
    	catch(Exception e) {
    	
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
	/**
	 * Guarda camara
	 * @param camara
	 * @return
	 */
	@PostMapping
	@PreAuthorize("hasRole('ROLE_WEB')")
	public ResponseEntity<RestResponse> save(@RequestBody CatCamara camara){
		
		response = new RestResponse();
		try {

      // INTER: José Antonio González Hernández
      // REQ: Itzia V. - No se debe repetir el nombre de las cámaras
      List<CatCamara> camaras = catCamaraService.camarasExistentes();

      if(camaras != null) {
        for(CatCamara item : camaras) {
          if(item.getNombre() != null && camara.getNombre() != null && item.getNombre().trim().equalsIgnoreCase(camara.getNombre().trim())) {
            response.setSuccess(false);
            response.setMessage("Una cámara con el nombre \"" + camara.getNombre() + "\" ya está registrada");

            return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
          }
        }
      }

      // INTER: END
			
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Registro exitoso");
    		catCamaraService.create(camara);
			response.setData(null);

			return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
    /**
     * Consulta de pipa por id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
    public ResponseEntity<RestResponse> getCamaraById(@PathVariable Integer id) {
    	response = new RestResponse();
    	try {
    		
    		response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Consulta de pipa id exitosa");
    		CatCamara camara = this.catCamaraService.findPipaById(id);
    		response.setData(camara);
    		
    		
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }

    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	}
    
    }
	
    /**
     * Actualiza camara
     * @param id
     * @return
     */
    @PutMapping
    @PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
    public ResponseEntity<RestResponse> updateCamara(@RequestBody CatCamara camara) {
    	response = new RestResponse();
    	try {

        // INTER: José Antonio González Hernández
        // REQ: Itzia V. - No se debe repetir el nombre de las cámaras
        List<CatCamara> camaras = catCamaraService.camarasExistentes();

        if(camaras != null) {
          for(CatCamara item : camaras) {
            if(item.getIdCamara().intValue() != camara.getIdCamara().intValue() && item.getNombre() != null && camara.getNombre() != null && item.getNombre().trim().equalsIgnoreCase(camara.getNombre().trim())) {
              response.setSuccess(false);
              response.setMessage("Una cámara con el nombre \"" + camara.getNombre() + "\" ya está registrada");

              return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
            }
          }
        }

        // INTER: END
    		
    		response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualización exitosa");
    		this.catCamaraService.updateCamara(camara);
    		
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }

    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	}
    
    }
    
    
    /**
     * Consulta de pipa por id
     * @param id
     * @return
     */
    @GetMapping("/selected/{id}")
    @PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
    public ResponseEntity<RestResponse> getSelected(@PathVariable Integer id) {
    	response = new RestResponse();
    	try {
    		
    		response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Consulta de camaras ");
    		List<CatCamara> camaras = this.catCamaraService.getAllForSelected(id);

    		response.setData(camaras);
    		
    		
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }

    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	}
    
    }
    
    
    
    

}
