package com.enermex.controller;

import java.util.Date;
import java.util.List;

import com.enermex.dto.ruta.RutaWeb;
import com.enermex.enumerable.RutaEstatus;
import com.enermex.modelo.RutaEntity;
import com.enermex.service.PermisoService;
import com.enermex.service.RutaService;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/rutas")
public class RutaController {
  private Logger logger = LoggerFactory.getLogger(RutaController.class);

  @Autowired
  private RutaService service;
  
  @Autowired
  private PermisoService permisoService; 

  /**
   * Obtienes
   * @param oauth
   * @return
   */
  @GetMapping
  public RestResponse<List<RutaWeb>> getRutas(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<RutaWeb>> response = new RestResponse<List<RutaWeb>>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "RUTAS", "ACCESS")) {
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    final List<RutaWeb> rutas = service.mapWeb(service.queryAll());
    if (rutas != null) {
      response.setSuccess(true);
      response.setData(rutas);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Obtiene ruta id
   * @param oauth
   * @param id
   * @return
   */
  @GetMapping("/{id}")
  public RestResponse<RutaEntity> getRuta(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id) {
    final RestResponse<RutaEntity> response = new RestResponse<RutaEntity>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "RUTAS", "ACCESS")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    RutaEntity ruta = service.queryById(id);

    if (ruta != null) {
      response.setSuccess(true);
      response.setData(ruta);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("No existe la zona de cobertura solicitada");
    }

    return response;
  }

  /**
   * Guarda ruta
   * @param oauth
   * @param ruta
   * @return
   */
  @PostMapping()
  public RestResponse<Integer> postRuta(@RequestHeader(value="Authorization") String oauth, @RequestBody RutaEntity ruta) {
    final RestResponse<Integer> response = new RestResponse<Integer>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "RUTAS", "CREATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    if(!validRepeated(response, ruta, 0)){
      return response;
    }

    if (service.create(ruta)) {
      response.setSuccess(true);
      response.setData(ruta.getIdRuta());
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Actualiza ruta
   * @param oauth
   * @param id
   * @param ruta
   * @return
   */
  @PutMapping("/{id}")
  public RestResponse<Void> putRuta(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id, @RequestBody RutaEntity ruta) {
    // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "RUTAS", "UPDATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    if(!validRepeated(response, ruta, id)){
      return response;
    }

    // Algunos campos no se deben actualizar por este medio
    RutaEntity db = service.queryById(id);

    if(db != null) {
      ruta.setIdRuta(db.getIdRuta());
      ruta.setFechaCreacion(db.getFechaCreacion());

      if(ruta.getEstatus() == RutaEstatus.INACTIVO && db.getEstatus() != RutaEstatus.INACTIVO) {
        ruta.setFechaBaja(new Date());
      } else {
        ruta.setFechaBaja(db.getFechaBaja());
      }
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Trata de actualizar una zona de cobertura inexistente");
      return response;
    }

    // TODO: Validar ID
    if (service.update(ruta)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

   /**
    * Elimina ruta
    * @param oauth
    * @param id
    * @return
    */
  @DeleteMapping("/{id}")
  public RestResponse<Integer> deleteRuta(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id) {
    // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
    final RestResponse<Integer> response = new RestResponse<Integer>();

    if(!permisoService.testPerm(oauth, "RUTAS", "DELETE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    RutaEntity ruta = service.queryById(id);

    // TODO: Validar ID
    if (ruta != null && service.delete(ruta)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Valida ruta repetida
   * @param response
   * @param ruta
   * @param idRuta
   * @return
   */
  // REQ: Itzia V. - No debe permitirse crear un nombre de ruta repetido
  private boolean validRepeated(RestResponse<?> response, RutaEntity ruta, int idRuta) {
    // Se obtiene la lista de roles para validar que no existan repetidos
    List<RutaEntity> roles = service.queryAll();

    // Logger si no se pueden obtener los roles
    if(roles == null) {
      logger.error("Error al validar las zonas de cobertura repetidas; se asume que no existen.");
      return true;
    }
    
    // Se valida que el nombre no sea repetido
    for(RutaEntity row : roles){
      if(row.getNombre().trim().equalsIgnoreCase(ruta.getNombre().trim()) && row.getIdRuta().intValue() != idRuta) {
        response.setSuccess(false);
        response.setCode("REQUEST");
        response.setMessage("Ya existe una zona de cobertura con el nombre \"" + ruta.getNombre() + "\"");
        return false;
      }
    }

    // No existen repetidos
    return true;
  }
}