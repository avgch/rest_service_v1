package com.enermex.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;
import com.enermex.dto.NotificacionDto;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.ClienteService;
import com.enermex.service.NotificacionService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/notificacion")
public class NotificacionController {

NotificacionDto notifica;

@Autowired
ClienteService clienteService;


@Autowired
private Token token;

@Autowired
NotificacionService notificacionService;

/**
 * Obtiene notificaciones
 * @param oauth
 * @return
 */
@GetMapping
@RolesAllowed({"ROLE_WEB" ,"ROLE_CLIENTE"})
public ResponseEntity<RestResponse>  getAll(@RequestHeader(value="Authorization") String oauth) {
	RestResponse response = new RestResponse();
	List<NotificacionDto> notificaciones = new ArrayList<>();
  try {
     System.out.println("consulta de notificaciones");
 
     notificaciones.add(notifica);
     response.setSuccess(true);
     response.setCode("200");
     response.setMessage("Consulta exitosa");
     response.setData(notificacionService.getAll());
       
        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
       
       }
       catch(Exception e) {
       
        response.setCode("500");
   response.setMessage(e.getMessage());
       
   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
       }
}

/**
 * Obtiene id de notificaciones
 * @param id
 * @return
 */
@GetMapping("/{id}")
@RolesAllowed({"ROLE_WEB" })
public ResponseEntity<RestResponse>  getNotificacionById(@PathVariable Integer id) {
	RestResponse response = new RestResponse();
	NotificacionDto notificaciones = new NotificacionDto();
  try {
     System.out.println("consulta de notificacion por id");
 
     
     response.setSuccess(true);
     response.setCode("200");
     response.setMessage("Consulta exitosa");
     
       
        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
       
       }
       catch(Exception e) {
       
        response.setCode("500");
   response.setMessage(e.getMessage());
       
   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
       }
   
}


/**
 * Guarda notificaciones
 * @param oauth
 * @param notificacion
 * @return
 */
@PostMapping
@RolesAllowed({"ROLE_WEB" })
public ResponseEntity<RestResponse>  save(@RequestHeader(value="Authorization") String oauth,@RequestBody NotificacionDto notificacion) {
	RestResponse response = new RestResponse();
	List<NotificacionDto> notificaciones = new ArrayList<>();
  try {
     System.out.println("Guardando notificacion: "+notificacion);
     UsuarioEntity usuario = token.getUsuarioFromToken(oauth);
     
     response.setSuccess(true);
     response.setCode("200");
     notificacion.setCreadoPor(usuario.getNombre()+" "+usuario.getApellidoPaterno());
     notificacionService.save(notificacion);        
     response.setMessage("Registro exitoso");
     response.setData(null);
       
        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
       
       }
       catch(Exception e) {
       
        response.setCode("500");
        response.setMessage(e.getMessage());
       
   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
       }
   
}
/**
 * Obtiene notificaciones de despachador
 * @return
 */
@GetMapping("/despachadores")
@RolesAllowed({"ROLE_WEB" ,"ROLE_DESPACHADOR"})
public ResponseEntity<RestResponse>  getAllNotiDespachadores() {
	RestResponse response = new RestResponse();
	
  try {
     System.out.println("consulta de notificaciones");
     response.setSuccess(true);
     response.setCode("200");
     response.setMessage("Consulta exitosa");
     response.setData(notificacionService.getAllNotiDespachadores());
       
        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
       
       }
       catch(Exception e) {
       
        response.setCode("500");
   response.setMessage(e.getMessage());
       
   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
       }
}

    /**
     *
     * @param oauth
     * @return
     */
    @GetMapping("/clientes")
@RolesAllowed({"ROLE_CLIENTE"})
public ResponseEntity<RestResponse>  getAllNotiClientes(@RequestHeader(value="Authorization") String oauth) {
	RestResponse response = new RestResponse();
	
	Cliente cliente = this.clienteService.getClienteFromToken(oauth);
  try {
	   
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
		     System.out.println("consulta de notificaciones clientes");
		     response.setSuccess(true);
		     response.setCode("200");
		     response.setMessage("Consulta exitosa");
		     response.setData(notificacionService.getAllNotiClientes(cliente.getIdCliente()));
       
        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
       
       }
       catch(Exception e) {
       
        response.setCode("500");
   response.setMessage(e.getMessage());
       
   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
       }
}





}
