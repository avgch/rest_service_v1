package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.json.PasswordChange;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.PermisoEntity;
import com.enermex.modelo.RolEntity;
import com.enermex.modelo.TokenEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.ClienteService;
import com.enermex.service.PermisoService;
import com.enermex.service.TokenService;
import com.enermex.service.UsuarioService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.utilerias.UrlSigner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador de cuentas de usuarios
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 17 de enero de 2020
 */
@RestController
@RequestMapping("/api/usuarios/account")
public class UsuarioAccountController {
  private Logger logger = LoggerFactory.getLogger(UsuarioAccountController.class);

  @Autowired
  private UsuarioService service;

  @Autowired
  private ClienteService clienteService;

  @Autowired
  private PermisoService permisoService;

  @Autowired
  SmtpGmailSsl sendMail;

  // REQ: Itzia V. - Un token no debe usarse más de una vez
  @Autowired
  private UrlSigner signer;
  
	@Autowired
	private TokenService tokenService;

  /**
   * Inicia el flujo de recuperación de la contraseña
   * 
   * @param correo Dirección de correo electrónico, como parámetro de Url
   * @return
   */
  @PostMapping("/recovery")
  public RestResponse<Void> recovery(@RequestParam(name = "correo") String correo) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Se obtiene al usuario por su dirección de correo
    UsuarioEntity usuario = service.queryByCorreo(correo);

    
    // Se verifica que el usuario sea válido
    if(usuario == null || usuario.getEstatus() == UsuarioEstatus.INACTIVO) {
      response.setMessage("No existe el usuario solicitado");
      response.setSuccess(false);
      return response;
    }

    // REQ: Itzia V. - No se debe permitir usar esta funcionalidad si tiene un rol sin permisos
    List<PermisoEntity> permisos = permisoService.queryAll();
    boolean permitido = false;

    for(RolEntity rol : usuario.getRoles()) {
      for(PermisoEntity perm : permisos){
        if(perm.getId().getPantalla().equals("BACKOFFICE")
            && rol.getIdRol() == perm.getId().getIdRol()
            && perm.getAsignado()) {
          permitido = true;
        }
      }
    }

    if(usuario.getEstatus() == UsuarioEstatus.INACTIVO) {
      response.setMessage("Correo electrónico inválido, favor de verificarlo");
      response.setSuccess(false);
      return response;
    }

    if(usuario.getEstatus() == UsuarioEstatus.POR_VERIFICAR) {
      response.setMessage("Correo electrónico inválido, debe finalizar su proceso de registro");
      response.setSuccess(false);
      return response;
    }

    // REQ: Itzia V. - Un token no debe usarse más de una vez
    TokenEntity tokenEntity = new TokenEntity();
    tokenEntity.setAplicacion("W");
    tokenEntity.setIdUsuario(usuario.getIdUsuario());

    if(tokenService.create(tokenEntity)){
      final String token = signer.sign(tokenEntity.getIdToken().toString());
      sendMail.sendEmail(correo, token, "recoveryPass", "WEB");

      // REQ: Movil e Itzia V. - Que indique que la operación fue exitosa
      response.setMessage("El correo para el restablecimiento de contraseña se envío exitosamente");
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setMessage("Error en el envío de correo");
    }

    return response;
  }

  /**
   * Verifica que el token de recuperación sea válido
   * 
   * @param token Token por validar
   * @param app Aplicación a la que se hace énfasis [WEB, MOVIL, DESPACHADOR]
   * @return
   */
  @GetMapping("/test")
  public RestResponse<Void> testToken(@RequestParam(name = "token") String token, @RequestParam(name = "app") String app) {
    RestResponse<Void> response = new RestResponse<>();

    // REQ: Itzia V. - Un token no debe usarse más de una vez
    String tokenIdStr = signer.unbox(token);

    // Validación de tokens de la versión anterior
    BigInteger tokenId;

    try {
      tokenId = new BigInteger(tokenIdStr);
    } catch (Exception e) {
      response.setSuccess(false);
      response.setMessage("Solicitud expirada, por favor genera de nuevo tu correo.");
      return response;
    }

    TokenEntity tokenEntity = tokenService.queryById(tokenId);

    // Verifica si fue utilizado anteriormente
    if(tokenEntity == null || !tokenEntity.getVigente()) {
      response.setSuccess(false);
      response.setMessage("Operación inválida, por favor genera de nuevo tu correo.");
      return response;
    }

    // Verifica la fecha de expiración
    if(!tokenService.validDate(tokenEntity)) {
      response.setSuccess(false);
      response.setMessage("Solicitud expirada, por favor genera de nuevo tu correo.");
      return response;
    }

    // Verifica que el usuario o cliente exista y sea válido
    if(!tokenService.validUser(tokenEntity)) {
      response.setSuccess(false);
      response.setMessage("Solicitud inválida, por favor genera de nuevo tu correo.");
      return response;
    }

    response.setSuccess(true);
    return response;
  }

  /**
   * Actualiza la contraseña de un usuario determinado
   * 
   * @param newPassword Objeto con token y nueva contraseña
     * @param app
   * @return
   */
  @PostMapping("/password")
  public RestResponse<Void> updatePass(@RequestBody PasswordChange newPassword, @RequestParam(name = "app") String app) {
    RestResponse<Void> response = new RestResponse<>();
    
    // REQ: Itzia V. - Un token no debe usarse más de una vez
    String tokenIdStr = signer.unbox(newPassword.getToken());
    
    // Validación de tokens de la versión anterior
    BigInteger tokenId;

    try {
      tokenId = new BigInteger(tokenIdStr);
    } catch (Exception e) {
      response.setSuccess(false);
      response.setMessage("Solicitud expirada, por favor genera de nuevo tu correo.");
      return response;
    }

    TokenEntity tokenEntity = tokenService.queryById(tokenId);

    // Verifica si fue utilizado anteriormente
    if(tokenEntity == null || !tokenEntity.getVigente()) {
      response.setSuccess(false);
      response.setMessage("Operación inválida, por favor genera de nuevo tu correo.");
      return response;
    }

    // Verifica la fecha de expiración
    if(!tokenService.validDate(tokenEntity)) {
      response.setSuccess(false);
      response.setMessage("Solicitud expirada, por favor genera de nuevo tu correo.");
      return response;
    }

    // Verifica que el usuario o cliente exista y sea válido
    if(!tokenService.validUser(tokenEntity)) {
      response.setSuccess(false);
      response.setMessage("Solicitud inválida, por favor genera de nuevo tu correo.");
      return response;
    }

    if(app.equalsIgnoreCase("MOVIL")){
      Cliente cliente = tokenService.getCliente(tokenEntity);
      
      if(cliente != null){
        clienteService.updatePass(newPassword.getPassword(), cliente.getIdCliente());
        tokenService.use(tokenEntity);
        response.setSuccess(true);
      } else {
        response.setSuccess(false);
        response.setMessage("Solicitud inválida, por favor genera de nuevo tu correo.");
      }
    } else {
      // Se obtiene el usuario guardado en el token
      UsuarioEntity usuario = tokenService.getUsuario(tokenEntity);

      System.out.println(usuario.getCorreo());

      // Si el usuario existe y es válido, se efectúa el cambio de contraseña
      if (usuario != null && service.updatePassword(usuario.getCorreo(), newPassword.getPassword())) {
        tokenService.use(tokenEntity);
        response.setSuccess(true);
      } else {
        response.setSuccess(false);
        response.setMessage("Error inesperado, por favor intenta de nuevo más tarde");
      }
    }

    return response;
  }
}
