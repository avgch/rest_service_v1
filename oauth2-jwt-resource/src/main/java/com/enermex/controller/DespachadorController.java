package com.enermex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;

import com.enermex.modelo.UsuarioEntity;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;
import com.enermex.service.DespachadorService;
import com.enermex.service.UsuarioService;
import com.enermex.service.RolService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/despachador")
public class DespachadorController {

  @Autowired
  DespachadorService despachadorService;
 
  @Autowired
  UsuarioService usuarioService;
 
  @Autowired
  RolService  rolService;

  @Resource(name="passwordEncoderBean")
  private PasswordEncoder passwordEncoder;

 
 
  @Autowired
  Token token;

/**
* Obtiene el perfil Despachador
* @param correo
* @return
*/
@GetMapping("/perfil")
@PreAuthorize("hasRole('ROLE_DESPACHADOR')")
public ResponseEntity<RestResponse> getClienteById(@RequestParam(name = "correo")String correo){

RestResponse response = new RestResponse();
UsuarioEntity despachador;

try {

response.setCode("200");
    response.setSuccess(true);
    response.setMessage("Consulta exitosa");
    despachador = despachadorService.findDespachadorByCorreo(correo);
    response.setData(despachador);
    }
    catch(Exception e) {
   
    response.setSuccess(false);
    response.setCode("500");
    response.setMessage(e.getMessage());
    return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
 


return new ResponseEntity<RestResponse>(response, HttpStatus.OK);

  }
  /**
   * Actualiza correo por id cliente
   * @param id
   * @param correo
   * @return
   */
  @PutMapping("{id}/correo")
  @PreAuthorize("hasRole('ROLE_DESPACHADOR')")
  public RestResponse<Void> putCorreo(@PathVariable("id") long id, @RequestParam(name = "value") String correo) {
    RestResponse<Void> response = new RestResponse<Void>();
    UsuarioEntity usuario = usuarioService.queryById(id);

    if(usuario == null) {
      response.setSuccess(false);
      response.setMessage("Error al actualizar el correo");
      return response;
    }

    // Validamos que el correo no exista
    UsuarioEntity repetido = usuarioService.queryByCorreo(correo);

    if(repetido != null && repetido.getIdUsuario().intValue() != usuario.getIdUsuario().intValue()) {
      response.setSuccess(false);
      response.setMessage("La dirección de correo ya ha sido utilizada");
      return response;
    }

    // Asignamos todos los valores menos el correo
    usuario.setCorreo(correo);

    if(usuarioService.update(usuario)) {
      response.setSuccess(true);
      response.setMessage("Correo electrónico modificado correctamente");
      return response;
    } else {
      response.setSuccess(false);
      response.setMessage("Error al actualizar el correo");
      return response;
    }
  }
 
  /**
   * Actualiza password por id cliente
   * @param id
   * @param pass
   * @param old
   * @return
   */
  @PutMapping("{id}/pass")
  @PreAuthorize("hasRole('ROLE_DESPACHADOR')")
  public RestResponse<Void> putPass(@PathVariable("id") long id, @RequestParam(name = "value") String pass, @RequestParam(name = "old") String old) {
    RestResponse<Void> response = new RestResponse<Void>();
    UsuarioEntity usuario = usuarioService.queryById(id);

    if(usuario == null) {
      response.setSuccess(false);
      response.setMessage("Error al actualizar la contraseña");
      return response;
    }

    // Se valida la contraseña anterior
    String pasada = usuarioService.findPassword(usuario.getIdUsuario());

    // Asignamos todos los valores menos el correo
    if(!passwordEncoder.matches(old, pasada)) {
      response.setSuccess(true);
      response.setMessage("Contraseña actual incorrecta");
      return response;
    }

    if(usuarioService.updatePassword(usuario.getCorreo(), pass)) {
      response.setSuccess(true);
      response.setMessage("Se ha modificado correctamente la contraseña");
      return response;
    } else {
      response.setSuccess(false);
      response.setMessage("Error al actualizar la contraseña");
      return response;
    }
  }

  /**
   * Actualiza telefono
   * @param id
   * @param telefono
   * @return
   */
  @PutMapping("{id}/telefono")
  @PreAuthorize("hasRole('ROLE_DESPACHADOR')")
  public RestResponse<Void> putTelefono(@PathVariable("id") long id, @RequestParam(name = "value") String telefono) {
    RestResponse<Void> response = new RestResponse<Void>();
    UsuarioEntity usuario = usuarioService.queryById(id);

    if(usuario == null) {
      response.setSuccess(false);
      response.setMessage("Error al actualizar el teléfono");
      return response;
    }

    // Validamos que el telefono no exista
    boolean existsTelefono = usuarioService.existsTelefono(telefono);

    if(existsTelefono && !usuario.getTelefono().equals(telefono)) {
      response.setSuccess(false);
      response.setMessage("El número telefónico ya ha sido utilizado");
      return response;
    }

    // Asignamos todos los valores menos el correo
    usuario.setTelefono(telefono);

    if(usuarioService.update(usuario)) {
      response.setSuccess(true);
      response.setMessage("Número telefónico modificado correctamente");
      return response;
    } else {
      response.setSuccess(false);
      response.setMessage("Error al actualizar el teléfono");
      return response;
    }
}
 
  /**
   * Obtiene todos los despachadores
   * @param oauth
   * @return
   */
  @GetMapping
  @RolesAllowed({"ROLE_WEB" })
  public ResponseEntity<RestResponse> getAllDespachadores(@RequestHeader(value="Authorization") String oauth) {
 
 RestResponse response = new RestResponse();
 
 
 System.out.println("Guardando notificacion: ");
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth);
    if(usuario!=null) {
   
    response.setSuccess(false);
        response.setMessage("Recurso no permitido");
    }
   
    response.setSuccess(false);
         response.setMessage("Despachadores");
         response.setData(despachadorService.getDespachadores());
   
 
 return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
   
}
 
  /**
   * Obtiene los despachadores a notificar
   * @param oauth
   * @return
   */
  @GetMapping("/notifica")
  @RolesAllowed({"ROLE_WEB" })
  public ResponseEntity<RestResponse> getAllDespachadoresNotificar(@RequestHeader(value="Authorization") String oauth) {
 
 RestResponse response = new RestResponse();
 
 
 System.out.println("Guardando notificacion: ");
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth);
    if(usuario!=null) {
   
    response.setSuccess(false);
        response.setMessage("Recurso no permitido");
    }
   
    response.setSuccess(true);
         response.setMessage("Despachadores");
         response.setData(despachadorService.getDespachadores());
   
 
 return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
   
}
 
 
 
 
}
