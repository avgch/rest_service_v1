package com.enermex.controller;

import java.util.List;

import com.enermex.dto.queja.QuejaMovil;
import com.enermex.dto.queja.QuejaAsunto;
import com.enermex.enumerable.QuejaEstatus;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.QuejaEntity;
import com.enermex.service.QuejaService;
import com.enermex.utilerias.FirebaseRealtime;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGenerico;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/clientes/quejas")
public class ClienteQuejaController {
  private Logger logger = LoggerFactory.getLogger(ClienteQuejaController.class);

  @Autowired
  private QuejaService service;

  @Autowired
  private Token token;

  @Autowired
  private FirebaseRealtime realtime;

  /**
   * Obtiene todas las quejas
   * @param oauth
   * @return
   */
  @GetMapping
  public RestResponse<List<QuejaMovil>> getQuejas(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<QuejaMovil>> response = new RestResponse<List<QuejaMovil>>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }

    // Consulta de las quejas asociadas al cliente
    final List<QuejaMovil> quejas = service.mapMovil(service.queryByCliente(cliente.getIdCliente()));

    // Preparación de la respuesta
    response.setData(quejas);
    response.setSuccess(quejas != null);
    response.setMessage(quejas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Guarda queja
   * @param oauth
   * @param asunto
   * @return
   */
  @PostMapping()
  public RestResponse<Long> postQueja(@RequestHeader(value="Authorization") String oauth, @RequestBody QuejaAsunto asunto) {
    final RestResponse<Long> response = new RestResponse<Long>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }

    // Queja que se creará y su detalle
    QuejaEntity queja = new QuejaEntity();
    queja.setIdPedido(asunto.getIdPedido());
    queja.setEstatus(QuejaEstatus.ASIGNADA);
    queja.setIdCliente(cliente.getIdCliente());
    
    // Se infieren los datos del pedido
    if(!service.inferencePedido(queja, response)) {
      return response;
    }

    // Bandera que indica si la queja se pudo crear
    boolean valido = service.create(queja) && service.addAsunto(queja.getIdQueja(), asunto.getAsunto());

    // Si se crea la queja, se envía por correo y se notifica al usuario web
    if (valido) {
      service.notifyCliente(cliente, queja, false);
      realtime.queja(service.asignadosCliente());
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(valido ? "Tu queja se ha guardado correctamente con el folio " + queja.getIdQueja() + ", en breve te estará contactando un asesor para brindarte atención" : "Servicio no disponible. Por favor, inténtelo más tarde");
    response.setData(valido ? queja.getIdQueja() : null);
    return response;
  }
  /**
   * Actualiza queja
   * @param oauth
   * @param id
   * @return
   */
  @PutMapping("/{id}/satisfecho")
  public RestResponse<Void> putSatisfecho(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }
    
    // Consulta de la queja 
    QuejaEntity queja = service.queryById(id);

    if (!verificaNulabilidad(cliente, queja, id, response) || !verificaAsociacion(cliente, queja, response)) {
      return response;
    }

    queja.setEstatus(QuejaEstatus.CERRADA);

    // Bandera que indica si la queja se pudo reestablecer
    boolean valido = service.update(queja);

    if (valido) {
      service.notifyCliente(cliente, queja, true);
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(valido ? "Se ha cerrado exitosamente tu queja" : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Cliente insatisfecha 
   * @param oauth
   * @param id
   * @param asunto
   * @return
   */
  @PutMapping("/{id}/insatisfecho")
  public RestResponse<Void> putInsatisfecho(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id, @RequestBody QuejaAsunto asunto) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }
    
    // Consulta de la queja 
    QuejaEntity queja = service.queryById(id);

    if (!verificaNulabilidad(cliente, queja, id, response) || !verificaAsociacion(cliente, queja, response)) {
      return response;
    }

    // Se asigna reestabelce el estatus de la queja
    queja.setEstatus(QuejaEstatus.ASIGNADA);

    // Bandera que indica si la queja se pudo reestablecer
    boolean valido = service.update(queja) && service.addAsunto(queja.getIdQueja(), asunto.getAsunto());

    // Si se recrea la queja se notifica al usuario web
    if (valido) {
      service.notifyCliente(cliente, queja, true);
      realtime.queja(service.asignadosCliente());
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(valido ? "Se ha asignado nuevamente tu queja, en breve te contactará un asesor" : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Obtiene queja por id
   * @param oauth
   * @param id
   * @return
   */
  @GetMapping("/{id}")
  public RestResponse<QuejaMovil> getQueja(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<QuejaMovil> response = new RestResponse<QuejaMovil>();
    
    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }
    
    // Consulta de la queja 
    QuejaEntity queja = service.queryById(id);

    if (!verificaNulabilidad(cliente, queja, id, response) || !verificaAsociacion(cliente, queja, response)) {
      return response;
    }

    // Preparación de la respuesta
    response.setSuccess(true);
    response.setData(service.mapMovil(queja));
    
    return response;
  }

  /**
   * Carga foto de queja
   * @param id
   * @param archivos
   * @return
   */
  @PostMapping("/{id}/upload/fotos")
  public RestResponse<Void> uploadFotos(@PathVariable("id") Long id, @RequestParam("files") MultipartFile[] archivos) {
    final RestResponse<Void> response = new RestResponse<Void>();
    QuejaEntity queja = service.queryById(id);

    boolean hasError = false;

    for(MultipartFile archivo : archivos){
      if (queja == null || archivo.isEmpty() || !service.addFoto(queja, archivo)) {
        hasError = true;
      }
    }

    if (!hasError) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Verifica que la operación sea legal; garantizando que la queja pertenezca al cliente
   * 
   * @param cliente Cliente registrado en base de datos
   * @param direccion Dirección registrada en base de datos
   * @return Resultado de la verificación
   */
  private boolean verificaAsociacion(Cliente cliente, QuejaEntity queja, RestResponse<?> response) {
    if(cliente.getIdCliente().longValue() == queja.getIdCliente().longValue()) {
      return true;
    }

    // TODO: Auditoría
    response.setSuccess(false);
    response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    logger.error("Se intentó acceder a la queja "+ queja.getIdQueja() + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }

  /**
   * Verifica que la operación sea legal; garantizando que la queja exista cuando se solicite
   * 
   * @param cliente Cliente registrado en base de datos
   * @param direccion Dirección registrada en base de datos
   * @return Resultado de la verificación
   */
  private boolean verificaNulabilidad(Cliente cliente, QuejaEntity queja, long idQueja, RestResponse<?> response) {
    if(queja != null) {
      return true;
    }

    // TODO: Auditoría
    response.setSuccess(false);
    response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    logger.error("Se intentó acceder a la queja "+ idQueja + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }

  
}
