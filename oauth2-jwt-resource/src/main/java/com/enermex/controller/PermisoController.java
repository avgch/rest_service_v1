package com.enermex.controller;

import java.util.List;

import com.enermex.modelo.PermisoEntity;
import com.enermex.service.PermisoService;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Obtiene permisos
 * @author abraham
 *
 */
@RestController
@RequestMapping("/api/permisos")
public class PermisoController {
  // NOTE: Permitir roles múltiples cuando se requiera; ignórese entre tanto
  
  Logger logger = LoggerFactory.getLogger(PermisoController.class);

  @Autowired
  private PermisoService service;

    /**
     *
     * @param oauth
     * @return
     */
    @GetMapping()
  public RestResponse<List<PermisoEntity>> getPermisos(@RequestHeader(value="Authorization") String oauth) {
    RestResponse<List<PermisoEntity>> response = new RestResponse<List<PermisoEntity>>();
    
    // Se verifican los permisos para este recurso
    if(!service.testPerm(oauth, "ROLES", "CREATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    List<PermisoEntity> permisos = service.queryAll();

    if(permisos != null) {
      response.setData(permisos);
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
    }

    return response;
  }

  /**
   * Obtiene el permiso por pantalla
   * @param id
   * @param pantalla
   * @param permiso
   * @return
   */
  @GetMapping("{id}/{pantalla}/{permiso}")
  public RestResponse<Boolean> getPermisos(@PathVariable("id") int id, @PathVariable("pantalla") String pantalla, @PathVariable("permiso") String permiso) {
    RestResponse<Boolean> response = new RestResponse<Boolean>();

    // Esta operación no se restringe a permisos asociados
    PermisoEntity perm = service.queryById(id, pantalla, permiso);

    if(perm != null) {
      response.setSuccess(true);
      response.setData(perm.getAsignado());
    } else {
      response.setSuccess(false);
      response.setData(false);
    }

    return response;
  }

  /**
   * Obtiene permiso de pantalla
   * @param id
   * @param pantalla
   * @return
   */
  @GetMapping("{id}/{pantalla}")
  public RestResponse<List<PermisoEntity>> getPermisos(@PathVariable("id") int id, @PathVariable("pantalla") String pantalla) {
    RestResponse<List<PermisoEntity>> response = new RestResponse<List<PermisoEntity>>();

    // Esta operación no se restringe a permisos asociados
    List<PermisoEntity> perm = service.queryById(id, pantalla);

    if(perm != null) {
      response.setSuccess(true);
      response.setData(perm);
    } else {
      response.setSuccess(false);
    }

    return response;
  }
 /**
  * Obtiene permiso por id 
  * @param id
  * @return
  */
  @GetMapping("{id}")
  public RestResponse<List<PermisoEntity>> getPermisos(@PathVariable("id") int id) {
    RestResponse<List<PermisoEntity>> response = new RestResponse<List<PermisoEntity>>();

    // Esta operación no se restringe a permisos asociados
    List<PermisoEntity> perm = service.queryById(id);

    if(perm != null) {
      response.setSuccess(true);
      response.setData(perm);
    } else {
      response.setSuccess(false);
    }

    return response;
  }

  /**
   * Guarda permiso
   * @param oauth
   * @param permisos
   * @return
   */
  @PostMapping()
  public RestResponse<Void> postPermisos(@RequestHeader(value="Authorization") String oauth, @RequestBody List<PermisoEntity> permisos) {
    RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!service.testPerm(oauth, "ROLES", "UPDATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    List<PermisoEntity> rows = service.queryAll();

    if(rows == null) {
      response.setSuccess(false);
      return response;
    }

    // Por defecto, la operación es correcta
    response.setSuccess(true);
    
    // Se buscan y actualizan los permisos
    for(PermisoEntity permiso : permisos){
      boolean finded = false;

      for(PermisoEntity row : rows) {
        if(permiso.getId().equals(row.getId())) {
          finded = true;

          if(permiso.getAsignado() != row.getAsignado()) {
            response.setSuccess(service.update(permiso) && response.isSuccess());
          }
        }
      }

      if(!finded) {
        response.setSuccess(service.create(permiso) && response.isSuccess());
      }
    }

    return response;
  }
}
