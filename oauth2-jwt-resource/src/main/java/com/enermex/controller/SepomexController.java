package com.enermex.controller;

import java.util.List;

import com.enermex.modelo.CodigoPostalEntity;
import com.enermex.modelo.EstadoEntity;
import com.enermex.service.SepomexService;
import com.enermex.utilerias.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/sepomex")
public class SepomexController {

  @Autowired
  SepomexService service;

  /**
   * Obtiene estados
   * @return
   */
  @GetMapping("/estados")
  public RestResponse<List<EstadoEntity>> getEstados() {
    RestResponse<List<EstadoEntity>> response = new RestResponse<List<EstadoEntity>>();
    List<EstadoEntity> estados = service.queryEstados();

    if(estados != null) {
      response.setSuccess(true);
      response.setData(estados);
    } else {
      response.setSuccess(false);
      response.setMessage("Servicio de ubicaciones no disponible");
    }

    return response;
  }

  /**
   * Obtiene codigos postales
   * @param idEstado
   * @param idMunicipio
   * @return
   */
  @GetMapping("/codigos/{idEstado}/{idMunicipio}")
  public RestResponse<List<CodigoPostalEntity>> getCodigos(@PathVariable("idEstado") Integer idEstado, @PathVariable("idMunicipio") Integer idMunicipio) {
    RestResponse<List<CodigoPostalEntity>> response = new RestResponse<List<CodigoPostalEntity>>();
    List<CodigoPostalEntity> codigos = service.queryCodigos(idEstado, idMunicipio);

    if(codigos != null) {
      response.setSuccess(true);
      response.setData(codigos);
    } else {
      response.setSuccess(false);
      response.setMessage("Servicio de ubicaciones no disponible");
    }

    return response;
  }
}
