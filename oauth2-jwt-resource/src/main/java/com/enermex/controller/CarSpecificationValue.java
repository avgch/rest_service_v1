package com.enermex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.service.CarSpecificationValueService;
import com.enermex.utilerias.RestResponse;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/especificacion")
public class CarSpecificationValue {

	@Autowired
	CarSpecificationValueService carSpecificationValueService;
	
	
	/**
	 * Obtiene la especificacion
	 * @param idTrim
	 * @return
	 */
	@GetMapping
	public ResponseEntity<RestResponse> getAll(@RequestParam(name = "idTrim")Integer idTrim){
		
		RestResponse response = new RestResponse();
		try {
			
			
			response.setCode("200");
			response.setMessage("Consulta exitosa");
			response.setData(carSpecificationValueService.getSpecificationValue(idTrim));
			
		}
		catch(Exception e) {
			response.setCode("500");
			response.setMessage("Contacte al administrador del sistema: \n" +e.getMessage());
		
			return new ResponseEntity<RestResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<RestResponse>(response,HttpStatus.OK);
		
		
	}
}
