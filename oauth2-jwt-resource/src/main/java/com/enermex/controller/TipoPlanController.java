package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import javax.websocket.ClientEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.dto.TipoPlanDto;
import com.enermex.dto.ClienteDto;
import com.enermex.service.ClienteService;
import com.enermex.utilerias.RestResponse;
import com.enermex.repository.TipoPlanRepository;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/plan")
public class TipoPlanController {
	
	
	@Autowired
    private TipoPlanRepository tipoPlanRepository;
	
	@Autowired
    private ClienteService clienteService;
	
	RestResponse response;
	
	
	/**
	 * Obtiene planes
	 * @return
	 */
	@GetMapping
	public List<TipoPlanDto> getAll() {
		
		List<TipoPlanDto> lista = (List<TipoPlanDto> )(List<?>) tipoPlanRepository.findAll() ;
		
		
		return lista;
		
	}
	/**
	 * Actualiza plan
	 * @param cliente
	 * @return
	 */
	@PutMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse>  updateFamiliar(@RequestBody ClienteDto cliente) {
		response = new RestResponse();
	
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Invitados exitosa");
    		this.clienteService.updateFamiliar(cliente);
    		
    		
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	

}
