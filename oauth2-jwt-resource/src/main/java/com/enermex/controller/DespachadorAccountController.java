package com.enermex.controller;

import java.util.List;

import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.modelo.PermisoEntity;
import com.enermex.modelo.RolEntity;
import com.enermex.modelo.TokenEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.ClienteService;
import com.enermex.service.PermisoService;
import com.enermex.service.TokenService;
import com.enermex.service.UsuarioService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.utilerias.UrlSigner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/despachadores/account")
public class DespachadorAccountController {
  @Autowired
  private UsuarioService service;

  @Autowired
  private PermisoService permisoService;

  @Autowired
  SmtpGmailSsl sendMail;

  // REQ: Itzia V. - Un token no debe usarse más de una vez
	@Autowired
  private UrlSigner signer;
  
	@Autowired
  private TokenService tokenService;
  
 /**
  * Recuperacion de contrasena (despachador)
  * @param correo
  * @return
  */
  @PostMapping("/recovery")
  public RestResponse<Void> recovery(@RequestParam(name = "correo") String correo) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Se obtiene al usuario por su dirección de correo
    UsuarioEntity despachador = service.queryByCorreo(correo);

    
    // Se verifica que el usuario exista
    if(despachador == null) {
      response.setMessage("Correo electrónico inválido, favor de verificarlo");
      response.setSuccess(false);
      return response;
    }

    // REQ: Itzia V. - No se debe permitir usar esta funcionalidad si tiene un rol sin permisos
    boolean permitido = false;

    for(RolEntity rol : despachador.getRoles()) {
      if(rol.getRol().equals("Despachador")) {
        permitido = true;
      }
    }

    if(!permitido) {
      response.setMessage("Correo electrónico inválido, favor de verificarlo");
      response.setSuccess(false);
      return response;
    }

    if(despachador.getEstatus() == UsuarioEstatus.INACTIVO) {
      response.setMessage("Correo electrónico inválido, favor de verificarlo");
      response.setSuccess(false);
      return response;
    }

    if(despachador.getEstatus() == UsuarioEstatus.POR_VERIFICAR) {
      response.setMessage("Correo electrónico inválido, debe finalizar su proceso de registro");
      response.setSuccess(false);
      return response;
    }

    // REQ: Itzia V. - Un token no debe usarse más de una vez
    TokenEntity tokenEntity = new TokenEntity();
    tokenEntity.setAplicacion("W");
    tokenEntity.setIdUsuario(despachador.getIdUsuario());

    if(tokenService.create(tokenEntity)){
      final String token = signer.sign(tokenEntity.getIdToken().toString());
      sendMail.sendEmail(correo, token, "recoveryPass", "DESPACHADOR");

      // REQ: Movil e Itzia V. - Que indique que la operación fue exitosa
      response.setMessage("El correo para el restablecimiento de contraseña se envío exitosamente");
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setMessage("Error en el envío de correo");
    }

    return response;
  }
}