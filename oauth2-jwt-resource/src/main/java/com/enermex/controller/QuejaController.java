package com.enermex.controller;

import java.util.ArrayList;
import java.util.List;

import com.enermex.dto.queja.QuejaRespuesta;
import com.enermex.dto.queja.QuejaWeb;
import com.enermex.enumerable.QuejaEstatus;
import com.enermex.modelo.QuejaEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.PermisoService;
import com.enermex.service.QuejaService;
import com.enermex.utilerias.FirebaseNotification;
import com.enermex.utilerias.FirebaseRealtime;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpQueja;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api")
public class QuejaController {
  private Logger logger = LoggerFactory.getLogger(QuejaController.class);

  @Autowired
  private QuejaService service;
  
  @Autowired
  private PermisoService permisoService;
  
  @Autowired
  private Token token;

  @Autowired
  private FirebaseRealtime realtime;

  /**
   * Obtiene quejas
   * @param oauth
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/quejas")
  public RestResponse<List<QuejaWeb>> getQuejasCliente(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<QuejaWeb>> response = new RestResponse<List<QuejaWeb>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "QUEJAS", "ACCESS")) {
      return response;
    }

    // Consulta del listado de quejas
    final List<QuejaEntity> quejas = service.queryAllCliente();

    // Preparación de la respuesta
    response.setData(service.mapWeb(quejas));
    response.setSuccess(quejas != null);
    response.setMessage(quejas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }
  /**
   * Obtiene quejas
   * @param oauth
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/quejasd")
  public RestResponse<List<QuejaWeb>> getQuejasDespachador(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<QuejaWeb>> response = new RestResponse<List<QuejaWeb>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "QUEJAS", "ACCESS")) {
      return response;
    }

    // Consulta del listado de quejas
    final List<QuejaEntity> quejas = service.queryAllDespachador();

    // Preparación de la respuesta
    response.setData(service.mapWeb(quejas));
    response.setSuccess(quejas != null);
    response.setMessage(quejas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

    /**
     *
     * @param oauth
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/quejas/limit")
  public RestResponse<List<QuejaWeb>> getQuejasLimitCliente(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<QuejaWeb>> response = new RestResponse<List<QuejaWeb>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "QUEJAS", "ACCESS")){
      return response;
    }

    // Consulta del listado de quejas
    final List<QuejaEntity> quejas = service.queryLimitCliente();

    // Preparación de la respuesta
    response.setData(service.mapWeb(quejas));
    response.setSuccess(quejas != null);
    response.setMessage(quejas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

    /**
     *
     * @param oauth
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/quejasd/limit")
  public RestResponse<List<QuejaWeb>> getQuejasLimitDespachador(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<QuejaWeb>> response = new RestResponse<List<QuejaWeb>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "QUEJAS", "ACCESS")){
      return response;
    }

    // Consulta del listado de quejas
    final List<QuejaEntity> quejas = service.queryLimitDespachador();

    // Preparación de la respuesta
    response.setData(service.mapWeb(quejas));
    response.setSuccess(quejas != null);
    response.setMessage(quejas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

    /**
     *
     * @param oauth
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/quejas/count")
  public RestResponse<Long> getQuejasCountCliente(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<Long> response = new RestResponse<Long>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "QUEJAS", "ACCESS")) {
      return response;
    }

    // Cantidad de quejas asignadas
    final Long quejas = service.asignadosCliente();

    // Preparación de la respuesta
    response.setData(quejas);
    response.setSuccess(quejas != null);
    response.setMessage(quejas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

    /**
     *
     * @param oauth
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/quejasd/count")
  public RestResponse<Long> getQuejasCountDespachador(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<Long> response = new RestResponse<Long>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "QUEJAS", "ACCESS")) {
      return response;
    }

    // Cantidad de quejas asignadas
    final Long quejas = service.asignadosDespachador();

    // Preparación de la respuesta
    response.setData(quejas);
    response.setSuccess(quejas != null);
    response.setMessage(quejas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

    /**
     *
     * @param oauth
     * @param id
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/quejas/{id}")
  public RestResponse<QuejaWeb> getQueja(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<QuejaWeb> response = new RestResponse<QuejaWeb>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "QUEJAS", "ACCESS")) {
      return response;
    }

    // Consulta de la queja solicitada
    QuejaEntity queja = service.queryById(id);

    // Preparación de la respuesta
    response.setData(service.mapWeb(queja));
    response.setSuccess(queja != null);
    response.setMessage(queja != null ? null : "No existe la queja solicitada");

    return response;
  }

    /**
     *
     * @param oauth
     * @param id
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @PutMapping("/quejas/{id}/atencion")
  public RestResponse<Void> putAtencion(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del usuario desde oAuth
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth, response);

    if(usuario == null) {
      return response;
    }

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(usuario, response, "QUEJAS", "UPDATE")) {
      return response;
    }

    // Algunos campos no se deben actualizar
    QuejaEntity db = service.queryById(id);

    //TODO: Verificar nulabilidad
    if(db == null) {
      // TODO: Auditoría
      response.setSuccess(false);
      return response;
    }
    
    // Si la queja era asignada, se intentará emitir a Firebase y se asigna el usuario
    boolean emitRealtime = false;

    if(db.getEstatus() == QuejaEstatus.ASIGNADA) {
      db.setIdUsuario(usuario.getIdUsuario());
      emitRealtime = true;
    } else {
      db.setIdUsuario(db.getIdUsuario());
    }

    db.setEstatus(QuejaEstatus.EN_ATENCION);

    boolean valido = service.update(db);

    // Servicios de mensajería si la actualización es correcta
    if (valido) {

      // Si se requiere, se emite el cambio a Firebase
      if(emitRealtime) {
        if (db.getIdCliente() != null) {
          realtime.queja(service.asignadosCliente());
        } else {
          realtime.quejad(service.asignadosDespachador());
        }
      }
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(valido ? null : "Servicio no disponible. Por favor, inténtelo más tarde");
    return response;
  }

    /**
     *
     * @param oauth
     * @param id
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @PutMapping("/quejas/{id}/cerrar")
  public RestResponse<Void> putCerrar(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del usuario desde oAuth
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth, response);

    if(usuario == null) {
      return response;
    }

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(usuario, response, "QUEJAS", "UPDATE")) {
      return response;
    }

    // Algunos campos no se deben actualizar
    QuejaEntity db = service.queryById(id);

    //TODO: Verificar nulabilidad
    if(db == null) {
      // TODO: Auditoría
      response.setSuccess(false);
      return response;
    }
    
    // Si la queja era asignada, se intentará emitir a Firebase y se asigna el usuario
    boolean emitRealtime = false;

    if(db.getEstatus() == QuejaEstatus.ASIGNADA) {
      db.setIdUsuario(usuario.getIdUsuario());
      emitRealtime = true;
    } else {
      db.setIdUsuario(db.getIdUsuario());
    }

    db.setEstatus(QuejaEstatus.CERRADA);

    boolean valido = service.update(db);

    // Servicios de mensajería si la actualización es correcta
    if (valido) {

      // Si se requiere, se emite el cambio a Firebase

      if(db.getCliente() != null) {
        service.notifyCliente(db.getCliente(), db, false);
      }

      if(db.getDespachador() != null) {
        service.notifyDespachador(db.getDespachador(), db, false);
      }

      if(emitRealtime) {
        if (db.getIdCliente() != null) {
          realtime.queja(service.asignadosCliente());
        } else {
          realtime.quejad(service.asignadosDespachador());
        }
      }
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(valido ? null : "Servicio no disponible. Por favor, inténtelo más tarde");
    return response;
  }
  
    /**
     *
     * @param oauth
     * @param id
     * @param repuesta
     * @return
     */
    @PreAuthorize("hasRole('ROLE_WEB')")
  @PutMapping("/quejas/{id}/atender")
  public RestResponse<Void> putQuejaAtender(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id, @RequestBody QuejaRespuesta repuesta) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del usuario desde oAuth
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth, response);

    if(usuario == null) {
      return response;
    }

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(usuario, response, "QUEJAS", "UPDATE")) {
      return response;
    }

    // Algunos campos no se deben actualizar
    QuejaEntity db = service.queryById(id);

    if(db == null) {
      // TODO: Auditoría
      response.setSuccess(false);
      return response;
    }

    // Bandera que indica si la operación debe emitir a los escuchadores Firebase
    boolean emitRealtime = false;

    // Si la queja era asignada, se intentará emitir a Firebase y se asigna el usuario
    if(db.getEstatus() == QuejaEstatus.ASIGNADA) {
      db.setIdUsuario(usuario.getIdUsuario());
      emitRealtime = true;
    } else {
      db.setIdUsuario(db.getIdUsuario());
    }

    db.setEstatus(QuejaEstatus.ATENDIDA);
    

    // Bandera que indica si se actualizó la queja correctamente
    // TODO: Transaccion
    boolean valido = service.update(db) && service.addRespuesta(db.getIdQueja(), repuesta.getRespuesta());

    // Servicios de mensajería si la actualización es correcta
    if (valido) {

      if(db.getCliente() != null) {
        service.notifyCliente(db.getCliente(), db, false);
      }

      if(db.getDespachador() != null) {
        service.notifyDespachador(db.getDespachador(), db, false);
      }

      // Si se requiere, se emite el cambio a Firebase
      if(emitRealtime) {
        if (db.getIdCliente() != null) {
          realtime.queja(service.asignadosCliente());
        } else {
          realtime.quejad(service.asignadosDespachador());
        }
      }

      // Si se atendió la queja, se notifica al cliente por correo y push notification
      /*if(db.getEstatus() == QuejaEstatus.ATENDIDA) {
        if (db.getIdCliente() != null) {
          smtp.sendEmailAtendido(db.getCliente().getCorreo(), repuesta.getRespuesta());
          notification.aclaracion(db.getCliente(), repuesta.getRespuesta());
        } else {
          smtp.sendEmailAtendido(db.getDespachador().getCorreo(), repuesta.getRespuesta());
          notification.aclaraciond(db.getDespachador(), repuesta.getRespuesta());
        }
      }*/
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(valido ? null : "Servicio no disponible. Por favor, inténtelo más tarde");
    return response;
  }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/quejas/{id}/fotos")
  public RestResponse<List<String>> fotos(@PathVariable("id") Long id) {
    final RestResponse<List<String>> response = new RestResponse<List<String>>();
    final List<String> list = new ArrayList<String>();
    final QuejaEntity queja = service.queryById(id);

    if (queja != null) {
      queja.getEvidencias().forEach(evidencia -> list.add("/api/quejas/foto/" + evidencia.getIdEvidencia()));

      response.setSuccess(true);
      response.setData(list);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("No existe la queja solicitada");
    }

    return response;
  }

    /**
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/quejas/foto/{id}", method = { RequestMethod.GET, RequestMethod.POST }, produces = "image/png")
  public ResponseEntity<InputStreamResource> foto(@PathVariable("id") long id) {
    InputStreamResource resource = service.downloadFoto(id);

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }
}
