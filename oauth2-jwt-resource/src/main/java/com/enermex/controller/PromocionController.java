package com.enermex.controller;

import java.math.BigInteger;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.azure.core.annotation.HeaderParam;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.dto.PromocionDto;
import com.enermex.dto.PromocionUpdateDto;
import com.enermex.dto.ValidaPromoDto;
import com.enermex.modelo.Cliente;
import com.enermex.service.CargaCombustiblesService;
import com.enermex.service.ClienteService;
import com.enermex.utilerias.RestResponse;
import com.enermex.service.PromocionService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/promocion")
public class PromocionController {
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	PromocionService promocionService;
	
	@Autowired
	CargaCombustiblesService cargaCombustiblesService;
	
	/**
	 * Valida promocion
	 * @param promo
	 * @param oauth
	 * @return
	 */
	@PostMapping("/valida")
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse>  aplicaCodigo(@RequestBody  ValidaPromoDto promo, @RequestHeader(value="Authorization") String oauth) {
		RestResponse response = new RestResponse();
		Cliente cliente;
		
		cliente = this.clienteService.getClienteFromToken(oauth);
	  try {
		  
		    if(cliente == null){
		          response.setSuccess(false);
		          response.setCode("FG007");
		          response.setMessage("Intentas acceder a recurso no permitido");
		          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		    }
	     System.out.println("validando codigo de promocion");
	 
	     response.setSuccess(true);
	     response.setCode("200");
	     response.setMessage("Consulta exitosa");
	     response.setData(promocionService.validaPromo(promo, cliente.getIdCliente()));
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	        response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
	/**
	 * Obtiene las notificaciones para MOVILES
	 * @param oauth
     * @param idCliente
	 * @return
	 */
	@GetMapping
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse>  getAll(@RequestHeader(value="Authorization") String oauth, @RequestHeader(name = "idCliente")  BigInteger idCliente) {
		RestResponse response = new RestResponse();
		Cliente cliente;
		
		cliente = this.clienteService.getClienteFromToken(oauth);
	  try {
		  
		    if(cliente == null){
		          response.setSuccess(false);
		          response.setCode("FG007");
		          response.setMessage("Intentas acceder a recurso no permitido");
		          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		        }
	     System.out.println("Obteniendo promociones");
	 
	     response.setSuccess(true);
	     response.setCode("200");
	     response.setMessage("Consulta exitosa");
	     response.setData(promocionService.getAll(idCliente));
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	        response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
	/**
	 * Obtiene las notificaciones para WEB
	 * @param oauth
	 * @return
	 */
	@GetMapping("/web")
	@RolesAllowed({"ROLE_WEB"})
	public ResponseEntity<RestResponse>  getAllWeb(@RequestHeader(value="Authorization") String oauth) {
		RestResponse response = new RestResponse();

		
	  try {
		  
	     System.out.println("Obteniendo promociones");
	 
	     response.setSuccess(true);
	     response.setCode("200");
	     response.setMessage("Consulta exitosa");
	     response.setData(promocionService.getAll());
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	        response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
	/**
	 * Guardamos promocion
	 * @param oauth
	 * @param promoP
	 * @return
	 */
	@PostMapping
	@RolesAllowed({"ROLE_WEB"})
	public ResponseEntity<RestResponse>  save(@RequestHeader(value="Authorization") String oauth,@RequestBody PromocionDto promoP) {
		RestResponse response = new RestResponse();
		
	  try {
		 
	     response.setSuccess(true);
	     response.setCode("FG004");
	     response.setMessage("Operación exitosa");
	     response.setData(promocionService.save(promoP));
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch (FastGasException e) {
	    	   
	    	   response.setCode(e.getCodeError());
		       response.setMessage(e.getMessage());   
		       return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);   
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	        response.setMessage(e.getMessage());
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
	/**
	 * Actualiza promocion
	 * @param oauth
	 * @param promoP
	 * @return
	 */
	@PutMapping
	@RolesAllowed({"ROLE_WEB"})
	public ResponseEntity<RestResponse>  update(@RequestHeader(value="Authorization") String oauth,@RequestBody PromocionDto promoP) {
		RestResponse response = new RestResponse();
		
	  try {
		 
	     response.setSuccess(true);
	     response.setCode("FG004");
	     response.setMessage("Operación exitosa");
	     response.setData(promocionService.update(promoP));
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch (FastGasException e) {
	    	   
	    	   response.setCode(e.getCodeError());
		       response.setMessage(e.getMessage());   
		       return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);   
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	        response.setMessage(e.getMessage());
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
    /**
     * Guardamos imagen promocion
     * @param oauth
     * @param id
     * @param fotoPromo
     * @return
     */
	@PostMapping("/image{id}")
	@RolesAllowed({"ROLE_WEB"})
	public ResponseEntity<RestResponse>  saveImagePromo(@RequestHeader(value="Authorization") String oauth,
		@PathVariable(name = "id")Long id	,@RequestParam(name = "fotoPromo")MultipartFile fotoPromo) {
		RestResponse response = new RestResponse();
		
	  try {
		 
		     response.setSuccess(true);
		     response.setCode("FG004");
		     response.setMessage("Operación exitosa");
	        this.promocionService.saveImagePromo(fotoPromo.getBytes(), id);
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	   response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
	/**
	 * Obtiene promocion por id
	 * @param oauth
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	@RolesAllowed({"ROLE_WEB","ROLE_CLIENTE"})
	public ResponseEntity<RestResponse>  getPromocionById(@RequestHeader(value="Authorization") String oauth,@PathVariable(name = "id")Long id) {
		RestResponse response = new RestResponse();
		
	  try {
		 
		     response.setSuccess(true);
		     response.setCode("FG004");
		     response.setMessage("Operación exitosa");
		     response.setData(this.promocionService.findByIdEdit(id));
	          
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	         response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }

	
	/**
	 * Obtiene precios 
	 * @param oauth
	 * @return
	 */
	@GetMapping("/precios")
	@RolesAllowed({"ROLE_WEB","ROLE_CLIENTE"})
	public ResponseEntity<RestResponse>  getPrecios(@RequestHeader(value="Authorization") String oauth) {
		RestResponse response = new RestResponse();
		
	  try {
		 
		     response.setSuccess(true);
		     response.setCode("FG004");
		     response.setMessage("Operación exitosa");
		     cargaCombustiblesService.loadPricesfuels();
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	   response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }

}
