package com.enermex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.dto.RolDto;
import com.enermex.service.RolServiceA;

/**
 *
 * @author abraham
 */
@RestController

@RequestMapping("/rol")
public class RolControllerA {
	
        @Autowired
	    private RolServiceA rolService;
	
    /**
     *
     * @return
     */
    @GetMapping("getRoles")
	    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	    @ResponseBody
	    public List<RolDto> getRol() {
	    
	        return rolService.getAll();
	    }


}
