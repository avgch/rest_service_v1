package com.enermex.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.enermex.modelo.CatColorEntity;
import com.enermex.service.CatColorService;
import com.enermex.utilerias.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/colores")
public class CatColorController {
  @Autowired
  private CatColorService service;

  /**
   * Obtiene los colores
   * @return
   */
  @GetMapping
  public RestResponse<List<String>> getColores() {
    RestResponse<List<String>> response = new RestResponse<>();

    List<CatColorEntity> catColores = service.queryAll();
    List<String> colores = catColores != null ?
      catColores.stream().map(catColor -> catColor.getNombre()).collect(Collectors.toList()) :
      null;
    
    // Preparación de la respuesta
    response.setData(colores);
    response.setSuccess(colores != null);

    return response;
  }
}