package com.enermex.controller;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enermex.dto.ClienteDto;
import com.enermex.modelo.Cliente;
import com.enermex.service.ClienteService;
import com.enermex.service.InvitadoService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpQueja;
import com.enermex.utilerias.Token;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/invitado")
public class InvitadoController {
	
	@Autowired
	private ClienteService clienteService;
	
	
	@Autowired
  private InvitadoService invitadoService;
  
  @Autowired
  private Token token;
	@Autowired
  private SmtpQueja smtpQueja;

	
	
	
	/**
	 * Invitados
	 * @param id cliente titular
	 * @return
	 */
	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> getInvitados(@PathVariable BigInteger id) {
		
		RestResponse response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Invitados exitosa");
    		response.setData(this.clienteService.getInvitados(id));
    		
    		
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	/**
	 * Invitados
     * @param cliente
	 * @return
	 */
	@PostMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	@Transactional(rollbackFor=Exception.class)
	public ResponseEntity<RestResponse> addInvitados(@RequestBody ClienteDto cliente) {
		
		RestResponse response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Su invitado se ha agregado correctamente");
    		this.invitadoService.save(cliente);
    		
    		
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Elimina incidencia
     * @param id
	 * @return
	 */
	@DeleteMapping("{id}")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> delete(@PathVariable BigInteger id) {
		
		RestResponse response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Se elimino el registro correctamente ");
    		this.invitadoService.delete(id);
    		
    		
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Bloquea y desbloquear
     * @param oauth
     * @param id
     * @param bloqueo
	 * @return
	 */
	@PutMapping("/bloqueo/{id}")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> lockUnlock(@RequestHeader(value="Authorization") String oauth, @PathVariable BigInteger id,@RequestParam(name = "bloqueo")boolean bloqueo) {
		
		RestResponse response = new RestResponse();
    
    // Obtención del cliente desde oAuth
    Cliente titular = token.getClienteFromToken(oauth, response);
    
    if(titular == null) {
      return new ResponseEntity<>(response, HttpStatus.OK);
    }
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("El bloque se realizo con éxito ");
            this.invitadoService.lockUnlock(id,bloqueo,titular);

    		
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Actualiza invitado
	 * @param cliente
	 * @return
	 */
	@PutMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updateInvitado(@RequestBody ClienteDto cliente) {
		
		RestResponse response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Su invitado se actualizo correctamente ");
            this.invitadoService.updateInvitado(cliente);
    		
    		
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	


}
