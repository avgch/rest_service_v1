package com.enermex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enermex.service.CarSerieService;
import com.enermex.utilerias.RestResponse;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/serie")
public class SerieCarController {
	
	
	
	@Autowired
	private CarSerieService carSerieService;
	
	/**
	 * Obtiene serie car
	 * @param idModelo
	 * @param idGeneration
	 * @return
	 */
	@GetMapping
	public ResponseEntity<RestResponse> getAll(@RequestParam(name = "idModelo")Integer idModelo,@RequestParam(name = "idGeneration")Integer idGeneration){
		
		RestResponse response = new RestResponse();
		try {
			
			
			response.setCode("200");
			response.setMessage("Consulta exitosa");
			response.setData(carSerieService.getSeriesByModeloId(idModelo,idGeneration));
			
		}
		catch(Exception e) {
			response.setCode("500");
			response.setMessage("Contacte al administrador del sistema: \n" +e.getMessage());
		
			return new ResponseEntity<RestResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<RestResponse>(response,HttpStatus.OK);
		
		
	}

}
