package com.enermex.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.MappedSuperclass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import com.azure.core.annotation.BodyParam;
import com.azure.core.annotation.Post;
import com.enermex.utilerias.ConektaOperation;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;

import io.conekta.Conekta;
import io.conekta.Customer;
import io.conekta.PaymentSource;

import com.enermex.avg.exceptions.ConectaException;
import com.enermex.dto.PedidoDto;
import com.enermex.dto.PlanIdsDto;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.Tarjeta;
import com.enermex.modelo.TarjetaPlan;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.TarjetaRepository;
import com.enermex.service.ClienteService;
import com.enermex.service.TarjetaService;
import com.enermex.service.TarjetaPlanService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/tarjeta")
public class TarjetaController {
	
	@Autowired
	private Environment env;
	
//	private RestResponse response;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	TarjetaService tarjetaService;
	
	@Autowired
	TarjetaRepository tarjetaRepository;
	
	@Autowired
    ConektaOperation conekta;
	
	@Autowired
	TarjetaPlanService tarjetaPlanService;
	 
	
	 @Autowired
	 private Token token;
	 
	/**
	 * Agrega tarjeta
	 * @param tokenCard
	 * @param idCliente
	 * @param planes
	 * @return
	 */
	@PostMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> add(@RequestHeader(name = "tokenCard") String tokenCard,@RequestHeader(name = "idCliente")  BigInteger idCliente,@RequestBody PlanIdsDto planes){
		
		RestResponse response = new RestResponse();
		conekta.setApiKey();
		Cliente cliente;
		String idCnktaCust;
		PaymentSource paymentSource=null;
		Tarjeta newCard;
		TarjetaPlan tp;
		List<TarjetaPlan> tarjetaPlanes = new ArrayList<>();
		//token de la tarjeta a aprobar
		String cardToKEN="";
		String msjTarjeta="",ordenToken="";
		try{
			/* Conekta */
			
			
			//consultamos el idConekta idCliente
			idCnktaCust = this.clienteService.getIdTokenCustumer(idCliente);
			idCnktaCust = (idCnktaCust==null ? "" : idCnktaCust );
			
				if(!idCnktaCust.isEmpty()) {
					boolean esValidaTareta=true;
					
					//1. si ya existe el usuario validamos que la tarjeta tiene fondos

					//2. Creamos la tarjeta					
					paymentSource = conekta.creaTarjeta(idCnktaCust,tokenCard);
					
					/*** VALIDAMOS SI ESTA TARJETA TIENE FONDOS O NO ESTA BLOQUEADA **/
					//creamos una compra por $15 cantidad
					cardToKEN = paymentSource.getVal("id").toString();
					try {
						ordenToken=conekta.cardhealthy(paymentSource.getVal("id").toString(),idCnktaCust);
					}
					catch(ConectaException conekta) {
						esValidaTareta=false;	
						msjTarjeta=conekta.getMessage();
					}
					if(esValidaTareta==false) {
						
					  conekta.borraTarjeta(cardToKEN, idCnktaCust);
					  
						response.setSuccess(false);
			    		response.setCode("ERRCKT01");
			    		response.setMessage(msjTarjeta);
			    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
					}else {
						conekta.cancelarOrdenPreAutorizada(ordenToken);
						
					}
					/*** END VALIDAMOS SI ESTA TARJETA TIENE FONDOS O NO ESTA BLOQUEADA **/
					
					//obtenemos los datos de la tarjeta para guardar en nuestra bd
					newCard=tarjetaService.addTarjeta(idCliente,paymentSource.getVal("id").toString(),paymentSource.getVal("last4").toString(),paymentSource.getVal("brand").toString());
					
					//Guardamos planes
					System.out.println(planes);
					
					
					for(Integer plan : planes.getPlanes()) {
						tp = new TarjetaPlan();
						tp.setIdPlan(plan);
						tp.setIdTarjetaCliente(newCard.getIdTarjeta());
						tarjetaPlanes.add(tp);
					}
					tarjetaPlanService.save(tarjetaPlanes);
		    		response.setSuccess(true);
		    		response.setMessage("La tarjeta se agrego correctamente");	
	    		}else {
	    			
	    			idCnktaCust=creaCliente(idCliente,tokenCard,response);
	    			
                   boolean esValidaTareta=true;
					
					//1. si ya existe el usuario validamos que la tarjeta tiene fondos

					//2. Creamos la tarjeta					
					paymentSource = conekta.creaTarjeta(idCnktaCust,tokenCard);
					
					/*** VALIDAMOS SI ESTA TARJETA TIENE FONDOS O NO ESTA BLOQUEADA **/
					//creamos una compra por $15 cantidad
					cardToKEN = paymentSource.getVal("id").toString();
					try {
						ordenToken=conekta.cardhealthy(paymentSource.getVal("id").toString(),idCnktaCust);
					}
					catch(ConectaException conekta) {
						esValidaTareta=false;	
						msjTarjeta=conekta.getMessage();
					}
					if(esValidaTareta==false) {
						
					  conekta.borraTarjeta(cardToKEN, idCnktaCust);
					  
						response.setSuccess(false);
			    		response.setCode("ERRCKT01");
			    		response.setMessage(msjTarjeta);
			    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
					}else {
						conekta.cancelarOrdenPreAutorizada(ordenToken);
						
					}
					/*** END VALIDAMOS SI ESTA TARJETA TIENE FONDOS O NO ESTA BLOQUEADA **/
					
					//obtenemos los datos de la tarjeta para guardar en nuestra bd
					newCard=tarjetaService.addTarjeta(idCliente,paymentSource.getVal("id").toString(),paymentSource.getVal("last4").toString(),paymentSource.getVal("brand").toString());
					
					//Guardamos planes
					System.out.println(planes);
					
					
					for(Integer plan : planes.getPlanes()) {
						tp = new TarjetaPlan();
						tp.setIdPlan(plan);
						tp.setIdTarjetaCliente(newCard.getIdTarjeta());
						tarjetaPlanes.add(tp);
					}
					tarjetaPlanService.save(tarjetaPlanes);
		    		response.setSuccess(true);
		    		response.setMessage("La tarjeta se agrego correctamente");	
	    		}
			
    	 }
		catch(ConectaException conekta) {
			
			response.setSuccess(false);
    		response.setCode("ERRCKT");
    		response.setMessage(conekta.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	/**
	 * Obtiene las tarjetas de un titular
	 * @param oauth
	 * @return
	 */
	@GetMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> getAll(@RequestHeader(value="Authorization") String oauth){
		
		RestResponse response = new RestResponse();
		conekta.setApiKey();
		Cliente cliente;
		String idCnktaCust;
		
	    cliente = this.clienteService.getClienteFromToken(oauth);
	     
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
		
		try{
			 response.setSuccess(true);
	         response.setCode("FG008");
	         response.setMessage("Consulta exitosa");
             response.setData( this.tarjetaService.getTarjetasActivas(cliente.getIdCliente()));		

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	/**
	 * Obtiene las tarjetas de un invitado
	 * @param oauth
	 * @return
	 */
	@GetMapping("/invitado")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> getAllTarjetasFamiliar(@RequestHeader(value="Authorization") String oauth){
		
		RestResponse response = new RestResponse();
		conekta.setApiKey();
		Cliente cliente;
		String idCnktaCust;
		
	    cliente = this.clienteService.getClienteFromToken(oauth);
	     
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
		
		try{
			 response.setSuccess(true);
	         response.setCode("FG008");
	         response.setMessage("Consulta exitosa");
	         response.setData( this.tarjetaService.getTarjetasInvitado(cliente.getIdCliente()));		

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	
	/**
	 * Borrado logico
	 * @param oauth
     * @param idTarjeta
	 * @return
	 */
	@DeleteMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> delete(@RequestHeader(value="Authorization") String oauth,@RequestHeader(value="idTarjeta") Integer idTarjeta){
		
		RestResponse response = new RestResponse();
		boolean tienePedidoTarjeta = false; 
		conekta.setApiKey();
		Cliente cliente;
		String idCnktaCust;
		
	    cliente = this.clienteService.getClienteFromToken(oauth);
	     
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
		
		try{
			
			 //RN La tarjeta para ser eliminada no puede tener un servicio en curso
			tienePedidoTarjeta=this.tarjetaService.tarjetaTienePedido(idTarjeta);
			if(tienePedidoTarjeta) {
				response.setSuccess(true);
		        response.setCode("FG012");
		        response.setMessage("Por el momento no se puede eliminar la tarjeta ya que tiene servicios por atender");
		        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
				
			}
			
			 response.setSuccess(true);
	         response.setCode("FG011");
	         Tarjeta card =this.tarjetaRepository.getTarjetasById(idTarjeta);
	         conekta.borraTarjeta(card.getTokenTarjeta(),cliente.getTokenConekta());
	         this.tarjetaRepository.borrar(idTarjeta);
	         response.setMessage("Se elimino la tarjeta correctamente");
             

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	

	/**
	 * update tarjeta
	 * @param oauth
     * @param idTarjeta
	 * @return
	 */
	@PutMapping
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> update(@RequestHeader(value="Authorization") String oauth,@RequestHeader(value="idTarjeta") Integer idTarjeta){
		
		RestResponse response = new RestResponse();
		conekta.setApiKey();
		Cliente cliente;
		String idCnktaCust;
		
	    cliente = this.clienteService.getClienteFromToken(oauth);
	     
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
		
		try{
			 response.setSuccess(false);
	         response.setCode("FG008");
	         Tarjeta card =this.tarjetaRepository.getTarjetasById(idTarjeta);
	         conekta.borraTarjeta(card.getTokenTarjeta(),cliente.getTokenConekta());
	         this.tarjetaRepository.borrar(idTarjeta);
	         response.setMessage("");
             

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	/**
	 * update tarjeta mes y anio, 
	 * @param oauth
     * @param idTarjeta
     * @param mesExp
     * @param anioExp
	 * @return
	 */
	@PutMapping("/card")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updateCard(@RequestHeader(value="Authorization") String oauth,
			@RequestHeader(value="idTarjeta") Integer idTarjeta,
			@RequestHeader(value="mesExp") String mesExp,
			@RequestHeader(value="anioExp") String anioExp){
		
		RestResponse response = new RestResponse();
		conekta.setApiKey();
		Cliente cliente;
		String idCnktaCust;
		
	    cliente = this.clienteService.getClienteFromToken(oauth);
	     
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
	     
		
		try{
			 response.setSuccess(true);
	         response.setCode("FG010");
	         Tarjeta card =this.tarjetaRepository.getTarjetasById(idTarjeta);
	         conekta.updateCard(card.getTokenTarjeta(),cliente.getTokenConekta(),mesExp,anioExp,null);
	         
	         //borramos los planes actuales y volvemos a setear

//				for(Integer plan : planes.getPlanes()) {
//					tp = new TarjetaPlan();
//					tp.setIdPlan(plan);
//					tp.setIdTarjetaCliente(newCard.getIdTarjeta());
//					tarjetaPlanes.add(tp);
//				}
	         
	         
	         response.setMessage("Tarjeta actualizada");
             

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	
	
	/**
	 * Update planes
	 * @param oauth
     * @param idTarjeta
     * @param planes
	 * @return
	 */
	@PutMapping("/planes")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updatePlanesCard(@RequestHeader(value="Authorization") String oauth,
			@RequestHeader(value="idTarjeta") Integer idTarjeta,@RequestBody PlanIdsDto planes){
		
		RestResponse response = new RestResponse();
		conekta.setApiKey();
		Cliente cliente;
		String idCnktaCust;
		TarjetaPlan tp;
		List<TarjetaPlan> planesCard =  new ArrayList<>();
		
	    cliente = this.clienteService.getClienteFromToken(oauth);
	     
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
	     
		
		try{
			 response.setSuccess(true);
			 response.setMessage("Actualización de tarjeta (planes) exitosa ");
	         response.setCode("FG013");
	         //borramos los planes actuales y volvemos a setear
	         tarjetaPlanService.deletePlanes(idTarjeta);
	         
				for(Integer plan : planes.getPlanes()) {
					tp = new TarjetaPlan();
					tp.setIdPlan(plan);
					tp.setIdTarjetaCliente(idTarjeta);
					planesCard.add(tp);
				}
				tarjetaPlanService.save(planesCard);

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
   /**
    * Crea cliente conekta	
    * @param idCliente
    * @param tokenCard
 * @throws ConectaException 
    */
   private String creaCliente(BigInteger idCliente,String tokenCard,RestResponse response) throws ConectaException {
	   
		Cliente cliente;
		String idCnktaCust;
	   
	    cliente =clienteService.getClienteById(idCliente.toString());
		idCnktaCust = conekta.createCustomerConekta(cliente);
	    this.clienteService.updateTokenConektaCustumer(idCnktaCust,idCliente);
	   
	   return idCnktaCust;
   }

}
