package com.enermex.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enermex.modelo.Parametro;
import com.enermex.service.ParametrosService;
import com.enermex.utilerias.RestResponse;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/parametros")
public class ParametrosController {
	
	@Autowired
	ParametrosService parametrosService;
	
	/**
	 * Obtiene parametros
	 * @return
	 */
	@GetMapping
	@RolesAllowed({"ROLE_WEB"})
	public ResponseEntity<RestResponse>  getAll() {
		RestResponse  response = new RestResponse();
		
	   try {
		      response.setSuccess(true);
		      response.setCode("200");
		      response.setMessage("Consulta exitosa");
		      response.setData(parametrosService.getParametrosFilter());
	        	
	        	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	
	        	
	        }
	        catch(Exception e) {
	        	
	        	response.setCode("500");
			    response.setMessage(e.getMessage());
	        	
			    return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	        }
	    			
	}
	
	
	/**
	 * Obtiene parametro por id
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	@RolesAllowed({"ROLE_WEB"})
	public ResponseEntity<RestResponse>  getParamById(@PathVariable(name = "id") Integer id) {
		RestResponse  response = new RestResponse();
		
	   try {
		      response.setSuccess(true);
		      response.setCode("200");
		      response.setMessage("Consulta exitosa");
		      response.setData(parametrosService.getParametroById(id));
	        	
	        	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	
	        	
	        }
	        catch(Exception e) {
	        	
	        	response.setCode("500");
			    response.setMessage(e.getMessage());
	        	
			    return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	        }
	    			
	}
	
	
	/**
	 * Actualiza parametro por id
	 * @param parametro
	 * @return
	 */
	@PutMapping
	@RolesAllowed({"ROLE_WEB"})
	public ResponseEntity<RestResponse>  updateParametro(@RequestBody Parametro parametro) {
		RestResponse  response = new RestResponse();
		
	   try {
		      response.setSuccess(true);
		      response.setCode("200");
		      response.setMessage("Consulta exitosa");
		      parametrosService.updateParametro(parametro.getValor(),parametro.getIdParametro());
		      response.setData(null);
	        	
	        	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	
	        	
	        }
	        catch(Exception e) {
	        	
	        	response.setCode("500");
			    response.setMessage(e.getMessage());
	        	
			    return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	        }
	    			
  }



  /**
   * Obtiene telefono
   * @return
   */
  // INTER: José Antonio González Hernández - Teléfono de central para app de despachadores
  @GetMapping("/telefono")
	@RolesAllowed({"ROLE_DESPACHADOR"})
	public ResponseEntity<RestResponse<String>> getTelefonoCentral() {
    RestResponse<String> response = new RestResponse<>();
    String telefono = null;
    String error = null;

    try {
      List<Parametro> parametros = parametrosService.getParametros();

      // Se realiza la busqueda del parámetro en cuestión
      for(Parametro parametro : parametros) {
        if(parametro.getNombre().equalsIgnoreCase("Teléfono de central")) {
          telefono = parametro.getValor();

          break;
        }
      }

      // Si el teléfono es nulo, no se encontró el parámetro
      if(telefono == null) {
        throw new IllegalStateException("No se encuentra el parámetro del teléfono de la central");
      }
    } catch(IllegalStateException e) {
      error = e.getMessage();
    } catch(Exception e) {
      // TODO: Logger
      e.printStackTrace();

      error = "Comuníquese con el área de sistemas";
    }

    // Preparación de la respuesta
    response.setSuccess(telefono != null);
    response.setData(telefono);
    response.setMessage(error);

    return new ResponseEntity<RestResponse<String>>(response, HttpStatus.OK);
  }




}
