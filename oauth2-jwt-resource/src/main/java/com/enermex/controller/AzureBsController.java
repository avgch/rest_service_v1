package com.enermex.controller;

import com.enermex.modelo.AzureBsEntity;
import com.enermex.service.AzureBsService;
import com.enermex.utilerias.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/azure/bs")
public class AzureBsController {

  @Autowired
  private AzureBsService service;

  /**
   * 
   * @return
   */
  @GetMapping
  public RestResponse<AzureBsEntity> getWebsite() {
    final RestResponse<AzureBsEntity> response = new RestResponse<AzureBsEntity>();
    
    final AzureBsEntity azure = service.queryById(1);

    // Preparación de la respuesta
    response.setData(azure);
    response.setSuccess(azure != null);
    response.setMessage(azure != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * 
   * @param oauth
   * @param intro
   * @return
   */
  @PostMapping
  public RestResponse<Void> postWebsite(@RequestHeader(value="Authorization") String oauth, @RequestBody AzureBsEntity intro) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    final AzureBsEntity azure = service.queryById(1);
    azure.setContainer(intro.getContainer());
    azure.setEndpoint(intro.getEndpoint());
    azure.setSas(intro.getSas());

    // Preparación de la respuesta
    response.setSuccess(service.update(azure));

    return response;
  }
}