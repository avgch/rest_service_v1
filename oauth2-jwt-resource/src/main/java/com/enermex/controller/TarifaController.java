package com.enermex.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.enermex.dto.tarifa.TarifaLista;
import com.enermex.dto.tarifa.TarifaVista;
import com.enermex.modelo.TarifaLavadoHistorialEntity;
import com.enermex.modelo.TarifaPresionHistorialEntity;
import com.enermex.service.PermisoService;
import com.enermex.service.TarifaService;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TarifaController
 */
@RestController
@RequestMapping("/api/tarifas")
public class TarifaController {
  private Logger logger = LoggerFactory.getLogger(TarifaController.class);

  @Autowired
  private TarifaService service;
  
  @Autowired
  private PermisoService permisoService;

  /**
   * Obtiene tarifas 
   * @param oauth
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping()
  public RestResponse<List<TarifaVista>> getTarifario(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<TarifaVista>> response = new RestResponse<List<TarifaVista>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "TARIFAS", "ACCESS")) {
      return response;
    }

    // Consulta del listado de tarifas
    final List<TarifaVista> tarifas = service.queryAll();

    // Preparación de la respuesta
    response.setData(tarifas);
    response.setSuccess(tarifas != null);
    response.setMessage(tarifas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Guarda tarifas
   * @param oauth
   * @param tarifas
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @PostMapping()
  public RestResponse<Void> postTarifaLavado(@RequestHeader(value="Authorization") String oauth, @RequestBody TarifaLista tarifas) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "TARIFAS", "ACCESS")) {
      return response;
    }

    // Primero se asigna la fecha actual
    Calendar date = Calendar.getInstance();
    tarifas.getLavado().forEach(tarifa -> {
      tarifa.setFechaCreacion(date);
      service.saveLavado(tarifa);
    });
    tarifas.getPresion().forEach(tarifa -> {
      tarifa.setFechaCreacion(date);
      service.savePresion(tarifa);
    });

    // Preparación de la respuesta
    response.setSuccess(tarifas != null);
    response.setMessage(tarifas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Test 
   * @param oauth
   * @param cps
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @PostMapping("test")
  public RestResponse<List<String>> postTest(@RequestHeader(value="Authorization") String oauth, @RequestBody List<String> cps) {
    final RestResponse<List<String>> response = new RestResponse<List<String>>();
    final ArrayList<String> sinTarifa = new ArrayList<>();

    cps.forEach(cp -> {
      if(!service.exists(cp)) {
        sinTarifa.add(cp);
      }
    });

    // Preparación de la respuesta
    response.setData(sinTarifa);
    response.setSuccess(true);

    return response;
  }


  /**
   * Historial tarifas presion
   * @param oauth
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/historial/presion")
  public RestResponse<List<TarifaPresionHistorialEntity>> getHistorialPresion(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<TarifaPresionHistorialEntity>> response = new RestResponse<List<TarifaPresionHistorialEntity>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "TARIFAS", "ACCESS")) {
      return response;
    }

    // Consulta del listado de tarifas
    final List<TarifaPresionHistorialEntity> tarifas = service.historialPresion();

    // Preparación de la respuesta
    response.setData(tarifas);
    response.setSuccess(tarifas != null);
    response.setMessage(tarifas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }


  /**
   * Get tarifas lavado
   * @param oauth
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/historial/lavado")
  public RestResponse<List<TarifaLavadoHistorialEntity>> getHistorialLavado(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<TarifaLavadoHistorialEntity>> response = new RestResponse<List<TarifaLavadoHistorialEntity>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "TARIFAS", "ACCESS")) {
      return response;
    }

    // Consulta del listado de tarifas
    final List<TarifaLavadoHistorialEntity> tarifas = service.historialLavado();

    // Preparación de la respuesta
    response.setData(tarifas);
    response.setSuccess(tarifas != null);
    response.setMessage(tarifas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }
  
}