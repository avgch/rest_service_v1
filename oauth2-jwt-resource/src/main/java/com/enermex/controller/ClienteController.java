package com.enermex.controller;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.azure.core.annotation.Delete;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.config.DefaultPasswordEncoderFactories;
import com.enermex.dto.ClienteDto;
import com.enermex.dto.UsuarioDto;
import com.enermex.map.PasswordChange;
import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteFcmEntity;
import com.enermex.modelo.TokenEntity;
import com.enermex.service.AutomovilClienteService;
import com.enermex.service.ClienteFcmService;
import com.enermex.service.ClienteService;
import com.enermex.service.TokenService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.utilerias.UrlSigner;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/clientes")
public class ClienteController {
	
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private AutomovilClienteService automovilClienteService;

	// REQ: Itzia V. - Un token no debe usarse más de una vez
	@Autowired
	private UrlSigner signer;
	@Autowired
	private TokenService tokenService;
	
	
	@Resource(name="passwordEncoderBean")
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	SmtpGmailSsl sendMail;
	
	@Autowired
	ClienteFcmService clienteFcmService;
	
	/**
	 * Metodo de prueba
	 * @return
	 */
	@GetMapping("/hellow")
	public ResponseEntity<RestResponse>  getSaludo() {
		RestResponse  response = new RestResponse();	
		
	   try {
		      response.setSuccess(true);
		      response.setCode("200");
		      response.setMessage("Consulta exitosa");
		      response.setData("Hola mundo");
	        	
	        	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	
	        	
	        }
	        catch(Exception e) {
	        	
	        	response.setCode("500");
			    response.setMessage(e.getMessage());
	        	
			    return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	        }
	    			
	}
	
	/**
	 * Obtiene los clientes
	 * @return
	 */
	@GetMapping
	public ResponseEntity<RestResponse>  getAll() {
		RestResponse  response = new RestResponse();
		
	   try {
		      response.setSuccess(true);
		      response.setCode("200");
		      response.setMessage("Consulta exitosa");
		      response.setData(clienteService.getAllClientesWebDto());
	        	
	        	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);	
	        	
	        }
	        catch(Exception e) {
	        	
	        	response.setCode("500");
			    response.setMessage(e.getMessage());
	        	
			    return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	        }
	    			
	}
	
    /**
     * Elimina el cliente por id
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
    public ResponseEntity<String> delete(@PathVariable BigInteger id) {
    	
    	RestResponse  response = new RestResponse();
    	try {
    		
    		clienteService.delete(id);	
    		return new ResponseEntity<>("borrado logico", HttpStatus.OK);
    	 }

    	catch(Exception e) {
    		System.out.println("Meesage:"+e.getMessage());
    		System.out.println("Meesagee"+e.getCause());
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    
    }
	
	/**
	 * Actualizacion de cliente
	 * @param cliente
	 * @return
	 */
    @PutMapping
	@PreAuthorize("hasRole('ROLE_WEB')")
	public ResponseEntity<RestResponse> update(@RequestBody Cliente cliente){
		
    	RestResponse  response = new RestResponse();
		try {
			
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualizacion exitosa");
    		clienteService.update(cliente);
			return new ResponseEntity<RestResponse>(response, HttpStatus.OK);

		     
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
	}
    
    /**
     * Valida registro
     * @param correo
     * @return
     */
    @GetMapping("/validaRegistro")
    public ModelAndView validaRegistro(@RequestParam(name = "correo")String correo){
    	
        ModelAndView vw =new ModelAndView();
		 vw.setViewName("template");

    	boolean esValido;

    	try {
    		//actualizamos el usuario acceso =true

    		esValido=this.clienteService.validaAcceso(correo);

    		if(esValido){

    			vw.addObject("encabezado","Bienvenido " +correo);

    			vw.addObject("cuerpo", "Felicidades su registro se ha completado!");

         		return vw;

    		}
    		else {

        		return vw;
    		}
    	}
        catch(Exception e) {
          return vw;
    	}
    	
    }
    
    /**
     * Registro de usuario
     * @param cliente
     * @return
     */
    @PostMapping("/register")
    @Transactional(rollbackFor=Exception.class)
	public ResponseEntity<RestResponse> save(@RequestBody ClienteDto cliente){
		
    	RestResponse  response = new RestResponse();
    	Cliente clienteE;
    	Boolean esRegValido=false;
    	Cliente correo,invitado;
		try {
			
			//valicaciones Objeto de validaciones
			boolean yaExisteUsuario = false;
			
			//validamos duplicidad de correo
			correo= clienteService.esRegistroValido(cliente.getCorreo());
			
			if(correo!=null) {
				
				/// Validamos que ya exista el correo con fecha_baja diferente de null
				yaExisteUsuario = clienteService.buscarClienteExistente(cliente.getCorreo());
				
				if(!yaExisteUsuario) {
					response.setSuccess(false);
		    		response.setCode("500");
		    		response.setMessage("Existe una cuenta registrada con este correo");
		    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else {
					// Get client id
					BigInteger idCliente = clienteService.getClienteByEmail(cliente.getCorreo()).getIdCliente();
					cliente.setIdCliente(idCliente);
				}
			}
			
			
			//validamos cuentas de invitados
			if(cliente.getInvitados()!=null) {
				for(Cliente cli :cliente.getInvitados()) {
					invitado=clienteService.esRegistroValido(cli.getCorreo());
					
					if(invitado!=null) {
						response.setSuccess(false);
			    		response.setCode("500");
			    		response.setMessage("El correo de invitado existente");
			    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
						
					}
					
				}
			}

    		//codificamos el password
    		String pass =passwordEncoder.encode(cliente.getPass());
    		cliente.setContrasena(pass);
    		
    		
    		if(yaExisteUsuario){
    			response.setCode("FG026");
        		response.setSuccess(true);
        		response.setMessage("Usuario previamente existente.Registro exitoso, verifica tu correo electrónico");
        		clienteE = clienteService.updateExistente(cliente);
        		response.setData(cliente.getIdCliente());
    		}else {
    			response.setCode("200");
        		response.setSuccess(true);
        		response.setMessage("Registro exitoso, verifica tu correo electrónico");
        		clienteE = clienteService.save(cliente);
        		response.setData(clienteE.getIdCliente());
    		}


			return new ResponseEntity<RestResponse>(response, HttpStatus.OK);		     
    	 }

    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
    
    /**
     * Inicia el flujo de la recuperación de la contraseña
	 * 
     * @param correo Correo electrónico del usuario; se recive como
	 *        parámetro de la URL
     * @return
     */
	@PostMapping("/recovery")
    public RestResponse<Void> recovery(
			@RequestParam(name = "correo")String correo) {
		 
		 Cliente cliente = clienteService.findByEmail(correo);
	
		RestResponse<Void> response = new RestResponse<>();
    	
    	if(cliente!=null) {
				// REQ: Itzia V. - Un token no debe usarse más de una vez
				TokenEntity tokenEntity = new TokenEntity();
				tokenEntity.setAplicacion("M");
				tokenEntity.setIdCliente(cliente.getIdCliente());

				if(tokenService.create(tokenEntity)){
					final String token = signer.sign(tokenEntity.getIdToken().toString());
					sendMail.sendEmail(correo, token,"recoveryPass","MOVIL");
					
					response.setSuccess(true);
				} else {
					response.setSuccess(false);
					response.setCode("500");
					response.setMessage("Error en el envío de correo");
				}
    	} else {
    		response.setSuccess(false);
			response.setCode("500");
			response.setMessage("Correo inválido");
		}
		
    	return response;
    }

	/**
	 * Obtiene el perfil del cliente app movil
	 * @param correo
	 * @return
	 */
	@GetMapping("/perfil")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> getClienteById(@RequestParam(name = "correo")String correo){
		
		RestResponse  response = new RestResponse();
		Cliente cliente;
		
		try {
			
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Consulta exitosa");
    		cliente = clienteService.findClienteByCorreo(correo);
    		response.setData(cliente);
    	 }
    	catch(Exception e) {
    	 
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	
	/**
	 * Login
     * @param id
     * @param password
	 * @return
	 */
	@PostMapping("/login")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> login(@RequestParam(name = "correo")String id,@RequestParam(name = "password")String password) {
		
		RestResponse  response = new RestResponse();
		Cliente cliente;
		
		try {
			
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Consulta exitosa");
    		cliente = clienteService.getClienteById(id);
    		response.setData(cliente);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * Agrega carro
	 * @param idCliente
	 * @param cliente
	 * @return
	 */
	@PostMapping("/addCars")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> addCar(@RequestParam(name = "idCliente")BigInteger idCliente,@RequestBody ClienteDto cliente){
		
		RestResponse  response = new RestResponse();
		
		try {
			
			clienteService.addCars(idCliente, cliente.getCars());
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Se registro el automóvil correctamente");
    		response.setData(null);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
		
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
    
	/**
	 * Agrega un nuevo plan
	 * @param clienteP
	 * @return
	 */
	@PutMapping("/updatePlan")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updatePlan(@RequestBody ClienteDto clienteP) {
		
		RestResponse  response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualiza plan");
    		response.setData(this.clienteService.updatePlan(clienteP));
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
    
	/**
	 * Actualiza el telefono
	 * @param clienteP
	 * @return
	 */
	@PutMapping("/updatePerfil/tel")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updatePerfilTel(@RequestBody ClienteDto clienteP) {
		
		RestResponse  response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Se han guardado los cambios correctamente");
    		this.clienteService.updatePerfilTel(clienteP);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
    
	/**
	 * Actualiza el password
	 * @param clienteP
	 * @return
	 */
	@PutMapping("/updatePerfil/pass")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updatePerfilPass(@RequestBody ClienteDto clienteP) {
		
		RestResponse  response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Se ha modificado correctamente la contraseña");
    		this.clienteService.updatePerfilPass(clienteP);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	 /**
     * Verifica que el token de recuperación sea válido
	 * 
     * @param justToken Objeto de contraseña, solo con el token
     * @return
     */
    @GetMapping("/recoveryToken")
    public RestResponse<Void> validaToken(
			@RequestBody PasswordChange justToken) {
		String email = signer.unbox(justToken.getToken());
		RestResponse  response = new RestResponse();
		//UsuarioDto user = (UsuarioDto) usuarioService.validateEmail(email);
//		RestResponse<Void> response = new RestResponse<>();
//
//    	if(user != null) {
//			response.setSuccess(true);
//    	} else {
//			response.setSuccess(false);
//			response.setCode("500");
//			response.setMessage("Token inválido");
//    	}

        return response;
	}
    
	/**
	 * Actualiza el nombre
	 * @param clienteP
	 * @return
	 */
	@PutMapping("/updatePerfil/nombre")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updatePerfilNombre(@RequestBody ClienteDto clienteP) {
		
		RestResponse  response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualización exitosa");
    		this.clienteService.updatePerfilNombre(clienteP);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Actualiza el apellido paterno
	 * @param clienteP
	 * @return
	 */
	@PutMapping("/updatePerfil/apellidoP")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updatePerfilApellidoP(@RequestBody ClienteDto clienteP) {
		
		RestResponse  response = new RestResponse();
		
		try {
			

			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualización exitosa");
    		this.clienteService.updatePerfilApellidoP(clienteP);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
    
	
	/**
	 * Actualiza el apellido materno
	 * @param clienteP
	 * @return
	 */
	@PutMapping("/updatePerfil/apellidoM")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updatePerfilApellidoM(@RequestBody ClienteDto clienteP) {
		
		RestResponse  response = new RestResponse();
		
		try {
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualización exitosa");
    		this.clienteService.updatePerfilApellidoM(clienteP);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
    /**
     * Valida si la cuenta se encuentra activa
     * @param password
     * @return
     */
	@PostMapping("/esCtaActiva")
	public ResponseEntity<RestResponse>  esCtaActiva(@RequestHeader(name = "correo") String correo){
		RestResponse  response = new RestResponse();
		
	try {
		   //primero validamos que el usuario exista en el sistema
		   this.clienteService.existeCliente(correo);
		   
		   //si existe validamos su estatus
		  //validamos si la cuenta existe
		    this.clienteService.tieneBaja(correo);
		
		    Cliente cli = this.clienteService.tieneAcceso(correo);
		    if(cli!=null) {
	    		if(cli.isAcceso()) {
	    			response.setCode("FG003");
		    		response.setSuccess(true);
		    		response.setMessage("Cuenta activa");	
		    		return new ResponseEntity<>(response, HttpStatus.OK);
	    			
	    		}else {
	    			response.setCode("FG002");
		    		response.setSuccess(false);
		    		response.setMessage("Por favor termina tu registro");	
		    		return new ResponseEntity<>(response, HttpStatus.OK);	
	    		}
		    	
		    }
    		
    	 }
	    catch (FastGasException e) {
	      	
	    	response.setSuccess(false);
    		response.setCode(e.getCodeError());
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
	    }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
    
    /**
     *
     * @param bloqueo
     * @param idCliente
     * @return
     */
    @PostMapping("/bloqueo")
	@PreAuthorize("hasRole('ROLE_WEB')")
    public  ResponseEntity<RestResponse> bloqueoClienteWEB(@RequestParam(name = "bloqueo")Boolean bloqueo,@RequestParam(name = "idCliente")BigInteger idCliente ){
		RestResponse  response = new RestResponse();
    	
    	try {
			response.setCode("FG004");
    		response.setSuccess(true);
    		response.setMessage("Operación exitosa");
    		this.clienteService.bloqueaCliente(bloqueo,idCliente);
    		
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.OK);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
    	
    }
	
	/**
	 * Actualiza el apellido materno
	 * @param clienteP
	 * @return
	 */
	@PutMapping("/updatePerfil/correo")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> updateCorreo(@RequestBody ClienteDto clienteP) {
		
		RestResponse  response = new RestResponse();
		
		try {
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualización exitosa");
    		this.clienteService.updatePerfilCorreo(clienteP);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("400");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
 
	/**
	 * Elimina la cuenta de un cliente 
     * @param oauth
	 * @param idPlan
	 * @return
	 */
	@DeleteMapping("/baja/cuenta")
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> bajaCuenta(@RequestHeader(value="Authorization") String oauth){

		RestResponse response = new RestResponse();
		Cliente cliente;
		try {

			cliente = this.clienteService.getClienteFromToken(oauth);

			if(cliente == null){
				response.setSuccess(false);
				response.setCode("FG007");
				response.setMessage("Intentas acceder a recurso no permitido");
				return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
			}	

			response.setData(null);

			List<BigInteger> familiaresIds = this.clienteService.getFamiliares(cliente);
      boolean seElimina= this.clienteService.eliminaPlan(cliente, familiaresIds, true);

      
      // INTER: Antonio González - Determinar si existe plan falimiar o individual
      // NOTE: En la base de datos no se almacena correctamente el plan; por lo que, por convención, se determina con la titularidad
      String plan = "";

      if (familiaresIds.size() > 0 || cliente.getIdTitular() != null) {
        plan = "familiar";
      } else {
        plan = "individual";
      }
      // INTER: Finaliza

			if(seElimina){
				List<BigInteger> clientesIds = familiaresIds;
				clientesIds.add(cliente.getIdCliente());
				List<String> familiaresEmail = this.clienteService.getEmails(familiaresIds, cliente);
				this.clienteService.eliminaCuenta(clientesIds);
				this.clienteService.eliminarRelacionTitularFamiliares(cliente.getIdCliente(), familiaresIds.size());
				this.clienteService.enviarEmailsBajaCuenta(familiaresEmail, cliente.getCorreo(), plan);    				

				//Notificacion PUSH
				MulticastMessage.Builder msg=  MulticastMessage.builder();	
				List<String> tokensDevices = clienteFcmService.getTokensClientes(clientesIds);

				if(tokensDevices!=null && tokensDevices.size()>0){
						Notification.Builder msNot = Notification.builder();
						msNot.setBody("Este usuario ha sido dado de baja");
						msNot.setTitle("FAST GAS BAJA");
						msg.addAllTokens(tokensDevices);
						msg.setNotification(msNot.build());
						FirebaseMessaging fm = FirebaseMessaging.getInstance();
						fm.sendMulticast(msg.build());
				}

				response.setCode("FG024");
				response.setMessage("Se eliminó la cuenta exitosamente");	    			
			}else{
				response.setCode("FG025");
				response.setMessage("No se puede eliminar la cuenta, un invitado o el cliente tiene un pedido  pendiente");
			}

			response.setSuccess(true);
		}
		catch(Exception e) {

			response.setSuccess(false);
			response.setCode("500");
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}

	/**
	 * Elimina el plan de un cliente validando no tenga ningun pedido
     * @param oauth
	 * @return
	 */
	
	@DeleteMapping("/baja/plan")
	@RolesAllowed({"ROLE_CLIENTE"})
	public ResponseEntity<RestResponse> eliminaPlanFamiliar(@RequestHeader(value="Authorization") String oauth){

		RestResponse response = new RestResponse();
		Cliente cliente;
		try {

			cliente = this.clienteService.getClienteFromToken(oauth);

			if(cliente == null){
				response.setSuccess(false);
				response.setCode("FG007");
				response.setMessage("Intentas acceder a recurso no permitido");
				return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
			}	

			response.setData(null);

			List<BigInteger> familiaresIds = this.clienteService.getFamiliares(cliente);
			boolean seElimina= this.clienteService.eliminaPlan(cliente, familiaresIds, false);

			if(seElimina){
				List<String> familiaresEmail = this.clienteService.getEmails(familiaresIds, cliente);
				this.clienteService.eliminaTitularFamiliares(familiaresIds);
				this.clienteService.eliminarRelacionTitularFamiliares(cliente.getIdCliente(), familiaresIds.size());
				this.clienteService.enviarEmailsBajaFamiliar(familiaresEmail, cliente.getCorreo());    				

				//Notificacion PUSH
				MulticastMessage.Builder msg=  MulticastMessage.builder();
				if(familiaresIds != null && familiaresIds.size() > 0){
					List<String> tokensDevices = clienteFcmService.getTokensClientes(familiaresIds);

					if(tokensDevices!=null && tokensDevices.size()>0){
							Notification.Builder msNot = Notification.builder();
							msNot.setBody("Este usuario ha sido dado de baja");
							msNot.setTitle("FAST GAS BAJA");
							msg.addAllTokens(tokensDevices);
							msg.setNotification(msNot.build());
							FirebaseMessaging fm = FirebaseMessaging.getInstance();
							fm.sendMulticast(msg.build());
					}
				}

				response.setCode("FG018");
				response.setMessage("Se eliminó el plan exitosamente");	    			
			}else{
				response.setCode("FG019");
				response.setMessage("No se puede eliminar el plan un invitado tiene un pedido pendiente");
			}

			response.setSuccess(true);

		}
		catch(Exception e) {

			response.setSuccess(false);
			response.setCode("500");
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);

	}

}
