package com.enermex.controller;

import java.math.BigInteger;

import com.enermex.enumerable.PedidoEvidenciaMomento;
import com.enermex.service.PedidoEvidenciaService;
import com.enermex.utilerias.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * PedidoEvidenciaController
 */
@RestController
@RequestMapping("/api/pedidos")
public class PedidoEvidenciaController {
  @Autowired
  PedidoEvidenciaService service;

 /**
  * Actualiza evidencia foto
  * @param idPedido
  * @param archivos
  * @return
  */
  @PostMapping("/{id}/upload/evidencias/antes")
  public RestResponse<Void> uploadFotosAntes(@PathVariable("id") BigInteger idPedido, @RequestParam("files") MultipartFile[] archivos) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // TODO: Validar el identificador
    boolean hasError = false;

    for(MultipartFile archivo : archivos){
      if (archivo.isEmpty() || !service.addFoto(idPedido, archivo, PedidoEvidenciaMomento.ANTES)) {
        hasError = true;
      }
    }

    if (!hasError) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Guarda evidencia
   * @param idPedido
   * @param archivos
   * @return
   */
  @PostMapping("/{id}/upload/evidencias/despues")
  public RestResponse<Void> uploadFotosDepues(@PathVariable("id") BigInteger idPedido, @RequestParam("files") MultipartFile[] archivos) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // TODO: Validar el identificador
    boolean hasError = false;

    for(MultipartFile archivo : archivos){
      if (archivo.isEmpty() || !service.addFoto(idPedido, archivo, PedidoEvidenciaMomento.DESPUES)) {
        hasError = true;
      }
    }

    if (!hasError) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Recupera evidencia por id
   * @param id
   * @return
   */
  @RequestMapping(value = "/evidencias/{id}", method = { RequestMethod.GET, RequestMethod.POST }, produces = "image/png")
  public ResponseEntity<InputStreamResource> foto(@PathVariable("id") BigInteger id) {
    InputStreamResource resource = service.downloadFoto(id);

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }
}