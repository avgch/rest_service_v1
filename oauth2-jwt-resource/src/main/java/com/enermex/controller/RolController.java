package com.enermex.controller;

import java.util.List;

import com.enermex.modelo.RolEntity;
import com.enermex.service.PermisoService;
import com.enermex.service.RolService;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController

/**
 * Get roles
 * @author abraham
 *
 */
@RequestMapping("/api/roles")
public class RolController {
  Logger logger = LoggerFactory.getLogger(RolController.class);

  @Autowired
  private RolService service;
  
  @Autowired
  private PermisoService permisoService;

    /**
     *
     * @param oauth
     * @return
     */
    @GetMapping()
  public RestResponse<List<RolEntity>> getRoles(@RequestHeader(value="Authorization") String oauth) {
    RestResponse<List<RolEntity>> response = new RestResponse<List<RolEntity>>();
    List<RolEntity> roles = service.queryAll();
    
    if(roles != null){
      response.setData(roles);
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
    }

    return response;
  }

  /**
   * Get rol
   * @param oauth
   * @param id
   * @return
   */
  @GetMapping("/{id}")
  public RestResponse<RolEntity> getRol(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") int id) {
    RestResponse<RolEntity> response = new RestResponse<RolEntity>();
    RolEntity rol = service.queryById(id);
    
    if(rol != null) {
      response.setData(rol);
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
    }

    return response;
  }

  /**
   * Guarda rol
   * @param oauth
   * @param rol
   * @return
   */
  @PostMapping()
  public RestResponse<Integer> postRol(@RequestHeader(value="Authorization") String oauth, @RequestBody RolEntity rol) {
    final RestResponse<Integer> response = new RestResponse<Integer>();

    if(!validRepeated(response, rol, 0)){
      return response;
    }

    if (service.create(rol)) {
      response.setSuccess(true);
      response.setData(rol.getIdRol());
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Actualiza rol
   * @param oauth
   * @param id
   * @param rol
   * @return
   */
  @PutMapping("/{id}")
  public RestResponse<Integer> putRol(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") int id, @RequestBody RolEntity rol) {
    final RestResponse<Integer> response = new RestResponse<Integer>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "ROLES", "UPDATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    if(!validRepeated(response, rol, id)){
      return response;
    }


    // Algunos campos no se deben actualizar por este medio
    RolEntity db = service.queryById(id);

    if(db != null) {
      rol.setIdRol(db.getIdRol());
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Trata de actualizar un rol inexistente");
    }

    if (service.update(rol)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Valida repeted
   * @param response
   * @param rol
   * @param idRol
   * @return
   */
  // REQ: Itzia V. - No debe permitirse crear un rol y/o descripción iguales
  private boolean validRepeated(RestResponse response, RolEntity rol, int idRol) {
    // Se obtiene la lista de roles para validar que no existan repetidos
    List<RolEntity> roles = service.queryAll();

    // Logger si no se pueden obtener los roles
    if(roles == null) {
      logger.error("Error al validar los roles repetidos; se asume que no existen.");
      return true;
    }
    
    // Se valida que el nombre no sea repetido
    for(RolEntity row : roles){
      if(row.getRol().trim().equalsIgnoreCase(rol.getRol().trim()) && row.getIdRol().intValue() != idRol) {
        response.setSuccess(false);
        response.setCode("REQUEST");
        response.setMessage("Ya existe el rol \"" + rol.getRol() + "\"");
        return false;
      }
    }

    // Se valida que la descripción no sea repetida
    for(RolEntity row : roles){
      if (row.getDescripcion().trim().equalsIgnoreCase(rol.getDescripcion().trim()) && row.getIdRol().intValue() != idRol) {
        response.setSuccess(false);
        response.setCode("REQUEST");
        response.setMessage("El rol \"" + row.getRol().trim() + "\" ya tiene la descripción \"" + rol.getDescripcion() + "\"");
        return false;
      }
    }

    // No existen repetidos
    return true;
  }
}
