package com.enermex.controller;

import javax.servlet.http.HttpServletResponse;

import com.enermex.dto.legales.LegalesText;
import com.enermex.modelo.LegalesEntity;
import com.enermex.service.LegalesService;
import com.enermex.service.PermisoService;
import com.enermex.utilerias.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/legales")
public class LegalesController {
  @Autowired
  private LegalesService service;

  @Autowired
  private PermisoService permisoService;




  /**
   * Guarda terminos legales
   * @param oauth
   * @param file
   * @return
   */
  @PostMapping("/terminos/upload")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> uploadTerminos(@RequestHeader(value="Authorization") String oauth, @RequestParam("file") MultipartFile file) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "LEGALES", "ACCESS")) {
      return response;
    }

    response.setSuccess(file != null && service.uploadTerminos(file));
    return response;
  }

  /**
   * Actualiza aviso de privacidad
   * @param oauth
   * @param file
   * @return
   */
  @PostMapping("/privacidad/upload")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> uploadPrivacidad(@RequestHeader(value="Authorization") String oauth, @RequestParam("file") MultipartFile file) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "LEGALES", "ACCESS")) {
      return response;
    }

    response.setSuccess(file != null && service.uploadPrivacidad(file));
    return response;
  }

    /**
     *
     * @param oauth
     * @param content
     * @return
     */
    @PostMapping("/terminos/text")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> textTerminos(@RequestHeader(value="Authorization") String oauth, @RequestBody LegalesText content) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "LEGALES", "ACCESS")) {
      return response;
    }

    response.setSuccess(content != null && service.textTerminos(content.getText()));
    return response;
  }

    /**
     *
     * @param oauth
     * @param content
     * @return
     */
    @PostMapping("/privacidad/text")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> textPrivacidad(@RequestHeader(value="Authorization") String oauth,  @RequestBody LegalesText content) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "LEGALES", "ACCESS")) {
      return response;
    }

    response.setSuccess(content != null && service.textPrivacidad(content.getText()));
    return response;
  }

    /**
     *
     * @param response
     * @return
     */
    @RequestMapping(value = "/terminos", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/pdf")
  public ResponseEntity<InputStreamResource> downloadTerminos(HttpServletResponse response) {
    InputStreamResource resource = service.downloadTerminos();

    if(resource != null) {
      response.setHeader("Content-Disposition", "attachment; filename=\"Términos y condiciones.pdf\"");
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  };

    /**
     *
     * @param response
     * @return
     */
    @RequestMapping(value = "/privacidad", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/pdf")
  public ResponseEntity<InputStreamResource> downloadPrivacidad(HttpServletResponse response) {
    InputStreamResource resource = service.downloadPrivacidad();

    if(resource != null) {
      response.setHeader("Content-Disposition", "attachment; filename=\"Aviso de privacidad.pdf\"");
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  };

    /**
     *
     * @return
     */
    @GetMapping("/terminos/text")
  public RestResponse<String> getTextTerminos() {
    final RestResponse<String> response = new RestResponse<String>();
    LegalesEntity legales = service.legales();

    response.setSuccess(legales != null);
    response.setData(legales != null ? legales.getTerminosText() : null);
    return response;
  }

    /**
     *
     * @return
     */
    @GetMapping("/privacidad/text")
  public RestResponse<String> getTextPrivacidad() {
    final RestResponse<String> response = new RestResponse<String>();
    LegalesEntity legales = service.legales();

    response.setSuccess(legales != null);
    response.setData(legales != null ? legales.getPrivacidadText() : null);
    return response;
  }



  
}