package com.enermex.controller;

import java.util.Date;
import java.util.List;

import com.enermex.enumerable.FaqEstatus;
import com.enermex.json.FaqsIncidencias;
import com.enermex.modelo.FaqEntity;
import com.enermex.modelo.IncidenciaEntity;
import com.enermex.service.FaqService;
import com.enermex.service.IncidenciaService;
import com.enermex.service.PermisoService;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO: Completar
 */
@RestController
@RequestMapping("/api/faqs")
public class FaqController {
  private Logger logger = LoggerFactory.getLogger(FaqController.class);

  @Autowired
  private FaqService service;
  
  @Autowired
  private IncidenciaService incidenciaService;

  /**
   * Listado de usuarios
   * 
   * @return RestResponse con la lista de usuarios
   */
  @GetMapping("/all")
  public RestResponse<FaqsIncidencias> getAll() {
    final RestResponse<FaqsIncidencias> response = new RestResponse<FaqsIncidencias>();

    final List<FaqEntity> faqs = service.queryActivos();
    final List<IncidenciaEntity> incidencias = incidenciaService.queryActivos();
    final FaqsIncidencias all = new FaqsIncidencias();

    all.setFaqs(faqs);
    all.setIncidencias(incidencias);

    if (faqs != null && incidencias != null) {
      response.setSuccess(true);
      response.setData(all);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  @Autowired
  private PermisoService permisoService; 

  /**
   * Obtiene preguntas frequentes
   * @param oauth
   * @return
   */
  @GetMapping
  public RestResponse<List<FaqEntity>> getFaqs(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<FaqEntity>> response = new RestResponse<List<FaqEntity>>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "ACCESS")) {
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    final List<FaqEntity> incidencias = service.queryAll();
    if (incidencias != null) {
      response.setSuccess(true);
      response.setData(incidencias);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Obtiene pregunta frequente por id
   * @param oauth
   * @param id
   * @return
   */
  @GetMapping("/{id}")
  public RestResponse<FaqEntity> getFaq(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id) {
    final RestResponse<FaqEntity> response = new RestResponse<FaqEntity>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "ACCESS")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    FaqEntity incidencia = service.queryById(id);

    if (incidencia != null) {
      response.setSuccess(true);
      response.setData(incidencia);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("No existe la incidencia solicitada");
    }

    return response;
  }

  /**
   * Guarda pregunta frequente
   * @param oauth
   * @param faq
   * @return
   */
  @PostMapping()
  public RestResponse<Integer> postFaq(@RequestHeader(value="Authorization") String oauth, @RequestBody FaqEntity faq) {
    final RestResponse<Integer> response = new RestResponse<Integer>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "CREATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    if(!validRepeated(response, faq, 0)){
      return response;
    }

    if (service.create(faq)) {
      response.setSuccess(true);
      response.setData(faq.getIdFaq());
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Actualiza pregunta frequente
   * @param oauth
   * @param id
   * @param faq
   * @return
   */
  @PutMapping("/{id}")
  public RestResponse<Void> putFaq(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id, @RequestBody FaqEntity faq) {
    // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "UPDATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    if(!validRepeated(response, faq, id)){
      return response;
    }

    // Algunos campos no se deben actualizar por este medio
    FaqEntity db = service.queryById(id);

    if(db != null) {
      faq.setIdFaq(db.getIdFaq());
      faq.setFechaCreacion(db.getFechaCreacion());

      if(faq.getEstatus() == FaqEstatus.INACTIVO && db.getEstatus() != FaqEstatus.INACTIVO) {
        faq.setFechaBaja(new Date());
      } else {
        faq.setFechaBaja(db.getFechaBaja());
      }
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Trata de actualizar una faq inexistente");
      return response;
    }

    // TODO: Validar ID
    if (service.update(faq)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Elimina pregunta frequente
   * @param oauth
   * @param id
   * @return
   */
  @DeleteMapping("/{id}")
  public RestResponse<Integer> deleteFaq(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id) {
    // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
    final RestResponse<Integer> response = new RestResponse<Integer>();

    if(!permisoService.testPerm(oauth, "SOPORTE", "DELETE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    FaqEntity faq = service.queryById(id);

    // TODO: Validar ID
    if (faq != null && service.delete(faq)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Valida pregunta frequente
   * @param response
   * @param faq
   * @param idFaq
   * @return
   */
  // REQ: Itzia V. - No debe permitirse crear un nombre de faq repetido
  private boolean validRepeated(RestResponse response, FaqEntity faq, int idFaq) {
    // Se obtiene la lista de roles para validar que no existan repetidos
    List<FaqEntity> roles = service.queryAll();

    // Logger si no se pueden obtener los roles
    if(roles == null) {
      logger.error("Error al validar las faqs repetidas; se asume que no existen.");
      return true;
    }
    
    // Se valida que el nombre no sea repetido
    for(FaqEntity row : roles) {
      if(row.getPregunta().trim().equalsIgnoreCase(faq.getPregunta().trim()) && row.getIdFaq().intValue() != idFaq) {
        response.setSuccess(false);
        response.setCode("REQUEST");
        response.setMessage("Ya existe la pregunta frecuente \"" + faq.getPregunta() + "\"");
        return false;
      }
    }

    // No existen repetidos
    return true;
  }
}
