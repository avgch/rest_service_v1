package com.enermex.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.service.ProductoServicioService;
import com.enermex.utilerias.RestResponse;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/producto")
public class ProductoServicioController {

	
	@Autowired
	ProductoServicioService productoServicioService;
	
	
	/**
	 * Obtiene las notificaciones para MOVILES
	 * @param oauth
	 * @return
	 */
	@GetMapping
	@RolesAllowed({"ROLE_WEB","ROLE_CLIENTE"})
	public ResponseEntity<RestResponse>  getAll(@RequestHeader(value="Authorization") String oauth) {
		RestResponse response = new RestResponse();
		
	  try {

	     System.out.println("Obteniendo productos y servicios");
	 
	     response.setSuccess(true);
	     response.setCode("200");
	     response.setMessage("Consulta exitosa");
	     response.setData(productoServicioService.getAllProdSer());
	       
	        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	       
	       }
	       catch(Exception e) {
	       
	        response.setCode("500");
	        response.setMessage(e.getMessage());
	       
	   return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	       }
	  }
	
	
	
	
	
}
