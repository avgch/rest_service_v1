package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import com.enermex.enumerable.ClienteFiscalEstatus;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteFiscalEntity;
import com.enermex.service.ClienteFiscalService;
import com.enermex.service.PedidoFacturaService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/clientes/fiscal")
public class ClienteFiscalController {
  private Logger logger = LoggerFactory.getLogger(ClienteFiscalController.class);

  @Autowired
  private ClienteFiscalService service;
  
  @Autowired
  private PedidoFacturaService facturaService;

  @Autowired
  private Token token;

  /**
   * Obtiene los datos fiscales
   * @param oauth
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping()
  public RestResponse<List<ClienteFiscalEntity>> getDatosFiscalesLista(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<ClienteFiscalEntity>> response = new RestResponse<List<ClienteFiscalEntity>>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }

    // Consulta de las datos fiscales del cliente
    final List<ClienteFiscalEntity> datosFiscales = service.queryAllByIdCliente(cliente.getIdCliente());

    // Preparación de la respuesta
    response.setData(datosFiscales);
    response.setSuccess(datosFiscales != null);
    response.setMessage(datosFiscales != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Obtiene datos fiscales por id cliente
   * @param oauth
   * @param id
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/{id}")
  public RestResponse<ClienteFiscalEntity> getDatosFiscales(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") BigInteger id) {
    final RestResponse<ClienteFiscalEntity> response = new RestResponse<ClienteFiscalEntity>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }
    
    // Consulta de la factura del cliente
    ClienteFiscalEntity factura = service.queryById(id);

    // Bandera que indica si la factura existe y pertenece al cliente
    boolean valido = verificaNulabilidad(cliente, factura, id) && verificaAsociacion(cliente, factura);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? factura : null);
    response.setMessage(!valido ? "No existe la factura solicitada" : null);

    return response;
  }

  /**
   * Guarda cliente fiscal
   * @param oauth
   * @param datosFiscales
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @PostMapping()
  public RestResponse<BigInteger> postDatosFiscales(@RequestHeader(value="Authorization") String oauth, @RequestBody ClienteFiscalEntity datosFiscales) {
    final RestResponse<BigInteger> response = new RestResponse<BigInteger>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Bandera que indica si la dirección se pudo crear
    datosFiscales.setIdCliente(cliente.getIdCliente());
    boolean valido = service.create(datosFiscales);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? datosFiscales.getIdFiscal() : null);
    response.setMessage(!valido ? "Servicio no disponible. Por favor, inténtelo más tarde" : null);

    return response;
  }

  /**
   * Actualiza cliente fiscal
   * @param oauth
   * @param id
   * @param fiscal
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @PutMapping("/{id}")
  public RestResponse<BigInteger> putDatosFiscales(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") BigInteger id, @RequestBody ClienteFiscalEntity fiscal) {
    final RestResponse<BigInteger> response = new RestResponse<BigInteger>();
    
    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Se verifica que no existan facturas para el identificador fiscal, si es así, se inmuta
    boolean valido = false;

    if(facturaService.existsByIdFiscal(id)) {
      fiscal.setIdFiscal(null);
      fiscal.setIdCliente(cliente.getIdCliente());
      fiscal.setEstatus(ClienteFiscalEstatus.ACTIVO);

      valido = verificaAsociacion(cliente, fiscal) && service.create(fiscal);

      // El identificador fiscal anterior se inactiva
      if(valido) {
        ClienteFiscalEntity anterior = service.queryById(id);

        if(anterior != null) {
          anterior.setEstatus(ClienteFiscalEstatus.INACTIVO);
          service.update(anterior);
        }
      }

    } else {
      fiscal.setIdFiscal(id);
      fiscal.setIdCliente(cliente.getIdCliente());
      fiscal.setEstatus(ClienteFiscalEstatus.ACTIVO);

      valido = service.update(fiscal);
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? fiscal.getIdFiscal() : null);
    response.setMessage(!valido ? "Servicio no disponible. Por favor, inténtelo más tarde" : null);

    return response;
  }

  /**
   * Elimina cliente fiscal
   * @param oauth
   * @param id
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @DeleteMapping("/{id}")
  public RestResponse<Void> deleteDatosFiscales(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") BigInteger id) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Consulta de los datos fiscales
    ClienteFiscalEntity fiscal = service.queryById(id);

    boolean valido = verificaNulabilidad(cliente, fiscal, id) && verificaAsociacion(cliente, fiscal) && service.delete(fiscal);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(!valido ? "Servicio no disponible. Por favor, inténtelo más tarde" : null);

    return response;
  }

  /**
   * Valida nulabilidad
   * @param cliente
   * @param fiscal
   * @param id
   * @return
   */
  private boolean verificaNulabilidad(Cliente cliente, ClienteFiscalEntity fiscal, BigInteger id) {
    if(fiscal != null) {
      return true;
    }

    // TODO: Auditoría
    logger.error("Se intentó consultar datos fiscales inexistentes "+ id + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }

  /**
   * Verifica asociacion
   * @param cliente
   * @param datosFiscales
   * @return
   */
  private boolean verificaAsociacion(Cliente cliente, ClienteFiscalEntity datosFiscales) {
    if(datosFiscales.getIdCliente() != null && cliente.getIdCliente().compareTo(datosFiscales.getIdCliente()) == 0) {
      return true;
    }

    // TODO: Auditoría
    logger.error("Se intentó acceder a datos fiscales con identificador "+ datosFiscales.getIdFiscal() + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }
}