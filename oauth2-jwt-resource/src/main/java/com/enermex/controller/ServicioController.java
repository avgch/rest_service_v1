package com.enermex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.utilerias.RestResponse;
import com.enermex.service.PipaService;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("servicio")
public class ServicioController {
	
	
	
	

	@Autowired
	PipaService pipaService;
	
	@Autowired
	RestResponse response;
	
	
	/**
	 * Guarda servicios
	 * @return
	 */
	@GetMapping
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@ResponseBody
	public ResponseEntity<Object> get(){
	
		
	try {
		
		
		response.setCode("200");
		response.setMessage("Se obtuvo la informacion exitosamente");

		
		response.setData(null);
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
		
    	
    	 }
    	catch(Exception e) {
    		
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		
    		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
  
    	
	}
	
	
	
	/*
    
	@RequestMapping(value = "/pipa", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@ResponseBody
    public ResponseEntity<PipaDto> getPipaById(@RequestParam("id") Integer id) {
    	
    	try {
    		PipaDto pipa = this.pipaService.findPipaById(id);
    		
    		
    		return new ResponseEntity<PipaDto>(pipa, HttpStatus.OK);
    	 }

    	catch(Exception e) {
    
    		return null;
    	}
    
    }
	
	@PostMapping
	public ResponseEntity<String> save(@RequestBody PipaDto pipa){
		
		
		try {
			
			pipaService.saveUpdate(pipa);

		     
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<>("El usuario se agrego correctamente", HttpStatus.OK);
	}
	
	
	@PutMapping
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public ResponseEntity<String> update(@RequestBody PipaDto pipa){
		
		
		try {
			
			pipaService.update(pipa);

		     
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<>("El usuario se agrego correctamente", HttpStatus.OK);
	}
	
	
	
	*/
	

}
