package com.enermex.controller;

import java.util.List;

import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteDireccionEntity;
import com.enermex.service.ClienteDireccionService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador de direcciones del cliente
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 5 de febrero de 2020
 * Última modificación: 28 de febrero de 2020
 */
@RestController
@RequestMapping("/api/clientes")
public class ClienteDireccionController {
  private Logger logger = LoggerFactory.getLogger(ClienteDireccionController.class);

  @Autowired
  private ClienteDireccionService service;

  @Autowired
  private Token token;

  /**
   * Listado de direcciones activas del usuario
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @param idCliente Identificador del cliente; se establece como parte del Path
   * @return RestResponse con la lista de direcciones
   * 
   * @deprecated El cliente no debe consultar información ajena a su cuenta; utilice /api/clientes/direcciones en su lugar
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/{idCliente}/direcciones")
  @Deprecated
  public RestResponse<List<ClienteDireccionEntity>> getDireccionesDeprecated(@RequestHeader(value="Authorization") String oauth, @PathVariable("idCliente") long idCliente) {
    return getDirecciones(oauth);
  }

  /**
   * Listado de direcciones activas del cliente
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @return RestResponse con la lista de direcciones
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/direcciones")
  public RestResponse<List<ClienteDireccionEntity>> getDirecciones(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<ClienteDireccionEntity>> response = new RestResponse<List<ClienteDireccionEntity>>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }

    // Consulta de las direcciones del cliente
    final List<ClienteDireccionEntity> direccion = service.queryAll(cliente.getIdCliente().longValue());

    // Preparación de la respuesta
    response.setData(direccion);
    response.setSuccess(direccion != null);
    response.setMessage(direccion != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Obtiene una dirección del cliente
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @param idCliente Identificador del cliente; se establece como parte del Path
   * @param id Identificador de la dirección; se establece como parte del Path
   * @return RestResponse con la dirección
   * 
   * @deprecated El cliente no debe consultar otra información; utilice /api/clientes/direcciones/{id} en su lugar
   */
  @Deprecated
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/{idCliente}/direcciones/{id}")
  public RestResponse<ClienteDireccionEntity> getDireccionDeprecated(@RequestHeader(value="Authorization") String oauth, @PathVariable("idCliente") long idCliente, @PathVariable("id") long id) {
    return getDireccion(oauth, id);
  }

  /**
   * Obtiene una dirección del cliente
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @param id Identificador de la dirección; se establece como parte del Path
   * @return RestResponse con la dirección
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/direcciones/{id}")
  public RestResponse<ClienteDireccionEntity> getDireccion(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<ClienteDireccionEntity> response = new RestResponse<ClienteDireccionEntity>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }
    
    // Consulta de la dirección del cliente
    ClienteDireccionEntity direccion = service.queryById(id);

    // Bandera que indica si la dirección existe y pertenece al cliente
    boolean valido = verificaNulabilidad(cliente, direccion, id) && verificaAsociacion(cliente, direccion);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? direccion : null);
    response.setMessage(!valido ? "No existe la dirección solicitada" : null);

    return response;
  }

  /**
   * Crea una nueva dirección del cliente
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @param idCliente Identificador del cliente; se establece como parte del Path
   * @param direccion Datos de la dirección del cliente; se establece como el cuerpo de la solicitud
   * @return RestResponse con el identificador autonumérico
   * 
   * @deprecated El cliente no debe consultar otra información; utilice /api/clientes/direcciones en su lugar
   */
  @Deprecated
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @PostMapping("/{idCliente}/direcciones")
  public RestResponse<Long> postDireccionDeprecated(@RequestHeader(value="Authorization") String oauth, @PathVariable("idCliente") long idCliente, @RequestBody ClienteDireccionEntity direccion) {
    return postDireccion(oauth, direccion);
  }

  /**
   * Crea una nueva dirección del cliente
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @param direccion Datos de la dirección del cliente; se establece como el cuerpo de la solicitud
   * @return RestResponse con el identificador autonumérico
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @PostMapping("/direcciones")
  public RestResponse<Long> postDireccion(@RequestHeader(value="Authorization") String oauth, @RequestBody ClienteDireccionEntity direccion) {
    final RestResponse<Long> response = new RestResponse<Long>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Bandera que indica si la dirección se pudo crear
    boolean valido = service.create(cliente.getIdCliente().longValue(), direccion);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? direccion.getIdDireccion() : null);
    response.setMessage(!valido ? "Servicio no disponible. Por favor, inténtelo más tarde" : null);

    return response;
  }

  /**
   * Eliminación lógica de una dirección del cliente
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @param idCliente Identificador del cliente; se establece como parte del Path
   * @param id Identificador de la dirección
   * @return RestResponse con el resultaco de la operación (vacío)
   * 
   * @deprecated El cliente no debe consultar otra información; utilice /api/clientes/direcciones/{id} en su lugar
   */
  @Deprecated
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @DeleteMapping("/{idCliente}/direcciones/{id}")
  public RestResponse<Void> deleteUsuarioDeprecated(@RequestHeader(value="Authorization") String oauth, @PathVariable("idCliente") long idCliente, @PathVariable("id") long id) {
    return deleteUsuario(oauth, id);
  }

  /**
   * Eliminación lógica de una dirección del cliente
   * 
   * @param oauth Token oAuth de cuenta del cliente; se establece en el encabezado HTTP Authorization
   * @param id Identificador de la dirección
   * @return RestResponse con el resultaco de la operación (vacío)
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @DeleteMapping("/direcciones/{id}")
  public RestResponse<Void> deleteUsuario(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Consulta de la dirección del cliente y su asociación al mismo
    ClienteDireccionEntity direccion = service.queryById(id);
    boolean asociado = verificaNulabilidad(cliente, direccion, id) && verificaAsociacion(cliente, direccion);

    // Bandera que indica si la dirección se pudo eliminar
    boolean valido = asociado && service.delete(direccion);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(!valido ? "No existe la dirección especificada" : null);

    return response;
  }

  /**
   * Verifica que la operación sea legal; garantizando que la dirección exista
   * 
   * @param cliente Cliente registrado en base de datos
   * @param direccion Dirección registrada en base de datos
   * @param id Identificador de la dirección
   * @return Resultado de la verificación
   */
  private boolean verificaNulabilidad(Cliente cliente, ClienteDireccionEntity direccion, long id) {
    if(direccion != null) {
      return true;
    }

    // TODO: Auditoría
    logger.error("Se intentó acceder a la dirección inexistente "+ id + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }

  /**
   * Verifica que la operación sea legal; garantizando que la dirección pertenezca al cliente
   * 
   * @param cliente Cliente registrado en base de datos
   * @param direccion Dirección registrada en base de datos
   * @return Resultado de la verificación
   */
  private boolean verificaAsociacion(Cliente cliente, ClienteDireccionEntity direccion) {
    if(cliente.getIdCliente().longValue() == direccion.getIdCliente().longValue()) {
      return true;
    }

    // TODO: Auditoría
    logger.error("Se intentó acceder a la dirección "+ direccion.getIdDireccion() + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }
}
