package com.enermex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import com.enermex.dto.UsuarioDto;
import com.enermex.service.TokenService;
import com.enermex.service.UsuarioServiceA;
import java.security.Principal;
import java.util.List;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.utilerias.UrlSigner;
import com.enermex.map.PasswordChange;
import com.enermex.modelo.TokenEntity;
import com.enermex.config.DefaultPasswordEncoderFactories;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/users")
public class UserControllerA {
	
	@Autowired
	private UsuarioServiceA usuarioService;
	
	
	@Autowired
	SmtpGmailSsl sendMail;
	
	// REQ: Itzia V. - Un token no debe usarse más de una vez
	@Autowired
	private UrlSigner signer;
	@Autowired
	private TokenService tokenService;
	
    
	private PasswordEncoder passwordEncoder;
	
    /**
     * perfil
     * @param principal
     * @return
     */
    @GetMapping("profile")
    @PreAuthorize("hasRole('ROLE_CLIENTE')")
    public ResponseEntity<Principal> get(final Principal principal) {
        return ResponseEntity.ok(principal);
    }
	
	/**
	 * Regresa usuarios del sistema
	 * @return
	 */
    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
    @ResponseBody
    public ResponseEntity<List<UsuarioDto>> getUsuarios() {
            
        try {
        	
        	return new ResponseEntity<>(usuarioService.getAll(), HttpStatus.OK);	
        	
        }
        catch(Exception e) {
        	
        	return new ResponseEntity<>(null,HttpStatus.OK);
        }
    	
    
    }
    
    /**
     * Guarda usuario
     * @param userP
     * @return
     */
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
    public ResponseEntity<String> create(@RequestBody UsuarioDto userP) {
    	
    	try {
    		
    		usuarioService.save(userP);	
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<>("El usuario se agrego correctamente", HttpStatus.OK);
    }
    
    /**
     * Actualiza usuario
     * @param userP
     * @return
     */
    @PutMapping
    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
    public ResponseEntity<String> update(@RequestBody UsuarioDto userP) {
    	
    	try {
    		
    		usuarioService.update(userP);	
    		return new ResponseEntity<>("El usuario se actualizo correctamente", HttpStatus.OK);
    	 }

    	catch(Exception e) {
    		System.out.println("Meesage:"+e.getMessage());
    		System.out.println("Meesagee"+e.getCause());
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    
    }
    
    
    
    /**
     * Elimina usuario
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
    	
    	try {
    		
    		usuarioService.delete(id);	
    		return new ResponseEntity<>("El usuario se elimino", HttpStatus.OK);
    	 }

    	catch(Exception e) {
    		System.out.println("Meesage:"+e.getMessage());
    		System.out.println("Meesagee"+e.getCause());
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    
    }
    
    
    /**
     * Registro de usuarios de la APP
     * @param userP
     * @return
     */
    @PostMapping("/register")
    public String register(@RequestBody UsuarioDto userP) {
    	
    	    usuarioService.register(userP);
        return "Se ha creado correctamente";
    }

    /**
     * Inicia el flujo de la recuperación de la contraseña
	 * 
     * @param correo Correo electrónico del usuario; se recive como
	 *        parámetro de la URL
     * @return
     */
	@PostMapping("/recovery")
    public RestResponse<Void> recovery(
			@RequestParam(name = "correo")String correo) {
		UsuarioDto user = (UsuarioDto) usuarioService.validateEmail(correo);
		RestResponse<Void> response = new RestResponse<>();
    	
    	if(user!=null) {
				// REQ: Itzia V. - Un token no debe usarse más de una vez
				TokenEntity tokenEntity = new TokenEntity();
				tokenEntity.setAplicacion("M");
				tokenEntity.setIdUsuario((long)user.getIdUsuario());

				if(tokenService.create(tokenEntity)){
					final String token = signer.sign(tokenEntity.getIdToken().toString());
					sendMail.sendEmail(correo, token,"recoveryPass", "WEB");
					
					response.setSuccess(true);
				} else {
					response.setSuccess(false);
					response.setCode("500");
					response.setMessage("Error en el envío de correo");
				}
    	} else {
    		response.setSuccess(false);
			response.setCode("500");
			response.setMessage("Correo inválido");
		}
		
    	return response;
    }
    
    /**
     * Actualiza la contraseña de un usuario determinado
	 * 
     * @param newPassword Objeto con token y nueva contraseña
     * @return
     */
    @PostMapping("/password")
    public RestResponse<Void> updatePass(@RequestBody PasswordChange newPassword) {
    	
		String email = signer.unbox(newPassword.getToken());
		UsuarioDto user = (UsuarioDto)usuarioService.validateEmail(email);
		RestResponse<Void> response = new RestResponse<>();

    	if(user != null) {
    		
			usuarioService.updateContrasena(user, this.passwordEncoder().encode(newPassword.getPassword()));
			response.setSuccess(true);
    	} else {
			response.setSuccess(false);
			response.setCode("500");
			response.setMessage("Operación fallida, intente de nuevo más tarde");
    	}
    	
        return response;
    }
    
    
    /**
     * Verifica que el token de recuperación sea válido
	 * 
     * @param justToken Objeto de contraseña, solo con el token
     * @return
     */
    @PostMapping("/recoveryToken")
    public RestResponse<Void> validaToken(
			@RequestBody PasswordChange justToken) {
		String email = signer.unbox(justToken.getToken());
		UsuarioDto user = (UsuarioDto) usuarioService.validateEmail(email);
		RestResponse<Void> response = new RestResponse<>();

    	if(user != null) {
			response.setSuccess(true);
    	} else {
			response.setSuccess(false);
			response.setCode("500");
			response.setMessage("Token inválido");
    	}

        return response;
	}
    
    /**
     *
     * @return
     */
    @Bean(name="passwordEncoderBean")
    public PasswordEncoder passwordEncoder() {
        if (passwordEncoder == null) {
            passwordEncoder = DefaultPasswordEncoderFactories.createDelegatingPasswordEncoder();
        }
        return passwordEncoder;
    }
    
}
