package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.service.PipaService;
import com.enermex.utilerias.RestResponse;

import com.enermex.dto.PipaDto;
import com.enermex.modelo.Pipa;
 
/**
 * Guarda pipas
 * @author abraham
 *
 */
@RestController
@RequestMapping("/api/pipas")
public class PipasController {
	
	
	
	@Autowired
	PipaService pipaService;
	
	@Autowired
	RestResponse response;
	
    /**
     *
     * @return
     */
    @GetMapping
	@PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
	public ResponseEntity<RestResponse> getPipas(){
	
		
	try {
		response.setData(pipaService.getPipas());
		response.setCode("200");
		response.setSuccess(true);
		response.setMessage("Consulta exitosa");
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }
    	catch(Exception e) {
    	
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
	
    /**
     * Consulta de pipa por id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
    public ResponseEntity<RestResponse> getPipaById(@PathVariable Integer id) {
    	
    	try {
    		
    		response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Consulta de pipa id exitosa");
    		Pipa pipa = this.pipaService.findPipaById(id);
    		response.setData(pipa);
    		
    		
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }

    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	}
    
    }
	/**
	 * Guarda pipas
	 * @param pipa
	 * @return
	 */
	@PostMapping
	@PreAuthorize("hasRole('ROLE_WEB')")
	public ResponseEntity<RestResponse> save(@RequestBody PipaDto pipa){
		
		
		try {
        // INTER: José Antonio González Hernández
        // REQ: Itzia V. - No se debe repetir el nombre del autotanque
        List<Pipa> pipas = pipaService.getPipas();

        if(pipas != null) {
          for(Pipa item : pipas) {
            if(item.getNombre() != null && pipa.getNombre() != null && item.getNombre().trim().equalsIgnoreCase(pipa.getNombre().trim())) {
              response.setSuccess(false);
              response.setMessage("Un autotanque con el nombre \"" + pipa.getNombre() + "\" ya está registrado");

              return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
            }
          }
        }

        // INTER: END
			
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Registro exitoso");
			pipaService.saveUpdate(pipa);
			response.setData(null);

			return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
	/**
	 * Actualiza pipas
	 * @param pipa
	 * @return
	 */
	@PutMapping
	@PreAuthorize("hasRole('ROLE_WEB')")
	public ResponseEntity<RestResponse> update(@RequestBody PipaDto pipa){
		
		
		try {

	    /*
	     * fecha update  01-05-2020
	     * Nota: No se podia actualizar una pipa existente  sin cambiarle el nombre
	     * Autor: Abraham Vargas
	     *  */
        // INTER: José Antonio González Hernández
        // REQ: Itzia V. - No se debe repetir el nombre del autotanque
        boolean seActualiza = pipaService.existeNombrePipa(pipa.getNombre(), pipa.getIdPipa());

        if(!seActualiza) {
          response.setSuccess(false);
          response.setMessage("Un autotanque con el nombre \"" + pipa.getNombre() + "\" ya está registrado");
        	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
        }
        	

        // INTER: END
			
			response.setCode("200");
    		response.setSuccess(true);
    		response.setMessage("Actualizacion exitosa");
			pipaService.update(pipa);
			return new ResponseEntity<RestResponse>(response, HttpStatus.OK);

		     
    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
 
	}
	
	/**
	 * Obtiene pipas activas
	 * @return
	 */
	@GetMapping("/activas")
	@PreAuthorize("hasRole('ROLE_WEB')")
	@ResponseBody
	public ResponseEntity<RestResponse> getPipasActivas(){
	
		
	try {
		response.setData(pipaService.getPipasActivas());
		response.setCode("200");
		response.setSuccess(true);
		response.setMessage("Consulta exitosa");
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    	 }
    	catch(Exception e) {
    	
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}

}
