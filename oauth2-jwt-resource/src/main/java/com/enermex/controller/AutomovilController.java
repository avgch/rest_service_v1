package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.dto.AutomovilClienteDto;
import com.enermex.service.AutomovilClienteService;
import com.enermex.utilerias.RestResponse;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/automovil")
public class AutomovilController {
	
	
	
	private RestResponse response;
	
	@Autowired
	private AutomovilClienteService automovilClienteService;
	
	/**
	 * Guarda 
	 * @param auto
	 * @return
	 */
	@PostMapping
	public ResponseEntity<RestResponse> save(@RequestBody AutomovilClienteDto auto){
		
		response = new RestResponse();
		
		try {
			
			response.setCode("200");
			response.setMessage("Registro exitoso");
		    

		     
    	 }
    	catch(Exception e) {
    		response.setCode("500");
			response.setMessage("Registro no exitoso");
    		
    		
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	
	/**
	 * Actualiza
	 * @param auto
	 * @return
	 */
	@PutMapping("/cliente")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> update(@RequestBody AutomovilClienteDto auto){
		response = new RestResponse();
		
		try {
			
			response.setCode("200");
			response.setSuccess(true);
			response.setMessage("Actualización exitosa");
			this.automovilClienteService.updateClienteCar(auto);
  
		     
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	/**
	 * Obtiene todos
	 * @param id
	 * @return
	 */
	@GetMapping("/cliente/{id}")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	@ResponseBody
	public  ResponseEntity<RestResponse> getAll(@PathVariable BigInteger id){
		response = new RestResponse(); 
		
	try {
		
		response.setSuccess(true);
		response.setMessage("Registro exitoso");
		response.setData(automovilClienteService.getAll(id));
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	
    	
    	 }
    	catch(Exception e) {
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
	/**
	 * Elimina
	 * @param id
	 * @return
	 */
    @DeleteMapping("/cliente/{id}")
    @PreAuthorize("hasRole('ROLE_CLIENTE')")
    public ResponseEntity<RestResponse> delete(@PathVariable Integer id) {
    	
    	response = new RestResponse();
    	try {
    		
    		 response.setSuccess(true);
    		 automovilClienteService.delete(id);	
    		
    	 }

    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setMessage("Contacte a sus administrador del sistema");
    		return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    	
    	return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    
    }
	
   
	

}
	