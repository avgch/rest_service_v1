package com.enermex.controller;

import java.util.List;

import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteFcmEntity;
import com.enermex.service.ClienteFcmService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador de tokens FCM del cliente
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 24 de febrero de 2020
 * Última modificación: 28 de febrero de 2020
 */
@RestController
@RequestMapping("/api/clientes/fcm")
public class ClienteFcmController {
  private Logger logger = LoggerFactory.getLogger(ClienteFcmController.class);

  @Autowired
  private ClienteFcmService service;

  @Autowired
  Token token;

  /**
   * Añade un token FCM del dispositivo del cliente
   * 
   * @param oauth
   * @param fcm
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @PostMapping()
  public RestResponse<Void> postDireccion(@RequestHeader(value="Authorization") String oauth, @RequestBody ClienteFcmEntity fcm) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Se asocia al token FCM con el identificador del cliente
    fcm.setIdCliente(cliente.getIdCliente());

    // Bandera que indica si el token FCM se pudo almacenar
    boolean valido = service.create(fcm);

    // Preparación de la respuesta
    response.setSuccess(valido);

    return response;
  }

  /**
   * Elimina un token FCM del dispositivo del cliente
   * 
   * @param oauth
   * @param fcm
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @DeleteMapping()
  public RestResponse<Void> deleteUsuario(@RequestHeader(value="Authorization") String oauth, @RequestBody ClienteFcmEntity fcm) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Consulta de los tokens FCM del cliente
    List<ClienteFcmEntity> fcms = service.queryByIdCliente(cliente.getIdCliente());

    // Bandera que indicará si el token FCM se pudo eliminar
    boolean valido = false;

    // Se busca el token FCM del cliente para eliminarlo
    if(fcms != null) {
      for(ClienteFcmEntity row : fcms) {
        if(row.getFcm().equals(fcm.getFcm())) {
          valido = service.delete(row);
          break;
        }
      }
    }

    // Preparación de la respuesta
    response.setSuccess(valido);

    return response;
  }
}
