package com.enermex.controller;

import java.util.ArrayList;

import com.enermex.dto.website.Contacto;
import com.enermex.dto.website.WebsiteDto;
import com.enermex.service.WebsiteService;
import com.enermex.utilerias.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// TODO: Seguridad
// TODO: Aplicaciones

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/website")
public class WebsiteController {
  @Autowired
  private WebsiteService service;

  /**
   * Obtiene el web site
   * @return
   */
  @GetMapping
  public RestResponse<WebsiteDto> getWebsite() {
    final RestResponse<WebsiteDto> response = new RestResponse<WebsiteDto>();
    
    final WebsiteDto website = service.getWebsite();

    // Preparación de la respuesta
    response.setData(website);
    response.setSuccess(website != null);
    response.setMessage(website != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Intro
   * @param oauth
   * @param intro
   * @return
   */
  @PostMapping("/intro")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> postIntro(@RequestHeader(value="Authorization") String oauth, @RequestBody WebsiteDto intro) {
    final RestResponse<Void> response = new RestResponse<Void>();
    final WebsiteDto website = service.getWebsite();

    // Asignaciones
    website.setIntroTitle(  intro.getIntroTitle());
    website.setIntroContent(intro.getIntroContent());

    // Preparación de la respuesta
    response.setSuccess(service.saveWebsite(website));

    return response;
  }

  /**
   * Servicios
   * @param oauth
   * @param servicios
   * @return
   */
  @PostMapping("/servicios")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> postServicios(@RequestHeader(value="Authorization") String oauth, @RequestBody WebsiteDto servicios) {
    final RestResponse<Void> response = new RestResponse<Void>();
    final WebsiteDto website = service.getWebsite();

    // Asignaciones
    website.setCombustible(servicios.getCombustible());
    website.setLavado(servicios.getLavado());
    website.setPresion(servicios.getPresion());

    // Preparación de la respuesta
    response.setSuccess(service.saveWebsite(website));

    return response;
  }
  /**
  * Planes
  * @param oauth
  * @param planes
  * @return
  */
  @PostMapping("/planes")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> postPlanes(@RequestHeader(value="Authorization") String oauth, @RequestBody WebsiteDto planes) {
    final RestResponse<Void> response = new RestResponse<Void>();
    final WebsiteDto website = service.getWebsite();

    // Asignaciones
    website.setPlanIndividual(planes.getPlanIndividual());
    website.setPlanFamiliar(planes.getPlanFamiliar());
    website.setPlanEmpresarial(planes.getPlanEmpresarial());

    // Preparación de la respuesta
    response.setSuccess(service.saveWebsite(website));

    return response;
  }

  /**
   * Beneficios
   * @param oauth
   * @param beneficios
   * @return
   */
  @PostMapping("/beneficios")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> postBeneficios(@RequestHeader(value="Authorization") String oauth, @RequestBody WebsiteDto beneficios) {
    final RestResponse<Void> response = new RestResponse<Void>();
    final WebsiteDto website = service.getWebsite();
    
    // Asignaciones
    website.setB1Title(beneficios.getB1Title());
    website.setB1Subtitle(beneficios.getB1Subtitle());
    website.setB1Content(beneficios.getB1Content());
    website.setB2Title(beneficios.getB2Title());
    website.setB2Subtitle(beneficios.getB2Subtitle());
    website.setB2Content(beneficios.getB2Content());
    website.setB3Title(beneficios.getB3Title());
    website.setB3Subtitle(beneficios.getB3Subtitle());
    website.setB3Content(beneficios.getB3Content());
    website.setB4Title(beneficios.getB4Title());
    website.setB4Subtitle(beneficios.getB4Subtitle());
    website.setB4Content(beneficios.getB4Content());
    website.setB5Title(beneficios.getB5Title());
    website.setB5Subtitle(beneficios.getB5Subtitle());
    website.setB5Content(beneficios.getB5Content());

    // Preparación de la respuesta
    response.setSuccess(service.saveWebsite(website));

    return response;
  }

  /**
   * Contacto
   * @param oauth
   * @param contacto
   * @return
   */
  @PostMapping("/contacto")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> postContacto(@RequestHeader(value="Authorization") String oauth, @RequestBody WebsiteDto contacto) {
    final RestResponse<Void> response = new RestResponse<Void>();
    final WebsiteDto website = service.getWebsite();
    
    // Asignaciones
    website.setContacto(contacto.getContacto());

    // Preparación de la respuesta
    response.setSuccess(service.saveWebsite(website));

    return response;
  }
 /**
  * Correos
  * @return
  */
  @GetMapping("/correos")
  public String getContactos() {
    final WebsiteDto website = service.getWebsite();

    if(website == null || website.getContacto() == null) {
      return "";
    }

    // Consultamos los contactos
    String[] contactos = website.getContacto().split(";");

    ArrayList<String> trims = new ArrayList<>();
    
    for(String contacto : contactos) {
      trims.add(contacto.trim());
    }

    String rfc2822 = String.join(", ", trims);

    return rfc2822;
  }

  /**
   * Contacto
   * @param contacto
   * @return
   */
  @PostMapping("/forms")
  public RestResponse<Void> postContacto(@RequestBody Contacto contacto) {
    RestResponse<Void> response = new RestResponse<>();

    boolean valido = false;

    if(contacto.getFormulario().intValue() == 1) {
      valido = service.formContacto(contacto);
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    return response;
  }

}