package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import com.enermex.dto.factura.FacturaMovil;
import com.enermex.dto.factura.FacturaVista;
import com.enermex.enumerable.PedidoFacturaEstatus;
import com.enermex.modelo.CatUsoCfdiEntity;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.PedidoFacturaEntity;
import com.enermex.service.CatUsoCfdiService;
import com.enermex.service.PedidoFacturaService;
import com.enermex.service.PermisoService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api")
public class PedidoFacturaController {
  private Logger logger = LoggerFactory.getLogger(PedidoFacturaController.class);

  @Autowired
  private PedidoFacturaService service;

  @Autowired
  private CatUsoCfdiService usoService;

  @Autowired
  private Token token;

  @Autowired
  private PermisoService permisoService;

  /**
   * Guarda factura
   * @param oauth
   * @return
   */
  // PART: SAT
  @GetMapping("/sat/usos")
  public RestResponse<List<CatUsoCfdiEntity>> getUsosCfdi(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<CatUsoCfdiEntity>> response = new RestResponse<List<CatUsoCfdiEntity>>();

    // Consulta de las usos del cfdi
    final List<CatUsoCfdiEntity> usos = usoService.queryAll();

    // Preparación de la respuesta
    response.setData(usos);
    response.setSuccess(usos != null);
    response.setMessage(usos != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Obtiene facturas
   * @param oauth
   * @return
   */
  // PART: Web
  @PreAuthorize("hasRole('ROLE_WEB')")
  @GetMapping("/facturas")
  public RestResponse<List<FacturaVista>> getFacturas(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<FacturaVista>> response = new RestResponse<List<FacturaVista>>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "SOPORTE", "ACCESS")) {
      return response;
    }

    // Consulta de las facturas
    final List<FacturaVista> facturas = service.queryAll();

    // Preparación de la respuesta
    response.setData(facturas);
    response.setSuccess(facturas != null);
    response.setMessage(facturas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Guarda factura
   * @param oauth
   * @param id
   * @return
   */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @PostMapping("/facturas/{id}/restablecer")
  public RestResponse<Void> getFacturas(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") BigInteger id) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "SOPORTE", "ACCESS")) {
      return response;
    }

    // Consulta de la factura
    final PedidoFacturaEntity factura = service.queryById(id);

    boolean valido = false;

    if(factura != null && factura.getEstatus() == PedidoFacturaEstatus.PROCESANDO) {
      factura.setEstatus(PedidoFacturaEstatus.SOLICITADA);
      valido = service.update(factura);
    } else if(factura != null && factura.getEstatus() == PedidoFacturaEstatus.DISPONIBLE) {
      factura.setEstatus(PedidoFacturaEstatus.PROCESANDO);
      valido = service.update(factura);
    }

    // Preparación de la respuesta
    response.setSuccess(valido);

    return response;
  }

 /**
  * Obtiene excel de factras
  * @return
  */
  // TODO: Excel
  @RequestMapping(value = "/facturas/excel", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
  public ResponseEntity<InputStreamResource> fotos() {
    InputStreamResource resource = service.excel();

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Guarda archivo de facturas
   * @param id
   * @param archivo
   * @return
   */
  // TODO: Upload
  @PreAuthorize("hasRole('ROLE_WEB')")
  @PostMapping("/facturas/{id}/upload")
  public RestResponse<Void> postFile(@PathVariable("id") BigInteger id, @RequestParam("file") MultipartFile archivo) {
    final RestResponse<Void> response = new RestResponse<Void>();

    PedidoFacturaEntity factura = service.queryById(id);

    if (factura != null && !archivo.isEmpty() && service.updateFactura(factura, archivo)) {
      response.setSuccess(true);

      // Envío de notificación y correo

    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }
 /**
  * Guarda foto
  * @param id
  * @return
  */
  @PreAuthorize("hasRole('ROLE_WEB')")
  @RequestMapping(value = "/facturas/{id}/file", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/zip")
  public ResponseEntity<InputStreamResource> fotos(@PathVariable("id") BigInteger id) {
    PedidoFacturaEntity factura = service.queryById(id);
    InputStreamResource resource = service.downloadFactura(factura);

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }

  // PART: Facturas del cliente
 /**
  * Guarda factura cliente
  * @param oauth
  * @return
  */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/clientes/facturas")
  public RestResponse<List<FacturaMovil>> getFacturasCliente(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<FacturaMovil>> response = new RestResponse<List<FacturaMovil>>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);

    if(cliente == null) {
      return response;
    }

    // Consulta de las facturas del cliente
    final List<PedidoFacturaEntity> facturas = service.queryAllByIdCliente(cliente.getIdCliente());

    // Preparación de la respuesta
    response.setData(service.mapMovil(facturas));
    response.setSuccess(facturas != null);
    response.setMessage(facturas != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Obtiene factura cliente
   * @param oauth
   * @param id
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/clientes/facturas/{id}")
  public RestResponse<FacturaMovil> getFacturaCliente(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") BigInteger id) {
    final RestResponse<FacturaMovil> response = new RestResponse<FacturaMovil>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }
    
    // Consulta de la factura del cliente
    PedidoFacturaEntity factura = service.queryById(id);

    // Bandera que indica si la factura existe y pertenece al cliente
    boolean valido = verificaNulabilidad(cliente, factura, id) && verificaAsociacion(cliente, factura);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? service.mapMovil(factura) : null);
    response.setMessage(!valido ? "No existe la factura solicitada" : null);

    return response;
  }

  /**
   * Obtiene factura por pedido
   * @param oauth
   * @param idPedido
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @GetMapping("/clientes/facturas/pedidos/{idPedido}")
  public RestResponse<FacturaMovil> getFacturaPorPedido(@RequestHeader(value="Authorization") String oauth, @PathVariable("idPedido") BigInteger idPedido) {
    final RestResponse<FacturaMovil> response = new RestResponse<FacturaMovil>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }
    
    // Consulta de la factura del cliente
    PedidoFacturaEntity factura = service.queryByIdPedido(idPedido);

    // Bandera que indica si la factura existe y pertenece al cliente
    boolean valido = factura != null && verificaAsociacion(cliente, factura);

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? service.mapMovil(factura) : null);
    response.setMessage(!valido ? "No existe la factura solicitada para el servicio" : null);

    return response;
  }

  /**
   * Guarda direccion
   * @param oauth
   * @param factura
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @PostMapping("/clientes/facturas")
  public RestResponse<BigInteger> postDireccion(@RequestHeader(value="Authorization") String oauth, @RequestBody PedidoFacturaEntity factura) {
    final RestResponse<BigInteger> response = new RestResponse<BigInteger>();

    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth, response);
    
    if(cliente == null) {
      return response;
    }

    // Se asignan los datos requeridos
    factura.setEstatus(PedidoFacturaEstatus.SOLICITADA);

    // Bandera que indica si la dirección se pudo crear
    boolean facturado = service.existsByIdPedido(factura.getIdPedido());
    boolean valido = !facturado && verificaAsociacion(cliente, factura) && service.create(factura);

    // Mensaje de respuesta
    String mensaje = "Se enviaron correctamente sus datos para facturar, en breve recibirá su factura por correo";

    if(!valido) {
      mensaje = "Servicio no disponible. Por favor, inténtelo más tarde";
    }

    if(facturado) {
      mensaje = "El pedido ya había sido facturado anteriormente";
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setData(valido ? factura.getIdFactura() : null);
    response.setMessage(mensaje);

    return response;
  }

  /**
   * Obtiene factura por id
   * @param oauth
   * @param id
   * @return
   */
  @PreAuthorize("hasRole('ROLE_CLIENTE')")
  @RequestMapping(value = "/clientes/facturas/{id}/file", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/zip")
  public ResponseEntity<InputStreamResource> facturaCliente(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") BigInteger id) {
    // Obtención del cliente desde oAuth
    Cliente cliente = token.getClienteFromToken(oauth);
    
    if(cliente == null) {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }

    PedidoFacturaEntity factura = service.queryById(id);

    if(factura == null || cliente.getIdCliente().longValue() != service.getIdCliente(factura).longValue()) {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }

    InputStreamResource resource = service.downloadFactura(factura);

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }
 /**
 * Verifica nulabilidad 
 * @param cliente
 * @param factura
 * @param id
 * @return
 */
  private boolean verificaNulabilidad(Cliente cliente, PedidoFacturaEntity factura, BigInteger id) {
    if(factura != null) {
      return true;
    }

    // TODO: Auditoría
    logger.error("Se intentó consultar una factura inexistente "+ id + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }

  /**
   * Verifica asociacion
   * @param cliente
   * @param factura
   * @return
   */
  private boolean verificaAsociacion(Cliente cliente, PedidoFacturaEntity factura) {
    BigInteger idClienteFactura = service.getIdCliente(factura);

    if(idClienteFactura != null && cliente.getIdCliente().compareTo(idClienteFactura) == 0) {
      return true;
    }

    // TODO: Auditoría
    logger.error("Se intentó acceder a la factura "+ factura.getIdFactura() + " desde la cuenta del cliente " + cliente.getIdCliente());
    return false;
  }
}
