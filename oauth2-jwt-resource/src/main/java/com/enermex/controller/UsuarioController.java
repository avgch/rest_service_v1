package com.enermex.controller;

import java.util.Date;
import java.util.List;

import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.modelo.TokenEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.PermisoService;
import com.enermex.service.TokenService;
import com.enermex.service.UsuarioService;
import com.enermex.utilerias.FirebaseRealtime;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.utilerias.Token;
import com.enermex.utilerias.UrlSigner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.enermex.modelo.RolEntity;

/**
 * Controlador de usuarios
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 16 de enero de 2020
 * Última modificación: 19 de marzo de 2020
 */
@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {
  private Logger logger = LoggerFactory.getLogger(UsuarioController.class);

  @Autowired
  private UsuarioService service;
  
  @Autowired
  private PermisoService permisoService;
  
  @Autowired
  SmtpGmailSsl sendMail;

  // REQ: Itzia V. - Un token de cambio de contraseña no debe usarse más de una vez
	@Autowired
  private UrlSigner signer;
  
	@Autowired
  private TokenService tokenService;
  
  @Autowired
  private Token token;

  @Autowired
  private FirebaseRealtime realtime;

  /**
  * Listado de usuarios
  * 
  * @param oauth Token oAuth de cuenta del usuario; se establece en el encabezado HTTP Authorization
  * @return RestResponse con la lista de usuarios
  */
  @GetMapping
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<UsuarioEntity>> getUsuarios(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<UsuarioEntity>> response = new RestResponse<List<UsuarioEntity>>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "USUARIOS", "ACCESS")) {
      return response;
    }
    
    // Lista de usuarios
    final List<UsuarioEntity> usuarios = service.queryAll();

    // Preparación de la respuesta
    response.setData(usuarios);
    response.setSuccess(usuarios != null);
    response.setMessage(usuarios != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
  * Obtiene un usuario
  * 
  * @param oauth Token oAuth de cuenta del usuario; se establece en el encabezado HTTP Authorization
  * @param id Identificador del usuario; se establece como parte del Path
  * @return RestResponse con el usuario
  */
  @GetMapping("/{id}")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<UsuarioEntity> getUsuario(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id) {
    final RestResponse<UsuarioEntity> response = new RestResponse<UsuarioEntity>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "USUARIOS", "ACCESS")) {
      return response;
    }

    UsuarioEntity usuario = service.queryById(id);

    // Preparación de la respuesta
    response.setData(usuario);
    response.setSuccess(usuario != null);
    response.setMessage(usuario != null ? null : "No existe el usuario solicitado");

    return response;
  }

  /**
  * Obtiene al usuario autenticado
  * 
  * @param oauth Token oAuth de cuenta del usuario; se establece en el encabezado HTTP Authorization
  * @return RestResponse con datos del usuario autenticado
  */
  @GetMapping("/token")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<UsuarioEntity> getUsuarioFromToken(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<UsuarioEntity> response = new RestResponse<UsuarioEntity>();

    // Obtención del usuario desde oAuth
    UsuarioEntity usuario = token.getUsuarioFromToken(oauth, response);

    if(usuario == null) {
      return response;
    }

    // Preparación de la respuesta
    response.setData(usuario);
    response.setSuccess(true);

    return response;
  }

  /**
   * Registro de un nuevo usuario
   * 
   * @param oauth Token oAuth de cuenta del usuario; se establece en el encabezado HTTP Authorization
   * @param usuario Usuario por registrar
   * @return
   */
  @PostMapping()
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Long> postUsuario(@RequestHeader(value="Authorization") String oauth, @RequestBody UsuarioEntity usuario) {
    final RestResponse<Long> response = new RestResponse<Long>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "USUARIOS", "ACCESS")) {
      return response;
    }
    
    // Verificar que el correo del usuario no exista
    if(service.exists(usuario.getCorreo().trim().toLowerCase())){
      response.setSuccess(false);
      response.setMessage("El correo que ingresó ya está registrado");
      return response;
    }
    
    // Verificar que el teléfono del usuario no exista
    if(service.existsTelefono(usuario.getTelefono().trim())){
      response.setSuccess(false);
      response.setMessage("El teléfono que ingresó ya está registrado");
      return response;
    }
    
    boolean valido = service.create(usuario);
    
    if(valido) {
      // REQ: Itzia V. - Un token de cambio de contraseña no debe usarse más de una vez
      TokenEntity tokenEntity = new TokenEntity();
      tokenEntity.setAplicacion("W");
      tokenEntity.setIdUsuario(usuario.getIdUsuario());
      
      if(tokenService.create(tokenEntity)){
        final String token = signer.sign(tokenEntity.getIdToken().toString());
        
        // INTER: Abraham Vargas - Envío de correo diferente para despachadores
        boolean esDespachador = false;

        for(RolEntity rol : usuario.getRoles()) {
        	if(rol.getRol().equalsIgnoreCase("Despachador")) {
            esDespachador = true;
            break;
        	}
        }

        if(esDespachador) {
        	sendMail.sendEmail(usuario.getCorreo(), token, "registroDespachador", "WEB");	
        } else {
        	sendMail.sendEmail(usuario.getCorreo(), token, "registroUsuarioWeb", "WEB");	
        }
      }

      // Se establecen los cambios en la pantalla de seguimiento de ruta
      realtime.seguimiento();
    }

    // Preparación de la respuesta
    response.setData(valido ? usuario.getIdUsuario() : null);
    response.setSuccess(valido);
    response.setMessage(valido ? null : "Servicio no disponible. Por favor, inténtelo más tarde");
    return response;
  }

  /**
   * Actualización de un usuario determinado
   * 
   * @param oauth Token oAuth de cuenta del usuario; se establece en el encabezado HTTP Authorization
   * @param id Identificador del usuario
   * @param usuario Datos nuevos del usuario
   * @return
   */
  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Void> putUsuario(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id, @RequestBody UsuarioEntity usuario) {
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "USUARIOS", "ACCESS")) {
      return response;
    }

    // Algunos campos no se deben actualizar por este medio
    UsuarioEntity db = service.queryById(id);

    if(db == null) {
      // TODO: Auditoría
      response.setSuccess(false);
      response.setMessage("Trata de actualizar un usuario inexistente");
      return response;
    }

    // Datos que no se pueden cambiar
    usuario.setIdUsuario(db.getIdUsuario());
    usuario.setFechaCreacion(db.getFechaCreacion());
    usuario.setFotoUri(db.getFotoUri());
    usuario.setFechaBaja(db.getFechaBaja());

    // Registro de la fecha de baja
    if(usuario.getEstatus() == UsuarioEstatus.INACTIVO && db.getEstatus() != UsuarioEstatus.INACTIVO) {
      // TODO: Auditoría
      usuario.setFechaBaja(new Date());
    }

    // Verifica si cambió el correo electrónico
    boolean changeCorreo = !usuario.getCorreo().equalsIgnoreCase(db.getCorreo());

    // Verificar que el correo del usuario no exista
    if(changeCorreo && service.exists(usuario.getCorreo())) {
      response.setSuccess(false);
      response.setMessage("El correo que ingresó ya está registrado");
      return response;
    }

    // Verifica si cambió el correo electrónico
    boolean changeTelefono = !usuario.getTelefono().equalsIgnoreCase(db.getTelefono());
    if(changeTelefono && service.existsTelefono(usuario.getTelefono())) {
      response.setSuccess(false);
      response.setMessage("El teléfono que ingresó ya está registrado");
      return response;
    }

    // Actualización del usuario
    boolean valido = service.update(usuario);

    // Verificar si existe un cambio en el correo
    if(valido && changeCorreo && usuario.getEstatus() == UsuarioEstatus.POR_VERIFICAR) {

      // REQ: Itzia V. - Un token de cambio de contraseña no debe usarse más de una vez
      TokenEntity tokenEntity = new TokenEntity();
      tokenEntity.setAplicacion("W");
      tokenEntity.setIdUsuario(usuario.getIdUsuario());

      if(tokenService.create(tokenEntity)){
        final String token = signer.sign(tokenEntity.getIdToken().toString());
        
        // INTER: Abraham Vargas - Envío de correo diferente para despachadores
        boolean esDespachador = false;

        for(RolEntity rol : usuario.getRoles()) {
        	if(rol.getRol().equalsIgnoreCase("Despachador")) {
            esDespachador = true;
            break;
        	}
        }

        if(esDespachador) {
        	sendMail.sendEmail(usuario.getCorreo(), token, "registroDespachador", "WEB");	
        } else {
        	sendMail.sendEmail(usuario.getCorreo(), token, "registroUsuarioWeb", "WEB");	
        }
      }
    }


    // Se establecen los cambios en la pantalla de seguimiento de ruta, si es válida la actualización
    if(valido) {
      realtime.seguimiento();
    }

    // Preparación de la respuesta
    response.setSuccess(valido);
    response.setMessage(valido ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  // TODO: Seguridad

    /**
     *
     * @param oauth
     * @param id
     * @param archivo
     * @return
     */
  @PostMapping("/{id}/upload/foto")
  public RestResponse<Void> uploadFoto(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") long id, @RequestParam("file") MultipartFile archivo) {
    final RestResponse<Void> response = new RestResponse<Void>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "USUARIOS", "ACCESS")) {
      return response;
    }

    UsuarioEntity usuario = service.queryById(id);

    if (usuario != null && !archivo.isEmpty() && service.updateFoto(usuario, archivo)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  // TODO: Seguridad

    /**
     *
     * @param id
     * @return
     */
  @RequestMapping(value = "/{id}/foto", method = { RequestMethod.GET, RequestMethod.POST }, produces = "image/png")
  public ResponseEntity<InputStreamResource> foto(@PathVariable("id") long id) {
    UsuarioEntity usuario = service.queryById(id);

    InputStreamResource resource = service.downloadFoto(usuario);

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }
}
