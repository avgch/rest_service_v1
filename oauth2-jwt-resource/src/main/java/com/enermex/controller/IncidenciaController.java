package com.enermex.controller;

import java.util.Date;
import java.util.List;

import com.enermex.enumerable.IncidenciaEstatus;
import com.enermex.modelo.IncidenciaEntity;
import com.enermex.service.PermisoService;
import com.enermex.service.IncidenciaService;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/incidencias")
public class IncidenciaController {
  private Logger logger = LoggerFactory.getLogger(IncidenciaController.class);

  @Autowired
  private IncidenciaService service;
  
  @Autowired
  private PermisoService permisoService; 

  /**
   * Obtiene incidencias
   * @param oauth
   * @return
   */
  @GetMapping
  public RestResponse<List<IncidenciaEntity>> getIncidencias(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<IncidenciaEntity>> response = new RestResponse<List<IncidenciaEntity>>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "ACCESS")) {
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    final List<IncidenciaEntity> incidencias = service.queryAll();
    if (incidencias != null) {
      response.setSuccess(true);
      response.setData(incidencias);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Obtiene incidencia por id
   * @param oauth
   * @param id
   * @return
   */
  @GetMapping("/{id}")
  public RestResponse<IncidenciaEntity> getIncidencia(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id) {
    final RestResponse<IncidenciaEntity> response = new RestResponse<IncidenciaEntity>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "ACCESS")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    IncidenciaEntity incidencia = service.queryById(id);

    if (incidencia != null) {
      response.setSuccess(true);
      response.setData(incidencia);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("No existe la incidencia solicitada");
    }

    return response;
  }

  /**
   * Guarda incidencia
   * @param oauth
   * @param incidencia
   * @return
   */
  @PostMapping()
  public RestResponse<Integer> postIncidencia(@RequestHeader(value="Authorization") String oauth, @RequestBody IncidenciaEntity incidencia) {
    final RestResponse<Integer> response = new RestResponse<Integer>();

    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "CREATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    if(!validRepeated(response, incidencia, 0)){
      return response;
    }

    if (service.create(incidencia)) {
      response.setSuccess(true);
      response.setData(incidencia.getIdIncidencia());
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Actualiza incidencia
   * @param oauth
   * @param id
   * @param incidencia
   * @return
   */
  @PutMapping("/{id}")
  public RestResponse<Void> putIncidencia(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id, @RequestBody IncidenciaEntity incidencia) {
    // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
    final RestResponse<Void> response = new RestResponse<Void>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, "SOPORTE", "UPDATE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    if(!validRepeated(response, incidencia, id)){
      return response;
    }

    // Algunos campos no se deben actualizar por este medio
    IncidenciaEntity db = service.queryById(id);

    if(db != null) {
      incidencia.setIdIncidencia(db.getIdIncidencia());
      incidencia.setFechaCreacion(db.getFechaCreacion());

      if(incidencia.getEstatus() == IncidenciaEstatus.INACTIVO && db.getEstatus() != IncidenciaEstatus.INACTIVO) {
        incidencia.setFechaBaja(new Date());
      } else {
        incidencia.setFechaBaja(db.getFechaBaja());
      }
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Trata de actualizar una incidencia inexistente");
      return response;
    }

    // TODO: Validar ID
    if (service.update(incidencia)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Elimina incidencia
   * @param oauth
   * @param id
   * @return
   */
  @DeleteMapping("/{id}")
  public RestResponse<Integer> deleteIncidencia(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") Integer id) {
    // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
    final RestResponse<Integer> response = new RestResponse<Integer>();

    if(!permisoService.testPerm(oauth, "SOPORTE", "DELETE")){
      response.setSuccess(false);
      response.setCode("SECURITY");
      response.setMessage("Está intentando acceder a un recurso protegido");
      return response;
    }

    IncidenciaEntity incidencia = service.queryById(id);

    // TODO: Validar ID
    if (incidencia != null && service.delete(incidencia)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("INTERNAL");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Valida incidencia repetida
   * @param response
   * @param incidencia
   * @param idIncidencia
   * @return
   */
  // REQ: Itzia V. - No debe permitirse crear un nombre de incidencia repetido
  private boolean validRepeated(RestResponse response, IncidenciaEntity incidencia, int idIncidencia) {
    // Se obtiene la lista de roles para validar que no existan repetidos
    List<IncidenciaEntity> roles = service.queryAll();

    // Logger si no se pueden obtener los roles
    if(roles == null) {
      logger.error("Error al validar las incidencias repetidas; se asume que no existen.");
      return true;
    }
    
    // Se valida que el nombre no sea repetido
    for(IncidenciaEntity row : roles) {
      if(row.getPregunta().trim().equalsIgnoreCase(incidencia.getPregunta().trim()) && row.getIdIncidencia().intValue() != idIncidencia) {
        response.setSuccess(false);
        response.setCode("REQUEST");
        response.setMessage("Ya existe la incidencia \"" + incidencia.getPregunta() + "\"");
        return false;
      }
    }

    // No existen repetidos
    return true;
  }
}