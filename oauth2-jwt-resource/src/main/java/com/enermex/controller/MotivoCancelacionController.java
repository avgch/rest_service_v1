package com.enermex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.modelo.Cliente;
import com.enermex.service.ClienteService;
import com.enermex.service.MotivoCancelacionService;
import com.enermex.utilerias.RestResponse;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/motivo/cancelacion")
public class MotivoCancelacionController {
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	MotivoCancelacionService motivoCancelacionService;
	
	
	/**
	 * Obtiene motivos de cancelacion
	 * @param oauth
	 * @return
	 */
	@GetMapping("/cliente")
	@PreAuthorize("hasRole('ROLE_CLIENTE')")
	public ResponseEntity<RestResponse> getAll(@RequestHeader(value="Authorization") String oauth){
		
		RestResponse response = new RestResponse();
		
		Cliente cliente;
		
	    cliente = this.clienteService.getClienteFromToken(oauth);
	     
	     if(cliente == null){
	          response.setSuccess(false);
	          response.setCode("FG007");
	          response.setMessage("Intentas acceder a recurso no permitido");
	          return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	        }
		
		try{
			 response.setSuccess(true);
	         response.setCode("FG008");
	         response.setData( motivoCancelacionService.getAllCliente());
	         response.setMessage("Consulta exitosa");
             		

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	
	/**
	 * Obtiene motivos de cancelacion para el despachador
	 * @param oauth
	 * @return
	 */
	@GetMapping("/despachador")
	@PreAuthorize("hasRole('ROLE_DESPACHADOR')")
	public ResponseEntity<RestResponse> getAllDespachador(@RequestHeader(value="Authorization") String oauth){
		
		RestResponse response = new RestResponse();
		
		try{
			 response.setSuccess(true);
	         response.setCode("FG008");
	         response.setData( motivoCancelacionService.getAllDespachador());
	         response.setMessage("Consulta exitosa");
             		

    	 }
    	catch(Exception e) {
    		
    		response.setSuccess(false);
    		response.setCode("500");
    		response.setMessage(e.getMessage());
    		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}

		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
	}
	
	

}
