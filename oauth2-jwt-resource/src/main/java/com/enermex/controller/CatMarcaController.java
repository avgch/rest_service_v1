package com.enermex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enermex.service.CatMarcaService;
import com.enermex.utilerias.RestResponse;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/marcas")
public class CatMarcaController {
	
	
	
	
	@Autowired
	private CatMarcaService catMarcaService;
	
	/**
	 * Obtiene la marca
	 * @return
	 */
	@GetMapping
	public ResponseEntity<RestResponse> getAll(){
		
		RestResponse response = new RestResponse();
		try {
			response.setSuccess(true);
			response.setCode("200");
			response.setMessage("Consulta exitosa");
			response.setData(catMarcaService.getAll());
		}
		catch(Exception e) {
			
			response.setSuccess(false);
			response.setCode("500");
			response.setMessage("Error consulte al admin del sistema: "+e.getMessage());
		
			return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	/**
	 * Obtiene la marca por el id
	 * @param idMarca
	 * @return
	 */
	@GetMapping("idMarca")
	public ResponseEntity<RestResponse> getMarcaById(@RequestParam(name = "idMarca")Integer idMarca){
		
		RestResponse response = new RestResponse();
		try {
			response.setSuccess(true);
			response.setCode("200");
			response.setMessage("Consulta exitosa");
			response.setData(catMarcaService.getMarcaById(idMarca));
		}
		catch(Exception e) {
			
			response.setSuccess(false);
			response.setCode("500");
			response.setMessage("Error consulte al admin del sistema: "+e.getMessage());
		
			return new ResponseEntity<RestResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
		
	}
	
	

}
