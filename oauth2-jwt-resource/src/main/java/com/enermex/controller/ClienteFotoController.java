package com.enermex.controller;

import com.enermex.modelo.Cliente;
import com.enermex.service.ClienteFotoService;
import com.enermex.service.ClienteService;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.Token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controlador de fotografías del cliente
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 25 de febrero de 2020
 * Última modificación: 19 de marzo de 2020
 */
@RestController
@RequestMapping("/api/clientes")
public class ClienteFotoController {
  Logger logger = LoggerFactory.getLogger(ClienteFotoController.class);

  @Autowired
  Token token;

  @Autowired
  private ClienteFotoService fotoService;

  @Autowired
  private ClienteService service;

  /**
   * Actualiza cliente foto
   * @param oauth
   * @param archivo
   * @return
   */
  @PostMapping("/upload/foto")
  public RestResponse<Void> uploadFoto(@RequestHeader(value="Authorization") String oauth, @RequestParam("file") MultipartFile archivo) {
    final RestResponse<Void> response = new RestResponse<Void>();

    Cliente cliente = token.getClienteFromToken(oauth);

    if (cliente != null && !archivo.isEmpty() && fotoService.updateFoto(cliente, archivo)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Actualiza foto por id cliente
   * @param id
   * @param archivo
   * @return
   */
  @PostMapping("{id}/upload/foto")
  public RestResponse<Void> uploadFotoWithId(@PathVariable("id") String id, @RequestParam("file") MultipartFile archivo) {
    final RestResponse<Void> response = new RestResponse<Void>();

    Cliente cliente = service.getClienteById(id);

    if (cliente != null && !archivo.isEmpty() && fotoService.updateFoto(cliente, archivo)) {
      response.setSuccess(true);
    } else {
      response.setSuccess(false);
      response.setCode("REQUEST");
      response.setMessage("Servicio no disponible. Por favor, inténtelo más tarde");
    }

    return response;
  }

  /**
   * Descarga foto del cliente
   * @param oauth
   * @return
   */
  @RequestMapping(value = "/foto", method = { RequestMethod.GET, RequestMethod.POST }, produces = "image/png")
  public ResponseEntity<InputStreamResource> foto(@RequestHeader(value="Authorization") String oauth) {
    Cliente cliente = token.getClienteFromToken(oauth);

    InputStreamResource resource = fotoService.downloadFoto(cliente);

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * Obtiene foto por id cliente
   * @param id
   * @return
   */
  @RequestMapping(value = "{id}/foto", method = { RequestMethod.GET, RequestMethod.POST }, produces = "image/png")
  public ResponseEntity<InputStreamResource> fotos(@PathVariable("id") String id) {
    Cliente cliente = service.getClienteById(id);

    InputStreamResource resource = fotoService.downloadFoto(cliente);

    if(resource != null) {
      return new ResponseEntity<InputStreamResource>(resource, HttpStatus.OK);
    } else {
      return new ResponseEntity<InputStreamResource>(HttpStatus.FORBIDDEN);
    }
  }
}