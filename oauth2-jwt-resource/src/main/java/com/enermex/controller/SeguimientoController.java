package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import com.enermex.dto.seguimiento.FiltroWeb;
import com.enermex.dto.seguimiento.PedidoWeb;
import com.enermex.modelo.SeguimientoPedidoEntity;
import com.enermex.modelo.SeguimientoPedidoEntity.SRutaEntity;
import com.enermex.service.PermisoService;
import com.enermex.service.SeguimientoService;
import com.enermex.utilerias.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SeguimientoController
 */
@RestController
@RequestMapping("/api/seguimiento")
public class SeguimientoController {
  @Autowired
  private SeguimientoService service;
  
  @Autowired
  private PermisoService permisoService;

  /**
   * Obtiene pedido por id
   * @param oauth
   * @param id
   * @return
   */
  @GetMapping("/{id}")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<PedidoWeb> getPedido(@RequestHeader(value="Authorization") String oauth, @PathVariable("id") BigInteger id) {
    final RestResponse<PedidoWeb> response = new RestResponse<PedidoWeb>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "SEGUIMIENTO", "ACCESS")) {
      return response;
    }

    SeguimientoPedidoEntity pedido = service.queryById(id);

    // Preparación de la respuesta
    response.setData(service.mapWeb(pedido));
    response.setSuccess(pedido != null);
    response.setMessage(pedido != null ? null : "No existe el pedido solicitado");

    return response;
  }

  /**
   * Obtiene pedidos
   * @param oauth
   * @param filtro
   * @return
   */
  @PostMapping()
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<PedidoWeb>> getPedidos(@RequestHeader(value="Authorization") String oauth, @RequestBody FiltroWeb filtro) {
    final RestResponse<List<PedidoWeb>> response = new RestResponse<List<PedidoWeb>>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "SEGUIMIENTO", "ACCESS")) {
      return response;
    }

    List<SeguimientoPedidoEntity> pedidos = service.queryPage(filtro.getInicio(), filtro.getFin(), filtro.getRuta(), filtro.getEstatus(), PageRequest.of(filtro.getPagina(), 10, Sort.by("fechaPedido").ascending()));

    // Preparación de la respuesta
    response.setData(service.mapWeb(pedidos));
    response.setSuccess(pedidos != null);
    response.setMessage(pedidos != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Filtro web
   * @param oauth
   * @param filtro
   * @return
   */
  @PostMapping("count")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<Long> getPedidosCount(@RequestHeader(value="Authorization") String oauth, @RequestBody FiltroWeb filtro) {
    final RestResponse<Long> response = new RestResponse<Long>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "SEGUIMIENTO", "ACCESS")) {
      return response;
    }

    Long cantidad = service.count(filtro.getInicio(), filtro.getFin(), filtro.getRuta(), filtro.getEstatus());

    // Preparación de la respuesta
    response.setData(cantidad);
    response.setSuccess(cantidad != null);
    response.setMessage(cantidad != null ? null : "Servicio no disponible. Por favor, inténtelo más tarde");

    return response;
  }

  /**
   * Obtiene rutas
   * @param oauth
   * @return
   */
  @GetMapping("/rutas")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<SRutaEntity>> getPedido(@RequestHeader(value="Authorization") String oauth) {
    final RestResponse<List<SRutaEntity>> response = new RestResponse<>();
    
    // Se verifican los permisos para este recurso
    if(!permisoService.testPerm(oauth, response, "SEGUIMIENTO", "ACCESS")) {
      return response;
    }

    List<SRutaEntity> rutas = service.queryRutas();

    // Preparación de la respuesta
    response.setData(rutas);
    response.setSuccess(rutas != null);
    response.setMessage(rutas != null ? null : "No es posible obtener las rutas");

    return response;
  }
}