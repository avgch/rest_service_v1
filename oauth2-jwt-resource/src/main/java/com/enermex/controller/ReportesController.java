package com.enermex.controller;

import java.math.BigInteger;
import java.util.List;

import com.enermex.dto.queja.QuejaMovil;
import com.enermex.dto.reporte.ReporteClienteFiltro;
import com.enermex.dto.reporte.ReporteClienteFiltroV2;
import com.enermex.dto.reporte.ReporteClienteLinea;
import com.enermex.dto.reporte.ReporteClienteLineaV2;
import com.enermex.dto.reporte.ReporteDespachador;
import com.enermex.dto.reporte.ReporteDespachadorPromedio;
import com.enermex.dto.reporte.ReporteServicio;
import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.QuejaEntity;
import com.enermex.modelo.RutaEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.service.QuejaService;
import com.enermex.service.ReporteClienteService;
import com.enermex.service.ReporteDespachadorService;
import com.enermex.service.ReportesDatosService;
import com.enermex.service.RutaService;
import com.enermex.service.ServicioReporteService;
import com.enermex.service.UsuarioService;
import com.enermex.utilerias.RestResponse;
import com.enermex.view.ClienteNombreView;
import com.enermex.view.CodigoPostalUsoView;
import com.enermex.view.DespachadorServicioView;
import com.enermex.view.ServicioReporteView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abraham
 */
@RestController
@RequestMapping("/api/reportes")
public class ReportesController {

  @Autowired
  ReportesDatosService datosService;
  
  @Autowired
  UsuarioService usuarioService;
  
  @Autowired
  RutaService rutaService;

  @Autowired
  ServicioReporteService servicioService;

  @Autowired
  ReporteDespachadorService despachadorService;

  @Autowired
  QuejaService quejaService;

  @Autowired
  ReporteClienteService clienteService;
  
  /**
   * Obtiene clientes clientes
   * @param cliente
   * @return
   */
  @PostMapping("/clientes")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<ClienteNombreView>> getCliente(@RequestBody ClienteNombreView cliente) {
    final RestResponse<List<ClienteNombreView>> response = new RestResponse<>();
    final List<ClienteNombreView> clientes = datosService.queryCliente(cliente.getNombreCompletoUpper());
    response.setSuccess(true);
    response.setData(clientes);
    return response;
  }
  /**
   * Regresa lista de clientes (individual)
   * @param cliente
   * @return
   */
  @PostMapping("/clientes/individual")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<ClienteNombreView>> getClienteIndividual(@RequestBody ClienteNombreView cliente) {
    final RestResponse<List<ClienteNombreView>> response = new RestResponse<>();
    final List<ClienteNombreView> clientes = datosService.queryClientePlanIndividual(cliente.getNombreCompletoUpper());
    response.setSuccess(true);
    response.setData(clientes);
    return response;
  }
  /**
   * Regresa lista de clientes (familiar)
   * @param cliente
   * @return
   */
  @PostMapping("/clientes/familiar")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<ClienteNombreView>> getClienteFamiliar(@RequestBody ClienteNombreView cliente) {
    final RestResponse<List<ClienteNombreView>> response = new RestResponse<>();
    final List<ClienteNombreView> clientes = datosService.queryClientePlanFamiliar(cliente.getNombreCompletoUpper());
    response.setSuccess(true);
    response.setData(clientes);
    return response;
  }

  /**
   * Regresa lista de clientes empresarial
   * @param cliente
   * @return
   */
  @PostMapping("/clientes/empresarial")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<ClienteNombreView>> getClienteEmpresarial(@RequestBody ClienteNombreView cliente) {
    final RestResponse<List<ClienteNombreView>> response = new RestResponse<>();
    final List<ClienteNombreView> clientes = datosService.queryClientePlanEmpresarial(cliente.getNombreCompletoUpper());
    response.setSuccess(true);
    response.setData(clientes);
    return response;
  }

  /**
   * Obtiene autos cliente
   * @param cliente
   * @return
   */
  @PostMapping("/clientes/autos")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<AutomovilCliente>> getClienteAutos(@RequestBody ClienteNombreView cliente) {
    final RestResponse<List<AutomovilCliente>> response = new RestResponse<>();
    final List<AutomovilCliente> autos = datosService.queryAutos(cliente.getIdCliente());
    response.setSuccess(true);
    response.setData(autos);
    return response;
  }

  /**
   * Obtiene autos
   * @param auto
   * @return
   */
  @PostMapping("/autos")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<AutomovilCliente>> getClienteAutos(@RequestBody AutomovilCliente auto) {
    final RestResponse<List<AutomovilCliente>> response = new RestResponse<>();
    final List<AutomovilCliente> autos = datosService.queryAutos(auto.getPlacas());
    response.setSuccess(true);
    response.setData(autos);
    return response;
  }

  /**
   * Obtiene despachadores
   * @return
   */
  @PostMapping("/despachadores")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<UsuarioEntity>> getDespachadores() {
    final RestResponse<List<UsuarioEntity>> response = new RestResponse<>();
    final List<UsuarioEntity> despachadores = usuarioService.queryDespachadores();
    response.setSuccess(true);
    response.setData(despachadores);
    return response;
  }

  /**
   * Obtiene calificacion promedio
   * @param idDespachador
   * @return
   */
  @PostMapping("/despachadores/{idDespachador}/promedio")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<ReporteDespachadorPromedio> getDespachadoresCalificaciones(@PathVariable("idDespachador") long idDespachador) {
    final RestResponse<ReporteDespachadorPromedio> response = new RestResponse<>();
    
    response.setSuccess(true);
    response.setData(datosService.promedioDespachador(idDespachador));
    return response;
  }

  /**
   * Guarda rutas
   * @return
   */
  @PostMapping("/rutas")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<RutaEntity>> getRutas() {
    final RestResponse<List<RutaEntity>> response = new RestResponse<>();
    final List<RutaEntity> rutas = rutaService.queryAll();
    response.setSuccess(true);
    response.setData(rutas);
    return response;
  }

  /**
   * Regresa codigo postal
   * @return
   */
  @PostMapping("/codigos")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<CodigoPostalUsoView>> getCodigos() {
    final RestResponse<List<CodigoPostalUsoView>> response = new RestResponse<>();
    final List<CodigoPostalUsoView> codigos = datosService.queryCodigos();
    response.setSuccess(true);
    response.setData(codigos);
    return response;
  }

  ///// REPORTES

  /**
   * Regresa ServicioReporteView
   * @param filtro
   * @return
   */
  @PostMapping("/servicio")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<ServicioReporteView>> getReporteServicio(@RequestBody ReporteServicio filtro) {
    final RestResponse<List<ServicioReporteView>> response = new RestResponse<>();
    final List<ServicioReporteView> pedidos = servicioService.queryByFilter(filtro);

    response.setSuccess(true);
    response.setData(pedidos);
    return response;
  }

  /**
   * Regresa ServicioReporteView (despachador)
   * @param filtro
   * @return
   */
  @PostMapping("/despachador")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<ServicioReporteView>> getReporteDespachador(@RequestBody ReporteDespachador filtro) {
    final RestResponse<List<ServicioReporteView>> response = new RestResponse<>();
    final List<ServicioReporteView> pedidos = despachadorService.queryByFilter(filtro);

    response.setSuccess(true);
    response.setData(pedidos);
    return response;
  }

  /**
   * Regresa ReporteClienteLineaV2
   * @param filtro
   * @return
   */
  @PostMapping("/cliente")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<ReporteClienteLineaV2>> getReporteClientes(@RequestBody ReporteClienteFiltroV2 filtro) {
    final RestResponse<List<ReporteClienteLineaV2>> response = new RestResponse<>();
    final List<ReporteClienteLineaV2> pedidos = clienteService.queryByFilterV2(filtro);

    response.setSuccess(true);
    response.setData(pedidos);
    return response;
  }

  /**
   * Obtiene QuejaEntity
   * @param idPedido
   * @return
   */
  @PostMapping("/quejas/{idPedido}")
  @PreAuthorize("hasRole('ROLE_WEB')")
  public RestResponse<List<QuejaMovil>> getReporteDespachador(@PathVariable("idPedido") BigInteger idPedido) {
    final RestResponse<List<QuejaMovil>> response = new RestResponse<>();
    final List<QuejaEntity> quejas = quejaService.queryByPedido(idPedido);

    response.setSuccess(true);
    response.setData(quejaService.mapMovil(quejas));
    return response;
  }


}