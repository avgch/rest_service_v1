package com.enermex.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.dto.PipaDto;
import com.enermex.modelo.CatCamara;
import com.enermex.modelo.PipaCamara;
import com.enermex.repository.CatCamaraRepository;
import com.enermex.service.PipaCamaraService;

/**
 *
 * @author abraham
 */
@Service
public class CatCamaraService {

	@Autowired
	CatCamaraRepository catCamaraRepository;
	
	@Autowired
	PipaCamaraService pipaCamaraService;
	
	/**
	 * 
	 * @param camara
	 */
	public void create(CatCamara camara) {
		
		camara.setFechaCreacion(new Date());
		catCamaraRepository.save(camara);
	}
	
    /**
     *
     * @return
     */
    public List<CatCamara> camarasDisponibles(){
		
	   List<CatCamara> removeList = new ArrayList<CatCamara>();
		//camaras que nos estan asignadas y no estan con estatus inactivo
		List<CatCamara> camarasActivasSinAsignacion = catCamaraRepository.camarasActivas();
		//camaras asignadas
		List<PipaCamara> camarasAsigandas = this.pipaCamaraService.camarasAsigandas();
		
		for(CatCamara c: camarasActivasSinAsignacion) {
			
			for(PipaCamara pc : camarasAsigandas) {
				
				if(Integer.compare(c.getIdCamara(), pc.getPipaCamaraKey().getIdCamara()) ==0)
					removeList.add(c);
			}
			
			
		}
		
		camarasActivasSinAsignacion.removeAll(removeList);
		
		return camarasActivasSinAsignacion;
		
		
		
	}
	
    /**
     *
     * @return
     */
    public List<CatCamara> camarasExistentes(){
		
	     return this.catCamaraRepository.getCamaras();
			
			
		}
		
    /**
     *
     * @param id
     * @return
     */
    public CatCamara findPipaById(Integer id) {
		
		return catCamaraRepository.findCamaraById(id);
		
	}

    /**
     *
     * @param camara
     */
    public void updateCamara(CatCamara camara) {
		
		catCamaraRepository.updateCamara(camara.getNombre(), camara.getNumSerie(), camara.getModelo(), camara.getEstatus(),camara.getMotivoBaja(),camara.getIdPipa(), camara.getIdCamara());
		
	}
    
    /**
     *
     * @param id
     * @return
     */
    public List<CatCamara> getAllForSelected(Integer id){
		
		//Busco todoas las camaras asignadas a esta pipa
		List<CatCamara> lista = catCamaraRepository.findAll();
		//camaras que tiene asignada la pipa
		List<PipaCamara> pipaCamara = this.pipaCamaraService.getSelectedCams(id);
		
		for(CatCamara c: lista) {
			
			for(PipaCamara pc :pipaCamara) {
				
				if(pc.getPipaCamaraKey().getIdCamara()==c.getIdCamara())
					c.setSelected(true);
			}
			
			
		}
		
		//elimino las camaras que ya tienen una asignacion auna pipa diferente a esta
		//camaras asignadas
		
		 List<CatCamara> removeList = new ArrayList<CatCamara>();
		 List<PipaCamara> camarasAsigandasOtrasPipas = this.pipaCamaraService.camarasAsigandasOtrasPipas(id);
				
				for(CatCamara c: lista) {
					
					for(PipaCamara cap : camarasAsigandasOtrasPipas) {
						
						if(c.getIdCamara()==cap.getPipaCamaraKey().getIdCamara())
							removeList.add(c);
					}
					
					
				}
				
				lista.removeAll(removeList);
		
		
		
		return lista;
	}
	
    /**
     *
     * @param estatus
     * @param nombrePipa
     * @param idCamara
     */
    public void updateStatusCamara(String estatus,String nombrePipa,Integer idCamara) {
		
		
		this.catCamaraRepository.updateCamaraStatus(estatus,nombrePipa, idCamara);
	}
	
	

}
