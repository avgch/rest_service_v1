package com.enermex.service;

import java.util.List;

import com.enermex.modelo.CatColorEntity;
import com.enermex.repository.CatColorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author abraham
 */
@Service
public class CatColorService {
  @Autowired
  private CatColorRepository repo;

    /**
     *
     * @return
     */
    public List<CatColorEntity> queryAll() {
    try {
      return repo.findByOrderByOrdenAsc();
    } catch (Exception e) {
      e.printStackTrace();
      //TODO: handle exception

      return null;
    }
  }
}