package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.modelo.CarSpecificationValue;
import com.enermex.repository.CarSpecificationValueRepository;

/**
 *
 * @author abraham
 */
@Service
public class CarSpecificationValueService {

	@Autowired
	CarSpecificationValueRepository carSpecificationValueRepository;
	
    /**
     *
     * @param idTrim
     * @return
     */
    public List<CarSpecificationValue> getSpecificationValue(Integer idTrim){
		boolean hasCapacity=false;
		
		List<CarSpecificationValue> specifications = carSpecificationValueRepository.getSpecificationValueByTrimAndSpecification(idTrim);
		for(CarSpecificationValue cs: specifications) {
			
			if(cs.getIdCarSpecification().compareTo(new Integer("35"))==0){
				hasCapacity = true;
			}
		}
		
//		if(hasCapacity==false) {
//			CarSpecificationValue tamTanque = new CarSpecificationValue();
//			tamTanque.setIdCarSpecification(new Integer("1"));
//			tamTanque.setIdCarTrim(new Integer("2"));
//			tamTanque.setIdCarSpecificationValue(new Integer("1"));
//			tamTanque.setValue("35");
//			
//			specifications.add(tamTanque);
//		}
		
		
		
		return specifications;
	} 
	
}
