package com.enermex.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.MotivoCancelacionRepository;
import com.enermex.modelo.CatMotivoCancelacion;

/**
 *
 * @author abraham
 */
@Service
public class MotivoCancelacionService {
	
	@Autowired
	MotivoCancelacionRepository motivoCancelacionRepository;
	
    /**
     *
     * @return
     */
    public List<CatMotivoCancelacion> getAllCliente(){
		
		return motivoCancelacionRepository.getAllCliente();
	}
	
    /**
     *
     * @return
     */
    public List<CatMotivoCancelacion> getAllDespachador(){
		
		return motivoCancelacionRepository.getAllDespachador();
	}

}
