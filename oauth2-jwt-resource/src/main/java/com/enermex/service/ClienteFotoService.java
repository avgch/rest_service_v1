package com.enermex.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.UUID;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.enermex.config.AzureConfiguration;
import com.enermex.modelo.AzureBsEntity;
import com.enermex.modelo.Cliente;
import com.enermex.repository.AzureBsRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * ClienteFotoService
 */
@Service
public class ClienteFotoService {
  Logger logger = LoggerFactory.getLogger(ClienteFotoService.class);


  @Autowired
  private AzureBsService azureService;

    /**
     *
     * @param cliente
     * @param file
     * @return
     */
    @Transactional
  public boolean updateFoto(Cliente cliente, MultipartFile file) {
    try {
      // Se calcula un nombre único de la imagen
      String nombreArchivo = UUID.randomUUID().toString() + "_" +  file.getOriginalFilename().replace(" ", "");

      // Se obtiene el BlobContainer de Azure
      BlobContainerClient blobContainerClient = azureContainer();

      // Se verifica que la imagen anterior exista; siendo así, se borra
      if(cliente.getFotoUri() != null && !cliente.getFotoUri().isEmpty()) {
        BlockBlobClient fotoActual = blobContainerClient.getBlobClient(cliente.getFotoUri()).getBlockBlobClient();
        
        if(fotoActual.exists()) {
          fotoActual.delete();
        }
      }

      // Se carga como bloque, es indistinto para imágenes tan pequeñas
      // https://docs.microsoft.com/es-mx/rest/api/storageservices/understanding-block-blobs--append-blobs--and-page-blobs
      BlockBlobClient foto = blobContainerClient.getBlobClient(nombreArchivo).getBlockBlobClient();
      foto.upload(file.getInputStream(), file.getSize());
      cliente.setFotoUri(nombreArchivo);

      return true;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param usuario
     * @return
     */
    public InputStreamResource downloadFoto(Cliente usuario) {
    try {
      BlobContainerClient blobContainerClient = azureContainer();

      // Flujo si la imagen ya había sido cargada
      if (usuario != null && usuario.getFotoUri() != null && !usuario.getFotoUri().isEmpty()){
        ByteArrayInputStream foto = azureInputStream(blobContainerClient, usuario.getFotoUri());

        if(foto != null) {
          return new InputStreamResource(foto);
        }
      }

      // Flujo si la imagen no ha sido cargada o fue eliminada
      ByteArrayInputStream fotoNull = azureInputStream(blobContainerClient, "null.png");

      if(fotoNull != null) {
        return new InputStreamResource(fotoNull);
      }

      // Si no se encuentra ninguna imagen
      return null;
    } catch (Exception e) {
      logger.error("Error al descargar la imagen: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  private BlobContainerClient azureContainer() {
    // Consulta de credenciales de Azure
    AzureBsEntity azureEntity = azureService.queryById(1);

    // Se crea un cliente del Blob Service de Azure
    BlobServiceClient blobServiceClient = new BlobServiceClientBuilder()
      .endpoint(azureEntity.getEndpoint())
      .sasToken(azureEntity.getSas())
      .buildClient();
    
    // Se crea un cliente para el contenedor
    BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(azureEntity.getContainer());
    return blobContainerClient;
  }

  private ByteArrayInputStream azureInputStream(BlobContainerClient blobContainerClient, String blobName) {
    BlockBlobClient fotoActual = blobContainerClient.getBlobClient(blobName).getBlockBlobClient();
        
    if(fotoActual.exists()) {
      // A ByteArrayOutputStream holds the content in memory
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      fotoActual.download(outputStream);
      
      // To convert it to a byte[] - simply use
      final byte[] bytes = outputStream.toByteArray();

      // To convert bytes to an InputStream, use a ByteArrayInputStream
      ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
      
      return inputStream;
    } else {
      return null;
    }
  }
  
}