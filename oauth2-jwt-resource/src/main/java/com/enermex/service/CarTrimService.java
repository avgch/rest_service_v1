package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.modelo.CarTrim;
import com.enermex.repository.CarTrimRepository;

/**
 *
 * @author abraham
 */
@Service
public class CarTrimService {

	@Autowired
	CarTrimRepository carTrimRepository;
	
	/**
	 * 
	 * @param idModelo
	 * @param idSerie
	 * @return
	 */
	public List<CarTrim> getTrim(Integer idModelo,Integer idSerie){
		
		
		return carTrimRepository.getTrimByModeloAndSerie(idModelo, idSerie);
	}
}
