package com.enermex.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import com.enermex.dto.reporte.ReporteDespachadorPromedio;
import com.enermex.modelo.AutomovilCliente;
import com.enermex.repository.AutomovilClienteRepository;
import com.enermex.repository.ClienteNombreRepository;
import com.enermex.repository.CodigoPostalUsoRepository;
import com.enermex.repository.DespachadorServicioRepository;
import com.enermex.repository.TiempoServicioRepository;
import com.enermex.repository.TiempoTrasladoRepository;
import com.enermex.view.ClienteNombreView;
import com.enermex.view.CodigoPostalUsoView;
import com.enermex.view.DespachadorServicioView;
import com.enermex.view.TiempoServicioView;
import com.enermex.view.TiempoTrasladoView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author abraham
 */
@Service
public class ReportesDatosService {
  @Autowired
  ClienteNombreRepository clienteRepo;
  
  @Autowired
  CodigoPostalUsoRepository codigoRepo;

  @Autowired
  AutomovilClienteRepository autoRepo;

  @Autowired
  DespachadorServicioRepository calificaRepo;

  @Autowired
  TiempoTrasladoRepository tiempoTrasladoRepo;

  @Autowired
  TiempoServicioRepository tiempoServicioRepo;

    /**
     *
     * @param nombre
     * @return
     */
    public List<ClienteNombreView> queryCliente(String nombre) {
    try {
      return clienteRepo.findTop10ByNombreCompletoUpperContaining(nombre);
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param nombre
     * @return
     */
    public List<ClienteNombreView> queryClientePlanIndividual(String nombre) {
    try {
      return clienteRepo.findIndividual(nombre);
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param nombre
     * @return
     */
    public List<ClienteNombreView> queryClientePlanFamiliar(String nombre) {
    try {
      return clienteRepo.findFamiliar(nombre);
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param nombre
     * @return
     */
    public List<ClienteNombreView> queryClientePlanEmpresarial(String nombre) {
    try {
      return null;
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @return
     */
    public List<CodigoPostalUsoView> queryCodigos() {
    try {
      return codigoRepo.findAll();
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param idCliente
     * @return
     */
    public List<AutomovilCliente> queryAutos(BigInteger idCliente) {
    try {
      return autoRepo.findByIdCliente(idCliente);
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param placas
     * @return
     */
    public List<AutomovilCliente> queryAutos(String placas) {
    try {
      return autoRepo.findTop5ByPlacasStartingWith(placas);
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param idDespachador
     * @return
     */
    public ReporteDespachadorPromedio promedioDespachador(long idDespachador) {
    ReporteDespachadorPromedio promedios = new ReporteDespachadorPromedio();

    try {
      Optional<DespachadorServicioView> calificacion = calificaRepo.findById(idDespachador);
      promedios.setCantidadPedidos(calificacion.isPresent() ? calificacion.get().getPedidos() : null);
      promedios.setPromedioCalificacion(calificacion.isPresent() ? calificacion.get().getPromedio() : null);

      Optional<TiempoTrasladoView> traslado = tiempoTrasladoRepo.findById(idDespachador);
      promedios.setPromedioTraslado(traslado.isPresent() ? traslado.get().getPromedio() : null);

      Optional<TiempoServicioView> servicio = tiempoServicioRepo.findById(idDespachador);
      promedios.setPromedioServicio(servicio.isPresent() ? servicio.get().getPromedio() : null);

    } catch (Exception e) {
      e.printStackTrace();
      // TODO: logger

    }

    return promedios;
  }
}