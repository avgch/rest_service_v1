package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.ProductoServicioRepository;
import com.enermex.modelo.ProductoServicio;

/**
 *
 * @author abraham
 */
@Service
public class ProductoServicioService {
	
	@Autowired
	ProductoServicioRepository productoServicioRepository;
	
    /**
     *
     * @return
     */
    public List<ProductoServicio> getAllProdSer(){
		
		
		return productoServicioRepository.getAll();
	}
	

}
