package com.enermex.service;

import java.util.List;

import com.enermex.modelo.CatUsoCfdiEntity;
import com.enermex.repository.CatUsoCfdiRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * CatUsoCfdiService
 */
@Service
public class CatUsoCfdiService {
  @Autowired
  private CatUsoCfdiRepository repo;

    /**
     *
     * @return
     */
    public List<CatUsoCfdiEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      return null;
      //TODO: handle exception
    }
  }
}