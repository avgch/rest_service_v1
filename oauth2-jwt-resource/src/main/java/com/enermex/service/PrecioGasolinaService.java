package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.dto.CoberturaDto;
import com.enermex.dto.EdoMunDto;
import com.enermex.dto.PrecioGasolinaRequestDto;
import com.enermex.enumerable.TipoCombustible;
import com.enermex.enumerable.TipoCombustiblePetroIntelligenceEnum;
import com.enermex.modelo.PrecioGasolina;
import com.enermex.repository.PrecioGasolinaRepository;
import com.enermex.repository.impl.AlcaldiaEstadoRepositoryImpl;
import com.enermex.repository.impl.PrecioGasolinaRepositoryImpl;

/**
 *
 * @author abraham
 */
@Service
public class PrecioGasolinaService {
	
	@Autowired
	PrecioGasolinaRepository precioGasolinaRepository;
	
	@Autowired
	AlcaldiaEstadoRepositoryImpl alcaldiaEstadoRepositoryImpl;
	
	@Autowired
	PrecioGasolinaRepositoryImpl precioGasolinaRepositoryImpl;
	
    /**
     *
     * @param lista
     */
    public void saveMany(List<PrecioGasolina> lista) {
		
		precioGasolinaRepository.saveAll(lista);
	}
	/**
	 * elimina registros
	 */
	public void deleteAll() {
		
		//precioGasolinaRepository.deleteAll();
		precioGasolinaRepositoryImpl.deletePreciosGasolina();
	}
	/**
	 * Obtiene el municipio y estado
	 * @param cp
	 * @return
	 */
	public PrecioGasolinaRequestDto getAlcEdoByCp(String cp) {
		
		return alcaldiaEstadoRepositoryImpl.getAlcEdoByCp(cp);
	}
	
	/**
	 * 
     * @param idEstado
     * @param idMunicipio
	 * @param idAlcMun
     * @param idCombustible
     * @return 
	 */
	public Double getPrecioPromAlcMun(Integer idEstado,Integer idMunicipio, Integer idCombustible) {
		//PETROINLLINGENCE  
		//1. Regular
		//2. Premium
		//3. Diesel
		Integer combustible=null;
		
		//Regular
		if(idCombustible.compareTo(TipoCombustible.REGULAR.getCombustible())==0) {
			combustible = new Integer(TipoCombustiblePetroIntelligenceEnum.REGULAR.getCombustible());	
		}
		//Premium
		if(idCombustible.compareTo(TipoCombustible.PREMIUM.getCombustible())==0) {
			combustible = new Integer(TipoCombustiblePetroIntelligenceEnum.PREMIUM.getCombustible());
		}
       //Diesel
		if(idCombustible.compareTo(TipoCombustible.DIESEL.getCombustible())==0) {
			combustible = new Integer(TipoCombustiblePetroIntelligenceEnum.DIESEL.getCombustible());
		}
		
		Double promedio = precioGasolinaRepositoryImpl.getPrecioPromedio(idEstado, idMunicipio, combustible);
		return promedio;
	}
	
	/**
	 * Crea tabla de precio de combustibles 
	 */
	public void createTablePreciosCombustiblesServerSide() {
		
		
		//primero eliminamos la tabla existente
		precioGasolinaRepositoryImpl.createAndDropTablePreciosCombustiblesServerSide();
	}
	
	/**
	 * Crea tabla de precio de combustibles 
	 */
	public void dropTablePreciosCombustiblesServerSide() {
		
		
		//primero eliminamos la tabla existente
		precioGasolinaRepositoryImpl.dropTablePreciosCombustiblesServerSide();
	}
	/**
	 * Obtiene los estados y municipios de acuerdo a la cobertura de las rutas para
	 * cargar solo esa info
     * @return 
	 */
	public List<EdoMunDto> getCoberturaPorEdoMunc() {
		
		List<EdoMunDto> cobertura = precioGasolinaRepositoryImpl.getCoberturaEdoMun();
		
		return cobertura;
		
	}
	
	
	

}
