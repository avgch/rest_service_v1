package com.enermex.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.enermex.dto.website.Contacto;
import com.enermex.dto.website.WebsiteDto;
import com.enermex.enumerable.RutaEstatus;
import com.enermex.modelo.AppStore;
import com.enermex.modelo.EstadoEntity;
import com.enermex.modelo.FaqEntity;
import com.enermex.modelo.MunicipioEntity;
import com.enermex.modelo.RutaCodigoEntity;
import com.enermex.modelo.RutaEntity;
import com.enermex.modelo.WebsiteEntity;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.EstadoRepository;
import com.enermex.repository.WebsiteRepository;
import com.enermex.utilerias.SmtpGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class WebsiteService {
  @Autowired
  WebsiteRepository repo;

  @Autowired
  FaqService faqService;

  @Autowired
  RutaService rutaService;

  @Autowired
  EstadoRepository estadoRepo;

  @Autowired
  ClienteRepository appstoreRepo;

  @Autowired
  SmtpGenerico smtp;

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public WebsiteDto getWebsite () {
    try {
      Optional<WebsiteEntity> optional = repo.findById(1);

      if(optional.isPresent()) {
        WebsiteDto dto = WebsiteDto.fromJsonString(optional.get().getContent());

        // Appstore
        List<AppStore> apps = appstoreRepo.getAppStorePaths();
        for(AppStore app : apps) {
          if(app.getAppTecnologia().equals("ios")) {
            dto.setIosUrl("https://www.apple.com/mx/ios/app-store/");
          } else if(app.getAppTecnologia().equals("android")) {
            dto.setAndroidUrl("https://play.google.com/store");
          }
        }

        // Faqs
        dto.setFaqs(new ArrayList<>());
        List<FaqEntity> faqs = faqService.queryActivos();

        if(faqs != null) {
          for(FaqEntity faq : faqs) {
            WebsiteDto.Faq faqDto = new WebsiteDto.Faq();
            faqDto.setPregunta(faq.getPregunta());
            faqDto.setRespuesta(faq.getRespuesta());

            dto.getFaqs().add(faqDto);
          }
        }

        // Rutas
        dto.setRutas(new ArrayList<>());
        List<RutaEntity> rutas = rutaService.queryAll();

        // Consulta de los nombres de estados y municipios
        List<EstadoEntity> estados = estadoRepo.findAll();

        if(rutas != null) {
          for (RutaEntity ruta : rutas) {
            if(ruta.getEstatus() == RutaEstatus.ACTIVO) {
              WebsiteDto.Ruta rutaDto = new WebsiteDto.Ruta();
              rutaDto.setNombre(ruta.getNombre());
              rutaDto.setLat(ruta.getLat());
              rutaDto.setLng(ruta.getLng());

              rutaDto.setCobertura(new ArrayList<>());

              for(RutaCodigoEntity codigo : ruta.getCodigos()) {
                WebsiteDto.Cobertura cobertura = null;

                for(WebsiteDto.Cobertura coberturaEnRuta: rutaDto.getCobertura()) {
                  if(coberturaEnRuta.idEstado == codigo.getIdEstado().intValue() && coberturaEnRuta.idMunicipio == codigo.getIdMunicipio().intValue()) {
                    cobertura = coberturaEnRuta;
                    break;
                  }
                }

                if(cobertura == null) {
                  cobertura = new WebsiteDto.Cobertura();

                  cobertura.setCps(new ArrayList<>());
                  cobertura.idEstado = codigo.getIdEstado();
                  cobertura.idMunicipio = codigo.getIdMunicipio();

                  for(EstadoEntity estado : estados) {
                    if(cobertura.idEstado == estado.getIdEstado().intValue()) {
                      cobertura.setEstado(estado.getNombre());
                      
                      for(MunicipioEntity municipio : estado.getMunicipios()) {
                        if(cobertura.idMunicipio == municipio.getIdMunicipio()) {
                          cobertura.setMunicipio(municipio.getNombre());
                          break;
                        }
                      }

                      break;
                    }
                  }

                  rutaDto.getCobertura().add(cobertura);
                }

                cobertura.getCps().add(codigo.getIdCodigo());
              }
              
              dto.getRutas().add(rutaDto);
            }
          }
        }

        return dto;
      } else {
        return null;
      }
    } catch(Exception e) {
      e.printStackTrace();

      return null;
    }
  }

    /**
     *
     * @param website
     * @return
     */
    @Transactional
  public boolean saveWebsite (WebsiteDto website) {
    try {
      Optional<WebsiteEntity> optional = repo.findById(1);

      if(optional.isPresent()) {
        WebsiteEntity entity = optional.get();
        entity.setContent(WebsiteDto.toJsonString(website));
        repo.save(entity);

        return true;
      } else {
        return false;
      }
    } catch(Exception e) {
      e.printStackTrace();

      return false;
    }
  }

    /**
     *
     * @param contacto
     * @return
     */
    @Transactional
  public boolean formContacto(Contacto contacto) {
    try {
      // Preparación del correo para administrador
      ArrayList<SmtpGenerico.BasicTabulator> info = new ArrayList<>();
      info.add(new SmtpGenerico.BasicTabulator("Nombre:", contacto.getNombre() + smtp.br() ));
      info.add(new SmtpGenerico.BasicTabulator("Correo:", contacto.getCorreo() + smtp.br() ));
      info.add(new SmtpGenerico.BasicTabulator("Mensaje:", contacto.getMensaje()));

      String contenido = smtp.lines("Información del formulario", info);

      // Búsqueda de los correos registrados en el administrador:
      final WebsiteDto website = getWebsite();

      if(website != null && website.getContacto() != null) {
        // Consultamos los correos de contacto
        String[] correos = website.getContacto().split(";");

        for(String correo : correos) {
          smtp.sendEmail("FASTGAS CONTACTO", correo.trim(), "Se ha utilizado el formulario de contacto del sitio informativo.", contenido);
        }
      }

      // Preparación del correo para cliente
      String contenidoContacto = smtp.join("A la brevedad nos comunicaremos contigo para atender tu solicitud", "Mensaje recibido:" + smtp.br() + contacto.getMensaje());
      smtp.sendEmail("FASTGAS CONTACTO", contacto.getCorreo().trim(), "¡Gracias por contactarnos!", contenidoContacto);

      return true;
    } catch (Exception e) {
      e.printStackTrace();
      //TODO: handle exception

      return false;
    }
  }
}