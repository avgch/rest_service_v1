package com.enermex.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.enermex.dto.RolDto;
import com.enermex.dto.UsuarioDto;
import com.enermex.enumerable.EnumRoles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.DireccionRepository;
import com.enermex.repository.UsuarioRepositoryA;
import com.enermex.repository.UsuarioRolesRepository;
import com.enermex.repository.RolRepositoryA;
import com.enermex.modelo.DireccionCliente;
import com.enermex.modelo.Usuario;
import com.enermex.modelo.UsuarioRoles;
import com.enermex.modelo.UsuarioRolesKey;
import com.enermex.modelo.UsuarioTipoPlan;
import com.enermex.modelo.UsuarioTipoPlanKey;
import com.enermex.repository.UsuarioTipoPlanRepository;

/**
 *
 * @author abraham
 */
@Service
public class UsuarioServiceA {

	@Autowired
	private UsuarioRepositoryA usuarioRepository;
		
	
	private UsuarioRoles usuarioRoles;
	
	@Autowired
	private UsuarioRolesRepository usuarioRolesRp;
	
	@Autowired
	private DireccionRepository direccionRepository;
	
	@Autowired
	private RolRepositoryA rolRepository;
	
	@Autowired
	private UsuarioTipoPlanRepository usrTipoPlanRepository;
	

	
	
	/**
	 * Obtiene las lista de usuarios con sus roles y direcciones
	 * @return
	 */
	public List<UsuarioDto> getAll()  {
		
		// TODO Auto-generated method stub
      List<Usuario> usuarios = usuarioRepository.findAll();
     
      
//      for(UsuarioDto u: usuarios) {
//    	  
//    	      
//    	 for (DireccionCliente d:u.getDireccion())
//    	 {
//    		 System.out.println(d.getColonia());
//    		 d.setUsuario(null);
//    		 
//    		 
//    	 }
//      }
      //List<UsuarioDto> lista =(List<UsuarioDto>) usuarios;
   
	    return null;
	}
	
	/**
	 * Registro de usuario
	 * @param userP
	 */
	public void register (Usuario userP) {
		
		//ROLE_CLIENTE
		userP.setApellidoMaterno("");
		userP.setApellidoPaterno("");
		userP.setEstatus(1);
		userP.setIdTipoUsuario(new Long(3));
		userP.setFechaCreacion(new Date());
		

	
		//limpiamos los planes del objeto user
		userP.setPlanes(null);
		
		//keys
		UsuarioRolesKey     urkey = new UsuarioRolesKey();
		UsuarioTipoPlanKey  utpkey ;
		
		//entities
		UsuarioTipoPlan uTipoPlan;
		UsuarioRoles    ur = new UsuarioRoles();
	
		Usuario user =usuarioRepository.save(userP);
			
		//asignamos isid
		usuarioRepository.updateIsid(user.getIdUsuario(),user.getIdUsuario());
		
		
		Integer id=rolRepository.findByRol(EnumRoles.ROLE_CLIENTE.getName());	
	
		urkey.setIdUsuario(user.getIdUsuario());
		urkey.setIdRol(id);
		
		ur.setId(urkey);
		
		usuarioRolesRp.save(ur);

			
		utpkey = new UsuarioTipoPlanKey();
		uTipoPlan   = new UsuarioTipoPlan();
		 
		//utpkey.setIdTipoPlan(userP.getIdPlan());
		//utpkey.setIdUsuario(userP.getIdUsuario());
		
		uTipoPlan.setId(utpkey);
		
		usrTipoPlanRepository.save(uTipoPlan);
	
		
		
		
	}
	
	
	
	/**
	 * Guarda un usuario con roles y direcciones
	 * @param userP
	 */
	public void save(Usuario userP) {
		
		
		DireccionCliente dir;
		List<Integer> roles = new ArrayList<>();
		List<DireccionCliente> direcciones = new ArrayList<>();
		Usuario userDto;
		
		
		//Sacamos de la estructura json la direccion para persistirla
//		 for(DireccionCliente d: userP.getDireccion()) {
//				
//			 dir = new DireccionCliente(d);
//	    	 direcciones.add(dir);
//			}
			
//		Sacamos de la estructura json del rol
		for(RolDto r : userP.getRoles()) {
			
			roles.add(r.getIdRol());
			
		}
		
		//eliminamos la lista de direcciones para que no trate de insertarla en la tabla rol
		//ya que no es la dueña de la relacion
	     //userP.setDireccion(null);
		//eliminamos la lista de roles para que no trate de insertarla en la tabla rol
		userP.setRoles(null);
		//Asignamos fecha de creacion
		userP.setFechaCreacion(new Date());
		userP.setIdTipoUsuario(new Long(1));
		userDto = usuarioRepository.save(userP);
	
	
		//persistimos direcciones con el usuario
//	     for(DireccionCliente d: direcciones) {
//	    	 d.setUsuario(userDto);
//	    	 d.setFechaCreacion(new Date());;
//	    	 direccionRepository.save(d);
//		}
		
		
		//persistimos roles en la tabla UsuariosRol
		for (Integer idRol :  roles) {
			
			usuarioRoles.getId().setIdUsuario(userDto.getIdUsuario());
			usuarioRoles.getId().setIdRol(idRol);
			usuarioRolesRp.save(usuarioRoles);
			
		}
	}
	
	
	/**
	 * Actualizamos el usuario
	 * @param user
	 * @return
	 */
	public void update(Usuario user) {
	
		
		usuarioRepository.updateUsuario(user.getNombre(),user.getApellidoPaterno(),user.getApellidoMaterno(),user.getEstadoCivil(),user.getCorreo(), user.getTelefono(), user.getIdUsuario());
		
//		for(DireccionCliente d: user.getDireccion()) {
//			
//			
//		//	direccionRepository.updateDireccion(d.getCalle(), d.getCp(), d.getColonia(), d.getLatitud(), d.getLongitud(), d.getIdDireccion());	
//		}
		
	
	}

    /**
     *
     * @param id
     */
    public void delete(Integer id) {
		
		usuarioRepository.delete(id);
	
	}
	
    /**
     *
     * @param email
     * @return
     */
    public Usuario validateEmail(String email) {
		
		return this.usuarioRepository.findByEmail(email);
		
	}
	
	
	
	/**
	 * José Antonio González Hernández
	 * Guardar la contraseña con la implementación de Abraham Vargas
     * @param user
     * @param contrasena
	 */
	public void updateContrasena(Usuario user, String contrasena){
		usuarioRepository.updatePassword(
			contrasena,
			user.getIdUsuario(),
			user.getCorreo());
	}
}
