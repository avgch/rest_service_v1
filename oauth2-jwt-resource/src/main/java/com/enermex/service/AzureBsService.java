package com.enermex.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.enermex.modelo.AzureBsEntity;
import com.enermex.repository.AzureBsRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class AzureBsService {
  Logger logger = LoggerFactory.getLogger(AzureBsService.class);

  @Autowired
	private AzureBsRepository repo;

    /**
     *
     * @param azure
     * @return
     */
    public boolean update(AzureBsEntity azure) {
    if (!exists(azure)) {
      logger.error("Trata de actualizar una cuenta de azure inexistente - ID: " + azure.getIdAzure());

      return false;
    }

    return save(azure);
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public AzureBsEntity queryById(Integer id) {
    try {
      Optional<AzureBsEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una azure: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(AzureBsEntity azure) {
    try {
      return azure != null && azure.getIdAzure() != null && repo.existsById(azure.getIdAzure());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una azure: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public boolean exists(Integer id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una azure: " + e.getMessage());
      return false;
    }
  }

  @Transactional()
  private boolean save(AzureBsEntity azure) {
    try {
      repo.save(azure);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una azure: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}
