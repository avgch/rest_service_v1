package com.enermex.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.enermex.config.AzureConfiguration;
import com.enermex.dto.factura.FacturaMovil;
import com.enermex.dto.factura.FacturaVista;
import com.enermex.enumerable.PedidoFacturaEstatus;
import com.enermex.modelo.AzureBsEntity;
import com.enermex.modelo.CatUsoCfdiEntity;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteFiscalEntity;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.PedidoFacturaEntity;
import com.enermex.repository.CatUsoCfdiRepository;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.FacturaVistaRepository;
import com.enermex.repository.PedidoFacturaRepository;
import com.enermex.repository.PedidoRepository;
import com.enermex.utilerias.FirebaseNotification;
import com.enermex.utilerias.SmtpGenerico;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * PedidoFacturaService
 */
@Service
public class PedidoFacturaService {
  Logger logger = LoggerFactory.getLogger(PedidoFacturaService.class);

  @Autowired
  private PedidoFacturaRepository repo;
  
  @Autowired
  private FacturaVistaRepository vistaRepo;

  @Autowired
  private ClienteFiscalService fiscalService;

  @Autowired
  private CatUsoCfdiRepository usoRepo;

  @Autowired
  private PedidoRepository pedidoRepo;

  @Autowired
  private SmtpGenerico smtp;

  @Autowired
  private FirebaseNotification notification;

  @Autowired
  private ClienteRepository clienteRepo;

    /**
     *
     * @param factura
     * @return
     */
    @Transactional
  public boolean create(PedidoFacturaEntity factura) {
    if (exists(factura)) {
      logger.error("Trata de añadir una factura existente - ID: " + factura.getIdFactura());

      return false;
    }

    factura.setEstatus(PedidoFacturaEstatus.SOLICITADA);
    factura.setFechaCreacion(new Date());

    return save(factura);
  }

    /**
     *
     * @param factura
     * @return
     */
    public boolean update(PedidoFacturaEntity factura) {
    if (!exists(factura)) {
      logger.error("Trata de actualizar una factura inexistente - ID: " + factura.getIdFactura());

      return false;
    }

    return save(factura);
  }

    /**
     *
     * @param factura
     * @return
     */
    public boolean delete(PedidoFacturaEntity factura) {
    if (!exists(factura)) {
      logger.error("Trata de eliminar una factura inexistente - ID: " + factura.getIdFactura());

      return false;
    }

    factura.setEstatus(PedidoFacturaEstatus.INACTIVO);
    return save(factura);
  }

    /**
     *
     * @param factura
     * @return
     */
    public BigInteger getIdCliente(PedidoFacturaEntity factura) {
    try {
      ClienteFiscalEntity datosFiscales = fiscalService.queryById(factura.getIdFiscal());

      return datosFiscales != null ? datosFiscales.getIdCliente() : null;
    } catch (Exception e) {
      // TODO: Logger
      e.printStackTrace();

      return null;
    }
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public List<FacturaVista> queryAll() {
    try {
      return vistaRepo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener facturas: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private List<FacturaVista> querySolicitadas() {
    try {
      return vistaRepo.findByFacturaEstatus(PedidoFacturaEstatus.SOLICITADA);
    } catch (Exception e) {
      logger.error("Error al obtener facturas: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param idCliente
     * @return
     */
    @Transactional(readOnly = true)
  public List<PedidoFacturaEntity> queryAllByIdCliente(BigInteger idCliente) {
    try {
      return repo.findByIdCliente(idCliente);
    } catch (Exception e) {
      logger.error("Error al obtener facturas: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param idPedido
     * @return
     */
    @Transactional(readOnly = true)
  public PedidoFacturaEntity queryByIdPedido(BigInteger idPedido) {
    try {
      return repo.findByIdPedido(idPedido);
    } catch (Exception e) {
      logger.error("Error al obtener facturas: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param idPedido
     * @return
     */
    @Transactional(readOnly = true)
  public boolean existsByIdPedido(BigInteger idPedido) {
    try {
      return repo.existsByIdPedido(idPedido);
    } catch (Exception e) {
      logger.error("Error al obtener facturas: " + e.getMessage());
      return true;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public PedidoFacturaEntity queryById(BigInteger id) {
    try {
      Optional<PedidoFacturaEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una factura: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(PedidoFacturaEntity factura) {
    try {
      return factura != null && factura.getIdFactura() != null && repo.existsById(factura.getIdFactura());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una factura: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public boolean exists(BigInteger id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una factura: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param idFiscal
     * @return
     */
    @Transactional(readOnly = true)
  public boolean existsByIdFiscal(BigInteger idFiscal) {
    try {
      return repo.existsByIdFiscal(idFiscal);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una factura: " + e.getMessage());
      return false;
    }
  }




  



  @Transactional(propagation = Propagation.NESTED)
  private boolean save(PedidoFacturaEntity factura) {
    try {
      repo.save(factura);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una factura: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }


  private void appendNumber(Cell cell, BigDecimal bigDecimal) {
    cell.setCellStyle(numberStyle);
    cell.setCellValue(bigDecimal.doubleValue());
  }


  DecimalFormat df = new DecimalFormat("#,##0.00");

  CellStyle numberStyle;

    /**
     *
     * @return
     */
    @Transactional
  public InputStreamResource excel() {
    // Se consultan las solicitudes de facturas
    List<FacturaVista> facturas = querySolicitadas();

    if(facturas == null) {
      return null;
    }

    final XSSFWorkbook facturaWorkbook = new XSSFWorkbook();
    final XSSFSheet sheet = facturaWorkbook.createSheet("Solicitudes");

    // Se preparan los encabezados
    excelHeader(sheet);

    // Formato numérico
    DataFormat format = facturaWorkbook.createDataFormat();
    numberStyle = facturaWorkbook.createCellStyle();
    numberStyle.setDataFormat(format.getFormat("#,##0.00"));

    
    int fila = 0;
    
    // Se realiza el vaciado del pedido en las filas
    for(FacturaVista factura : facturas) {
      // Paso 0: Verificar el porcentaje del descuento; si es que este aplica.
      BigDecimal descuentoCombustible = null;
      BigDecimal descuentoServicio    = null;
      BigDecimal descuentoLavado      = null;
      BigDecimal descuentoPresion     = null;

      boolean hasDescuento = false;

      try {
        hasDescuento = Double.parseDouble(factura.getDescuento()) > 0;
      } catch (Exception e) {
        //e.printStackTrace();
      }
      
      if(hasDescuento){
        try {
          BigDecimal descuentoPorPeso = new BigDecimal(factura.getDescuento()).divide(new BigDecimal(factura.getTotalsd()), 20, RoundingMode.HALF_UP);
          
          descuentoCombustible = new BigDecimal(factura.getCombustible()).multiply(descuentoPorPeso);
          descuentoServicio    = new BigDecimal(factura.getServicio()   ).multiply(descuentoPorPeso);
          descuentoLavado      = factura.getHasLavado()  ? new BigDecimal(factura.getLavado() ).multiply(descuentoPorPeso) : null;
          descuentoPresion     = factura.getHasPresion() && factura.getPresion() != null ? new BigDecimal(factura.getPresion()).multiply(descuentoPorPeso) : null;
        } catch(Exception e) { 
          e.printStackTrace();
        }
      }
      // Primero, se crea la fila del servicio
      fila += 1;
      Row servicio = sheet.createRow(fila);
      excelGeneral(factura, servicio);
      excelServicio(factura, servicio, descuentoServicio);

      // Se crea la fila del combustible
      fila += 1;
      Row combustible = sheet.createRow(fila);
      excelGeneral(factura, combustible);
      excelCombustible(factura, combustible, descuentoCombustible);

      // Si aplica, se crea la fila de servicios adicionales
      if(factura.getHasLavado() || factura.getHasPresion()) {
        fila += 1;
        Row adicional = sheet.createRow(fila);
        excelGeneral(factura, adicional);

        // Tipo de servicios adicionales: S2 para lavado y S3 para lavado + presion
        String tipoAdicional = factura.getHasPresion() ? "S3" : "S2";
        adicional.createCell(0).setCellValue(tipoAdicional);

        // Precios de servicios adicionales
        BigDecimal precioLavadoNeto  = new BigDecimal(factura.getLavado());
        BigDecimal precioPresionNeto = factura.getHasPresion() && factura.getPresion() != null ? new BigDecimal(factura.getPresion()) : null;
        BigDecimal totalBruto = precioPresionNeto != null ? precioLavadoNeto.add(precioPresionNeto) : precioLavadoNeto;

        appendNumber(adicional.createCell(15), precioLavadoNeto);

        if(precioPresionNeto != null) {
          appendNumber(adicional.createCell(16), precioPresionNeto);
        } else {
          adicional.createCell(16).setCellValue("");
        }
        
        BigDecimal descuentoTotal = null;
        if(descuentoLavado != null) {
          descuentoTotal = descuentoLavado.add(new BigDecimal("0.00"));
          
          totalBruto = totalBruto.subtract(descuentoLavado);
        }
        if(precioPresionNeto != null && descuentoPresion != null) {
          descuentoTotal = descuentoTotal != null ? descuentoTotal.add(descuentoPresion) : descuentoPresion.add(new BigDecimal("0.00"));
          
          totalBruto = totalBruto.subtract(descuentoPresion);
        }
        
        if(descuentoTotal != null) {
          appendNumber(adicional.createCell(17), descuentoTotal);
        }
        
        // Costo del servicio
        BigDecimal totalNeto = totalBruto.divide(new BigDecimal("1.16"), 2, RoundingMode.HALF_UP);
        BigDecimal iva = totalBruto.subtract(totalNeto);

        appendNumber(adicional.createCell(18), totalNeto);
        appendNumber(adicional.createCell(19), iva);
        appendNumber(adicional.createCell(20), totalBruto);
      }

    }

    // Termina la asignación del Excel

    try {
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      facturaWorkbook.write(stream);
      facturaWorkbook.close();
      ByteArrayInputStream inputStream = new ByteArrayInputStream(stream.toByteArray());
      InputStreamResource resource = new InputStreamResource(inputStream);

      // Con el Excel listo, se marcan las facturas
      for(FacturaVista factura : facturas) {
        PedidoFacturaEntity pedidoFactura = queryById(factura.getIdFactura());
        pedidoFactura.setEstatus(PedidoFacturaEstatus.PROCESANDO);
        save(pedidoFactura);
      }

      return resource;
    } catch(Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  private void excelCombustible(FacturaVista factura, Row row, BigDecimal descuento) {
    if(descuento != null) {
      appendNumber(row.createCell(17), descuento);
    }

    // Tipo de cobro
    row.createCell(0).setCellValue("S1");

    // Importe por combustible
    Integer tipoId         = factura.getIdCombustible();
    String tipoCombustible = tipoId == 1 || tipoId == null ? "Diesel" : (tipoId == 2 ? "Premium" : "Regular");
    BigDecimal costoLitro  = new BigDecimal(factura.getPrecioLitro());
    BigDecimal bruto       = new BigDecimal(factura.getCombustible());

    row.createCell(11).setCellValue(tipoCombustible);
    appendNumber(row.createCell(12), new BigDecimal(factura.getLitros()));
    appendNumber(row.createCell(13), costoLitro);
    appendNumber(row.createCell(14), bruto);
    
    if(descuento != null) {
      bruto.multiply(descuento);
    }
    
    BigDecimal neto = bruto.divide(new BigDecimal("1.16"), 2, RoundingMode.HALF_UP);
    BigDecimal iva  = bruto.subtract(neto);
    
    appendNumber(row.createCell(18), neto);
    appendNumber(row.createCell(19), iva);
    appendNumber(row.createCell(20), bruto);
  }

  private void excelServicio(FacturaVista factura, Row row, BigDecimal descuento) {
    if(descuento != null) {
      appendNumber(row.createCell(17), descuento);
    }

    // Tipo de servicio
    String tipoServicio = "C" + (factura.getIdTipoPedido() != null ? factura.getIdTipoPedido() : 1);
    row.createCell(0).setCellValue(tipoServicio);
    
    // Costo bruto del servicio
    BigDecimal bruto = new BigDecimal(factura.getServicio());
    appendNumber(row.createCell(10), bruto);
    
    
    if(descuento != null) {
      bruto = bruto.subtract(descuento);
    }
    
    BigDecimal neto = bruto.divide(new BigDecimal("1.16"), 2, RoundingMode.HALF_UP);
    BigDecimal iva  = bruto.subtract(neto);
    
    appendNumber(row.createCell(18), neto);
    appendNumber(row.createCell(19), iva);
    appendNumber(row.createCell(20), bruto);
  }

  private void excelGeneral(FacturaVista factura, Row row) {
    // Fecha de servicio
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    dateFormat.setTimeZone(factura.getFechaPedido().getTimeZone());
    String fechaPedido = dateFormat.format(factura.getFechaPedido().getTime());
    row.createCell(1).setCellValue(fechaPedido);

    // Estado, alcaldía y código postal
    row.createCell(2).setCellValue(factura.getEstado());
    row.createCell(3).setCellValue(factura.getMunicipio());
    row.createCell(4).setCellValue(factura.getCodigo());
    
    // Datos del cliente
    row.createCell(5).setCellValue(factura.getIdCliente().toString());
    row.createCell(6).setCellValue(factura.getClienteNombre());
    row.createCell(7).setCellValue(factura.getClienteApellidoPaterno());
    row.createCell(8).setCellValue(factura.getClienteApellidoMaterno());

    // Código de promoción
    row.createCell(9).setCellValue( factura.getCodigoPromocion() != null ?  factura.getCodigoPromocion() : "");

    // Facturación electrónica
    row.createCell(21).setCellValue(factura.getUsoCodigo());
    row.createCell(22).setCellValue(factura.getRfc());
    row.createCell(23).setCellValue(factura.getRazonSocial());
    row.createCell(24).setCellValue(factura.getCodigo());
    row.createCell(25).setCellValue(factura.getCalle());
    row.createCell(26).setCellValue(factura.getColonia());
    row.createCell(27).setCellValue(factura.getNumInterior());
    row.createCell(28).setCellValue(factura.getNumExterior());
    row.createCell(29).setCellValue(factura.getFiscalEstado());
    row.createCell(30).setCellValue(factura.getFiscalMunicipio());
    row.createCell(31).setCellValue(factura.getCorreo());
    row.createCell(32).setCellValue(factura.getTelefono());
  }

  private void excelHeader(XSSFSheet sheet) {
    // Se crean todos los encabezados
    Row row = sheet.createRow(0);

    row.createCell(0).setCellValue("ID Servicio");
    row.createCell(1).setCellValue("Fecha de servicio");
    row.createCell(2).setCellValue("Estado");
    row.createCell(3).setCellValue("Alcaldía / Municipio");
    row.createCell(4).setCellValue("Codigo postal");
    row.createCell(5).setCellValue("ID cliente");
    row.createCell(6).setCellValue("Nombre(s)");
    row.createCell(7).setCellValue("Apellido paterno");
    row.createCell(8).setCellValue("Apellido materno");
    row.createCell(9).setCellValue("Código de promoción");
    row.createCell(10).setCellValue("Importe por servicio ");
    row.createCell(11).setCellValue("Tipo de Gasolina solicitada");
    row.createCell(12).setCellValue("# Litros solicitados");
    row.createCell(13).setCellValue("Precio por litro");
    row.createCell(14).setCellValue("Importe por Servicio de Gasolina ");
    row.createCell(15).setCellValue("Importe por Servicio de Lavado");
    row.createCell(16).setCellValue("Servicio de Neumáticos ");
    row.createCell(17).setCellValue("Descuento");
    row.createCell(18).setCellValue("sin iva");
    row.createCell(19).setCellValue("16 iva");
    row.createCell(20).setCellValue("Total a pagar");
    row.createCell(21).setCellValue("Uso de CFDI");
    row.createCell(22).setCellValue("RFC");
    row.createCell(23).setCellValue("Nombre / Razón Social");
    row.createCell(24).setCellValue("Código postal");
    row.createCell(25).setCellValue("Calle");
    row.createCell(26).setCellValue("Colonia");
    row.createCell(27).setCellValue("No Interior");
    row.createCell(28).setCellValue("No Exterior");
    row.createCell(29).setCellValue("Estado");
    row.createCell(30).setCellValue("Municipio");
    row.createCell(31).setCellValue("Correo electrónico");
    row.createCell(32).setCellValue("Teléfono");
  }

  @Autowired
  private AzureBsService azureService;

    /**
     *
     * @param factura
     * @param file
     * @return
     */
    @Transactional
  public boolean updateFactura(PedidoFacturaEntity factura, MultipartFile file) {
    try {
      // Se calcula un nombre único de la imagen
      String nombreArchivo = UUID.randomUUID().toString() + "_" +  file.getOriginalFilename().replace(" ", "");

      // Se obtiene el BlobContainer de Azure
      BlobContainerClient blobContainerClient = azureContainer();

      // Se verifica que la imagen anterior exista; siendo así, se borra
      if(factura.getArchivo() != null && !factura.getArchivo().isEmpty()) {
        BlockBlobClient facturaActual = blobContainerClient.getBlobClient(factura.getArchivo()).getBlockBlobClient();
        
        if(facturaActual.exists()) {
          facturaActual.delete();
        }
      }

      // Se carga como bloque, es indistinto para imágenes tan pequeñas
      // https://docs.microsoft.com/es-mx/rest/api/storageservices/understanding-block-blobs--append-blobs--and-page-blobs
      BlockBlobClient zip = blobContainerClient.getBlobClient(nombreArchivo).getBlockBlobClient();
      zip.upload(file.getInputStream(), file.getSize());
      factura.setArchivo(nombreArchivo);
      factura.setEstatus(PedidoFacturaEstatus.DISPONIBLE);
      save(factura);

      // Intento de envío de correo
      try {
        // Consulta del cliente y pedido
        Optional<Pedido> optional = pedidoRepo.findById(factura.getIdPedido());
        Pedido pedido = optional.isPresent() ? optional.get() : null;

        if(pedido != null && pedido.getCliente() != null) {
          String message = "La factura de su servicio con folio " + pedido.getUuid() + " ha sido generada, puede descargarla desde la app";

          smtp.sendEmail("FASTGAS FACTURACIÓN", pedido.getCliente().getCorreo(), "Notificación de facturación", message);
          notification.notificar(pedido.getCliente(), "FastGas Facturación", message);
        }
      } catch (Exception e) {
        // TODO: Logger
        e.printStackTrace();
      }

      return true;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param factura
     * @return
     */
    public InputStreamResource downloadFactura(PedidoFacturaEntity factura) {
    try {
      BlobContainerClient blobContainerClient = azureContainer();

      // Flujo si la imagen ya había sido cargada
      if (factura != null && factura.getArchivo() != null && !factura.getArchivo().isEmpty()){
        ByteArrayInputStream foto = azureInputStream(blobContainerClient, factura.getArchivo());

        if(foto != null) {
          return new InputStreamResource(foto);
        }
      }

      // Si no se encuentra ninguna imagen
      return null;
    } catch (Exception e) {
      logger.error("Error al descargar la imagen: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  private BlobContainerClient azureContainer() {
    // Consulta de credenciales de Azure
    AzureBsEntity azureEntity = azureService.queryById(1);

    // Se crea un cliente del Blob Service de Azure
    BlobServiceClient blobServiceClient = new BlobServiceClientBuilder()
      .endpoint(azureEntity.getEndpoint())
      .sasToken(azureEntity.getSas())
      .buildClient();
    
    // Se crea un cliente para el contenedor
    BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(azureEntity.getContainer());
    return blobContainerClient;
  }

  private ByteArrayInputStream azureInputStream(BlobContainerClient blobContainerClient, String blobName) {
    BlockBlobClient fotoActual = blobContainerClient.getBlobClient(blobName).getBlockBlobClient();
        
    if(fotoActual.exists()) {
      // A ByteArrayOutputStream holds the content in memory
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      fotoActual.download(outputStream);
      
      // To convert it to a byte[] - simply use
      final byte[] bytes = outputStream.toByteArray();

      // To convert bytes to an InputStream, use a ByteArrayInputStream
      ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
      
      return inputStream;
    } else {
      return null;
    }
  }

    /**
     *
     * @param factura
     * @return
     */
    public FacturaMovil mapMovil(PedidoFacturaEntity factura) {
    if(factura == null) {
      return null;
    }

    String usoDesc = "";
    Double total = null;
    Calendar fechaPedido = null;
    String uuid = "";
    
    try {
      Optional<Pedido> optionalPedido = pedidoRepo.findById(factura.getIdPedido());
      
      if(optionalPedido.isPresent()) {
        Pedido pedido = optionalPedido.get();
        fechaPedido = pedido.getFechaPedido();
        total = pedido.getTotalPagar();
        uuid = pedido.getUuid();
      }
      
      Optional<CatUsoCfdiEntity> optionalUso = usoRepo.findById(factura.getUsoCfdi());
      
      if(optionalUso.isPresent()) {
        CatUsoCfdiEntity uso = optionalUso.get();
        usoDesc = uso.getDescripcion();
      }
    } catch (Exception e) { 
      e.printStackTrace();
    }

    FacturaMovil movil = new FacturaMovil();
    movil.setIdFactura(factura.getIdFactura());
    movil.setIdPedido(factura.getIdPedido());
    movil.setIdFiscal(factura.getIdFiscal());
    movil.setEstatus(factura.getEstatus());
    movil.setUsoCfdi(factura.getUsoCfdi());
    movil.setUsoCfdiTexto(usoDesc);
    movil.setTotal(total);
    movil.setDisponible(factura.getEstatus() == PedidoFacturaEstatus.DISPONIBLE);
    movil.setFechaPedido(fechaPedido);
    movil.setUuid(uuid);

    return movil;
  }

    /**
     *
     * @param facturas
     * @return
     */
    public List<FacturaMovil> mapMovil(List<PedidoFacturaEntity> facturas) {
    if(facturas == null) {
      return null;
    }

    ArrayList<FacturaMovil> movil = new ArrayList<>();
    for(PedidoFacturaEntity factura : facturas) {
      movil.add(mapMovil(factura));
    }

    return movil;
  }
}
