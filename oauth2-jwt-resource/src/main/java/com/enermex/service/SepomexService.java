package com.enermex.service;

import java.util.List;

import com.enermex.modelo.CodigoPostalEntity;
import com.enermex.modelo.EstadoEntity;
import com.enermex.repository.CodigoPostalRepository;
import com.enermex.repository.EstadoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SepomexService
 */
@Service
public class SepomexService {
  @Autowired
  private EstadoRepository estadoRepo;
  
  @Autowired
  private CodigoPostalRepository codigoRepo;

    /**
     *
     * @return
     */
    public List<EstadoEntity> queryEstados() {
    try {
      return estadoRepo.findAll();
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param idEstado
     * @param idMunicipio
     * @return
     */
    public List<CodigoPostalEntity> queryCodigos(Integer idEstado, Integer idMunicipio) {
    try {
      return codigoRepo.findByIdEstadoAndIdMunicipio(idEstado, idMunicipio);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}