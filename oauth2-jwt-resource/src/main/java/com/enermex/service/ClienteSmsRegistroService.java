package com.enermex.service;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.avg.exceptions.FastGasException;
import com.enermex.modelo.ClienteSmsRegistro;
import com.enermex.repository.ClienteSmsRegistroRepository;
import com.enermex.service.ClienteService;

/**
 *
 * @author abraham
 */
@Service
public class ClienteSmsRegistroService {
	
	@Autowired
	ClienteSmsRegistroRepository clienteSmsRegistroRepository;
	
	@Autowired	
	ClienteService  clienteService;
	/**
	 * 
	 * @param currentDate
	 * @param codigo
	 * @param id
	 * @return
	 * @throws FastGasException 
	 */
	public boolean  esCodigoVigente (String currentDate,String codigo,BigInteger id) throws FastGasException {
		
		
	 BigInteger esVigente =	clienteSmsRegistroRepository.esCodigoVigente(currentDate, codigo, id);
	
	 
	 if(esVigente.compareTo(new BigInteger("0"))==0) {
		 
		 throw new  FastGasException("FG029","El código ingresado es incorrecto, favor de intentar nuevamente");
	 }
		 
	 
	 if(esVigente.compareTo(new BigInteger("0"))==1) {
		 //lo tomamos como valido y actualizamos el uso del codigo y el update del registro del cliente
		 
		 clienteSmsRegistroRepository.updateCodigoCliente(codigo, id);
		 
		 //actualizamos al cliente a validado 
		 clienteService.updateAccesoSms(new Boolean("true"),id);
		 
		 
		 return true;
	 }
		 
	 
	 return false;
		
	}

	
	/**
	 * 
	 * @param e
	 */
	public void save(ClienteSmsRegistro e) {
		
		clienteSmsRegistroRepository.save(e);
	}
	
	

}
