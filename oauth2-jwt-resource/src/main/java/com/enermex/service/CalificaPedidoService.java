package com.enermex.service;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.CalificaPedidoRepository;
import com.enermex.modelo.CalificaPedido;
import com.enermex.repository.PedidoRepository;

/**
 *
 * @author abraham
 */
@Service
public class CalificaPedidoService {
	
	@Autowired
	CalificaPedidoRepository calificaPedidoRepository;
	
	@Autowired
	PedidoRepository pedidoRepository;
	
	/**
	 * Califica al despachador el cliente
	 * @param idPedido
	 * @param calificacion
	 * @param comentario
	 */
	public void calificaClienteDespachador(BigInteger idPedido,Integer calificacion,String comentario) {
		
		CalificaPedido cp = new CalificaPedido(idPedido,calificacion,"Cliente",comentario);
		
		
		calificaPedidoRepository.save(cp);
		
		//actualiza el pedido como calificado
		pedidoRepository.calificaPedido(new Boolean(true), idPedido);
		
		//enviamos push notification
		//Estimado cliente el ID servicio XXXXXX, ha sido atendido, gracias por confiar en nosotros
		
		
	}
	
	/**
	 * Califica al cliente el despachador
	 * @param idPedido
	 * @param calificacion
	 * @param comentario
	 */
	public void calificaDespachadorCliente(BigInteger idPedido,Integer calificacion,String comentario) {
		
		CalificaPedido cp = new CalificaPedido(idPedido,calificacion,"Despachador",comentario);
		
		
		calificaPedidoRepository.save(cp);
		
		//enviamos push notification
	}
	
	/**
	 * Obtiene la calificacion del despachador con relacion al pedido
	 * @param idPedido
	 * @return
	 */
	public Integer getCalificacionByIdPedido(BigInteger idPedido) {
		
		return calificaPedidoRepository.getCalificacionByIdPedido(idPedido);
	}
	
	
	
}
