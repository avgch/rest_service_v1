package com.enermex.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.enermex.enumerable.DireccionEstatus;
import com.enermex.modelo.ClienteDireccionEntity;
import com.enermex.repository.ClienteDireccionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class ClienteDireccionService {
  Logger logger = LoggerFactory.getLogger(ClienteDireccionService.class);
  
	@Autowired
	private ClienteDireccionRepository repo;
	
	/**
   * Creación de una dirección
   * 
     * @param idCliente
     * @param direccion
   * @param dirección Direccion por insertar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  public boolean create(Long idCliente, ClienteDireccionEntity direccion) {
    direccion.setIdCliente(idCliente);
    direccion.setFechaCreacion(new Date());
    direccion.setIdEstatus(DireccionEstatus.ACTIVO);
    return save(direccion);
  }

	/**
   * Creación de una dirección
   * 
     * @param direccion
   * @param dirección Direccion por insertar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  public boolean delete(ClienteDireccionEntity direccion) {
    direccion.setIdEstatus(DireccionEstatus.ELIMINADO);
    return save(direccion);
  }

  /**
   * Consulta de las direcciones registradas y activas
   * 
     * @param idCliente
   * @return Lista con direcciones registradas y activas
   */
  @Transactional(readOnly = true)
  public List<ClienteDireccionEntity> queryAll(Long idCliente) {
    try {
      return repo.findAllByIdClienteAndIdEstatus(idCliente, DireccionEstatus.ACTIVO);
    } catch (Exception e) {
      logger.error("Error al obtener direcciones: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de una dirección por su identificador
   * 
   * @param id Identificador de la dirección
   * @return La dirección, si se encuentra
   */
  @Transactional(readOnly = true)
  public ClienteDireccionEntity queryById(Long id) {
    try {
      Optional<ClienteDireccionEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener un direccion: " + e.getMessage());
      return null;
    }
  }

  /**
   * Comprueba si existe una dirección
   * 
   * @param direccion
   * @return Bandera que indica si existe el dirección
   */
  @Transactional(readOnly = true)
  private boolean exists(ClienteDireccionEntity direccion) {
    try {
      return direccion != null && direccion.getIdDireccion() != null && repo.existsById(direccion.getIdDireccion());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un direccion: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe una dirección
   * 
   * @param id Identificador de la dirección
   * @return Bandera que indica si existe el dirección
   */
  @Transactional(readOnly = true)
  public boolean exists(Long id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un direccion: " + e.getMessage());
      return false;
    }
  }

  /**
   * Guardado de la dirección
   * 
   * @param direccion Dirección por guardar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  private boolean save(ClienteDireccionEntity direccion) {
    try {
      repo.save(direccion);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar un direccion: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}
