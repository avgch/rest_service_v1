package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.modelo.CarSerie;
import com.enermex.repository.CarSerieRepository;

/**
 *
 * @author abraham
 */
@Service
public class CarSerieService {
	
	@Autowired
	CarSerieRepository carSerieRepository;
	
    /**
     *
     * @param idModelo
     * @param idGeneration
     * @return
     */
    public List<CarSerie> getSeriesByModeloId(Integer idModelo,Integer idGeneration){
		
		return carSerieRepository.getSeriesByModeloId(idModelo,idGeneration);
	}

}
