package com.enermex.service;


import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.dto.CodigoPromocionResponseDto;
import com.enermex.dto.ProductoDto;
import com.enermex.dto.PromocionDto;
import com.enermex.dto.PromocionMovilDto;
import com.enermex.dto.PromocionUpdateDto;
import com.enermex.dto.ValidaPromoDto;
import com.enermex.enumerable.PlanesEnum;
import com.enermex.enumerable.ProductosEnum;
import com.enermex.enumerable.PushEnum;
import com.enermex.repository.ProductoPromocionRepository;
import com.enermex.repository.PromocionClienteRepository;
import com.enermex.repository.PromocionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.repository.impl.ClienteRepositoryImpl;
import com.enermex.repository.impl.CodigoPostalRepositoryImpl;
import com.enermex.repository.impl.PromocionRepositoryImpl;
import com.enermex.utilerias.CalculoPromocion;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.enermex.modelo.Promocion;
import com.enermex.modelo.PromocionCliente;
import com.enermex.modelo.CodigoPostalEntity;
import com.enermex.modelo.CpPromocion;
import com.enermex.modelo.PlanPromocion;
import com.enermex.modelo.ProductoPromocion;
import com.enermex.repository.PromocionPlanRepository;
import com.enermex.repository.CpPromocionRepository;

/**
 *
 * @author abraham
 */
@Service
public class PromocionService {
	
	@Autowired
	PromocionRepositoryImpl promocionRepositoryImpl;
	
	@Autowired
	PromocionRepository promocionRepository;
	
	@Autowired
	CalculoPromocion calculoPromocion;
	
	@Autowired
	ClienteRepositoryImpl clienteRepositoryImpl;
	
	@Autowired
	CodigoPostalRepositoryImpl codigoPostalRepositoryImpl;
	
	@Autowired
	PromocionClienteRepository promocionClienteRepository;
	
	@Autowired
	ProductoPromocionRepository productoPromocionRepository;
	
	@Autowired
	PromocionPlanRepository promocionPlanRepository;
	
	@Autowired
	CpPromocionRepository  cpPromocionRepository;
  
	/**
	 * 
	 * @param promo
	 * @param idCliente
	 * @return
	 */
    public CodigoPromocionResponseDto validaPromo(ValidaPromoDto promo,BigInteger idCliente) {
    	
    	 
    	
    	CodigoPromocionResponseDto promoDto = this.promocionRepositoryImpl.validaPromo(promo,idCliente);
    	
    	Promocion p = this.promocionRepository.getById(promoDto.getIdPromocion());
    	
    	
    	if(p==null) {
    		promoDto.setEsInvalido(true);
			promoDto.setMensaje("Código de promoción inválido");
			return promoDto;
    	}
    	
    	if(p.getEstatus()==null) {
    		promoDto.setEsInvalido(true);
			promoDto.setMensaje("Código de promoción inactivo");
			return promoDto;
    	}
    	
    	if(!p.getEstatus()) {
    		promoDto.setEsInvalido(true);
			promoDto.setMensaje("Código de promoción inactivo");
			
			return promoDto;
    	}
    		
    	
    	//Si es valida la promocion entonces accedemos a los productos que tienen descuento
    	if(promoDto.isEsInvalido()==false) {
    		System.out.println("Promo valida");
    		
    		//consultamos los ids de productos asociados a las promo
    		//List<ProductoPromocion>  productosPromo = productoPromocionRepository.getProductosPromo(promoDto.getIdPromocion());
    		    		
    		if(p!=null) {    			
    		for(ProductoPromocion pp : p.getProductosPromo()) {
    			
    			if(pp.getProductoServicio().getId().compareTo(new Long(ProductosEnum.REGULAR.getValor()))==0) {
    				if(pp.getProductoServicio().getId().compareTo(new Long(promo.getIdCombustible()))==0) {
    				  promoDto.setIdCombustible(ProductosEnum.REGULAR.getValor());
    				  promoDto.setCombustible(true);
    				}
    			}
                if(pp.getProductoServicio().getId().compareTo(new Long(ProductosEnum.PREMIUM.getValor()))==0) {
                	if(pp.getProductoServicio().getId().compareTo(new Long(promo.getIdCombustible()))==0) {
                  	  promoDto.setIdCombustible(ProductosEnum.PREMIUM.getValor());
                	  promoDto.setCombustible(true);
                	}
    			}
                if(pp.getProductoServicio().getId().compareTo(new Long(ProductosEnum.DIESEL.getValor()))==0) {
                	if(pp.getProductoServicio().getId().compareTo(new Long(promo.getIdCombustible()))==0) {
                  	   promoDto.setIdCombustible(ProductosEnum.DIESEL.getValor());
                	   promoDto.setCombustible(true);
                	}
    			}
                if(pp.getProductoServicio().getId().compareTo(new Long(ProductosEnum.LAVADO_AUTO.getValor()))==0) {
                	if(promo.isEsLavado())
                	  promoDto.setLavado(true);
    			}
                if(pp.getProductoServicio().getId().compareTo(new Long(ProductosEnum.REVISION_NEUMATICOS.getValor()))==0) {
                	if(promo.isEsNeumaticos())
                	  promoDto.setNeumaticos(true);
    			}
    			
    		}
    	  }
    		if((promoDto.isCombustible()==false)  && (promoDto.isNeumaticos()==false ) &&
    			(promoDto.isLavado()==false ) ){
    			
    			promoDto.setEsInvalido(true);
    			promoDto.setMensaje("Código de promoción inválido");
    		}
    		
    	}
      
    	
    	return promoDto;
    }
    
    
    /**
     * For web
     * @return
     */
    public List<PromocionMovilDto> getAll() {
 
    	
    	Calendar hoy = Calendar.getInstance();
    	
    	List<Promocion> promos=this.promocionRepository.getAll();
    	
    	List<PromocionMovilDto> promosActivas = new ArrayList<>();
    	
    	for(Promocion p :promos) {
    		PromocionMovilDto pDto= new PromocionMovilDto();
    		
    		pDto.setIdPromocion(p.getIdPromocion());
    		pDto.setTitulo(p.getTitulo());
    		pDto.setCodigoPromocion(p.getCodigoPromocion());
    		pDto.setDetalle(p.getDetalle());
    		pDto.setFechaHoraInicio(p.getFechaHoraInicio());
    		pDto.setFechaHoraVigencia(p.getFechaHoraVigencia());
    		//pDto.setEstatus( (p.getFechaHoraVigencia().compareTo(hoy)>0) ? true : false);
    		pDto.setEstatus(p.getEstatus());
    		pDto.setImagenPromo(p.getImagenPromo());
    		
    		promosActivas.add(pDto);
    	}
    	
    	
     	
    	return promosActivas;
    	
    }
    
    /**
     * For movil
     * @param idCliente
     * @return
     */
    public List<PromocionMovilDto> getAll(BigInteger idCliente) {
 
    	List<Promocion> promosActivas = new ArrayList<>();
    	Calendar hoy = Calendar.getInstance();
    	Long[] promosSegFreqIds;
    	List<Promocion> promosSegFreq;
    	
    	//Primero consultamos todas las promos vigentes y que fueron para
    	//todos los clientes
    	List<Promocion> promosTodos=this.promocionRepository.getAllActivasForAllCustomers(hoy);
    	
    	if(promosTodos!=null) {
    		if(promosTodos.size()>0) {
    			promosActivas.addAll(promosTodos);		
    		}
    	}
    	
    	
    	
    	//Ahora validamos si tenemos para clientes frecuentes y segmentadas
    	//1. Buscamos los ids
    	promosSegFreqIds=this.promocionRepository.promosClientesFreqSegmetado(idCliente);
    	
    	if(promosSegFreqIds!=null) {
    	//1. Buscamos las promos con los ids recuperados
    		
    		if(promosSegFreqIds.length>0) {
    			 promosSegFreq=this.promocionRepository.getAllActiveByIds(hoy,promosSegFreqIds);
    			 
    			 if(promosSegFreq!=null) {
    			     if(promosSegFreq.size()>0)
    				   promosActivas.addAll(promosSegFreq);
    			 }
    		    		
    		}
    	
    	
    	}
    	
    	List<PromocionMovilDto> promosShow = new ArrayList<>();
    	
    	for(Promocion p :promosActivas) {
    		PromocionMovilDto pDto= new PromocionMovilDto();
    		
    		pDto.setIdPromocion(p.getIdPromocion());
    		pDto.setTitulo(p.getTitulo());
    		pDto.setCodigoPromocion(p.getCodigoPromocion());
    		pDto.setDetalle(p.getDetalle());
    		pDto.setFechaHoraInicio(p.getFechaHoraInicio());
    		pDto.setFechaHoraVigencia(p.getFechaHoraVigencia());
    		pDto.setEstatus( (p.getFechaHoraVigencia().compareTo(hoy)>0) ? true : false); 
    		pDto.setImagenPromo(p.getImagenPromo());
    		
    		promosShow.add(pDto);
    	}
    	
    	
     	
    	return promosShow;
    	
    }
    /**
     * 
     * @param idPromocion
     * @return
     * @throws ParseException 
     */
    public PromocionDto findByIdEdit(Long idPromocion) throws ParseException {
    	
    	PromocionDto dto = new PromocionDto() ;
        ProductoDto  prod ;
           Promocion p = this.promocionRepository.getById(idPromocion);
                      dto.setTitulo(p.getTitulo());
           dto.setDetalle(p.getDetalle());
           dto.setClientesAlgunos(p.getParaAlgunos());
           dto.setClientesFrecuentes(p.getEsFrecuente());
           dto.setClientesTodos(p.getParaTodos());
           
           if(p.getMonto()==null) {
        	   dto.setMontoDescuento(p.getDescuento());
        	   dto.setDescuentoMonto(true);
           }
           if(p.getDescuento()==null) {
        	   dto.setMontoDescuento(p.getMonto().toString());
        	   dto.setDescuentoPorcentaje(true);
           }
           
           //productos
           for(ProductoPromocion pp : p.getProductosPromo()) {
        	   prod = new ProductoDto();
        	   prod.setId(pp.getProductoServicio().getId());
        	   prod.setNombre(pp.getProductoServicio().getNombre());
        	   dto.getProductosSave().add(prod);
           }
           //planes
           for(PlanPromocion plan  :p.getPlanesPromo()) {
        	   
        	   if(plan.getTipoPlan().getId().compareTo(PlanesEnum.PARTICULAR.getValor())==0)
        		   dto.setPlanIndividual(true);
              
        	   if(plan.getTipoPlan().getId().compareTo(PlanesEnum.FAMILIAR.getValor())==0)
        		   dto.setPlanFamiliar(true);
        	   
        	   if(plan.getTipoPlan().getId().compareTo(PlanesEnum.EMPRESARIAL.getValor())==0)
        		   dto.setPlanEmpresarial(true);
           
           }
           
           SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
 		  TimeZone cdmx = TimeZone.getTimeZone("Mexico/General");
 		  sdf.setTimeZone(cdmx);
 		  
 		  String fechaInc= sdf.format(p.getFechaHoraInicio().getTime());
 		  Date fechaIncF=sdf.parse(fechaInc);
           
           dto.setFechaInicio(p.getFechaHoraInicio().getTime());
           dto.setHoraInicio(p.getFechaHoraInicio().getTime());
           
           dto.setFechaFin(p.getFechaHoraVigencia().getTime());
           dto.setHoraFin(p.getFechaHoraVigencia().getTime());
           
           dto.setIdEstado(p.getIdEstado());
           dto.setIdAlcaldia(p.getIdAlcaldia());
           dto.setIdPromocion(p.getIdPromocion());
           for(CpPromocion cp :p.getCps()) {
        	   dto.getCpsSave().add(cp.getCodigo());
           }
           
           
           return dto;
    	
    }
    
    /**
     * 
     * @param idPromocion
     * @return
     */
    public Promocion findById(Long idPromocion) {
    	

         return  this.promocionRepository.getById(idPromocion);
    	
    }
    /**
     * 
     * @param promoP
     * @param promo
     * @return 
     * @throws FastGasException 
     */
    public Promocion save(PromocionDto promoP) throws FastGasException {
    	
    	List<Integer> planes;
        Promocion promo = new Promocion(promoP,false);
        List<BigInteger> clientesIds=null;
        List<PromocionCliente> promoClientes = new ArrayList<>();
        
 	    PromocionCliente promoCliente;
 	    
 	    //Formateamos fecha
 	    SimpleDateFormat sdfD = new SimpleDateFormat("dd/MM/yyyy HH:mm");
 	    String hoy = sdfD.format(new Date());
    
 	   //Generar el codigo de promocion
        String codigoPromocion="";
        boolean codigoValido=false;
        
        while(codigoValido==false) {
        	
        	 codigoPromocion  = String.valueOf(Math.abs(new Random(System.currentTimeMillis()).nextInt(90000)+10000));
        	 List<Long> idsPromo =promocionRepository.existeCodigoPromo(codigoPromocion, hoy);
        	 
        	 if(idsPromo==null) {
        		 codigoValido=true;
        		 break;
        	 }
        	 
        	 if(idsPromo.size()==0) {
        		 
        		 codigoValido=true;
        		 break;
        	 }
        		 
        	System.out.println("Codigo de promocion valido");
        	
        }
        
        System.out.println("Codigo de promocion valido");
        
        String push = buildPushPromo(promoP,codigoPromocion);
        promo.setCodigoPromocion(codigoPromocion);
        //validamos aqui si ya existe la promo
        Promocion result = this.promocionRepository.save(promo);
      
        
        planes =calculoPromocion.getPlanes(promoP.getPlanFamiliar(), promoP.getPlanIndividual(),promoP.getPlanEmpresarial());
        
        /* CLIENTES FRECUENTES */
        if(promoP.getClientesFrecuentes()==true) {
        	calculoPromocion.clientesFrecuentes(planes,result.getIdPromocion(),push,promoP.getDetalle());
        }
        
        /* CLIENTES ALGUNOS */
        if(promoP.getClientesAlgunos()) {
        	
        	//obtenemos los alcaldias con base a ellas obtenemos los tokens
        	
        	try {
        		clientesIds=customerToNotify(promoP.getCps(),push,promoP.getDetalle(), true);
			} catch (FirebaseMessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new FastGasException("Contacte al administrador del sistema","ADMERR");
			}
        	
     	   //notificamos y guardamos en t_promocion_cliente
          if(clientesIds!=null) {
     	   for(BigInteger id : clientesIds) {
     		   promoCliente = new PromocionCliente();
     		   promoCliente.setIdCliente(id);
     		   promoCliente.setIdPromocion(result.getIdPromocion());
     		   
     		   promoClientes.add(promoCliente);
     		  promocionClienteRepository.save(promoCliente);
     	   }
     	   promocionClienteRepository.saveAll(promoClientes);
     	   
          }
        	
        }
        
        /* CLIENTES TODOS */
        if(promoP.getClientesTodos()) {
        	
        	try {
				   notifyAllClientApp(promoP,push, true);
				
			} catch (FirebaseMessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			 throw new FastGasException("Contacte al administrador del sistema","ADMERR");
			}
        	
        }
    	    	
    	return result;
    	
    }
    
    /**
     * 
     * @param promoP
     * @param promo
     * @return 
     * @throws FastGasException 
     */
    public Long update(PromocionDto promoP) throws FastGasException {
    	
    	List<Integer> planes;
    	
    	//eliminamos los planes
    	
    	
    	//eliminamos los productos
        productoPromocionRepository.deleteByIdPromo(promoP.getIdPromocion());
    	
    	//eliminamos cliente promocion
    	promocionClienteRepository.delete(promoP.getIdPromocion());
    	
    	//eliminamos los planes
    	promocionPlanRepository.delete(promoP.getIdPromocion());
  	
    	//elimina los cps de la promo
    	cpPromocionRepository.delete(promoP.getIdPromocion());
    	
    	Promocion promoUpdate = new Promocion(promoP,true);
    	promoUpdate.setEstatus(promoP.getEstatus());
    	promoUpdate.setIdPromocion(promoP.getIdPromocion());
        List<BigInteger> clientesIds=null;
        List<PromocionCliente> promoClientes = new ArrayList<>();
 	    PromocionCliente promoCliente;
 	    
 	    //Formateamos fecha
 	    SimpleDateFormat sdfD = new SimpleDateFormat("dd/MM/yyyy HH:mm");
 	    String hoy = sdfD.format(new Date());
    
 	   //Generar el codigo de promocion
        String codigoPromocion="";
        codigoPromocion=this.promocionRepository.getCodPromoById( promoUpdate.getIdPromocion());
        
        String push = buildPushPromo(promoP,codigoPromocion);
        promoUpdate.setCodigoPromocion(codigoPromocion);
        //actualizamos la promo 
        this.promocionRepository.update(promoUpdate.getTitulo(), promoUpdate.getCodigoPromocion(), promoUpdate.getDescuento(),
        		promoUpdate.getMonto(), promoUpdate.getDetalle(), promoUpdate.getEstatus(), promoUpdate.getEsFrecuente(), promoUpdate.getParaTodos(),
        		promoUpdate.getParaAlgunos(), promoUpdate.getFechaHoraInicio(), promoUpdate.getFechaHoraVigencia(), promoUpdate.getIdEstado(), promoUpdate.getIdAlcaldia(), promoUpdate.getIdPromocion());
        
        
        //guardamos los planes-promo
        List<PlanPromocion> planesPromo = new ArrayList<>(promoUpdate.getPlanesPromo());
        System.out.println(planesPromo.size());
        promocionPlanRepository.saveAll(planesPromo);
        
        //guardamos productos promo
        List<ProductoPromocion> productosPromo = new ArrayList<>(promoUpdate.getProductosPromo());
        productoPromocionRepository.saveAll(productosPromo);
        
        //guarda los nuevos cps si aplica
        List<CpPromocion>       cpsPromo     = new ArrayList<>(promoUpdate.getCps());
        cpPromocionRepository.saveAll(cpsPromo);
        
        planes =calculoPromocion.getPlanes(promoP.getPlanFamiliar(), promoP.getPlanIndividual(),promoP.getPlanEmpresarial());
        
        /* CLIENTES FRECUENTES */
        if(promoP.getClientesFrecuentes()==true) {
        	calculoPromocion.clientesFrecuentes(planes,promoP.getIdPromocion(),push,promoP.getDetalle());
        }
        
        /* CLIENTES ALGUNOS */
        if(promoP.getClientesAlgunos()) {
        	
        	//obtenemos los alcaldias con base a ellas obtenemos los tokens
        	
        	try {
        		clientesIds=customerToNotify(promoP.getCps(),push,promoP.getDetalle(), promoP.getEstatus());
			} catch (FirebaseMessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new FastGasException("Contacte al administrador del sistema","ADMERR");
			}
        	
     	   //notificamos y guardamos en t_promocion_cliente
          if(clientesIds!=null) {
     	   for(BigInteger id : clientesIds) {
     		   promoCliente = new PromocionCliente();
     		   promoCliente.setIdCliente(id);
     		   promoCliente.setIdPromocion(promoP.getIdPromocion());
     		   
     		   promoClientes.add(promoCliente);
     		  promocionClienteRepository.save(promoCliente);
     	   }
     	   promocionClienteRepository.saveAll(promoClientes);
     	   
          }
        	
        }
        
        /* CLIENTES TODOS */
        if(promoP.getClientesTodos()) {
        	
        	try {
				   notifyAllClientApp(promoP,push, promoP.getEstatus());
				
			} catch (FirebaseMessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			 throw new FastGasException("Contacte al administrador del sistema","ADMERR");
			}
        	
        }
    	    	
    	System.out.println("promocion actualizada");
    	return promoP.getIdPromocion();
    }
    
    /**
     * 
     * @param imagePromo
     * @param idPromocion
     */
    public void saveImagePromo(byte[] imagePromo,Long idPromocion) {
    	
    	this.promocionRepository.updateImagePromo(imagePromo, idPromocion);
    }
    

	
	/**
	 * Notifica a todos los usuarios activos de la app
	 * @throws FirebaseMessagingException 
	 */
    private void notifyAllClientApp(PromocionDto promoP,String push, boolean sendNotification) throws FirebaseMessagingException{
		
		
		List<String> tokens = clienteRepositoryImpl.notificaClientesTodos();
		System.out.println(tokens);
		//NOTIFICAMOS
		
//	    //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();			    
	    
	    
	    if(tokens!=null)
	    {
	    	if(tokens.size()>0 && sendNotification) {
		    //Notificacion
		    Notification.Builder msNot = Notification.builder();
		    
		    msNot.setBody(push);
		    msNot.setTitle(promoP.getTitulo());
		    msg.addAllTokens( (List<String>)(List<?>)tokens);
		    
		    msg.setNotification(msNot.build());
		    
		    FirebaseMessaging fm = FirebaseMessaging.getInstance();
		    
		    fm.sendMulticast(msg.build());
	       }
	   }
	    
	}
    
    

	/**
	 * Notifica a clientes por zona
	 * @param areasANotificas
	 * @throws FirebaseMessagingException
	 */
	private List<BigInteger> customerToNotify(List<CodigoPostalEntity> cps,String push,String body, boolean sendNotification ) throws FirebaseMessagingException{
		
		List<String> tokens;		
		
		System.out.println(cps);
		//obtengo los cps participantes 
		
		//obtengo mi lista de tokens participantes con base al cp
		tokens = clienteRepositoryImpl.getAllTokensNotify(cps);
		
		//NOTIFICAMOS
		
//	    //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();			    
	    
            

	    if(tokens!=null){
		    //Notificacion
	    	if(tokens.size()>0) {

          if (sendNotification){
            Notification.Builder msNot = Notification.builder();
            
            msNot.setBody(body);
            msNot.setTitle(push);
            msg.addAllTokens( (List<String>)(List<?>)tokens);
            
            msg.setNotification(msNot.build());
            
            FirebaseMessaging fm = FirebaseMessaging.getInstance();
            
            fm.sendMulticast(msg.build());
          }
			    
			   return  clienteRepositoryImpl.getAllClinetesNotify(cps);
	    	}   
	    }
	    
	    return null;	   
	}
	private String buildPushPromo(PromocionDto promo,String codigo) {
		
		SimpleDateFormat spdf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat spdf2 = new SimpleDateFormat("HH:mm");
		
		String fechaInc = spdf.format(promo.getFechaInicio());
		String fechaFin = spdf.format(promo.getFechaFin());
		String hrInc   = spdf2.format(promo.getHoraInicio());
        String hrFin   = spdf2.format(promo.getHoraFin());
		
		String push="";
		push = PushEnum.PROMOCION.getValor().replace("CODIGO",codigo);
        push = push.replace("FECHA_INC", fechaInc);
        push = push.replace("FECHA_FIN", fechaFin);
        push = push.replace("HR_INC", hrInc);
        push = push.replace("HR_FIN", hrFin);
        
		
       return push;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public PromocionDto getPromoById(Long id) {
		
		this.promocionRepository.getById(id);
		
		
		
		
		
		
		return null;
	}
	
	
    
	

}
