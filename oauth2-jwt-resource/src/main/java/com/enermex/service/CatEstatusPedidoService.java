package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.modelo.CatEstatusPedido;
import com.enermex.repository.CatPedidoEstatusRepository;

/**
 *
 * @author abraham
 */
@Service
public class CatEstatusPedidoService {
	
	@Autowired
	CatPedidoEstatusRepository catPedidoEstatusRepository; 
	
    /**
     *
     * @return
     */
    public List<CatEstatusPedido> getAll(){
		
		return  (List<CatEstatusPedido>) catPedidoEstatusRepository.findAll();
		
	}
	

}
