package com.enermex.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import com.enermex.modelo.DespachadorFcmEntity;
import com.enermex.repository.DespachadorFcmRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class DespachadorFcmService {
  Logger logger = LoggerFactory.getLogger(DespachadorFcmService.class);

  @Autowired
  private DespachadorFcmRepository repo;

    /**
     *
     * @param push
     * @return
     */
    @Transactional
  public boolean create(DespachadorFcmEntity push) {
    try {
      DespachadorFcmEntity anterior = repo.findByFcm(push.getFcm());

      if(anterior != null) {
        anterior.setIdDespachador(push.getIdDespachador());
        return save(anterior);
      }
      
      return save(push);
    } catch(Exception e) {
      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param push
     * @return
     */
    public boolean delete(DespachadorFcmEntity push) {
    if (!exists(push)) {
      logger.error("Trata de eliminar una llave inexistente - ID: " + push.getIdFcm());

      return false;
    }

    try {
      repo.delete(push);
      return true;
    } catch (Exception e) {
      logger.error("Error al eliminar una llave - ID: " + push.getIdFcm());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public DespachadorFcmEntity queryById(BigInteger id) {
    try {
      Optional<DespachadorFcmEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una ruta: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param idDespachador
     * @return
     */
    @Transactional(readOnly = true)
  public List<DespachadorFcmEntity> queryByIdDespachador(Long idDespachador) {
    try {
      return repo.findByIdDespachador(idDespachador);
    } catch (Exception e) {
      logger.error("Error al obtener llaves: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(DespachadorFcmEntity push) {
    try {
      return push != null && push.getIdFcm() != null && repo.existsById(push.getIdFcm());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una llave: " + e.getMessage());
      return false;
    }
  }

  @Transactional(propagation = Propagation.NESTED)
  private boolean save(DespachadorFcmEntity push) {
    try {
      repo.save(push);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una llave: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}