package com.enermex.service;


import com.enermex.dto.NotificaDespachadorDto;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.DespachadorRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.impl.UsuarioRepositoryImpl;

/**
 *
 * @author abraham
 */
@Service
public class DespachadorService {
    Logger logger = LoggerFactory.getLogger(DespachadorService.class);

    @Autowired
    private DespachadorRepository despachadorRepository;
    
    @Autowired
    UsuarioRepositoryImpl usuarioRepositoryImpl;
    
    /**
     *
     * @param correo
     * @return
     */
    public UsuarioEntity findDespachadorByCorreo(String correo) {
        
    	UsuarioEntity cli=null; 
  	
        cli= this.despachadorRepository.findByEmail(correo);
           
  	return cli;
  	
    }
    
    /**
     *
     * @return
     */
    public List<NotificaDespachadorDto> getDespachadores (){
    	
    	
    	return  usuarioRepositoryImpl.getDespachadores();    	
    	
    	 
    }
    
}
