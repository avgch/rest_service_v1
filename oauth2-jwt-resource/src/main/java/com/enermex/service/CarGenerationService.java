package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.modelo.CarGeneration;
import com.enermex.repository.CarGenerationRepository;

/**
 *
 * @author abraham
 */
@Service
public class CarGenerationService {

	@Autowired
	CarGenerationRepository carGenerationRepository;
	
    /**
     *
     * @param idModelo
     * @return
     */
    public List<CarGeneration> getGenerationByModeloId(Integer idModelo){
		
		return carGenerationRepository.getGenerationByModeloId(idModelo);
	}
}
