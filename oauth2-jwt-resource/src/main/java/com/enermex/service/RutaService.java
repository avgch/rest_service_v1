package com.enermex.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.enermex.dto.ruta.RutaWeb;
import com.enermex.enumerable.RutaEstatus;
import com.enermex.modelo.RutaCodigoEntity;
import com.enermex.modelo.RutaEntity;
import com.enermex.repository.RutaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.modelo.UsuarioEntity;

/**
 * RutaService
 */
@Service
public class RutaService {
  Logger logger = LoggerFactory.getLogger(RutaService.class);

  @Autowired
  private RutaRepository repo;

    /**
     *
     * @param ruta
     * @return
     */
    @Transactional
  public boolean create(RutaEntity ruta) {
    if (exists(ruta)) {
      logger.error("Trata de añadir una zona de cobertura existente - ID: " + ruta.getIdRuta());

      return false;
    }

    // Se establece el estado y la fecha de creación
    Date date = new Date();
    ruta.setFechaCreacion(date);

    return save(ruta);
  }

    /**
     *
     * @param ruta
     * @return
     */
    public boolean update(RutaEntity ruta) {
    if (!exists(ruta)) {
      logger.error("Trata de actualizar una zona de cobertura inexistente - ID: " + ruta.getIdRuta());

      return false;
    }

    return save(ruta);
  }

    /**
     *
     * @param ruta
     * @return
     */
    public boolean delete(RutaEntity ruta) {
    if (!exists(ruta)) {
      logger.error("Trata de eliminar una zona de cobertura inexistente - ID: " + ruta.getIdRuta());

      return false;
    }

    ruta.setEstatus(RutaEstatus.INACTIVO);
    return save(ruta);
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public List<RutaEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener zonas de cobertura: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public RutaEntity queryById(Integer id) {
    try {
      Optional<RutaEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una zona de cobertura: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(RutaEntity ruta) {
    try {
      return ruta != null && ruta.getIdRuta() != null && repo.existsById(ruta.getIdRuta());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una zona de cobertura: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public boolean exists(Integer id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una zona de cobertura: " + e.getMessage());
      return false;
    }
  }

  @Transactional(propagation = Propagation.NESTED)
  private boolean save(RutaEntity ruta) {
    try {
      repo.save(ruta);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una zona de cobertura: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
  
  //@Transactional

    /**
     *
     * @param cp
     * @return
     */
  public List<RutaEntity> getRutasActivas(String cp) {
	  List<RutaEntity> rutas;
	  List<RutaEntity> rutasConCobertura = new ArrayList<>();
	  
	  rutas = this.repo.findAll();
	  //Obtenemos las rutas que tienen  cobertura
	  for( RutaEntity r :rutas) {
		  if(r.getEstatus().getEstatus().equalsIgnoreCase("A")){
			  for ( RutaCodigoEntity re: r.getCodigos()) {
				  
				  //validamos cobertura vs cp pedido
				  if(re.getIdCodigo().equalsIgnoreCase(cp)){
					  rutasConCobertura.add(r);
				  }
			  }
		  }
	  }
	  
	  
	  
	  
	  return rutasConCobertura;
  }

    /**
     *
     * @param rutas
     * @return
     */
    public List<RutaWeb> mapWeb(List<RutaEntity> rutas) {
    if(rutas == null) {
      return null;
    }

    ArrayList<RutaWeb> web = new ArrayList<>();
    rutas.forEach(ruta -> web.add(mapWeb(ruta)));
    return web;
  }

    /**
     *
     * @param ruta
     * @return
     */
    public RutaWeb mapWeb(RutaEntity ruta) {
    if(ruta == null) {
      return null;
    }

    RutaWeb web = new RutaWeb();
    web.setIdRuta(ruta.getIdRuta());
    web.setNombre(ruta.getNombre());
    web.setEstatus(ruta.getEstatus());
    web.setFechaCreacion(ruta.getFechaCreacion());
    web.setCodigos(ruta.getCodigos());
    web.setFechaBaja(ruta.getFechaBaja());
    
    ruta.getDespachadores().forEach(despachador -> {
      RutaWeb.Usuario usuario = new RutaWeb.Usuario();
      usuario.setNombre(despachador.getNombre());
      usuario.setApellidoPaterno(despachador.getApellidoPaterno());
      usuario.setApellidoMaterno(despachador.getApellidoMaterno());

      web.getDespachadores().add(usuario);
    });

    return web;
  }
}
