package com.enermex.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.enermex.config.AzureConfiguration;
import com.enermex.dto.queja.QuejaMovil;
import com.enermex.dto.queja.QuejaWeb;
import com.enermex.enumerable.QuejaEstatus;
import com.enermex.enumerable.QuejaTipoDetalle;
import com.enermex.modelo.AzureBsEntity;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.DireccionCliente;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.QuejaDetalleEntity;
import com.enermex.modelo.QuejaEntity;
import com.enermex.modelo.QuejaEvidenciaEntity;
import com.enermex.modelo.RutaEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.DireccionClienteRepository;
import com.enermex.repository.PedidoRepository;
import com.enermex.repository.QuejaDetalleRepository;
import com.enermex.repository.QuejaEvidenciaRepository;
import com.enermex.repository.QuejaRepository;
import com.enermex.utilerias.FirebaseNotification;
import com.enermex.utilerias.RestResponse;
import com.enermex.utilerias.SmtpGenerico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Servicio de gestión de quejas
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 5 de febrero de 2020
 * Última modificación: 3 de febrero de 2020
 */
@Service
public class QuejaService {
  Logger logger = LoggerFactory.getLogger(QuejaService.class);

  @Autowired
  private QuejaRepository repo;

  @Autowired
  private QuejaDetalleRepository detalleRepo;

  @Autowired
  private QuejaEvidenciaRepository evidenciaRepo;

  @Autowired
  private UsuarioService usuarioService;

  @Autowired
  private PedidoRepository pedidoRepository;

  @Autowired
  private RutaService rutaService;

  @Autowired
  private AzureBsService azureService;

  @Autowired
  private DireccionClienteRepository direccionClienteRepository;

  @Autowired
  private SmtpGenerico smtp;

  @Autowired
  private FirebaseNotification notification;

    /**
     *
     * @param idPedido
     * @return
     */
    @Transactional
  public List<QuejaEntity> queryByPedido(BigInteger idPedido) {
    try {
      return repo.findByIdPedido(idPedido);
    } catch(Exception e) {
      e.printStackTrace();
      //TODO: Logger

      return null;
    }
  }
  /**
   * Lista de quejas generadas por clientes
   * @return
   */
  @Transactional(readOnly = true)
  public List<QuejaEntity> queryAllCliente() {
    try {
      return repo.findByIdClienteNotNull();
    } catch (Exception e) {
      logger.error("Error al obtener quejas: " + e.getMessage());
      return null;
    }
  }

  /**
   * Lista de quejas generadas por clientes; para nutrir notificaciones en Web
   * @return
   */
  @Transactional(readOnly = true)
  public List<QuejaEntity> queryLimitCliente() {
    try {
      return repo.findLimitAsignadasCliente();
    } catch (Exception e) {
      logger.error("Error al obtener quejas: " + e.getMessage());
      return null;
    }
  }

  /**
   * Lista de quejas generadas por despachadores
   * @return
   */
  @Transactional(readOnly = true)
  public List<QuejaEntity> queryAllDespachador() {
    try {
      return repo.findByIdDespachadorNotNull();
    } catch (Exception e) {
      logger.error("Error al obtener quejas: " + e.getMessage());
      return null;
    }
  }

  /**
   * Lista de quejas generadas por clientes; para nutrir notificaciones en Web
   * @return
   */
  @Transactional(readOnly = true)
  public List<QuejaEntity> queryLimitDespachador() {
    try {
      return repo.findLimitAsignadasDespachador();
    } catch (Exception e) {
      logger.error("Error al obtener quejas: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de queja por identificador
   * @param id
   * @return
   */
  @Transactional(readOnly = true)
  public QuejaEntity queryById(Long id) {
    try {
      Optional<QuejaEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una queja: " + e.getMessage());
      return null;
    }
  }

  /**
   * Lista de quejas generadas por un cliente determinado
     * @param idCliente
   * @return
   */
  @Transactional(readOnly = true)
  public List<QuejaEntity> queryByCliente(BigInteger idCliente) {
    try {
      return repo.findByIdCliente(idCliente);
    } catch (Exception e) {
      logger.error("Error al obtener quejas: " + e.getMessage());
      return null;
    }
  }

  /**
   * Lista de quejas atendidas por un usuario determinado
     * @param idUsuario
   * @return
   */
  @Transactional(readOnly = true)
  public List<QuejaEntity> queryByUsuario(Long idUsuario) {
    try {
      return repo.findByIdDespachador(idUsuario);
    } catch (Exception e) {
      logger.error("Error al obtener quejas: " + e.getMessage());
      return null;
    }
  }
  
  /**
   * Lista de quejas generadas por un cliente determinado
     * @param queja
   * @return
   */
  @Transactional
  public boolean create(QuejaEntity queja) {
    if (exists(queja)) {
      logger.error("Trata de añadir una queja existente - ID: " + queja.getIdQueja());

      return false;
    }

    // Se establece el estado y la fecha de creación
    queja.setFechaCreacion(Calendar.getInstance());
    queja.setEstatus(QuejaEstatus.ASIGNADA);

    return save(queja);
  }


  /**
   * Establece ciertos valores de las quejas, asociadas al pedido.
   * @param queja
   * @param response
   * @return
   */
  @Transactional
  public boolean inferencePedido(QuejaEntity queja, RestResponse<?> response) {
    try{
      Optional<Pedido> optional = pedidoRepository.findById(queja.getIdPedido());

      if(!optional.isPresent()){
        logger.error("Trata de crear una queja sin pedido - ID: " + queja.getIdPedido());
        response.setSuccess(false);
        response.setMessage("No existe el pedido");

        return false;
      }

      Pedido pedido = optional.get();

      // Se verifica que el pedido corresponda al cliente; si este existe
      if(queja.getIdCliente() != null && pedido.getCliente().getIdCliente().longValue() != queja.getIdCliente().longValue() ){
        // TODO: Auditoría
        response.setSuccess(false);
        response.setMessage("No existe el pedido");

        return false;
      }
      queja.setIdPedido(pedido.getIdPedido());



      return true;
    } catch(Exception e) {

      response.setSuccess(false);
      response.setMessage("No existe el pedido");

      logger.error("Error en el pedido de la queja - ID: " + queja.getIdPedido());

      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * Indica si una queja existe en base de datos
   * @param queja
   * @return
   */
  @Transactional(readOnly = true)
  private boolean exists(QuejaEntity queja) {
    return queja != null && queja.getIdQueja() != null && exists(queja.getIdQueja());
  }

  /**
   * Indica si un identificador de queja existe en base de datos
   * @param id
   * @return
   */
  @Transactional(readOnly = true)
  public boolean exists(Long id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una queja: " + e.getMessage());
      return false;
    }
  }

  /**
   * Cuenta la cantidad de quejas asignadas (sin atender) generadas por clientes
   * @return
   */
  @Transactional(readOnly = true)
  public Long asignadosCliente() {
    try {
      return repo.countByEstatusAndIdClienteNotNull(QuejaEstatus.ASIGNADA);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una queja: " + e.getMessage());
      return 0L;
    }
  }

  /**
   * Cuenta la cantidad de quejas asignadas (sin atender) generadas por despachadores
   * @return
   */
  @Transactional(readOnly = true)
  public Long asignadosDespachador() {
    try {
      return repo.countByEstatusAndIdDespachadorNotNull(QuejaEstatus.ASIGNADA);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una queja: " + e.getMessage());
      return 0L;
    }
  }

  /**
   * Guarda una queja, aplica para actualización y creación
   * @param queja
   * @return
   */
  @Transactional
  private boolean save(QuejaEntity queja) {
    try {
      repo.save(queja);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una queja: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Añade un asunto a la queja
   * @param idQueja
   * @param asunto
   * @return
   */
  @Transactional
  public boolean addAsunto(Long idQueja, String asunto) {
    try {
      QuejaDetalleEntity detalle = new QuejaDetalleEntity();

      detalle.setFechaCreacion(Calendar.getInstance());
      detalle.setIdQueja(idQueja);
      detalle.setTexto(asunto);
      detalle.setTipo(QuejaTipoDetalle.ASUNTO);

      detalleRepo.save(detalle);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una queja: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Añade una respuesta a la queja
   * @param idQueja
   * @param respuesta
   * @return
   */
  @Transactional
  public boolean addRespuesta(Long idQueja, String respuesta) {
    try {
      QuejaDetalleEntity detalle = new QuejaDetalleEntity();

      detalle.setFechaCreacion(Calendar.getInstance());
      detalle.setIdQueja(idQueja);
      detalle.setTexto(respuesta);
      detalle.setTipo(QuejaTipoDetalle.RESPUESTA);

      detalleRepo.save(detalle);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una queja: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Historial de quejas 
   * @param idQueja
   * @return
   */
  @Transactional
  public List<QuejaDetalleEntity> historial(Long idQueja) {
    try {
      return detalleRepo.findByIdQueja(idQueja);
    } catch (Exception e) {
      logger.error("Error al guardar una queja: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  /**
   * Actualización de una queja
   * @param queja
   * @return
   */
  @Transactional
  public boolean update(QuejaEntity queja) {
    if (!exists(queja)) {
      logger.error("Trata de actualizar una queja inexistente - ID: " + queja.getIdQueja());

      return false;
    }

    return save(queja);
  }


  // PART: Azure Blob Storage

  /**
   * Añade, en Azure, una fotografía de evidencia a la queja
   * @param queja
   * @param file
   * @return
   */
  @Transactional
  public boolean addFoto(QuejaEntity queja, MultipartFile file) {
    try {
      // Se calcula un nombre único de la imagen
      String nombreArchivo = UUID.randomUUID().toString() + "_" +  file.getOriginalFilename().replace(" ", "");

      // Se obtiene el BlobContainer de Azure
      BlobContainerClient blobContainerClient = azureContainer();

      // Se carga como bloque, es indistinto para imágenes tan pequeñas
      // https://docs.microsoft.com/es-mx/rest/api/storageservices/understanding-block-blobs--append-blobs--and-page-blobs
      BlockBlobClient evidencia = blobContainerClient.getBlobClient(nombreArchivo).getBlockBlobClient();
      evidencia.upload(file.getInputStream(), file.getSize());

      // Registro de la evidencia en la base de datos
      QuejaEvidenciaEntity registro = new QuejaEvidenciaEntity();
      registro.setIdQueja(queja.getIdQueja());
      registro.setNombre(nombreArchivo);
      evidenciaRepo.save(registro);

      return true;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Descarga, desde Azure, una fotografía de evidencia de queja
   * @param idEvidencia
   * @return
   */
  public InputStreamResource downloadFoto(Long idEvidencia) {
    try {
      Optional<QuejaEvidenciaEntity> optional = evidenciaRepo.findById(idEvidencia);

      if(!optional.isPresent()) {
        return null;
      }

      QuejaEvidenciaEntity evidencia = optional.get();
      BlobContainerClient blobContainerClient = azureContainer();

      // Flujo si la imagen ya había sido cargada
      if (evidencia != null && evidencia.getNombre() != null && !evidencia.getNombre().isEmpty()) {
        ByteArrayInputStream foto = azureInputStream(blobContainerClient, evidencia.getNombre());

        if(foto != null) {
          return new InputStreamResource(foto);
        }
      }

      // Si no se encuentra ninguna imagen
      return null;
    } catch (Exception e) {
      logger.error("Error al descargar la imagen: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  /**
   * Prepara un contenedor de Azure
   * @return
   */
  private BlobContainerClient azureContainer() {
    // Consulta de credenciales de Azure
    AzureBsEntity azureEntity = azureService.queryById(1);

    // Se crea un cliente del Blob Service de Azure
    BlobServiceClient blobServiceClient = new BlobServiceClientBuilder()
      .endpoint(azureEntity.getEndpoint())
      .sasToken(azureEntity.getSas())
      .buildClient();
    
    // Se crea un cliente para el contenedor
    BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(azureEntity.getContainer());
    return blobContainerClient;
  }

  /**
   * Descarga del InputStream de Azure
   * @param blobContainerClient
   * @param blobName
   * @return
   */
  private ByteArrayInputStream azureInputStream(BlobContainerClient blobContainerClient, String blobName) {
    BlockBlobClient fotoActual = blobContainerClient.getBlobClient(blobName).getBlockBlobClient();
        
    if(fotoActual.exists()) {
      // A ByteArrayOutputStream holds the content in memory
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      fotoActual.download(outputStream);
      
      // To convert it to a byte[] - simply use
      final byte[] bytes = outputStream.toByteArray();

      // To convert bytes to an InputStream, use a ByteArrayInputStream
      ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
      
      return inputStream;
    } else {
      return null;
    }
  }


  // PART: Apartado de métodos de tranformación

  /**
   * Transformación de la queja a un formato válido para dispositivos móviles
   * @param queja
   * @return
   */
  public QuejaMovil mapMovil(QuejaEntity queja) {
    // Verificación de nulabilidad
    if(queja == null) {
      return null;
    }

    // Obtención de la "conversación" generada por la queja
    List<QuejaDetalleEntity> detalles = historial(queja.getIdQueja());
    ArrayList<QuejaMovil.Detalle> detallesMovil = new ArrayList<>();
    detalles.forEach(detalle -> detallesMovil.add(new QuejaMovil.Detalle(detalle.getFechaCreacion(), detalle.getTipo(), detalle.getTexto())));
    
    // Obtención de las evidencias
    ArrayList<String> evidencias = new ArrayList<>();
    queja.getEvidencias().forEach(evidencia -> evidencias.add("/api/quejas/foto/" + evidencia.getIdEvidencia()));
    
    // Generación de la queja mapeada para móviles
    QuejaMovil movil = new QuejaMovil();
    movil.setIdQueja(      queja.getIdQueja());
    movil.setEstatus(      queja.getEstatus());
    movil.setDetalle(      detallesMovil);
    movil.setEvidencias(   evidencias);
    movil.setFechaCreacion(queja.getFechaCreacion());

    // REQ: Pablo Pérez - Obtener datos extendidos del pedido
    QuejaMovil.Pedido pedido = new QuejaMovil.Pedido();
    
    // Se establecen los datos del pedido
    pedido.setIdPedido(     queja.getPedido().getIdPedido());
    pedido.setIdRuta(       queja.getPedido().getIdRuta());
    pedido.setIdAutomovil(  queja.getPedido().getIdAutomovil());
    pedido.setIdCombustible(queja.getPedido().getIdCombustible());
    pedido.setIdServicio(   queja.getPedido().getIdServicio());
    pedido.setLitroCarga(   queja.getPedido().getLitroCarga());
    pedido.setMontoCarga(   queja.getPedido().getMontoCarga());
    pedido.setFechaCreacion(queja.getPedido().getFechaCreacion());
    pedido.setFechaPedido(  queja.getPedido().getFechaPedido());
    pedido.setIdPromocion(  queja.getPedido().getIdPromocion());
    pedido.setCargaGas(     queja.getPedido().isCargaGas());
    pedido.setLavadoAuto(   queja.getPedido().isLavadoAuto());
    pedido.setIdCliente(    queja.getPedido().getCliente().getIdCliente());
    pedido.setEstatusPedido(queja.getPedido().getEstatusPedido());
    pedido.setDetalle(      queja.getPedido().getDetalle());

    // REQ: Pablo Pérez - Agregar litros despachados
    pedido.setLitrosDespachados(queja.getPedido().getLitrosDespachados());

    // Revisión de neumáticos
    // pedido.setVerificaPresionNeumatico(queja.getPedido().isVerificaPresionNeumatico());
    pedido.setRevisionNeumaticos(      queja.getPedido().isRevisionNeumaticos());
    pedido.setLlantasDelanterasPresion(queja.getPedido().getLlantasDelanterasPresion());
    pedido.setLlantasTraserasPresion(  queja.getPedido().getLlantasTraserasPresion());
    
    // REQ: Rogelio Bernal - Agregar información adicional del pedido
    pedido.setTotalPagar(queja.getPedido().getTotalPagar());
    pedido.setUuid(queja.getPedido().getUuid());
    
    // Dirección del cliente
    try {
      Optional<DireccionCliente> optionalDir = direccionClienteRepository.findById(queja.getPedido().getIdDireccion());

      if(optionalDir.isPresent()) {
        pedido.setDireccion(optionalDir.get());
      }
    } catch(Exception e) {
      // TODO: Logger
    }

    // REQ: Alejandro Frías
    if(pedido.getIdRuta() != null) {
      RutaEntity ruta = rutaService.queryById(pedido.getIdRuta());

      if(ruta != null) {
        pedido.setRutaNombre(ruta.getNombre());
      }
    }

    movil.setPedido(pedido);

    return movil;
  }

  /**
   * Transformación de una lista de quejas a un formato válido para dispositivos móviles
   * @param quejas
   * @return
   */
  public List<QuejaMovil> mapMovil(List<QuejaEntity> quejas) {
    // Verificación de nulabilidad
    if(quejas == null) {
      return null;
    }

    // Recolección de las quejas
    ArrayList<QuejaMovil> movil = new ArrayList<>();
    quejas.forEach(queja -> movil.add(mapMovil(queja)));
    return movil;
  }

  /**
   * Transformación de la queja a un formato válido para aplicación Web
   * @param queja
   * @return
   */
  public QuejaWeb mapWeb(QuejaEntity queja) {
    // Verificación de nulabilidad
    if(queja == null) {
      return null;
    }

    // Generación de la queja mapeada para web
    QuejaWeb web = new QuejaWeb();
    web.setIdQueja(      queja.getIdQueja());
    web.setFechaCreacion(queja.getFechaCreacion());
    web.setEstatus(      queja.getEstatus());
    web.setUsuario(      queja.getUsuario());
    web.setDespachador(  queja.getDespachador());
    web.setCliente(      queja.getCliente());
    web.setEvidencias(   queja.getEvidencias());
    web.setDetalle(      historial(queja.getIdQueja()));

    // Si existe un despachador asociado al pedido, se añade
    if(queja.getPedido() != null && queja.getPedido().getIdDespachador() != null){
      web.setAtendedor(usuarioService.queryById(queja.getPedido().getIdDespachador().longValue()));
    }

    return web;
  }

  /**
   * Transformación de una lista de quejas a un formato válido para aplicación web
   * @param quejas
   * @return
   */
  public List<QuejaWeb> mapWeb(List<QuejaEntity> quejas) {
    // Verificación de nulabilidad
    if(quejas == null) {
      return null;
    }

    // Recolección de las quejas
    ArrayList<QuejaWeb> web = new ArrayList<>();
    quejas.forEach(queja -> web.add(mapWeb(queja)));
    return web;
  }

    /**
     *
     * @param cliente
     * @param queja
     * @param calificada
     */
    public void notifyCliente(Cliente cliente, QuejaEntity queja, boolean calificada) {
    notify(cliente, queja, calificada);
  }

    /**
     *
     * @param despachador
     * @param queja
     * @param calificada
     */
    public void notifyDespachador(UsuarioEntity despachador, QuejaEntity queja, boolean calificada) {
    notify(despachador, queja, calificada);
  }

  private void notify(Object uc, QuejaEntity queja, boolean calificada) {
    String subject      = "FAST GAS RECLAMACIONES";
    String title        = "";
    String generalData  = "";
    String tableData    = "";
    String conversacion = "";

    // Se determinan los datos dependidendo del estatus de la queja
    if(queja.getEstatus() == QuejaEstatus.ASIGNADA && !calificada) {
      title       = "Tu reclamo será procesado a la brevedad";
      generalData = "Tu queja se ha guardado correctamente con el folio " + 
        queja.getIdQueja() +
        "; en breve te estará contactando un asesor para brindarte atención.";
    } else if(queja.getEstatus() == QuejaEstatus.ASIGNADA && calificada) {
      title       = "Has dado un comentario insatisfactorio a la resolución de tu queja";
      generalData = "Se ha asignado nuevamente tu queja; en breve te contactará un asesor.";
    } else if(queja.getEstatus() == QuejaEstatus.ATENDIDA) {
      title       = "Contestación de reclamo generada";
      generalData = "Se ha dado contestación a tu queja con folio " + 
        queja.getIdQueja() +
        ", favor de verificar el comentario completo en la sección de Quejas de tu App.";
    } else if(queja.getEstatus() == QuejaEstatus.CERRADA  && !calificada) {
      title       = "Reclamación solucionada";
      generalData = "Estamos para servirte, tu queja con folio" + queja.getIdQueja() + ", ha quedado cerrada.";
    } else if(queja.getEstatus() == QuejaEstatus.CERRADA && calificada) {
      title       = "Has dado un comentario satisfactorio a la resolución de tu queja";
      generalData = "Tu queja con el folio" + queja.getIdQueja() + ", ha quedado cerrada satisfactoriamente.";
    }

    // Fecha de creación
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    dateFormat.setTimeZone(queja.getFechaCreacion().getTimeZone());
    String fechaCreacion = dateFormat.format(queja.getFechaCreacion().getTime());

    // Datos de la tabla:
    ArrayList<SmtpGenerico.BasicTabulator> quejaTabs = new ArrayList<>();
    quejaTabs.add(new SmtpGenerico.BasicTabulator("Id de servicio:", queja.getIdPedido().toString()));
    quejaTabs.add(new SmtpGenerico.BasicTabulator("Folio de queja:", queja.getIdQueja().toString()));
    quejaTabs.add(new SmtpGenerico.BasicTabulator("Fecha de levantamiento:", fechaCreacion));

    // Estatus:
    String estatus = "";

    if(queja.getEstatus() == QuejaEstatus.ASIGNADA) {
      estatus = "Asignada";
    } else if(queja.getEstatus() == QuejaEstatus.EN_ATENCION) {
      estatus = "En atención";
    } else if(queja.getEstatus() == QuejaEstatus.ATENDIDA) {
      estatus = "Atendida";
    } else if(queja.getEstatus() == QuejaEstatus.CERRADA) {
      estatus = "Cerrada";
    }
    quejaTabs.add(new SmtpGenerico.BasicTabulator("Estatus", estatus));

    tableData = smtp.table("Datos de la queja", quejaTabs);

    // Datos de las conversaciones:
    ArrayList<SmtpGenerico.BasicTabulator> detalleTabs = new ArrayList<>();
    List<QuejaDetalleEntity> detalle = historial(queja.getIdQueja());

    for(QuejaDetalleEntity det : detalle) {
      if(det.getTipo() == QuejaTipoDetalle.ASUNTO) {
        detalleTabs.add(new SmtpGenerico.BasicTabulator("Asunto:", det.getTexto()));
      } else {
        detalleTabs.add(new SmtpGenerico.BasicTabulator("Respuesta:", det.getTexto() + smtp.br()));
      }
    }

    conversacion = smtp.lines("Asuntos y respuestas", detalleTabs);

    if(uc instanceof Cliente) {
      Cliente cliente = (Cliente)uc;

      smtp.sendEmail(subject, cliente.getCorreo(), title, smtp.join(smtp.text(generalData), tableData, conversacion));

      if(queja.getEstatus() != QuejaEstatus.ASIGNADA) {
        notification.notificar(cliente, "FastGas Aclaraciones", generalData);
      }
    } else if(uc instanceof UsuarioEntity) {
      UsuarioEntity despachador = (UsuarioEntity)uc;

      smtp.sendEmail(subject, despachador.getCorreo(), title, smtp.join(smtp.text(generalData), tableData, conversacion));

      if(queja.getEstatus() != QuejaEstatus.ASIGNADA) {
        notification.notificar(despachador, "FastGas Aclaraciones", generalData);
      }
    }
  }
}
