package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.dto.RolDto;
import com.enermex.repository.RolRepositoryA;

/**
 *
 * @author abraham
 */
@Service
public class RolServiceA {
	
	
	@Autowired
	private RolRepositoryA rolRepository;
	
    /**
     *
     * @return
     */
    public List<RolDto> getAll(){
		
		return rolRepository.findAll();
	
	}

}
