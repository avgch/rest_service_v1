package com.enermex.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import com.enermex.dto.seguimiento.PedidoWeb;
import com.enermex.enumerable.PedidoEvidenciaMomento;
import com.enermex.modelo.CalificaPedido;
import com.enermex.modelo.CodigoPostalEntity;
import com.enermex.modelo.MunicipioEntity;
import com.enermex.modelo.MunicipioId;
import com.enermex.modelo.SeguimientoPedidoEntity;
import com.enermex.modelo.PedidoEvidenciaEntity;
import com.enermex.modelo.SeguimientoPedidoEntity.SRutaEntity;
import com.enermex.repository.CalificaPedidoRepository;
import com.enermex.repository.CodigoPostalRepository;
import com.enermex.repository.MunicipioRepository;
import com.enermex.repository.PedidoEvidenciaRepository;
import com.enermex.repository.SeguimientoRepository;
import com.enermex.repository.SeguimientoRutaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * SeguimientoService
 */
@Service
public class SeguimientoService {
  @Autowired
  private SeguimientoRepository repo;

  @Autowired
  private PedidoEvidenciaRepository evidenciaRepo;
  
  @Autowired
  private CalificaPedidoRepository calificaRepo;
  
  @Autowired
  private PedidoFacturaService facturaService;

  @Autowired
  private CodigoPostalRepository cpRepo;

  @Autowired
  private MunicipioRepository municipioRepo;

  @Autowired
  private SeguimientoRutaRepository rutaRepo;

    /**
     *
     * @param inicio
     * @param fin
     * @param ruta
     * @param estatus
     * @return
     */
    public Long count(Calendar inicio, Calendar fin, Integer ruta, Integer estatus) {
    try {
      if(ruta != null && estatus != null) {
        return repo.countByFechaPedidoBetweenAndIdRutaAndEstatusPedido(inicio, fin, ruta, estatus);
      } else if(ruta != null) {
        return repo.countByFechaPedidoBetweenAndIdRuta(inicio, fin, ruta);
      } else if(estatus != null) {
        return repo.countByFechaPedidoBetweenAndEstatusPedido(inicio, fin, estatus);
      } else {
        return repo.countByFechaPedidoBetween(inicio, fin);
      }
    } catch (Exception e) {
      // TODO: logger
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param inicio
     * @param fin
     * @param ruta
     * @param estatus
     * @param pageable
     * @return
     */
    public List<SeguimientoPedidoEntity> queryPage(Calendar inicio, Calendar fin, Integer ruta, Integer estatus, Pageable pageable) {
    try {
      if(ruta != null && estatus != null) {
        return repo.findByFechaPedidoBetweenAndIdRutaAndEstatusPedidoOrderByIdPedidoAsc(inicio, fin, ruta, estatus, pageable);
      } else if(ruta != null) {
        return repo.findByFechaPedidoBetweenAndIdRutaOrderByIdPedidoAsc(inicio, fin, ruta, pageable);
      } else if(estatus != null) {
        return repo.findByFechaPedidoBetweenAndEstatusPedidoOrderByIdPedidoAsc(inicio, fin, estatus, pageable);
      } else {
        return repo.findByFechaPedidoBetweenOrderByIdPedidoAsc(inicio, fin, pageable);
      }
    } catch (Exception e) {
      // TODO: logger
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    public SeguimientoPedidoEntity queryById(BigInteger id) {
    try {
      Optional<SeguimientoPedidoEntity> optional = repo.findById(id);
      
      if(optional.isPresent()) {
        return optional.get();
      } else {
        return null;
      }
    } catch (Exception e) {
      // TODO: logger
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @return
     */
    public List<SRutaEntity> queryRutas() {
    try {
      return rutaRepo.findAll();
    } catch (Exception e) {
      e.printStackTrace();
      //TODO: handle exception

      return null;
    }
  }

    /**
     *
     * @param pedido
     * @return
     */
    public PedidoWeb mapWeb(SeguimientoPedidoEntity pedido) {
    if(pedido == null) {
      return null;
    }

    PedidoWeb web = new PedidoWeb();
    web.setIdPedido(pedido.getIdPedido());
    web.setUuid(pedido.getUuid());
    web.setIdCombustible(pedido.getIdCombustible());
    web.setIdServicio(pedido.getIdServicio());
    web.setLitroCarga(pedido.getLitroCarga());
    web.setMontoCarga(pedido.getMontoCarga());
    web.setVerificaPresionNeumatico(pedido.isRevisionNeumaticos());
    web.setLlantasDelanterasPresion(pedido.getLlantasDelanterasPresion());
    web.setLlantasTraserasPresion(pedido.getLlantasTraserasPresion());
    web.setFechaCreacion(pedido.getFechaCreacion());
    web.setFechaPedido(pedido.getFechaPedido());
    web.setCargaGas(pedido.isCargaGas());
    web.setLavadoAuto(pedido.isLavadoAuto());
    web.setRevisionNeumaticos(pedido.isRevisionNeumaticos());
    web.setEstatusPedido(pedido.getEstatusPedido());
    web.setDetalle(pedido.getDetalle());
    web.setSubTotal(pedido.getSubTotal());
    // web.setTotal(pedido.getTotal());
    web.setIva(pedido.getIva());
    web.setTotalPagar(pedido.getTotalPagar());
    web.setDescuento(pedido.getDescuento());
    web.setTiempoTraslado(pedido.getTiempoTraslado());
    web.setFechaHoraInicio(pedido.getFechaHoraInicio());
    web.setFechaHoraFin(pedido.getFechaHoraFin());
    
    web.setCostoServicio(pedido.getCostoServicio());
    web.setPrecioLavadoAuto(pedido.getPrecioLavadoAuto());
    web.setPrecioPresionNeumaticos(pedido.getPrecioPresionNeumaticos());
    web.setPrecioPorLitroFinal(pedido.getPrecioPorLitroFinal());
    web.setLitrosDespachados(pedido.getLitrosDespachados());
    web.setPrecioPorLitro(pedido.getPrecioPorLitro());
    
    
    // Datos de la dirección
    if(pedido.getDireccion() != null) {
      web.setDireccion(pedido.getDireccion());

      String cp = web.getDireccion() != null ? web.getDireccion().getCp() : null;
      if(cp != null){
        Optional<CodigoPostalEntity> optional = cpRepo.findById(cp);
        
        if(optional.isPresent()){
          CodigoPostalEntity codigo = optional.get();
          
          MunicipioId id = new MunicipioId();
          id.setIdEstado(codigo.getIdEstado());
          id.setIdMunicipio(codigo.getIdMunicipio());
          
          Optional<MunicipioEntity> optionalMun = municipioRepo.findById(id);
          web.setMunicipio(optionalMun.isPresent() ? optionalMun.get().getNombre() : "");
        }
      }
    }
    
    // Datos de la ruta
    if(pedido.getRuta() != null) {
      web.setIdRuta(pedido.getRuta().getIdRuta());
      web.setRutaNombre(pedido.getRuta().getNombre() != null ? pedido.getRuta().getNombre() : "");
    }
    
    // Cancelación
    if(pedido.getCancelacion() != null) {
      web.setCancelaMotivo(pedido.getCancelacion().getMotivo() != null ? pedido.getCancelacion().getMotivo().getMotivo() : "");      
      web.setCancelaComentario(pedido.getCancelacion().getComentario());      
    }
    
    // Datos del despachador
    web.setDespachador(pedido.getDespachador());
    
    // Datos de la promoción
    web.setIdPromocion(    pedido.getPromocion() != null ? pedido.getPromocion().getIdPromocion()     : null);
    web.setCodigoPromocion(pedido.getPromocion() != null ? pedido.getPromocion().getCodigoPromocion() : null);
    
    // Datos del cliente
    web.setCliente(pedido.getCliente());

    // Datos del autotanque
    web.setPipa(pedido.getPipa());

    // Datos de calificaciones
    try {
      List<CalificaPedido> calificaciones = calificaRepo.findByIdPedido(pedido.getIdPedido());

      if(calificaciones != null) {
        for(CalificaPedido calificacion : calificaciones) {
          if(calificacion.getCalifica().equalsIgnoreCase("Cliente")) {
            web.setCalificacionCliente(calificacion.getCalificacion());
          } else if(calificacion.getCalifica().equalsIgnoreCase("Despachador")) {
            web.setCalificacionDespachador(calificacion.getCalificacion());
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      // TODO: Logger
    }

    // Datos de factura
    web.setFactura(facturaService.queryByIdPedido(pedido.getIdPedido()));

    // Datos de la tarjeta
    if(pedido.getTarjeta() != null) {
      web.setTarjeta(pedido.getTarjeta().getNumeroTarjeta());
    }

    List<PedidoEvidenciaEntity> evidencias = evidenciaRepo.findByIdPedido(pedido.getIdPedido());

    evidencias.forEach(evidencia -> {
      if(evidencia.getMomento() == PedidoEvidenciaMomento.ANTES){
        web.getEvidenciasAntes().add("/api/pedidos/evidencias/" + evidencia.getIdEvidencia());
      } else {
        web.getEvidenciasDespues().add("/api/pedidos/evidencias/" + evidencia.getIdEvidencia());
      }
    });

    return web;
  }

    /**
     *
     * @param pedidos
     * @return
     */
    public List<PedidoWeb> mapWeb(List<SeguimientoPedidoEntity> pedidos) {
    if(pedidos == null) {
      return null;
    }

    ArrayList<PedidoWeb> web = new ArrayList<>();
    pedidos.forEach(pedido -> web.add(mapWeb(pedido)));

    return web;
  }
}