package com.enermex.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import com.enermex.dto.reporte.ReporteServicio;
import com.enermex.modelo.ClienteTipoPlan;
import com.enermex.repository.ClienteNombreRepository;
import com.enermex.repository.ClienteTipoPlanRepository2;
import com.enermex.repository.ServicioReporteRepository;
import com.enermex.view.ClienteNombreView;
import com.enermex.view.ServicioReporteView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author abraham
 */
@Service
public class ServicioReporteService {

  @Autowired
  ServicioReporteRepository repo;

  @Autowired
  ClienteNombreRepository planRepo;

  private List<ServicioReporteView> queryByDate(Calendar inicio, Calendar fin) {
    try {
      return repo.findByFechaPedidoGreaterThanEqualAndFechaPedidoLessThanEqual(inicio, fin);
    } catch (Exception e) {
      e.printStackTrace();

      //TODO: Logger
      return null;
    }
  }

    /**
     *
     * @param filtro
     * @return
     */
    public List<ServicioReporteView> queryByFilter(ReporteServicio filtro) {
    Calendar   inicio       = filtro.getInicio();
    Calendar   fin          = filtro.getFin();
    Integer    plan         = filtro.getPlan();
    BigInteger idCliente    = filtro.getIdCliente();
    Integer    idEstado     = filtro.getIdEstado();
    Integer    idMunicipio  = filtro.getIdMunicipio();
    String     cp           = filtro.getCp();
    Integer    estatus      = filtro.getEstatus();
    Boolean    hasLavado    = filtro.getHasLavado();
    Boolean    hasPresion   = filtro.getHasPresion();
    Integer    idTipoPedido = filtro.getIdTipoPedido();

    List<ServicioReporteView> servicios = queryByDate(inicio, fin);
    
    if(servicios == null){
      return null;
    }
    
    ArrayList<ServicioReporteView> filtrados = new ArrayList<>();
    
    for(ServicioReporteView servicio : servicios) {
      // System.out.println(servicio.getUuid());
      // Filtrar por cliente
      if(idCliente != null && idCliente.compareTo(servicio.getIdCliente()) != 0) {
        // System.out.println("idCliente");
        continue;
      }

      // Filtrar por estado
      if(idEstado != null && idEstado.intValue() != servicio.getIdEstado().intValue()) {
        // System.out.println("idEstado");
        continue;
      }

      // Filtrar por municipio
      if(idMunicipio != null && idMunicipio.intValue() != servicio.getIdMunicipio().intValue()) {
        // System.out.println("idMunicipio");
        continue;
      }

      // Filtrar por código postal
      if(cp != null && !cp.equals(servicio.getCpDireccion())) {
        // System.out.println("cp");
        continue;
      }

      // Estatus
      if(estatus != null && estatus.intValue() != servicio.getEstatus().intValue()) {
        // System.out.println("estatus");
        continue;
      }

      // Filtrar por plan
      if(idCliente == null && plan != null) {
        try {
          Optional<ClienteNombreView> clienteOptional = planRepo.findById(servicio.getIdCliente());

          if(!clienteOptional.isPresent()) {
            continue;
          }

          ClienteNombreView clienteNombre = clienteOptional.get();

          if(plan.intValue() == 1 && !(clienteNombre.getIdTitular() == null && clienteNombre.getIdFamiliar() == null)) {
            continue;
          }
          if(plan.intValue() == 2 && !(clienteNombre.getIdTitular() != null || clienteNombre.getIdFamiliar() != null)) {
            continue;
          }
          if(plan.intValue() == 3) {
            continue;
          }
        } catch (Exception e) {
          //TODO: handle exception
          e.printStackTrace();
        }
      }

      if(hasLavado != null && hasLavado.booleanValue() != servicio.getHasLavado().booleanValue()) {
        // System.out.println("hasLavado");
        continue;
      }

      if(hasPresion != null && hasPresion.booleanValue() != servicio.getHasPresion().booleanValue()) {
        // System.out.println("hasPresion");
        continue;
      }

      if(idTipoPedido != null && idTipoPedido.intValue() != servicio.getIdTipoPedido().intValue()) {
        // System.out.println("idTipoPedido");
        continue;
      }

      // Agregar el servicio
      filtrados.add(servicio);
    }

    return filtrados;
  }
}