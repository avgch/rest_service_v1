package com.enermex.service;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.NotificacionRepository;
import com.enermex.repository.impl.CodigoPostalRepositoryImpl;
import com.enermex.repository.impl.ClienteRepositoryImpl;
import com.enermex.repository.impl.UsuarioRepositoryImpl;
import com.enermex.repository.NotificacionClienteRepository;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.enermex.dto.AlcaldiaEstadoNotiDto;
import com.enermex.dto.NotificaDespachadorDto;
import com.enermex.dto.NotificacionDto;
import com.enermex.modelo.Notificacion;
import com.enermex.modelo.NotificacionCliente;

/**
 *
 * @author abraham
 */
@Service
public class NotificacionService {
	
	@Autowired
	NotificacionRepository notificacionRepository;
	
	@Autowired
	ClienteFcmService clienteFcmService;
	
	@Autowired
	CodigoPostalRepositoryImpl codigoPostalRepositoryImpl;
	
	@Autowired
	ClienteRepositoryImpl clienteRepositoryImpl;

	@Autowired
	UsuarioRepositoryImpl usuarioRepositoryImpl;
	
	@Autowired
	NotificacionClienteRepository notificacionClienteRepository;
	
	/**
	 * Guarda notificacion
	 * @param notifica
	 * @throws FirebaseMessagingException
	 */
	public void save (NotificacionDto notifica) throws FirebaseMessagingException {
		
		Notificacion noti = new Notificacion(notifica);
		List<Long> idDesp ;
		List<BigInteger> clientesIds=null;
		List<NotificacionCliente> notiClientes = new ArrayList<>();
		NotificacionCliente notiCliente;
 		noti.setCreadoPor(notifica.getCreadoPor());
	    //notifica por zonas
		if(notifica.getEsCliente().equalsIgnoreCase("true")) {
			noti.setEnviadaA("Clientes por zona");
			clientesIds=customerToNotify(notifica.getAreasANotificas(),notifica.getTitulo(),notifica.getMensaje());
			notificacionRepository.save(noti);
			
			if(clientesIds!=null) {
			    for(BigInteger id : clientesIds) {
			    	notiCliente = new NotificacionCliente(id,noti.getIdNotificacion());
			    	notiClientes.add(notiCliente);
			    }
			    notificacionClienteRepository.saveAll(notiClientes);
				
			}
			
		}
			
		//notifica a despachadores
		if(notifica.getEsDespachador().equalsIgnoreCase("true")) {
			idDesp = new ArrayList<>();
			for(NotificaDespachadorDto d: notifica.getNotificaDespachadores()) {
				idDesp.add(d.getIdUsuario());
				
			}
			
			noti.setEnviadaA("Despachadores");
		    notifyAllDespachadorApp(notifica.getTitulo(),notifica.getMensaje(),idDesp);
		    notificacionRepository.save(noti);
		}
			
		//notifica a todos los clientes
		if(notifica.getEsTodosClientes().equalsIgnoreCase("true")) {
			notifyAllClientApp(notifica.getTitulo(),notifica.getMensaje());
			noti.setEnviadaA("Clientes");
			notificacionRepository.save(noti);
		}
			
		
	}
	/**
	 * Obtenida
	 * @return
	 */
	public List<Notificacion> getAll(){
		
		return notificacionRepository.getNotificaciones();
	}
	
	/**
	 * Author Ing. Abraham Vargas Garcia
	 * @return
	 */
	public List<Notificacion> getAllNotiDespachadores(){
		
		return notificacionRepository.getNotificacionesDespachadores();
	}
	/**
	 * Author Ing. Abraham Vargas Garcia
     * @param idCliente
	 * @return
	 */
	public List<Notificacion> getAllNotiClientes(BigInteger idCliente){
		
		List<Notificacion> notificaciones;
		List<Notificacion> notificacionesSegmetadas = new ArrayList<>();
		List<BigInteger> idNotificaciones;
		Notificacion noti;
		notificaciones=notificacionRepository.getNotificacionesClientes();
		
		//consultamos las que han sido enviadas solo a el cliente
		idNotificaciones=notificacionClienteRepository.getAllNotiClienteById(idCliente);
		
		for(BigInteger id : idNotificaciones) {
			noti = notificacionRepository.getNotificacionesByIdCliente(id);
			notificacionesSegmetadas.add(noti);
		}
		notificaciones.addAll(notificacionesSegmetadas);
		Collections.sort(notificaciones);
		
		return notificaciones;
	}
	

	/**
	 * Notifica a clientes por zona
	 * @param areasANotificas
	 * @throws FirebaseMessagingException
	 */
	private List<BigInteger> customerToNotify(List<AlcaldiaEstadoNotiDto> areasANotificas,String titulo,String body ) throws FirebaseMessagingException{
		
		List<Integer> alcMunc = new ArrayList<>();
		List<String> tokens;
		
		for(AlcaldiaEstadoNotiDto o : areasANotificas) {
			
			alcMunc.add(o.getIdAlcaldia());
		}
		
		
		List<String> cps= codigoPostalRepositoryImpl.notificarA(alcMunc);
		
		
		System.out.println(cps);
		//obtengo los cps participantes 
		
		//obtengo mi lista de tokens participantes con base al cp
		tokens = clienteRepositoryImpl.getAllTokensNotifyNoti(cps);
		
		//NOTIFICAMOS
		
//	    //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();			    
	    
         System.out.println(tokens);	    

	    if(tokens!=null){
		    //Notificacion
	    	if(tokens.size()>0) {
			    Notification.Builder msNot = Notification.builder();
			    
			    msNot.setBody(body);
			    msNot.setTitle(titulo);
			    msg.addAllTokens( (List<String>)(List<?>)tokens);
			    
			    msg.setNotification(msNot.build());
			    
			    FirebaseMessaging fm = FirebaseMessaging.getInstance();
			    
			    fm.sendMulticast(msg.build());
			    
			   return  clienteRepositoryImpl.getAllClinetesNotifyNoti(cps);
	    	}   
	    }
	    
	    return null;
	    
		
	}
	
	/**
	 * Notifica a todos los usuarios activos de la app
	 * @throws FirebaseMessagingException 
	 */
    private void notifyAllClientApp(String titulo,String body) throws FirebaseMessagingException{
		
		
		List<String> tokens = clienteRepositoryImpl.notificaClientesTodos();
		System.out.println(tokens);
		//NOTIFICAMOS
		
//	    //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();			    
	    
	    
	    if(tokens!=null)
	    {
	    	if(tokens.size()>0) {
		    //Notificacion
		    Notification.Builder msNot = Notification.builder();
		    
		    msNot.setBody(body);
		    msNot.setTitle(titulo);
		    msg.addAllTokens( (List<String>)(List<?>)tokens);
		    
		    msg.setNotification(msNot.build());
		    
		    FirebaseMessaging fm = FirebaseMessaging.getInstance();
		    
		    fm.sendMulticast(msg.build());
	    }
	    }
	    
	
	}
    
	/**
	 * Notifica a todos los despachadores activos de la app despachador
	 * @throws FirebaseMessagingException 
	 */
    private void notifyAllDespachadorApp(String titulo,String body,List<Long> ids) throws FirebaseMessagingException{
		
		//Recibimos ids de despachador
		List<String> tokens = usuarioRepositoryImpl.notificaDespachadoresTodos(ids);
		System.out.println(tokens);
		//NOTIFICAMOS
		
//	    //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();			    
	    
	    if(tokens!=null)
	    {
	    	if(tokens.size()>0) {
			    //Notificacion
			    Notification.Builder msNot = Notification.builder();
			    
			    msNot.setBody(body);
			    msNot.setTitle(titulo);
			    msg.addAllTokens( (List<String>)(List<?>)tokens);
			    
			    msg.setNotification(msNot.build());
			    
			    FirebaseMessaging fm = FirebaseMessaging.getInstance();
			    
			    fm.sendMulticast(msg.build());
	    	}
	    }

	
	}	
	

}
