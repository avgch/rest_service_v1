package com.enermex.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.dto.AutomovilClienteDto;
import com.enermex.modelo.AutomovilCliente;
import com.enermex.repository.AutomovilClienteRepository;
import com.enermex.repository.impl.AutomovilClienteRepositoryImpl;

/**
 *
 * @author abraham
 */
@Service
public class AutomovilClienteService {
	
	
	@Autowired
	private AutomovilClienteRepository automovilClienteRepository;
	
	@Autowired
	AutomovilClienteRepositoryImpl automovilClienteRepositoryImpl;

    /**
     *
     * @param idCliente
     * @return
     */
    public List<AutomovilCliente> getAll(BigInteger idCliente){
		
		return automovilClienteRepository.findCarByIdCliente(idCliente);
	}
	
    /**
     *
     * @param id
     */
    public void delete(Integer id) {
		
		
		automovilClienteRepository.delete(id);
	}
	
    /**
     *
     * @param auto
     */
    public void updateClienteCar(AutomovilClienteDto auto) {
		
		automovilClienteRepository.update(auto.getColor(), auto.getPlacas(),auto.getAnio(), auto.getCapacidad(),auto.getTamanioTanque(),auto.getIdCombustible(),auto.getVersion(), auto.getSerie(),auto.getIdAutomovil());
	}
	
    /**
     *
     * @param idAutomovil
     * @return
     */
    public AutomovilCliente buscarAutoClieneteById(Integer idAutomovil) {
		
		
		return automovilClienteRepository.buscaAutoClienteById(idAutomovil);
	}
	
	
	

}
