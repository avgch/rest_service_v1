package com.enermex.service;

import java.util.List;
import java.util.Optional;

import com.enermex.dto.tarifa.TarifaVista;
import com.enermex.modelo.TarifaLavadoEntity;
import com.enermex.modelo.TarifaLavadoHistorialEntity;
import com.enermex.modelo.TarifaPresionEntity;
import com.enermex.modelo.TarifaPresionHistorialEntity;
import com.enermex.repository.TarifaLavadoHistorialRepository;
import com.enermex.repository.TarifaLavadoRepository;
import com.enermex.repository.TarifaPresionHistorialRepository;
import com.enermex.repository.TarifaPresionRepository;
import com.enermex.repository.TarifaVistaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// HACKXXX: Cortar con los códigos postales en versiones posteriores

/**
 * TarifaService
 */
@Service
public class TarifaService {
  @Autowired
  TarifaLavadoRepository lavadoRepo;
  
  @Autowired
  TarifaPresionRepository presionRepo;
  
  @Autowired
  TarifaVistaRepository webRepo;
  
  
  @Autowired
  TarifaLavadoHistorialRepository lavadoHistorialRepo;
  
  @Autowired
  TarifaPresionHistorialRepository presionHistorialRepo;

  /**
   * Obtiene el precio de lavado determinado para el código postal; para realizar pruebas, se envía "100.00" si no existe
   * 
   * @param cp Código postal
   * @return El precio como cadena; por ahora si no se pudo encontrar
   */
  public String queryLavadoPrecioByCp(String cp) {
	  return lavadoRepo.getTarifaLavadoByCp(cp);
  }

  /**
   * Obtiene el precio de revisión de presión de neumáticos determinado para el código postal; para realizar pruebas, se envía "0.00" si no existe
   * 
   * @param cp Código postal
   * @return El precio como cadena; nulo si no se pudo encontrar
   */
  public String queryPresionPrecioByCp(String cp) {
	  return presionRepo.getTarifaPresionByCp(cp);
  }

    /**
     *
     * @param cp
     * @return
     */
    public boolean exists(String cp) {
    try {
      return lavadoRepo.existsById(cp);
    } catch(Exception e) {
      e.printStackTrace();
      // TODO: Logger
      return false;
    }
  }

    /**
     *
     * @return
     */
    public List<TarifaVista> queryAll() {
    try {
      return webRepo.findAll();
    } catch(Exception e) {
      e.printStackTrace();
      // TODO: Logger
      return null;
    }
  }

    /**
     *
     * @param lavado
     * @return
     */
    public boolean saveLavado(TarifaLavadoEntity lavado) {
    try {
      TarifaLavadoHistorialEntity historial = new TarifaLavadoHistorialEntity();
      historial.setPrecio(lavado.getPrecio());
      historial.setIdCodigo(lavado.getIdCodigo());
      historial.setFechaCreacion(lavado.getFechaCreacion());
      
      lavadoHistorialRepo.save(historial);
      lavadoRepo.save(lavado);

      return true;
    } catch(Exception e) {
      e.printStackTrace();
      // TODO: Logger
      return false;
    }
  }

    /**
     *
     * @param presion
     * @return
     */
    public boolean savePresion(TarifaPresionEntity presion) {
    try {
      TarifaPresionHistorialEntity historial = new TarifaPresionHistorialEntity();
      historial.setPrecio(presion.getPrecio());
      historial.setIdCodigo(presion.getIdCodigo());
      historial.setFechaCreacion(presion.getFechaCreacion());
      
      presionHistorialRepo.save(historial);
      presionRepo.save(presion);

      return true;
    } catch(Exception e) {
      e.printStackTrace();
      // TODO: Logger
      return false;
    }
  }

    /**
     *
     * @return
     */
    public List<TarifaPresionHistorialEntity> historialPresion() {
    try {
      return presionHistorialRepo.findAllByOrderByFechaCreacionAsc();
    } catch(Exception e) {
      e.printStackTrace();
      // TODO: Logger
      return null;
    }
  }

    /**
     *
     * @return
     */
    public List<TarifaLavadoHistorialEntity> historialLavado() {
    try {
      return lavadoHistorialRepo.findAllByOrderByFechaCreacionAsc();
    } catch(Exception e) {
      e.printStackTrace();
      // TODO: Logger
      return null;
    }
  }
}
