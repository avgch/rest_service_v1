package com.enermex.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import com.enermex.dto.reporte.ReporteClienteFiltro;
import com.enermex.dto.reporte.ReporteClienteFiltroV2;
import com.enermex.dto.reporte.ReporteClienteLinea;
import com.enermex.dto.reporte.ReporteClienteLineaV2;
import com.enermex.modelo.ClienteTipoPlan;
import com.enermex.repository.AutomovilClienteRepository;
import com.enermex.repository.CantidadServicioClienteRepository;
import com.enermex.repository.ClienteNombreRepository;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.ClienteTipoPlanRepository2;
import com.enermex.repository.ReporteClienteRepository;
import com.enermex.repository.ServicioReporteRepository;
import com.enermex.view.CantidadServicioClienteView;
import com.enermex.view.ClienteNombreView;
import com.enermex.view.ServicioReporteView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class ReporteClienteService {
  @Autowired
  ServicioReporteRepository repo;

  @Autowired
  ClienteNombreRepository planRepo;

  @Autowired ClienteRepository clienteRepo;

  @Autowired
  CantidadServicioClienteRepository cantidadRepo;

  @Autowired
  AutomovilClienteRepository autoRepo;

  @Autowired
  ReporteClienteRepository reporteRepo;

  private List<ServicioReporteView> queryByDate(Calendar inicio, Calendar fin) {
    try {
      return repo.findByFechaPedidoGreaterThanEqualAndFechaPedidoLessThanEqual(inicio, fin);
    } catch (Exception e) {
      e.printStackTrace();

      //TODO: Logger
      return null;
    }
  }

    /**
     *
     * @param filtro
     * @return
     */
    public List<ReporteClienteLinea> queryByFilter(ReporteClienteFiltro filtro){
    Calendar   inicio         = filtro.getInicio();
    Calendar   fin            = filtro.getFin();
    BigInteger idCliente      = filtro.getIdCliente();
    Integer    idEstado       = filtro.getIdEstado();
    Integer    idMunicipio    = filtro.getIdMunicipio();
    String     cp             = filtro.getCp();
    Integer    plan           = filtro.getPlan();
    BigInteger idAutomovil    = filtro.getIdAutomovil();
    Boolean    hasLavado      = filtro.getHasLavado();
    Boolean    hasPresion     = filtro.getHasPresion();
    Integer    idTipoPedido   = filtro.getIdTipoPedido();
    Integer    estatus        = filtro.getEstatus();

    List<ServicioReporteView> servicios = queryByDate(inicio, fin);
    
    if(servicios == null){
      return null;
    }
    
    ArrayList<ReporteClienteLinea> filtrados = new ArrayList<>();
    
    for(ServicioReporteView servicio : servicios) {
      try{
        // Filtrar por cliente
        if(idCliente != null && idCliente.compareTo(servicio.getIdCliente()) != 0) {
          // System.out.println("idCliente");
          continue;
        }

        if(idAutomovil != null && idAutomovil.compareTo(servicio.getIdAutomovil()) != 0) {
          // System.out.println("idCliente");
          continue;
        }

        // Filtrar por estado
        if(idEstado != null && idEstado.intValue() != servicio.getIdEstado().intValue()) {
          // System.out.println("idEstado");
          continue;
        }

        // Filtrar por municipio
        if(idMunicipio != null && idMunicipio.intValue() != servicio.getIdMunicipio().intValue()) {
          // System.out.println("idMunicipio");
          continue;
        }

        // Filtrar por código postal
        if(cp != null && !cp.equals(servicio.getCpDireccion())) {
          // System.out.println("cp");
          continue;
        }

        // Estatus
        if(estatus != null && estatus.intValue() != servicio.getEstatus().intValue()) {
          // System.out.println("estatus");
          continue;
        }

        // Filtrar por plan
        if(idCliente == null && plan != null) {
          try {
            Optional<ClienteNombreView> clienteOptional = planRepo.findById(servicio.getIdCliente());
  
            if(!clienteOptional.isPresent()) {
              continue;
            }
  
            ClienteNombreView clienteNombre = clienteOptional.get();
  
            if(plan.intValue() == 1 && !(clienteNombre.getIdTitular() == null && clienteNombre.getIdFamiliar() == null)) {
              continue;
            }
            if(plan.intValue() == 2 && !(clienteNombre.getIdTitular() != null || clienteNombre.getIdFamiliar() != null)) {
              continue;
            }
            if(plan.intValue() == 3) {
              continue;
            }
          } catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
          }
        }

        if(hasLavado != null && hasLavado.booleanValue() != servicio.getHasLavado().booleanValue()) {
          // System.out.println("hasLavado");
          continue;
        }

        if(hasPresion != null && hasPresion.booleanValue() != servicio.getHasPresion().booleanValue()) {
          // System.out.println("hasPresion");
          continue;
        }

        if(idTipoPedido != null && idTipoPedido.intValue() != servicio.getIdTipoPedido().intValue()) {
          // System.out.println("idTipoPedido");
          continue;
        }

        // Armado de la línea

        // Se añaden los invitados

        ReporteClienteLinea linea = new ReporteClienteLinea();
        linea.setServicio(servicio);
        linea.setFamiliares(clienteRepo.findByIdTitular(servicio.getIdCliente()));
        
        Optional<CantidadServicioClienteView> opt = cantidadRepo.findById(servicio.getIdCliente());
        linea.setCantidadServicios(opt.isPresent() ? opt.get().getCantidad() : null);
        linea.setCantidadAutos(autoRepo.countByIdCliente(servicio.getIdCliente()));

        Optional<ClienteNombreView> clienteOptional = planRepo.findById(servicio.getIdCliente());
        ClienteNombreView clienteNombre = clienteOptional.isPresent() ? clienteOptional.get() : null;
        ArrayList<String> planes = new ArrayList<>();

        if(clienteNombre != null){
          if(clienteNombre.getIdTitular() == null && clienteNombre.getIdFamiliar() == null) {
            planes.add("Plan individual");
          }
          
          if(clienteNombre.getIdTitular() != null || clienteNombre.getIdFamiliar() != null) {
            planes.add("Plan familiar");
          }
        }

        linea.setPlanes(planes);

        filtrados.add(linea);
      } catch(Exception e) {
        e.printStackTrace();
      }
    }

    return filtrados;
  }

    /**
     *
     * @param filtro
     * @return
     */
    @Transactional(readOnly = true)
  public List<ReporteClienteLineaV2> queryByFilterV2(ReporteClienteFiltroV2 filtro){
    Long    idCliente = filtro.getIdCliente();
    Integer plan      = filtro.getPlan();

    List<ReporteClienteLineaV2> lineas = reporteRepo.findAll(filtro);

    lineas.forEach(linea -> {
      try {
        linea.setAutos(autoRepo.countByIdCliente(BigInteger.valueOf(linea.getIdCliente())));
        linea.setFamiliares(clienteRepo.countByIdTitular(BigInteger.valueOf(linea.getIdCliente())));
      } catch (Exception e) {
        e.printStackTrace();
        //TODO: handle exception
      }
    });

    // El filtro del plan solo aplica si no se eligió cliente
    if(idCliente == null && plan != null) {
      ArrayList<ReporteClienteLineaV2> lineas2 = new ArrayList<>();

      if(plan.intValue() == 1) {
        lineas.forEach(linea -> {
          if(linea.getIdTitular() == 0) {
            lineas2.add(linea);
          }
        });
      } else if(plan.intValue() == 2) {
        lineas.forEach(linea -> {
          if(linea.getIdTitular() > 0 || linea.getFamiliares() > 0) {
            lineas2.add(linea);
          }
        });
      }

      return lineas2;

    } else {
      return lineas;
    }
  }
  
}