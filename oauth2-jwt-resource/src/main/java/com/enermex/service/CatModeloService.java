package com.enermex.service;

import java.util.List;
import com.enermex.dto.ModeloAutoDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.modelo.CatModelo;
import com.enermex.repository.CatModeloRepository;

/**
 *
 * @author abraham
 */
@Service
public class CatModeloService {
	
	
	@Autowired
	private CatModeloRepository catModeloRepository;
	
    /**
     *
     * @param idMarca
     * @return
     */
    public List<CatModelo> getModelosByIdMarca(Integer idMarca){
		
		
		List<CatModelo> modelos = catModeloRepository.getModelosByIdMarca(idMarca);
		
		return modelos;
	}
    
}
