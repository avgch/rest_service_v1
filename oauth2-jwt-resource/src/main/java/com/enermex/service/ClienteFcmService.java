package com.enermex.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import com.enermex.modelo.ClienteFcmEntity;
import com.enermex.repository.ClienteFcmRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class ClienteFcmService {
  Logger logger = LoggerFactory.getLogger(ClienteFcmService.class);

  @Autowired
  private ClienteFcmRepository repo;

    /**
     *
     * @param push
     * @return
     */
    @Transactional
  public boolean create(ClienteFcmEntity push) {
    try {
      ClienteFcmEntity anterior = repo.findByFcm(push.getFcm());

      if(anterior != null) {
        anterior.setIdCliente(push.getIdCliente());
        return save(anterior);
      }
      
      return save(push);
    } catch(Exception e) {
      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param push
     * @return
     */
    public boolean delete(ClienteFcmEntity push) {
    if (!exists(push)) {
      logger.error("Trata de eliminar una llave inexistente - ID: " + push.getIdFcm());

      return false;
    }

    try {
      repo.delete(push);
      return true;
    } catch (Exception e) {
      logger.error("Error al eliminar una llave - ID: " + push.getIdFcm());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public ClienteFcmEntity queryById(BigInteger id) {
    try {
      Optional<ClienteFcmEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una ruta: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param idCliente
     * @return
     */
    @Transactional(readOnly = true)
  public List<ClienteFcmEntity> queryByIdCliente(BigInteger idCliente) {
    try {
      return repo.findByIdCliente(idCliente);
    } catch (Exception e) {
      logger.error("Error al obtener llaves: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(ClienteFcmEntity push) {
    try {
      return push != null && push.getIdFcm() != null && repo.existsById(push.getIdFcm());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una llave: " + e.getMessage());
      return false;
    }
  }

  @Transactional(propagation = Propagation.NESTED)
  private boolean save(ClienteFcmEntity push) {
    try {
      repo.save(push);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una llave: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
  
    /**
     *
     * @param idsClientes
     * @return
     */
    @Transactional(readOnly = true)
  public List<String> getTokensClientes(List<BigInteger> idsClientes) {
    try {
      return repo.getTokensClientes(idsClientes);
    } catch (Exception e) {
      logger.error("Error al obtener llaves: " + e.getMessage());
      return null;
    }
  }

}