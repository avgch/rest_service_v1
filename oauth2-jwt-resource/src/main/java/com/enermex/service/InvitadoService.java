package com.enermex.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestBody;

import com.enermex.dto.ClienteDto;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.TokenEntity;
import com.enermex.repository.InvitadoRepository;
import com.enermex.repository.impl.ClienteRepositoryImpl;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.utilerias.SmtpQueja;
import com.enermex.utilerias.UrlSigner;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.PedidoRepository;

/**
 *
 * @author abraham
 */
@Service
public class InvitadoService {
	
	@Autowired
	InvitadoRepository invitadoRepository;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	SmtpGmailSsl sendEmail;
	
	@Autowired
	ClienteRepositoryImpl clienteRepositoryImpl;
	
	@Autowired
	PedidoRepository pedidoRepository;

	@Autowired
	private TokenService tokenService;
	
	 @Autowired
  private UrlSigner signer;
  
  @Autowired
  private SmtpQueja smtpQueja;
	
    /**
     *
     * @param cliente
     * @throws Exception
     */
    public void save(ClienteDto cliente) throws Exception {
		
		Cliente invitado = new Cliente(cliente);
		Cliente invitadoExiste=null, titular=null;
		
		
		
		invitadoExiste=esRegistroValido(invitado.getCorreo());
   	 
	   	 if(invitadoExiste!=null) {
	   		 
	   	     throw new Exception("El correo de invitado ya existe");
	   	    
	   	 }
		
		invitadoRepository.save(invitado);
		
		//creamos token 
 	   TokenEntity tokenEntity = new TokenEntity();
 	   tokenEntity.setAplicacion("M");
 	   tokenEntity.setIdCliente(invitado.getIdCliente());
 	   tokenService.create(tokenEntity);
 	   String token = signer.sign(tokenEntity.getIdToken().toString());
		
		sendEmail.sendEmail(invitado.getCorreo(),token, "registroInvitado", "MOVIL");
		//notifica al titular a quien acaba de registrar
		
		//titular
	    titular = this.clienteRepository.findClienteById(cliente.getIdTitular());
   	   sendEmail.sendEmailNotificaFamiliarTitular(titular.getCorreo(),"notificaFamiliar&"+invitado.getCorreo());
		
	}
	
    /**
     *
     * @param id
     */
    public void delete(BigInteger id) {
		
		Cliente titular=null,invitado = null;
		
		//invitado
		invitado = this.clienteRepository.findClienteById(id);
		
		//invitadoRepository.deleteCliente(id);
		
		
		//titular
		titular = this.clienteRepository.findClienteById(invitado.getIdTitular());
		
		clienteRepositoryImpl.deleteTokenCliente(invitado.getIdCliente());
		invitadoRepository.delete(invitado);
		
		//consultamos al titular y lo notificamos
		sendEmail.sendEmailNotificaFamiliarTitular(titular.getCorreo(),"bajaInvitado&"+invitado.getCorreo());
		
		
	}

    /**
     *
     * @param idCliente
     * @param bloqueo
     * @param titular
     */
    public void lockUnlock(BigInteger idCliente,boolean bloqueo,Cliente titular) {
		
		boolean tienePedidos=false;
		invitadoRepository.blockUnlock(idCliente,bloqueo);
		//buscamos si el invitado tiene pedidodos
		 BigInteger numPedidos= pedidoRepository.getNumPedidosCliente(idCliente);
		 
		 //tiene pedidos
		 if(numPedidos.compareTo(new BigInteger("0"))==1)
			 tienePedidos = true;
		 
		
	  System.out.println(tienePedidos);	 
		// INTER: Antonio González - Envío de correo electrónico de bloqueo
     Cliente cliente = clienteRepository.findClienteById(idCliente);
     String nombre    = cliente.getNombre() != null ? cliente.getNombre() : "";
     String apPaterno = cliente.getApellidoPaterno() != null ? cliente.getApellidoPaterno() : "";
     String apMaterno = cliente.getApellidoMaterno() != null ? cliente.getApellidoMaterno() : "";
     String completo  = nombre + " " + apPaterno + " " + apMaterno;
    if(bloqueo) {
    	
        // Construcción del nombre completo
        smtpQueja.sendEmailBloqueado(cliente.getCorreo(), completo,true,tienePedidos);
    }else {
    	
    	smtpQueja.sendEmailBloqueado(cliente.getCorreo(), completo,false,tienePedidos);
    	
    }
    
    smtpQueja.sendEmailBloqueador(titular,completo,bloqueo,tienePedidos);
    
    
	}
   
    /**
     *
     * @param cliente
     * @throws Exception
     */
    public void updateInvitado(@RequestBody ClienteDto cliente) throws Exception{
		Cliente cli;
	   cli = esRegistroValido(cliente.getCorreo());
	   	 
	   	 if(cli!=null) {
	   		 
	   	     throw new Exception("El correo de invitado ya existe");
	   	    
	   	 }
		
		invitadoRepository.update(cliente.getNombre(),cliente.getApellidoPaterno(),cliente.getApellidoMaterno(), cliente.getTelefono(), cliente.getIdCliente());
		
		
	}

   /**
    * Valida registro valido
    * @param correo
    * @return
    */
   public Cliente esRegistroValido(String correo) {
   	
   	
   	return clienteRepository.findByEmail(correo);
   }
}
