package com.enermex.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.enumerable.PedidoVarios;
import com.enermex.modelo.Parametro;
import com.enermex.repository.ParametroRepository;

/**
 *
 * @author abraham
 */
@Service
public class ParametrosService {

	@Autowired
	ParametroRepository parametroRepositoryl;
	
    /**
     *
     * @return
     */
    public List<Parametro> getParametros(){
		
		return parametroRepositoryl.getParametros();
	}
	
    /**
     *
     * @return
     */
    public List<Parametro> getParametrosFilter(){
		
    	List<Parametro> parametros=null;
    	parametros = parametroRepositoryl.getParametros();
    	
    	List<Parametro> parametrosFilters = parametros.stream().filter
    			(parametro -> parametro.getTipo().equalsIgnoreCase("Tipo pedido")==false && parametro.getTipo().equalsIgnoreCase("Tiempo cancelación")==false)
    			.collect(Collectors.toList());
	    	
    	
    	
		return parametrosFilters;
	}
	
    /**
     *
     * @param id
     * @return
     */
    public Parametro getParametroById(Integer id) {
		
		return parametroRepositoryl.getParametroById(id);
		
	}
	
    /**
     *
     * @param valor
     * @param id
     */
    public void updateParametro(String valor,Integer id) {
		
		 parametroRepositoryl.updateParametro(valor,id);
		
	}
	
}
