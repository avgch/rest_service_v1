package com.enermex.service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.modelo.ClienteFcmEntity;
import com.enermex.modelo.DespachadorFcmEntity;
import com.enermex.modelo.Pedido;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;

/**
 *
 * @author abraham
 */
@Service
public class PushNotificationService {
	
	
	Logger logger = LoggerFactory.getLogger(ClienteFcmService.class);
	
	@Autowired
	DespachadorFcmService despachadorFcmService;
	
	@Autowired
	ClienteFcmService clienteFcmService;
	
	/**
	 * 
	 * @param p
	 */
	public void pushCancelaServiceToDespachador(Pedido p) {
		
		 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		 sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		List<String>  tokensDevices=null;
		 //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();	
		List<DespachadorFcmEntity>  tokenDespachador = despachadorFcmService.queryByIdDespachador(p.getIdDespachador());
		tokensDevices = tokenDespachador.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());
		
	   //parseamos la fecha a string 
		
		String fechaPedido = sdf.format(p.getFechaPedido().getTime());
		
		
	    if(tokensDevices!=null)
	    {
	    	if(tokensDevices.size()>0) {
			    //Notificacion
			    Notification.Builder msNot = Notification.builder();
			    
			    msNot.setBody("Estimado Despachador se ha cancelado su servicio ID: "+p.getUuid()+", que tenia asignado para atender: "+fechaPedido+" ");
			    msNot.setTitle("FAST GAS ");
			    msg.addAllTokens(tokensDevices);
			    
			    msg.setNotification(msNot.build());
			    
			    FirebaseMessaging fm = FirebaseMessaging.getInstance();
			    
			    try {
					fm.sendMulticast(msg.build());
				} catch (FirebaseMessagingException e) {
					// TODO Auto-generated catch block
					
					logger.error(e.getMessage());
				}
	    	}
	    }
	}
	/**
	 * 
	 * @param p
	 * @param texto
	 */
	public void pushUpdatePedidoCliente(Pedido p,String texto) {
		
		
		List<String>  tokensDevices=null;
		 //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();	
		List<ClienteFcmEntity>  tokenCliente = clienteFcmService.queryByIdCliente(p.getCliente().getIdCliente());
		tokensDevices = tokenCliente.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());
		
		
	    if(tokensDevices!=null)
	    {
	    	if(tokensDevices.size()>0) {
			    //Notificacion
			    Notification.Builder msNot = Notification.builder();
			    
			    msNot.setBody(texto);
			    msNot.setTitle("FAST GAS ");
			    msg.addAllTokens(tokensDevices);
			    
			    msg.setNotification(msNot.build());
			    
			    FirebaseMessaging fm = FirebaseMessaging.getInstance();
			    
			    try {
					fm.sendMulticast(msg.build());
				} catch (FirebaseMessagingException e) {
					// TODO Auto-generated catch block
					
					logger.error(e.getMessage());
				}
	    	}
	    }
	}
	
	/**
	 * 
	 * @param p
	 * @param texto
	 */
	public void pushCancelaDespachador(Pedido p,String texto) {
		
		
		List<String>  tokensDevices=null;
		 //Mensaje dependiedo del numero de device
	    MulticastMessage.Builder msg=  MulticastMessage.builder();	
	    List<ClienteFcmEntity>  tokenCliente = clienteFcmService.queryByIdCliente(p.getCliente().getIdCliente());
		tokensDevices = tokenCliente.stream().map(fcm -> fcm.getFcm()).collect(Collectors.toList());
		
		
	    if(tokensDevices!=null)
	    {
	    	if(tokensDevices.size()>0) {
			    //Notificacion
			    Notification.Builder msNot = Notification.builder();
			    
			    msNot.setBody(texto);
			    msNot.setTitle("FAST GAS ");
			    msg.addAllTokens(tokensDevices);
			    
			    msg.setNotification(msNot.build());
			    
			    FirebaseMessaging fm = FirebaseMessaging.getInstance();
			    
			    try {
					fm.sendMulticast(msg.build());
				} catch (FirebaseMessagingException e) {
					// TODO Auto-generated catch block
					
					logger.error(e.getMessage());
				}
	    	}
	    }
	}
	
	

}
