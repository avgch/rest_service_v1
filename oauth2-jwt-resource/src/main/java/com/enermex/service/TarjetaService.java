package com.enermex.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.modelo.Tarjeta;
import com.enermex.repository.TarjetaRepository;
import com.enermex.utilerias.ConektaOperation;
import com.enermex.modelo.Tarjeta;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.PedidoRepository;
import com.enermex.repository.TarjetaPlanRepository;

/**
 *
 * @author abraham
 */
@Service
public class TarjetaService {
	
	
	ConektaOperation conekta;
	
	@Autowired
	TarjetaRepository tarjetaRepository;
	
	@Autowired
	PedidoRepository pedidoRepository;
	
	@Autowired
	TarjetaPlanRepository tarjetaPlanRepository;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	
	/**
	 * Agregar tarjeta
	 * @param idCliente
	 * @param idTokenTarjeta
	 * @param cuatroDigitos
     * @param tipoTarjeta
     * @return 
	 */
	public Tarjeta addTarjeta(BigInteger idCliente,String idTokenTarjeta,String cuatroDigitos,String tipoTarjeta) {
		Tarjeta tarjeta;
		
		//agregamos los datos de la tarjeta enmascarada los últimos 4 dígitos
		tarjeta = new Tarjeta();
		tarjeta.setTokenTarjeta(idTokenTarjeta);
		tarjeta.setIdCliente(idCliente);
		tarjeta.setEstatus(true);
		tarjeta.setNumeroTarjeta("************"+cuatroDigitos);
		tarjeta.setTipoTarjeta(tipoTarjeta);
		
		return tarjetaRepository.save(tarjeta);
	}

	/**
	 * Obtiene tarjetas activas del titular
	 * @param idCliente
	 * @return
	 */
	public List<Tarjeta> getTarjetasActivas(BigInteger idCliente){
		
		List<Tarjeta> tarjetas = this.tarjetaRepository.getTarjetasActivas(idCliente);
		
		for(Tarjeta t : tarjetas) {
			t.setPlanes(tarjetaPlanRepository.getTarjetaPlanesById(t.getIdTarjeta()));
		}
		
		
		return tarjetas;
	}
	
	/**
	 * Obtiene tarjetas activas del titular
     * @param idInvitado
	 * @param idCliente
	 * @return
	 */
	public List<Tarjeta> getTarjetasInvitado(BigInteger idInvitado){
		
		//1. Obtengo el id del titular 
		
		BigInteger idTIutlar=this.clienteRepository.getIdTitularByIdInvitado(idInvitado);
		
		List<Tarjeta> tarjetas = this.tarjetaRepository.getTarjetasAPlanFamiliar(idTIutlar);
		
		return tarjetas;
	}
	
	/**
	 * Borrar tarjeta
	 * @param idTarjeta
	 */
	public void deleteTarjeta(Integer idTarjeta) {
		
		
		this.tarjetaRepository.borrar(idTarjeta);
		
		//eliminamos de conekta
	
	}
	/**
	 * Valida si la tarjeta tiene pedido
	 * @param idTarjeta
	 * @return
	 */
	public boolean tarjetaTienePedido(Integer idTarjeta) {
		
		BigInteger idPedido =this.pedidoRepository.tarjetaTienePedido(idTarjeta);
		
		
		
		if(idPedido==null)
			return false;
		
		if(idPedido.compareTo(new BigInteger("0"))==0)
			return false;
		  
		
			return true;
			
	}
	
	
	
}
