package com.enermex.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import com.enermex.repository.PipasRepository;
import com.enermex.dto.PipaDto;
import com.enermex.modelo.Pipa;
import com.enermex.service.PipaCamaraService;
import com.enermex.modelo.PipaCamara;
import com.enermex.modelo.PipaCamaraKey;
import com.enermex.service.CatCamaraService;
import com.enermex.enumerable.CamarasEstatus;
import com.enermex.enumerable.TipoCombustible;
import com.enermex.modelo.CatCamara;

/**
 *
 * @author abraham
 */
@Service
public class PipaService {


@Autowired
PipasRepository pipasRepository;

@Autowired
PipaCamaraService pipaCamaraService;

@Autowired
CatCamaraService  catCamaraService;

Pipa pipa;
PipaCamara pipaCamara;
PipaCamaraKey pipaKey;

    /**
     *
     * @return
     */
    public List<Pipa> getPipas(){


return pipasRepository.allPipasActivasPipas();
}

    /**
     *
     * @param pipaP
     */
    public void saveUpdate(PipaDto pipaP) {

pipa = new Pipa(pipaP);
pipaKey = new  PipaCamaraKey();
        pipa.setFechaCreacion(new Date());
        pipa.setEstatus("Activo");
        pipa.setUsuario(1);
       
       
        pipa=this.pipasRepository.save(pipa);
       
        if(pipaP.getCamarasSelected()!=null) {
       
            for(CatCamara camara: pipaP.getCamarasSelected()) {
               
            this.pipaCamara = new PipaCamara();
                this.pipaKey   = new PipaCamaraKey();
                this.pipaCamara.setPipaCamaraKey(pipaKey);
       
                 this.pipaKey.setIdCamara(camara.getIdCamara());
                 this.pipaKey.setIdPipa(pipa.getIdPipa());
                catCamaraService.updateStatusCamara(CamarasEstatus.ASIGNADA.getEstatus(),pipa.getNombre(), camara.getIdCamara());
        pipaCamaraService.save(pipaCamara);
       
            }
   
        }
   
}

    /**
     *
     * @param id
     * @return
     */
    public PipaDto findPipaById(Integer id) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Date hoy = new Date();
	String hoyS = sdf.format(hoy);
	
	Pipa pipa = pipasRepository.findPipaById(id);
	Double ltrProgramadosRegular=null,ltrProgramadosDiesel=null,ltrProgramadosPremium=null;
    //buscamos los litros programados para regular
	ltrProgramadosRegular=pipasRepository.getLitrosProgramadosDelDia(hoyS,TipoCombustible.REGULAR.getCombustible(),id);
    //buscamos los litros programados para diesel
	ltrProgramadosDiesel=pipasRepository.getLitrosProgramadosDelDia(hoyS,TipoCombustible.DIESEL.getCombustible(),id);
    //buscamos los litros programados para premium
	ltrProgramadosPremium=pipasRepository.getLitrosProgramadosDelDia(hoyS,TipoCombustible.PREMIUM.getCombustible(),id);
	
	ltrProgramadosRegular = (ltrProgramadosRegular==null) ? 0 : ltrProgramadosRegular;
	ltrProgramadosDiesel  = (ltrProgramadosDiesel==null)  ? 0 : ltrProgramadosDiesel;
	ltrProgramadosPremium = (ltrProgramadosPremium==null) ? 0 : ltrProgramadosPremium;
	
	
	PipaDto dto = new PipaDto(pipa);
	dto.setLitrosProgramadosDiesel(ltrProgramadosDiesel);
	dto.setLitrosProgramadosPremium(ltrProgramadosPremium);
	dto.setLitrosProgramadosRegular(ltrProgramadosRegular);
	
	
return dto;

}

    /**
     *
     * @param pipa
     */
    public void update(PipaDto pipa) {

//consultamos al momento los litros restantes y le sumamos litros carga	
Double litrosCargarDisel,litrosCargarPremium,litrosCargarRegular;
Calendar hoy = Calendar.getInstance();
SimpleDateFormat  sdf = new SimpleDateFormat("dd/MM/yyyy"); 
String hoyS, dateModifyS="";
hoyS        = sdf.format(hoy.getTime());
if(pipa.getFechaModificacion()!=null) {
 Date dateModify = pipa.getFechaModificacion().getTime();
 dateModifyS = sdf.format(dateModify);
}

    Double listrosRestantesDiesel  = pipasRepository.getLitrosRestantesDiesel(pipa.getIdPipa());
    Double listrosRestantesRegular = pipasRepository.getLitrosRestantesRegular(pipa.getIdPipa());
    Double listrosRestantesPremium = pipasRepository.getLitrosRestantesPremium(pipa.getIdPipa());
    
    listrosRestantesDiesel = (listrosRestantesDiesel==null)   ? 0d : listrosRestantesDiesel;
    listrosRestantesRegular = (listrosRestantesRegular==null) ? 0d : listrosRestantesRegular;
    listrosRestantesPremium = (listrosRestantesPremium==null) ? 0d : listrosRestantesPremium;

//DIESEL
  listrosRestantesDiesel += (pipa.getLitrosCargarDiesel()==null) ? 0d : pipa.getLitrosCargarDiesel();
 

//REGULAR
  listrosRestantesRegular += (pipa.getLitrosCargarRegular()==null) ? 0d : pipa.getLitrosCargarRegular();

//PREMIUM  
  listrosRestantesPremium += (pipa.getLitrosCargarPremium()==null) ? 0d : pipa.getLitrosCargarPremium();
	
  if(!(dateModifyS.equalsIgnoreCase(hoyS)) || pipa.getFechaModificacion()==null) {
        this.pipasRepository.updatePipa(pipa.getNombre(),pipa.getNumSerie(),pipa.getPlacas(),pipa.getEstatus(),pipa.getMotivoBaja(),pipa.isDiesel(),pipa.isRegular(),pipa.isPremium(),pipa.getTamanoTotalTanque(),pipa.getTotalRegular(),
		pipa.getTotalPremium(),pipa.getTotalDiesel(),listrosRestantesDiesel,listrosRestantesRegular,
		listrosRestantesPremium,hoy,pipa.getIdPipa());
  }
  else {
        this.pipasRepository.updatePipaSinCarga(pipa.getNombre(),pipa.getNumSerie(),pipa.getPlacas(),pipa.getEstatus(),pipa.getMotivoBaja(),pipa.isDiesel(),pipa.isRegular(),pipa.isPremium(),pipa.getTamanoTotalTanque(),pipa.getTotalRegular(),
		pipa.getTotalPremium(),pipa.getTotalDiesel(),pipa.getIdPipa());
  }

//ELIMINAMOS LAS CAMARAS ASOCIADAS AL
this.pipaCamaraService.deleteCurrentePipaCamara(pipa.getIdPipa());

//ACTUALIZAMOS LAS CAMARAS
pipaKey = new  PipaCamaraKey();

     
        if(pipa.getCamarasSelected()!=null) {
       
            for(CatCamara camara: pipa.getCamarasSelected()) {
               
            this.pipaCamara = new PipaCamara();
                this.pipaKey   = new PipaCamaraKey();
                this.pipaCamara.setPipaCamaraKey(pipaKey);
       
                 this.pipaKey.setIdCamara(camara.getIdCamara());
                 this.pipaKey.setIdPipa(pipa.getIdPipa());
                 
                 
                 System.out.println(camara.getEstatus());
                 System.out.println(CamarasEstatus.ACTIVA.getEstatus());
                 //Si la camara esta inactiva no movemos el estatis se queda tal cual hasta que este activa
                 if(camara.getEstatus().equalsIgnoreCase(CamarasEstatus.INACTIVA.getEstatus()))
                   catCamaraService.updateStatusCamara(CamarasEstatus.INACTIVA.getEstatus(),pipa.getNombre(), camara.getIdCamara());
                 else if (camara.getEstatus().equalsIgnoreCase(CamarasEstatus.ACTIVA.getEstatus()))
                catCamaraService.updateStatusCamara(CamarasEstatus.ASIGNADA.getEstatus(),pipa.getNombre(), camara.getIdCamara());
                 else if (camara.getEstatus().equalsIgnoreCase(CamarasEstatus.ASIGNADA.getEstatus()))
                catCamaraService.updateStatusCamara(CamarasEstatus.ASIGNADA.getEstatus(),pipa.getNombre(), camara.getIdCamara());

                 
        pipaCamaraService.save(pipaCamara);
            }
   
        }
}

    /**
     *
     * @return
     */
    public List<Pipa> getPipasActivas(){

List<Pipa>      pipas   = pipasRepository.allPipasActivas();

return pipas;
}
/**
 * Abraham Vargas
 * Solo se puede actualizar con el mismo nombre la pipa que se esta editando
     * @param nombre
     * @param idPipa
 * @return
 */
public boolean existeNombrePipa(String nombre,Integer idPipa) {
	
	return pipasRepository.existeNombrePipa(nombre,idPipa);
}



}