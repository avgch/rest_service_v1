package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.TarjetaPlanRepository;
import com.enermex.modelo.TarjetaPlan;

/**
 *
 * @author abraham
 */
@Service
public class TarjetaPlanService {
	@Autowired
	TarjetaPlanRepository tarjetaPlanRepository;
	
    /**
     *
     * @param tarjetaPlanes
     */
    public void save(List<TarjetaPlan> tarjetaPlanes) {
		
		tarjetaPlanRepository.saveAll(tarjetaPlanes);
	}
	
	/**
	 * Obtiene los planes asociados a una tarjeta
	 * @return
	 */
	public List<TarjetaPlan> getTarjetaPlanes(){
		
		return tarjetaPlanRepository.getTarjetaPlanes();
	}
	/**
	 * Elimina los planes asociados a una tarjeta
     * @param idTarjeta
	 */
	public void deletePlanes(Integer idTarjeta){
		
		this.tarjetaPlanRepository.deletePlanes(idTarjeta);
	}
	
	
	
}
