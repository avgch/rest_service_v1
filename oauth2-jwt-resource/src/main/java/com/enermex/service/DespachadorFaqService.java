package com.enermex.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.enermex.enumerable.FaqEstatus;
import com.enermex.modelo.DespachadorFaqEntity;
import com.enermex.repository.DespachadorFaqRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class DespachadorFaqService {
  Logger logger = LoggerFactory.getLogger(DespachadorFaqService.class);

  @Autowired
	private DespachadorFaqRepository repo;

    /**
     *
     * @param faq
     * @return
     */
    @Transactional
  public boolean create(DespachadorFaqEntity faq) {
    if (exists(faq)) {
      logger.error("Trata de añadir una faq existente - ID: " + faq.getIdFaq());

      return false;
    }

    // Se establece el estado y la fecha de creación
    Date date = new Date();
    faq.setFechaCreacion(date);

    return save(faq);
  }

    /**
     *
     * @param faq
     * @return
     */
    public boolean update(DespachadorFaqEntity faq) {
    if (!exists(faq)) {
      logger.error("Trata de actualizar una faq inexistente - ID: " + faq.getIdFaq());

      return false;
    }

    return save(faq);
  }

    /**
     *
     * @param faq
     * @return
     */
    public boolean delete(DespachadorFaqEntity faq) {
    if (!exists(faq)) {
      logger.error("Trata de eliminar una faq inexistente - ID: " + faq.getIdFaq());

      return false;
    }

    faq.setEstatus(FaqEstatus.INACTIVO);
    return save(faq);
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public List<DespachadorFaqEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener faqs: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public List<DespachadorFaqEntity> queryActivos() {
    try {
      return repo.findByEstatus(FaqEstatus.ACTIVO);
    } catch (Exception e) {
      logger.error("Error al obtener faqs: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public DespachadorFaqEntity queryById(Integer id) {
    try {
      Optional<DespachadorFaqEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una faq: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(DespachadorFaqEntity faq) {
    try {
      return faq != null && faq.getIdFaq() != null && repo.existsById(faq.getIdFaq());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una faq: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public boolean exists(Integer id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una faq: " + e.getMessage());
      return false;
    }
  }

  @Transactional()
  private boolean save(DespachadorFaqEntity faq) {
    try {
      repo.save(faq);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una faq: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}
