package com.enermex.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Optional;
import java.util.UUID;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.enermex.modelo.AzureBsEntity;
import com.enermex.modelo.LegalesEntity;
import com.enermex.repository.LegalesRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abraham
 */
@Service
public class LegalesService {
  Logger logger = LoggerFactory.getLogger(LegalesService.class);

  @Autowired
  private AzureBsService azureService;
  
  @Autowired
  private LegalesRepository repo;

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public LegalesEntity legales() {
    try {
      Optional<LegalesEntity> optional = repo.findById(1);
        
      if(optional.isPresent()) {
        return optional.get();
      }

      return null;
    } catch(Exception e) {
      // TODO: logger
      e.printStackTrace();
      return null;
    }
  }
  
    /**
     *
     * @param text
     * @return
     */
    @Transactional
  public boolean textTerminos(String text) {
    try {
      Optional<LegalesEntity> optional = repo.findById(1);
        
      if(optional.isPresent()) {
        LegalesEntity legal = optional.get();
        legal.setTerminosText(text);
        repo.save(legal);
        return true;
      }

      return false;
    } catch(Exception e) {
      // TODO: logger
      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param file
     * @return
     */
    @Transactional
  public boolean uploadTerminos(MultipartFile file) {
    try {
      String fileName = upload(file);

      if(fileName != null) {
        Optional<LegalesEntity> optional = repo.findById(1);
        
        if(optional.isPresent()) {
          LegalesEntity legal = optional.get();
          legal.setTerminos(fileName);
          repo.save(legal);
          return true;
        }
      }

      return false;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public InputStreamResource downloadTerminos() {
    Optional<LegalesEntity> optional = repo.findById(1);
    return optional.isPresent() ? download(optional.get().getTerminos()) : null;
  }

    /**
     *
     * @param text
     * @return
     */
    @Transactional
  public boolean textPrivacidad(String text) {
    try {
      Optional<LegalesEntity> optional = repo.findById(1);
        
      if(optional.isPresent()) {
        LegalesEntity legal = optional.get();
        legal.setPrivacidadText(text);
        repo.save(legal);
        return true;
      }

      return false;
    } catch(Exception e) {
      // TODO: logger
      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param file
     * @return
     */
    @Transactional
  public boolean uploadPrivacidad(MultipartFile file) {
    try {
      String fileName = upload(file);

      if(fileName != null) {
        Optional<LegalesEntity> optional = repo.findById(1);
        
        if(optional.isPresent()) {
          LegalesEntity legal = optional.get();
          legal.setPrivacidad(fileName);
          repo.save(legal);
          return true;
        }
      }

      return false;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public InputStreamResource downloadPrivacidad() {
    Optional<LegalesEntity> optional = repo.findById(1);
    return optional.isPresent() ? download(optional.get().getPrivacidad()) : null;
  }
  
  private String upload(MultipartFile file) {
    try {
      // Se calcula un nombre único de la imagen
      String fileName = UUID.randomUUID().toString() + "_" +  file.getOriginalFilename().replace(" ", "");

      // Se obtiene el BlobContainer de Azure
      BlobContainerClient blobContainerClient = azureContainer();
      BlockBlobClient evidencia = blobContainerClient.getBlobClient(fileName).getBlockBlobClient();
      evidencia.upload(file.getInputStream(), file.getSize());

      return fileName;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  
  private InputStreamResource download(String fileName) {
    try {
      BlobContainerClient blobContainerClient = azureContainer();
      ByteArrayInputStream file = azureInputStream(blobContainerClient, fileName);

      if(file != null) {
        return new InputStreamResource(file);
      }

      return null;
    } catch (Exception e) {
      logger.error("Error al descargar la imagen: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  
  private BlobContainerClient azureContainer() {
    // Consulta de credenciales de Azure
    AzureBsEntity azureEntity = azureService.queryById(1);

    // Se crea un cliente del Blob Service de Azure
    BlobServiceClient blobServiceClient = new BlobServiceClientBuilder()
      .endpoint(azureEntity.getEndpoint())
      .sasToken(azureEntity.getSas())
      .buildClient();
    
    // Se crea un cliente para el contenedor
    BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(azureEntity.getContainer());
    return blobContainerClient;
  }

  private ByteArrayInputStream azureInputStream(BlobContainerClient blobContainerClient, String blobName) {
    BlockBlobClient fotoActual = blobContainerClient.getBlobClient(blobName).getBlockBlobClient();
        
    if(fotoActual.exists()) {
      // A ByteArrayOutputStream holds the content in memory
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      fotoActual.download(outputStream);
      
      // To convert it to a byte[] - simply use
      final byte[] bytes = outputStream.toByteArray();

      // To convert bytes to an InputStream, use a ByteArrayInputStream
      ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
      
      return inputStream;
    } else {
      return null;
    }
  }
}
