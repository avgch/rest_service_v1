package com.enermex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.CodigoPromocionClienteRepository;
import com.enermex.modelo.CodigoPromoCliente;

/**
 *
 * @author abraham
 */
@Service
public class ClientePromocionService {
	
	@Autowired
	CodigoPromocionClienteRepository clientePromocionRepository;
	
    /**
     *
     * @param pc
     */
    public void save(CodigoPromoCliente pc) {
		
		this.clientePromocionRepository.save(pc);
	}
	

}
