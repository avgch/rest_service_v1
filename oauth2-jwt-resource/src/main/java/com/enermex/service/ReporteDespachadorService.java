package com.enermex.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.enermex.dto.reporte.ReporteDespachador;
import com.enermex.repository.ServicioReporteRepository;
import com.enermex.view.ServicioReporteView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author abraham
 */
@Service
public class ReporteDespachadorService {
  @Autowired
  ServicioReporteRepository repo;

  private List<ServicioReporteView> queryByDate(Calendar inicio, Calendar fin) {
    try {
      return repo.findByFechaPedidoGreaterThanEqualAndFechaPedidoLessThanEqual(inicio, fin);
    } catch (Exception e) {
      e.printStackTrace();

      //TODO: Logger
      return null;
    }
  }

    /**
     *
     * @param filtro
     * @return
     */
    public List<ServicioReporteView> queryByFilter(ReporteDespachador filtro){
    Calendar inicio        = filtro.getInicio();
    Calendar fin           = filtro.getFin();
    Integer  idRuta        = filtro.getIdRuta();
    Long     idDespachador = filtro.getIdDespachador();
    Integer  estatus       = filtro.getEstatus();

    List<ServicioReporteView> servicios = queryByDate(inicio, fin);
    
    if(servicios == null){
      return null;
    }
    
    ArrayList<ServicioReporteView> filtrados = new ArrayList<>();
    
    for(ServicioReporteView servicio : servicios) {
      if(idRuta != null && (servicio.getIdRuta() == null || idRuta.intValue() != servicio.getIdRuta().intValue())) {
        continue;
      }

      if(idDespachador != null && (servicio.getIdDespachador() == null || idDespachador.longValue() != servicio.getIdDespachador().longValue())) {
        continue;
      }

      if(estatus != null && (servicio.getEstatus() == null || estatus.intValue() != servicio.getEstatus().intValue())) {
        continue;
      }

      filtrados.add(servicio);
    }

    return filtrados;
  }
  
}