package com.enermex.service;

import java.util.List;
import java.util.Optional;

import com.enermex.modelo.RolEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.RolRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class RolService {
  Logger logger = LoggerFactory.getLogger(RolService.class);
  
	@Autowired
	private RolRepository repo;
	
	/**
   * Creación de un rol
   * 
   * @param rol Rol por insertar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  public boolean create(RolEntity rol) {
    return save(rol);
  }

  /**
   * Actualización de un rol
   * 
   * @param rol Rol por actualizar
   * @return Bandera que indica el resultado de la operación
   */
  public boolean update(RolEntity rol) {
    if (!exists(rol)) {
      logger.error("Trata de actualizar un rol inexistente - ID: " + rol.getIdRol());

      return false;
    }

    return save(rol);
  }

  /**
   * Consulta de los roles registrados
   * 
   * @return Lista con roles registrados
   */
  @Transactional(readOnly = true)
  public List<RolEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener roles: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un rol por su identificador
   * 
   * @param id Identificador del rol
   * @return El rol, si se encuentra
   */
  @Transactional(readOnly = true)
  public RolEntity queryById(Integer id) {
    try {
      Optional<RolEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener un rol: " + e.getMessage());
      return null;
    }
  }

  /**
   * Comprueba si existe un rol
   * 
   * @param rol
   * @return Bandera que indica si existe el rol
   */
  @Transactional(readOnly = true)
  private boolean exists(RolEntity rol) {
    try {
      return rol != null && rol.getIdRol() != null && repo.existsById(rol.getIdRol());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un rol: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un rol
   * 
   * @param id Identificador del rol
   * @return Bandera que indica si existe el rol
   */
  @Transactional(readOnly = true)
  public boolean exists(Integer id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un rol: " + e.getMessage());
      return false;
    }
  }

  /**
   * Guardado del rol
   * 
   * @param rol Rol por guardar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  private boolean save(RolEntity rol) {
    try {
      repo.save(rol);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar un rol: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
  
  
}
