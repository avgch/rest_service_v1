package com.enermex.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.enermex.dto.CoberturaDto;
import com.enermex.dto.EdoMunDto;
import com.enermex.modelo.PrecioGasolina;
import com.enermex.repository.impl.PreciosGasolinaHistoricoReporteImpl;
import com.google.gson.Gson;
import com.enermex.service.PrecioGasolinaService;
import com.enermex.utilerias.SqlOperations;

/**
 *
 * @author abraham
 */
@Service
public class CargaCombustiblesService {
	
	@Autowired
	PrecioGasolinaService precioGasolinaService;
	
	@Autowired
	SqlOperations SqlOperations;
	
	@Autowired
	PreciosGasolinaHistoricoReporteImpl preciosGasolinaHistoricoImpl;
	
	@Autowired
	PreciosGasolinaHistoricoReporteImpl preciosGasolinaHistoricoReporteImpl;
	
	/**
	 * 
	 */
	@Scheduled(cron = "0 15 6 * * ?")
	public void loadPricesfuels() {
		
		System.out.println(new Date());
		System.out.println("ejecutando cron");
		
		/**
		 *  Creamos:
		 * 1. t_gasolina_estado_municipio
		 * 2. t_gasolina_estado_municipio_historico
		 * 3. t_gasolina_estado_municipio_historico_temp
		 * 4. t_
		 */
		
		//respaldamos informacion de precios en historico
		/**  LIMPIAMOS NUESTRA TABLA DE PRECIOS DE PETRO INTELLIGENCE  **/
		//GUARDAMOS AL HISTORIAL se 
		preciosGasolinaHistoricoImpl.saveHistoricPrices(SqlOperations.saveHistoricPricesFuels());
		//LIMPIAMOS LA TABLA t_gasolina_estado_municipio
		precioGasolinaService.deleteAll();
		
	
		boolean isReady=false;
		while(isReady==false) {
			
			try {
				Thread.sleep(6000);
				isReady = checkFuelsIsReady();
				if(isReady) {
					cargaPreciosGasolina();
				}else {
					//podemos registrar un evento si encuentra nada
				}	
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
		
		/** SE CREA TABLA TEMPORAL PARA PRECIOS **/
		System.out.println("*** Termino de cargar el combustible para el listado de precios ***");
		System.out.println("*** Creando tabla para server side procesing (precios_combustibles) ***");
		precioGasolinaService.dropTablePreciosCombustiblesServerSide();
		precioGasolinaService.createTablePreciosCombustiblesServerSide();
		
//		/** SE CREA TABLA TEMPORAL PARA GENERAR REPORTE HISTORICO DE PRECIOS **/
//		//eliminamos nuestra tabla temporal de historico t_gasolina_estado_municipio_historico_temp
//		preciosGasolinaHistoricoReporteImpl.dropTablaTemporalParaReporte();
//		System.out.println("*** eliminamos nuestra tabla temporal de historico t_gasolina_estado_municipio_historico_temp ***");
//		//eliminamos nuestra tabla tempora de reporte t_gasolina_estado_municipio_historico_temp
//		preciosGasolinaHistoricoReporteImpl.creaTablaTemporalParaReporte();
//		System.out.println("*** eliminamos nuestra tabla temporal de reporte t_gasolina_estado_municipio_historico_temp ***");
//		preciosGasolinaHistoricoReporteImpl.creaIndexTemporalParaReporte();
//		System.out.println("*** creamos index de nuestra tabla temporal de reporte t_gasolina_estado_municipio_historico_temp ***");
		//limpiamos la tabla
	}
	
	/**
	 * 
     * @return 
     * @return  
	 */
	public boolean chekReady() {
		
		String terminoS="";
		boolean termino=false;
		
		URL url;
		HttpURLConnection con;
		try {
			url = new URL("http://servicioswebintel.com/enermex/wsCheck.php");
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(10000);
			con.setReadTimeout(10000);
			con = (HttpURLConnection) url.openConnection();
			
			BufferedReader in = new BufferedReader(
					  new InputStreamReader(con.getInputStream()));
					String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
			    content.append(inputLine);
			    System.out.println(inputLine);
			}
			in.close();
			int status = con.getResponseCode();
			con.disconnect();
			
			Gson g = new Gson();

			CheckCombustibleDto[] check =  g.fromJson(content.toString(),CheckCombustibleDto[].class );
	
			for(CheckCombustibleDto c : check) {
				
				 terminoS = c.getTermino();
				 break;
			}
	
			if(status==200) {
				System.out.println(status);
			}else {
				System.out.println(status);	
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(terminoS.equalsIgnoreCase("1"))
		 termino=true;
		
		
		return termino;
		
	}
	
	
	/**
	 * 
	 */
	public void cargaPreciosGasolina() {
		
		URL url;
		HttpURLConnection con;
		int  numRegistros=0;
		List<PrecioGasolina> preciosList = new ArrayList<>();
		//obtenemos la cobertura que requerimos de petro 
		List<EdoMunDto> cobertura = precioGasolinaService.getCoberturaPorEdoMunc();
		
		try {
			url = new URL("https://petrointelligence.com/api/consultaProms.php");
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(10000);
			con.setReadTimeout(10000);
			con = (HttpURLConnection) url.openConnection();
			
			BufferedReader in = new BufferedReader(
					  new InputStreamReader(con.getInputStream()));
					String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
			    content.append(inputLine);
			    System.out.println(inputLine);
			}
			in.close();
			int status = con.getResponseCode();
			con.disconnect();
			
			Gson g = new Gson();
			
			ArrayList jsonObjList = g.fromJson(content.toString(), ArrayList.class);
			
			PrecioGasolina[] precios = g.fromJson(content.toString(), PrecioGasolina[].class);  
			
			
			if(status==200) {
				
				for(PrecioGasolina ps: precios ) {
					
					EdoMunDto edoMun=cobertura.stream().filter(e -> e.getIdEdo().compareTo(new Integer(ps.getID_ESTADO()))==0 && e.getIdMun().compareTo(new Integer(ps.getID_MUNICIPIO()))==0 )
							  .findAny().orElse(null);
					if(edoMun!=null) {
						System.out.println("idEDo:"+edoMun.getIdEdo()+" idMun:"+edoMun.getIdMun());
					     PrecioGasolina e =new   PrecioGasolina(ps);
					     preciosList.add(e);
					     numRegistros++;
					}
				}
				System.out.println(status);
			}else {
				System.out.println(status);	
			}
			System.out.println("Inicia el proceso de carga de precios");
			precioGasolinaService.saveMany(preciosList);
			System.out.println("Fin del proceso de carga de precios");
			System.out.println("Num. reg guardados: "+numRegistros);
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	
	/**
	 * 
	 * @return
	 */
	private boolean checkFuelsIsReady(){
	    
		boolean cargaGasolina=false;
		cargaGasolina = chekReady();
	
		return cargaGasolina;
	}
	
	
	/**
	 * Clase interna para obtener el estatus de descarga
	 * @author Abraham Vargas
	 *
	 */
	 private class CheckCombustibleDto {
		
		 private String termino;

		public String getTermino() {
			return termino;
		}

		public void setTermino(String termino) {
			this.termino = termino;
		}

		
		
	}
	
	
	
	
}
