package com.enermex.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.dto.ListadoCombustiblesDto;
import com.enermex.modelo.DataTableFilter;
import com.enermex.modelo.DataTableResult;
import com.enermex.repository.impl.PrecioGasolinaRepositoryImpl;

/**
 *
 * @author abraham
 */
@Service
public class ListadoCombustiblesService {
	
	@Autowired
	PrecioGasolinaRepositoryImpl precioGasolinaRepositoryImpl;
	
	/**
	 * 
	 * @return
	 */
	public List<ListadoCombustiblesDto> getCombustiblesTarifas(){
		
		List<ListadoCombustiblesDto> prediosDto = new ArrayList<ListadoCombustiblesDto>();
				
		prediosDto =precioGasolinaRepositoryImpl.getPrecios();
		
		return prediosDto;
		
	}
	/**
	 * 
	 * @param filter
	 * @return
	 */
	public DataTableResult getCombustiblesTarifas(DataTableFilter filter){
		
		return precioGasolinaRepositoryImpl.getPreciosCombustibles(filter);
		
		
	}

	

}
