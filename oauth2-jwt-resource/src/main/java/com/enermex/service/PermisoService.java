package com.enermex.service;

import java.util.List;
import java.util.Optional;

import com.enermex.modelo.PermisoEntity;
import com.enermex.modelo.PermisoId;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.PermisoRepository;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class PermisoService {
  Logger logger = LoggerFactory.getLogger(PermisoService.class);
  
	@Autowired
	private PermisoRepository repo;
	
	/**
   * Creación de un permiso
   * 
   * @param permiso Permiso por insertar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  public boolean create(PermisoEntity permiso) {
    return save(permiso);
  }

  /**
   * Actualización de un permiso
   * 
   * @param permiso Permiso por actualizar
   * @return Bandera que indica el resultado de la operación
   */
  public boolean update(PermisoEntity permiso) {
    if (!exists(permiso)) {
      logger.error("Trata de actualizar un permiso inexistente" + permiso.getId());

      return false;
    }

    return save(permiso);
  }

  /**
   * Consulta de los permisoes registrados
   * 
   * @return Lista con permisoes registrados
   */
  @Transactional(readOnly = true)
  public List<PermisoEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener permisoes: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un permiso por su identificador
   * 
   * @param id Identificador del permiso
   * @return El permiso, si se encuentra
   */
  @Transactional(readOnly = true)
  public PermisoEntity queryById(PermisoId id) {
    try {
      Optional<PermisoEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener un permiso: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un permiso por los valores de su identificador
   * 
     * @param idRol
   * @param id Identificador del permiso
     * @param pantalla
     * @param permiso
   * @return El permiso, si se encuentra
   */
  public PermisoEntity queryById(Integer idRol, String pantalla, String permiso) {
    PermisoId id = new PermisoId();
    id.setIdRol(idRol);
    id.setPantalla(pantalla);
    id.setPermiso(permiso);

    return queryById(id);
  }

  /**
   * Consulta de un permiso por los valores de su identificador
   * 
   * @param idRol Identificador del rol
   * @param pantalla Nombre de la pantalla
   * @return El permiso, si se encuentra
   */
  public List<PermisoEntity> queryById(Integer idRol, String pantalla) {
    try {
      return repo.findByIdIdRolAndIdPantalla(idRol, pantalla);
    } catch (Exception e) {
      logger.error("Error al obtener un permiso: " + e.getMessage());
      return null;
    }
  }
  /**
   * Consulta de un permiso por los valores de su identificador
   * 
   * @param idRol Identificador del rol
   * @return El permiso, si se encuentra
   */
  public List<PermisoEntity> queryById(Integer idRol) {
    try {
      return repo.findByIdIdRol(idRol);
    } catch (Exception e) {
      logger.error("Error al obtener un permiso: " + e.getMessage());
      return null;
    }
  }

  /**
   * Comprueba si existe un permiso
   * 
   * @param permiso
   * @return Bandera que indica si existe el permiso
   */
  @Transactional(readOnly = true)
  private boolean exists(PermisoEntity permiso) {
    try {
      return permiso != null && permiso.getId() != null && repo.existsById(permiso.getId());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un permiso: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un permiso
   * 
   * @param id Identificador del permiso
   * @return Bandera que indica si existe el permiso
   */
  @Transactional(readOnly = true)
  public boolean exists(PermisoId id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un permiso: " + e.getMessage());
      return false;
    }
  }

  /**
   * Guardado del permiso
   * 
   * @param permiso Permiso por guardar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  private boolean save(PermisoEntity permiso) {
    try {
      repo.save(permiso);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar un permiso: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param oauthHttpHeader
     * @param pantalla
     * @param permiso
     * @return
     * @deprecated
     */
    @Deprecated
  public boolean testPerm(String oauthHttpHeader, String pantalla, String permiso) {
    // Se busca al usuario a quien corresponde el token
    // Se comprueban sus roles
    return true;
  }

    /**
     *
     * @param oauthHttpHeader
     * @param response
     * @param pantalla
     * @param permiso
     * @return
     */
    public boolean testPerm(String oauthHttpHeader, RestResponse<?> response, String pantalla, String permiso) {
    // Se busca al usuario a quien corresponde el token
    // Se comprueban sus roles
    return true;
  }

    /**
     *
     * @param usuario
     * @param response
     * @param pantalla
     * @param permiso
     * @return
     */
    public boolean testPerm(UsuarioEntity usuario, RestResponse<?> response, String pantalla, String permiso) {
    // Se comprueban sus roles
    return true;
  }
}
