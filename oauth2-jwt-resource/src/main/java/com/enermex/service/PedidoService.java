package com.enermex.service;

import com.enermex.dto.PedidoDto;
import com.enermex.dto.PedidoTicketRequestDto;
import com.enermex.dto.PrecioGasolinaRequestDto;
import com.enermex.dto.TicketFinalDto;
import com.enermex.dto.UbicacionDespachadorRequestDto;
import com.enermex.enumerable.MotivoCancelacionPenalizaEnum;
import com.enermex.enumerable.PedidoEstatus;
import com.enermex.enumerable.PedidoVarios;
import com.enermex.enumerable.PipaEstatusEnum;
import com.enermex.enumerable.ProductosEnum;
import com.enermex.enumerable.TipoCombustible;
import com.enermex.enumerable.TipoPedidoEnum;
import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.CodigoPromoCliente;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.PedidoConekta;
import com.enermex.modelo.PedidoEvidenciaEntity;
import com.enermex.modelo.Pipa;
import com.enermex.modelo.ProductoPromocion;
import com.enermex.modelo.Promocion;
import com.enermex.modelo.RutaEntity;
import com.enermex.modelo.Tarjeta;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.enermex.repository.PedidoRepository;
import com.enermex.repository.PipasRepository;
import com.enermex.repository.RutaRepository;
import com.enermex.repository.ClienteRepository;
import com.enermex.service.RutaService;
import com.enermex.utilerias.CalculoPedido;
import com.enermex.utilerias.ConektaOperation;
import com.enermex.utilerias.SmtpGmailSsl;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.Distance;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixRow;
import com.google.maps.model.LatLng;
import com.enermex.repository.VwPedidosRepository;
import com.enermex.repository.DireccionClienteRepository;
import com.enermex.repository.UsuarioRepository;
import com.enermex.avg.exceptions.ConectaException;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.avg.exceptions.PedidoException;
import com.enermex.conekta.models.Servicio;
import com.enermex.dto.CancelacionEmailDto;
import com.enermex.dto.CancelarServicioDto;
import com.enermex.dto.DisponibilidadPedidoDto;
import com.enermex.dto.FinalizarCompraRequestDto;
import com.enermex.dto.HorarioServicioDto;
import com.enermex.dto.PedidoClienteDetailDto;
import com.enermex.dto.PedidoClienteDto;
import com.enermex.dto.PedidoDespachador;
import com.enermex.dto.PedidoDetailDto;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.Cancelacion;
import com.enermex.modelo.CatMotivoCancelacion;
import com.enermex.modelo.DireccionCliente;
import com.enermex.modelo.Parametro;
import com.enermex.service.AutomovilClienteService;
import com.enermex.service.ClienteService;
import com.enermex.repository.impl.CodigoPostalRepositoryImpl;
import com.enermex.repository.impl.PedidoRepositoryImpl;
import com.enermex.repository.ParametroRepository;
import com.enermex.repository.TarjetaRepository;
import com.enermex.repository.TipoPedidoRepository;
import org.apache.poi.hpsf.Array;
import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import com.enermex.repository.PedidoConektaRepository;
import com.enermex.repository.CancelacionRepository;
import com.enermex.repository.ClienteDireccionRepository;
import com.enermex.repository.MotivoCancelacionRepository;
import com.enermex.enumerable.PedidoMensajes;
import com.google.maps.model.Duration; 
import com.enermex.repository.DireccionClienteRepository;
import com.enermex.service.ClientePromocionService;

/**
 *
 * @author abraham
 */
@Service
public class PedidoService {

	@Autowired
	PedidoRepository pedidoRepository;
	
	@Autowired
	ConektaOperation conkt;
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	RutaService rutaService;
	
	@Autowired
	VwPedidosRepository vwPedidosRepository;
	
	@Autowired
	DireccionClienteRepository direccionClienteRepository;
	
	@Autowired
	UsuarioRepository  usuarioRepository;
	
	@Autowired
	RutaRepository rutaRepository;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	AutomovilClienteService  automovilClienteService;
	
	@Autowired
	PedidoRepositoryImpl pedidoRepositoryImpl;
	
	@Autowired
	ParametroRepository parametroRepository;
	
	@Autowired
	TarjetaRepository tarjetaRepository;
	
	@Autowired
	CalculoPedido calculoPedidoServie;
	
	@Autowired
	PipasRepository pipaRepository;
	
	
	@Autowired
	private Environment env;
	
	@Autowired
	PedidoConektaRepository pedidoConektaRepository;
	
	@Autowired
	SmtpGmailSsl sendMail;
	
	@Autowired
	CancelacionRepository cancelacionRepository;
	
	@Autowired
	MotivoCancelacionRepository  motivoCancelacionRepository;
	
	@Autowired
	PushNotificationService pushNotificationService;
	
	@Autowired
	TipoPedidoRepository tipoPedidoRepository;
	
	@Autowired
	PedidoEvidenciaService service;
	
	@Autowired
	CalificaPedidoService calificaPedidoService;
	
	
	@Autowired
	CodigoPostalRepositoryImpl  codigoPostalRepositoryImpl;
	
	@Autowired
	ClientePromocionService clientePromocionService;
	
	@Autowired
	PrecioGasolinaService precioGasolinaService;
	

	String apiKeyRoutes;
	
	/**
	 * Author: Abraham Vargas
	 * Nuevo pedido
	 * @param pedidoP
     * @return 
	 * @throws ParseException 
	 * @throws ConectaException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws JSONException 
	 * @throws PedidoException 
	 * @throws FastGasException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public Pedido nuevoPedido(PedidoDto pedidoP) throws ParseException, ConectaException, JSONException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, PedidoException, FastGasException  {
		
		    Cliente cli;
		    conkt.setApiKey();
            BigInteger idHorarioServicio;
		    SimpleDateFormat sdfD = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		    DateTime today = new DateTime();
		    Calendar pedidoCalendar =Calendar.getInstance(); 
		    DateTimeFormatter sdf = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
		    String[] fechaHoraAP = pedidoP.getFechaPedidoS().split("-");
		    
		    DateTime fechaPedido = sdf.parseDateTime(fechaHoraAP[0]);
		    Date     pedidoDate  = sdfD.parse(fechaHoraAP[0]);
		    pedidoCalendar.setTime(pedidoDate);
		    
		    
			int dias    = Days.daysBetween(today, fechaPedido).getDays();
			int horas   = Hours.hoursBetween(today, fechaPedido).getHours() % 24;
			int minutos = Minutes.minutesBetween(today, fechaPedido).getMinutes() % 60;
			
			//titular
			Cliente titular=null;
		    
			
			if(dias<=0) {		
				   //si horas > 2
				   if(horas<2)
					   throw new  PedidoException("Estimado cliente no se puede solicitar su servicio con menos de 2 horas, favor de intentar con otro horario","FG024");
			}
			
			
			
			//le sumamos los dias en horas
			horas+=dias*24;
			
			
			//consultamos horario se dervicio
			idHorarioServicio = pedidoRepositoryImpl.getIdHorarioServicio(pedidoP.getFechaPedidoS());
			DisponibilidadPedidoDto horarioDeatail =this.calculoPedidoServie.validaDisponibilidad(idHorarioServicio,fechaHoraAP[0],pedidoP);
			
			//Validamos si el cliente tiene ya un peiddo para el mismo carro en el mismo horario
			BigInteger duplicidad = pedidoRepositoryImpl.existeDuplicidadPedidoCliente(idHorarioServicio,pedidoP);
			
			
			if(duplicidad.compareTo(new BigInteger("1"))== 0)
			      throw new FastGasException("Ya cuentas con pedido para este vehículo para la fecha solicitada.","FG033");
			
			
			
		    System.setProperty("user.timezone","Mexico/General");
            
            //Aplicamos 15% mas sobre el monto
		    Pedido pedido = new Pedido(pedidoP);
		    pedido.setIdDespachador(horarioDeatail.getIdDespachador());
		    pedido.setIdPipa(horarioDeatail.getIdPipa());
		    pedido.setIdRuta(horarioDeatail.getIdRuta());
		    
		    Random r3 = new Random(System.currentTimeMillis());
		    //ASIGNAMOS IDENTIFICADO 
            pedido.setUuid(String.valueOf(Math.abs((r3.nextInt()+1))));
            //VALIDAMOS SI EXISTE
            
		    			
		    //INICIALIZAMOS LAS LLAVES DE CONEKTA

			//1.Consultamos el idToken cliente (deberia de tener ya que dio de alta una tarjeta y en ese momento se crea el idcard conekta)
            
            
			cli = clienteService.getClienteById(pedidoP.getIdCliente().toString());
			String tokenCustomer ;
			if(cli.getIdTitular()==null) {
				
				tokenCustomer = cli.getTokenConekta();
			}else {
				
				titular = clienteService.getClienteById(cli.getIdTitular().toString());
				
				tokenCustomer = titular.getTokenConekta();	
			}
			 
			
			
			//2. Buscamos el token de la tarjeta en nuestro sistema
			 Tarjeta card = tarjetaRepository.getTarjetasById(pedidoP.getIdTarjeta());
			 String tokenCard = card.getTokenTarjeta();
			
			
		
			pedido.setIdHorarioServicio(idHorarioServicio);
			pedido.setEstatusPedido(1);
			pedido.setDetalle(pedidoP.getDetalle());
			
			//Obtenemos el tipo de pedido
			pedido.setTipoPedido(tipoPedidoRepository.getTipoPedido(pedidoP.getTipoPedidoP()));
			
			//seteamos la fecha pedido
			 pedido.setFechaPedido(pedidoCalendar);
			 //buscamos el cliente
			 Cliente cliente = clienteRepository.findClienteById(pedidoP.getIdCliente());
			 pedido.setCliente(cliente);
			pedidoRepository.save(pedido);	
			
			//registramos el uso de la promo
			 CodigoPromoCliente promoCli = new CodigoPromoCliente();
    		 promoCli.setIdCliente(pedidoP.getIdCliente());
    		 promoCli.setIdPromocion(pedidoP.getIdPromocion());
    		 clientePromocionService.save(promoCli);
			
			//Actualizamos la fecha de pedido
			pedidoRepositoryImpl.updateFechaPedido(pedido.getIdPedido(),pedidoP.getFechaPedidoS());
			
			//Consultamos el costo del servicio de acuerdo al cargo

			conkt.creaPreCompra(tokenCustomer,tokenCard,pedido);
			
			pedido.setTarjeta(card);
			
			this.sendMail.sendEmailTicketPreCompra(pedido,pedidoP.getFechaPedidoS(),null,"");
			
			//validamos si es invitado si es enviamos un email al titular
			if(titular!=null) {
				String nombreInvitado = cli.getNombre() + ((cli.getApellidoPaterno()==null) ? " " : (" "+cli.getApellidoPaterno()))+ ((cli.getApellidoMaterno()==null) ? " " : (" "+cli.getApellidoMaterno())) ;
				this.sendMail.sendEmailTicketPreCompra(pedido,pedidoP.getFechaPedidoS(),titular,nombreInvitado);
			}
				
			
			return pedido;
			
	}
	

	
	/**
	 * Author: Abraham Vargas
	 * Retorna los pedidos de un cliente
	 * @param idCliente
     * @param fechaConsulta
	 * @return
	 * @throws PedidoException 
	 */
	public List<PedidoClienteDto> getPedidosCliente(BigInteger idCliente,String fechaConsulta) throws PedidoException{
		
		  List<PedidoClienteDto> pedidosDto = new ArrayList<>();
		  List<Pedido>   pedidos=null;
		  Date fechaHoy=null, fechaPedido =null; 
		  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		  
		  //para la hora actual
		  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
		  TimeZone cdmx = TimeZone.getTimeZone("Mexico/General");
		  sdf.setTimeZone(cdmx);
		  
		  String dateHoy= sdf.format(new Date());
		  try {
			 fechaHoy = sdf.parse(dateHoy);
		     
		  } catch (ParseException e) {
			// TODO Auto-generated catch block
			 throw new PedidoException("Contacte al administrador del sistema","ADMERR");
		  }
		  
		  System.out.println("Imprimiendo fecha actual");
		  System.out.println(new Date());
			
		  if(fechaConsulta.isEmpty()) {
			  
			  pedidos = pedidoRepository.getPedidosClienteDesc(idCliente);
		  
		     //sacamos  el id de ruta
			  for(Pedido pd : pedidos) {
				  PedidoClienteDto pcd = new PedidoClienteDto(pd);
				  if(pd.getRuta()!=null) {
					  pcd.setRuta(pd.getRuta());
					  pcd.getRuta().setCodigos(null);
					  pcd.getRuta().setDespachadores(null);
				  }
				  pedidosDto.add(pcd);
			  }
		     
		     
		  }
		  else {
			  try {
				  
				  fechaPedido = formatter.parse(fechaConsulta);
			     pedidos = this.pedidoRepository.getPedidosCliente(idCliente,fechaPedido);
				//sacamos  el id de ruta
				  for(Pedido pd :pedidos) {
					  PedidoClienteDto pcd = new PedidoClienteDto(pd);
					  if(pd.getRuta()!=null) {
					    pcd.setRuta(pd.getRuta());
					    pcd.getRuta().setCodigos(null);
					    pcd.getRuta().setDespachadores(null);
					  }
					  
					  pedidosDto.add(pcd);
				  }  
			
			   } catch (ParseException e) {
				// TODO Auto-generated catch block
				   throw new PedidoException("Contacte al administrador del sistema","ADMERR");
			   }  
			   
		  }	
		  
		  return pedidosDto;
		
	}
	
	/**
	 * Author: Abraham Vargas
	 * Retorna los pedidos de un despachador
	 * @param idDespachador
     * @param fecha
	 * @return
	 * @throws ParseException 
	 */
	public List<PedidoDespachador>  getPedidosDespachador(Long idDespachador,String fecha) throws ParseException{
		
		List<Pedido> pedidos = new ArrayList<>();
		List<PedidoDespachador> pedidosDto = new ArrayList<>();
		PedidoDespachador  pedidoD;
		
		//SEGUNDA VERSION
		  Date fechaHoy ; 
		  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		  
		  //para la hora actual
		  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
		  TimeZone cdmx = TimeZone.getTimeZone("Mexico/General");
		  sdf.setTimeZone(cdmx);
		  
		  String dateHoy= sdf.format(new Date());
		  fechaHoy = sdf.parse(dateHoy);
		  
		  System.out.println("Imprimiendo fecha actual");
		  System.out.println(new Date());
			
		  if(fecha.isEmpty())
		   pedidos =this.pedidoRepository.getPedidosDespachador(idDespachador,fechaHoy);
		  else {
			  Date fechaPedido = formatter.parse(fecha);
			  pedidos =this.pedidoRepository.getPedidosDespachador(idDespachador,fechaPedido);
		  }
		
		
	
		DireccionCliente dir;
		for(Pedido p: pedidos) {
		
			dir = new DireccionCliente();
			pedidoD = new PedidoDespachador();
			Long id = new Long(idDespachador.toString());
			
			UsuarioEntity despachador= this.usuarioRepository.findDespachador(id);
			
			pedidoD.setCargaCas(p.isCargaGas());
			pedidoD.setFecha_pedido(p.getFechaPedido());
			pedidoD.setIdPedido(p.getIdPedido());
			pedidoD.setLavadoAuto(p.isLavadoAuto());
			
			if(p.getEstatusPedido().compareTo(PedidoEstatus.TRABAJO_TERMINADO.getEstatus())==0)
				pedidoD.setLitroCarga(p.getLitrosDespachados());
			else
				pedidoD.setLitroCarga(p.getLitroCarga());
				
			
			
			pedidoD.setLlantasTraserasPresion(p.getLlantasTraserasPresion());
			pedidoD.setLlantasDelanterasPresion(p.getLlantasDelanterasPresion());
			pedidoD.setMontoCarga(p.getMontoCarga());
			pedidoD.setEstatusPedido(p.getEstatusPedido());
			  
			//pedidoD
	        pedidoD.setRuta(p.getRuta().getNombre());
			pedidoD.setRevisionNeumaticos(p.isRevisionNeumaticos());
		    pedidoD.setDetalle(p.getDetalle());
		    pedidoD.setIdCombustible(p.getIdCombustible());
		    pedidoD.setUuid(p.getUuid());
		    pedidoD.setTotalPagar(p.getTotalPagar());
		    pedidoD.setTotalImporte(p.getTotalImporte());
		    pedidoD.setTotalConDescuento(p.getTotalConDescuento());
		    
		    dir = this.direccionClienteRepository.buscaDireccionClienteDespachador(p.getIdDireccion());
		    
		    System.out.println(p.getCliente().getIdCliente());
		    System.out.println("pedido"+p.getIdPedido());
		    if(dir!=null) {

			    pedidoD.setDireccion(dir);
		    }
		    
		    pedidoD.setNombreCliente(this.clienteService.getNombreCliente(p.getCliente().getIdCliente()));
		    System.out.println("seteamos nombre completo");
			
			pedidosDto.add(pedidoD);
			
		}

		
		return pedidosDto;
	
		
	}
	

	/**
	 * Author: Abraham Vargas
	 * Retorna los pedidos de un despachador
     * @param idPedido
	 * @param idDespachador
	 * @return
	 */
	public List<PedidoDetailDto>  getPedidoDetail(BigInteger idPedido){
		
	    Pedido p;
		List<PedidoDetailDto> pedidosDto = new ArrayList<>();
		List<PedidoEvidenciaEntity> evidencias;
		PedidoDetailDto  pedidoD;
		Long idDespachador;
		DireccionCliente dir;
		AutomovilCliente ac;
		Cliente cliente;
		
		//SEGUNDA VERSION
		
         
		//consultamos el id despachador
		idDespachador =this.pedidoRepository.getIdDespachadorById(idPedido);
	
	
		    p = pedidoRepository.findPedidoById(idPedido);
			pedidoD = new PedidoDetailDto();
			
			
			Long id = new Long(idDespachador.toString());
			
			UsuarioEntity despachador= this.usuarioRepository.findDespachador(id);
			
			//consultamos direccion cliente
			  dir = this.direccionClienteRepository.buscaDireccionClienteDespachador(p.getIdDireccion());
			  
			  //buscarAutoClienete
			  ac = automovilClienteService.buscarAutoClieneteById(p.getIdAutomovil());
			  
			  //buscamos a cliente del pedido
			  cliente = clienteService.getClienteById(p.getCliente().getIdCliente().toString());
			  
			//datos del automovil del servicio
			
			pedidoD.setCargaGas(p.isCargaGas());
			pedidoD.setFechaPedido(p.getFechaPedido());
			pedidoD.setIdPedido(p.getIdPedido());
			pedidoD.setLavadoAuto(p.isLavadoAuto());
			pedidoD.setLitroCarga(p.getLitroCarga());
			pedidoD.setLlantasTraserasPresion(p.getLlantasTraserasPresion());
			pedidoD.setLlantasDelanterasPresion(p.getLlantasDelanterasPresion());
			pedidoD.setMontoCarga(p.getMontoCarga());
			pedidoD.setRuta(p.getRuta().getNombre());
			pedidoD.setRevisionNeumaticos(p.isRevisionNeumaticos());
		    pedidoD.setDetalle(p.getDetalle());
		    pedidoD.setDireccion(dir);
		    pedidoD.setNombreDespachador(despachador.getNombre() +" "+despachador.getApellidoPaterno() +" "+despachador.getApellidoMaterno());
			pedidoD.setAutomovil(ac);
			pedidoD.setIdCliente(p.getCliente().getIdCliente());
			pedidoD.setTelefono(cliente.getTelefono());
			pedidoD.setNombre(cliente.getNombre());
			pedidoD.setCorreo(cliente.getCorreo());
		    pedidoD.setIdCombustible(p.getIdCombustible());
		    pedidoD.setEstatusPedido(p.getEstatusPedido());
		    pedidoD.setDistancia(p.getDistancia());
		    pedidoD.setTiempoTraslado(p.getTiempoTraslado());
		    pedidoD.setFechaHoraInicio(p.getFechaHoraInicio());
		    pedidoD.setFechaHoraFin(p.getFechaHoraFin());
		    pedidoD.setLitrosEntregados(p.getLitrosDespachados());
		    pedidoD.setLatitudDespachador(p.getLatitudDespachador());
		    pedidoD.setLongitudDespachador(p.getLongitudDespachador());
		    pedidoD.setUuid(p.getUuid());
		    pedidoD.setTotalPagar(p.getTotalPagar());
		    
		    
		    if(p.getEstatusPedido().compareTo(PedidoEstatus.CANCELADO.getEstatus())==0) {
		    	pedidoD.setMotivo(p.getCancelacion().getMotivo().getMotivo());
		    	pedidoD.setComentarioCancelacion(p.getCancelacion().getComentario());
		    	pedidoD.setFechaCancelacion(p.getCancelacion().getFechaCreacion());
		    }else if(p.getEstatusPedido().compareTo(PedidoEstatus.TRABAJO_TERMINADO.getEstatus())==0) {
		    	
		    	evidencias=service.getEvidenciasPedido(p.getIdPedido());
		    	if(evidencias!=null) {
		        for(PedidoEvidenciaEntity e: evidencias) {
		        	

		        	if(e.getMomento().name().equalsIgnoreCase("ANTES")) {
		        		
		        		pedidoD.getAntes().add(e.getIdEvidencia());
		        		
		        	}else if(e.getMomento().name().equalsIgnoreCase("DESPUES")) {
		        		pedidoD.getDespues().add(e.getIdEvidencia());
		        	}
		        	
		          }
		    	}
		    }
		    
			pedidosDto.add(pedidoD);
			
		return pedidosDto;
	
		
	}
	
	/**
	 * Author: Abraham Vargas
	 * Retorna los pedidos de un despachador
     * @param idPedido
	 * @param idDespachador
	 * @return
	 */
	public List<PedidoClienteDetailDto>  getPedidoClienteDetail(BigInteger idPedido){
		
	    Pedido p;
		List<PedidoClienteDetailDto> pedidosDto = new ArrayList<>();
		PedidoClienteDetailDto  pedidoD;
		List<PedidoEvidenciaEntity> evidencias;
		Long idDespachador;
		DireccionCliente dir;
		AutomovilCliente ac;
		Cliente cliente;
		//SEGUNDA VERSION
		//consultamos el id despachador
		idDespachador =this.pedidoRepository.getIdDespachadorById(idPedido);
	

		p = pedidoRepository.findPedidoById(idPedido);
			pedidoD = new PedidoClienteDetailDto();
			
			
			Long id = new Long(idDespachador.toString());
			
			UsuarioEntity despachador= this.usuarioRepository.findDespachador(id);
			
			//consultamos direccion cliente
			  dir = this.direccionClienteRepository.buscaDireccionClienteDespachador(p.getIdDireccion());
			  
			  //buscarAutoClienete
			  ac = automovilClienteService.buscarAutoClieneteById(p.getIdAutomovil());
			  
			  //buscamos a cliente del pedido
			  cliente = clienteService.getClienteById(p.getCliente().getIdCliente().toString());
			  
			  //Buscamos calificacion del despachador con respecto al pedido
			  Integer calificacionDespachador;
			  calificacionDespachador =this.calificaPedidoService.getCalificacionByIdPedido(p.getIdPedido());
			  
			//datos del automovil del servicio
			pedidoD.setEstatusPedido(p.getEstatusPedido());
			pedidoD.setCargaGas(p.isCargaGas());
			pedidoD.setFechaPedido(p.getFechaPedido());
			pedidoD.setIdPedido(p.getIdPedido());
			pedidoD.setLavadoAuto(p.isLavadoAuto());
			pedidoD.setLitroCarga(p.getLitroCarga());
			pedidoD.setLlantasTraserasPresion(p.getLlantasTraserasPresion());
			pedidoD.setLlantasDelanterasPresion(p.getLlantasDelanterasPresion());
			pedidoD.setMontoCarga(p.getMontoCarga());
			pedidoD.setRuta(p.getRuta().getNombre());
			pedidoD.setRevisionNeumaticos(p.isRevisionNeumaticos());
		    pedidoD.setDetalle(p.getDetalle());
		    pedidoD.setDireccion(dir);
		    pedidoD.setNombreDespachador(despachador.getNombre() +" "+despachador.getApellidoPaterno() +" "+despachador.getApellidoMaterno());
		    pedidoD.setTelDespachador(despachador.getTelefono());
		    pedidoD.setFotoUri(despachador.getFotoUri());
		    pedidoD.setIdDespachador(id);
		    pedidoD.setAutomovil(ac);
		    pedidoD.setIdCombustible(p.getIdCombustible());
		    pedidoD.setCalificado(p.getCalificado());
		    pedidoD.setUuid(p.getUuid());
		    pedidoD.setTiempoTraslado(p.getTiempoTraslado());
		    pedidoD.setDistancia(p.getDistancia());
		    pedidoD.setSubTotal(p.getSubTotal());
		    pedidoD.setTotalImporte(p.getTotalImporte());
		    pedidoD.setTotalConDescuento(p.getTotalConDescuento());
		    pedidoD.setIva(p.getIva());
		    pedidoD.setDescuento(p.getDescuento());
		    pedidoD.setTotalPagar(p.getTotalPagar());
		    pedidoD.setCalificacionDespachador(calificacionDespachador);
		    if(despachador.getPipa()!=null) {
			    pedidoD.getPipa().setPlacas(despachador.getPipa().getPlacas());
			    pedidoD.getPipa().setNumSerie(despachador.getPipa().getNumSerie());
			    pedidoD.getPipa().setNombre(despachador.getPipa().getNombre());
		    }
		    pedidoD.setCostoServicio(p.getCostoServicio());
		    pedidoD.setCostoLavado(p.getPrecioLavadoAuto());
		    pedidoD.setCostoNeumaticos(p.getPrecioPresionNeumaticos());
		    pedidoD.setTarjeta(p.getTarjeta().getNumeroTarjeta());
		    pedidoD.setPrecioPorLitro(p.getPrecioPorLitro());
		    pedidoD.setPrecioPorLitroFinal(p.getPrecioPorLitroFinal());
		    pedidoD.setLitrosDespachados(p.getLitrosDespachados());
		    pedidoD.setAlcaldiaMunicipio(p.getAlcaldiaMunicipio());
		    pedidoD.setHoraEstimada(p.getHoraEstimada());
		    pedidoD.setHoraLlegada(p.getFechaHoraInicio());
		    
		    if(p.getPromo()!=null)
		       pedidoD.setCodigoPromocion(p.getPromo().getCodigoPromocion());
		    
		    
             if(p.getEstatusPedido().compareTo(PedidoEstatus.TRABAJO_TERMINADO.getEstatus())==0) {
		    	
		    	evidencias=service.getEvidenciasPedido(p.getIdPedido());
		       if(evidencias!=null) {
		        for(PedidoEvidenciaEntity e: evidencias) {
		        	

		        	if(e.getMomento().name().equalsIgnoreCase("ANTES")) {
		        		
		        		pedidoD.getAntes().add(e.getIdEvidencia());
		        		
		        	}else if(e.getMomento().name().equalsIgnoreCase("DESPUES")) {
		        		pedidoD.getDespues().add(e.getIdEvidencia());
		        	}
		        	
		        }
		       }
		    }
		    
		    
			pedidosDto.add(pedidoD);
			
		
		return pedidosDto;
	
		
	}
	/**
	 * Author: Abraham Vargas
	 * Obtiene los horarios disponibles de acuerdo a la fecha
	 * @param fecha
     * @param cp
	 * @return
	 * @throws PedidoException 
	 */
	public List<HorarioServicioDto> disponibilidad(String fecha,String cp) throws PedidoException{
		
		RutaEntity ruta =null;
	    UsuarioEntity  despachador= null;
	    List<RutaEntity> rutas = this.rutaService.getRutasActivas(cp);
	    List<UsuarioEntity> despachadoresActivos ;
	    List<UsuarioEntity> hayDespachadores= new ArrayList<>();
	    
	    if(rutas==null)
	    throw new  PedidoException("FG005","Sin cobertura");
	    
	    if(rutas.size()==0)
		    throw new  PedidoException("FG005","Sin cobertura");
	    
	    
	    //Validamos que al menos tengamos un despachador
	    for(RutaEntity r  :rutas) {
	  			
	  			if(r.getDespachadores()!=null) {
	  				for(UsuarioEntity d : r.getDespachadores()) {
	  					hayDespachadores.add(d);		
	  				}
	  			}
	  		}
		
		if(hayDespachadores.size()==0)
			throw new  PedidoException("FG021","La ruta no cuenta con despachadores en este momento");
		
		//Validamos que solo sean despachadores activos y autotanques activos
		despachadoresActivos= hayDespachadores.stream().filter(des ->  des.getEstatus().getEstatus().equalsIgnoreCase(UsuarioEstatus.ACTIVO.getEstatus())
				&& des.getPipa().getEstatus().equalsIgnoreCase(PipaEstatusEnum.ACTIVO.getEstatus()))
		.collect(Collectors.toList());
		
		if(despachadoresActivos==null || despachadoresActivos.isEmpty())
			throw new  PedidoException("FG021","La ruta no cuenta con despachadores en este momento");
		

		
		return this.pedidoRepositoryImpl.getOcupabilidad(fecha,despachadoresActivos);
	}
	
	/**
	 * Author: AbRaHaM VaRgAs GaRcIa
	 * @param cancelar
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public void  cancelaPedidoCliente(CancelarServicioDto cancelar) throws NumberFormatException, Exception {
		
		 conkt.setApiKey();
		  String fechaPedido;
		  DateTime fechaPj;
		  PedidoConekta pconekta;
		  Cancelacion cancela;
		  CatMotivoCancelacion motivo;
		  CancelacionEmailDto cancelacionDto;
		  //para la hora actual
		  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			
		  sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		  
	
		//1. Primero validamos que el pedido no se encuentre en estus (Servicio Iniciado)
		 Pedido pedido = this.pedidoRepository.findPedidoById(cancelar.getIdPedido());
		 //obtenemos el id del token
		 pconekta = pedidoConektaRepository.finById(pedido.getIdPedido());
		 
		 
		 System.out.println(sdf.format(pedido.getFechaCreacion().getTime()));
		 System.out.println(sdf.format(pedido.getFechaPedido().getTime()));
		 //System.out.println(pedido.getFechaPedido().getTimeZone().getDefault().getRawOffset());
		 
		 fechaPedido = sdf.format(pedido.getFechaPedido().getTime());
		 sdf.setTimeZone(TimeZone.getTimeZone("Mexico/General"));
		 Date pedidoDate = sdf.parse(fechaPedido);
		 
		 //Fecha de hoy y de pedido en formato joda
		 DateTime todayJ = new DateTime(new Date());
		 fechaPj = new DateTime(pedidoDate);
		 
		 int dias    = Days.daysBetween(todayJ, fechaPj).getDays();
		 int horas   = Hours.hoursBetween(todayJ, fechaPj).getHours() % 24 ;
		 int minutos = Minutes.minutesBetween(todayJ, fechaPj).getMinutes() % 60;
		 float timeToDoService;
		 
		 
		 System.out.println(dias);
		 System.out.println(horas);
		 System.out.println(minutos);
		 
		 if(dias<0)
			 throw new PedidoException("No se puede cancelar un pedido pasado","FG034");
		 if(dias==0) {
		   if(horas<0 || minutos <0)
		   throw new PedidoException("No se puede cancelar un pedido pasado","FG034");
		 }
		 horas+=dias*24;
	
		 if(minutos < 0) {
			 
			 horas+=-1;
			 minutos= 60 + minutos;
			 
			 timeToDoService=Float.parseFloat(String.valueOf(horas)+"."+String.valueOf(minutos));
			 
		 }else {
			 
			  //unimos horas y minutos para tomarlos como un valor flotante para comparar
			 timeToDoService=Float.parseFloat(String.valueOf(horas)+"."+String.valueOf(minutos)); 	 
		 }
		
		 
		 boolean seCancela   = (pedido.getEstatusPedido().compareTo(PedidoEstatus.ASIGNADO.getEstatus()))== 0 
				  || (pedido.getEstatusPedido().compareTo(PedidoEstatus.EN_CAMINO.getEstatus()))== 0
				 ? true : false ;
		 
		 
		 /* Consultamos tarifas */
		 List<Parametro> tarifasProrrogas=  parametroRepository.costosPorServicioAndTimepoProrroga();
		 
		 /* Obtenemos tiempos de cancelacion */
		 List<Parametro> tiposPedido=  parametroRepository.tiemposCancelacionService();
		 if(seCancela) {
			 
			 //Actualizamos la orden a cancelada
			 pedidoRepository.updatePedido(PedidoEstatus.CANCELADO.getEstatus(), pedido.getIdPedido());
			 
			 //Consultamos el motivo de cancelacion 
			 motivo = motivoCancelacionRepository.getById(cancelar.getIdMotivo());
			 
			 //Guardamos le detalle de cancelacion
			 cancela = new Cancelacion(pedido,motivo,cancelar.getDetalle());
			 cancelacionRepository.save(cancela);
			 
			 
			 //PEDIDO PROGRAMADO
			 if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())==0) {
				 
				 System.out.println("CANCELAR PROGRAMADO");
				 
				 //consultamos el tiempo permitido para  cancelacion
				 cancelacionDto = new CancelacionEmailDto();
				 
				 Parametro tiempoCancelacion = tiposPedido.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_PROGRAMADO_TIEMPO_CANCELACION.getValor()))
		        	    	.findAny()
		        	    	.orElse(null);
				 
				 float tcancelar = Float.parseFloat(tiempoCancelacion.getValor());
				 
				 if(timeToDoService>=tcancelar) {
					 
					 System.out.println("CANCELANDO");
					 //CANCELANDO SIN PENALIZACION AL USUARIO
					 conkt.cancelarOrdenPreAutorizada(pconekta.getIdOrden());
					 
					 //Cobramos el dinero
					 //conkt.cierraOrden(pconekta.getIdOrden());
					
				 }else {
					 System.out.println("CANCELANDO CON COBRO");
					  //Consultamos el monto de penalizacion
					 
					  Parametro tarifa = tarifasProrrogas.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_CANCELACION_PROGRAMADO.getValor()))
			        	    	.findAny()
			        	    	.orElse(null);
					  
					 //aplicamos la devolucion
					  conkt.devolucionOrdenPenazila(pconekta.getIdOrden(), pedido.getTotalPagar(), Double.parseDouble(tarifa.getValor()));
					  cancelacionDto.setPenalizacion(true);
					  cancelacionDto.setMontoPenalizacion(tarifa.getValor());
					 
				 }
				 
				 sendMail.sendEmailCancelar(pedido,cancelacionDto);
				 
			 }
			 //PEDIDO MISMO DIA
			 else if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.MISMO_DIA.getId())==0) {

				 System.out.println("CANCELAR MISMO DIA");
				 cancelacionDto = new CancelacionEmailDto();
				 //consultamos el tiempo permitido para  cancelacion
				 
				 Parametro tiempoCancelacion = tiposPedido.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_MISMO_DIA_TIEMPO_CANCELACION.getValor()))
		        	    	.findAny()
		        	    	.orElse(null);
				 
                 float tcancelar = Float.parseFloat(tiempoCancelacion.getValor());
				 
				 if(timeToDoService>=tcancelar) {
					 
					 System.out.println("CANCELANDO SIN COBRO");
					 
					 conkt.cancelarOrdenPreAutorizada(pconekta.getIdOrden());
					
				 }else {
					 System.out.println("CANCELANDO CON COBRO");
					 
					 //Cobramos el dinero
					  conkt.cierraOrden(pconekta.getIdOrden());
					  //Consultamos el monto de penalizacion
					  
					  Parametro tarifa = tarifasProrrogas.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_CANCELACION_MISMO_DIA.getValor()))
			        	    	.findAny()
			        	    	.orElse(null);
					  
					 //aplicamos la devolucion
					  conkt.devolucionOrdenPenazila(pconekta.getIdOrden(), pedido.getTotalPagar(), Double.parseDouble(tarifa.getValor()));
					  cancelacionDto.setPenalizacion(true);
					  cancelacionDto.setMontoPenalizacion(tarifa.getValor());
				 }
				 cancelacionDto.setComentarioCancelacion(cancelar.getDetalle());
				 sendMail.sendEmailCancelar(pedido,cancelacionDto);
				 
			 }
			 //PEDIDO URGENTE
			 else if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.URGENTE.getId())==0){
				 
				 System.out.println("CANCELAR URGENTE");
				 cancelacionDto = new CancelacionEmailDto();
				 //consultamos el tiempo permitido para  cancelacion
				 Parametro tiempoCancelacion = tiposPedido.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.PEDIDO_URGENTE_TIEMPO_CANCELACION.getValor()))
		        	    	.findAny()
		        	    	.orElse(null);
				 
				 float tcancelar = Float.parseFloat(tiempoCancelacion.getValor());
				 
				 if(timeToDoService>=tcancelar) {
					 System.out.println("CANCELANDO");
					 conkt.cancelarOrdenPreAutorizada(pconekta.getIdOrden());
					 
					 
				 }else {
					 System.out.println("CANCELANDO CON COBRO");
					 
					 //Cobramos el dinero
					 conkt.cierraOrden(pconekta.getIdOrden());
					  //Consultamos el monto de penalizacion
					  
					  Parametro tarifa = tarifasProrrogas.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_CANCELACION_URGENTE.getValor()))
			        	    	.findAny()
			        	    	.orElse(null);
					  
					 //aplicamos la devolucion
					  conkt.devolucionOrdenPenazila(pconekta.getIdOrden(), pedido.getTotalPagar(), Double.parseDouble(tarifa.getValor()));
					  cancelacionDto.setPenalizacion(true);
					  cancelacionDto.setMontoPenalizacion(tarifa.getValor());
					  
					 
				 }
				 cancelacionDto.setComentarioCancelacion(cancelar.getDetalle());
			
				 sendMail.sendEmailCancelar(pedido,cancelacionDto);
			 }
			 
			 //le regresamos los litros a la pipa 
			 //acedemos a la pipa obtenemos sus litros restantes  y los sumamos a los litros carga del pedido
			 //de acuerdo al tipo de combustible solicitado
			 Pipa pipa = pipaRepository.findPipaById(pedido.getIdPipa());
		     if(pedido.getIdCombustible().compareTo(TipoCombustible.DIESEL.getCombustible())==0 && (pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
				 
				Double litersComback = new Double(pedido.getLitroCarga())+pipa.getLitrosRestantesDiesel();
				this.pipaRepository.updateLitrosRestantesDiesel(litersComback, pedido.getIdPipa());
				 
			 }
		     if(pedido.getIdCombustible().compareTo(TipoCombustible.REGULAR.getCombustible())==0 && (pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
				 
					Double litersComback = new Double(pedido.getLitroCarga())+pipa.getLitrosRestantesRegular();
					this.pipaRepository.updateLitrosRestantesRegular(litersComback, pedido.getIdPipa());
					 
			}
		    if(pedido.getIdCombustible().compareTo(TipoCombustible.PREMIUM.getCombustible())==0 && (pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
				 
					Double litersComback = new Double(pedido.getLitroCarga())+pipa.getLitrosRestantesPremium();
					this.pipaRepository.updateLitrosRestantesPremium(litersComback, pedido.getIdPipa());
					 
		    }
			 
			 //ENVIAMOS A NOTIFICACION AL DESPACHADOR DEL SERVICIO CANCELADO
			 pushNotificationService.pushCancelaServiceToDespachador(pedido);
			 
		 }
		 //NO SE CANCELA TRABANO INICIADO
		 else if(seCancela==false) {
			 throw new  PedidoException("No se puede cancelar un servicio iniciado,finalizado o cancelado","FG015");
		 }
		 
		
	}
	
	/**
	 * Author: AbRaHaM VaRgAs
	 * Cancela el pedido el despachador
	 * @param cancelar
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	public void  cancelaPedidoDespachador(CancelarServicioDto cancelar) throws NumberFormatException, Exception {
		
		CancelacionEmailDto cancelacionDto = new CancelacionEmailDto();
		conkt.setApiKey();
		PedidoConekta pConekta;
		Cancelacion cancela;
		CatMotivoCancelacion motivo;
		pConekta = pedidoConektaRepository.finById(cancelar.getIdPedido());
		//1. Primero validamos que el pedido no se encuentre en estus (Servicio Iniciado o finalizado)
		 Pedido pedido = this.pedidoRepository.findPedidoById(cancelar.getIdPedido());
		 List<Parametro> penalizaciones = this.parametroRepository.costosPorServicioAndTimepoProrroga();
		 Parametro penalizacion =null;
		
		if(MotivoCancelacionPenalizaEnum.AUTO_CERRADO.getValor().compareTo(cancelar.getIdMotivo())==0 
				||MotivoCancelacionPenalizaEnum.NO_HAY_AUTOMOVIL.getValor().compareTo(cancelar.getIdMotivo())==0) {
			
			cancelacionDto.setPenalizacion(true);
			conkt.cierraOrden(pConekta.getIdOrden());
			//De acuerdo al tipo de pedido aplicamos penalizacion
			
			if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())==0) {
				//Busacamos el monto a cobrar
			  penalizacion =	penalizaciones.stream().filter(p -> p.getNombre().contains(PedidoVarios.TARIFA_CANCELACION_PROGRAMADO.getValor()))
				   .findAny()
				   .orElse(null);
			  System.out.println(penalizacion);
			  
			  
			  conkt.devolucionOrdenPenazila(pConekta.getIdOrden(), pedido.getTotalPagar(), Double.parseDouble(penalizacion.getValor()));
			}
            if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.MISMO_DIA.getId())==0) {
            	
            	penalizacion =	penalizaciones.stream().filter(p -> p.getNombre().contains(PedidoVarios.TARIFA_CANCELACION_MISMO_DIA.getValor()))
     				   .findAny()
     				   .orElse(null);
            	
            	conkt.devolucionOrdenPenazila(pConekta.getIdOrden(), pedido.getTotalPagar(), Double.parseDouble(penalizacion.getValor()));
            	//System.out.println(penalizacion);
				
			}
            if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.URGENTE.getId())==0) {
            	
            	penalizacion =	penalizaciones.stream().filter(p -> p.getNombre().contains(PedidoVarios.TARIFA_CANCELACION_URGENTE.getValor()))
     				   .findAny()
     				   .orElse(null);
            	System.out.println(penalizacion);
            	conkt.devolucionOrdenPenazila(pConekta.getIdOrden(), pedido.getTotalPagar(), Double.parseDouble(penalizacion.getValor()));
			}
			
          //Consultamos el motivo de cancelacion 
			 motivo = motivoCancelacionRepository.getById(cancelar.getIdMotivo());
          //Guardamos le detalle de cancelacion
			 cancela = new Cancelacion(pedido,motivo,cancelar.getDetalle());
			 cancelacionRepository.save(cancela);
			 cancelacionDto.setMontoPenalizacion(penalizacion.getValor());
			
			
		}else {
			
			//Cancelamos la orden
			conkt.cancelarOrdenPreAutorizada(pConekta.getIdOrden());
			cancelacionDto.setPenalizacion(false);
			cancelacionDto.setIdServicio(pedido.getUuid());
			
	          //Consultamos el motivo de cancelacion 
		    motivo = motivoCancelacionRepository.getById(cancelar.getIdMotivo());
			cancela = new Cancelacion(pedido,motivo,cancelar.getDetalle());
			cancelacionRepository.save(cancela);
			
		}
		String noti = (PedidoMensajes.SERVICIO_CANCELADO_DESPACHADOR_CLIENTE.getValor().replace("XXXXXXX",pedido.getUuid().toString()));
		//Actualizamos la orden a cancelada
		pedidoRepository.updatePedido(PedidoEstatus.CANCELADO.getEstatus(), pedido.getIdPedido());
		pushNotificationService.pushCancelaDespachador(pedido,noti);
		cancelacionDto.setComentarioCancelacion(cancelar.getDetalle());
		sendMail.sendEmailCancelarDespachador(pedido,cancelacionDto);
		//regresamos los litros
		 //le regresamos los litros a la pipa 
		 //acedemos a la pipa obtenemos sus litros restantes  y los sumamos a los litros carga del pedido
		 //de acuerdo al tipo de combustible solicitado
		 Pipa pipa = pipaRepository.findPipaById(pedido.getIdPipa());
	     if(pedido.getIdCombustible().compareTo(TipoCombustible.DIESEL.getCombustible())==0 && (pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
			 
			Double litersComback = new Double(pedido.getLitroCarga())+pipa.getLitrosRestantesDiesel();
			this.pipaRepository.updateLitrosRestantesDiesel(litersComback, pedido.getIdPipa());
			 
		 }
	     if(pedido.getIdCombustible().compareTo(TipoCombustible.REGULAR.getCombustible())==0 && (pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
			 
				Double litersComback = new Double(pedido.getLitroCarga())+pipa.getLitrosRestantesRegular();
				this.pipaRepository.updateLitrosRestantesRegular(litersComback, pedido.getIdPipa());
				 
		}
	    if(pedido.getIdCombustible().compareTo(TipoCombustible.PREMIUM.getCombustible())==0 && (pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0)) {
			 
				Double litersComback = new Double(pedido.getLitroCarga())+pipa.getLitrosRestantesPremium();
				this.pipaRepository.updateLitrosRestantesPremium(litersComback, pedido.getIdPipa());
				 
	    }
		 
		
		
	}
	
	
	/**
	 * Author: Abraham Vargas
	 * Actualiza el estatus del pedido
	 * @param idPedido
	 * @param idEstaus
     * @param ubicacion
	 * @throws ConectaException 
	 */
	public void updateStatus(Integer idEstaus,BigInteger idPedido,UbicacionDespachadorRequestDto ubicacion) throws ConectaException {
		
		 Pedido pedido = this.pedidoRepository.findPedidoById(idPedido);
		 Calendar horaEstimada = Calendar.getInstance();
		 
		 recoverEnviroment();
		 
		 
		 DireccionCliente dir = pedido.getCliente().getDirecciones().stream().filter(direccion -> direccion.getIdDireccion().compareTo(pedido.getIdDireccion())==0)
				 .findAny()
				 .orElse(null);
		   
		  
		 if(idEstaus.compareTo(PedidoEstatus.EN_CAMINO.getEstatus())==0) {
				
			 String tiempoTraslado,distanciaRecorrida;
			//1a recibimos latitud y longitud del despachador para calcular tiempo de traslado y recorrido(km)
			 if(ubicacion!=null) {
				 
				 System.out.println(ubicacion.getLatitud());
				 System.out.println(ubicacion.getLongitud());
				
				//Primeri va la latitud y luego la longitud
				 DistanceMatrix result =DistanceMatrixApi.newRequest(new GeoApiContext.Builder().apiKey("AIzaSyBx5VsyxO2kPtxaK4oykrnnRm_iJnjRdAs").build())
			          .origins(new LatLng(new Double(ubicacion.getLatitud()),new Double(ubicacion.getLongitud())))
			          .destinations(new LatLng(dir.getLatitud(),dir.getLongitud()))
			          .language("es")
			          .awaitIgnoreError();

				 for(DistanceMatrixRow e  :result.rows) {
					 for(DistanceMatrixElement de : e.elements) {
						 
						 Duration  du = de.duration;
						 Distance  d  = de.distance;        
						 System.out.println(de);
						 long duracion = du.inSeconds;
						 System.out.println("Duracion");
						 System.out.println(duracion);
						 System.out.println("tiempo actual");
						 System.out.println(System.currentTimeMillis());
						 System.out.println("Tiempo estimado");
						 long tiempoEstimado =  System.currentTimeMillis()+(duracion*1000); 
						 System.out.println(tiempoEstimado);
						 
						 horaEstimada.setTimeInMillis(tiempoEstimado);
						 System.out.println(horaEstimada);
						 
						 this.pedidoRepository.updateDurationAndDistance(du.humanReadable,d.humanReadable,horaEstimada, idPedido);
						 break;
					 }
					 System.out.println(e);
					 
				 }
				 
				 
				 //guardamos la ubicacion para que los moviles puedan mostrar en su historico
				 this.pedidoRepository.saveLatAndLong(ubicacion.getLatitud(),ubicacion.getLongitud(),idPedido);
			  }
			 //1a end
			 
			 
			String teArrivo = pedido.getTiempoTraslado();
			//SERVICIO EN CAMINO
			 String noti = (PedidoMensajes.SERVICIO_EN_CAMINO.getValor().replace("XXXXXXXXXX",pedido.getUuid().toString())).
					       replace("YYYYY", teArrivo);
			 pushNotificationService.pushUpdatePedidoCliente(pedido,noti);
			 
	
		 }
		 if(idEstaus.compareTo(PedidoEstatus.TRABAJO_INICIADO.getEstatus())==0) {
				
			 //PUSH INICIO DE SERVICIO
			 String noti = PedidoMensajes.SERVICIO_INICIADO.getValor().replace("XXXXXXXXXX",pedido.getUuid().toString());
			 pushNotificationService.pushUpdatePedidoCliente(pedido,noti);
			 
			 //Guardamos hora de inicio del servicio
			 this.pedidoRepository.updateHoraInicio(Calendar.getInstance(), pedido.getIdPedido());
	
		 }
	
		
		this.pedidoRepository.updatePedido(idEstaus, idPedido);
		
	}
	
	/**
	 * Author: Abraham Vargas
	 * Finaliza el servicio
	 * @param pedidoP
     * @return 
	 * @throws ConectaException
	 * @throws PedidoException 
	 */
	@Transactional
	public TicketFinalDto finalizarPedido(FinalizarCompraRequestDto pedidoP) throws ConectaException, PedidoException {
		
		conkt.setApiKey();
		PedidoConekta pconekta;
		TicketFinalDto ticket=null;		
		double totalImporte=0,servicioGasolina=0,totalConDescuento=0,descuento=0,descuentoA=0,descuentoTotal=0,precioLavado=0,precioRevision=0;
		double totalAPagar=0,subTotal=0,iva=0;

		//obtenemos el id del token
		 pconekta = pedidoConektaRepository.finById(pedidoP.getIdPedido());
		 Pedido pedidoP2 = this.pedidoRepository.findPedidoById(pedidoP.getIdPedido());
		 
		 if( new Double(pedidoP2.getLitroCarga()).compareTo(new Double(pedidoP.getLitros()))<0)
			   throw new PedidoException("Los litros ingresados son mayores a los que solicito el cliente.","FG037");
			  
		 
		 
		 List<Parametro> costos=this.parametroRepository.costosPorServicioAndTimepoProrroga();
		 Pedido pedido = new Pedido(pedidoP2); 
		 DireccionCliente dc = direccionClienteRepository.buscaDireccionClienteDespachador(pedido.getIdDireccion());
	
		//consultamos el precio por litro de ese dia
		 PrecioGasolinaRequestDto alcEdo =precioGasolinaService.getAlcEdoByCp(dc.getCp());
		 alcEdo.setIdCombustible(pedido.getIdCombustible());
		 
		 //consultamos el precio promedio
		 Double precioPorLitro =precioGasolinaService.getPrecioPromAlcMun(alcEdo.getIdEstado(),alcEdo.getIdMunicipio(),alcEdo.getIdCombustible());
		 
		 /* CONSULTAMOS EL COSTO POR SERVICIO */
		 Parametro costoService=null;
		 
		 
		 if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())==0) {
			  costoService = costos.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_PROGRAMADO.getValor()))
				    	.findAny()
				    	.orElse(null);	 
			 
		 }
		 else if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.MISMO_DIA.getId())==0 ){
			 costoService = costos.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_MISMO_DIA.getValor()))
				    	.findAny()
				    	.orElse(null);	 
		 }
		 else if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.URGENTE.getId())==0){
			 costoService = costos.stream().filter(parametro -> parametro.getNombre().contains(PedidoVarios.TARIFA_URGENTE.getValor()))
				    	.findAny()
				    	.orElse(null);	 
			 
		 }
		  
		 double costoPedido=Float.parseFloat(costoService.getValor());
		 
        boolean esDescuentoresult =false;
        
        if(pedido.getPromo()!=null) 
        	esDescuentoresult = (pedido.getPromo().getDescuento()!=null) ? true : false;
        
		 
        totalImporte+=costoPedido;
        //PRIMERO SACAMOS DESCUENTO
		 if(pedido.getPromo()!=null) {
			 Set<ProductoPromocion>  promoProd = pedido.getPromo().getProductosPromo();	 
		    
			 System.out.println(pedido.getCombustible().getCombustible());
			 
			 //BUSCAMOS PROMO PARA CARGA DE GASOLINA
			 ProductoPromocion promoGas= promoProd.stream().filter(prod -> prod.getProductoServicio().getId().compareTo(new Long(pedido.getIdCombustible()))==0)
			    .findAny()
			    .orElse(null);
			
			 if(promoGas!=null) {
				  
				 if(esDescuentoresult) {
				  descuento = Double.parseDouble(pedido.getPromo().getDescuento());
				  descuentoA = (descuento*Double.parseDouble(pedidoP.getMonto()))/100; 
				  descuentoTotal += descuentoA;
			    }
			  else {
				  if(new Double(pedidoP.getMonto())>pedido.getPromo().getMonto()) {
					  descuento = pedido.getPromo().getMonto();
					  descuentoTotal += descuento;
				  }
			   }
			 }
			
			//BUSCAMOS PROMO LAVADO DE AUTO
			 ProductoPromocion promoLavado= promoProd.stream().filter(prod -> prod.getProductoServicio().getId().compareTo(new Long(ProductosEnum.LAVADO_AUTO.getValor()))==0)
			    .findAny()
			    .orElse(null);
			
			 //SI TIENE PROMO VALIDAMOS SI ES POR DESCUENTO O POR MONTO
			 if(promoLavado!=null) {
				  //descuento %
				 if(esDescuentoresult) {
					 
					 if(pedido.isLavadoAuto()) {
						  precioLavado = (pedido.getPrecioLavadoAuto()==null) ? 0 : pedido.getPrecioLavadoAuto() ;
							 //Validamos si existe descuento para lavado de auto solicitado
						  descuento = Double.parseDouble(pedido.getPromo().getDescuento());
						  descuentoA= ((descuento*precioLavado)/100); 
						  descuentoTotal += descuentoA;
				    }
			    }
				//descuento por monto
			  else {
					 if(pedido.isLavadoAuto()) {
						 precioLavado = (pedido.getPrecioLavadoAuto()==null) ? 0 : pedido.getPrecioLavadoAuto() ;
						 if(precioLavado>pedido.getPromo().getMonto()) {
						 //Validamos si existe descuento para lavado de auto solicitado
						  descuento = pedido.getPromo().getMonto();
						  descuentoTotal += descuento;
						 }
				    }
			   }
			 }
			 
				
			//BUSCAMOS PROMO LAVADO DE AUTO
			 ProductoPromocion promoNeumaticos= promoProd.stream().filter(prod -> prod.getProductoServicio().getId().compareTo(new Long(ProductosEnum.REVISION_NEUMATICOS.getValor()))==0)
			    .findAny()
			    .orElse(null);
			 
			 //SI TIENE PROMO VALIDAMOS SI ES POR DESCUENTO O POR MONTO
			 if(promoNeumaticos!=null) {
				 double neumaticosD= (pedido.getPrecioPresionNeumaticos()==null) ? 0 : pedido.getPrecioPresionNeumaticos();
				  //descuento %
				 if(esDescuentoresult) {
					 
					 if(pedido.isRevisionNeumaticos() && neumaticosD>0) {
						 precioRevision = (pedido.getPrecioPresionNeumaticos()==null) ? 0 : pedido.getPrecioPresionNeumaticos() ;
							 //Validamos si existe descuento para lavado de auto solicitado
						  descuento = Double.parseDouble(pedido.getPromo().getDescuento());
						  descuentoA= ((descuento*precioRevision)/100); 
						  descuentoTotal += descuentoA;
				    }
			    }
				//descuento por monto
			  else {
					 if(pedido.isRevisionNeumaticos() && neumaticosD>0 ) {
						 if(neumaticosD>pedido.getPromo().getMonto()) {
							 precioRevision = (pedido.getPrecioPresionNeumaticos()==null) ? 0 : pedido.getPrecioPresionNeumaticos() ;
							 //Validamos si existe descuento para lavado de auto solicitado
							  descuento = pedido.getPromo().getMonto();
							  descuentoTotal += descuento;
						 }
				    }
			   }
			 }
			 
			 
		 }
		
		 
		 
		 if(pedido.isLavadoAuto()) {
			 precioLavado = (pedido.getPrecioLavadoAuto()==null) ? 0 : pedido.getPrecioLavadoAuto() ;
			 //Validamos si existe descuento para lavado de auto solicitado
			 totalImporte += precioLavado;
		 }
		if(pedido.isRevisionNeumaticos()) {
				
			precioRevision = (pedido.getPrecioPresionNeumaticos()==null) ? 0 : pedido.getPrecioPresionNeumaticos() ;
			totalImporte+=precioRevision;
		}
		 
		 servicioGasolina =Double.parseDouble(pedidoP.getMonto());
		 totalImporte               += servicioGasolina;
		 totalConDescuento          = totalImporte-descuentoTotal;
		 subTotal                   = totalConDescuento/1.16;
		 iva                        = totalConDescuento-subTotal;
		 totalAPagar                = subTotal+iva;
		 
         /*FORMAT VALUES*/
		 Double precioLavadoF       = new Double(String.format("%.2f", precioLavado));
		 Double montoCargaF         = new Double(String.format("%.2f", Double.parseDouble(pedidoP.getMonto())));
		 Double montoNeumaticos     = new Double(String.format("%.2f", precioRevision));
         Double costoServicioF      = new Double(String.format("%.2f", costoPedido));
         
         //importes totales
         totalImporte               = new Double(String.format("%.2f", totalImporte));
         
         Double subTotalF           = new Double(String.format("%.2f", subTotal));
         Double ivaPagarF           = new Double(String.format("%.2f", iva));
         Double descuentoTotalF     = new Double(String.format("%.2f", descuentoTotal));
         
         Double litrosReales        = Double.parseDouble(pedidoP.getLitros());
         
         precioPorLitro             = new Double(String.format("%.2f", precioPorLitro));
         
         totalConDescuento          = new Double(String.format("%.2f", totalConDescuento));
         
         Double totlaPagarF         = new Double(String.format("%.2f", totalAPagar));


         
         
			//si el estatus es  4(Trabajo terminado)
		  if(pedido.getEstatusPedido().compareTo(PedidoEstatus.TRABAJO_INICIADO.getEstatus())==0) {
					
			  
				Cliente cli = clienteService.getClienteById(pedido.getCliente().getIdCliente().toString());
				String tokenCustomer ;
				if(cli.getIdTitular()==null) {
					
					tokenCustomer = cli.getTokenConekta();
				}else {
					
					Cliente titular;
					titular = clienteService.getClienteById(cli.getIdTitular().toString());
					
					tokenCustomer = titular.getTokenConekta();	
				}
				 
				
				
				//2. Buscamos el token de la tarjeta en nuestro sistema
				 Tarjeta card = tarjetaRepository.getTarjetasById(pedido.getIdTarjeta());
				 String tokenCard = card.getTokenTarjeta();
			  

				 
				 String noti = PedidoMensajes.SERVICIO_TERMINADO.getValor().replace("XXXXXXX",pedido.getUuid().toString());
				 //Mandamos NORTIFICACION DE CIERRE
				 pushNotificationService.pushUpdatePedidoCliente(pedido,noti);
				 
				 
				 ticket = new TicketFinalDto();
				 if(pedido.getPromo()!=null)
				 ticket.setCodigoPromo(pedido.getPromo().getCodigoPromocion());
				 
				 ticket.setIdCombustible(pedido.getIdCombustible());
				 ticket.setLavado(pedido.isLavadoAuto());
				 ticket.setRevNeumaticos(pedido.isRevisionNeumaticos());
				 ticket.setCostoServicio(costoServicioF);
                 ticket.setLitros(pedidoP.getLitros());
                 ticket.setPrecioPorlitro(precioPorLitro);
				 ticket.setIvaPagar(ivaPagarF);
				 ticket.setMontoCarga(montoCargaF);
				 ticket.setMontoLavado(precioLavadoF);
				 ticket.setMontoNeumaticos(montoNeumaticos);
				 ticket.setTarjeta(pedido.getTarjeta().getNumeroTarjeta());
				 ticket.setDescuento(descuentoTotalF);
				 ticket.setTotalImporte(totalImporte);
				 ticket.setTotalConDescuento(totalConDescuento);
				 ticket.setSubTotal(subTotalF);
				 ticket.setTotalPagar(totlaPagarF);
				 String alcMuncipio = codigoPostalRepositoryImpl.getMunicipioByCp(dc.getCp());
				 ticket.setAlcMun(alcMuncipio);
				 
				 
				 
				 //Posibles excepciones
				 //La tarjeta: no tenga suficientes fondos, invalida, bloqueada 
				 //Por lo tanto el servicio queda finalizado y no podemos realizar ningun cobro
				 
				 try {

					 //verificamos el monto de compra de gasolina
					 //hacemos una resta del monto Incial y monto final y lo restamos
					 //lo aplicamos como descuentos
					 conkt.cierraOrden(pconekta.getIdOrden());
					 conkt.ajustaOrdenFinal(pconekta.getIdOrden(),pedido.getTotalPagar(),new Double(ticket.getTotalPagar()));
					 //creamos una nuevav
					 pedido.setMontoCarga(ticket.getMontoCarga());
					 pedido.setDescuento(ticket.getDescuento());

					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new PedidoException("Contacte al administrador del sistema", "ADMERR");
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new PedidoException("Contacte al administrador del sistema", "ADMERR");
					
				} 
				 sendMail.sendEmailTicket(ticket,pedido);
		
			}else {
				
				throw new PedidoException("No se puede finalizar un servicio que no se encuentra en estatus (Servicio iniciado)","FG016");
			}
		  
		  
		
		this.pedidoRepository.updatePedido(PedidoEstatus.TRABAJO_TERMINADO.getEstatus(), pedido.getIdPedido());
		//Guardamos hora de fin del servicio
		this.pedidoRepository.updateHoraFin(Calendar.getInstance(), pedido.getIdPedido());
		//Guardamos hora de fin del servicio
		 this.pedidoRepository.updateLitrosDespachados(pedidoP.getLitros(), pedido.getIdPedido());
		 //update iva, total, subtotal, total a pagar
		 this.pedidoRepository.updateCompra(ticket.getMontoCarga(),ticket.getIvaPagar(), ticket.getSubTotal(), ticket.getTotalImporte(), ticket.getTotalPagar(),ticket.getDescuento(),ticket.getTotalConDescuento(),ticket.getPrecioPorlitro(), pedido.getIdPedido());
		
		try {
			Pipa pipa= pipaRepository.findPipaById(pedido.getIdPipa());
		
			//Les restamos los litros depachados a la pipa
			if(pedido.getIdCombustible().compareTo(TipoCombustible.DIESEL.getCombustible())==0) {
				
				if(pipa.getLitrosRestantesDiesel()==null)
					throw new PedidoException("No cuenta con combustible suficiente la pipa","FG023");
				
				 if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0){
					 Double diferenciaLitros =  new Double(pedido.getLitroCarga())-new Double(pedidoP.getLitros());
					this.pipaRepository.updateLitrosRestantesDiesel(pipa.getLitrosRestantesDiesel()+diferenciaLitros, pedido.getIdPipa());	 
				 }else {
					  
					  this.pipaRepository.updateLitrosRestantesDiesel(pipa.getLitrosRestantesDiesel()-new Double(pedidoP.getLitros()), pedido.getIdPipa()); 
					 
				 }
				
				
			}
	        if(pedido.getIdCombustible().compareTo(TipoCombustible.PREMIUM.getCombustible())==0) {
	        	
	        	if(pipa.getLitrosRestantesPremium()==null)
	        		throw new PedidoException("No cuenta con combustible suficiente la pipa","FG023");
	        	if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0){
	        		Double diferenciaLitros =  new Double(pedido.getLitroCarga())-new Double(pedidoP.getLitros());
		            this.pipaRepository.updateLitrosRestantesPremium(pipa.getLitrosRestantesPremium()+diferenciaLitros, pedido.getIdPipa());	
	        	}else {
		            this.pipaRepository.updateLitrosRestantesPremium(pipa.getLitrosRestantesPremium()- new Double(pedidoP.getLitros()), pedido.getIdPipa());
	        	}
				
	        	
			}
	        if(pedido.getIdCombustible().compareTo(TipoCombustible.REGULAR.getCombustible())==0) {
	        	
	        	if(pipa.getLitrosRestantesRegular()==null)
	        		throw new PedidoException("No cuenta con combustible suficiente la pipa","FG023");
	        	if(pedido.getTipoPedido().getIdTipoPedido().compareTo(TipoPedidoEnum.PROGRAMADO.getId())!=0){
	        	   Double diferenciaLitros =  new Double(pedido.getLitroCarga())-new Double(pedidoP.getLitros());
				   this.pipaRepository.updateLitrosRestantesRegular(pipa.getLitrosRestantesRegular()+diferenciaLitros, pedido.getIdPipa());
	        	}else {
	        		
	        		this.pipaRepository.updateLitrosRestantesRegular(pipa.getLitrosRestantesRegular()-new Double(pedidoP.getLitros()), pedido.getIdPipa());
	        	}
		    }
		}
		catch(PedidoException ex) {
			 System.out.println(ex.getCodeError());
			 System.out.println(ex.getMessage());
			
		}
		
		
		return ticket;
		
	}
	
	/**
	 * Valida duplicidad de pedido
     * @param ticket
	 * @param pedidoP
	 * @throws FastGasException
	 */
	public void existeDuplicidadPedidoCliente(PedidoTicketRequestDto ticket) throws FastGasException {
		
		BigInteger idHorarioServicio;
		//consultamos horario se dervicio
		idHorarioServicio = pedidoRepositoryImpl.getIdHorarioServicio(ticket.getFechaPedido());
		
		//Validamos si el cliente tiene ya un peiddo para el mismo carro en el mismo horario
		BigInteger  duplicidad= pedidoRepositoryImpl.existeDuplicidadPedidoCliente(idHorarioServicio,ticket);
		
		if(duplicidad.compareTo(new BigInteger("1"))== 0)
	      throw new FastGasException("Ya cuentas con pedido para este vehículo para fecha solicitada.","FG033");
		
		
	}
	
	/**
	 * Valida duplicidad de pedido
	 * @param pedidoP
	 * @throws FastGasException
	 */
	public void existeDuplicidadPedidoCliente(PedidoDto pedidoP) throws FastGasException {
		
		BigInteger idHorarioServicio;
		//consultamos horario se dervicio
		idHorarioServicio = pedidoRepositoryImpl.getIdHorarioServicio(pedidoP.getFechaPedidoS());
		
		//Validamos si el cliente tiene ya un peiddo para el mismo carro en el mismo horario
		BigInteger  duplicidad= pedidoRepositoryImpl.existeDuplicidadPedidoCliente(idHorarioServicio,pedidoP);
		
		if(duplicidad.compareTo(new BigInteger("1"))== 1)
	      throw new FastGasException("Ya cuentas con pedido para este vehículo para fecha solicitada.","FG033");
		
	}
	
	
	
	/**
	 * Recupera valor de variables
	 */
	private void recoverEnviroment() {
		//cuenta de la c
		this.apiKeyRoutes = env.getProperty("routes.apiKey");
		
		
		
	}
	  
	
	
}
