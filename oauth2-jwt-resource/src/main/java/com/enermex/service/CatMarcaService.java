package com.enermex.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.modelo.CatMarca;
import com.enermex.modelo.CatModelo;
import com.enermex.dto.MarcaAutoDto;
import com.enermex.repository.CatMarcaRepository;

/**
 *
 * @author abraham
 */
@Service
public class CatMarcaService {
	
	@Autowired
	private CatMarcaRepository catMarcaRepository;
	
    /**
     *
     * @return
     */
    public List<MarcaAutoDto> getAll(){
		
		
		List<CatMarca> marcas = catMarcaRepository.getAll(); 
		

//		for(CatMarca marca : marcas) {
//			marca.setModelos(null);
//			
//		}
		
		List<MarcaAutoDto> listDto = (List<MarcaAutoDto>)(List<?>)marcas;
		
		
		return listDto;
	}
	
    /**
     *
     * @param idMarca
     * @return
     */
    public CatMarca getMarcaById(Integer idMarca) {
		
		return catMarcaRepository.getMarcaById(idMarca);
	}
	

}
