package com.enermex.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.poi.hpsf.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.enermex.repository.AutomovilClienteRepository;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.DireccionClienteRepository;
import com.enermex.repository.UsuarioTipoPlanRepository;
import com.enermex.utilerias.SmtpGmailSsl;
import com.enermex.utilerias.SmtpQueja;
import com.enermex.utilerias.UrlSigner;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.enermex.repository.ClienteTipoPlanRepository;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.dto.AppStoreDto;
import com.enermex.dto.ClienteDto;
import com.enermex.dto.ClienteWebDto;
import com.enermex.modelo.AppStore;
import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteTipoPlan;
import com.enermex.modelo.ClienteTipoPlanKey;
import com.enermex.modelo.TokenEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.impl.ClienteRepositoryImpl;
import com.enermex.enumerable.MsjBloqueCuentaEnum;

/**
 *
 * @author abraham
 */
@Service
public class ClienteService {
	
	
	
	@Autowired
	UsuarioTipoPlanRepository  usuarioTipoPlanRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private AutomovilClienteRepository automovilClienteRepository;
	
	@Autowired
	private DireccionClienteRepository direccionClienteRepository;
	
	@Resource(name="passwordEncoderBean")
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	SmtpGmailSsl sendEmail;
	
	@Autowired
	ClienteTipoPlanRepository clienteTipoPlanRepository;
	
	@Autowired
	private TokenService tokenService;
	
	 @Autowired
  private UrlSigner signer;
  
  @Autowired
  private SmtpQueja smtpQueja;
	
	 @Autowired
	 private ClienteRepositoryImpl clienteRepositoryImpl;
	 
	/**
	 * 
	 * @return
	 */
	public List<Cliente> getAll(){
		
		List<Cliente> clientes =this.clienteRepository.getClientes();	

	  return clientes;
     }
	
    /**
     *
     * @param correo
     * @return
     */
    public Cliente findByEmail(String correo) {
		
		
		return clienteRepository.findByEmail(correo);
	}
	
    /**
     *
     * @param id
     */
    public void delete(BigInteger id) {
		
		this.delete(id);
	} 
	
    /**
     *
     * @param newPass
     * @param idCliente
     */
    public void updatePass(String newPass,BigInteger idCliente) {
	
	    clienteRepository.updatePassword(passwordEncoder.encode(newPass),idCliente);
		
	}
	
    /**
     *
     * @param cliente
     */
    public void update(Cliente cliente){
		this.clienteRepository.update(cliente.getNombre(), cliente.getCorreo(), cliente.getTelefono(), cliente.getIdCliente());
		
	}
	
	/**
	 * Actualiza usuario y envia correos
     * @param clienteP
	 * @param idTitular
	 * @return
	 */
	public Cliente updateExistente(ClienteDto clienteP){
		
	     Cliente cliente = new Cliente(clienteP);
	     cliente.setIdCliente(clienteP.getIdCliente());
	     Cliente invitado;
		
		this.clienteRepositoryImpl.updateCuentaExistente(clienteP.getNombre(), clienteP.getCorreo(), clienteP.getTelefono(), clienteP.getIdCliente());
		
		sendEmail.sendEmail(cliente.getCorreo(), "","registro", "MOVIL");
		
	     //Registro de clientes a traves de invitacion	
	     if(clienteP.getInvitados()!=null) {
	    	 
	    	 //registramos invitados si tiene 
	         for(ClienteDto cl : clienteP.getInvitados()) {
	        	 Cliente client;
	        	 invitado = new Cliente(cl);
	        	 
	        	 invitado.setNombre(cl.getNombre());
	        	 invitado.setCorreo(cl.getCorreo());
	        	 invitado.setEstatus(1);
	        	 invitado.setIdTitular(cliente.getIdCliente());
	        	 
	        	 invitado=this.clienteRepository.save(invitado);
	        	 System.out.println("id cliente"+invitado.getIdCliente());
	        	 
	        	 //creamos token 
	        	   TokenEntity tokenEntity = new TokenEntity();
	        	   tokenEntity.setAplicacion("M");
	        	   tokenEntity.setIdCliente(invitado.getIdCliente());
	        	   tokenService.create(tokenEntity);
	        	   String token = signer.sign(tokenEntity.getIdToken().toString());
	        	   
	    
	        	 sendEmail.sendEmail(invitado.getCorreo(), token,"registroInvitado", "MOVIL");
	        	 //notifica al titular a quien acaba de registrar
	        	 sendEmail.sendEmailNotificaFamiliarTitular(cliente.getCorreo(),"notificaFamiliar&"+invitado.getCorreo());
	        	 
	         }
	     }
		
	     return cliente;
	}
	
	/**
	 * Obtiene los titulares
	 * @param idTitular
	 * @return
	 */
	public List<Cliente> getInvitados(BigInteger idTitular){
		
		return this.clienteRepository.getInvitados(idTitular);
	}
	
	
	
	/**
	 * Registro de cliente
	 * @param clienteP
     * @return 
     * @throws java.lang.Exception 
	 */
    public Cliente save(ClienteDto clienteP) throws Exception{
    	
     Cliente cliente = new Cliente(clienteP);
     Cliente invitado;
     Boolean esRegValido=false;
    
     cliente = this.clienteRepository.save(cliente);
     
    System.out.println("id del cliente"+cliente.getIdCliente());

     
     
     sendEmail.sendEmail(cliente.getCorreo(), "","registro", "MOVIL");
     
     
     //Registro de clientes a traves de invitacion	
     if(clienteP.getInvitados()!=null) {
    	 
    	 //registramos invitados si tiene 
         for(ClienteDto cl : clienteP.getInvitados()) {
        	 Cliente client;
        	 invitado = new Cliente(cl);
        	 
        	 invitado.setNombre(cl.getNombre());
        	 invitado.setCorreo(cl.getCorreo());
        	 invitado.setEstatus(1);
        	 invitado.setIdTitular(cliente.getIdCliente());
        	 
        	 invitado=this.clienteRepository.save(invitado);
        	 System.out.println("id cliente"+invitado.getIdCliente());
        	 
        	 //creamos token 
        	   TokenEntity tokenEntity = new TokenEntity();
        	   tokenEntity.setAplicacion("M");
        	   tokenEntity.setIdCliente(invitado.getIdCliente());
        	   tokenService.create(tokenEntity);
        	   String token = signer.sign(tokenEntity.getIdToken().toString());
        	   
    
        	 sendEmail.sendEmail(invitado.getCorreo(), token,"registroInvitado", "MOVIL");
        	 //notifica al titular a quien acaba de registrar
        	 sendEmail.sendEmailNotificaFamiliarTitular(cliente.getCorreo(),"notificaFamiliar&"+invitado.getCorreo());
        	 
         }	 
     }
     
     return cliente;
    
	}
    /**
     * Service obtiene el cliente por id
     * @param id
     * @return
     */
    public Cliente getClienteById(String id) {
    	
    	
    	BigInteger idCliente = new BigInteger(id);
    	
    	return clienteRepository.findClienteById(idCliente);
    	
    }
    
    /**
     *
     * @param correo
     * @return
     */
    public Cliente findClienteByCorreo(String correo) {
    
          Cliente cli=null; 
    	
          cli= this.clienteRepository.findByEmail(correo);
          
          
          
    	return cli;
    	
    }
    
    /**
     * Service obtiene el cliente por correo
     * @param email
     * @param id
     * @return
     */
    public Cliente getClienteByEmail(String email) {
    	return clienteRepository.findByEmail(email);
   
    }
    
    /**
     * Autenticacion del usuario
     * @param correo
     * @param pass
     * @return
     */
    public Cliente login (String correo, String pass) {
    	
    	return null;
    }
    
    /**
     * Valida registro valido
     * @param correo
     * @param password
     * @return
     */
    public Cliente esRegistroValido(String correo) {
    	
    	
    	return clienteRepository.findByEmail(correo);
    }
    
    /**
     * 
     * @param email
     * @return
     */
    public boolean validaAcceso(String email) {
    	
    	Cliente cliente = clienteRepository.findByEmail(email);
    	

    	if(cliente!=null) {
    		this.clienteRepository.updateAcces(cliente.getIdCliente());	
    	}
    	else {
    		
    		return false;
    	}
    	
    	return true;
    	
    }
    
    /**
     * Agrega auto cliente
     * @param idCliente
     * @param autos
     */
    public void addCars(BigInteger idCliente, List<AutomovilCliente> autos) {
    	
    	Cliente cliente = clienteRepository.findClienteById(idCliente);
    	
    	
    	for(AutomovilCliente auto  :autos) {
    		
    		auto.setCliente(cliente);
    		auto.setEstatus(1);
    		automovilClienteRepository.save(auto);	
    		
    	}
    	
    	
    	clienteRepository.save(cliente);
    	
    }
    
    /**
     * 
     * @param clienteP
     * @param cliente
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public ClienteDto updatePlan(ClienteDto clienteP){
    	
         	
    	Cliente cliente = new Cliente(clienteP);

    	Cliente invitado ; 



    	this.clienteRepository.save(cliente);

    	for(Cliente c: clienteP.getInvitados()) {

    		

    		//invitado = new Cliente(c);

    	    //this.clienteRepository.save(invitado);

    	}
    	

    	return null;
     }
    
    
    /**
     * Actualizacion de perfil telefono
     * @param cliente
     * @param cell
     * @param phone
     */
    @Transactional(rollbackFor=Exception.class)
    public void updatePerfilTel(ClienteDto cliente) {
    	
    	
    	this.clienteRepository.updatePerfilTel(cliente.getTelefono(), cliente.getIdCliente());
    	
    	//al actualizar el cel solicitamos su validacion sms nuevamente
    	updateAccesoSms(new Boolean("false"),cliente.getIdCliente());
    	
    } 
    
    
    /**
     * Actualizacion de perfil pass
     * @param clienteP
     * @param cell
     * @param phone
     * @throws java.lang.Exception
     */
    @Transactional(rollbackFor=Exception.class)
    public void updatePerfilPass(ClienteDto clienteP)throws Exception {
    	
    	Cliente cliente ;
    	
    	cliente = this.clienteRepository.getPassClienteBycript(clienteP.getIdCliente());
    	
    	boolean passActual=this.passwordEncoder.matches(clienteP.getPassActual(),cliente.getContrasena());
    	
    	if(passActual==false) {
    		
    		throw new Exception("La contraseña actual no coincide ");
    	}
    			
    	
    	
    	this.clienteRepository.updatePerfilPass(this.passwordEncoder.encode(clienteP.getPass()), clienteP.getIdCliente());
    	
    } 
    
    
    /**
     * Actualizacion de perfil pass desde perfil
     * @param clienteP
     * @param cell
     * @param phone
     * @throws java.lang.Exception
     */
    @Transactional(rollbackFor=Exception.class)
    public void updateFamiliar(ClienteDto clienteP)throws Exception {
    	ClienteTipoPlanKey tpk = new ClienteTipoPlanKey();
    	ClienteTipoPlan   ctp  = new ClienteTipoPlan();
    	
    	tpk.setIdCliente(clienteP.getIdCliente());
    	tpk.setIdEstatus(1);
    	
    	for(Integer id :clienteP.getPlanesId()) {
    		tpk.setIdPlan(id);
    		ctp.setClienteTipoPlanKey(tpk);
    		clienteTipoPlanRepository.save(ctp);
    		
    	}
        
    }
    
  /**
   * Actualiza apellido paterno desde perfil
   * @param clienteP
   * @throws Exception
   */
    @Transactional(rollbackFor=Exception.class)
    public void updatePerfilApellidoP(ClienteDto clienteP)throws Exception {
    	
    	Cliente cliente ;
    	
    	cliente = this.clienteRepository.getPassClienteBycript(clienteP.getIdCliente());
    	
    			
    	this.clienteRepository.updatePerfilApellidoP(clienteP.getApellidoPaterno(), clienteP.getIdCliente());
    	
    } 
    
    
    /**
     * Update apellido materno desde perfil
     * @param clienteP
     * @throws Exception
     */
    @Transactional(rollbackFor=Exception.class)
    public void updatePerfilApellidoM(ClienteDto clienteP)throws Exception {
    	
    	Cliente cliente ;
    	
    	cliente = this.clienteRepository.getPassClienteBycript(clienteP.getIdCliente());
    	
    			
    	this.clienteRepository.updatePerfilApellidoM(clienteP.getApellidoMaterno(), clienteP.getIdCliente());
    	
    } 

    /**
     * Update apellido materno desde perfil
     * @param clienteP
     * @throws Exception
     */
    @Transactional(rollbackFor=Exception.class)
    public void updatePerfilNombre(ClienteDto clienteP)throws Exception {
    	
    	Cliente cliente ;
    	
    	cliente = this.clienteRepository.getPassClienteBycript(clienteP.getIdCliente());
    	
    			
    	this.clienteRepository.updatePerfilNombre(clienteP.getNombre(), clienteP.getIdCliente());
    	
    } 
    
    
    /**
     * Establece el valor del token
     * @param token
     * @param idCliete
     */
    @Transactional(rollbackFor=Exception.class)
    public void updateTokenConektaCustumer(String token,BigInteger idCliete) {
    	
    	this.clienteRepository.updateTokenConektaCustumer(token, idCliete);
    	
    	
    }
    
    /**
     * Obtiene el token id Conekta del cliente
     * @param idCliente
     * @return 
     */
    @Transactional(rollbackFor=Exception.class)
    public String getIdTokenCustumer(BigInteger idCliente) {
    	
    	return this.clienteRepository.getIdTokenCustumer(idCliente);
    	
    	
    }
    
    /**
     * Obtiene el token id Conekta del cliente
     * @param bloqueo
     * @param idCliente
     * @throws Exception 
     */
    @Transactional(rollbackFor=Exception.class)
    public void bloqueaCliente(boolean bloqueo,BigInteger idCliente) throws Exception {
    	
      this.clienteRepository.bloqueoCliente(bloqueo, idCliente);
      Cliente cliente = clienteRepository.findClienteById(idCliente);

      
      String nombre,apPaterno,apMaterno,completo;
      // Construcción del nombre completo
      nombre    = cliente.getNombre() != null ? cliente.getNombre() : "";
      apPaterno = cliente.getApellidoPaterno() != null ? cliente.getApellidoPaterno() : "";
      apMaterno = cliente.getApellidoMaterno() != null ? cliente.getApellidoMaterno() : "";
      completo  = nombre + " " + apPaterno + " " + apMaterno;

      if(cliente == null){
    	  throw new Exception("FG009");
      	
      }
      
      // INTER: Antonio González - Envío de correo electrónico de bloqueo
      if(bloqueo){
    
    	  //"Bloqueo de cuenta " + email,  --> motivo
    	  //"Tu cuenta ha sido bloqueada " + email,  --> detalle
    	  
          smtpQueja.sendEmailBloqueadoSys(cliente.getCorreo(), completo,"FAST GAS BLOQUEO DE CUENTA","Bloqueo de cuenta " + cliente.getCorreo(),"Tu cuenta ha sido bloqueada");
        }
        else {
        	smtpQueja.sendEmailBloqueadoSys(cliente.getCorreo(), completo,"FAST GAS DESBLOQUEO DE CUENTA","Desbloqueo de cuenta " + cliente.getCorreo(),"Tu cuenta ha sido desbloqueada");	
        	
        }
      
    }
    
    /**
     * Update perfil
     * @param clienteP
     * @throws Exception
     */
    @Transactional(rollbackFor=Exception.class)
    public void updatePerfilCorreo(ClienteDto clienteP)throws Exception {
    	
    	Cliente cliente ;
    	
    	cliente = this.clienteRepository.getPassClienteBycript(clienteP.getIdCliente());
    	
    			
    	this.clienteRepository.updatePerfilCorreo(clienteP.getCorreo(), clienteP.getIdCliente());
    	
    } 
    
    /**
     *
     * @param oauth
     * @return
     */
    public Cliente getClienteFromToken(String oauth) {
        try{
          return   clienteRepository.findClienteById(new BigInteger(getId(oauth)));
        } catch(Exception e) {
          e.printStackTrace();
          return null;
        }
      }
    
    
    private String getId(String oauth) {
        try {
          // Se obtiene el usuario desde el token
          String dataPart = oauth.split("\\.")[1];
          String jsonPart = new String(Base64.getDecoder().decode(dataPart));
          
          // Deserialización del token
          ObjectMapper objectMapper = new ObjectMapper();
          objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          Map<String, Object> jsonMap = objectMapper.readValue(jsonPart, new TypeReference<Map<String,Object>>(){});
          
          // Se eliminan posibles caracteres extras
          return (String)jsonMap.get("user_name");
        } catch(Exception e) {
          e.printStackTrace();
          return null;
        }
      }
    
    /**
     *
     * @param idCliente
     * @return
     */
    public String getNombreCliente(BigInteger idCliente) {
    
       return  this.clienteRepositoryImpl.geNombreCliente(idCliente);	
    }
    
    /**
     * Busca los familiares de un cliente
     * @param cliente
     * @param idCliente
     * @return 
     */
    public List<BigInteger> getFamiliares(Cliente cliente) {
    	
    	//Buscar familiares asociados a un cliente 
    	List<Cliente> clientes = getInvitados(cliente.getIdCliente());
    	List<BigInteger> clientesIds = new ArrayList<>();
    	
    	for( Cliente  cli: clientes) {
    		clientesIds.add(cli.getIdCliente());
    		
    	}
    	  	
    	return clientesIds;
    }
    
    
    /**
     * Elimina el plande un cliente
     * @param cliente
     * @param idCliente
     * @param familiaresIds
     * @param isCuenta
     * @return 
     */
    public boolean eliminaPlan(Cliente cliente, List<BigInteger> familiaresIds, Boolean isCuenta) {
    	return this.clienteRepositoryImpl.seEliminaPlan(familiaresIds, cliente.getIdCliente(), isCuenta);
    }
    
    /**
     * Elimina la relación entre un cliente y sus familiares
     * @param familiaresIds
     * @param idCliente
     * @param idPlan
     * @return 
     */
    public boolean eliminaTitularFamiliares(List<BigInteger> familiaresIds) {
    	return this.clienteRepositoryImpl.seEliminaTitularFamiliar(familiaresIds);
    }
    
    /**
     * Elimina al cliente y sus familiares
     * @param clientesIds
     * @param idCliente
     * @param idPlan
     * @return 
     */
    public boolean eliminaCuenta(List<BigInteger> clientesIds) {
    	return this.clienteRepositoryImpl.seEliminaCuenta(clientesIds);
    }
    
    /**
     * Get Emails
     * @param famClientes
     * @param idCliente
     * @param cliente
     * @param idPlan
     * @return 
     */
    public List<String> getEmails(List<BigInteger> famClientes, Cliente cliente) {
    	List<Cliente> clientes = getInvitados(cliente.getIdCliente());
    	List<String> clientesEmails = new ArrayList<>();
    	
    	for( Cliente  cli: clientes) {
    		clientesEmails.add(cli.getCorreo());
    		
    	}
    	  	
    	return clientesEmails;
    }
    
    /**
     * Send Emails Baja Plan Familiar
     * @param famEmails
     * @param clienteEmail
     * @param idCliente
     * @param idPlan
     */
    public void enviarEmailsBajaFamiliar(List<String> famEmails, String clienteEmail) {
    	

    	List<AppStore> appStorePathResult = this.clienteRepository.getAppStorePaths();
    	List<AppStoreDto> appStorePaths = new ArrayList<>();
    	
    	for(AppStore app : appStorePathResult){
    		AppStoreDto appStore = new AppStoreDto();
    		appStore.setTransformer(app.getIdApp(), app.getAppTecnologia(), app.getValor());
    		appStorePaths.add(appStore);
    	}
    	
    	for(String email : famEmails){
    		sendEmail.sendEmailBajaPlanFamiliares(email, appStorePaths);
    	}
    	    	
    	sendEmail.sendEmailBajaPlanFamiliaresAdmin(clienteEmail);
    }
    
    /**
     * Send Emails Baja Cuenta
     * @param famEmails
     * @param idCliente
     * @param clienteEmail
     * @param plan
     * @param idPlan
     */
    public void enviarEmailsBajaCuenta(List<String> famEmails, String clienteEmail, String plan) {
    	
    	List<AppStore> appStorePathResult = this.clienteRepository.getAppStorePaths();
    	List<AppStoreDto> appStorePaths = new ArrayList<>();
    	
    	for(AppStore app : appStorePathResult){
    		AppStoreDto appStore = new AppStoreDto();
    		appStore.setTransformer(app.getIdApp(), app.getAppTecnologia(), app.getValor());
    		appStorePaths.add(appStore);
    	}
    	
    	for(String email : famEmails){
    		sendEmail.sendEmailBajaCuentaFamiliares(email, appStorePaths);
    	}
    	    	
    	sendEmail.sendEmailBajaCuentaAdmin(clienteEmail, plan);
    }
    
    /**
     * Buscar si el cliente ya existia
     * @param correo
     * @param idCliente
     * @param idPlan
     * @return 
     */
    public boolean buscarClienteExistente(String correo)
    {
    	return this.clienteRepositoryImpl.buscarClienteExistente(correo);
    }
    
    /**
     * 
     * @param estatus
     * @param idCliente
     */
    public void updateAccesoSms(Boolean estatus ,BigInteger idCliente) {
    	
    	this.clienteRepository.updateAccesoSms(estatus,idCliente);
    }
    
    
    /**
     * Eliminar relacion entre titular y familiares
     * @param clientId
     * @param idCliente
     * @param familiaresCount
     * @param idPlan
     * @return 
     */
    public boolean eliminarRelacionTitularFamiliares(BigInteger clientId, Integer familiaresCount)
    {
    	return this.clienteRepositoryImpl.seEliminaRelacionTitularFamiliar(clientId);
    }
    
    /**
     *
     * @param correo
     * @throws FastGasException
     */
    public  void tieneBaja(String correo) throws FastGasException {
    	
    	
    	 BigInteger idCliente = this.clienteRepository.tieneBaja(correo);
    	 
    	 if(idCliente.compareTo(new BigInteger("0"))==1)
    		throw new FastGasException("Estimado cliente su cuenta fue dada de baja.", "FG035");
    	 
   
    }
 
    /**
     *
     * @param correo
     * @return
     */
    public Cliente tieneAcceso(String correo) {
		
		
		return clienteRepository.tieneAcceso(correo);
	}
   
   /**
    * 
    * @return
    */
   public List<ClienteWebDto> getAllClientesWebDto(){
	   
	   return clienteRepositoryImpl.getAllClientes();
   }
   
    /**
     *
     * @param correo
     * @throws FastGasException
     */
    public  void existeCliente(String correo) throws FastGasException {
   	
   	
  	 Cliente cliente = this.clienteRepository.findByEmail(correo);
  	 
  	 if(cliente==null)
  		throw new FastGasException("Usuario o Contraseña incorrectos", "FG001");
  	 
 
  }
   
	
}