package com.enermex.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import com.enermex.modelo.AzureBsEntity;
import com.enermex.modelo.UsuarioEntity;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.enermex.config.AzureConfiguration;
import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.repository.UsuarioRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Servicio de operaciones de usuarios
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 16 de enero de 2020
 */
@Service
public class UsuarioService {
  Logger logger = LoggerFactory.getLogger(UsuarioService.class);

  @Autowired
  private UsuarioRepository repo;
  
  @Resource(name="passwordEncoderBean")
  private PasswordEncoder passwordEncoder;

  @Autowired
  private AzureBsService azureService;

  /**
   * Creación de un usuario
   * 
   * @param usuario Usuario por insertar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  public boolean create(UsuarioEntity usuario) {
    if (exists(usuario)) {
      logger.error("Trata de añadir un usuario existente - ID: " + usuario.getIdUsuario());

      return false;
    }

    if(exists(usuario.getCorreo())){
      return false;
    }

    // Se establece el estado y la fecha de creación
    Date date = new Date();
    usuario.setFechaCreacion(date);
    usuario.setFechaBaja(null);
    usuario.setEstatus(UsuarioEstatus.POR_VERIFICAR);

    return save(usuario);
  }

  /**
   * Actualización de un usuario
   * 
   * @param usuario Usuario por actualizar
   * @return Bandera que indica el resultado de la operación
   */
  public boolean update(UsuarioEntity usuario) {
    if (!exists(usuario)) {
      logger.error("Trata de actualizar un usuario inexistente - ID: " + usuario.getIdUsuario());

      return false;
    }

    return save(usuario);
  }

  /**
   * Eliminación lógica del usuario
   * 
   * @param usuario Usuario por eliminar
   * @return Bandera que indica el resultado de la operación
   */
  public boolean delete(UsuarioEntity usuario) {
    if (!exists(usuario)) {
      logger.error("Trata de eliminar un usuario inexistente - ID: " + usuario.getIdUsuario());

      return false;
    }

    usuario.setEstatus(UsuarioEstatus.INACTIVO);
    return save(usuario);
  }

  /**
   * Consulta de los usuario registrados
   * 
   * @return Lista con usuario registrados
   */
  @Transactional(readOnly = true)
  public List<UsuarioEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener usuarioes: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public List<UsuarioEntity> queryDespachadores() {
    try {
      return repo.findDespachadores();
    } catch (Exception e) {
      logger.error("Error al obtener usuarioes: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un usuario por su identificador
   * 
   * @param id Identificador del usuario
   * @return El usuario, si se encuentra
   */
  @Transactional(readOnly = true)
  public UsuarioEntity queryById(Long id) {
    try {
      Optional<UsuarioEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener un usuario: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un usuario por su identificador
   * 
     * @param correo
   * @return El usuario, si se encuentra
   */
  @Transactional(readOnly = true)
  public UsuarioEntity queryByCorreo(String correo) {
    try {
      UsuarioEntity usuario = repo.findByCorreo(correo);
      return usuario;
    } catch (Exception e) {
      logger.error("Error al obtener un usuario: " + e.getMessage());
      return null;
    }
  }

  /**
   * Consulta de un usuario por su contraseña
   * 
   * @param id Identificador del usuario
   * @return El usuario, si se encuentra
   */
  @Transactional(readOnly = true)
  public String findPassword(Long id) {
    try {
      String pass = repo.findPassword(id);
      return pass;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Comprueba si existe un usuario
   * 
   * @param usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  private boolean exists(UsuarioEntity usuario) {
    try {
      return usuario != null && usuario.getIdUsuario() != null && repo.existsById(usuario.getIdUsuario());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un usuario
   * 
   * @param id Identificador del usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  public boolean exists(Long id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un usuario
   * 
   * @param correo Correo electrónico del usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  public boolean exists(String correo) {
    try {
      return repo.existsByCorreo(correo);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Comprueba si existe un usuario con un teléfono
   * 
   * @param telefono Teléfono del usuario
   * @return Bandera que indica si existe el usuario
   */
  @Transactional(readOnly = true)
  public boolean existsTelefono(String telefono) {
    try {
      return repo.existsByTelefono(telefono);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un usuario: " + e.getMessage());
      return false;
    }
  }

  /**
   * Guardado del usuario
   * 
   * @param usuario Usuario por guardar
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional(propagation = Propagation.NESTED)
  private boolean save(UsuarioEntity usuario) {
    try {
      repo.save(usuario);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar un usuario: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Comprobación de la contraseña
   * 
   * @param id Identificador del usuario
   * @param password Problable contraseña del usuario
   * @return Bandera que indica si la contraseña corresponde
   */
  @Transactional
  public boolean testPassword(Long id, String password) {
    try {
      return repo.findWithPassword(id,passwordEncoder.encode( password)) != null;
    } catch (Exception e) {
      logger.error("Error al consultar un usuario por su contraseña: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Actualización de la contraseña
   * 
     * @param correo
   * @param id Identificador del usuario
   * @param password Problable contraseña del usuario
   * @return Bandera que indica si la contraseña corresponde
   */
  @Transactional
  public boolean updatePassword(String correo, String password) {
    try {
      repo.updatePassword(correo, passwordEncoder.encode(password));
      return true;
    } catch (Exception e) {
      logger.error("Error al actualizar la contraseña de un usuario: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

  /**
   * Actualización o ingreso de la foto
   * 
   * @param usuario Usuario a quien se debe ingresar la foto
   * @param file Archivo de imagen
   * @return Bandera que indica el resultado de la operación
   */
  @Transactional
  public boolean updateFoto(UsuarioEntity usuario, MultipartFile file) {
    try {
      // Se calcula un nombre único de la imagen
      String nombreArchivo = UUID.randomUUID().toString() + "_" +  file.getOriginalFilename().replace(" ", "");

      // Se obtiene el BlobContainer de Azure
      BlobContainerClient blobContainerClient = azureContainer();

      // Se verifica que la imagen anterior exista; siendo así, se borra
      if(usuario.getFotoUri() != null && !usuario.getFotoUri().isEmpty()) {
        BlockBlobClient fotoActual = blobContainerClient.getBlobClient(usuario.getFotoUri()).getBlockBlobClient();
        
        if(fotoActual.exists()) {
          fotoActual.delete();
        }
      }

      // Se carga como bloque, es indistinto para imágenes tan pequeñas
      // https://docs.microsoft.com/es-mx/rest/api/storageservices/understanding-block-blobs--append-blobs--and-page-blobs
      BlockBlobClient foto = blobContainerClient.getBlobClient(nombreArchivo).getBlockBlobClient();
      foto.upload(file.getInputStream(), file.getSize());
      usuario.setFotoUri(nombreArchivo);

      return true;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param usuario
     * @return
     */
    public InputStreamResource downloadFoto(UsuarioEntity usuario) {
    try {
      BlobContainerClient blobContainerClient = azureContainer();

      // Flujo si la imagen ya había sido cargada
      if (usuario != null && usuario.getFotoUri() != null && !usuario.getFotoUri().isEmpty()){
        ByteArrayInputStream foto = azureInputStream(blobContainerClient, usuario.getFotoUri());

        if(foto != null) {
          return new InputStreamResource(foto);
        }
      }

      // Flujo si la imagen no ha sido cargada o fue eliminada
      ByteArrayInputStream fotoNull = azureInputStream(blobContainerClient, "null.png");

      if(fotoNull != null) {
        return new InputStreamResource(fotoNull);
      }

      // Si no se encuentra ninguna imagen
      return null;
    } catch (Exception e) {
      logger.error("Error al descargar la imagen: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  private BlobContainerClient azureContainer() {
    // Consulta de credenciales de Azure
    AzureBsEntity azureEntity = azureService.queryById(1);

    // Se crea un cliente del Blob Service de Azure
    BlobServiceClient blobServiceClient = new BlobServiceClientBuilder()
      .endpoint(azureEntity.getEndpoint())
      .sasToken(azureEntity.getSas())
      .buildClient();
    
    // Se crea un cliente para el contenedor
    BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(azureEntity.getContainer());
    return blobContainerClient;
  }

  private ByteArrayInputStream azureInputStream(BlobContainerClient blobContainerClient, String blobName) {
    BlockBlobClient fotoActual = blobContainerClient.getBlobClient(blobName).getBlockBlobClient();
        
    if(fotoActual.exists()) {
      // A ByteArrayOutputStream holds the content in memory
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      fotoActual.download(outputStream);
      
      // To convert it to a byte[] - simply use
      final byte[] bytes = outputStream.toByteArray();

      // To convert bytes to an InputStream, use a ByteArrayInputStream
      ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
      
      return inputStream;
    } else {
      return null;
    }
  }
}
