package com.enermex.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.enermex.config.AzureConfiguration;
import com.enermex.enumerable.PedidoEvidenciaMomento;
import com.enermex.modelo.AzureBsEntity;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.PedidoEvidenciaEntity;
import com.enermex.repository.PedidoEvidenciaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * PedidoEvicenciaService
 */
@Service
public class PedidoEvidenciaService {
  Logger logger = LoggerFactory.getLogger(PedidoEvidenciaService.class);

  @Autowired
  private AzureBsService azureService;

  @Autowired
  private PedidoEvidenciaRepository repo;
  
    /**
     *
     * @param idPedido
     * @param file
     * @param momento
     * @return
     */
    @Transactional
  public boolean addFoto(BigInteger idPedido, MultipartFile file, PedidoEvidenciaMomento momento) {
    try {
      // Se calcula un nombre único de la imagen
      String nombreArchivo = UUID.randomUUID().toString() + "_" +  file.getOriginalFilename().replace(" ", "");

      // Se obtiene el BlobContainer de Azure
      BlobContainerClient blobContainerClient = azureContainer();

      // Se carga como bloque, es indistinto para imágenes tan pequeñas
      // https://docs.microsoft.com/es-mx/rest/api/storageservices/understanding-block-blobs--append-blobs--and-page-blobs
      BlockBlobClient evidencia = blobContainerClient.getBlobClient(nombreArchivo).getBlockBlobClient();
      evidencia.upload(file.getInputStream(), file.getSize());

      // Registro de la evidencia en la base de datos
      PedidoEvidenciaEntity registro = new PedidoEvidenciaEntity();
      registro.setIdPedido(idPedido);
      registro.setNombre(nombreArchivo);
      registro.setMomento(momento);
      repo.save(registro);

      return true;
    } catch (Exception e) {
      logger.error("Error al guardar la imagen: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }

    /**
     *
     * @param idEvidencia
     * @return
     */
    public InputStreamResource downloadFoto(BigInteger idEvidencia) {
    try {
      Optional<PedidoEvidenciaEntity> optional = repo.findById(idEvidencia);

      if(!optional.isPresent()) {
        return null;
      }

      PedidoEvidenciaEntity evidencia = optional.get();
      BlobContainerClient blobContainerClient = azureContainer();

      // Flujo si la imagen ya había sido cargada
      if (evidencia != null && evidencia.getNombre() != null && !evidencia.getNombre().isEmpty()) {
        ByteArrayInputStream foto = azureInputStream(blobContainerClient, evidencia.getNombre());

        if(foto != null) {
          return new InputStreamResource(foto);
        }
      }

      // Si no se encuentra ninguna imagen
      return null;
    } catch (Exception e) {
      logger.error("Error al descargar la imagen: " + e.getMessage());

      e.printStackTrace();
      return null;
    }
  }

  private BlobContainerClient azureContainer() {
    // Consulta de credenciales de Azure
    AzureBsEntity azureEntity = azureService.queryById(1);

    // Se crea un cliente del Blob Service de Azure
    BlobServiceClient blobServiceClient = new BlobServiceClientBuilder()
      .endpoint(azureEntity.getEndpoint())
      .sasToken(azureEntity.getSas())
      .buildClient();
    
    // Se crea un cliente para el contenedor
    BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(azureEntity.getContainer());
    return blobContainerClient;
  }

  
  private ByteArrayInputStream azureInputStream(BlobContainerClient blobContainerClient, String blobName) {
    BlockBlobClient fotoActual = blobContainerClient.getBlobClient(blobName).getBlockBlobClient();
        
    if(fotoActual.exists()) {
      // A ByteArrayOutputStream holds the content in memory
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      fotoActual.download(outputStream);
      
      // To convert it to a byte[] - simply use
      final byte[] bytes = outputStream.toByteArray();

      // To convert bytes to an InputStream, use a ByteArrayInputStream
      ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
      
      return inputStream;
    } else {
      return null;
    }
  }
  
  /**
   * Obtiene las evidencias de un pedido
   * @param idPedido
   * @return
   */
  public List<PedidoEvidenciaEntity> getEvidenciasPedido(BigInteger idPedido){
	  
	  return repo.findByIdPedido(idPedido);
  }
  
}