package com.enermex.service;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Optional;

import com.enermex.enumerable.UsuarioEstatus;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.TokenEntity;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.repository.ClienteRepository;
import com.enermex.repository.TokenRepository;
import com.enermex.repository.UsuarioRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * TokenService
 */
@Service
public class TokenService {
  Logger logger = LoggerFactory.getLogger(TokenService.class);
  
	@Autowired
	private TokenRepository repo;
	@Autowired
	private ClienteRepository clienteRepo;
	@Autowired
	private UsuarioRepository usuarioRepo;
	
    /**
     *
     * @param token
     * @return
     */
    @Transactional
  public boolean create(TokenEntity token) {
    if (exists(token)) {
      logger.error("Trata de crear un token existente - ID: " + token.getIdToken());

      return false;
    }

    // Se asigna la fecha actual mas dos días
    Calendar validez = Calendar.getInstance();
    validez.add(Calendar.DAY_OF_MONTH, 2);
    token.setValidez(validez);
    token.setVigente(true);

    return save(token);
  }

    /**
     *
     * @param token
     * @return
     */
    @Transactional
  public boolean use(TokenEntity token) {
    if (!exists(token)) {
      logger.error("Trata de utilizar un token inexistente - ID: " + token.getIdToken());

      return false;
    }

    token.setVigente(false);

    return save(token);
  }

    /**
     *
     * @param token
     * @return
     */
    public boolean validDate(TokenEntity token) {
    Calendar now = Calendar.getInstance();
    return token.getValidez() != null && now.compareTo(token.getValidez()) <= 0;
  }

    /**
     *
     * @param token
     * @return
     */
    @Transactional(readOnly = true)
  public boolean validUser(TokenEntity token) {
    if(token.getAplicacion().equals("M")) {
      // Siendo la aplicación móvil, se busca al cliente
      Optional<Cliente> optional = clienteRepo.findById(token.getIdCliente());

      // TODO: Definir si el usuario es válido
      if(optional.isPresent()) {
        return true;
      } else {
        logger.error("Error en token " + token.getIdToken() + " para el cliente " + token.getIdCliente());
      }

      return false;
    } else if(token.getAplicacion().equals("W") || token.getAplicacion().equals("D")) {
      // Siendo la aplicación de backoffice o despachador, se busca al usuario
      Optional<UsuarioEntity> optional = usuarioRepo.findById(token.getIdUsuario());

      if (optional.isPresent() 
          && (optional.get().getEstatus() == UsuarioEstatus.POR_VERIFICAR ||
              optional.get().getEstatus() == UsuarioEstatus.ACTIVO)) {
        return true;
      } else {
        logger.error("Error en token " + token.getIdToken() + " para el usuario " + token.getIdCliente());
      }

      return false;
    } else {
      logger.error("Error en la aplicación registrada en un token - ID: " + token.getIdToken());
      return false;
    }
  }

    /**
     *
     * @param token
     * @return
     */
    @Transactional(readOnly = true)
  public UsuarioEntity getUsuario(TokenEntity token) {
    Optional<UsuarioEntity> optional = usuarioRepo.findById(token.getIdUsuario());

    if(optional.isPresent() 
       && (optional.get().getEstatus() == UsuarioEstatus.POR_VERIFICAR ||
           optional.get().getEstatus() == UsuarioEstatus.ACTIVO))  {
      return optional.get();
    } else {
      logger.error("Error en token " + token.getIdToken() + " para el usuario " + token.getIdCliente());
      return null;
    }
  }

    /**
     *
     * @param token
     * @return
     */
    @Transactional(readOnly = true)
  public Cliente getCliente(TokenEntity token) {
    Optional<Cliente> optional = clienteRepo.findById(token.getIdCliente());

    // TODO: Definir si el usuario es válido
    if(optional.isPresent()) {
      return optional.get();
    } else {
      logger.error("Error en token " + token.getIdToken() + " para el cliente " + token.getIdCliente());
      return null;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public TokenEntity queryById(BigInteger id) {
    try {
      Optional<TokenEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener un token: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(TokenEntity token) {
    try {
      return token != null && token.getIdToken() != null && repo.existsById(token.getIdToken());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un token: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public boolean exists(BigInteger id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe un token: " + e.getMessage());
      return false;
    }
  }

  @Transactional
  private boolean save(TokenEntity token) {
    try {
      repo.save(token);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar un token: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}
