package com.enermex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.GasolinaPrecioRepository;
import com.enermex.modelo.GasolinaPrecio;

/**
 *
 * @author abraham
 */
@Service
public class GasolinaPrecioService {
	
	
	@Autowired
	GasolinaPrecioRepository gasolinaPrecioRepository;
	
	
	/**
	 * Precio combustible por:
	 * @param cp
	 * @param idTipoCombustible
	 * @return
	 */
	public Double getPrecioByCp(String cp, Integer idTipoCombustible) {
		
		GasolinaPrecio gp =gasolinaPrecioRepository.getPrecioByCp(cp,idTipoCombustible);
		
		return gp.getPrecioPorLitro();
	}

}
