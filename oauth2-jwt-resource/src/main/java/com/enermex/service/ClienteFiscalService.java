package com.enermex.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import com.enermex.enumerable.ClienteFiscalEstatus;
import com.enermex.modelo.ClienteFiscalEntity;
import com.enermex.repository.ClienteFiscalRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * ClienteFiscalService
 */
@Service
public class ClienteFiscalService {
  Logger logger = LoggerFactory.getLogger(ClienteFiscalService.class);

  @Autowired
  private ClienteFiscalRepository repo;

    /**
     *
     * @param fiscal
     * @return
     */
    @Transactional
  public boolean create(ClienteFiscalEntity fiscal) {
    if (exists(fiscal)) {
      logger.error("Trata de añadir datos fiscales existentes - ID: " + fiscal.getIdFiscal());

      return false;
    }

    fiscal.setEstatus(ClienteFiscalEstatus.ACTIVO);

    return save(fiscal);
  }

    /**
     *
     * @param fiscal
     * @return
     */
    public boolean update(ClienteFiscalEntity fiscal) {
    if (!exists(fiscal)) {
      logger.error("Trata de actualizar datos fiscales inexistentes - ID: " + fiscal.getIdFiscal());

      return false;
    }

    return save(fiscal);
  }

    /**
     *
     * @param fiscal
     * @return
     */
    public boolean delete(ClienteFiscalEntity fiscal) {
    if (!exists(fiscal)) {
      logger.error("Trata de eliminar datos fiscales inexistentes - ID: " + fiscal.getIdFiscal());

      return false;
    }

    fiscal.setEstatus(ClienteFiscalEstatus.INACTIVO);
    return save(fiscal);
  }

    /**
     *
     * @param idCliente
     * @return
     */
    @Transactional(readOnly = true)
  public List<ClienteFiscalEntity> queryAllByIdCliente(BigInteger idCliente) {
    try {
      return repo.findByIdClienteAndEstatus(idCliente, ClienteFiscalEstatus.ACTIVO);
    } catch (Exception e) {
      logger.error("Error al obtener datos fiscales: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public ClienteFiscalEntity queryById(BigInteger id) {
    try {
      Optional<ClienteFiscalEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener datos fiscales: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(ClienteFiscalEntity fiscal) {
    try {
      return fiscal != null && fiscal.getIdFiscal() != null && repo.existsById(fiscal.getIdFiscal());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe datos fiscales: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public boolean exists(BigInteger id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe datos fiscales: " + e.getMessage());
      return false;
    }
  }

  @Transactional(propagation = Propagation.NESTED)
  private boolean save(ClienteFiscalEntity fiscal) {
    try {
      repo.save(fiscal);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar datos fiscales: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}
