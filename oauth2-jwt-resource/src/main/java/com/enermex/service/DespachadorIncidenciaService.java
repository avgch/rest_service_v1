package com.enermex.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.enermex.enumerable.IncidenciaEstatus;
import com.enermex.modelo.DespachadorIncidenciaEntity;
import com.enermex.repository.DespachadorIncidenciaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abraham
 */
@Service
public class DespachadorIncidenciaService {
  Logger logger = LoggerFactory.getLogger(DespachadorIncidenciaService.class);

  @Autowired
	private DespachadorIncidenciaRepository repo;

    /**
     *
     * @param incidencia
     * @return
     */
    @Transactional
  public boolean create(DespachadorIncidenciaEntity incidencia) {
    if (exists(incidencia)) {
      logger.error("Trata de añadir una incidencia existente - ID: " + incidencia.getIdIncidencia());

      return false;
    }

    // Se establece el estado y la fecha de creación
    Date date = new Date();
    incidencia.setFechaCreacion(date);

    return save(incidencia);
  }

    /**
     *
     * @param incidencia
     * @return
     */
    public boolean update(DespachadorIncidenciaEntity incidencia) {
    if (!exists(incidencia)) {
      logger.error("Trata de actualizar una incidencia inexistente - ID: " + incidencia.getIdIncidencia());

      return false;
    }

    return save(incidencia);
  }

    /**
     *
     * @param incidencia
     * @return
     */
    public boolean delete(DespachadorIncidenciaEntity incidencia) {
    if (!exists(incidencia)) {
      logger.error("Trata de eliminar una incidencia inexistente - ID: " + incidencia.getIdIncidencia());

      return false;
    }

    incidencia.setEstatus(IncidenciaEstatus.INACTIVO);
    return save(incidencia);
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public List<DespachadorIncidenciaEntity> queryAll() {
    try {
      return repo.findAll();
    } catch (Exception e) {
      logger.error("Error al obtener incidencias: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @return
     */
    @Transactional(readOnly = true)
  public List<DespachadorIncidenciaEntity> queryActivos() {
    try {
      return repo.findByEstatus(IncidenciaEstatus.ACTIVO);
    } catch (Exception e) {
      logger.error("Error al obtener incidencias: " + e.getMessage());
      return null;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public DespachadorIncidenciaEntity queryById(Integer id) {
    try {
      Optional<DespachadorIncidenciaEntity> optional = repo.findById(id);
      return optional.isPresent() ? optional.get() : null;
    } catch (Exception e) {
      logger.error("Error al obtener una incidencia: " + e.getMessage());
      return null;
    }
  }

  @Transactional(readOnly = true)
  private boolean exists(DespachadorIncidenciaEntity incidencia) {
    try {
      return incidencia != null && incidencia.getIdIncidencia() != null && repo.existsById(incidencia.getIdIncidencia());
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una incidencia: " + e.getMessage());
      return false;
    }
  }

    /**
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
  public boolean exists(Integer id) {
    try {
      return repo.existsById(id);
    } catch (Exception e) {
      logger.error("Error al comprobar si existe una incidencia: " + e.getMessage());
      return false;
    }
  }

  @Transactional()
  private boolean save(DespachadorIncidenciaEntity incidencia) {
    try {
      repo.save(incidencia);
      return true;
    } catch (Exception e) {
      logger.error("Error al guardar una incidencia: " + e.getMessage());

      e.printStackTrace();
      return false;
    }
  }
}
