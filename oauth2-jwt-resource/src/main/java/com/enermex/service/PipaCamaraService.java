package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enermex.repository.PipaCamaraRepository;
import com.enermex.modelo.PipaCamara;

/**
 *
 * @author abraham
 */
@Service
public class PipaCamaraService {
	
	@Autowired
	PipaCamaraRepository pipaCamaraRepository;
	
    /**
     *
     * @param e
     */
    public void save (PipaCamara e) {
    	
    	
    	pipaCamaraRepository.save(e);
    } 
    
    /**
     *
     * @param id
     * @return
     */
    public List<PipaCamara> getSelectedCams(Integer id){
    	
    	return this.pipaCamaraRepository.getAll(id);
    }
    
    /**
     *
     * @param idPipa
     */
    public void deleteCurrentePipaCamara(Integer idPipa) {
    	
    	 this.pipaCamaraRepository.deleteCurrentePipaCamara(idPipa);    	
    	
    }
    
    /**
     *
     * @return
     */
    public List<PipaCamara> camarasAsigandas(){
    	
    	return this.pipaCamaraRepository.camarasAsigandas();
    }
    
    /**
     *
     * @param idPipa
     * @return
     */
    public List<PipaCamara> camarasAsigandasOtrasPipas(Integer idPipa){
    	
    	return this.pipaCamaraRepository.camarasAsigandasOtrasPipas(idPipa);
    }
    
    
   
    

}
