package com.enermex.modelo;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.enermex.enumerable.RutaEstatus;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * RutaEntity
 */
@Entity
@Table(name = "t_ruta")
public class RutaEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_ruta")
  Integer idRuta;
  
  @Column(name = "nombre")
  String nombre;
  
  @Column(name = "estatus")
  RutaEstatus estatus;
  
  @Column(name = "fecha_creacion")
  Date fechaCreacion;

  @Column(name = "fecha_baja")
  Date fechaBaja;

  @Column(name = "lat")
  String lat;

  @Column(name = "lng")
  String lng;



  
  // @OneToMany(cascade = CascadeType.ALL)  
  // @JoinColumn(name = "id_ruta")

  // REQ: Itzia V. - Un despachador puede tener asignada más de una ruta
  @JsonIgnore
  @ManyToMany(mappedBy = "rutas")
  private Set<UsuarioEntity> despachadores = new HashSet();
  
  
  @JoinTable(
      name = "t_ruta_codigo",
      joinColumns = @JoinColumn(name = "id_ruta", nullable = false),
      inverseJoinColumns = @JoinColumn(name="id_codigo", nullable = false)
  )
  @ManyToMany(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
  private List<RutaCodigoEntity> codigos;

    /**
     *
     * @return
     */
    public Integer getIdRuta() {
    return idRuta;
  }

    /**
     *
     * @param idRuta
     */
    public void setIdRuta(Integer idRuta) {
    this.idRuta = idRuta;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public RutaEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(RutaEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public List<RutaCodigoEntity> getCodigos() {
    return codigos;
  }

    /**
     *
     * @param codigos
     */
    public void setCodigos(List<RutaCodigoEntity> codigos) {
    this.codigos = codigos;
  }

    /**
     *
     * @return
     */
    public Date getFechaBaja() {
    return fechaBaja;
  }

    /**
     *
     * @param fechaBaja
     */
    public void setFechaBaja(Date fechaBaja) {
    this.fechaBaja = fechaBaja;
  }

    /**
     *
     * @return
     */
    public Set<UsuarioEntity> getDespachadores() {
    return despachadores;
  }

    /**
     *
     * @param despachadores
     */
    public void setDespachadores(Set<UsuarioEntity> despachadores) {
    this.despachadores = despachadores;
  }

    /**
     *
     * @return
     */
    public String getLat() {
    return lat;
  }

    /**
     *
     * @param lat
     */
    public void setLat(String lat) {
    this.lat = lat;
  }

    /**
     *
     * @return
     */
    public String getLng() {
    return lng;
  }

    /**
     *
     * @param lng
     */
    public void setLng(String lng) {
    this.lng = lng;
  }
}
