package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="t_cliente_sms_registro")
public class ClienteSmsRegistro implements Serializable{



	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private BigInteger id;
	
	@Column(name="id_cliente")
	private BigInteger idCliente;
	
	@Column(name="codigo")
	private String codigo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_creacion")
	private Calendar fechaCreacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_vigencia")
	private Calendar fechaVigencia;
	
	@Column(name="activo")
	private Boolean activo;

	/**
	 * 
	 * @param csr
	 */
	public ClienteSmsRegistro() {
		super();
		
		this.fechaCreacion = Calendar.getInstance();
		
	}

    /**
     *
     * @return
     */
    public BigInteger getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(BigInteger id) {
		this.id = id;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public String getCodigo() {
		return codigo;
	}

    /**
     *
     * @param codigo
     */
    public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaVigencia() {
		return fechaVigencia;
	}

    /**
     *
     * @param fechaVigencia
     */
    public void setFechaVigencia(Calendar fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

    /**
     *
     * @return
     */
    public Boolean getActivo() {
		return activo;
	}

    /**
     *
     * @param activo
     */
    public void setActivo(Boolean activo) {
		this.activo = activo;
	}
    
	
	


}
