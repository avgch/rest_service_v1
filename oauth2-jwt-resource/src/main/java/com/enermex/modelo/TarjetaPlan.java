package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_tarjeta_plan")
@Entity
public class TarjetaPlan {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_tarjeta_plan")
	private Integer id_tarjeta_plan;
    
    
    @Column(name = "id_tarjeta_cliente")
	private Integer idTarjetaCliente;
    
    @Column(name = "id_plan")
	private Integer idPlan;
    
    /**
     *
     */
    public TarjetaPlan() {}

    /**
     *
     * @return
     */
    public Integer getIdTarjetaCliente() {
		return idTarjetaCliente;
	}

    /**
     *
     * @param idTarjetaCliente
     */
    public void setIdTarjetaCliente(Integer idTarjetaCliente) {
		this.idTarjetaCliente = idTarjetaCliente;
	}

    /**
     *
     * @return
     */
    public Integer getIdPlan() {
		return idPlan;
	}

    /**
     *
     * @param idPlan
     */
    public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}

    /**
     *
     * @return
     */
    public Integer getId_tarjeta_plan() {
		return id_tarjeta_plan;
	}

    /**
     *
     * @param id_tarjeta_plan
     */
    public void setId_tarjeta_plan(Integer id_tarjeta_plan) {
		this.id_tarjeta_plan = id_tarjeta_plan;
	}
 
    

}
