package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_azure_bs")
public class AzureBsEntity {
  @Id
  @Column(name = "id_azure")
  private Integer idAzure;
  
  @Column(name = "container")
  private String container;
  
  @Column(name = "endpoint")
  private String endpoint;
  
  @Column(name = "sas")
  private String sas;

    /**
     *
     * @return
     */
    public Integer getIdAzure() {
    return idAzure;
  }

    /**
     *
     * @param idAzure
     */
    public void setIdAzure(Integer idAzure) {
    this.idAzure = idAzure;
  }

    /**
     *
     * @return
     */
    public String getContainer() {
    return container;
  }

    /**
     *
     * @param container
     */
    public void setContainer(String container) {
    this.container = container;
  }

    /**
     *
     * @return
     */
    public String getEndpoint() {
    return endpoint;
  }

    /**
     *
     * @param endpoint
     */
    public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

    /**
     *
     * @return
     */
    public String getSas() {
    return sas;
  }

    /**
     *
     * @param sas
     */
    public void setSas(String sas) {
    this.sas = sas;
  }
  
}
