package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "car_specification_value")
public class CarSpecificationValue {
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_car_specification_value")
	private Integer idCarSpecificationValue;
	
	@Column(name = "id_car_trim")
	private Integer  idCarTrim;
	
	@Column(name = "id_car_specification")
	private Integer  idCarSpecification;
	
	@Column(name = "value")
	private String  value;

    /**
     *
     * @return
     */
    public Integer getIdCarSpecificationValue() {
		return idCarSpecificationValue;
	}

    /**
     *
     * @param idCarSpecificationValue
     */
    public void setIdCarSpecificationValue(Integer idCarSpecificationValue) {
		this.idCarSpecificationValue = idCarSpecificationValue;
	}

    /**
     *
     * @return
     */
    public Integer getIdCarTrim() {
		return idCarTrim;
	}

    /**
     *
     * @param idCarTrim
     */
    public void setIdCarTrim(Integer idCarTrim) {
		this.idCarTrim = idCarTrim;
	}

    /**
     *
     * @return
     */
    public Integer getIdCarSpecification() {
		return idCarSpecification;
	}

    /**
     *
     * @param idCarSpecification
     */
    public void setIdCarSpecification(Integer idCarSpecification) {
		this.idCarSpecification = idCarSpecification;
	}

    /**
     *
     * @return
     */
    public String getValue() {
		return value;
	}

    /**
     *
     * @param value
     */
    public void setValue(String value) {
		this.value = value;
	}
	
	
	
	

}
