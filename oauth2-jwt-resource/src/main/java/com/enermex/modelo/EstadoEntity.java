package com.enermex.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * EstadoEntity
 */
@Entity
@Table(name = "t_cat_estado")
public class EstadoEntity {
  @Id
  @Column(name = "id_estado")
  Integer idEstado;

  @Column(name = "nombre")
  String nombre;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "estado")
  List<MunicipioEntity> municipios;

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public List<MunicipioEntity> getMunicipios() {
    return municipios;
  }

    /**
     *
     * @param municipios
     */
    public void setMunicipios(List<MunicipioEntity> municipios) {
    this.municipios = municipios;
  }
}
