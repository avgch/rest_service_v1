package com.enermex.modelo;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.enermex.enumerable.QuejaEstatus;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_queja")
public class QuejaEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_queja")
  Long idQueja;

  @Column(name = "fecha_creacion", updatable = false)
  Calendar fechaCreacion;
  
  @Column(name = "estatus")
  QuejaEstatus estatus;

  @Column(name = "id_usuario")
  Long idUsuario;
  
  @ManyToOne()
  @JoinColumn(name = "id_usuario", insertable = false, updatable = false)
  UsuarioEntity usuario;

  @Column(name = "id_despachador")
  Long idDespachador;
  
  @ManyToOne()
  @JoinColumn(name = "id_despachador", insertable = false, updatable = false)
  UsuarioEntity despachador;

  @Column(name = "id_cliente")
  BigInteger idCliente;
  
  @ManyToOne()
  @JoinColumn(name = "id_cliente", insertable = false, updatable = false)
  Cliente cliente;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "queja")
  List<QuejaEvidenciaEntity> evidencias;

  @Column(name = "id_pedido")
  private BigInteger idPedido;
  
  @ManyToOne()
  @JoinColumn(name = "id_pedido", insertable = false, updatable = false)
  Pedido pedido;

    /**
     *
     * @return
     */
    public Long getIdQueja() {
    return idQueja;
  }

    /**
     *
     * @param idQueja
     */
    public void setIdQueja(Long idQueja) {
    this.idQueja = idQueja;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public QuejaEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(QuejaEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public Long getIdUsuario() {
    return idUsuario;
  }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

    /**
     *
     * @return
     */
    public UsuarioEntity getUsuario() {
    return usuario;
  }

    /**
     *
     * @param usuario
     */
    public void setUsuario(UsuarioEntity usuario) {
    this.usuario = usuario;
  }

    /**
     *
     * @return
     */
    public Long getIdDespachador() {
    return idDespachador;
  }

    /**
     *
     * @param idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
    this.idDespachador = idDespachador;
  }

    /**
     *
     * @return
     */
    public UsuarioEntity getDespachador() {
    return despachador;
  }

    /**
     *
     * @param despachador
     */
    public void setDespachador(UsuarioEntity despachador) {
    this.despachador = despachador;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public Cliente getCliente() {
    return cliente;
  }

    /**
     *
     * @param cliente
     */
    public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }

    /**
     *
     * @return
     */
    public List<QuejaEvidenciaEntity> getEvidencias() {
    return evidencias;
  }

    /**
     *
     * @param evidencias
     */
    public void setEvidencias(List<QuejaEvidenciaEntity> evidencias) {
    this.evidencias = evidencias;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
    return idPedido;
  }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
    this.idPedido = idPedido;
  }

    /**
     *
     * @return
     */
    public Pedido getPedido() {
    return pedido;
  }

    /**
     *
     * @param pedido
     */
    public void setPedido(Pedido pedido) {
    this.pedido = pedido;
  }
}
