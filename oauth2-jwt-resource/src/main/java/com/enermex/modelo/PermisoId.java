package com.enermex.modelo;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Identificador de los permisos
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 16 de enero de 2020
 */
@Embeddable
public class PermisoId implements Serializable {
  /**
   * Version ID
   */
  private static final long serialVersionUID = 1L;

  @Column(name = "id_rol")
  Integer idRol;

  @Column(name = "pantalla")
  String pantalla;

  @Column(name = "permiso")
  String permiso;

  @Override
  public String toString(){
    return idRol + " - " + pantalla + " - " + permiso;
  }

  @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.idRol);
        hash = 59 * hash + Objects.hashCode(this.pantalla);
        hash = 59 * hash + Objects.hashCode(this.permiso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final PermisoId other = (PermisoId) obj;
        if (!Objects.equals(this.idRol, other.idRol)) {
            return false;
        }
        if (!Objects.equals(this.pantalla, other.pantalla)) {
            return false;
        }
        if (!Objects.equals(this.permiso, other.permiso)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public Integer getIdRol() {
    return idRol;
  }

    /**
     *
     * @param idRol
     */
    public void setIdRol(Integer idRol) {
    this.idRol = idRol;
  }

    /**
     *
     * @return
     */
    public String getPantalla() {
    return pantalla;
  }

    /**
     *
     * @param pantalla
     */
    public void setPantalla(String pantalla) {
    this.pantalla = pantalla;
  }

    /**
     *
     * @return
     */
    public String getPermiso() {
    return permiso;
  }

    /**
     *
     * @param permiso
     */
    public void setPermiso(String permiso) {
    this.permiso = permiso;
  }
}
