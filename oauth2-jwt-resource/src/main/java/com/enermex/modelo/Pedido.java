package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.UsuarioEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.enermex.dto.PedidoDto;

/**
 *
 * @author abraham
 */
@Table(name = "t_pedido")
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pedido implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_pedido")
	private BigInteger idPedido;
	
	@Column(name = "id_direccion")
	private Integer  idDireccion;
	
	@Column(name = "id_ruta")
	private Integer  idRuta;
	
	@Column(name = "id_despachador")
	private Long  idDespachador;

	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_ruta", referencedColumnName = "id_ruta",insertable = false, updatable = false)
	private RutaEntity ruta;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario",insertable = false, updatable = false)
	private UsuarioEntity despachador;
	
	@Column(name = "id_automovil")
	private Integer  idAutomovil;
	
	@Column(name = "id_combustible")
	private Integer  idCombustible;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_combustible", referencedColumnName = "id_combustible",insertable = false, updatable = false)
	private CatTipoCombustible  combustible;
	
	@Column(name = "id_servicio")
	private Integer  idServicio;
	
	@Column(name = "litro_carga")
	private String  litroCarga;
	
	//precio de gasolina
	@Column(name = "monto_carga")
	private Double  montoCarga;
	
//	@Column(name = "verifica_presion_neumatico")
//	private boolean  verificaPresionNeumatico;
	
	@Column(name = "llantas_delanteras_presion")
	private String  llantasDelanterasPresion;
	
	@Column(name = "llantas_traseras_presion")
	private String  llantasTraserasPresion;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Calendar fechaCreacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_pedido")
	private Calendar  fechaPedido;
	
	
	@Column(name = "id_promocion")
	private Long idPromocion;
	
	@Column(name = "carga_gas")
	private boolean  cargaGas;
	
	@Column(name = "lavado_auto")
	private boolean  lavadoAuto;
	
	@Column(name = "revision_neumaticos")
	private boolean  revisionNeumaticos;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
	private Cliente cliente;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_tarjeta_cliente", referencedColumnName = "id_tarjeta_cliente",insertable = false, updatable = false)
	private Tarjeta tarjeta;
	
	@Column(name = "estatus_pedido")
	private Integer estatusPedido;
	
	@Column(name = "detalle")
	private String detalle;
	
	@Column(name = "sub_total")
	private Double subTotal;
	
	
	@Column(name = "iva")
	private Double iva;
	
	@Column(name = "total_pagar")
	private Double totalPagar;
	
	@Column(name = "descuento")
	private Double descuento;
	
	@Column(name = "total_importe")
	private Double   totalImporte;
	
	@Column(name = "total_con_descuento")
	private Double   totalConDescuento;

	
	@Column(name="id_tarjeta_cliente")
	private Integer idTarjeta ;
	
	@Column(name="id_horario_servicio")
	private BigInteger idHorarioServicio ;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="tiempo_traslado")
	private String tiempoTraslado;
	
	@Column(name="distancia")
	private String distancia;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_tipo_pedido", referencedColumnName = "id_tipo_pedido")
	private TipoPedido tipoPedido;
	
   
   @OneToOne(mappedBy = "pedido", cascade = CascadeType.ALL)
   private Cancelacion cancelacion;
	 
   
   @OneToOne(cascade = CascadeType.ALL)
   @JoinColumn(name = "id_promocion", referencedColumnName = "id_promocion",insertable = false, updatable = false)
   private Promocion promo;
   
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_hora_inicio")
	private Calendar  fechaHoraInicio;
    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_hora_fin")
	private Calendar  fechaHoraFin;
	
	
	@Column(name="precio_lavado_auto")
	private Double precioLavadoAuto ;
	
	
	@Column(name="precio_presion_neumaticos")
	private Double precioPresionNeumaticos ;

	@Column(name="calificado")
	private Boolean calificado ;
	
	@Column(name="costo_servicio")
	private Double costoServicio ;

	@Column(name="id_pipa")
	private Integer idPipa ;
	
	
	@Column(name="litros_despachados")
	private String litrosDespachados ;
	
	@Column(name="latitud_despachador")
	private String latitudDespachador ;
	
	@Column(name="logitud_despachador")
	private String longitudDespachador ;
	
	@Column(name="precio_por_litro")
	private Double precioPorLitro ;
	
	@Column(name = "precio_por_litro_final")
	private Double   precioPorLitroFinal;
	
	@Column(name="alcaldia_municipio")
	private String alcaldiaMunicipio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="hora_estimada")
	private Calendar horaEstimada;  
	
    /**
     *
     */
    public Pedido() {
		
	}
	
    /**
     *
     * @param p
     */
    public Pedido(Pedido p) {
		super();
		this.idPedido = p.getIdPedido();
		this.idDireccion = p.getIdDireccion();
		this.idRuta = p.getIdRuta();
		this.idDespachador = p.getIdDespachador();
		this.ruta = p.getRuta();
		this.despachador = p.getDespachador();
		this.idAutomovil = p.getIdAutomovil();
		this.idCombustible = p.getIdCombustible();
		this.combustible = p.getCombustible();
		this.idServicio = p.getIdServicio();
		this.litroCarga = p.getLitroCarga();
		this.montoCarga = p.getMontoCarga();
		this.llantasDelanterasPresion = p.getLlantasDelanterasPresion();
		this.llantasTraserasPresion = p.getLlantasTraserasPresion();
		this.fechaCreacion = p.getFechaCreacion();
		this.fechaPedido = p.getFechaPedido();
		this.idPromocion = p.getIdPromocion();
		this.cargaGas = p.isCargaGas();
		this.lavadoAuto = p.isLavadoAuto();
		this.revisionNeumaticos = p.isRevisionNeumaticos();
		this.cliente = p.getCliente();
		this.tarjeta = p.getTarjeta();
		this.estatusPedido = p.getEstatusPedido();
		this.detalle = p.getDetalle();
		this.subTotal = p.getSubTotal();
		this.iva = p.getIva();
		this.totalPagar = p.getTotalPagar();
		this.descuento = p.getDescuento();
		this.totalImporte = p.getTotalImporte();
		this.totalConDescuento = p.getTotalConDescuento();
		this.idTarjeta = p.getIdTarjeta();
		this.idHorarioServicio = p.getIdHorarioServicio();
		this.uuid = p.getUuid();
		this.tiempoTraslado = p.getTiempoTraslado();
		this.distancia = p.getDistancia();
		this.tipoPedido = p.getTipoPedido();
		this.cancelacion = p.getCancelacion();
		this.promo = p.getPromo();
		this.fechaHoraInicio = p.getFechaHoraInicio();
		this.fechaHoraFin = p.getFechaHoraFin();
		this.precioLavadoAuto = p.getPrecioLavadoAuto();
		this.precioPresionNeumaticos = p.getPrecioPresionNeumaticos();
		this.calificado = p.getCalificado();
		this.costoServicio = p.getCostoServicio();
		this.idPipa = p.getIdPipa();
		this.litrosDespachados = p.getLitrosDespachados();
		this.latitudDespachador = p.getLatitudDespachador();
		this.longitudDespachador = p.getLongitudDespachador();
		this.precioPorLitro = p.getPrecioPorLitro();
		this.precioPorLitroFinal = p.getPrecioPorLitroFinal();
		this.alcaldiaMunicipio = p.getAlcaldiaMunicipio();
		this.horaEstimada = p.getHoraEstimada();
	}

    /**
     *
     * @param pedido
     * @throws ParseException
     */
    public Pedido(PedidoDto pedido) throws ParseException {
		
		
		
		this.idDireccion = pedido.getIdDireccion();
		this.idRuta = pedido.getIdRuta();
		this.idDespachador = pedido.getIdDespachador();
		this.idAutomovil = pedido.getIdAutomovil();
		this.idCombustible = pedido.getIdCombustible();
		this.idServicio = pedido.getIdServicio();
		this.litroCarga = pedido.getLitroCarga();
		this.llantasDelanterasPresion = pedido.getLlantasDelanterasPresion();
		this.llantasTraserasPresion = pedido.getLlantasTraserasPresion();
		this.fechaCreacion = Calendar.getInstance();
		this.idPromocion = pedido.getIdPromocion();
		this.cargaGas = pedido.isCargaGas();
		this.lavadoAuto = pedido.isLavadoAuto();
		this.revisionNeumaticos = pedido.isRevisionNeumaticos();		
		this.idTarjeta = pedido.getIdTarjeta();
		this.calificado = new Boolean(false);
		this.tiempoTraslado = "0";
		this.distancia = "0";
		this.alcaldiaMunicipio = pedido.getAlcaldiaMunicipio();
		
		//montos 
		this.totalImporte = pedido.getTotalImporte();
		this.totalConDescuento = pedido.getTotalConDescuento();
		this.subTotal = pedido.getSubTotal();
		this.iva = pedido.getIva();
		this.totalPagar = pedido.getTotalPagar();
		this.montoCarga = pedido.getMontoCarga();
		this.precioLavadoAuto = pedido.getPrecioLavadoAutoS();
		this.descuento = pedido.getDescuento();
		this.costoServicio = pedido.getCostoServicio();
		this.precioPorLitro = pedido.getPrecioPorLitro();
		this.precioPresionNeumaticos = pedido.getPrecioRevicionLLantasS();
		
		
	}
	
    /**
     *
     * @return idPedido
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     * idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     *
     * @return idDireccion
     */
    public Integer getIdDireccion() {
		return idDireccion;
	}

    /**
     *
     * @param idDireccion
     * idDireccion
     */
    public void setIdDireccion(Integer idDireccion) {
		this.idDireccion = idDireccion;
	}

    /**
     *
     * @return idRuta
     */
    public Integer getIdRuta() {
		return idRuta;
	}

    /**
     *
     * @param idRuta
     * idRuta
     */
    public void setIdRuta(Integer idRuta) {
		this.idRuta = idRuta;
	}

    /**
     *
     * @return idDespachador
     */
    public Long getIdDespachador() {
		return idDespachador;
	}

    /**
     *
     * @param idDespachador
     * idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
		this.idDespachador = idDespachador;
	}

    /**
     *
     * @return idAutomovil
     */
    public Integer getIdAutomovil() {
		return idAutomovil;
	}

    /**
     *
     * @param idAutomovil
     * idAutomovil
     */
    public void setIdAutomovil(Integer idAutomovil) {
		this.idAutomovil = idAutomovil;
	}

    /**
     *
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return idServicio
     */
    public Integer getIdServicio() {
		return idServicio;
	}

    /**
     *
     * @param idServicio
     * idServicio
     */
    public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

    /**
     *
     * @return litroCarga
     */
    public String getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     * litroCarga
     */
    public void setLitroCarga(String litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     *
     * @return montoCarga
     */
    public Double getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     * montoCarga
     */
    public void setMontoCarga(Double montoCarga) {
		this.montoCarga = montoCarga;
	}

//	public boolean isVerificaPresionNeumatico() {
//		return verificaPresionNeumatico;
//	}
//
//	public void setVerificaPresionNeumatico(boolean verificaPresionNeumatico) {
//		this.verificaPresionNeumatico = verificaPresionNeumatico;
//	}

    /**
     *
     * @return llantasDelanterasPresion
     */

	public String getLlantasDelanterasPresion() {
		return llantasDelanterasPresion;
	}		

    /**
     *
     * @param llantasDelanterasPresion
     * llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
		this.llantasDelanterasPresion = llantasDelanterasPresion;
	}

    /**
     *
     * @return llantasTraserasPresion
     */
    public String getLlantasTraserasPresion() {
		return llantasTraserasPresion;
	}

    /**
     *
     * @param llantasTraserasPresion
     * llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion) {
		this.llantasTraserasPresion = llantasTraserasPresion;
	}

    /**
     *
     * @return idPromocion
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     * idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     *
     * @return cargaGas
     */
    public boolean isCargaGas() {
		return cargaGas;
	}

    /**
     *
     * @param cargaGas
     * cargaGas
     */
    public void setCargaGas(boolean cargaGas) {
		this.cargaGas = cargaGas;
	}

    /**
     *
     * @return lavadoAuto
     */
    public boolean isLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     * lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     *
     * @return revisionNeumaticos
     */
    public boolean isRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     * revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     *
     * @return fechaPedido
     */
    public Calendar getFechaPedido() {
		return fechaPedido;
	}

    /**
     *
     * @param fechaPedido
     * fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido)  {
		this.fechaPedido = fechaPedido;
	}

    /**
     *
     * @return despachador
     */
    public UsuarioEntity getDespachador() {
		return despachador;
	}

    /**
     *
     * @param despachador
     * despachador
     */
    public void setDespachador(UsuarioEntity despachador) {
		this.despachador = despachador;
	}

    /**
     *
     * @return estatusPedido
     */
    public Integer getEstatusPedido() {
		return estatusPedido;
	}

    /**
     *
     * @param estatusPedido
     * estatusPedido
     */
    public void setEstatusPedido(Integer estatusPedido) {
		this.estatusPedido = estatusPedido;
	}

    /**
     *
     * @return detalle
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     *
     * @param detalle
     * detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

    /**
     *
     * @return subTotal
     */
    public Double getSubTotal() {
		return subTotal;
	}

    /**
     *
     * @param subTotal
     * subTotal
     */
    public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

    /**
     *
     * @return iva
     */
    public Double getIva() {
		return iva;
	}

    /**
     *
     * @param iva
     * iva
     */
    public void setIva(Double iva) {
		this.iva = iva;
	}

    /**
     *
     * @return totalPagar
     */
    public Double getTotalPagar() {
		return totalPagar;
	}

    /**
     *
     * @param totalPagar
     * totalPagar
     */
    public void setTotalPagar(Double totalPagar) {
		this.totalPagar = totalPagar;
	}

    /**
     *
     * @return descuento
     */
    public Double getDescuento() {
		return descuento;
	}

    /**
     *
     * @param descuento
     * descuento
     */
    public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

    /**
     *
     * @return idTarjeta
     */
    public Integer getIdTarjeta() {
		return idTarjeta;
	}

    /**
     *
     * @param idtarjeta
     * idtarjeta
     */
    public void setIdTarjeta(Integer idtarjeta) {
		this.idTarjeta = idtarjeta;
	}

    /**
     *
     * @return idHorarioServicio
     */
    public BigInteger getIdHorarioServicio() {
		return idHorarioServicio;
	}

    /**
     *
     * @param idHorarioServicio
     * idHorarioServicio
     */
    public void setIdHorarioServicio(BigInteger idHorarioServicio) {
		this.idHorarioServicio = idHorarioServicio;
	}

    /**
     *
     * @return fechaCreacion
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return tipoPedido
     */
    public TipoPedido getTipoPedido() {
		return tipoPedido;
	}

    /**
     *
     * @param tipoPedido
     * tipoPedido
     */
    public void setTipoPedido(TipoPedido tipoPedido) {
		this.tipoPedido = tipoPedido;
	}

    /**
     *
     * @return calificado
     */
    public Boolean getCalificado() {
		return calificado;
	}

    /**
     *
     * @param calificado
     * calificado
     */
    public void setCalificado(Boolean calificado) {
		this.calificado = calificado;
	}

    /**
     *
     * @return cancelacion
     */
    @JsonIgnore
	public Cancelacion getCancelacion() {
		return cancelacion;
	}

    /**
     *
     * @param cancelacion
     * cancelacion
     */
    public void setCancelacion(Cancelacion cancelacion) {
		this.cancelacion = cancelacion;
	}

    /**
     *
     * @return cliente
     */
    @JsonIgnore
	public Cliente getCliente() {
		return cliente;
	}

    /**
     *
     * @param cliente
     * cliente
     */
    public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

    /**
     *
     * @return uuid
     */
    public String getUuid() {
		return uuid;
	}

    /**
     *
     * @param uuid
     * uuid
     */
    public void setUuid(String uuid) {
		this.uuid = uuid;
	}

    /**
     *
     * @return tiempoTraslado
     */
    public String getTiempoTraslado() {
		return tiempoTraslado;
	}

    /**
     *
     * @param tiempoTraslado
     * tiempoTraslado
     */
    public void setTiempoTraslado(String tiempoTraslado) {
		this.tiempoTraslado = tiempoTraslado;
	}

    /**
     *
     * @return promo
     */
    @JsonIgnore
	public Promocion getPromo() {
		return promo;
	}

    /**
     *
     * @param promo
     * promo
     */
    public void setPromo(Promocion promo) {
		this.promo = promo;
	}

    /**
     *
     * @return precioLavadoAuto
     */
    @JsonIgnore
	public Double getPrecioLavadoAuto() {
		return precioLavadoAuto;
	}

    /**
     *
     * @param precioLavadoAuto
     * precioLavadoAuto
     */
    public void setPrecioLavadoAuto(Double precioLavadoAuto) {
		this.precioLavadoAuto = precioLavadoAuto;
	}

    /**
     *
     * @return combustible
     */
    @JsonIgnore
	public CatTipoCombustible getCombustible() {
		return combustible;
	}

    /**
     *
     * @param combustible
     * combustible
     */
    public void setCombustible(CatTipoCombustible combustible) {
		this.combustible = combustible;
	}

    /**
     *
     * @return tarjeta
     */
    @JsonIgnore
	public Tarjeta getTarjeta() {
		return tarjeta;
	}

    /**
     *
     * @param tarjeta
     * tarjeta
     */
    public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

    /**
     *
     * @return distancia
     */
    public String getDistancia() {
		return distancia;
	}

    /**
     *
     * @param distancia
     * distancia
     */
    public void setDistancia(String distancia) {
		this.distancia = distancia;
	}

    /**
     *
     * @return costoServicio
     */
    public Double getCostoServicio() {
		return costoServicio;
	}

    /**
     *
     * @param costoServicio
     * costoServicio
     */
    public void setCostoServicio(Double costoServicio) {
		this.costoServicio = costoServicio;
	}

    /**
     *
     * @return idPipa
     */
    public Integer getIdPipa() {
		return idPipa;
	}

    /**
     *
     * @param idPipa
     * idPipa
     */
    public void setIdPipa(Integer idPipa) {
		this.idPipa = idPipa;
	}

    /**
     *
     * @return fechaHoraInicio
     */
    public Calendar getFechaHoraInicio() {
		return fechaHoraInicio;
	}

    /**
     *
     * @param fechaHoraInicio
     * fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

    /**
     *
     * @return fechaHoraFin
     */
    public Calendar getFechaHoraFin() {
		return fechaHoraFin;
	}

    /**
     *
     * @param fechaHoraFin
     * fechaHoraFin
     */
    public void setFechaHoraFin(Calendar fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

    /**
     *
     * @return litrosDespachados
     */
    public String getLitrosDespachados() {
		return litrosDespachados;
	}

    /**
     *
     * @param litrosDespachados
     * litrosDespachados
     */
    public void setLitrosDespachados(String litrosDespachados) {
		this.litrosDespachados = litrosDespachados;
	}

    /**
     *
     * @return precioPresionNeumaticos
     */
    public Double getPrecioPresionNeumaticos() {
		return precioPresionNeumaticos;
	}

    /**
     *
     * @param precioPresionNeumaticos
     * precioPresionNeumaticos
     */
    public void setPrecioPresionNeumaticos(Double precioPresionNeumaticos) {
		this.precioPresionNeumaticos = precioPresionNeumaticos;
	}

    /**
     *
     * @return latitudDespachador
     */
    public String getLatitudDespachador() {
		return latitudDespachador;
	}

    /**
     *
     * @param latitudDespachador
     * latitudDespachador
     */
    public void setLatitudDespachador(String latitudDespachador) {
		this.latitudDespachador = latitudDespachador;
	}

    /**
     *
     * @return longitudDespachador
     */
    public String getLongitudDespachador() {
		return longitudDespachador;
	}

    /**
     *
     * @param longitudDespachador
     * longitudDespachador
     */
    public void setLongitudDespachador(String longitudDespachador) {
		this.longitudDespachador = longitudDespachador;
	}

    /**
     *
     * @return precioPorLitro
     */
    public Double getPrecioPorLitro() {
		return precioPorLitro;
	}

    /**
     *
     * @param precioPorLitro
     * precioPorLitro
     */
    public void setPrecioPorLitro(Double precioPorLitro) {
		this.precioPorLitro = precioPorLitro;
	}

    /**
     *
     * @return ruta
     */
    public RutaEntity getRuta() {
		return ruta;
	}

    /**
     *
     * @param ruta
     * ruta
     */
    public void setRuta(RutaEntity ruta) {
		this.ruta = ruta;
	}

    /**
     *
     * @return totalImporte
     */
    public Double getTotalImporte() {
		return totalImporte;
	}

    /**
     *
     * @param totalImporte
     * totalImporte
     */
    public void setTotalImporte(Double totalImporte) {
		this.totalImporte = totalImporte;
	}

    /**
     *
     * @return totalConDescuento
     */
    public Double getTotalConDescuento() {
		return totalConDescuento;
	}

    /**
     *
     * @param totalConDescuento
     * totalConDescuento
     */
    public void setTotalConDescuento(Double totalConDescuento) {
		this.totalConDescuento = totalConDescuento;
	}

    /**
     *
     * @return precioPorLitroFinal
     */
    public Double getPrecioPorLitroFinal() {
		return precioPorLitroFinal;
	}

    /**
     *
     * @param precioPorLitroFinal
     * precioPorLitroFinal
     */
    public void setPrecioPorLitroFinal(Double precioPorLitroFinal) {
		this.precioPorLitroFinal = precioPorLitroFinal;
	}

    /**
     *
     * @return alcaldiaMunicipio
     */
    public String getAlcaldiaMunicipio() {
		return alcaldiaMunicipio;
	}

    /**
     *
     * @param alcaldiaMunicipio
     * alcaldiaMunicipio
     */
    public void setAlcaldiaMunicipio(String alcaldiaMunicipio) {
		this.alcaldiaMunicipio = alcaldiaMunicipio;
	}

    /**
     *
     * @return horaEstimada
     */
    public Calendar getHoraEstimada() {
		return horaEstimada;
	}

    /**
     *
     * @param horaEstimada
     * horaEstimada
     */
    public void setHoraEstimada(Calendar horaEstimada) {
		this.horaEstimada = horaEstimada;
	}
    

  
	
	
	
    
	
   
	
}
