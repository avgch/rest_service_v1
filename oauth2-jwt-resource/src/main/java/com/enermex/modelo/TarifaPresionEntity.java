package com.enermex.modelo;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TarifaPresionEntity
 */
@Entity
@Table(name = "t_tarifa_presion")
public class TarifaPresionEntity {
  @Id
  @Column(name = "id_codigo")
  String idCodigo;

  @Column(name = "precio")
  String precio;

  @Column(name = "fecha_creacion")
  Calendar fechaCreacion;

    /**
     *
     * @return
     */
    public String getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(String idCodigo) {
    this.idCodigo = idCodigo;
  }

    /**
     *
     * @return
     */
    public String getPrecio() {
    return precio;
  }

    /**
     *
     * @param precio
     */
    public void setPrecio(String precio) {
    this.precio = precio;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }
  
}