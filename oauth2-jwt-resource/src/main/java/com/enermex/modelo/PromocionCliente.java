package com.enermex.modelo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_promocion_cliente")
public class PromocionCliente {
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_promocion_cliente")
	private Long idPromocionCliente;
	
	@Column(name = "id_cliente")
	private BigInteger idCliente;
	
	@Column(name = "id_promocion")
	private Long idPromocion;

    /**
     *
     * @return
     */
    public Long getIdPromocionCliente() {
		return idPromocionCliente;
	}

    /**
     *
     * @param idPromocionCliente
     */
    public void setIdPromocionCliente(Long idPromocionCliente) {
		this.idPromocionCliente = idPromocionCliente;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}
	
	

}
