package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_pedido_conekta")
@Entity
public class PedidoConekta implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_pedido_conekta")
	private Long idPedidoConekta;
	
	
	@Column(name = "id_pedido")
	private BigInteger idPedido;
	
	@Column(name = "id_orden")
	private String idOrden;

    /**
     *
     * @return
     */
    public Long getIdPedidoConekta() {
		return idPedidoConekta;
	}

    /**
     *
     * @param idPedidoConekta
     */
    public void setIdPedidoConekta(Long idPedidoConekta) {
		this.idPedidoConekta = idPedidoConekta;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     *
     * @return
     */
    public String getIdOrden() {
		return idOrden;
	}

    /**
     *
     * @param idOrden
     */
    public void setIdOrden(String idOrden) {
		this.idOrden = idOrden;
	}
	
	
	
	

}
