package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_tipo_pedido")
@Entity
public class TipoPedido implements Serializable {
	
	
	/**
	*
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tipo_pedido")
	private Integer idTipoPedido;
	
	
	@Column(name = "tipo")
	private String tipo;
	
	
	@Column(name = "create_on")
	private Date fechaCreacion;
	
	
	@Column(name = "create_by")
	private String creadoPor;

    /**
     *
     * @return
     */
    public Integer getIdTipoPedido() {
		return idTipoPedido;
	}

    /**
     *
     * @param idTipoPedido
     */
    public void setIdTipoPedido(Integer idTipoPedido) {
		this.idTipoPedido = idTipoPedido;
	}

    /**
     *
     * @return
     */
    public String getTipo() {
		return tipo;
	}

    /**
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
		this.tipo = tipo;
	}

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public String getCreadoPor() {
		return creadoPor;
	}

    /**
     *
     * @param creadoPor
     */
    public void setCreadoPor(String creadoPor) {
		this.creadoPor = creadoPor;
	}
	
	
	
	
}
