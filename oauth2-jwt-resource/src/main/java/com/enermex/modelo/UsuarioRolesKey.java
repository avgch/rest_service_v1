
/*##############################################################################
# Nombre del Programa : UsuarioRolesKey.java                                          #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/



package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abraham
 */
@Embeddable
public class UsuarioRolesKey implements Serializable  {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Column(name="id_usuario")
	private Integer idUsuario;
	@Column(name="id_rol")
	private Integer idRol;
	
    /**
     *
     */
    public UsuarioRolesKey() {}

    /**
     *
     * @return
     */
    public Integer getIdUsuario() {
		return idUsuario;
	}

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

    /**
     *
     * @return
     */
    public Integer getIdRol() {
		return idRol;
	}

    /**
     *
     * @param idRol
     */
    public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	

}
