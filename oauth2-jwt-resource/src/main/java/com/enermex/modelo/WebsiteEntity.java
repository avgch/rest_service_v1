package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_website")
public class WebsiteEntity {
  @Id
  @Column(name = "id_website")
  private Integer idWebsite;

  @Column(name = "content")
  private String content;

  @Column(name = "preview")
  private String preview;

    /**
     *
     * @return
     */
    public Integer getIdWebsite() {
    return idWebsite;
  }

    /**
     *
     * @param idWebsite
     */
    public void setIdWebsite(Integer idWebsite) {
    this.idWebsite = idWebsite;
  }

    /**
     *
     * @return
     */
    public String getContent() {
    return content;
  }

    /**
     *
     * @param content
     */
    public void setContent(String content) {
    this.content = content;
  }

    /**
     *
     * @return
     */
    public String getPreview() {
    return preview;
  }

    /**
     *
     * @param preview
     */
    public void setPreview(String preview) {
    this.preview = preview;
  }
}