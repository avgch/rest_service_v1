package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_gasolina_precio")
public class GasolinaPrecio implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_gasolina_precio")
	private Integer idGasolinaPrecio;
	
	@Column(name = "precio_por_litro")
	private Double precioPorLitro;
	
	@Column(name = "cp")
	private String cp;
	
	@Column(name = "idTipoCombustible")
	private Integer idTipoCombustible;
	
    /**
     *
     * @return
     */
    public Integer getIdGasolinaPrecio() {
		return idGasolinaPrecio;
	}

    /**
     *
     * @param idGasolinaPrecio
     */
    public void setIdGasolinaPrecio(Integer idGasolinaPrecio) {
		this.idGasolinaPrecio = idGasolinaPrecio;
	}

    /**
     *
     * @return
     */
    public Double getPrecioPorLitro() {
		return precioPorLitro;
	}

    /**
     *
     * @param precioPorLitro
     */
    public void setPrecioPorLitro(Double precioPorLitro) {
		this.precioPorLitro = precioPorLitro;
	}

    /**
     *
     * @return
     */
    public String getCp() {
		return cp;
	}

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
		this.cp = cp;
	}

    /**
     *
     * @return
     */
    public Integer getIdTipoCombustible() {
		return idTipoCombustible;
	}

    /**
     *
     * @param idTipoCombustible
     */
    public void setIdTipoCombustible(Integer idTipoCombustible) {
		this.idTipoCombustible = idTipoCombustible;
	}
	
	

	

}
