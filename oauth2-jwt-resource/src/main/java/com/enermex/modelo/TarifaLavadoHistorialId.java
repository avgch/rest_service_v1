package com.enermex.modelo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;

import javax.persistence.Column;


/**
 * TarifaLavadoId
 */
public class TarifaLavadoHistorialId implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "id_codigo")
  String idCodigo;

  @Column(name = "fecha_creacion")
  Calendar fechaCreacion;

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 59 * hash + Objects.hashCode(idCodigo);
    hash = 59 * hash + Objects.hashCode(fechaCreacion);

    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final TarifaLavadoHistorialId other = (TarifaLavadoHistorialId) obj;
    if (!Objects.equals(this.idCodigo, other.idCodigo)) {
      return false;
    }
    if (!Objects.equals(this.fechaCreacion, other.fechaCreacion)) {
      return false;
    }

    return true;
  }
  
}
