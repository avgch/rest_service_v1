/*##############################################################################
# Nombre del Programa : RolDto.java                                          #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               : Antonio González                                       #
# Compania            : Enermex                                                #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/



package com.enermex.modelo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.enermex.modelo.UsuarioEntity;

import org.springframework.stereotype.Component;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="t_rol")
public class RolEntity implements Serializable{

  private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_rol")
	private Integer idRol;
	
	@Column(name="rol")
	private String rol;
	
	@Column(name="descripcion")
	private String descripcion;
	
    /**
     *
     */
    public RolEntity() {
		
	}

    /**
     *
     * @return
     */
    public Integer getIdRol() {
		return idRol;
	}

    /**
     *
     * @param idRol
     */
    public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

    /**
     *
     * @return
     */
    public String getRol() {
		return rol;
	}

    /**
     *
     * @param rol
     */
    public void setRol(String rol) {
		this.rol = rol;
	}

    /**
     *
     * @return
     */
    public String getDescripcion() {
		return descripcion;
	}

    /**
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


    
    
	

}
