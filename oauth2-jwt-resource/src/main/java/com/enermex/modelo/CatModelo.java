package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.enermex.modelo.CatMarca;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "car_model")
public class CatModelo implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_car_model")
	private Integer idModelo;
	
	@Column(name = "name")
	private String modelo;
	
	
//	 @ManyToOne
//	 @JoinColumn(name = "id_car_make")
//	 private CatMarca marca;

	@Column(name = "id_car_make")
	private Integer idMarca;

    /**
     *
     * @return idModelo
     */
    public Integer getIdModelo() {
		return idModelo;
	}

    /**
     *
     * @param idModelo
     * idModelo
     */
    public void setIdModelo(Integer idModelo) {
		this.idModelo = idModelo;
	}

    /**
     *
     * @return modelo
     */
    public String getModelo() {
		return modelo;
	}

    /**
     *
     * @param modelo
     * modelo
     */
    public void setModelo(String modelo) {
		this.modelo = modelo;
	}

    /**
     *
     * @return idMarca
     */
    public Integer getIdMarca() {
		return idMarca;
	}

    /**
     *
     * @param idMarca
     * idMarca
     */
    public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	

	 
	 
}
