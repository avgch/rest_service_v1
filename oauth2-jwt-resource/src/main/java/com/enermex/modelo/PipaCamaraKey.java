package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abraham
 */
@Embeddable
public class PipaCamaraKey implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	

	@Column(name="id_pipa")
	private Integer idPipa;
	
	@Column(name="id_camara")
	private Integer idCamara;

    /**
     *
     */
    public PipaCamaraKey() {
		super();
	}

    /**
     *
     * @return
     */
    public Integer getIdPipa() {
		return idPipa;
	}

    /**
     *
     * @param idPipa
     */
    public void setIdPipa(Integer idPipa) {
		this.idPipa = idPipa;
	}

    /**
     *
     * @return
     */
    public Integer getIdCamara() {
		return idCamara;
	}

    /**
     *
     * @param idCamara
     */
    public void setIdCamara(Integer idCamara) {
		this.idCamara = idCamara;
	}

	
	
	
}
