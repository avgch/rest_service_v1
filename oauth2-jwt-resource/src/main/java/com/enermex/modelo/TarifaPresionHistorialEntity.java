package com.enermex.modelo;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * TarifaPresionHistorialEntity
 */
@Entity
@IdClass(value = TarifaPresionHistorialId.class)
@Table(name = "t_tarifa_presion_historial")
public class TarifaPresionHistorialEntity {
  @Id
  @Column(name = "id_codigo")
  String idCodigo;
  
  @Id
  @Column(name = "fecha_creacion")
  Calendar fechaCreacion;

  @Column(name = "precio")
  String precio;

    /**
     *
     * @return
     */
    public String getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(String idCodigo) {
    this.idCodigo = idCodigo;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public String getPrecio() {
    return precio;
  }

    /**
     *
     * @param precio
     */
    public void setPrecio(String precio) {
    this.precio = precio;
  }
  
}