package com.enermex.modelo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="t_cliente_fcm")
public class ClienteFcmEntity {
  @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fcm")
	private BigInteger idFcm;
	
	@Column(name="id_cliente")
	private BigInteger idCliente;
	
	@Column(name="fcm")
  private String fcm;

    /**
     *
     * @return
     */
    public BigInteger getIdFcm() {
    return idFcm;
  }

    /**
     *
     * @param idFcm
     */
    public void setIdFcm(BigInteger idFcm) {
    this.idFcm = idFcm;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public String getFcm() {
    return fcm;
  }

    /**
     *
     * @param fcm
     */
    public void setFcm(String fcm) {
    this.fcm = fcm;
  }
}
