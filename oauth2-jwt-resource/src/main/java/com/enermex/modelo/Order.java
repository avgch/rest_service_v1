package com.enermex.modelo;

/**
 *
 * @author abraham
 */
public class Order {
	
    /**
     *
     */
    public int column;

    /**
     *
     */
    public String dir;

    /**
     *
     * @return
     */
    public int getColumn() {
		return column;
	}

    /**
     *
     * @param column
     */
    public void setColumn(int column) {
		this.column = column;
	}

    /**
     *
     * @return
     */
    public String getDir() {
		return dir;
	}

    /**
     *
     * @param dir
     */
    public void setDir(String dir) {
		this.dir = dir;
	}

    

}
