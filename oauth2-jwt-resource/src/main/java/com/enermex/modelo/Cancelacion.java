package com.enermex.modelo;

import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.enermex.modelo.CatMotivoCancelacion;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cancelacion")
public class Cancelacion {
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_cancelacion")
	private Long idCancelacion;
	
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pedido", referencedColumnName = "id_pedido")
	private Pedido pedido;
	
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "id_motivo", referencedColumnName = "id_motivo_cancelacion")
	private CatMotivoCancelacion motivo;
	
	
	@Column(name = "comentario")
	private String comentario;
	
	@Column(name = "create_on")
	private Calendar fechaCreacion;

    /**
     *
     */
    public Cancelacion() {}
   
    /**
     *
     * @param pedido
     * @param motivo
     * @param comentario
     */
    public Cancelacion(Pedido pedido,CatMotivoCancelacion motivo, String comentario) {
		super();
		this.pedido = pedido;
		this.motivo = motivo;
		this.comentario = comentario;
		this.fechaCreacion = Calendar.getInstance();
	}

    /**
     *
     * @return
     */
    public Long getIdCancelacion() {
		return idCancelacion;
	}

    /**
     *
     * @param idCancelacion
     */
    public void setIdCancelacion(Long idCancelacion) {
		this.idCancelacion = idCancelacion;
	}

    /**
     *
     * @return
     */
    public String getComentario() {
		return comentario;
	}

    /**
     *
     * @param comentario
     */
    public void setComentario(String comentario) {
		this.comentario = comentario;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public Pedido getPedido() {
		return pedido;
	}

    /**
     *
     * @param pedido
     */
    public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

    /**
     *
     * @return
     */
    public CatMotivoCancelacion getMotivo() {
		return motivo;
	}

    /**
     *
     * @param motivo
     */
    public void setMotivo(CatMotivoCancelacion motivo) {
		this.motivo = motivo;
	}
	
   
	 
 
	
	
	
	
	
	
}
