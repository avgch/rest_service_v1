package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * ColoniaEntity
 */
@Entity
@Table(name = "t_cat_colonia")
@IdClass(value = ColoniaId.class)
public class ColoniaEntity {
  @Id
  @Column(name = "id_codigo")
  Integer idCodigo;

  @Id
  @Column(name = "id_colonia")
  Integer idColonia;

  @Column(name = "nombre")
  String nombre;

  @JsonIgnore
  @ManyToOne()
  @JoinColumn(name = "id_codigo", insertable = false, updatable = false)
  private CodigoPostalEntity codigoPostal;

    /**
     *
     * @return
     */
    public Integer getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(Integer idCodigo) {
    this.idCodigo = idCodigo;
  }

    /**
     *
     * @return
     */
    public Integer getIdColonia() {
    return idColonia;
  }

    /**
     *
     * @param idColonia
     */
    public void setIdColonia(Integer idColonia) {
    this.idColonia = idColonia;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public CodigoPostalEntity getCodigoPostal() {
    return codigoPostal;
  }

    /**
     *
     * @param codigoPostal
     */
    public void setCodigoPostal(CodigoPostalEntity codigoPostal) {
    this.codigoPostal = codigoPostal;
  }
}
