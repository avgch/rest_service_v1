package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

import com.enermex.dto.ClienteDto;
import com.enermex.modelo.AutomovilCliente;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cliente")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cliente implements Serializable {

	/**
	*
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cliente")
	private BigInteger idCliente;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "correo")
	private String correo;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "contrasena")
	private String contrasena;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Calendar fechaCreacion;

	@Column(name = "id_titular")
	private BigInteger idTitular;

	@Column(name = "foto_uri")
	private String fotoUri;

	@Column(name = "id_estatus")
	private int estatus;

	@Column(name = "acceso")
	private boolean acceso;

	@Column(name = "bloqueo")
	private boolean bloqueo;

	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	// private List<DireccionCliente> direcciones = new
	// ArrayList<DireccionCliente>();
	private Set<DireccionCliente> direcciones = new HashSet();

	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	// private List<AutomovilCliente> autos = new ArrayList<AutomovilCliente>();
	private Set<AutomovilCliente> autos = new HashSet();

// @JoinTable(
//        name = "t_cliente_cat_tipo_plan",
//        joinColumns = @JoinColumn(name = "id_cliente", nullable = false),
//        inverseJoinColumns = @JoinColumn(name="id_plan", nullable = false)
//    )
// @ManyToMany(cascade = CascadeType.ALL)
//    private Set<TipoPlan> planes;

	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private Set<UsuarioTipoPlanA> planes = new HashSet();

	@Column(name = "apellido_paterno")
	private String apellidoPaterno;

	@Column(name = "apellido_materno")
	private String apellidoMaterno;

	@Column(name = "token_conekta")
	private String tokenConekta;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_baja")
	private Date fechaBaja;
	
	@Column(name = "acceso_sms")
	private Boolean accesoSms;
	
    /**
     *
     */
    public Cliente() {
	}

    /**
     *
     * @return fechaBaja
     */
    public Date getFechaBaja() {
		return fechaBaja;
	}

    /**
     *
     * @param fechaBaja
     * fechaBaja
     */
    public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

    /**
     *
     * @param planes
     * planes
     */
    public Cliente(UsuarioTipoPlanA... planes) {
		for (UsuarioTipoPlanA plan : planes)
			plan.setCliente(this);
		this.planes = Stream.of(planes).collect(Collectors.toSet());
	}

	/**
	 *
	 * @param clienteP
	 */
	public Cliente(ClienteDto clienteP) {
		super();
		DireccionCliente dir;
		AutomovilCliente aut;
		UsuarioTipoPlanA ustp;
		TipoPlan tp;
		this.nombre = clienteP.getNombre();
		this.apellidoPaterno = clienteP.getApellidoPaterno();
		this.apellidoMaterno = clienteP.getApellidoMaterno();
		this.correo = clienteP.getCorreo();
		this.telefono = clienteP.getTelefono();
		this.contrasena = clienteP.getContrasena();
		this.fechaCreacion = Calendar.getInstance();
		this.idTitular = clienteP.getIdTitular();
		this.fotoUri = "";
		this.estatus = 1;
		this.acceso = false;
		this.accesoSms = false;

		if (clienteP.getDirecciones() != null) {
			for (DireccionCliente dc : clienteP.getDirecciones()) {

				dir = new DireccionCliente(dc);

				this.addToDirecciones(dir);
			}

		}
		if (clienteP.getAutos() != null) {

			for (AutomovilCliente ac : clienteP.getAutos()) {
				aut = new AutomovilCliente(ac);
				this.addToAutomovil(aut);

			}

		}
		if (clienteP.getPlanesId() != null) {

			for (Integer id : clienteP.getPlanesId()) {
				tp = new TipoPlan();
				tp.setId(id);
				ustp = new UsuarioTipoPlanA(tp);
				this.addToPlan(ustp);
			}

		}

	}

    /**
     * 
     * @return idCliente
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     * idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return nombre
     * nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return correo
     */
    public String getCorreo() {
		return correo;
	}

    /**
     *
     * @param correo
     * correo
     */
    public void setCorreo(String correo) {
		this.correo = correo;
	}

    /**
     *
     * @return telefono
     */
    public String getTelefono() {
		return telefono;
	}

    /**
     *
     * @param telefono
     * telefono
     */
    public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

    /**
     *
     * @return contrasena
     */
    @JsonIgnore
	public String getContrasena() {
		return contrasena;
	}

    /**
     *
     * @param contrasena
     * contrasena
     */
    public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

    /**
     *
     * @return fechaCreacion
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return idTitular
     */
    public BigInteger getIdTitular() {
		return idTitular;
	}

    /**
     *
     * @param idTitular
     * idTitular
     */
    public void setIdTitular(BigInteger idTitular) {
		this.idTitular = idTitular;
	}

    /**
     *
     * @return fotoUri
     */
    public String getFotoUri() {
		return fotoUri;
	}

    /**
     *
     * @param fotoUri
     * fotoUri
     */
    public void setFotoUri(String fotoUri) {
		this.fotoUri = fotoUri;
	}

    /**
     *
     * @return estatus
     */
    public Integer getEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

// public List<AutomovilCliente> getAutos() {
// return autos;
// }
//
//
// public void setAutos(List<AutomovilCliente> autos) {
// this.autos = autos;
// }

    /**
     *
     * @param estatus
     * estatus
     */

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

// public List<DireccionCliente> getDirecciones() {
// return direcciones;
// }
//
//
// public void setDirecciones(List<DireccionCliente> direcciones) {
// this.direcciones = direcciones;
// }

// public Set<TipoPlan> getPlanes() {
// return planes;
// }
//
// public void setPlanes(Set<TipoPlan> planes) {
// this.planes = planes;
// }

    /**
     *
     * @return acceso
     */

	@JsonIgnore
	public boolean isAcceso() {
		return acceso;
	}

    /**
     *
     * @param acceso
     * acceso
     */
    public void setAcceso(boolean acceso) {
		this.acceso = acceso;
	}

    /**
     *
     * @param direccion
     * direccion
     */
    public void addToDirecciones(DireccionCliente direccion) {
		direccion.setCliente((this));
		this.direcciones.add(direccion);
	}

    /**
     *
     * @param auto
     * auto
     */
    public void addToAutomovil(AutomovilCliente auto) {
		auto.setCliente((this));
		this.autos.add(auto);
	}

    /**
     *
     * @param ustp
     * ustp
     */
    public void addToPlan(UsuarioTipoPlanA ustp) {
		ustp.setCliente((this));
		this.planes.add(ustp);
	}

    /**
     *
     * @return direcciones
     */
    public Set<DireccionCliente> getDirecciones() {
		return direcciones;
	}

    /**
     *
     * @param direcciones
     * direcciones
     */
    public void setDirecciones(Set<DireccionCliente> direcciones) {
		this.direcciones = direcciones;
	}

    /**
     *
     * @return autos
     */
    public Set<AutomovilCliente> getAutos() {
		return autos;
	}

    /**
     *
     * @param autos
     * autos
     */
    public void setAutos(Set<AutomovilCliente> autos) {
		this.autos = autos;
	}

    /**
     *
     * @return planes
     */
    public Set<UsuarioTipoPlanA> getPlanes() {
		return planes;
	}

    /**
     *
     * @param planes
     * planes
     */
    public void setPlanes(Set<UsuarioTipoPlanA> planes) {
		this.planes = planes;
	}

    /**
     *
     * @return bloqueo
     */
    public boolean isBloqueo() {
		return bloqueo;
	}

    /**
     *
     * @param bloqueo
     * bloqueo
     */
    public void setBloqueo(boolean bloqueo) {
		this.bloqueo = bloqueo;
	}

    /**
     *
     * @return apellidoPaterno
     */
    public String getApellidoPaterno() {
		return apellidoPaterno;
	}

    /**
     *
     * @param apellidoPaterno
     * apellidoPaterno
     */
    public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

    /**
     * 
     * @return apellidoMaterno
     * 
     */
    public String getApellidoMaterno() {
		return apellidoMaterno;
	}

    /**
     *
     * @param apellidoMaterno
     * apellidoMaterno
     */
    public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

    /**
     *
     * @return tokenConekta
     */
    public String getTokenConekta() {
		return tokenConekta;
	}

    /**
     *
     * @param tokenConekta
     * tokenConekta
     */
    public void setTokenConekta(String tokenConekta) {
		this.tokenConekta = tokenConekta;
	}

    /**
     *
     * @return accesoSms
     */
    public Boolean getAccesoSms() {
		return accesoSms;
	}

    /**
     *
     * @param accesoSms
     * accesoSms
     */
    public void setAccesoSms(Boolean accesoSms) {
		this.accesoSms = accesoSms;
	}
    
	
}
