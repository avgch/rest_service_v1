package com.enermex.modelo;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enermex.enumerable.PedidoFacturaEstatus;

/**
 * PedidoFacturaEntity
 */
@Entity
@Table(name = "t_pedido_factura")
public class PedidoFacturaEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_factura")
  private BigInteger idFactura;

  @Column(name = "id_pedido")
  private BigInteger idPedido;

  @Column(name = "id_fiscal")
  private BigInteger idFiscal;

  @Column(name = "estatus")
  private PedidoFacturaEstatus estatus;

  @Column(name = "uso_cfdi")
  private String usoCfdi;

  @Column(name = "archivo")
  private String archivo;

  @Column(name = "fecha_creacion")
  private Date fechaCreacion;

    /**
     *
     * @return
     */
    public BigInteger getIdFactura() {
    return idFactura;
  }

    /**
     *
     * @param idFactura
     */
    public void setIdFactura(BigInteger idFactura) {
    this.idFactura = idFactura;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
    return idPedido;
  }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
    this.idPedido = idPedido;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdFiscal() {
    return idFiscal;
  }

    /**
     *
     * @param idFiscal
     */
    public void setIdFiscal(BigInteger idFiscal) {
    this.idFiscal = idFiscal;
  }

    /**
     *
     * @return
     */
    public PedidoFacturaEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(PedidoFacturaEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public String getUsoCfdi() {
    return usoCfdi;
  }

    /**
     *
     * @param usoCfdi
     */
    public void setUsoCfdi(String usoCfdi) {
    this.usoCfdi = usoCfdi;
  }

    /**
     *
     * @return
     */
    public String getArchivo() {
    return archivo;
  }

    /**
     *
     * @param archivo
     */
    public void setArchivo(String archivo) {
    this.archivo = archivo;
  }

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }
}
