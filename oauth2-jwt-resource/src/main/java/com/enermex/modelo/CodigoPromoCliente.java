package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_codigo_promo_cliente")
@Entity
public class CodigoPromoCliente implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_codigo_promo_cliente")
	private BigInteger idCodigoPromoCliente;
	
	@Column(name = "id_cliente")
	private BigInteger idCliente;
	
	@Column(name = "id_promocion")
	private Long idPromocion;

    /**
     *
     */
    public CodigoPromoCliente() {
		super();
	}

    /**
     *
     * @return
     */
    public BigInteger getIdCodigoPromoCliente() {
		return idCodigoPromoCliente;
	}

    /**
     *
     * @param idCodigoPromoCliente
     */
    public void setIdCodigoPromoCliente(BigInteger idCodigoPromoCliente) {
		this.idCodigoPromoCliente = idCodigoPromoCliente;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}

   
	
	
	
	
}
