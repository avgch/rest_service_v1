package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_appstore")
@Entity
public class AppStore implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_app")
	private Integer idApp;
	
	
	@Column(name = "app_tecnologia")
	private String appTecnologia;
	
	@Column(name = "valor")
	private String valor;

    /**
     *
     * @return
     */
    public Integer getIdApp() {
		return idApp;
	}

    /**
     *
     * @param idApp
     */
    public void setIdApp(Integer idApp) {
		this.idApp = idApp;
	}

    /**
     *
     * @return
     */
    public String getAppTecnologia() {
		return appTecnologia;
	}

    /**
     *
     * @param appTecnologia
     */
    public void setAppTecnologia(String appTecnologia) {
		this.appTecnologia = appTecnologia;
	}

    /**
     *
     * @return
     */
    public String getValor() {
		return valor;
	}

    /**
     *
     * @param valor
     */
    public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
}