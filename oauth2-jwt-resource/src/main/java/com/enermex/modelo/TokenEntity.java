package com.enermex.modelo;

import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TokenEntity
 */
@Entity
@Table(name = "t_token")
public class TokenEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id_token")
  BigInteger idToken;

  @Column(name="id_usuario")
  Long idUsuario;

  @Column(name="id_cliente")
  BigInteger idCliente;

  @Column(name="validez")
  Calendar validez;

  @Column(name="aplicacion")
  String aplicacion;

  @Column(name="vigente")
  Boolean vigente;

    /**
     *
     * @return
     */
    public BigInteger getIdToken() {
    return idToken;
  }

    /**
     *
     * @param idToken
     */
    public void setIdToken(BigInteger idToken) {
    this.idToken = idToken;
  }

    /**
     *
     * @return
     */
    public Long getIdUsuario() {
    return idUsuario;
  }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public Calendar getValidez() {
    return validez;
  }

    /**
     *
     * @param validez
     */
    public void setValidez(Calendar validez) {
    this.validez = validez;
  }

    /**
     *
     * @return
     */
    public String getAplicacion() {
    return aplicacion;
  }

    /**
     *
     * @param aplicacion
     */
    public void setAplicacion(String aplicacion) {
    this.aplicacion = aplicacion;
  }

    /**
     *
     * @return
     */
    public Boolean getVigente() {
    return vigente;
  }

    /**
     *
     * @param vigente
     */
    public void setVigente(Boolean vigente) {
    this.vigente = vigente;
  }
}
