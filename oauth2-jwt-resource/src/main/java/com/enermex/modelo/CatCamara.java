package com.enermex.modelo;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cat_camara")
public class CatCamara {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_camara")
	private Integer idCamara;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "num_serie")
	private String numSerie;
	
	@Column(name = "modelo")
	private String modelo;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name = "estatus")
	private String estatus;
	
	@Column(name = "id_pipa")
	private Integer idPipa;
	
	@Transient
	private  Integer pipaId;
	
	@Column(name = "motivo_baja")
	private String motivoBaja;
	
	@Column(name = "pipa_asignada")
	private String pipaAsignada;
	
	
	
	@Transient
	private boolean selected;
	
    /**
     *
     */
    public CatCamara() {
		
		
	}

    /**
     *
     * @return idCamara
     */
    public Integer getIdCamara() {
		return idCamara;
	}

    /**
     *
     * @param idCamara
     * idCamara
     */
    public void setIdCamara(Integer idCamara) {
		this.idCamara = idCamara;
	}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     * 
     * @return numSerie
     */
    public String getNumSerie() {
		return numSerie;
	}

    /**
     *
     * @param numSerie
     * numSerie
     */
    public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

    /**
     *
     * @return modelo
     */
    public String getModelo() {
		return modelo;
	}

    /**
     *
     * @param modelo
     */
    public void setModelo(String modelo) {
		this.modelo = modelo;
	}

    /**
     *
     * @return fechaCreacion
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

    /**
     *
     * @return idPipa
     */
    public Integer getIdPipa() {
		return idPipa;
	}

    /**
     *
     * @param idPipa
     * idPipa
     */
    public void setIdPipa(Integer idPipa) {
		this.idPipa = idPipa;
	}

    /**
     *
     * @return pipaId
     */
    public Integer getPipaId() {
		return pipaId;
	}

    /**
     *
     * @param pipaId
     * pipaId
     */
    public void setPipaId(Integer pipaId) {
		this.pipaId = pipaId;
	}

    /**
     *
     * @return motivoBaja
     */
    public String getMotivoBaja() {
		return motivoBaja;
	}

    /**
     *
     * @param motivoBaja
     * motivoBaja
     */
    public void setMotivoBaja(String motivoBaja) {
		this.motivoBaja = motivoBaja;
	}

    /**
     *
     * @return selected
     */
    public boolean isSelected() {
		return selected;
	}

    /**
     *
     * @param selected
     * selected
     */
    public void setSelected(boolean selected) {
		this.selected = selected;
	}

    /**
     *
     * @return pipaAsignada
     */
    public String getPipaAsignada() {
		return pipaAsignada;
	}

    /**
     *
     * @param pipaAsignada
     * pipaAsignada
     */
    public void setPipaAsignada(String pipaAsignada) {
		this.pipaAsignada = pipaAsignada;
	}
	
	
	
	
}
