package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_cat_producto_servicio")
@Entity
public class ProductoServicio implements Serializable {

	/**
	*
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_producto_servicio")
	private Long id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "borrado")
	private Boolean borrado;
	
    /**
     *
     * @return
     */
    public Long getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(Long id) {
		this.id = id;
	}

    /**
     *
     * @return
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return
     */
    public String getTipo() {
		return tipo;
	}

    /**
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
		this.tipo = tipo;
	}

    /**
     *
     * @return
     */
    public Boolean getBorrado() {
		return borrado;
	}

    /**
     *
     * @param borrado
     */
    public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	
	

}
