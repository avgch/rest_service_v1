package com.enermex.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abraham
 */
@Table(name = "t_cat_motivo_cancelacion")
@Entity
public class CatMotivoCancelacion {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_motivo_cancelacion")
	private Integer idMotivoCancelacion;
	
	@Column(name = "motivo")
	private String motivo;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_on")
	private Date fechaCreacion;
	

	@Column(name = "create_by")
	private String creadoPor;

    /**
     *
     * @return
     */
    public Integer getIdMotivoCancelacion() {
		return idMotivoCancelacion;
	}

    /**
     *
     * @param idMotivoCancelacion
     */
    public void setIdMotivoCancelacion(Integer idMotivoCancelacion) {
		this.idMotivoCancelacion = idMotivoCancelacion;
	}

    /**
     *
     * @return
     */
    public String getMotivo() {
		return motivo;
	}

    /**
     *
     * @param motivo
     */
    public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

    /**
     *
     * @return
     */
    public String getTipo() {
		return tipo;
	}

    /**
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
		this.tipo = tipo;
	}

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public String getCreadoPor() {
		return creadoPor;
	}

    /**
     *
     * @param creadoPor
     */
    public void setCreadoPor(String creadoPor) {
		this.creadoPor = creadoPor;
	}
	
	






}
