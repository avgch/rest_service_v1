package com.enermex.modelo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="t_automovil_cliente")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutomovilCliente  implements Serializable{



/**
*
*/
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id_automovil")
private Integer idAutomovil;

@Column(name = "color")
private String color;

@Column(name = "placas")
private String  placas;

@Column(name = "capacidad")
private String  capacidad;


@Column(name = "id_estatus")
private int  estatus;

@Column(name = "anio")
private String anio;

@Column(name = "tamanio_tanque")
private String tamanioTanque;

@Column(name = "id_combustible")
private int idCombustible;

@Column(name = "id_marca")
private int  idMarca=1;

@Column(name = "id_modelo")
private int  idModelo=1;

@Column(name = "marca")
private String  marca;

@Column(name = "modelo")
private String  modelo;


@Column(name = "version")
private String  version;


@Column(name = "serie")
private String  serie;




@Temporal(value = TemporalType.TIMESTAMP)
@Column(name = "fecha_creacion")
private Calendar fechaCreacion;


@JsonBackReference
@ManyToOne
@JoinColumn(name = "id_cliente",referencedColumnName = "id_cliente")
private Cliente cliente;

    /**
     *
     */
    public AutomovilCliente(){}

    /**
     *
     * @param cli
     */
    public AutomovilCliente(AutomovilCliente cli) {
super();

this.color = cli.getColor();
this.placas = cli.getPlacas();
this.capacidad = cli.getCapacidad();
this.estatus = 1;
this.anio = cli.getAnio();
this.tamanioTanque = cli.getTamanioTanque();
this.idCombustible = cli.getIdCombustible();
this.idMarca = cli.getIdMarca();
this.idModelo = cli.getIdModelo();
this.marca = cli.getMarca();
this.modelo = cli.getModelo();
this.version = cli.getVersion();
this.serie   = cli.getSerie();
this.fechaCreacion = Calendar.getInstance();


}

    /**
     *
     * @return idAutomovil
     */
    public Integer getIdAutomovil() {
return idAutomovil;
}

    /**
     *
     * @param idAutomovil
     * idAutomovil
     */
    public void setIdAutomovil(Integer idAutomovil) {
this.idAutomovil = idAutomovil;
}

    /**
     *
     * @return color
     */
    public String getColor() {
return color;
}

    /**
     *
     * @param color
     * color
     */
    public void setColor(String color) {
this.color = color;
}

    /**
     *
     * @return placas
     */
    public String getPlacas() {
return placas;
}

    /**
     *
     * @param placas
     * placas
     */
    public void setPlacas(String placas) {
this.placas = placas;
}

    /**
     *
     * @return capacidad
     */
    public String getCapacidad() {
return capacidad;
}

    /**
     *
     * @param capacidad
     * capacidad
     */
    public void setCapacidad(String capacidad) {
this.capacidad = capacidad;
}

    /**
     *
     * @return cliente
     */
    public Cliente getCliente() {
return cliente;
}

    /**
     *
     * @param cliente
     * cliente
     */
    public void setCliente(Cliente cliente) {
this.cliente = cliente;
}

    /**
     *
     * @return estatus
     */
    public int getEstatus() {
return estatus;
}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(int estatus) {
this.estatus = estatus;
}

    /**
     *
     * @return anio
     */
    public String getAnio() {
return anio;
}

    /**
     *
     * @param anio
     * anio
     */
    public void setAnio(String anio) {
this.anio = anio;
}

    /**
     *
     * @return tamanioTanque
     */
    public String getTamanioTanque() {
return tamanioTanque;
}

    /**
     *
     * @param tamanioTanque
     * tamanioTanque
     */
    public void setTamanioTanque(String tamanioTanque) {
this.tamanioTanque = tamanioTanque;
}

    /**
     *
     * @return idCombustible
     */
    public int getIdCombustible() {
return idCombustible;
}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(int idCombustible) {
this.idCombustible = idCombustible;
}

    /**
     *
     * @return idMarca
     */
    @JsonIgnore
public int getIdMarca() {
return idMarca;
}

    /**
     *
     * @param idMarca
     * idMarca
     */
    public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
		}

    /**
     *
     * @return idModelo
     */
    @JsonIgnore
	public int getIdModelo() {
	return idModelo;
	}

    /**
     *
     * @param idModelo
     * idModelo
     */
    public void setIdModelo(int idModelo) {
	this.idModelo = idModelo;
	}

    /**
     *
     * @return marca
     */
    public String getMarca() {
	return marca;
	}

    /**
     *
     * @param marca
     * marca
     */
    public void setMarca(String marca) {
	this.marca = marca;
	}

    /**
     *
     * @return modelo
     */
    public String getModelo() {
	return modelo;
	}

    /**
     *
     * @param modelo
     * modelo
     */
    public void setModelo(String modelo) {
	this.modelo = modelo;
	}

    /**
     *
     * @return version
     */
    public String getVersion() {
	return version;
}

    /**
     *
     * @param version
     * version
     */
    public void setVersion(String version) {
	this.version = version;
}

    /**
     *
     * @return serie
     */
    public String getSerie() {
	return serie;
    }

    /**
     *
     * @param serie
     * serie
     */
    public void setSerie(String serie) {
	this.serie = serie;
     }

    /**
     *
     * @return fechaCreacion
     */
    @JsonIgnore
	public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
	this.fechaCreacion = fechaCreacion;
}










}
