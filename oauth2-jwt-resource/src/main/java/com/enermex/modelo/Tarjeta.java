package com.enermex.modelo;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cat_tarjeta_cliente")
public class Tarjeta {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_tarjeta_cliente")
	private Integer idTarjeta;
    
    @Column(name = "numero_tarjeta")
	private String  numeroTarjeta;
    
    @Column(name = "id_token_tarjeta")
	private String  tokenTarjeta;
    
    @Column(name = "id_cliente")
	private BigInteger  idCliente;
    
    @Column(name = "estatus")
    private boolean  estatus;
    
    @Column(name = "borrado")
    private boolean  borrado;
    
    @Column(name = "tipo_tarjeta")
    private String  tipoTarjeta;
    
    @Transient
    private List<Integer> planes;
    
    /**
     *
     */
    public Tarjeta() {}

    /**
     *
     * @return
     */
    public Integer getIdTarjeta() {
		return idTarjeta;
	}

    /**
     *
     * @param idTarjeta
     */
    public void setIdTarjeta(Integer idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

    /**
     *
     * @return
     */
    public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

    /**
     *
     * @param numeroTarjeta
     */
    public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

    /**
     *
     * @return
     */
    public String getTokenTarjeta() {
		return tokenTarjeta;
	}

    /**
     *
     * @param tokenTarjeta
     */
    public void setTokenTarjeta(String tokenTarjeta) {
		this.tokenTarjeta = tokenTarjeta;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public boolean isEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     */
    public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

    /**
     *
     * @return
     */
    public boolean isBorrado() {
		return borrado;
	}

    /**
     *
     * @param borrado
     */
    public void setBorrado(boolean borrado) {
		this.borrado = borrado;
	}

    /**
     *
     * @return
     */
    public String getTipoTarjeta() {
		return tipoTarjeta;
	}

    /**
     *
     * @param tipoTarjeta
     */
    public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

    /**
     *
     * @return
     */
    public List<Integer> getPlanes() {
		return planes;
	}

    /**
     *
     * @param planes
     */
    public void setPlanes(List<Integer> planes) {
		this.planes = planes;
	}
	
	
	
	
    
    
    
	

}
