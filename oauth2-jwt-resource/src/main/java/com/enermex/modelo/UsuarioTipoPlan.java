
/*##############################################################################
# Nombre del Programa : UsuarioTipoPlan.java                                   #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/




package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.enermex.modelo.UsuarioTipoPlanKey;
	
/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cliente_cat_tipo_plan")
public class UsuarioTipoPlan implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private UsuarioTipoPlanKey id;

    /**
     *
     * @return
     */
    public UsuarioTipoPlanKey getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(UsuarioTipoPlanKey id) {
		this.id = id;
	}
	
	
	
	

}
