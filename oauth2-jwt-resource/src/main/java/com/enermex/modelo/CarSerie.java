package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "car_serie")
public class CarSerie {
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_car_serie")
	private Integer idCarSerie;
	
	@Column(name = "name")
	private String nombre;
	
	@Column(name = "id_car_model")
	private Integer idModelo;
	
	@Column(name = "id_car_generation")
	private Integer idCarGeneration;

    /**
     *
     * @return
     */
    public Integer getIdCarSerie() {
		return idCarSerie;
	}

    /**
     *
     * @param idCarSerie
     */
    public void setIdCarSerie(Integer idCarSerie) {
		this.idCarSerie = idCarSerie;
	}

    /**
     *
     * @return
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return
     */
    public Integer getIdModelo() {
		return idModelo;
	}

    /**
     *
     * @param idModelo
     */
    public void setIdModelo(Integer idModelo) {
		this.idModelo = idModelo;
	}

    /**
     *
     * @return
     */
    public Integer getIdCarGeneration() {
		return idCarGeneration;
	}

    /**
     *
     * @param idCarGeneration
     */
    public void setIdCarGeneration(Integer idCarGeneration) {
		this.idCarGeneration = idCarGeneration;
	}
	
	
	

}
