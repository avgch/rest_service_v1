package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abraham
 */
@Table(name = "t_califica_pedido")
@Entity
public class CalificaPedido implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_califica_pedido")
	private Integer idCalificaPedido;
	
	
	@Column(name = "id_pedido")
	private BigInteger idPedido;
	
	
	@Column(name = "calificacion")
	private Integer calificacion;
	
	@Column(name = "califica")
	private String califica;
	

	@Column(name = "comentario")
	private String comentario;
	
	
	@Column(name = "create_on")
	private Calendar fechaCreacion;
	
    /**
     *
     * @param idPedido
     * @param calificacion
     * @param califica
     * @param comentario
     */
    public  CalificaPedido(BigInteger idPedido,Integer calificacion,String califica,String comentario) {
		
		this.idPedido = idPedido;
		this.calificacion = calificacion;
		this.califica = califica;
		this.comentario = comentario;
		this.fechaCreacion = Calendar.getInstance();
		
	}
	
    /**
     *
     */
    public CalificaPedido() {}
	
    /**
     *
     * @return
     */
    public Integer getIdCalificaPedido() {
		return idCalificaPedido;
	}

    /**
     *
     * @param idCalificaPedido
     */
    public void setIdCalificaPedido(Integer idCalificaPedido) {
		this.idCalificaPedido = idCalificaPedido;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public String getComentario() {
		return comentario;
	}

    /**
     *
     * @param comentario
     */
    public void setComentario(String comentario) {
		this.comentario = comentario;
	}

    /**
     *
     * @return
     */
    public Integer getCalificacion() {
		return calificacion;
	}

    /**
     *
     * @param calificacion
     */
    public void setCalificacion(Integer calificacion) {
		this.calificacion = calificacion;
	}

    /**
     *
     * @return
     */
    public String getCalifica() {
		return califica;
	}

    /**
     *
     * @param califica
     */
    public void setCalifica(String califica) {
		this.califica = califica;
	}
	
	
	
	
	

}
