package com.enermex.modelo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cat_tipo_plan")
public class TipoPlan {

@Id
@Column(name = "id_plan")
private Integer  id;

@Column(name = "plan")
private String   plan;

@Column(name = "id_estatus")
private Integer  estatus;

@OneToMany(mappedBy="tipoPlan", cascade = CascadeType.ALL)
@JsonBackReference
private Set<UsuarioTipoPlanA> planes = new HashSet();

    /**
     *
     */
    public TipoPlan() {}

    /**
     *
     * @return id
     */
    public Integer getId() {
return id;
}

    /**
     *
     * @param id
     * id
     */
    public void setId(Integer id) {
this.id = id;
}

    /**
     *
     * @return plan
     */
    public String getPlan() {
return plan;
}

    /**
     *
     * @param plan
     * plan
     */
    public void setPlan(String plan) {
this.plan = plan;
}

    /**
     *
     * @return estatus
     */
    public Integer getEstatus() {
return estatus;
}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(Integer estatus) {
this.estatus = estatus;
}

    /**
     *
     * @return planes
     */
    public Set<UsuarioTipoPlanA> getPlanes() {
return planes;
}

    /**
     *
     * @param planes
     * planes
     */
    public void setPlanes(Set<UsuarioTipoPlanA> planes) {
this.planes = planes;
}


}
