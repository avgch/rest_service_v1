package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_promocion_plan")
@Entity
public class PlanPromocion implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_promocion_plan")
	private Long idPromocionPlan;
	
    @ManyToOne
    @JoinColumn(name = "id_plan")
    private TipoPlan tipoPlan;

	@ManyToOne
	@JoinColumn(name = "id_promocion")
	private Promocion promocion;

    /**
     *
     * @return
     */
    public Long getIdPromocionPlan() {
		return idPromocionPlan;
	}

    /**
     *
     * @param idPromocionPlan
     */
    public void setIdPromocionPlan(Long idPromocionPlan) {
		this.idPromocionPlan = idPromocionPlan;
	}

    /**
     *
     * @return
     */
    public TipoPlan getTipoPlan() {
		return tipoPlan;
	}

    /**
     *
     * @param tipoPlan
     */
    public void setTipoPlan(TipoPlan tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

    /**
     *
     * @return
     */
    public Promocion getPromocion() {
		return promocion;
	}

    /**
     *
     * @param promocion
     */
    public void setPromocion(Promocion promocion) {
		this.promocion = promocion;
	}
	
	
	
	
	
	
	
	
	
}
