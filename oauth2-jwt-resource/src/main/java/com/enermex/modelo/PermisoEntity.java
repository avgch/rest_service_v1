package com.enermex.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * Entidad JPA de tabla t_usuario_perm
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 22 de enero de 2020
 */
@Entity
@Table(name = "t_usuario_perm")
public class PermisoEntity {
  @EmbeddedId
  private PermisoId id;

  @Column(name = "asignado")
  private Boolean asignado;

    /**
     *
     * @return
     */
    public PermisoId getId() {
    return id;
  }

    /**
     *
     * @param id
     */
    public void setId(PermisoId id) {
    this.id = id;
  }

    /**
     *
     * @return
     */
    public Boolean getAsignado() {
    return asignado;
  }

    /**
     *
     * @param asignado
     */
    public void setAsignado(Boolean asignado) {
    this.asignado = asignado;
  }
}
