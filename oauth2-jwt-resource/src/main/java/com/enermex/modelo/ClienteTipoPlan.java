package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.enermex.modelo.ClienteTipoPlanKey;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cliente_cat_tipo_plan")
public class ClienteTipoPlan implements Serializable {
		
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		@EmbeddedId
		ClienteTipoPlanKey clienteTipoPlanKey;
		
    /**
     *
     * @return
     */
    public ClienteTipoPlanKey getClienteTipoPlanKey() {
			return clienteTipoPlanKey;
		}

    /**
     *
     * @param clienteTipoPlanKey
     */
    public void setClienteTipoPlanKey(ClienteTipoPlanKey clienteTipoPlanKey) {
			this.clienteTipoPlanKey = clienteTipoPlanKey;
		}
		
		
		
		

}
