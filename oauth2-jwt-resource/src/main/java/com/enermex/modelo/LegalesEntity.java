package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_legales")
public class LegalesEntity {
  @Id
  @Column(name = "id_legales")
  private Integer idLegales;
  
  @Column(name = "privacidad")
  private String privacidad;
  
  @Column(name = "terminos")
  private String terminos;

  @Column(name = "privacidad_text")
  private String privacidadText;
  
  @Column(name = "terminos_text")
  private String terminosText;

    /**
     *
     * @return
     */
    public Integer getIdLegales() {
    return idLegales;
  }

    /**
     *
     * @param idLegales
     */
    public void setIdLegales(Integer idLegales) {
    this.idLegales = idLegales;
  }

    /**
     *
     * @return
     */
    public String getPrivacidad() {
    return privacidad;
  }

    /**
     *
     * @param privacidad
     */
    public void setPrivacidad(String privacidad) {
    this.privacidad = privacidad;
  }

    /**
     *
     * @return
     */
    public String getTerminos() {
    return terminos;
  }

    /**
     *
     * @param terminos
     */
    public void setTerminos(String terminos) {
    this.terminos = terminos;
  }

    /**
     *
     * @return
     */
    public String getPrivacidadText() {
    return privacidadText;
  }

    /**
     *
     * @param privacidadText
     */
    public void setPrivacidadText(String privacidadText) {
    this.privacidadText = privacidadText;
  }

    /**
     *
     * @return
     */
    public String getTerminosText() {
    return terminosText;
  }

    /**
     *
     * @param terminosText
     */
    public void setTerminosText(String terminosText) {
    this.terminosText = terminosText;
  }
}
