package com.enermex.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.enermex.dto.RolDto;
import com.enermex.dto.TipoPlanDto;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario")
	private Integer idUsuario;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellido_paterno")
	private String apellidoPaterno;
	
	@Column(name="apellido_materno")
	private String apellidoMaterno;
	
	@Column(name="correo")
	private String correo;
	
	@Column(name="telefono")
	private String telefono;
	
	@Column(name="contrasena")
	private String contrasena;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name = "id_tipo_usuario")
	private Long idTipoUsuario;
	
	@Column(name = "estatus")
	private Integer estatus; 
	
	@Transient
	private Integer idPlan;
	
	@Column(name = "genero")
	private String genero;
	
	@Column(name = "isid")
	private Integer isId;
	
	@Column(name = "estado_civil")
	private String estadoCivil;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="fecha_nacimiento")
	private Date fechaNacimiento;
	
	@JoinTable(
	        name = "usuario_tipo_plan",
	        joinColumns = @JoinColumn(name = "id_usuario", nullable = false),
	        inverseJoinColumns = @JoinColumn(name="id_tipo_plan", nullable = false)
	    )
	@ManyToMany(cascade = CascadeType.ALL)
    private List<TipoPlan> planes;
	
	 
//	@OneToMany(mappedBy="usuario")
//    private List<DireccionCliente> direccion;
	
	@JoinTable(
	        name = "usuarios_rol",
	        joinColumns = @JoinColumn(name = "id_usuario", nullable = false),
	        inverseJoinColumns = @JoinColumn(name="id_rol", nullable = false)
	    )
	@ManyToMany(cascade = CascadeType.ALL)
	 private List<RolDto> roles ;
	
    /**
     *
     */
    public Usuario() {

	}
	
    /**
     *
     * @return idUsuario
     */
    public Integer getIdUsuario() {
		return idUsuario;
	}

    /**
     *
     * @param idUsuario
     * idUsuario
     */
    public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return correo
     */
    public String getCorreo() {
		return correo;
	}

    /**
     *
     * @param correo
     * correo
     */
    public void setCorreo(String correo) {
		this.correo = correo;
	}

    /**
     *
     * @return telefono
     */
    public String getTelefono() {
		return telefono;
	}

    /**
     *
     * @param telefono
     * telefono
     */
    public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

    /**
     *
     * @return contrasena
     */
    public String getContrasena() {
		return contrasena;
	}

    /**
     *
     * @param contrasena
     * contrasena
     */
    public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

    /**
     *
     * @return fechaCreacion
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return idTipoUsuario
     */
    public Long getIdTipoUsuario() {
		return idTipoUsuario;
	}

    /**
     *
     * @param idTipoUsuario
     * idTipoUsuario
     */
    public void setIdTipoUsuario(Long idTipoUsuario) {
		this.idTipoUsuario = idTipoUsuario;
	}

    /**
     *
     * @return roles
     */
    public List<RolDto> getRoles() {
		return roles;
	}

    /**
     *
     * @param roles
     * roles
     */
    public void setRoles(List<RolDto> roles) {
		this.roles = roles;
	}



//	public List<DireccionCliente> getDireccion() {
//		return direccion;
//	}
//
//
//
//	public void setDireccion(List<DireccionCliente> direccion) {
//		this.direccion = direccion;
//	}

    /**
     *
     * @return estatus
     */
	public Integer isEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

    /**
     *
     * @return planes
     */
    public List<TipoPlan> getPlanes() {
		return planes;
	}

    /**
     *
     * @param planes
     * planes
     */
    public void setPlanes(List<TipoPlan> planes) {
		this.planes = planes;
	}

    /**
     *
     * @return idPlan
     */
    public Integer getIdPlan() {
		return idPlan;
	}

    /**
     *
     * @param idPlan
     * idPlan
     */
    public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}

    /**
     *
     * @return genero
     */
    public String getGenero() {
		return genero;
	}

    /**
     *
     * @param genero
     * genero
     */
    public void setGenero(String genero) {
		this.genero = genero;
	}

    /**
     *
     * @return estadoCivil
     */
    public String getEstadoCivil() {
		return estadoCivil;
	}

    /**
     *
     * @param estadoCivil
     * estadoCivil
     */
    public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

    /**
     *
     * @return estatus
     */
    public Integer getEstatus() {
		return estatus;
	}

    /**
     *
     * @return fechaNacimiento
     */
    public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

    /**
     *
     * @param fechaNacimiento
     * fechaNacimiento
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

    /**
     *
     * @return apellidoPaterno
     */
    public String getApellidoPaterno() {
		return apellidoPaterno;
	}

    /**
     *
     * @param apellidoPaterno
     * apellidoPaterno
     */
    public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

    /**
     *
     * @return apellidoMaterno
     */
    public String getApellidoMaterno() {
		return apellidoMaterno;
	}

    /**
     *
     * @param apellidoMaterno
     * apellidoMaterno
     */
    public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

    /**
     *
     * @return isId
     */
    public Integer getIsId() {
		return isId;
	}

    /**
     *
     * @param isId
     * isId
     */
    public void setIsId(Integer isId) {
		this.isId = isId;
	}
  
	
	

    
	
}
