/*##############################################################################
# Nombre del Programa : DireccionDto.java                                      #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/

package com.enermex.modelo;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.stereotype.Component;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_direccion_cliente")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DireccionCliente  implements Serializable{



/**
*
*/
private static final long serialVersionUID = 1L;


@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name = "id_direccion")
private Integer   idDireccion;


@Column(name = "cp")
private String cp;

@Column(name = "calle")
private String calle;

@Column(name = "colonia")
private String colonia;

@Temporal(value = TemporalType.TIMESTAMP)
@Column(name = "fecha_creacion",columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
private Date   fechaCreacion;

@Column(name = "latitud")
private Double latitud;

@Column(name = "longitud")
private Double longitud;

@Column(name = "id_estatus")
private int estatus;


@JsonBackReference
@ManyToOne
@JoinColumn(name = "id_cliente")
private Cliente cliente;

    /**
     *
     */
    public DireccionCliente() {


}
/**
*
* @param o
*/
    public DireccionCliente (DireccionCliente o) {

        this.setCalle(o.getCalle());
    this.setColonia(o.getColonia());
    this.setCp(o.getCp());
    this.setFechaCreacion(new Date());
    this.setLatitud(o.getLatitud());
    this.setLongitud(o.getLongitud());
    this.setEstatus(1);
   
}

    /**
     *
     * @return
     */
    public Integer getIdDireccion() {
return idDireccion;
}

    /**
     *
     * @param idDireccion
     */
    public void setIdDireccion(Integer idDireccion) {
this.idDireccion = idDireccion;
}

    /**
     *
     * @return
     */
    public String getCp() {
return cp;
}

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
this.cp = cp;
}

    /**
     *
     * @return
     */
    public String getCalle() {
return calle;
}

    /**
     *
     * @param calle
     */
    public void setCalle(String calle) {
this.calle = calle;
}

    /**
     *
     * @return
     */
    public String getColonia() {
return colonia;
}

    /**
     *
     * @param colonia
     */
    public void setColonia(String colonia) {
this.colonia = colonia;
}

    /**
     *
     * @return
     */
    @JsonIgnore
public Date getFechaCreacion() {
return fechaCreacion;
}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
this.fechaCreacion = fechaCreacion;
}

    /**
     *
     * @return
     */
    public Double getLatitud() {
return latitud;
}

    /**
     *
     * @param latitud
     */
    public void setLatitud(Double latitud) {
this.latitud = latitud;
}

    /**
     *
     * @return
     */
    public Double getLongitud() {
return longitud;
}

    /**
     *
     * @param longitud
     */
    public void setLongitud(Double longitud) {
this.longitud = longitud;
}

    /**
     *
     * @return
     */
    public Cliente getCliente() {
return cliente;
}

    /**
     *
     * @param cliente
     */
    public void setCliente(Cliente cliente) {
this.cliente = cliente;
}

    /**
     *
     * @return
     */
    public int getEstatus() {
return estatus;
}

    /**
     *
     * @param estatus
     */
    public void setEstatus(int estatus) {
this.estatus = estatus;
}




   

   

}