package com.enermex.modelo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_cat_tipo_combustible")
@Entity
public class CatTipoCombustible {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_combustible")
	private Integer idCombustible;
	
	
	@Column(name = "combustible")
	private String  combustible;

    /**
     *
     * @return
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return
     */
    public String getCombustible() {
		return combustible;
	}

    /**
     *
     * @param combustible
     */
    public void setCombustible(String combustible) {
		this.combustible = combustible;
	}
	
	
	
	
}
