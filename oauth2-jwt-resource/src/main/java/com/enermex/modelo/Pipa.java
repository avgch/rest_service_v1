package com.enermex.modelo;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.enermex.dto.PipaDto;
import com.enermex.modelo.CatCamara;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cat_pipa")
public class Pipa {


@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name = "id_pipa")
private Integer idPipa;

@Column(name = "nombre")
private String nombre;

@Column(name = "num_serie")
private String numSerie;

@Column(name = "placas")
private String placas;

@Column(name = "estatus")
private String estatus;

@Column(name = "id_usuario")
private int usuario;

@Column(name = "fecha_creacion")
@Temporal(value = TemporalType.TIMESTAMP)
private Date fechaCreacion;

@Column(name = "modify_on")
@Temporal(value = TemporalType.TIMESTAMP)
private Calendar fechaModificacion;


@Column(name = "motivo_baja")
private String motivoBaja;


@Transient
private boolean selected;

@Column(name = "diesel")
private boolean diesel;

@Column(name = "regular")
private boolean regular;

@Column(name = "premium")
private boolean premium;

@Column(name = "tamano_tanque")
private String tamanoTotalTanque;

@Column(name = "total_premium")
private Double  totalPremium;

@Column(name = "total_regular")
private Double  totalRegular;

@Column(name = "total_diesel")
private Double  totalDiesel;

@Column(name = "litros_restantes_diesel")
private Double  litrosRestantesDiesel;

@Column(name = "litros_restantes_regular")
private Double  litrosRestantesRegular;

@Column(name = "litros_restantes_premium")
private Double  litrosRestantesPremium;

    /**
     *
     */
    public Pipa() {}

    /**
     *
     * @param pipa
     */
    public Pipa(PipaDto pipa) {
super();
this.nombre = pipa.getNombre();
this.numSerie = pipa.getNumSerie();
this.placas = pipa.getPlacas();
this.estatus = pipa.getEstatus();
this.usuario = pipa.getUsuario();
this.fechaCreacion = pipa.getFechaCreacion();
this.diesel = pipa.isDiesel();
this.regular = pipa.isRegular();
this.premium = pipa.isPremium();
this.tamanoTotalTanque = pipa.getTamanoTotalTanque();
this.totalPremium = pipa.getTotalPremium();
this.totalDiesel = pipa.getTotalDiesel();
this.totalRegular = pipa.getTotalRegular();
this.litrosRestantesDiesel = pipa.getLitrosRestantesDiesel();
this.litrosRestantesPremium = pipa.getLitrosRestantesPremium();
this.litrosRestantesRegular = pipa.getLitrosRestantesRegular();


}

    /**
     *
     * @return idPipa
     */
    public Integer getIdPipa() {
return idPipa;
}

    /**
     *
     * @param idPipa
     * idPipa
     */
    public void setIdPipa(Integer idPipa) {
this.idPipa = idPipa;
}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
return nombre;
}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
this.nombre = nombre;
}

    /**
     *
     * @return numSerie
     */
    public String getNumSerie() {
return numSerie;
}

    /**
     *
     * @param numSerie
     * numSerie
     */
    public void setNumSerie(String numSerie) {
this.numSerie = numSerie;
}

    /**
     *
     * @return placas
     */
    public String getPlacas() {
return placas;
}

    /**
     *
     * @param placas
     * placas
     */
    public void setPlacas(String placas) {
this.placas = placas;
}

    /**
     *
     * @return totalPremium
     */
    public Double getTotalPremium() {
return totalPremium;
}

    /**
     *
     * @param totalPremium
     * totalPremium
     */
    public void setTotalPremium(Double totalPremium) {
this.totalPremium = totalPremium;
}

    /**
     *
     * @return totalRegular
     */
    public Double getTotalRegular() {
return totalRegular;
}

    /**
     *
     * @param totalRegular
     * totalRegular
     */
    public void setTotalRegular(Double totalRegular) {
this.totalRegular = totalRegular;
}

    /**
     *
     * @return totalDiesel
     */
    public Double getTotalDiesel() {
return totalDiesel;
}

    /**
     *
     * @param totalDiesel
     * totalDiesel
     */
    public void setTotalDiesel(Double totalDiesel) {
this.totalDiesel = totalDiesel;
}

    /**
     *
     * @return fechaCreacion
     */
    public Date getFechaCreacion() {
return fechaCreacion;
}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
this.fechaCreacion = fechaCreacion;
}

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
return estatus;
}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(String estatus) {
this.estatus = estatus;
}

    /**
     *
     * @return usuario
     */
    public int getUsuario() {
return usuario;
}

    /**
     *
     * @param usuario
     * usuario
     */
    public void setUsuario(int usuario) {
this.usuario = usuario;
}

    /**
     *
     * @return motivoBaja
     */
    public String getMotivoBaja() {
return motivoBaja;
}

    /**
     *
     * @param motivoBaja
     * motivoBaja
     */
    public void setMotivoBaja(String motivoBaja) {
this.motivoBaja = motivoBaja;
}

    /**
     *
     * @return selected
     */
    public boolean isSelected() {
return selected;
}

    /**
     *
     * @param selected
     * selected
     */
    public void setSelected(boolean selected) {
this.selected = selected;
}

    /**
     *
     * @return diesel
     */
    public boolean isDiesel() {
return diesel;
}

    /**
     *
     * @param diesel
     * diesel
     */
    public void setDiesel(boolean diesel) {
this.diesel = diesel;
}

    /**
     *
     * @return regular
     */
    public boolean isRegular() {
return regular;
}

    /**
     *
     * @param regular
     * regular
     */
    public void setRegular(boolean regular) {
this.regular = regular;
}

    /**
     *
     * @return premium
     */
    public boolean isPremium() {
return premium;
}

    /**
     *
     * @param premium
     * premium
     */
    public void setPremium(boolean premium) {
this.premium = premium;
}

    /**
     *
     * @return tamanoTotalTanque
     */
    public String getTamanoTotalTanque() {
return tamanoTotalTanque;
}

    /**
     *
     * @param tamanoTotalTanque
     * tamanoTotalTanque
     */
    public void setTamanoTotalTanque(String tamanoTotalTanque) {
this.tamanoTotalTanque = tamanoTotalTanque;
}

    /**
     *
     * @return litrosRestantesDiesel
     */
    public Double getLitrosRestantesDiesel() {
	return litrosRestantesDiesel;
}

    /**
     *
     * @param litrosRestantesDiesel
     * litrosRestantesDiesel
     */
    public void setLitrosRestantesDiesel(Double litrosRestantesDiesel) {
	this.litrosRestantesDiesel = litrosRestantesDiesel;
}

    /**
     *
     * @return litrosRestantesRegular
     */
    public Double getLitrosRestantesRegular() {
	return litrosRestantesRegular;
}

    /**
     *
     * @param litrosRestantesRegular
     * litrosRestantesRegular
     */
    public void setLitrosRestantesRegular(Double litrosRestantesRegular) {
	this.litrosRestantesRegular = litrosRestantesRegular;
}

    /**
     *
     * @return litrosRestantesPremium
     */
    public Double getLitrosRestantesPremium() {
	return litrosRestantesPremium;
}

    /**
     *
     * @param litrosRestantesPremium
     * litrosRestantesPremium
     */
    public void setLitrosRestantesPremium(Double litrosRestantesPremium) {
	this.litrosRestantesPremium = litrosRestantesPremium;
}

    /**
     *
     * @return fechaModificacion
     */
    public Calendar getFechaModificacion() {
	return fechaModificacion;
}

    /**
     *
     * @param fechaModificacion
     * fechaModificacion
     */
    public void setFechaModificacion(Calendar fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
}


	


}