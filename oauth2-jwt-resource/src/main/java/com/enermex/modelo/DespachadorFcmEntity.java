package com.enermex.modelo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="t_despachador_fcm")
public class DespachadorFcmEntity {
  @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fcm")
	private BigInteger idFcm;
	
	@Column(name="id_despachador")
	private Long idDespachador;
	
	@Column(name="fcm")
  private String fcm;

    /**
     *
     * @return
     */
    public BigInteger getIdFcm() {
    return idFcm;
  }

    /**
     *
     * @param idFcm
     */
    public void setIdFcm(BigInteger idFcm) {
    this.idFcm = idFcm;
  }

    /**
     *
     * @return
     */
    public Long getIdDespachador() {
    return idDespachador;
  }

    /**
     *
     * @param idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
    this.idDespachador = idDespachador;
  }

    /**
     *
     * @return
     */
    public String getFcm() {
    return fcm;
  }

    /**
     *
     * @param fcm
     */
    public void setFcm(String fcm) {
    this.fcm = fcm;
  }
}
