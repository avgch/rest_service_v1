package com.enermex.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * RutaCodigoEntity
 */
@Entity
@Table(name = "t_cat_codigo")
public class RutaCodigoEntity {
  @Id
  @Column(name = "id_codigo")
  String idCodigo;

  @JsonIgnore
  @ManyToMany(mappedBy = "codigos")
  private List<RutaEntity> rutas;

  @Column(name = "id_estado")
  Integer idEstado;

  @Column(name = "id_municipio")
  Integer idMunicipio;

    /**
     *
     * @return
     */
    public String getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(String idCodigo) {
    this.idCodigo = idCodigo;
  }

    /**
     *
     * @return
     */
    public List<RutaEntity> getRutas() {
    return rutas;
  }

    /**
     *
     * @param rutas
     */
    public void setRutas(List<RutaEntity> rutas) {
    this.rutas = rutas;
  }

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }
}
