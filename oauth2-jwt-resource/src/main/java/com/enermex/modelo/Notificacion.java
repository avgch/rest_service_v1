package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.enermex.dto.NotificacionDto;

/**
 *
 * @author abraham
 */
@Table(name = "t_cat_notificacion")
@Entity
public class Notificacion implements Serializable,Comparable<Notificacion>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_notificacion")
	private BigInteger idNotificacion;
	
	
	@Column(name = "titulo")
	private String titulo;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Date  fechaCreacion;
	
	@Column(name = "enviado_a")
	private String enviadaA;
	
	@Column(name = "creado_por")
	private String creadoPor;
	
	@Column(name="mensaje")
	private String mensaje;
	
    /**
     *
     */
    public Notificacion() {
		super();
	}

    /**
     *
     * @param notifica
     */
    public Notificacion(NotificacionDto notifica) {
		super();
		
		this.titulo = notifica.getTitulo();
		this.mensaje = notifica.getMensaje();
		this.fechaCreacion = new Date();
	}

    /**
     *
     * @return
     */
    public BigInteger getIdNotificacion() {
		return idNotificacion;
	}

    /**
     *
     * @param idNotificacion
     */
    public void setIdNotificacion(BigInteger idNotificacion) {
		this.idNotificacion = idNotificacion;
	}

    /**
     *
     * @return
     */
    public String getTitulo() {
		return titulo;
	}

    /**
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public String getEnviadaA() {
		return enviadaA;
	}

    /**
     *
     * @param enviadaA
     */
    public void setEnviadaA(String enviadaA) {
		this.enviadaA = enviadaA;
	}

    /**
     *
     * @return
     */
    public String getCreadoPor() {
		return creadoPor;
	}

    /**
     *
     * @param creadoPor
     */
    public void setCreadoPor(String creadoPor) {
		this.creadoPor = creadoPor;
	}

    /**
     *
     * @return
     */
    public String getMensaje() {
		return mensaje;
	}

    /**
     *
     * @param mensaje
     */
    public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	 @Override
	  public int compareTo(Notificacion u) {
	    if (getFechaCreacion() == null || u.getFechaCreacion() == null) {
	      return 0;
	    }
	    return getFechaCreacion().compareTo(u.getFechaCreacion());
	  }
	

	
	
	
}
