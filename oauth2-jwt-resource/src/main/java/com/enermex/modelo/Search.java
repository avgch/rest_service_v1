package com.enermex.modelo;

/**
 *
 * @author abraham
 */
public class Search {
	
    /**
     *
     */
    public String value;

    /**
     *
     */
    public String product_segment;

    /**
     *
     */
    public String branchid ;

    /**
     *
     */
    public String username;

    /**
     *
     */
    public String vendorno ;

    /**
     *
     */
    public String fromdate;

    /**
     *
     */
    public String todate ;

    /**
     *
     */
    public boolean regex ;

    /**
     *
     * @return
     */
    public String getValue() {
		return value;
	}

    /**
     *
     * @param value
     */
    public void setValue(String value) {
		this.value = value;
	}

    /**
     *
     * @return
     */
    public String getProduct_segment() {
		return product_segment;
	}

    /**
     *
     * @param product_segment
     */
    public void setProduct_segment(String product_segment) {
		this.product_segment = product_segment;
	}

    /**
     *
     * @return
     */
    public String getBranchid() {
		return branchid;
	}

    /**
     *
     * @param branchid
     */
    public void setBranchid(String branchid) {
		this.branchid = branchid;
	}

    /**
     *
     * @return
     */
    public String getUsername() {
		return username;
	}

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
		this.username = username;
	}

    /**
     *
     * @return
     */
    public String getVendorno() {
		return vendorno;
	}

    /**
     *
     * @param vendorno
     */
    public void setVendorno(String vendorno) {
		this.vendorno = vendorno;
	}

    /**
     *
     * @return
     */
    public String getFromdate() {
		return fromdate;
	}

    /**
     *
     * @param fromdate
     */
    public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

    /**
     *
     * @return
     */
    public String getTodate() {
		return todate;
	}

    /**
     *
     * @param todate
     */
    public void setTodate(String todate) {
		this.todate = todate;
	}

    /**
     *
     * @return
     */
    public boolean isRegex() {
		return regex;
	}

    /**
     *
     * @param regex
     */
    public void setRegex(boolean regex) {
		this.regex = regex;
	}
    
    
    
    


}
