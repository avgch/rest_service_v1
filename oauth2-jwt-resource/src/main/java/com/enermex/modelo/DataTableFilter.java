package com.enermex.modelo;

import java.util.List;
import java.util.Map;


/**
 * Ing Abraham Vargas Garcia
 * @author abraham
 *
 */
public class DataTableFilter {
	
	
	private int draw;
	private int start;
	private int length;

    /**
     *
     */
    public Search search;
//	private Map<SearchCriterias, String> search;
	private List<Column> columns;
	private List<Order> order;
	//private List<Map<OrderCriterias, String>> order;

    /**
     *
     */
    public enum SearchCriterias {

            /**
             *
             */
            value,

            /**
             *
             */
            regex
	}

    /**
     *
     */
    public enum OrderCriterias {

            /**
             *
             */
            column,

            /**
             *
             */
            dir
	}

    /**
     *
     */
    public enum ColumnCriterias {

            /**
             *
             */
            data,

            /**
             *
             */
            name,

            /**
             *
             */
            searchable,

            /**
             *
             */
            orderable,

            /**
             *
             */
            searchValue,

            /**
             *
             */
            searchRegex
	}

    /**
     *
     * @return
     */
    public int getDraw() {
		return draw;
	}

    /**
     *
     * @param draw
     */
    public void setDraw(int draw) {
		this.draw = draw;
	}

    /**
     *
     * @return
     */
    public int getStart() {
		return start;
	}

    /**
     *
     * @param start
     */
    public void setStart(int start) {
		this.start = start;
	}

    /**
     *
     * @return
     */
    public int getLength() {
		return length;
	}

    /**
     *
     * @param length
     */
    public void setLength(int length) {
		this.length = length;
	}
//
//	public Map<SearchCriterias, String> getSearch() {
//		return search;
//	}
//
//	public void setSearch(Map<SearchCriterias, String> search) {
//		this.search = search;
//	}

	
	
//	public List<Map<ColumnCriterias, String>> getColumns() {
//		return columns;
//	}
//
//	public void setColumns(List<Map<ColumnCriterias, String>> columns) {
//		this.columns = columns;
//	}

    /**
     *
     * @return
     */

	public List<Column> getColumns() {
		return columns;
	}

    /**
     *
     * @param columns
     */
    public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

    /**
     *
     * @return
     */
    public List<Order> getOrder() {
		return order;
	}

    /**
     *
     * @param order
     */
    public void setOrder(List<Order> order) {
		this.order = order;
	}

    /**
     *
     * @return
     */
    public Search getSearch() {
		return search;
	}

    /**
     *
     * @param search
     */
    public void setSearch(Search search) {
		this.search = search;
	}
    
	

}
