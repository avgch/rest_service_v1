package com.enermex.modelo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enermex.enumerable.PedidoEvidenciaMomento;

/**
 * PedidoEvidenciaEntity
 */
@Entity
@Table(name = "t_pedido_evidencia")
public class PedidoEvidenciaEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_evidencia")
  private BigInteger idEvidencia;
  
  @Column(name = "id_pedido")
  private BigInteger idPedido;
  
  @Column(name = "momento")
  private PedidoEvidenciaMomento momento;
  
  @Column(name = "nombre")
  private String nombre;

    /**
     *
     * @return
     */
    public BigInteger getIdEvidencia() {
    return idEvidencia;
  }

    /**
     *
     * @param idEvidencia
     */
    public void setIdEvidencia(BigInteger idEvidencia) {
    this.idEvidencia = idEvidencia;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
    return idPedido;
  }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
    this.idPedido = idPedido;
  }

    /**
     *
     * @return
     */
    public PedidoEvidenciaMomento getMomento() {
    return momento;
  }

    /**
     *
     * @param momento
     */
    public void setMomento(PedidoEvidenciaMomento momento) {
    this.momento = momento;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}
