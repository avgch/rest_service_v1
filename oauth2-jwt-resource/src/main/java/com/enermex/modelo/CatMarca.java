package com.enermex.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.enermex.modelo.CatModelo;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "car_make")
public class CatMarca implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_car_make")
	private Integer idMarca;
	

	@Column(name = "name")
	private String marca;
	
	
//	@OneToMany(mappedBy="marca",cascade = {CascadeType.ALL})
//    private List<CatModelo> modelos;

    /**
     *
     * @return idMarca
     */
	
	public Integer getIdMarca() {
		return idMarca;
	}

    /**
     *
     * @param idMarca
     * idMarca
     */
    public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

    /**
     *
     * @return marca
     */
    public String getMarca() {
		return marca;
	}

    /**
     *
     * @param marca
     * marca
     */
    public void setMarca(String marca) {
		this.marca = marca;
	}
//
//
//	public List<CatModelo> getModelos() {
//		return modelos;
//	}
//
//
//	public void setModelos(List<CatModelo> modelos) {
//		this.modelos = modelos;
//	}


	
	
	
	

}
