package com.enermex.modelo;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enermex.enumerable.QuejaTipoDetalle;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_queja_detalle")
public class QuejaDetalleEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_queja_detalle")
  Integer idQuejaDetalle;

  @Column(name = "id_queja")
  Long idQueja;

  @Column(name = "fecha_creacion")
  Calendar fechaCreacion;
  
  @Column(name = "tipo")
  QuejaTipoDetalle tipo;
  
  @Column(name = "texto")
  String texto;

    /**
     *
     * @return
     */
    public Integer getIdQuejaDetalle() {
    return idQuejaDetalle;
  }

    /**
     *
     * @param idQuejaDetalle
     */
    public void setIdQuejaDetalle(Integer idQuejaDetalle) {
    this.idQuejaDetalle = idQuejaDetalle;
  }

    /**
     *
     * @return
     */
    public Long getIdQueja() {
    return idQueja;
  }

    /**
     *
     * @param idQueja
     */
    public void setIdQueja(Long idQueja) {
    this.idQueja = idQueja;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public QuejaTipoDetalle getTipo() {
    return tipo;
  }

    /**
     *
     * @param tipo
     */
    public void setTipo(QuejaTipoDetalle tipo) {
    this.tipo = tipo;
  }

    /**
     *
     * @return
     */
    public String getTexto() {
    return texto;
  }

    /**
     *
     * @param texto
     */
    public void setTexto(String texto) {
    this.texto = texto;
  }
}
