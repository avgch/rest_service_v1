package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cat_color")
public class CatColorEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_color")
  private Integer idColor;

  @Column(name = "nombre")
  private String nombre;

  @Column(name = "orden")
  private Integer orden;

    /**
     *
     * @return
     */
    public Integer getIdColor() {
    return idColor;
  }

    /**
     *
     * @param idColor
     */
    public void setIdColor(Integer idColor) {
    this.idColor = idColor;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public Integer getOrden() {
    return orden;
  }

    /**
     *
     * @param orden
     */
    public void setOrden(Integer orden) {
    this.orden = orden;
  }
}