package com.enermex.modelo;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;

/**
 * MunicipioId
 */
public class MunicipioId implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "id_estado")
  Integer idEstado;

  @Column(name = "id_municipio")
  Integer idMunicipio;

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 59 * hash + Objects.hashCode(idEstado);
    hash = 59 * hash + Objects.hashCode(idMunicipio);

    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final MunicipioId other = (MunicipioId) obj;
    if (!Objects.equals(this.idEstado, other.idEstado)) {
      return false;
    }
    if (!Objects.equals(this.idMunicipio, other.idMunicipio)) {
      return false;
    }

    return true;
  }

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }

}