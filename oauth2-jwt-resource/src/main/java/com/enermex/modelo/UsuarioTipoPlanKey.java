
/*##############################################################################
# Nombre del Programa : UsuarioTipoPlanKey.java                                   #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/


package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abraham
 */
@Embeddable
public class UsuarioTipoPlanKey implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="id_cliente")
	private Integer idCliente;
	
	@Column(name="id_plan")
	private Integer idPlan;
	
    /**
     *
     */
    public UsuarioTipoPlanKey() {}

    /**
     *
     * @return
     */
    public Integer getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public Integer getIdPlan() {
		return idPlan;
	}

    /**
     *
     * @param idPlan
     */
    public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}


	 
	
	

}
