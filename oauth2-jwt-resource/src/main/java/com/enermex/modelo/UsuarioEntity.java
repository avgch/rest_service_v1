package com.enermex.modelo;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.enermex.enumerable.EstadoCivil;
import com.enermex.enumerable.Genero;
import com.enermex.enumerable.UsuarioEstatus;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Entidad JPA de tabla t_usuario
 * @author José Antonio González Hernández
 * 
 * Contacto: jgonzalez@grupotama.mx
 * Fecha de creación: 16 de enero de 2020
 */
@Entity
@Table(name = "t_usuario")
public class UsuarioEntity{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_usuario")
  private Long idUsuario;

  @Column(name = "nombre")
  private String nombre;

  @Column(name = "apellido_paterno")
  private String apellidoPaterno;

  @Column(name = "apellido_materno")
  private String apellidoMaterno;

  @Column(name = "correo")
  private String correo;

  @Column(name = "telefono")
  private String telefono;

  @Column(name = "fecha_creacion")
  private Date fechaCreacion;

  // REQ: Sellcom - Ultima fecha de baja
  @Column(name = "fecha_baja")
  private Date fechaBaja;

  @Column(name = "estatus")
  private UsuarioEstatus estatus;

  @Column(name = "fecha_nacimiento")
  private Date fechaNacimiento;


  @Column(name = "genero")
  private Genero genero;

  @Column(name = "estado_civil")
  private EstadoCivil estadoCivil;

  @Column(name = "foto_uri")
  private String fotoUri;

  @JoinTable(
    name = "t_usuarios_rol",
    joinColumns = @JoinColumn(name = "id_usuario"),
    inverseJoinColumns = @JoinColumn(name = "id_rol"))
  @ManyToMany(cascade = CascadeType.MERGE)
  private List<RolEntity> roles;

  @Column(name = "id_pipa")
  private Integer idPipa;

  @ManyToOne()
  @JoinColumn(name = "id_pipa", insertable = false, updatable = false)
  private Pipa pipa;

  // REQ: Itzia V. - Un despachador puede tener asignada más de una ruta
  // @Column(name = "id_ruta")
  // private Integer idRuta;

  @JoinTable(
    name = "t_usuario_ruta",
    joinColumns = @JoinColumn(name = "id_usuario"),
    inverseJoinColumns = @JoinColumn(name = "id_ruta"))
  @ManyToMany(cascade = CascadeType.MERGE)
  private List<RutaEntity> rutas;

    /**
     *
     * @return
     */
    public Long getIdUsuario() {
    return idUsuario;
  }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public String getApellidoPaterno() {
    return apellidoPaterno;
  }

    /**
     *
     * @param apellidoPaterno
     */
    public void setApellidoPaterno(String apellidoPaterno) {
    this.apellidoPaterno = apellidoPaterno;
  }

    /**
     *
     * @return
     */
    public String getApellidoMaterno() {
    return apellidoMaterno;
  }

    /**
     *
     * @param apellidoMaterno
     */
    public void setApellidoMaterno(String apellidoMaterno) {
    this.apellidoMaterno = apellidoMaterno;
  }

    /**
     *
     * @return
     */
    public String getCorreo() {
    return correo;
  }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
    this.correo = correo;
  }

    /**
     *
     * @return
     */
    public String getTelefono() {
    return telefono;
  }

    /**
     *
     * @param telefono
     */
    public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public UsuarioEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(UsuarioEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

    /**
     *
     * @param fechaNacimiento
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

    /**
     *
     * @return
     */
    public Genero getGenero() {
    return genero;
  }

    /**
     *
     * @param genero
     */
    public void setGenero(Genero genero) {
    this.genero = genero;
  }

    /**
     *
     * @return
     */
    public EstadoCivil getEstadoCivil() {
    return estadoCivil;
  }

    /**
     *
     * @param estadoCivil
     */
    public void setEstadoCivil(EstadoCivil estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

    /**
     *
     * @return
     */
    public String getFotoUri() {
    return fotoUri;
  }

    /**
     *
     * @param fotoUri
     */
    public void setFotoUri(String fotoUri) {
    this.fotoUri = fotoUri;
  }

    /**
     *
     * @return
     */
    public List<RolEntity> getRoles() {
    return roles;
  }

    /**
     *
     * @param roles
     */
    public void setRoles(List<RolEntity> roles) {
    this.roles = roles;
  }

    /**
     *
     * @return
     */
    public Integer getIdPipa() {
    return idPipa;
  }

    /**
     *
     * @param idPipa
     */
    public void setIdPipa(Integer idPipa) {
    this.idPipa = idPipa;
  }

    /**
     *
     * @return
     */
    public Pipa getPipa() {
    return pipa;
  }

    /**
     *
     * @param pipa
     */
    public void setPipa(Pipa pipa) {
    this.pipa = pipa;
  }

    /**
     *
     * @return
     */
    public Date getFechaBaja() {
    return fechaBaja;
  }

    /**
     *
     * @param fechaBaja
     */
    public void setFechaBaja(Date fechaBaja) {
    this.fechaBaja = fechaBaja;
  }

    /**
     *
     * @return
     */
    public List<RutaEntity> getRutas() {
    return rutas;
  }

    /**
     *
     * @param rutas
     */
    public void setRutas(List<RutaEntity> rutas) {
    this.rutas = rutas;
  }
}
