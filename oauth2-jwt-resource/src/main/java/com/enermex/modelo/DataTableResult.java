package com.enermex.modelo;

import java.math.BigInteger;
import java.util.Collection;

/**
 *
 * @author abraham
 */
public class DataTableResult {
	
	private int draw;
	private BigInteger recordsTotal;
	private BigInteger recordsFiltered;
	private Collection<?> data;

    /**
     *
     * @return
     */
    public int getDraw() {
		return draw;
	}

    /**
     *
     * @param draw
     */
    public void setDraw(int draw) {
		this.draw = draw;
	}

    /**
     *
     * @return
     */
    public BigInteger getRecordsTotal() {
		return recordsTotal;
	}

    /**
     *
     * @param recordsTotal
     */
    public void setRecordsTotal(BigInteger recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

    /**
     *
     * @return
     */
    public BigInteger getRecordsFiltered() {
		return recordsFiltered;
	}

    /**
     *
     * @param recordsFiltered
     */
    public void setRecordsFiltered(BigInteger recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

    /**
     *
     * @return
     */
    public Collection<?> getData() {
		return data;
	}

    /**
     *
     * @param data
     */
    public void setData(Collection<?> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "DataTableResult [draw=" + draw + ", recordsTotal=" + recordsTotal + ", recordsFiltered="
				+ recordsFiltered + ", data=" + data + "]";
	}
}
