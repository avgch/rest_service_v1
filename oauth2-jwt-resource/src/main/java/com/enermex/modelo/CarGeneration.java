package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "car_generation")
public class CarGeneration {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_car_generation")
	private Integer idCarGeneration;
	
	
	@Column(name = "id_car_model")
	private Integer idCarModel;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "year_begin")
	private String yearBegin;
	
	@Column(name = "year_end")
	private String yearEnd;

    /**
     *
     * @return
     */
    public Integer getIdCarGeneration() {
		return idCarGeneration;
	}

    /**
     *
     * @param idCarGeneration
     */
    public void setIdCarGeneration(Integer idCarGeneration) {
		this.idCarGeneration = idCarGeneration;
	}

    /**
     *
     * @return
     */
    public Integer getIdCarModel() {
		return idCarModel;
	}

    /**
     *
     * @param idCarModel
     */
    public void setIdCarModel(Integer idCarModel) {
		this.idCarModel = idCarModel;
	}

    /**
     *
     * @return
     */
    public String getName() {
		return name;
	}

    /**
     *
     * @param name
     */
    public void setName(String name) {
		this.name = name;
	}

    /**
     *
     * @return
     */
    public String getYearBegin() {
		return yearBegin;
	}

    /**
     *
     * @param yearBegin
     */
    public void setYearBegin(String yearBegin) {
		this.yearBegin = yearBegin;
	}

    /**
     *
     * @return
     */
    public String getYearEnd() {
		return yearEnd;
	}

    /**
     *
     * @param yearEnd
     */
    public void setYearEnd(String yearEnd) {
		this.yearEnd = yearEnd;
	}
	
	
	
	
	
	

}
