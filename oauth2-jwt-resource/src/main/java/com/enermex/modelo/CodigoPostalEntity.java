package com.enermex.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * CodigoPostalEntity
 */
@Entity
@Table(name = "t_cat_codigo")
public class CodigoPostalEntity {
  @Id
  @Column(name = "id_codigo")
  String idCodigo;

  @Column(name = "id_estado")
  Integer idEstado;

  @Column(name = "id_municipio")
  Integer idMunicipio;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "codigoPostal")
  List<ColoniaEntity> colonias;

    /**
     *
     * @return
     */
    public String getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(String idCodigo) {
    this.idCodigo = idCodigo;
  }

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }

    /**
     *
     * @return
     */
    public List<ColoniaEntity> getColonias() {
    return colonias;
  }

    /**
     *
     * @param colonias
     */
    public void setColonias(List<ColoniaEntity> colonias) {
    this.colonias = colonias;
  }
}
