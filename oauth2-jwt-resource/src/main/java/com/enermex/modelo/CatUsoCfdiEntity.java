package com.enermex.modelo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * CatUsrCfdiEntity
 */
@Entity
@Table(name = "t_cat_uso_cfdi")
public class CatUsoCfdiEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "uso_codigo")
  private String codigo;

  @Column(name = "descripcion")
  private String descripcion;

  @Column(name = "estatus")
  private String estatus;

    /**
     *
     * @return
     */
    public String getCodigo() {
    return codigo;
  }

    /**
     *
     * @param codigo
     */
    public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

    /**
     *
     * @return
     */
    public String getDescripcion() {
    return descripcion;
  }

    /**
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

    /**
     *
     * @return
     */
    public String getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
}
