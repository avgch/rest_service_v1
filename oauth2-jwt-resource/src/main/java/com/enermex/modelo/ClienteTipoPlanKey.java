package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abraham
 */
@Embeddable
public class ClienteTipoPlanKey implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	

	@Column(name="id_cliente")
	private BigInteger idCliente;
	
	@Column(name="id_plan")
	private Integer idPlan;
	
	@Column(name="id_estatus")
	private Integer idEstatus;

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public Integer getIdPlan() {
		return idPlan;
	}

    /**
     *
     * @param idPlan
     */
    public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}

    /**
     *
     * @return
     */
    public Integer getIdEstatus() {
		return idEstatus;
	}

    /**
     *
     * @param idEstatus
     */
    public void setIdEstatus(Integer idEstatus) {
		this.idEstatus = idEstatus;
	}
	
	
	
	
}
