package com.enermex.modelo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author abraham
 */
@Table(name = "t_producto_promocion")
@Entity
public class ProductoPromocion implements Serializable{
		
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;


		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "id_producto")
		private Long idProducto;
		
	    @ManyToOne
	    @JoinColumn(name = "id_producto_servicio")
	    private ProductoServicio productoServicio;

		@ManyToOne
		@JoinColumn(name = "id_promocion")
		private Promocion promocion;
		
		
		
		/**
		 * 
		 * @param ps
		 */
		public ProductoPromocion(ProductoServicio ps) {
			super();
			this.productoServicio = ps;
		
		}
		
    /**
     *
     */
    public ProductoPromocion() {}

    /**
     *
     * @return
     */
    public Long getIdProducto() {
			return idProducto;
		}

    /**
     *
     * @param idProducto
     */
    public void setIdProducto(Long idProducto) {
			this.idProducto = idProducto;
		}

    /**
     *
     * @return
     */
    public Promocion getPromocion() {
			return promocion;
		}

    /**
     *
     * @param promocion
     */
    public void setPromocion(Promocion promocion) {
			this.promocion = promocion;
		}

    /**
     *
     * @return
     */
    public ProductoServicio getProductoServicio() {
			return productoServicio;
		}

    /**
     *
     * @param productoServicio
     */
    public void setProductoServicio(ProductoServicio productoServicio) {
			this.productoServicio = productoServicio;
		}

       		
		
	
	   	
		
		
}
