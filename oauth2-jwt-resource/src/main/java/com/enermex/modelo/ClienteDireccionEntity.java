package com.enermex.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enermex.enumerable.DireccionEstatus;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Permite crear y consultar direcciones asociadas a un cliente particular
 */
@Entity
@Table(name = "t_direccion_cliente")
public class ClienteDireccionEntity {
  @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_direccion")
  Long idDireccion;
  
  @Column(name = "cp")
  String cp;
  
  @Column(name = "calle")
  String calle;
  
  @Column(name = "colonia")
  String colonia;
  
  @Column(name = "fecha_creacion")
  Date fechaCreacion;
  
  @Column(name = "latitud")
  String latitud;
  
  @Column(name = "longitud")
  String longitud;
  
  @Column(name = "id_estatus")
  DireccionEstatus idEstatus;

  @JsonIgnore
  @Column(name = "id_cliente")
  Long idCliente;

  @Column(name = "etiqueta")
  String etiqueta;

    /**
     *
     * @return
     */
    public Long getIdDireccion() {
    return idDireccion;
  }

    /**
     *
     * @param idDireccion
     */
    public void setIdDireccion(Long idDireccion) {
    this.idDireccion = idDireccion;
  }

    /**
     *
     * @return
     */
    public String getCp() {
    return cp;
  }

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
    this.cp = cp;
  }

    /**
     *
     * @return
     */
    public String getCalle() {
    return calle;
  }

    /**
     *
     * @param calle
     */
    public void setCalle(String calle) {
    this.calle = calle;
  }

    /**
     *
     * @return
     */
    public String getColonia() {
    return colonia;
  }

    /**
     *
     * @param colonia
     */
    public void setColonia(String colonia) {
    this.colonia = colonia;
  }

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public String getLatitud() {
    return latitud;
  }

    /**
     *
     * @param latitud
     */
    public void setLatitud(String latitud) {
    this.latitud = latitud;
  }

    /**
     *
     * @return
     */
    public String getLongitud() {
    return longitud;
  }

    /**
     *
     * @param longitud
     */
    public void setLongitud(String longitud) {
    this.longitud = longitud;
  }

    /**
     *
     * @return
     */
    public DireccionEstatus getIdEstatus() {
    return idEstatus;
  }

    /**
     *
     * @param idEstatus
     */
    public void setIdEstatus(DireccionEstatus idEstatus) {
    this.idEstatus = idEstatus;
  }

    /**
     *
     * @return
     */
    public Long getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(Long idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public String getEtiqueta() {
    return etiqueta;
  }

    /**
     *
     * @param etiqueta
     */
    public void setEtiqueta(String etiqueta) {
    this.etiqueta = etiqueta;
  }
}
