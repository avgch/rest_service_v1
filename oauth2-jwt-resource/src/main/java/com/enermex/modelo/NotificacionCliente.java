package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Table(name = "t_notificacion_cliente")
@Entity
public class NotificacionCliente implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_notificacion_cliente")
	private Integer idNotificacionCliente;
	
	@Column(name = "id_cliente")
	private BigInteger idCliente;
	
	@Column(name = "id_notificacion")
	private BigInteger idNotificacion;
	
    /**
     *
     */
    public NotificacionCliente() {}

    
	
	/**
	 * 
	 * @param idCliente
	 * @param idNotificacion
	 */
	public NotificacionCliente(BigInteger idCliente, BigInteger idNotificacion) {
		super();
		this.idCliente = idCliente;
		this.idNotificacion = idNotificacion;
	}

    /**
     *
     * @return
     */
    public Integer getIdNotificacionCliente() {
		return idNotificacionCliente;
	}

    /**
     *
     * @param idNotificacionCliente
     */
    public void setIdNotificacionCliente(Integer idNotificacionCliente) {
		this.idNotificacionCliente = idNotificacionCliente;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdNotificacion() {
		return idNotificacion;
	}

    /**
     *
     * @param idNotificacion
     */
    public void setIdNotificacion(BigInteger idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	
	
	
	

}
