package com.enermex.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enermex.enumerable.IncidenciaEstatus;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cat_despachador_incidencia")
public class DespachadorIncidenciaEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_incidencia")
  private Integer idIncidencia;

  @Column(name = "pregunta")
  private String pregunta;

  @Column(name = "respuesta")
  private String respuesta;

  @Column(name = "fecha_creacion")
  private Date fechaCreacion;

  @Column(name = "fecha_baja")
  private Date fechaBaja;

  @Column(name = "estatus")
  private IncidenciaEstatus estatus;

    /**
     *
     * @return
     */
    public Integer getIdIncidencia() {
    return idIncidencia;
  }

    /**
     *
     * @param idIncidencia
     */
    public void setIdIncidencia(Integer idIncidencia) {
    this.idIncidencia = idIncidencia;
  }

    /**
     *
     * @return
     */
    public String getPregunta() {
    return pregunta;
  }

    /**
     *
     * @param pregunta
     */
    public void setPregunta(String pregunta) {
    this.pregunta = pregunta;
  }

    /**
     *
     * @return
     */
    public String getRespuesta() {
    return respuesta;
  }

    /**
     *
     * @param respuesta
     */
    public void setRespuesta(String respuesta) {
    this.respuesta = respuesta;
  }

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public Date getFechaBaja() {
    return fechaBaja;
  }

    /**
     *
     * @param fechaBaja
     */
    public void setFechaBaja(Date fechaBaja) {
    this.fechaBaja = fechaBaja;
  }

    /**
     *
     * @return
     */
    public IncidenciaEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(IncidenciaEstatus estatus) {
    this.estatus = estatus;
  }
}
