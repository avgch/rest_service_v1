package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "car_trim")
public class CarTrim {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_car_trim")
	private Integer idCarTrim;
	
	@Column(name = "id_car_serie")
	private Integer idCarSerie;
	
	@Column(name = "id_car_model")
	private Integer idCarModel;
	
	@Column(name = "name")
	private String nombre;
	
	@Column(name = "start_production_year")
	private String incioProduccion;
	
	@Column(name = "end_production_year")
	private String finProduccion;

    /**
     *
     * @return
     */
    public Integer getIdCarTrim() {
		return idCarTrim;
	}

    /**
     *
     * @param idCarTrim
     */
    public void setIdCarTrim(Integer idCarTrim) {
		this.idCarTrim = idCarTrim;
	}

    /**
     *
     * @return
     */
    public Integer getIdCarSerie() {
		return idCarSerie;
	}

    /**
     *
     * @param idCarSerie
     */
    public void setIdCarSerie(Integer idCarSerie) {
		this.idCarSerie = idCarSerie;
	}

    /**
     *
     * @return
     */
    public Integer getIdCarModel() {
		return idCarModel;
	}

    /**
     *
     * @param idCarModel
     */
    public void setIdCarModel(Integer idCarModel) {
		this.idCarModel = idCarModel;
	}

    /**
     *
     * @return
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return
     */
    public String getIncioProduccion() {
		return incioProduccion;
	}

    /**
     *
     * @param incioProduccion
     */
    public void setIncioProduccion(String incioProduccion) {
		this.incioProduccion = incioProduccion;
	}

    /**
     *
     * @return
     */
    public String getFinProduccion() {
		return finProduccion;
	}

    /**
     *
     * @param finProduccion
     */
    public void setFinProduccion(String finProduccion) {
		this.finProduccion = finProduccion;
	}

  
	
	
	
	
	
}
