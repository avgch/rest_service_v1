package com.enermex.modelo;

import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_pedido")
public class SeguimientoPedidoEntity {
  // Identificador del pedido
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_pedido") private BigInteger idPedido;

  // Columnas del pedido
  @Column(name = "id_servicio")                private Integer    idServicio;
	@Column(name = "litro_carga")                private String     litroCarga;
	@Column(name = "monto_carga")                private Double     montoCarga;
	@Column(name = "llantas_delanteras_presion") private String     llantasDelanterasPresion;
	@Column(name = "llantas_traseras_presion")   private String     llantasTraserasPresion;
	@Column(name = "fecha_creacion")             private Calendar   fechaCreacion;
	@Column(name = "fecha_pedido")               private Calendar   fechaPedido;
	@Column(name = "id_promocion")               private Long       idPromocion;
	@Column(name = "carga_gas")                  private boolean    cargaGas;
	@Column(name = "lavado_auto")                private boolean    lavadoAuto;
	@Column(name = "revision_neumaticos")        private boolean    revisionNeumaticos;
	@Column(name = "estatus_pedido")             private Integer    estatusPedido;
	@Column(name = "detalle")                    private String     detalle;
	@Column(name = "sub_total")                  private Double     subTotal;
	@Column(name = "iva")                        private Double     iva;
	@Column(name = "total_pagar")                private Double     totalPagar;
	@Column(name = "descuento")                  private Double     descuento;
	@Column(name = "total_importe")              private Double     totalImporte;
	@Column(name = "total_con_descuento")        private Double     totalConDescuento;
	@Column(name = "id_tarjeta_cliente")         private Integer    idTarjeta;
	@Column(name = "id_horario_servicio")        private BigInteger idHorarioServicio;
	@Column(name = "uuid")                       private String     uuid;
	@Column(name = "tiempo_traslado")            private String     tiempoTraslado;
  @Column(name = "distancia")                  private String     distancia;
  @Column(name = "fecha_hora_inicio")          private Calendar   fechaHoraInicio;
  @Column(name = "fecha_hora_fin")             private Calendar   fechaHoraFin;
  @Column(name = "precio_lavado_auto")         private Double     precioLavadoAuto;
  @Column(name = "precio_presion_neumaticos")  private Double     precioPresionNeumaticos;
  @Column(name = "calificado")                 private Boolean    calificado;
  @Column(name = "costo_servicio")             private Double     costoServicio;
  @Column(name = "litros_despachados")         private String     litrosDespachados;
  @Column(name = "latitud_despachador")        private String     latitudDespachador;
  @Column(name = "logitud_despachador")        private String     longitudDespachador;
  @Column(name = "precio_por_litro")           private Double     precioPorLitro;
  @Column(name = "precio_por_litro_final")     private Double     precioPorLitroFinal;
  @Column(name = "alcaldia_municipio")         private String     alcaldiaMunicipio;
  @Column(name = "hora_estimada")              private Calendar   horaEstimada;
  @Column(name = "id_ruta")                    private Integer    idRuta;
  
  // Dirección
  @ManyToOne()
  @JoinColumn(name = "id_direccion", insertable = false, updatable = false)
  private ClienteDireccionEntity direccion;

	@ManyToOne()
	@JoinColumn(name = "id_ruta", insertable = false, updatable = false)
  private SRutaEntity ruta;
  
  @ManyToOne()
	@JoinColumn(name = "id_despachador", insertable = false, updatable = false)
	private SUsuarioEntity despachador;
	
	@Column(name = "id_combustible") private Integer  idCombustible;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
	private SClienteEntity cliente;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_tarjeta_cliente", referencedColumnName = "id_tarjeta_cliente",insertable = false, updatable = false)
	private Tarjeta tarjeta;
  
  @OneToOne(mappedBy = "pedido", cascade = CascadeType.ALL)
  private Cancelacion cancelacion;
	 
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "id_promocion",insertable = false, updatable = false)
  private SPromocionEntity promocion;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "id_pipa",insertable = false, updatable = false)
  private Pipa pipa;

    /**
     *
     */
    @Entity
  @Table(name = "t_ruta")
  public static class SRutaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ruta") Integer idRuta;
    @Column(name = "nombre")  String  nombre;

    // Getters

            /**
             *
             * @return
             */
    public Integer getIdRuta() { return idRuta; }

            /**
             *
             * @return
             */
            public String  getNombre() { return nombre; }
    
    // Setters

            /**
             *
             * @param idRuta
             */
    public void setIdRuta(Integer idRuta) { this.idRuta = idRuta; }

            /**
             *
             * @param nombre
             */
            public void setNombre(String nombre)  { this.nombre = nombre; }
  }

    /**
     *
     */
    @Entity
  @Table(name = "t_usuario")
  public static class SUsuarioEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")       private Long   idUsuario;
    @Column(name = "nombre")           private String nombre;
    @Column(name = "apellido_paterno") private String apellidoPaterno;
    @Column(name = "apellido_materno") private String apellidoMaterno;
    
    // Getters

            /**
             *
             * @return
             */
    public Long   getIdUsuario()       { return idUsuario; }

            /**
             *
             * @return
             */
            public String getNombre()          { return nombre; }

            /**
             *
             * @return
             */
            public String getApellidoPaterno() { return apellidoPaterno; }

            /**
             *
             * @return
             */
            public String getApellidoMaterno() { return apellidoMaterno; }

    // Setters

            /**
             *
             * @param idUsuario
             */
    public void setIdUsuario(Long idUsuario)               { this.idUsuario = idUsuario; }

            /**
             *
             * @param nombre
             */
            public void setNombre(String nombre)                   { this.nombre = nombre; }

            /**
             *
             * @param apellidoPaterno
             */
            public void setApellidoPaterno(String apellidoPaterno) { this.apellidoPaterno = apellidoPaterno; }

            /**
             *
             * @param apellidoMaterno
             */
            public void setApellidoMaterno(String apellidoMaterno) { this.apellidoMaterno = apellidoMaterno; }
  }

    /**
     *
     */
    @Entity
  @Table(name = "t_cliente")
  public static class SClienteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente")       private BigInteger idCliente;
    @Column(name = "nombre")           private String     nombre;
    @Column(name = "correo")           private String     correo;
    @Column(name = "id_titular")       private BigInteger idTitular;
    @Column(name = "apellido_paterno") private String     apellidoPaterno;
    @Column(name = "apellido_materno") private String     apellidoMaterno;

    // Getters

            /**
             *
             * @return
             */
    public BigInteger getIdCliente()       { return idCliente; }

            /**
             *
             * @return
             */
            public String     getNombre()          { return nombre; }

            /**
             *
             * @return
             */
            public String     getCorreo()          { return correo; }

            /**
             *
             * @return
             */
            public BigInteger getIdTitular()       { return idTitular; }

            /**
             *
             * @return
             */
            public String     getApellidoPaterno() { return apellidoPaterno; }

            /**
             *
             * @return
             */
            public String     getApellidoMaterno() { return apellidoMaterno; }
    
    // Setters

            /**
             *
             * @param idCliente
             */
    public void setIdCliente(BigInteger idCliente)         { this.idCliente = idCliente; }

            /**
             *
             * @param nombre
             */
            public void setNombre(String nombre)                   { this.nombre = nombre; }

            /**
             *
             * @param correo
             */
            public void setCorreo(String correo)                   { this.correo = correo; }

            /**
             *
             * @param idTitular
             */
            public void setIdTitular(BigInteger idTitular)         { this.idTitular = idTitular; }

            /**
             *
             * @param apellidoPaterno
             */
            public void setApellidoPaterno(String apellidoPaterno) { this.apellidoPaterno = apellidoPaterno; }

            /**
             *
             * @param apellidoMaterno
             */
            public void setApellidoMaterno(String apellidoMaterno) { this.apellidoMaterno = apellidoMaterno; }
  }

    /**
     *
     */
    @Entity
  @Table(name = "t_promocion")
  public static class SPromocionEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_promocion")     private Long   idPromocion;
    @Column(name = "codigo_promocion") private String codigoPromocion;

    // Getters

            /**
             *
             * @return
             */
    public Long   getIdPromocion()     { return idPromocion; }

            /**
             *
             * @return
             */
            public String getCodigoPromocion() { return codigoPromocion; }

    // Setters

            /**
             *
             * @param idPromocion
             */
    public void setIdPromocion(Long idPromocion)           { this.idPromocion = idPromocion; }

            /**
             *
             * @param codigoPromocion
             */
            public void setCodigoPromocion(String codigoPromocion) { this.codigoPromocion = codigoPromocion; }
  }

  // Getters

    /**
     *
     * @return
     */
  public BigInteger getIdPedido()                 { return idPedido; }

    /**
     *
     * @return
     */
    public Integer    getIdServicio()               { return idServicio; }

    /**
     *
     * @return
     */
    public String     getLitroCarga()               { return litroCarga; }

    /**
     *
     * @return
     */
    public Double     getMontoCarga()               { return montoCarga; }

    /**
     *
     * @return
     */
    public String     getLlantasDelanterasPresion() { return llantasDelanterasPresion; }

    /**
     *
     * @return
     */
    public String     getLlantasTraserasPresion()   { return llantasTraserasPresion; }

    /**
     *
     * @return
     */
    public Calendar   getFechaCreacion()            { return fechaCreacion; }

    /**
     *
     * @return
     */
    public Calendar   getFechaPedido()              { return fechaPedido; }

    /**
     *
     * @return
     */
    public Long       getIdPromocion()              { return idPromocion; }

    /**
     *
     * @return
     */
    public boolean    isCargaGas()                  { return cargaGas; }

    /**
     *
     * @return
     */
    public boolean    isLavadoAuto()                { return lavadoAuto; }

    /**
     *
     * @return
     */
    public boolean    isRevisionNeumaticos()        { return revisionNeumaticos; }

    /**
     *
     * @return
     */
    public Integer    getEstatusPedido()            { return estatusPedido; }

    /**
     *
     * @return
     */
    public String     getDetalle()                  { return detalle; }

    /**
     *
     * @return
     */
    public Double     getSubTotal()                 { return subTotal; }

    /**
     *
     * @return
     */
    public Double     getIva()                      { return iva; }

    /**
     *
     * @return
     */
    public Double     getTotalPagar()               { return totalPagar; }

    /**
     *
     * @return
     */
    public Double     getDescuento()                { return descuento; }

    /**
     *
     * @return
     */
    public Double     getTotalImporte()             { return totalImporte; }

    /**
     *
     * @return
     */
    public Double     getTotalConDescuento()        { return totalConDescuento; }

    /**
     *
     * @return
     */
    public Integer    getIdTarjeta()                { return idTarjeta; }

    /**
     *
     * @return
     */
    public BigInteger getIdHorarioServicio()        { return idHorarioServicio; }

    /**
     *
     * @return
     */
    public String     getUuid()                     { return uuid; }

    /**
     *
     * @return
     */
    public String     getTiempoTraslado()           { return tiempoTraslado; }

    /**
     *
     * @return
     */
    public String     getDistancia()                { return distancia; }

    /**
     *
     * @return
     */
    public Calendar   getFechaHoraInicio()          { return fechaHoraInicio; }

    /**
     *
     * @return
     */
    public Calendar   getFechaHoraFin()             { return fechaHoraFin; }

    /**
     *
     * @return
     */
    public Double     getPrecioLavadoAuto()         { return precioLavadoAuto; }

    /**
     *
     * @return
     */
    public Double     getPrecioPresionNeumaticos()  { return precioPresionNeumaticos; }

    /**
     *
     * @return
     */
    public Boolean    getCalificado()               { return calificado; }

    /**
     *
     * @return
     */
    public Double     getCostoServicio()            { return costoServicio; }

    /**
     *
     * @return
     */
    public String     getLitrosDespachados()        { return litrosDespachados; }

    /**
     *
     * @return
     */
    public String     getLatitudDespachador()       { return latitudDespachador; }

    /**
     *
     * @return
     */
    public String     getLongitudDespachador()      { return longitudDespachador; }

    /**
     *
     * @return
     */
    public Double     getPrecioPorLitro()           { return precioPorLitro; }

    /**
     *
     * @return
     */
    public Double     getPrecioPorLitroFinal()      { return precioPorLitroFinal; }

    /**
     *
     * @return
     */
    public String     getAlcaldiaMunicipio()        { return alcaldiaMunicipio; }

    /**
     *
     * @return
     */
    public Calendar   getHoraEstimada()             { return horaEstimada; }

    /**
     *
     * @return
     */
    public ClienteDireccionEntity getDireccion()      { return direccion; }

    /**
     *
     * @return
     */
    public SRutaEntity            getRuta()           { return ruta; }

    /**
     *
     * @return
     */
    public SUsuarioEntity         getDespachador()    { return despachador; }

    /**
     *
     * @return
     */
    public Integer                getIdCombustible()  { return idCombustible; }

    /**
     *
     * @return
     */
    public SClienteEntity         getCliente()        { return cliente; }

    /**
     *
     * @return
     */
    public Tarjeta                getTarjeta()        { return tarjeta; }

    /**
     *
     * @return
     */
    public Cancelacion            getCancelacion()    { return cancelacion; }

    /**
     *
     * @return
     */
    public SPromocionEntity       getPromocion()      { return promocion; }

    /**
     *
     * @return
     */
    public Pipa                   getPipa()           { return pipa; }

  // Setters

    /**
     *
     * @param idPedido
     */
  public void setIdPedido(BigInteger idPedido)                             { this.idPedido = idPedido; }

    /**
     *
     * @param idServicio
     */
    public void setIdServicio(Integer idServicio)                            { this.idServicio = idServicio; }

    /**
     *
     * @param litroCarga
     */
    public void setLitroCarga(String litroCarga)                             { this.litroCarga = litroCarga; }

    /**
     *
     * @param montoCarga
     */
    public void setMontoCarga(Double montoCarga)                             { this.montoCarga = montoCarga; }

    /**
     *
     * @param llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) { this.llantasDelanterasPresion = llantasDelanterasPresion; }

    /**
     *
     * @param llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion)     { this.llantasTraserasPresion = llantasTraserasPresion; }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion)                     { this.fechaCreacion = fechaCreacion; }

    /**
     *
     * @param fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido)                         { this.fechaPedido = fechaPedido; }

    /**
     *
     * @param idPromocion
     */
    public void setIdPromocion(Long idPromocion)                             { this.idPromocion = idPromocion; }

    /**
     *
     * @param cargaGas
     */
    public void setCargaGas(boolean cargaGas)                                { this.cargaGas = cargaGas; }

    /**
     *
     * @param lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto)                            { this.lavadoAuto = lavadoAuto; }

    /**
     *
     * @param revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos)            { this.revisionNeumaticos = revisionNeumaticos; }

    /**
     *
     * @param estatusPedido
     */
    public void setEstatusPedido(Integer estatusPedido)                      { this.estatusPedido = estatusPedido; }

    /**
     *
     * @param detalle
     */
    public void setDetalle(String detalle)                                   { this.detalle = detalle; }

    /**
     *
     * @param subTotal
     */
    public void setSubTotal(Double subTotal)                                 { this.subTotal = subTotal; }

    /**
     *
     * @param iva
     */
    public void setIva(Double iva)                                           { this.iva = iva; }

    /**
     *
     * @param promocion
     */
    public void setPromocion(SPromocionEntity promocion)                     { this.promocion = promocion; }

    /**
     *
     * @param totalPagar
     */
    public void setTotalPagar(Double totalPagar)                             { this.totalPagar = totalPagar; }

    /**
     *
     * @param cancelacion
     */
    public void setCancelacion(Cancelacion cancelacion)                      { this.cancelacion = cancelacion; }

    /**
     *
     * @param descuento
     */
    public void setDescuento(Double descuento)                               { this.descuento = descuento; }

    /**
     *
     * @param tarjeta
     */
    public void setTarjeta(Tarjeta tarjeta)                                  { this.tarjeta = tarjeta; }

    /**
     *
     * @param totalImporte
     */
    public void setTotalImporte(Double totalImporte)                         { this.totalImporte = totalImporte; }

    /**
     *
     * @param cliente
     */
    public void setCliente(SClienteEntity cliente)                           { this.cliente = cliente; }

    /**
     *
     * @param totalConDescuento
     */
    public void setTotalConDescuento(Double totalConDescuento)               { this.totalConDescuento = totalConDescuento; }

    /**
     *
     * @param idCombustible
     */
    public void setIdCombustible(Integer idCombustible)                      { this.idCombustible = idCombustible; }

    /**
     *
     * @param idTarjeta
     */
    public void setIdTarjeta(Integer idTarjeta)                              { this.idTarjeta = idTarjeta; }

    /**
     *
     * @param despachador
     */
    public void setDespachador(SUsuarioEntity despachador)                   { this.despachador = despachador; }

    /**
     *
     * @param idHorarioServicio
     */
    public void setIdHorarioServicio(BigInteger idHorarioServicio)           { this.idHorarioServicio = idHorarioServicio; }

    /**
     *
     * @param ruta
     */
    public void setRuta(SRutaEntity ruta)                                    { this.ruta = ruta; }

    /**
     *
     * @param uuid
     */
    public void setUuid(String uuid)                                         { this.uuid = uuid; }

    /**
     *
     * @param direccion
     */
    public void setDireccion(ClienteDireccionEntity direccion)               { this.direccion = direccion; }

    /**
     *
     * @param tiempoTraslado
     */
    public void setTiempoTraslado(String tiempoTraslado)                     { this.tiempoTraslado = tiempoTraslado; }

    /**
     *
     * @param horaEstimada
     */
    public void setHoraEstimada(Calendar horaEstimada)                       { this.horaEstimada = horaEstimada; }

    /**
     *
     * @param distancia
     */
    public void setDistancia(String distancia)                               { this.distancia = distancia; }

    /**
     *
     * @param alcaldiaMunicipio
     */
    public void setAlcaldiaMunicipio(String alcaldiaMunicipio)               { this.alcaldiaMunicipio = alcaldiaMunicipio; }

    /**
     *
     * @param fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio)                 { this.fechaHoraInicio = fechaHoraInicio; }

    /**
     *
     * @param precioPorLitroFinal
     */
    public void setPrecioPorLitroFinal(Double precioPorLitroFinal)           { this.precioPorLitroFinal = precioPorLitroFinal; }

    /**
     *
     * @param fechaHoraFin
     */
    public void setFechaHoraFin(Calendar fechaHoraFin)                       { this.fechaHoraFin = fechaHoraFin; }

    /**
     *
     * @param precioPorLitro
     */
    public void setPrecioPorLitro(Double precioPorLitro)                     { this.precioPorLitro = precioPorLitro; }

    /**
     *
     * @param precioLavadoAuto
     */
    public void setPrecioLavadoAuto(Double precioLavadoAuto)                 { this.precioLavadoAuto = precioLavadoAuto; }

    /**
     *
     * @param longitudDespachador
     */
    public void setLongitudDespachador(String longitudDespachador)           { this.longitudDespachador = longitudDespachador; }

    /**
     *
     * @param precioPresionNeumaticos
     */
    public void setPrecioPresionNeumaticos(Double precioPresionNeumaticos)   { this.precioPresionNeumaticos = precioPresionNeumaticos; }

    /**
     *
     * @param latitudDespachador
     */
    public void setLatitudDespachador(String latitudDespachador)             { this.latitudDespachador = latitudDespachador; }

    /**
     *
     * @param calificado
     */
    public void setCalificado(Boolean calificado)                            { this.calificado = calificado; }

    /**
     *
     * @param litrosDespachados
     */
    public void setLitrosDespachados(String litrosDespachados)               { this.litrosDespachados = litrosDespachados; }

    /**
     *
     * @param costoServicio
     */
    public void setCostoServicio(Double costoServicio)                       { this.costoServicio = costoServicio; }

    /**
     *
     * @param pipa
     */
    public void setPipa(Pipa pipa)                                           { this.pipa = pipa; }

  // Requerido para búsqueda

    /**
     *
     * @return
     */
  public Integer getIdRuta() {
    return idRuta;
  }

    /**
     *
     * @param idRuta
     */
    public void setIdRuta(Integer idRuta) {
    this.idRuta = idRuta;
  }
}