package com.enermex.modelo;

/**
 *
 * @author abraham
 */
public class Column {
	
    /**
     *
     */
    public String data ;

    /**
     *
     */
    public String name ;

    /**
     *
     */
    public boolean searchable;

    /**
     *
     */
    public boolean orderable ;

    /**
     *
     * @return
     */
    public String getData() {
		return data;
	}

    /**
     *
     * @param data
     */
    public void setData(String data) {
		this.data = data;
	}

    /**
     *
     * @return
     */
    public String getName() {
		return name;
	}

    /**
     *
     * @param name
     */
    public void setName(String name) {
		this.name = name;
	}

    /**
     *
     * @return
     */
    public boolean isSearchable() {
		return searchable;
	}

    /**
     *
     * @param searchable
     */
    public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

    /**
     *
     * @return
     */
    public boolean isOrderable() {
		return orderable;
	}

    /**
     *
     * @param orderable
     */
    public void setOrderable(boolean orderable) {
		this.orderable = orderable;
	}
    
    
    

}
