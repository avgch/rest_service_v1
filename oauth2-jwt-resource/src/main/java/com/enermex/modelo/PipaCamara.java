package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.enermex.modelo.PipaCamaraKey;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_pipa_camara")
public class PipaCamara implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@EmbeddedId
	PipaCamaraKey pipaCamaraKey;

    /**
     *
     */
    public PipaCamara() {
		super();
	}

    /**
     *
     * @return
     */
    public PipaCamaraKey getPipaCamaraKey() {
		return pipaCamaraKey;
	}

    /**
     *
     * @param pipaCamaraKey
     */
    public void setPipaCamaraKey(PipaCamaraKey pipaCamaraKey) {
		this.pipaCamaraKey = pipaCamaraKey;
	}

    /**
     *
     * @param idPipa
     * @param idCamara
     */
    public PipaCamara(Integer idPipa, Integer idCamara) {
		this.pipaCamaraKey = new PipaCamaraKey();
		this.pipaCamaraKey.setIdCamara(idCamara);
		this.pipaCamaraKey.setIdPipa(idPipa);
		
	}
	
	
	
	

}
