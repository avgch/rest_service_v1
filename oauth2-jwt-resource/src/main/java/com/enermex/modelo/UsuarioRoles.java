
/*##############################################################################
# Nombre del Programa : UsuarioRoles.java                                          #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/


package com.enermex.modelo;


import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "usuarios_rol")
public class UsuarioRoles  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private UsuarioRolesKey id;
	
    /**
     *
     */
    public UsuarioRoles () {
		this.id = new UsuarioRolesKey();
	}

    /**
     *
     * @return
     */
    public UsuarioRolesKey getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(UsuarioRolesKey id) {
		this.id = id;
	}
	
     
    

}
