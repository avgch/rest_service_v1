package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cat_estatus_pedido")
public class CatEstatusPedido {
	
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_estatus_pedido")
	private Integer idEstatusPedido;
	
	@Column(name = "estatus")
	private String estatus;

    /**
     *
     */
    public CatEstatusPedido() {
		super();
	}

    /**
     *
     * @return
     */
    public Integer getIdEstatusPedido() {
		return idEstatusPedido;
	}

    /**
     *
     * @param idEstatusPedido
     */
    public void setIdEstatusPedido(Integer idEstatusPedido) {
		this.idEstatusPedido = idEstatusPedido;
	}

    /**
     *
     * @return
     */
    public String getEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     */
    public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	
	
	

}
