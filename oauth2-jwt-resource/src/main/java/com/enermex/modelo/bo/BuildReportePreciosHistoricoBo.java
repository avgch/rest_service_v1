package com.enermex.modelo.bo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.sl.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.VerticalAlign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.enermex.enumerable.ColumnasReporteHistoricoEnum;
import com.enermex.enumerable.TipoCombustiblePetroIntelligenceEnum;
import com.enermex.repository.impl.PreciosGasolinaHistoricoReporteImpl;
import com.enermex.avg.exceptions.FastGasException;
import com.enermex.dto.reportes.ReporteHistoricoDto;

/**
 *
 * @author abraham
 */
@Component
public class BuildReportePreciosHistoricoBo extends BuildReporteExcelBo {
	
	@Autowired
	PreciosGasolinaHistoricoReporteImpl preciosGasolinaHistoricoReporteImpl;
	
    /**
     *
     * @param wb
     * @param fechaInc
     * @param fechaFin
     * @throws IOException
     * @throws FastGasException
     */
    public void buildReporteExcel(SXSSFWorkbook wb,String fechaInc,String fechaFin) throws IOException, FastGasException {
    
		buildRegistrosReporteExcel(wb,fechaInc,fechaFin);
        System.out.println("###  Fin build reporte historico de precios excel###");
        

    }

    /**
     *
     * @param sheet
     */
    public void buildTitulosReporteExcel(SXSSFSheet sheet) {
    	
    	
    	buildFechaCreacion(sheet);
        System.out.println("### Creando titulos reposte historico de precios ###");
        Row row = sheet.createRow(8);
        String colName = "";

        for (int i = 0; i < 7; i++) {

            switch (i) {
                case 0:
                    colName = ColumnasReporteHistoricoEnum.Column1.getColumnName();
                    break;
                case 1:
                    colName = ColumnasReporteHistoricoEnum.Column2.getColumnName();
                    break;
                case 2:
                    colName = ColumnasReporteHistoricoEnum.Column3.getColumnName();
                    break;
                case 3:
                    colName = ColumnasReporteHistoricoEnum.Column4.getColumnName();
                    break;
                case 4:
                    colName = ColumnasReporteHistoricoEnum.Column5.getColumnName();
                    break;
                case 5:
                    colName = ColumnasReporteHistoricoEnum.Column6.getColumnName();
                    break;
                case 6:
                    colName = ColumnasReporteHistoricoEnum.Column7.getColumnName();
                    break;
    
            }

            Cell cell = row.createCell(i);
            cell.setCellValue(colName);

            
            //font 
            Font font= row.getSheet().getWorkbook().createFont();
            font.setFontHeightInPoints((short)10);
            font.setFontName("Arial");
            font.setBold(true);
            
            //cellStyle.setFont(font);
            
           // cell.setCellStyle(cellStyle);
            
            System.out.println("###  FIN de bloque . ###");
        }

    }
    
    /**
     *
     * @param wb
     * @param fechInc
     * @param fechaFin
     * @throws FastGasException
     * @throws IOException
     */
    public void buildRegistrosReporteExcel(SXSSFWorkbook wb,String fechInc, String fechaFin) throws FastGasException, IOException {
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	Date creacion=null;
    	int numRows = preciosGasolinaHistoricoReporteImpl.numeroRows(fechInc, fechaFin);
    	int numReg=0;
    	int numHoja=1;
    	int    bloque = 10000;
    	int paginasD = numRows/bloque;
    	int    paginas  = (int)paginasD;
    	int startPos=0,endPos=0;
    	SXSSFSheet sheet=null;
    	CellStyle cellStyle=null;
    	
    	if(numRows==0)
    	  throw new FastGasException("ADMIN","No se encontraron registros");
    	
  
         System.out.println("###  Creando REPORTE HISTORICO ###");
         System.out.println("### numero de registros para crear reporte: "+numRows);
         //en que row indicia
        int j=9;
        
        if(paginas!=0) {
        
           for (int i = 0; i <=paginas ; i++) 
           {
        	  endPos+=bloque;
           	List<ReporteHistoricoDto>  registros= this.preciosGasolinaHistoricoReporteImpl.consultaReporte(fechInc,fechaFin,startPos,endPos);
               
        	 //si el numero de rows por hoja es igual a 900
           	//creamos una hoja nueva
           
        	  for(ReporteHistoricoDto reporte : registros) {
    		  	if(numReg==0) {
    	           	 sheet = wb.createSheet("precios_historico"+numHoja);
    	           	buildTitulosReporteExcel(sheet);
    	           	setAnchoColumnas(sheet);
    	           	setImage(sheet,wb);
    	           	numHoja++;
    	           	j=9;
    	           	}    	        		
        
                 Row rowData = sheet.createRow(j);
                 
                 if(j==9) {
                	 
                	cellStyle = rowData.getSheet().getWorkbook().createCellStyle();
                    cellStyle.setAlignment(HorizontalAlignment.CENTER);
                    cellStyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
                 }

                 //ID
                 Cell cell00 = rowData.createCell(0);
                 cell00.setCellValue(reporte.getId().toString());
                 cell00.setCellStyle(cellStyle);
                 
                 //ESTADO
                 Cell cell01 = rowData.createCell(1);
                 cell01.setCellValue(reporte.getEstado());
                 cell01.setCellStyle(cellStyle);
                 
                 //ALCALDIA
                 Cell cell02 = rowData.createCell(2);
                 cell02.setCellValue(reporte.getAlcaldia());
                 cell02.setCellStyle(cellStyle);
                 
                 //CP
                 Cell cell03 = rowData.createCell(3);
                 cell03.setCellValue(reporte.getCp());
                 cell03.setCellStyle(cellStyle);
                 
                 //COMBUSTIBLE VIGENCIA
                 Cell cell04 = rowData.createCell(4);
                 if(reporte.getIdCombustible().compareTo(TipoCombustiblePetroIntelligenceEnum.REGULAR.getCombustible())==0) {
                	 cell04.setCellValue("Regular");
                	 cell04.setCellStyle(cellStyle);
                 }
                 if(reporte.getIdCombustible().compareTo(TipoCombustiblePetroIntelligenceEnum.PREMIUM.getCombustible())==0) {
                	 cell04.setCellValue("Premium");
                	 cell04.setCellStyle(cellStyle);
                 }
                     
                 
                 if(reporte.getIdCombustible().compareTo(TipoCombustiblePetroIntelligenceEnum.DIESEL.getCombustible())==0) {
                	 cell04.setCellValue("Diesel");
                	 cell04.setCellStyle(cellStyle);
                 }
                     
                 
                 //PROMEDIO
                 Cell cell05 = rowData.createCell(5);
                 cell05.setCellValue("$"+String.format("%.2f",reporte.getPromedio()));
                 cell05.setCellStyle(cellStyle);
                 
                 //ESTADO
                 Cell cell06 = rowData.createCell(6);
                 try {
                	 creacion = sdf2.parse(reporte.getFechaRegistro());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                 
                 
                 cell06.setCellValue(sdf.format(creacion));
                 cell06.setCellStyle(cellStyle);
                 
                 
                 
                  numReg++;
                  
            	  if(numReg==1000000)
            		  numReg=0;
            	  
                  j++;
                  startPos=endPos;
        	  }
        	  
        	
            }
        }else {
        	
        	List<ReporteHistoricoDto>  registros= this.preciosGasolinaHistoricoReporteImpl.consultaReporteSinBloque(fechInc,fechaFin);
            
      	  
      	  for(ReporteHistoricoDto reporte : registros) {
      	  	if(numReg==0) {
	           	 sheet = wb.createSheet("precios_historico"+numHoja);
	           	buildTitulosReporteExcel(sheet);
	           	setAnchoColumnas(sheet);
	           	setImage(sheet,wb);
	           	numHoja++;
	           	j=9;
	           	}    	        		
   
            Row rowData = sheet.createRow(j);
            
            if(j==9) {
           	 
           	cellStyle = rowData.getSheet().getWorkbook().createCellStyle();
               cellStyle.setAlignment(HorizontalAlignment.CENTER);
               cellStyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
            }

            //ID
            Cell cell00 = rowData.createCell(0);
            cell00.setCellValue(reporte.getId().toString());
            cell00.setCellStyle(cellStyle);
            
            //ESTADO
            Cell cell01 = rowData.createCell(1);
            cell01.setCellValue(reporte.getEstado());
            cell01.setCellStyle(cellStyle);
            
            //ALCALDIA
            Cell cell02 = rowData.createCell(2);
            cell02.setCellValue(reporte.getAlcaldia());
            cell02.setCellStyle(cellStyle);
            
            //CP
            Cell cell03 = rowData.createCell(3);
            cell03.setCellValue(reporte.getCp());
            cell03.setCellStyle(cellStyle);
            
            //COMBUSTIBLE VIGENCIA
            Cell cell04 = rowData.createCell(4);
            if(reporte.getIdCombustible().compareTo(TipoCombustiblePetroIntelligenceEnum.REGULAR.getCombustible())==0) {
           	 cell04.setCellValue("Regular");
           	 cell04.setCellStyle(cellStyle);
            }
            if(reporte.getIdCombustible().compareTo(TipoCombustiblePetroIntelligenceEnum.PREMIUM.getCombustible())==0) {
           	 cell04.setCellValue("Premium");
           	 cell04.setCellStyle(cellStyle);
            }
                
            
            if(reporte.getIdCombustible().compareTo(TipoCombustiblePetroIntelligenceEnum.DIESEL.getCombustible())==0) {
           	 cell04.setCellValue("Diesel");
           	 cell04.setCellStyle(cellStyle);
            }
                
            
            //PROMEDIO
            Cell cell05 = rowData.createCell(5);
            cell05.setCellValue("$"+String.format("%.2f",reporte.getPromedio()));
            cell05.setCellStyle(cellStyle);
            
            //ESTADO
            Cell cell06 = rowData.createCell(6);
            try {
           	 creacion = sdf2.parse(reporte.getFechaRegistro());
           	 System.out.println(creacion);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            
            cell06.setCellValue(sdf.format(creacion));
            cell06.setCellStyle(cellStyle);
            
            
            
            
             numReg++;
             
       	  if(numReg==1000000)
       		  numReg=0;
       	  
             j++;
             startPos=endPos;
      	  }
        	
        }

            System.out.println("### end bloque ###");
      }

    
    private void buildFechaCreacion(SXSSFSheet sheet) {
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    	String fecha = sdf.format(new Date());
    	
    	Row row = sheet.createRow(1);
    	Cell cellCreateOn = row.createCell(5);
        cellCreateOn.setCellValue("Fecha de reporte: "+fecha);
    	
    }
    /**
     *
     * @param sheet
     */
    private void setAnchoColumnas(SXSSFSheet sheet) {
    	
		sheet.setColumnWidth(0, 7000);
		sheet.setColumnWidth(1, 7000);
		sheet.setColumnWidth(2, 7000);
		sheet.setColumnWidth(3, 7000);
		sheet.setColumnWidth(4, 7000);
		sheet.setColumnWidth(5, 7000);
		sheet.setColumnWidth(6, 7000);
    }
    private void setImage(SXSSFSheet sheet,SXSSFWorkbook wb) throws IOException {
    	
    	CreationHelper helper = wb.getCreationHelper();
    	final FileInputStream streamImage = new FileInputStream(getClass().getClassLoader().getResource("logo.png").getFile());
    	final ClientAnchor anchor = helper.createClientAnchor();
    	final Drawing drawing = sheet.createDrawingPatriarch();
    	final int pictureIndex =wb.addPicture(IOUtils.toByteArray(streamImage), SXSSFWorkbook.PICTURE_TYPE_PNG);
    	 anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);
    	 anchor.setCol1( 2);
    	 anchor.setCol2( 3);
    	 anchor.setRow1( 0 );
    	 anchor.setRow2( 4 );
    	 // same row is okay
    	 final Picture pict = drawing.createPicture( anchor, pictureIndex );
    	 pict.resize(2.0,2.0);
    	
    }
    
    
}
