package com.enermex.modelo.bo;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 *
 * @author abraham
 */
public abstract class BuildReporteExcelBo {
	
	
    
    
    private SXSSFWorkbook             workbook;
    private SXSSFSheet                    sheet;
    private List<Object>             list;
    private InputStream              fileIn;
    
    
//    abstract public Object buildReporteExcel(ReporteConsolidadoQDto consultaDto);
//    abstract public Object buildReportePdf(ReporteConsolidadoQDto consultaDto,ServletOutputStream out);
//    abstract public void   buildTitulosReporteExcel();
//    abstract public void   buildRegistrosReporteExcel(ReporteConsolidadoQDto consultaDto);
//    abstract public void   buildRegistrosReportePdf(PdfPTable tableRegTable,ReporteConsolidadoQDto consultaDto) ;
    
    /**
     *
     * @return
     */
    public SXSSFWorkbook getWorkbook() {
		return workbook;
	}

    /**
     *
     * @return
     */
    public SXSSFSheet getSheet() {
		return sheet;
	}

    /**
     *
     * @param sheet
     */
    public void setSheet(SXSSFSheet sheet) {
		this.sheet = sheet;
	}

    /**
     *
     * @param workbook
     */
    public void setWorkbook(SXSSFWorkbook workbook) {
		this.workbook = workbook;
	}

    /**
     *
     * @return
     */
    public List<Object> getList() {
        return list;
    }

    /**
     *
     * @param list
     */
    public void setList(List<Object> list) {
        this.list = list;
    }

    /**
     *
     * @return
     */
    public InputStream getFileIn() {
        return fileIn;
    }

    /**
     *
     * @param fileIn
     */
    public void setFileIn(InputStream fileIn) {
        this.fileIn = fileIn;
    }

}
