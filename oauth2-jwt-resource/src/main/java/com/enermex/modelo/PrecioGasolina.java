package com.enermex.modelo;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_gasolina_estado_municipio")
public class PrecioGasolina {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "id_estado")
	private String ID_ESTADO;
	
	@Column(name = "id_municipio")
	private String ID_MUNICIPIO;
	
	@Column(name = "fecha_registro")
	private String FECHA_REGISTRO;
	
	@Column(name = "combustible")
	private String COMBUSTIBLE;
	
	@Column(name = "minimo")
	private String MINIMO;
	
	@Column(name = "maximo")
	private String MAXIMO;
	
	@Column(name = "promedio")
	private String PROMEDIO;
	
	@Column(name = "tot_gas_prom")
	private String TOT_GAS_PROM;
	
	@Column(name = "tot_gas")
	private String TOT_GAS;
	
	@Column(name = "fecha_creacion")
	private Calendar fechaCreacion;
	
    /**
     *
     */
    public PrecioGasolina() {}
	
    /**
     *
     * @param pg
     */
    public PrecioGasolina(PrecioGasolina pg) {
		super();
		ID_ESTADO = pg.getID_ESTADO();
		ID_MUNICIPIO = pg.getID_MUNICIPIO();
		FECHA_REGISTRO = pg.getFECHA_REGISTRO();
		COMBUSTIBLE = pg.getCOMBUSTIBLE();
		MINIMO = pg.getMINIMO();
		MAXIMO = pg.getMAXIMO();
		PROMEDIO = pg.getPROMEDIO();
		TOT_GAS_PROM = pg.getTOT_GAS_PROM();
		TOT_GAS = pg.getTOT_GAS();
		this.fechaCreacion = Calendar.getInstance();
	}

    /**
     *
     * @return
     */
    public String getID_ESTADO() {
		return ID_ESTADO;
	}

    /**
     *
     * @param iD_ESTADO
     */
    public void setID_ESTADO(String iD_ESTADO) {
		ID_ESTADO = iD_ESTADO;
	}

    /**
     *
     * @return
     */
    public String getID_MUNICIPIO() {
		return ID_MUNICIPIO;
	}

    /**
     *
     * @param iD_MUNICIPIO
     */
    public void setID_MUNICIPIO(String iD_MUNICIPIO) {
		ID_MUNICIPIO = iD_MUNICIPIO;
	}

    /**
     *
     * @return
     */
    public String getFECHA_REGISTRO() {
		return FECHA_REGISTRO;
	}

    /**
     *
     * @param fECHA_REGISTRO
     */
    public void setFECHA_REGISTRO(String fECHA_REGISTRO) {
		FECHA_REGISTRO = fECHA_REGISTRO;
	}

    /**
     *
     * @return
     */
    public String getCOMBUSTIBLE() {
		return COMBUSTIBLE;
	}

    /**
     *
     * @param cOMBUSTIBLE
     */
    public void setCOMBUSTIBLE(String cOMBUSTIBLE) {
		COMBUSTIBLE = cOMBUSTIBLE;
	}

    /**
     *
     * @return
     */
    public String getMINIMO() {
		return MINIMO;
	}

    /**
     *
     * @param mINIMO
     */
    public void setMINIMO(String mINIMO) {
		MINIMO = mINIMO;
	}

    /**
     *
     * @return
     */
    public String getMAXIMO() {
		return MAXIMO;
	}

    /**
     *
     * @param mAXIMO
     */
    public void setMAXIMO(String mAXIMO) {
		MAXIMO = mAXIMO;
	}

    /**
     *
     * @return
     */
    public String getPROMEDIO() {
		return PROMEDIO;
	}

    /**
     *
     * @param pROMEDIO
     */
    public void setPROMEDIO(String pROMEDIO) {
		PROMEDIO = pROMEDIO;
	}

    /**
     *
     * @return
     */
    public String getTOT_GAS_PROM() {
		return TOT_GAS_PROM;
	}

    /**
     *
     * @param tOT_GAS_PROM
     */
    public void setTOT_GAS_PROM(String tOT_GAS_PROM) {
		TOT_GAS_PROM = tOT_GAS_PROM;
	}

    /**
     *
     * @return
     */
    public String getTOT_GAS() {
		return TOT_GAS;
	}

    /**
     *
     * @param tOT_GAS
     */
    public void setTOT_GAS(String tOT_GAS) {
		TOT_GAS = tOT_GAS;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public Long getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(Long id) {
		this.id = id;
	}

   
	

}
