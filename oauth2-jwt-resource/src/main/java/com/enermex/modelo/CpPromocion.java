package com.enermex.modelo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cp_promocion")
public class CpPromocion {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private BigInteger id;
	
	@ManyToOne
	@JoinColumn(name = "id_codigo")
	private CodigoPostalEntity codigo;
	
	@ManyToOne
	@JoinColumn(name = "id_promocion")
	private Promocion promocion;
	
    /**
     *
     * @param codigo
     */
    public CpPromocion(CodigoPostalEntity codigo) {
		super();
		this.codigo = codigo;
	
	}
	
    /**
     *
     */
    public CpPromocion() {
		
	
	}

    /**
     *
     * @return
     */
    public BigInteger getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(BigInteger id) {
		this.id = id;
	}

    /**
     *
     * @return
     */
    public CodigoPostalEntity getCodigo() {
		return codigo;
	}

    /**
     *
     * @param codigo
     */
    public void setCodigo(CodigoPostalEntity codigo) {
		this.codigo = codigo;
	}

    /**
     *
     * @return
     */
    public Promocion getPromocion() {
		return promocion;
	}

    /**
     *
     * @param promocion
     */
    public void setPromocion(Promocion promocion) {
		this.promocion = promocion;
	}

    
	
	
	
	

}
