package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * QuejaEvidenciaEntity
 */
@Entity
@Table(name = "t_queja_evidencia")
public class QuejaEvidenciaEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_evidencia")
  Long idEvidencia;

  @Column(name = "id_queja")
  Long idQueja;

  @Column(name = "nombre")
  String nombre;

  @JsonIgnore
  @ManyToOne()
  @JoinColumn(name = "id_queja", insertable = false, updatable = false)
  QuejaEntity queja;

    /**
     *
     * @return
     */
    public Long getIdEvidencia() {
    return idEvidencia;
  }

    /**
     *
     * @param idEvidencia
     */
    public void setIdEvidencia(Long idEvidencia) {
    this.idEvidencia = idEvidencia;
  }

    /**
     *
     * @return
     */
    public Long getIdQueja() {
    return idQueja;
  }

    /**
     *
     * @param idQueja
     */
    public void setIdQueja(Long idQueja) {
    this.idQueja = idQueja;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public QuejaEntity getQueja() {
    return queja;
  }

    /**
     *
     * @param queja
     */
    public void setQueja(QuejaEntity queja) {
    this.queja = queja;
  }
}
