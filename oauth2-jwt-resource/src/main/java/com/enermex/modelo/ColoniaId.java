package com.enermex.modelo;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;

/**
 * ColoniaId
 */
public class ColoniaId implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "id_codigo")
  Integer idCodigo;

  @Column(name = "id_colonia")
  Integer idColonia;

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 59 * hash + Objects.hashCode(idCodigo);
    hash = 59 * hash + Objects.hashCode(idColonia);

    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final ColoniaId other = (ColoniaId) obj;
    if (!Objects.equals(this.idCodigo, other.idCodigo)) {
      return false;
    }
    if (!Objects.equals(this.idColonia, other.idColonia)) {
      return false;
    }

    return true;
  }

    /**
     *
     * @return
     */
    public Integer getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(Integer idCodigo) {
    this.idCodigo = idCodigo;
  }

    /**
     *
     * @return
     */
    public Integer getIdColonia() {
    return idColonia;
  }

    /**
     *
     * @param idColonia
     */
    public void setIdColonia(Integer idColonia) {
    this.idColonia = idColonia;
  }
}