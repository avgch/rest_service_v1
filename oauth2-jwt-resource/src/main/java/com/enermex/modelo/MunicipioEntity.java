package com.enermex.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * MunicipioEntity
 */
@Entity
@Table(name = "t_cat_municipio")
@IdClass(value = MunicipioId.class)
public class MunicipioEntity {
  @Id
  @Column(name = "id_estado")
  Integer idEstado;

  @Id
  @Column(name = "id_municipio")
  Integer idMunicipio;
  
  @Column(name = "nombre")
  String nombre;


  @JsonIgnore
  @ManyToOne()
  @JoinColumn(name = "id_estado", insertable = false, updatable = false)
  private EstadoEntity estado;

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public EstadoEntity getEstado() {
    return estado;
  }

    /**
     *
     * @param estado
     */
    public void setEstado(EstadoEntity estado) {
    this.estado = estado;
  }
}
