package com.enermex.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "t_cliente_cat_tipo_plan")
public class UsuarioTipoPlanA  implements Serializable{


    @Id
    @ManyToOne
    @JoinColumn(name = "id_cliente")
    @JsonBackReference
    private Cliente cliente;


    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_plan")
    private TipoPlan tipoPlan;

	@Column(name = "id_estatus")
	private int estatus;



	/**
	*
	* @param tipoPlan
	* @param cliente
	*/
	public UsuarioTipoPlanA(TipoPlan tipoPlan) {
		super();
		this.tipoPlan = tipoPlan;
		this.estatus = 1;
	}

    /**
     *
     */
    public UsuarioTipoPlanA() {
	
	}
	
    /**
     *
     * @return
     */
    public Cliente getCliente() {
	return cliente;
	}
	
    /**
     *
     * @param cliente
     */
    public void setCliente(Cliente cliente) {
	this.cliente = cliente;
	}
	
    /**
     *
     * @return
     */
    public TipoPlan getTipoPlan() {
	return tipoPlan;
	}
	
    /**
     *
     * @param tipoPlan
     */
    public void setTipoPlan(TipoPlan tipoPlan) {
	this.tipoPlan = tipoPlan;
	}
	
    /**
     *
     * @return
     */
    public int getEstatus() {
	return estatus;
	}
	
    /**
     *
     * @param estatus
     */
    public void setEstatus(int estatus) {
	this.estatus = estatus;
	}




}

