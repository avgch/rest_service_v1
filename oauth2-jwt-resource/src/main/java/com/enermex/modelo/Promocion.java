package com.enermex.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.poi.hpsf.Array;

import com.enermex.dto.ProductoDto;
import com.enermex.dto.PromocionDto;
import com.enermex.enumerable.PlanesEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.enermex.modelo.CpPromocion;

/**
 *
 * @author abraham
 */
@Table(name = "t_promocion")
@Entity
public class Promocion implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_promocion")
	private Long idPromocion;
	
	@Column(name = "titulo")
	private String  titulo;
	
	@Column(name = "codigo_promocion")
	private String  codigoPromocion;
	
	@Column(name = "producto")
	private String  producto;
	
	@Column(name = "descuento")
	private String  descuento;
	
	@Column(name = "monto")
	private Double monto;
	
	@Column(name = "detalle")
	private String  detalle;
	
	@Column(name = "estatus")
	private Boolean  estatus;
	
	@Column(name = "create_by")
	private String  creadoPor;
	
	@Column(name = "es_frecuente")
	private Boolean esFrecuente;
	
	@Column(name = "para_todos")
	private Boolean paraTodos;
	
	@Column(name = "para_algunos")
	private Boolean paraAlgunos;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_hora_inicio")
	private Calendar  fechaHoraInicio;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_hora_vigencia")
	private Calendar  fechaHoraVigencia;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "create_on")
	private Calendar  fechaCreacion;
	
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	@JoinColumn(name = "id_promocion")
	private Set<ProductoPromocion> productosPromo = new HashSet();
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	@JoinColumn(name = "id_promocion")
	private Set<PlanPromocion> planesPromo = new HashSet();
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	@JoinColumn(name = "id_promocion")
	private Set<CpPromocion> cps = new HashSet();
	
	@Lob
	@Column(name = "imagen_promo")
	private byte[] imagenPromo;
	
	@Column(name = "id_estado")
	private Integer idEstado;
	
	@Column(name = "id_municipio")
	private Integer idAlcaldia;
	
    /**
     *
     */
    public Promocion() {}

	/**
	 * 
	 * @param promoP
     * @param esUpdate
	 */
	public Promocion(PromocionDto promoP,boolean esUpdate) {
		super();
		
		  ProductoServicio ps;  
		  TipoPlan tipoPlan;
		  ProductoPromocion pp;
		  CodigoPostalEntity   cp;
		  CpPromocion          codPromo;
		  PlanPromocion     plp;
		  String incS,finS;
		  promoP.getFechaInicio().setHours(promoP.getHoraInicio().getHours());
		  promoP.getFechaInicio().setMinutes(promoP.getHoraInicio().getMinutes());
		  
		  promoP.getFechaFin().setHours(promoP.getHoraFin().getHours());
		  promoP.getFechaFin().setMinutes(promoP.getHoraFin().getMinutes());
		  
		  Date fechaInicio=null,fechaFin;
		  Calendar fechaIncC = Calendar.getInstance();
		  Calendar fechaFinC = Calendar.getInstance();
		  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			
		  TimeZone cdmx = TimeZone.getTimeZone("Mexico/General");
		  sdf.setTimeZone(cdmx);
		  try {
		if(!esUpdate) {
		   incS= sdf.format(promoP.getFechaInicio());
		   finS= sdf.format(promoP.getFechaFin());
		}
		else {
			
			incS= sdf.format(promoP.getFechaInicio());
			finS= sdf.format(promoP.getFechaFin());
//			incS= sdf.format(promoP.getHoraInicio());
//			finS= sdf.format(promoP.getHoraFin());
		}
		     //fecha de inicio de promo
			fechaInicio = sdf.parse(incS);
			fechaIncC.setTime(fechaInicio);
			//fecha de fin de promo
			fechaFin = sdf.parse(finS);
			fechaFinC.setTime(fechaFin);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.titulo = promoP.getTitulo();
		
		if(promoP.getDescuentoPorcentaje()) {
			
			this.descuento = promoP.getMontoDescuento();	
		}
		else if(promoP.getDescuentoMonto()) {
			
			this.monto = Double.parseDouble(promoP.getMontoDescuento());	
		}
			
		
		this.detalle = promoP.getDetalle();
		this.estatus = true;
		//validamos que no vengan nulos
		
		
		this.esFrecuente = (promoP.getClientesFrecuentes()==null) ? false : promoP.getClientesFrecuentes();
		this.paraAlgunos = (promoP.getClientesAlgunos()==null) ? false : promoP.getClientesAlgunos();
		this.paraTodos   = (promoP.getClientesTodos()==null) ? false : promoP.getClientesTodos();
		this.fechaHoraInicio = fechaIncC;
		this.fechaHoraVigencia = fechaFinC;
		this.idAlcaldia        = promoP.getIdAlcaldia();
		this.idEstado          = promoP.getIdEstado();
		this.fechaCreacion = Calendar.getInstance();
		
		//De acuerdo a los productos seleccionados
		if (promoP.getProductos() != null) {

			for (ProductoDto dto : promoP.getProductos()) {
				ps = new ProductoServicio();
				ps.setId(dto.getId());
				pp = new ProductoPromocion(ps);
				this.addToProdPromo(pp);
			}

		}
		//guardamos los cps
		if (promoP.getCps() != null) {
			
			List<String> codigos = new ArrayList<>();

			for(CodigoPostalEntity c: promoP.getCps()) {
				codigos.add(c.getIdCodigo());
				
			}
			
			for (String codigo : codigos) {
				cp = new CodigoPostalEntity();
				cp.setIdCodigo(codigo);
				codPromo = new CpPromocion(cp);
				this.addToCpsPromo(codPromo);
			}

		}
		
		//De acuerdo a los planes seleccionados
		//PLAN INDIVIDUAL
		
		if((promoP.getPlanIndividual()==null) ? false : promoP.getPlanIndividual()) {
			tipoPlan = new TipoPlan();
			tipoPlan.setId(PlanesEnum.PARTICULAR.getValor());
			plp = new PlanPromocion();
			plp.setTipoPlan(tipoPlan);
			this.addToPlanPromo(plp);
		}
		//PLAN FAMILIAR
		if((promoP.getPlanFamiliar()==null) ? false : promoP.getPlanFamiliar()) {
			tipoPlan = new TipoPlan();
			tipoPlan.setId(PlanesEnum.FAMILIAR.getValor());
			plp = new PlanPromocion();
			plp.setTipoPlan(tipoPlan);
			this.addToPlanPromo(plp);
		}
		//PLAN EMPRESARIAL
		if ((promoP.getPlanEmpresarial()==null) ? false : promoP.getPlanEmpresarial()) {
			tipoPlan = new TipoPlan();
			tipoPlan.setId(PlanesEnum.EMPRESARIAL.getValor());
			plp = new PlanPromocion();
			plp.setTipoPlan(tipoPlan);
			this.addToPlanPromo(plp);
		}
			
		
	}

    /**
     *
     * @return
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     *
     * @return
     */
    public String getTitulo() {
		return titulo;
	}

    /**
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

    /**
     *
     * @return
     */
    public String getCodigoPromocion() {
		return codigoPromocion;
	}

    /**
     *
     * @param codigoPromocion
     */
    public void setCodigoPromocion(String codigoPromocion) {
		this.codigoPromocion = codigoPromocion;
	}

    /**
     *
     * @return
     */
    public String getProducto() {
		return producto;
	}

    /**
     *
     * @param producto
     */
    public void setProducto(String producto) {
		this.producto = producto;
	}

    /**
     *
     * @return
     */
    public String getDescuento() {
		return descuento;
	}

    /**
     *
     * @param descuento
     */
    public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

    /**
     *
     * @return
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     *
     * @param detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

    /**
     *
     * @return
     */
    public Boolean getEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     */
    public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

    /**
     *
     * @return
     */
    public String getCreadoPor() {
		return creadoPor;
	}

    /**
     *
     * @param creadoPor
     */
    public void setCreadoPor(String creadoPor) {
		this.creadoPor = creadoPor;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaHoraInicio() {
		return fechaHoraInicio;
	}

    /**
     *
     * @param fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaHoraVigencia() {
		return fechaHoraVigencia;
	}

    /**
     *
     * @param fechaHoraVigencia
     */
    public void setFechaHoraVigencia(Calendar fechaHoraVigencia) {
		this.fechaHoraVigencia = fechaHoraVigencia;
	}

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public Boolean getEsFrecuente() {
		return esFrecuente;
	}

    /**
     *
     * @param esFrecuente
     */
    public void setEsFrecuente(Boolean esFrecuente) {
		this.esFrecuente = esFrecuente;
	}

    /**
     *
     * @return
     */
    public Double getMonto() {
		return monto;
	}

    /**
     *
     * @param monto
     */
    public void setMonto(Double monto) {
		this.monto = monto;
	}

    /**
     *
     * @return
     */
    @JsonIgnore
	public Set<ProductoPromocion> getProductosPromo() {
		return productosPromo;
	}

    /**
     *
     * @param productosPromo
     */
    public void setProductosPromo(Set<ProductoPromocion> productosPromo) {
		this.productosPromo = productosPromo;
	}

    /**
     *
     * @return
     */
    public byte[] getImagenPromo() {
		return imagenPromo;
	}

    /**
     *
     * @param imagenPromo
     */
    public void setImagenPromo(byte[] imagenPromo) {
		this.imagenPromo = imagenPromo;
	}

    /**
     *
     * @param prodPromo
     */
    public void addToProdPromo(ProductoPromocion prodPromo) {
		prodPromo.setPromocion(this);
		this.productosPromo.add(prodPromo);
	}
	
    /**
     *
     * @param planPromo
     */
    public void addToPlanPromo(PlanPromocion planPromo) {
		planPromo.setPromocion(this);
		this.planesPromo.add(planPromo);
	}
	
    /**
     *
     * @param cpPromo
     */
    public void addToCpsPromo(CpPromocion cpPromo) {
		cpPromo.setPromocion(this);
		this.cps.add(cpPromo);
	}

    /**
     *
     * @return
     */
    public Boolean getParaTodos() {
		return paraTodos;
	}

    /**
     *
     * @param paraTodos
     */
    public void setParaTodos(Boolean paraTodos) {
		this.paraTodos = paraTodos;
	}

    /**
     *
     * @return
     */
    public Boolean getParaAlgunos() {
		return paraAlgunos;
	}

    /**
     *
     * @param paraAlgunos
     */
    public void setParaAlgunos(Boolean paraAlgunos) {
		this.paraAlgunos = paraAlgunos;
	}

    /**
     *
     * @return
     */
    @JsonIgnore
	public Set<PlanPromocion> getPlanesPromo() {
		return planesPromo;
	}

    /**
     *
     * @param planesPromo
     */
    public void setPlanesPromo(Set<PlanPromocion> planesPromo) {
		this.planesPromo = planesPromo;
	}

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
		return idEstado;
	}

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

    /**
     *
     * @return
     */
    public Integer getIdAlcaldia() {
		return idAlcaldia;
	}

    /**
     *
     * @param idAlcaldia
     */
    public void setIdAlcaldia(Integer idAlcaldia) {
		this.idAlcaldia = idAlcaldia;
	}

    /**
     *
     * @return
     */
    @JsonIgnore
	public Set<CpPromocion> getCps() {
		return cps;
	}

    /**
     *
     * @param cps
     */
    public void setCps(Set<CpPromocion> cps) {
		this.cps = cps;
	}
   
	

   

	

}
