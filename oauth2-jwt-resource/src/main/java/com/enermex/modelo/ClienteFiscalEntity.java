package com.enermex.modelo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enermex.enumerable.ClienteFiscalEstatus;
import com.enermex.enumerable.ClienteFiscalPersona;

/**
 * ClienteFiscalEntity
 */
@Entity
@Table(name = "t_cliente_fiscal")
public class ClienteFiscalEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_fiscal")
  private BigInteger idFiscal;

  @Column(name  = "persona")
  private ClienteFiscalPersona persona;

  @Column(name = "id_cliente")
  private BigInteger idCliente;

  @Column(name = "estatus")
  private ClienteFiscalEstatus estatus;

  @Column(name = "rfc")
  private String rfc;

  @Column(name = "razon_social")
  private String razonSocial;

  @Column(name = "cp")
  private String cp;

  @Column(name = "calle")
  private String calle;

  @Column(name = "colonia")
  private String colonia;

  @Column(name = "num_interior")
  private String numInterior;

  @Column(name = "num_exterior")
  private String numExterior;

  @Column(name = "estado")
  private String estado;

  @Column(name = "municipio")
  private String municipio;

  @Column(name = "correo")
  private String correo;

  @Column(name = "telefono")
  private String telefono;

    /**
     *
     * @return
     */
    public BigInteger getIdFiscal() {
    return idFiscal;
  }

    /**
     *
     * @param idFiscal
     */
    public void setIdFiscal(BigInteger idFiscal) {
    this.idFiscal = idFiscal;
  }

    /**
     *
     * @return
     */
    public ClienteFiscalPersona getPersona() {
    return persona;
  }

    /**
     *
     * @param persona
     */
    public void setPersona(ClienteFiscalPersona persona) {
    this.persona = persona;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public ClienteFiscalEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(ClienteFiscalEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public String getRfc() {
    return rfc;
  }

    /**
     *
     * @param rfc
     */
    public void setRfc(String rfc) {
    this.rfc = rfc;
  }

    /**
     *
     * @return
     */
    public String getRazonSocial() {
    return razonSocial;
  }

    /**
     *
     * @param razonSocial
     */
    public void setRazonSocial(String razonSocial) {
    this.razonSocial = razonSocial;
  }

    /**
     *
     * @return
     */
    public String getCp() {
    return cp;
  }

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
    this.cp = cp;
  }

    /**
     *
     * @return
     */
    public String getCalle() {
    return calle;
  }

    /**
     *
     * @param calle
     */
    public void setCalle(String calle) {
    this.calle = calle;
  }

    /**
     *
     * @return
     */
    public String getColonia() {
    return colonia;
  }

    /**
     *
     * @param colonia
     */
    public void setColonia(String colonia) {
    this.colonia = colonia;
  }

    /**
     *
     * @return
     */
    public String getNumInterior() {
    return numInterior;
  }

    /**
     *
     * @param numInterior
     */
    public void setNumInterior(String numInterior) {
    this.numInterior = numInterior;
  }

    /**
     *
     * @return
     */
    public String getNumExterior() {
    return numExterior;
  }

    /**
     *
     * @param numExterior
     */
    public void setNumExterior(String numExterior) {
    this.numExterior = numExterior;
  }

    /**
     *
     * @return
     */
    public String getEstado() {
    return estado;
  }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
    this.estado = estado;
  }

    /**
     *
     * @return
     */
    public String getMunicipio() {
    return municipio;
  }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
    this.municipio = municipio;
  }

    /**
     *
     * @return
     */
    public String getCorreo() {
    return correo;
  }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
    this.correo = correo;
  }

    /**
     *
     * @return
     */
    public String getTelefono() {
    return telefono;
  }

    /**
     *
     * @param telefono
     */
    public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

}