package com.enermex.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abraham
 */
@Table(name = "t_parametros")
@Entity
public class Parametro implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_parametro")
	private Integer idParametro;

	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "valor")
	private String valor;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "tipo")
	private String tipo;
	
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name = "creado_por")
	private String creado_por;

    /**
     *
     */
    public Parametro() {
		super();
	}

    /**
     *
     * @return
     */
    public Integer getIdParametro() {
		return idParametro;
	}

    /**
     *
     * @param idParametro
     */
    public void setIdParametro(Integer idParametro) {
		this.idParametro = idParametro;
	}

    /**
     *
     * @return
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return
     */
    public String getValor() {
		return valor;
	}

    /**
     *
     * @param valor
     */
    public void setValor(String valor) {
		this.valor = valor;
	}

    /**
     *
     * @return
     */
    public String getDescripcion() {
		return descripcion;
	}

    /**
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

    /**
     *
     * @return
     */
    public String getTipo() {
		return tipo;
	}

    /**
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
		this.tipo = tipo;
	}

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return
     */
    public String getCreado_por() {
		return creado_por;
	}

    /**
     *
     * @param creado_por
     */
    public void setCreado_por(String creado_por) {
		this.creado_por = creado_por;
	}
	
	
	
	
	
	
	
}
