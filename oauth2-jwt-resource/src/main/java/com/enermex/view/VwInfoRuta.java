package com.enermex.view;

import java.math.BigInteger;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vw_info_ruta")
public class VwInfoRuta {
	
	
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	   private int id;
	   
	   @Column(name = "id_ruta")
	   private int idRuta;
	   
	   @Column(name = "id_usuario")
	   private BigInteger idUsuario;
	   
	   @Column(name = "ruta")
	   private String ruta;
	   
	   @Column(name = "cp")
	   private String cp;
	   
	   @Column(name = "despachador")
	   private String despachador;
	   
	   @Column(name = "estatus")
	   private String estatus;

		public int getId() {
			return id;
		}
	
		public void setId(int id) {
			this.id = id;
		}
	
		public int getIdRuta() {
			return idRuta;
		}
	
		public void setIdRuta(int idRuta) {
			this.idRuta = idRuta;
		}
	
		public BigInteger getIdUsuario() {
			return idUsuario;
		}
	
		public void setIdUsuario(BigInteger idUsuario) {
			this.idUsuario = idUsuario;
		}
	
		public String getRuta() {
			return ruta;
		}
	
		public void setRuta(String ruta) {
			this.ruta = ruta;
		}
	
		public String getCp() {
			return cp;
		}
	
		public void setCp(String cp) {
			this.cp = cp;
		}
	
		public String getDespachador() {
			return despachador;
		}
	
		public void setDespachador(String despachador) {
			this.despachador = despachador;
		}
	
		public String getEstatus() {
			return estatus;
		}
	
		public void setEstatus(String estatus) {
			this.estatus = estatus;
		} 
	   
	   
	   
}
