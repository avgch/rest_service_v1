package com.enermex.view;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "v_cliente_nombre")
public class ClienteNombreView {
  @Id
  @Column(name = "id_cliente")
  private BigInteger idCliente;

  @Column(name = "nombre_completo")
  private String nombreCompleto;

  @Column(name = "nombre_completo_upper")
  private String nombreCompletoUpper;

  @Column(name = "id_titular")
  private BigInteger idTitular;

  @Column(name = "id_familiar")
  private BigInteger idFamiliar;

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public String getNombreCompleto() {
    return nombreCompleto;
  }

    /**
     *
     * @param nombreCompleto
     */
    public void setNombreCompleto(String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }

    /**
     *
     * @return
     */
    public String getNombreCompletoUpper() {
    return nombreCompletoUpper;
  }

    /**
     *
     * @param nombreCompletoUpper
     */
    public void setNombreCompletoUpper(String nombreCompletoUpper) {
    this.nombreCompletoUpper = nombreCompletoUpper;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdTitular() {
    return idTitular;
  }

    /**
     *
     * @param idTitular
     */
    public void setIdTitular(BigInteger idTitular) {
    this.idTitular = idTitular;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdFamiliar() {
    return idFamiliar;
  }

    /**
     *
     * @param idFamiliar
     */
    public void setIdFamiliar(BigInteger idFamiliar) {
    this.idFamiliar = idFamiliar;
  }
}