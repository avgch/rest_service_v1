package com.enermex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "v_despachador_servicio")
public class DespachadorServicioView {
  @Id
  @Column(name = "id_despachador")
  private Long idDespachador;

  @Column(name = "promedio")
  private Double promedio;

  @Column(name = "pedidos")
  private Long pedidos;

    /**
     *
     * @return
     */
    public Long getIdDespachador() {
    return idDespachador;
  }

    /**
     *
     * @param idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
    this.idDespachador = idDespachador;
  }

    /**
     *
     * @return
     */
    public Double getPromedio() {
    return promedio;
  }

    /**
     *
     * @param promedio
     */
    public void setPromedio(Double promedio) {
    this.promedio = promedio;
  }

    /**
     *
     * @return
     */
    public Long getPedidos() {
    return pedidos;
  }

    /**
     *
     * @param pedidos
     */
    public void setPedidos(Long pedidos) {
    this.pedidos = pedidos;
  }
}
