package com.enermex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "v_codigo")
public class CodigoPostalUsoView {
  @Id
  @Column(name = "id_codigo")
  private String idCodigo;

    /**
     *
     * @return
     */
    public String getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(String idCodigo) {
    this.idCodigo = idCodigo;
  }
}