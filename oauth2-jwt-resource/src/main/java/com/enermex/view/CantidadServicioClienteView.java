package com.enermex.view;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "v_cantidad_servicio_cliente")
public class CantidadServicioClienteView {
  @Id
  @Column(name = "id_cliente")
  private BigInteger idCliente;

  @Column(name = "cantidad")
  private Long cantidad;

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public Long getCantidad() {
    return cantidad;
  }

    /**
     *
     * @param cantidad
     */
    public void setCantidad(Long cantidad) {
    this.cantidad = cantidad;
  }
}