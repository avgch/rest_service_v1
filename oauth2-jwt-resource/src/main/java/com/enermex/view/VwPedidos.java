package com.enermex.view;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.enermex.modelo.DireccionCliente;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "vw_pedido")
public class VwPedidos {
	
	
	
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	   private int id;
	   
	   @Column(name = "id_pedido")
	   private int idPedido;
	   
	   @Column(name = "id_usuario")
	   private int idUsuario;
	   
	   @Column(name = "id_cliente")
	   private BigInteger idCliente;
	   
	   @Column(name = "nombre")
	   private String nombre;
	   
	   @Column(name = "fecha_pedido")
	   private String fecha_pedido;
	   
	   @Column(name = "monto_carga")
	   private float montoCarga;
	   
	   @Column(name = "litro_carga")
	   private String litroCarga;
	   
	   @Column(name = "verifica_presion_neumatico")
	   private boolean verificaPresionNeumatico;
	   
	   @Column(name = "llantas_delanteras_presion")
	   private String llantasDelanterasPresion;
	   
	   @Column(name = "llantas_traseras_presion")
	   private String llantasTraserasPresion;
	   
	   @Column(name = "cargaGas")
	   private boolean cargaCas;
	   
	   @Column(name = "lavadoAuto")
	   private boolean lavadoAuto;
	   
	   @Column(name = "revision_neumaticos")
	   private boolean revisionNeumaticos;
	   
	   @Column(name = "id_direccion")
	   private Integer idDireccion;
	   
	   @Transient
	   private DireccionCliente direccionCliente;

    /**
     *
     * @return
     */
    public int getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

    /**
     *
     * @return
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return
     */
    public String getFecha_pedido() {
		return fecha_pedido;
	}

    /**
     *
     * @param fecha_pedido
     */
    public void setFecha_pedido(String fecha_pedido) {
		this.fecha_pedido = fecha_pedido;
	}

    /**
     *
     * @return
     */
    public float getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     */
    public void setMontoCarga(float montoCarga) {
		this.montoCarga = montoCarga;
	}

    /**
     *
     * @return
     */
    public String getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     */
    public void setLitroCarga(String litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     *
     * @return
     */
    public boolean isVerificaPresionNeumatico() {
		return verificaPresionNeumatico;
	}

    /**
     *
     * @param verificaPresionNeumatico
     */
    public void setVerificaPresionNeumatico(boolean verificaPresionNeumatico) {
		this.verificaPresionNeumatico = verificaPresionNeumatico;
	}

    /**
     *
     * @return
     */
    public String getLlantasDelanterasPresion() {
		return llantasDelanterasPresion;
	}

    /**
     *
     * @param llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
		this.llantasDelanterasPresion = llantasDelanterasPresion;
	}

    /**
     *
     * @return
     */
    public String getLlantasTraserasPresion() {
		return llantasTraserasPresion;
	}

    /**
     *
     * @param llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion) {
		this.llantasTraserasPresion = llantasTraserasPresion;
	}

    /**
     *
     * @return
     */
    public boolean isCargaCas() {
		return cargaCas;
	}

    /**
     *
     * @param cargaCas
     */
    public void setCargaCas(boolean cargaCas) {
		this.cargaCas = cargaCas;
	}

    /**
     *
     * @return
     */
    public boolean isLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     *
     * @return
     */
    public boolean isRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     *
     * @return
     */
    public DireccionCliente getDireccionCliente() {
		return direccionCliente;
	}

    /**
     *
     * @param direccionCliente
     */
    public void setDireccionCliente(DireccionCliente direccionCliente) {
		this.direccionCliente = direccionCliente;
	}

    /**
     *
     * @return
     */
    public int getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(int id) {
		this.id = id;
	}

    /**
     *
     * @return
     */
    public int getIdUsuario() {
		return idUsuario;
	}

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return
     */
    public Integer getIdDireccion() {
		return idDireccion;
	}

    /**
     *
     * @param idDireccion
     */
    public void setIdDireccion(Integer idDireccion) {
		this.idDireccion = idDireccion;
	}
	
	
    	   

	   
	   
	

}
