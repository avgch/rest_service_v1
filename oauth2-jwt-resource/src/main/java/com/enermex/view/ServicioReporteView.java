package com.enermex.view;

import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "v_servicio_reporte")
public class ServicioReporteView {
  @Id
  @Column(name = "id_pedido")
  private BigInteger idPedido;
  
  @Column(name = "uuid")
  private String uuid;
  
  @Column(name = "fecha_creacion")
  private Calendar fechaCreacion;
  
  @Column(name = "fecha_pedido")
  private Calendar fechaPedido;
  
  @Column(name = "fecha_hora_inicio")
  private Calendar fechaHoraInicio;
  
  @Column(name = "fecha_hora_fin")
  private Calendar fechaHoraFin;
  
  @Column(name = "estatus")
  private Integer estatus;
  
  @Column(name = "has_presion")
  private Boolean hasPresion;
  
  @Column(name = "has_lavado")
  private Boolean hasLavado;
  
  @Column(name = "id_tipo_pedido")
  private Integer idTipoPedido;
  
  @Column(name = "id_combustible")
  private Integer idCombustible;
  
  @Column(name = "litros_solicitados")
  private Double litrosSolicitados;
  
  @Column(name = "litros_despachados")
  private Double litrosDespachados;
  
  @Column(name = "litro_precio")
  private Double litroPrecio;

  @Column(name = "litro_precio_inicio")
  private Double litroPrecioInicio;
  
  @Column(name = "servicio_precio")
  private Double servicioPrecio;
  
  @Column(name = "subtotal")
  private Double subtotal;
  
  @Column(name = "descuento")
  private Double descuento;
  
  @Column(name = "total_importe")
  private Double totalImporte;
  
  @Column(name = "iva")
  private Double iva;
  
  @Column(name = "total_pagar")
  private Double totalPagar;
  
  @Column(name = "id_cliente")
  private BigInteger idCliente;
  
  @Column(name = "nombre_cliente")
  private String nombreCliente;
  
  @Column(name = "apellido_paterno_cliente")
  private String apellidoPaternoCliente;
  
  @Column(name = "apellido_materno_cliente")
  private String apellidoMaternoCliente;
  
  @Column(name = "id_estado")
  private Integer idEstado;
  
  @Column(name = "nombre_estado")
  private String nombreEstado;
  
  @Column(name = "id_municipio")
  private Integer idMunicipio;
  
  @Column(name = "nombre_municipio")
  private String nombreMunicipio;
  
  @Column(name = "cp_direccion")
  private String cpDireccion;
  
  @Column(name = "calle_direccion")
  private String calleDireccion;
  
  @Column(name = "colonia_direccion")
  private String coloniaDireccion;
  
  @Column(name = "id_promocion")
  private Long idPromocion;
  
  @Column(name = "codigo_promocion")
  private String codigoPromocion;
  
  @Column(name = "id_ruta")
  private Integer idRuta;
  
  @Column(name = "nombre_ruta")
  private String nombreRuta;
  
  @Column(name = "califica_cliente")
  private Integer calificaCliente;
  
  @Column(name = "califica_despachador")
  private Integer calificaDespachador;
  
  @Column(name = "id_factura")
  private BigInteger idFactura;
  
  @Column(name = "estatus_factura")
  private String estatusFactura;
  
  @Column(name = "id_automovil")
  private BigInteger idAutomovil;
  
  @Column(name = "marca_automovil")
  private String marcaAutomovil;
  
  @Column(name = "modelo_automovil")
  private String modeloAutomovil;
  
  @Column(name = "anio_automovil")
  private String anioAutomovil;
  
  @Column(name = "color_automovil")
  private String colorAutomovil;
  
  @Column(name = "placas_automovil")
  private String placasAutomovil;

  @Column(name = "id_despachador")
  private Long idDespachador;

  @Column(name = "nombre_despachador")
  private String nombreDespachador;

  @Column(name = "apellido_paterno_despachador")
  private String apellidoPaternoDespachador;

  @Column(name = "apellido_materno_despachador")
  private String apellidoMaternoDespachador;


  @Column(name = "estatus_cliente")
  private Integer estatusCliente;

  @Column(name = "fecha_creacion_cliente")
  private Calendar fechaCreacionCliente;

  @Column(name = "fecha_baja_cliente")
  private Calendar fechaBajaCliente;

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
    return idPedido;
  }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
    this.idPedido = idPedido;
  }

    /**
     *
     * @return
     */
    public String getUuid() {
    return uuid;
  }

    /**
     *
     * @param uuid
     */
    public void setUuid(String uuid) {
    this.uuid = uuid;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaPedido() {
    return fechaPedido;
  }

    /**
     *
     * @param fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido) {
    this.fechaPedido = fechaPedido;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaHoraInicio() {
    return fechaHoraInicio;
  }

    /**
     *
     * @param fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio) {
    this.fechaHoraInicio = fechaHoraInicio;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaHoraFin() {
    return fechaHoraFin;
  }

    /**
     *
     * @param fechaHoraFin
     */
    public void setFechaHoraFin(Calendar fechaHoraFin) {
    this.fechaHoraFin = fechaHoraFin;
  }

    /**
     *
     * @return
     */
    public Integer getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public Boolean getHasPresion() {
    return hasPresion;
  }

    /**
     *
     * @param hasPresion
     */
    public void setHasPresion(Boolean hasPresion) {
    this.hasPresion = hasPresion;
  }

    /**
     *
     * @return
     */
    public Boolean getHasLavado() {
    return hasLavado;
  }

    /**
     *
     * @param hasLavado
     */
    public void setHasLavado(Boolean hasLavado) {
    this.hasLavado = hasLavado;
  }

    /**
     *
     * @return
     */
    public Integer getIdTipoPedido() {
    return idTipoPedido;
  }

    /**
     *
     * @param idTipoPedido
     */
    public void setIdTipoPedido(Integer idTipoPedido) {
    this.idTipoPedido = idTipoPedido;
  }

    /**
     *
     * @return
     */
    public Integer getIdCombustible() {
    return idCombustible;
  }

    /**
     *
     * @param idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
    this.idCombustible = idCombustible;
  }

    /**
     *
     * @return
     */
    public Double getLitrosSolicitados() {
    return litrosSolicitados;
  }

    /**
     *
     * @param litrosSolicitados
     */
    public void setLitrosSolicitados(Double litrosSolicitados) {
    this.litrosSolicitados = litrosSolicitados;
  }

    /**
     *
     * @return
     */
    public Double getLitrosDespachados() {
    return litrosDespachados;
  }

    /**
     *
     * @param litrosDespachados
     */
    public void setLitrosDespachados(Double litrosDespachados) {
    this.litrosDespachados = litrosDespachados;
  }

    /**
     *
     * @return
     */
    public Double getLitroPrecio() {
    return litroPrecio;
  }

    /**
     *
     * @param litroPrecio
     */
    public void setLitroPrecio(Double litroPrecio) {
    this.litroPrecio = litroPrecio;
  }

    /**
     *
     * @return
     */
    public Double getServicioPrecio() {
    return servicioPrecio;
  }

    /**
     *
     * @param servicioPrecio
     */
    public void setServicioPrecio(Double servicioPrecio) {
    this.servicioPrecio = servicioPrecio;
  }

    /**
     *
     * @return
     */
    public Double getSubtotal() {
    return subtotal;
  }

    /**
     *
     * @param subtotal
     */
    public void setSubtotal(Double subtotal) {
    this.subtotal = subtotal;
  }

    /**
     *
     * @return
     */
    public Double getDescuento() {
    return descuento;
  }

    /**
     *
     * @param descuento
     */
    public void setDescuento(Double descuento) {
    this.descuento = descuento;
  }

    /**
     *
     * @return
     */
    public Double getTotalImporte() {
    return totalImporte;
  }

    /**
     *
     * @param totalImporte
     */
    public void setTotalImporte(Double totalImporte) {
    this.totalImporte = totalImporte;
  }

    /**
     *
     * @return
     */
    public Double getIva() {
    return iva;
  }

    /**
     *
     * @param iva
     */
    public void setIva(Double iva) {
    this.iva = iva;
  }

    /**
     *
     * @return
     */
    public Double getTotalPagar() {
    return totalPagar;
  }

    /**
     *
     * @param totalPagar
     */
    public void setTotalPagar(Double totalPagar) {
    this.totalPagar = totalPagar;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public String getNombreCliente() {
    return nombreCliente;
  }

    /**
     *
     * @param nombreCliente
     */
    public void setNombreCliente(String nombreCliente) {
    this.nombreCliente = nombreCliente;
  }

    /**
     *
     * @return
     */
    public String getApellidoPaternoCliente() {
    return apellidoPaternoCliente;
  }

    /**
     *
     * @param apellidoPaternoCliente
     */
    public void setApellidoPaternoCliente(String apellidoPaternoCliente) {
    this.apellidoPaternoCliente = apellidoPaternoCliente;
  }

    /**
     *
     * @return
     */
    public String getApellidoMaternoCliente() {
    return apellidoMaternoCliente;
  }

    /**
     *
     * @param apellidoMaternoCliente
     */
    public void setApellidoMaternoCliente(String apellidoMaternoCliente) {
    this.apellidoMaternoCliente = apellidoMaternoCliente;
  }

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public String getNombreEstado() {
    return nombreEstado;
  }

    /**
     *
     * @param nombreEstado
     */
    public void setNombreEstado(String nombreEstado) {
    this.nombreEstado = nombreEstado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }

    /**
     *
     * @return
     */
    public String getNombreMunicipio() {
    return nombreMunicipio;
  }

    /**
     *
     * @param nombreMunicipio
     */
    public void setNombreMunicipio(String nombreMunicipio) {
    this.nombreMunicipio = nombreMunicipio;
  }

    /**
     *
     * @return
     */
    public String getCpDireccion() {
    return cpDireccion;
  }

    /**
     *
     * @param cpDireccion
     */
    public void setCpDireccion(String cpDireccion) {
    this.cpDireccion = cpDireccion;
  }

    /**
     *
     * @return
     */
    public String getCalleDireccion() {
    return calleDireccion;
  }

    /**
     *
     * @param calleDireccion
     */
    public void setCalleDireccion(String calleDireccion) {
    this.calleDireccion = calleDireccion;
  }

    /**
     *
     * @return
     */
    public String getColoniaDireccion() {
    return coloniaDireccion;
  }

    /**
     *
     * @param coloniaDireccion
     */
    public void setColoniaDireccion(String coloniaDireccion) {
    this.coloniaDireccion = coloniaDireccion;
  }

    /**
     *
     * @return
     */
    public Long getIdPromocion() {
    return idPromocion;
  }

    /**
     *
     * @param idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
    this.idPromocion = idPromocion;
  }

    /**
     *
     * @return
     */
    public String getCodigoPromocion() {
    return codigoPromocion;
  }

    /**
     *
     * @param codigoPromocion
     */
    public void setCodigoPromocion(String codigoPromocion) {
    this.codigoPromocion = codigoPromocion;
  }

    /**
     *
     * @return
     */
    public Integer getIdRuta() {
    return idRuta;
  }

    /**
     *
     * @param idRuta
     */
    public void setIdRuta(Integer idRuta) {
    this.idRuta = idRuta;
  }

    /**
     *
     * @return
     */
    public String getNombreRuta() {
    return nombreRuta;
  }

    /**
     *
     * @param nombreRuta
     */
    public void setNombreRuta(String nombreRuta) {
    this.nombreRuta = nombreRuta;
  }

    /**
     *
     * @return
     */
    public Integer getCalificaCliente() {
    return calificaCliente;
  }

    /**
     *
     * @param calificaCliente
     */
    public void setCalificaCliente(Integer calificaCliente) {
    this.calificaCliente = calificaCliente;
  }

    /**
     *
     * @return
     */
    public Integer getCalificaDespachador() {
    return calificaDespachador;
  }

    /**
     *
     * @param calificaDespachador
     */
    public void setCalificaDespachador(Integer calificaDespachador) {
    this.calificaDespachador = calificaDespachador;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdFactura() {
    return idFactura;
  }

    /**
     *
     * @param idFactura
     */
    public void setIdFactura(BigInteger idFactura) {
    this.idFactura = idFactura;
  }

    /**
     *
     * @return
     */
    public String getEstatusFactura() {
    return estatusFactura;
  }

    /**
     *
     * @param estatusFactura
     */
    public void setEstatusFactura(String estatusFactura) {
    this.estatusFactura = estatusFactura;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdAutomovil() {
    return idAutomovil;
  }

    /**
     *
     * @param idAutomovil
     */
    public void setIdAutomovil(BigInteger idAutomovil) {
    this.idAutomovil = idAutomovil;
  }

    /**
     *
     * @return
     */
    public String getMarcaAutomovil() {
    return marcaAutomovil;
  }

    /**
     *
     * @param marcaAutomovil
     */
    public void setMarcaAutomovil(String marcaAutomovil) {
    this.marcaAutomovil = marcaAutomovil;
  }

    /**
     *
     * @return
     */
    public String getModeloAutomovil() {
    return modeloAutomovil;
  }

    /**
     *
     * @param modeloAutomovil
     */
    public void setModeloAutomovil(String modeloAutomovil) {
    this.modeloAutomovil = modeloAutomovil;
  }

    /**
     *
     * @return
     */
    public String getAnioAutomovil() {
    return anioAutomovil;
  }

    /**
     *
     * @param anioAutomovil
     */
    public void setAnioAutomovil(String anioAutomovil) {
    this.anioAutomovil = anioAutomovil;
  }

    /**
     *
     * @return
     */
    public String getColorAutomovil() {
    return colorAutomovil;
  }

    /**
     *
     * @param colorAutomovil
     */
    public void setColorAutomovil(String colorAutomovil) {
    this.colorAutomovil = colorAutomovil;
  }

    /**
     *
     * @return
     */
    public String getPlacasAutomovil() {
    return placasAutomovil;
  }

    /**
     *
     * @param placasAutomovil
     */
    public void setPlacasAutomovil(String placasAutomovil) {
    this.placasAutomovil = placasAutomovil;
  }

    /**
     *
     * @return
     */
    public Long getIdDespachador() {
    return idDespachador;
  }

    /**
     *
     * @param idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
    this.idDespachador = idDespachador;
  }

    /**
     *
     * @return
     */
    public String getNombreDespachador() {
    return nombreDespachador;
  }

    /**
     *
     * @param nombreDespachador
     */
    public void setNombreDespachador(String nombreDespachador) {
    this.nombreDespachador = nombreDespachador;
  }

    /**
     *
     * @return
     */
    public String getApellidoPaternoDespachador() {
    return apellidoPaternoDespachador;
  }

    /**
     *
     * @param apellidoPaternoDespachador
     */
    public void setApellidoPaternoDespachador(String apellidoPaternoDespachador) {
    this.apellidoPaternoDespachador = apellidoPaternoDespachador;
  }

    /**
     *
     * @return
     */
    public String getApellidoMaternoDespachador() {
    return apellidoMaternoDespachador;
  }

    /**
     *
     * @param apellidoMaternoDespachador
     */
    public void setApellidoMaternoDespachador(String apellidoMaternoDespachador) {
    this.apellidoMaternoDespachador = apellidoMaternoDespachador;
  }

    /**
     *
     * @return
     */
    public Integer getEstatusCliente() {
    return estatusCliente;
  }

    /**
     *
     * @param estatusCliente
     */
    public void setEstatusCliente(Integer estatusCliente) {
    this.estatusCliente = estatusCliente;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacionCliente() {
    return fechaCreacionCliente;
  }

    /**
     *
     * @param fechaCreacionCliente
     */
    public void setFechaCreacionCliente(Calendar fechaCreacionCliente) {
    this.fechaCreacionCliente = fechaCreacionCliente;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaBajaCliente() {
    return fechaBajaCliente;
  }

    /**
     *
     * @param fechaBajaCliente
     */
    public void setFechaBajaCliente(Calendar fechaBajaCliente) {
    this.fechaBajaCliente = fechaBajaCliente;
  }

    /**
     *
     * @return
     */
    public Double getLitroPrecioInicio() {
    return litroPrecioInicio;
  }

    /**
     *
     * @param litroPrecioInicio
     */
    public void setLitroPrecioInicio(Double litroPrecioInicio) {
    this.litroPrecioInicio = litroPrecioInicio;
  }
}