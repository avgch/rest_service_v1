package com.enermex.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author José Antonio González Hernández
 * 
 * Configuraciones de BlobStorage de Azure
 * 
 * Fecha de creación: 29 de enero de 2020
 */
@ConfigurationProperties("azure.blob")
@Component
public class AzureConfiguration {
  private String endpoint;
  private String sasToken;
  private String usuarioFoto;

  /**
   * Url del almacenamiento tipo Blob Storage en Azure
   * @return
   */
  public String getEndpoint() {
    return endpoint;
  }

    /**
     *
     * @param endpoint
     */
    public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  /**
   * Sas Token de seguridad del almacenamiento en Azure
   * @return
   */
  public String getSasToken() {
    return sasToken;
  }

    /**
     *
     * @param sasToken
     */
    public void setSasToken(String sasToken) {
    this.sasToken = sasToken;
  }

  /**
   * Nombre del contenedor de fotos de usuarios en Azure
   * @return
   */
  public String getUsuarioFoto() {
    return usuarioFoto;
  }

    /**
     *
     * @param usuarioFoto
     */
    public void setUsuarioFoto(String usuarioFoto) {
    this.usuarioFoto = usuarioFoto;
  }
}
