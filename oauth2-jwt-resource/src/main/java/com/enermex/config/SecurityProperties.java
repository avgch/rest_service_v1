package com.enermex.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;

/**
 *
 * @author abraham
 */
@ConfigurationProperties("security")
public class SecurityProperties {

    private JwtProperties jwt;

    /**
     *
     * @return
     */
    public JwtProperties getJwt() {
        return jwt;
    }

    /**
     *
     * @param jwt
     */
    public void setJwt(JwtProperties jwt) {
        this.jwt = jwt;
    }

    /**
     *
     */
    public static class JwtProperties {

        private Resource publicKey;

        /**
         *
         * @return
         */
        public Resource getPublicKey() {
            return publicKey;
        }

        /**
         *
         * @param publicKey
         */
        public void setPublicKey(Resource publicKey) {
            this.publicKey = publicKey;
        }
    }

}
