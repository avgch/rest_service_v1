package com.enermex.dto;



import java.util.List;



import com.enermex.dto.NotificaDespachadorDto;

import com.enermex.dto.AlcaldiaEstadoNotiDto;

/**
 * Nombre de clase
 * @author abraham
 */
public class NotificacionDto {





	  private String status;



	  private String mensaje;



	  private String titulo;



	  private String envioTipo;



	  private String esCliente;



	  private String esDespachador;



	  private String esTodosClientes;



	  private String creadoPor;

	 

	  List<AlcaldiaEstadoNotiDto> areasANotificas;

	  

	  List<NotificaDespachadorDto> notificaDespachadores;

    /**
     * getStatus
     * @return status
     */
    public String getStatus() {



			return status;



		}

    /**
     * 
     * @param status
     * setStatus
     */
    public void setStatus(String status) {



			this.status = status;



		}

    /**
     * getMensaje
     * @return mensaje
     */
    public String getMensaje() {



			return mensaje;



		}

    /**
     * 
     * @param mensaje
     * setMensaje
     */
    public void setMensaje(String mensaje) {



			this.mensaje = mensaje;



		}

    /**
     * getTitulo
     * @return titulo
     */
    public String getTitulo() {



			return titulo;



		}

    /**
     * 
     * @param titulo
     * setTitulo
     */
    public void setTitulo(String titulo) {



			this.titulo = titulo;



		}

    /**
     * getEnvioTipo
     * @return envioTipo
     */
    public String getEnvioTipo() {



			return envioTipo;



		}

    /**
     * 
     * @param envioTipo
     * setEnvioTipo
     */
    public void setEnvioTipo(String envioTipo) {



			this.envioTipo = envioTipo;



		}

    /**
     * getEsCliente
     * @return esCliente
     */
    public String getEsCliente() {



			return esCliente;



		}

    /**
     * 
     * @param esCliente
     * setEsCliente
     */
    public void setEsCliente(String esCliente) {



			this.esCliente = esCliente;



		}

    /**
     * getEsDespachador
     * @return esDespachador
     */
    public String getEsDespachador() {



			return esDespachador;



		}

    /**
     * 
     * @param esDespachador
     * setEsDespachador
     */
    public void setEsDespachador(String esDespachador) {



			this.esDespachador = esDespachador;



		}

    /**
     * getEsTodosClientes
     * @return esTodosClientes
     */
    public String getEsTodosClientes() {



			return esTodosClientes;



		}

    /**
     * 
     * @param esTodosClientes
     * setEsTodosClientes
     */
    public void setEsTodosClientes(String esTodosClientes) {



			this.esTodosClientes = esTodosClientes;



		}

    /**
     * getCreadoPor
     * @return creadorPor
     */
    public String getCreadoPor() {

			return creadoPor;

		}

    /**
     * 
     * @param creadoPor
     * setCreadoPor
     */
    public void setCreadoPor(String creadoPor) {

			this.creadoPor = creadoPor;

		}

    /**
     * getAreasANotificas
     * @return areasANotificar
     */
    public List<AlcaldiaEstadoNotiDto> getAreasANotificas() {

			return areasANotificas;

		}

    /**
     * 
     * @param areasANotificas
     * setAreasANotificas
     */
    public void setAreasANotificas(List<AlcaldiaEstadoNotiDto> areasANotificas) {

			this.areasANotificas = areasANotificas;

		}

    /**
     * getNotificaDespachadores
     * @return notificaDespachadores
     */
    public List<NotificaDespachadorDto> getNotificaDespachadores() {

			return notificaDespachadores;

		}

    /**
     * 
     * @param notificaDespachadores
     * setNotificaDespachadores
     */
    public void setNotificaDespachadores(List<NotificaDespachadorDto> notificaDespachadores) {

			this.notificaDespachadores = notificaDespachadores;

		}

		



}