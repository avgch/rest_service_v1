package com.enermex.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.DireccionCliente;;

/**
 * Nombre de clase
 * @author abraham
 */
public class PedidoDetailDto {

	private AutomovilCliente automovil;
	private BigInteger idPedido;
	private DireccionCliente direccion;
	private String ruta;
	private String nombreDespachador;
	private Long idDespachador;
	private String litroCarga;
	private double montoCarga;
	private boolean verificaPresionNeumatico;
	private String llantasDelanterasPresion;
	private String llantasTraserasPresion;
	private Date fechaCreacion;
	private Calendar fechaPedido;
	private BigInteger idPromocion;
	private boolean cargaGas;
	private boolean lavadoAuto;
	private boolean revisionNeumaticos;
	private BigInteger idCliente;
	private Integer estatusPedido;
	private String detalle;
	private String nombre;
	private String correo;
	private String telefono;
	private int idCombustible;
	private String tiempoTraslado;
	private String distancia;
	private Calendar fechaHoraInicio;
	private Calendar fechaHoraFin;
	private String motivo;
	private String comentarioCancelacion;
	private String litrosEntregados;
	private Calendar fechaCancelacion;
	List<BigInteger> antes = new ArrayList<>();
	List<BigInteger> despues = new ArrayList<>();
	private String  latitudDespachador;
	private String  longitudDespachador;
	private String  uuid;
	private Double  totalPagar;

    /**
     *
     * @return latitudDespachador
     */ 
    public String getLatitudDespachador() {
		return latitudDespachador;
	}

    /**
     *
     * @param latitudDespachador
     * latitudDespachador
     */
    public void setLatitudDespachador(String latitudDespachador) {
		this.latitudDespachador = latitudDespachador;
	}

    /**
     *
     * @return longitudDespachador
     */
    public String getLongitudDespachador() {
		return longitudDespachador;
	}

    /**
     *
     * @param longitudDespachador
     * longitudDespachador
     */
    public void setLongitudDespachador(String longitudDespachador) {
		this.longitudDespachador = longitudDespachador;
	}

    /**
     *
     * @return antes
     */
    public List<BigInteger> getAntes() {
		return antes;
	}

    /**
     *
     * @param antes
     * antes
     */
    public void setAntes(List<BigInteger> antes) {
		this.antes = antes;
	}

    /**
     *
     * @return despues
     */
    public List<BigInteger> getDespues() {
		return despues;
	}

    /**
     *
     * @param despues
     * despues
     */
    public void setDespues(List<BigInteger> despues) {
		this.despues = despues;
	}

    /**
     *
     * @return fechaCancelacion
     */
    public Calendar getFechaCancelacion() {
		return fechaCancelacion;
	}

    /**
     *
     * @param fechaCancelacion
     * fechaCancelacion
     */
    public void setFechaCancelacion(Calendar fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

    /**
     *
     * @return automovil
     */
    public AutomovilCliente getAutomovil() {
		return automovil;
	}

    /**
     *
     * @param automovil
     * automovil
     */
    public void setAutomovil(AutomovilCliente automovil) {
		this.automovil = automovil;
	}

    /**
     *
     * @return idPedido
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     * idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     *
     * @return direccion
     */
    public DireccionCliente getDireccion() {
		return direccion;
	}

    /**
     *
     * @param direccion
     * direccion
     */
    public void setDireccion(DireccionCliente direccion) {
		this.direccion = direccion;
	}

    /**
     *
     * @return ruta
     */
    public String getRuta() {
		return ruta;
	}

    /**
     *
     * @param ruta
     * ruta
     */
    public void setRuta(String ruta) {
		this.ruta = ruta;
	}

    /**
     *
     * @return nombreDespachador
     */
    public String getNombreDespachador() {
		return nombreDespachador;
	}

    /**
     *
     * @param nombreDespachador
     * nombreDespachador
     */
    public void setNombreDespachador(String nombreDespachador) {
		this.nombreDespachador = nombreDespachador;
	}

    /**
     *
     * @return montoCarga
     */
    public String getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     * litroCarga
     */
    public void setLitroCarga(String litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     *
     * @return montoCarga
     */
    public double getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     * montoCarga
     */
    public void setMontoCarga(double montoCarga) {
		this.montoCarga = montoCarga;
	}

    /**
     *
     * @return verificaPresionNeumatico
     */
    public boolean isVerificaPresionNeumatico() {
		return verificaPresionNeumatico;
	}

    /**
     *
     * @param verificaPresionNeumatico
     * verificaPresionNeumatico
     */
    public void setVerificaPresionNeumatico(boolean verificaPresionNeumatico) {
		this.verificaPresionNeumatico = verificaPresionNeumatico;
	}

    /**
     *
     * @return llantasDelanterasPresion
     */
    public String getLlantasDelanterasPresion() {
		return llantasDelanterasPresion;
	}

    /**
     *
     * @param llantasDelanterasPresion
     * llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
		this.llantasDelanterasPresion = llantasDelanterasPresion;
	}

    /**
     *
     * @return llantasTraserasPresion
     */
    public String getLlantasTraserasPresion() {
		return llantasTraserasPresion;
	}

    /**
     *
     * @param llantasTraserasPresion
     * llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion) {
		this.llantasTraserasPresion = llantasTraserasPresion;
	}

    /**
     *
     * @return fechaCreacion
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return fechaPedido
     */
    public Calendar getFechaPedido() {
		return fechaPedido;
	}

    /**
     *
     * @param fechaPedido
     * fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

    /**
     *
     * @return idPromocion
     */
    public BigInteger getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     * idPromocion
     */
    public void setIdPromocion(BigInteger idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     *
     * @return cargaGas
     */
    public boolean isCargaGas() {
		return cargaGas;
	}

    /**
     *
     * @param cargaGas
     * cargaGas
     */
    public void setCargaGas(boolean cargaGas) {
		this.cargaGas = cargaGas;
	}

    /**
     *
     * @return lavadoAuto
     */
    public boolean isLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     * lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     *
     * @return revisionNeumaticos
     */
    public boolean isRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     * revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     *
     * @return idCliente
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     * idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return estatusPedido
     */
    public Integer getEstatusPedido() {
		return estatusPedido;
	}

    /**
     *
     * @param estatusPedido
     * estatusPedido
     */
    public void setEstatusPedido(Integer estatusPedido) {
		this.estatusPedido = estatusPedido;
	}

    /**
     *
     * @return detalle
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     *
     * @param detalle
     * detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return correo
     */
    public String getCorreo() {
		return correo;
	}

    /**
     *
     * @param correo
     * correo
     */
    public void setCorreo(String correo) {
		this.correo = correo;
	}

    /**
     *
     * @return telefono
     */
    public String getTelefono() {
		return telefono;
	}

    /**
     *
     * @param telefono
     * telefono
     */
    public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

    /**
     *
     * @return idCombustible
     */
    public int getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(int idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return idDespachador
     */
    public Long getIdDespachador() {
		return idDespachador;
	}

    /**
     *
     * @param idDespachador
     * idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
		this.idDespachador = idDespachador;
	}

    /**
     *
     * @return tiempoTraslado
     */
    public String getTiempoTraslado() {
		return tiempoTraslado;
	}

    /**
     *
     * @param tiempoTraslado
     * tiempoTraslado
     */
    public void setTiempoTraslado(String tiempoTraslado) {
		this.tiempoTraslado = tiempoTraslado;
	}

    /**
     *
     * @return distancia
     */
    public String getDistancia() {
		return distancia;
	}

    /**
     *
     * @param distancia
     * distancia
     */
    public void setDistancia(String distancia) {
		this.distancia = distancia;
	}

    /**
     *
     * @return fechaHoraInicio
     */
    public Calendar getFechaHoraInicio() {
		return fechaHoraInicio;
	}

    /**
     *
     * @param fechaHoraInicio
     * fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

    /**
     *
     * @return fechaHoraFin
     */
    public Calendar getFechaHoraFin() {
		return fechaHoraFin;
	}

    /**
     *
     * @param fechaHoraFin
     * fechaHoraFin
     */
    public void setFechaHoraFin(Calendar fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

    /**
     *
     * @return motivo
     */
    public String getMotivo() {
		return motivo;
	}

    /**
     *
     * @param motivo
     * motivo
     */
    public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

    /**
     *
     * @return comentarioCancelacion
     */
    public String getComentarioCancelacion() {
		return comentarioCancelacion;
	}

    /**
     *
     * @param comentarioCancelacion
     * comentarioCancelacion
     */
    public void setComentarioCancelacion(String comentarioCancelacion) {
		this.comentarioCancelacion = comentarioCancelacion;
	}

    /**
     *
     * @return litrosEntregados
     */
    public String getLitrosEntregados() {
		return litrosEntregados;
	}

    /**
     *
     * @param litrosEntregados
     * litrosEntregados
     */
    public void setLitrosEntregados(String litrosEntregados) {
		this.litrosEntregados = litrosEntregados;
	}

    /**
     *  totalPagar
     * @return totalPagar
     */
    public Double getTotalPagar() {
		return totalPagar;
	}

    /**
     *
     * @param totalPagar
     * totalPagar
     */
    public void setTotalPagar(Double totalPagar) {
		this.totalPagar = totalPagar;
	}

    /**
     * uuid
     * @return uuid
     */
    public String getUuid() {
		return uuid;
	}

    /**
     *
     * @param uuid
     * uuid
     */
    public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
    

    
	
}
