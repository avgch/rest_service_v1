/*##############################################################################
# Nombre del Programa : ServiciosDto.java                                          #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/



package com.enermex.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * Nombre de clase
 * @author abraham
 */
@Component
@Entity
@Table(name ="servicios")
public class ServiciosDto {
	
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_servicio")
	private Integer id;
	
	@Column(name = "servicio")
	private String servicio;
	
	
	@Column(name = "precio")
	private float precio;

    /**
     * Constructor de clase
     */
    public ServiciosDto() {
		
		
	}
	
    /**
     *
     * @return id
     */
    public Integer getId() {
		return id;
	}

    /**
     *
     * @param id
     * id
     */
    public void setId(Integer id) {
		this.id = id;
	}

    /**
     *
     * @return servicio
     */
    public String getServicio() {
		return servicio;
	}

    /**
     *
     * @param servicio
     * servicio
     */
    public void setServicio(String servicio) {
		this.servicio = servicio;
	}

    /**
     *
     * @return precio
     */
    public float getPrecio() {
		return precio;
	}

    /**
     *
     * @param precio
     * precio
     */
    public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	
	
	
	

}
