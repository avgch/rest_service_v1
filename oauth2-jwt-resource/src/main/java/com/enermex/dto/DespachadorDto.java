package com.enermex.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.enermex.modelo.DireccionCliente;

/**
 * DTO de tabla Despachador
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 27 de diciembre de 2019
 */
@Entity
@Table(name="despachador")
public class DespachadorDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_despachador")
    private Long idDespachador;

    @Column(name="nombre")
    private String nombre;

    @Column(name="fecha_nac")
    private Date fechaNac;

    @Column(name="fecha_ingreso")
    private Date fechaIngreso;

    @Column(name="correo")
    private String correo;

    @Column(name="telefono")
    private String telefono;

    @Column(name="status")
    private Integer status;

    @Column(name="apellido_paterno")
    private String apellidoPaterno;

    @Column(name="apellido_materno")
    private String apellidoMaterno;
    
    // REQ: Kevin Villa
    // Utilizar una nomenclatura de identificación; postpuesto
    @Column(name="internal_id")
    private String internalId;

    @Column(name="estado_civil")
    private String estadoCivil;

    @Column(name="genero")
    private String genero;

    // REQ: Abraham Vargas
    // Al ingresar un despachador, debe especificarse su dirección
    @JoinTable(
        name = "despachador_direccion",
        joinColumns = @JoinColumn(name = "id_despachador"),
        inverseJoinColumns = @JoinColumn(name="id_direccion")
    )
    @ManyToMany(cascade = CascadeType.ALL)
    private List<DireccionCliente> direcciones;

    /**
     * getIdDespachador
     * @return idDespachador
     */
    public Long getIdDespachador() {
        return idDespachador;
    }

    /**
     * 
     * @param idDespachador
     * setIdDespachador
     */
    public void setIdDespachador(Long idDespachador) {
        this.idDespachador = idDespachador;
    }

    /**
     * getNombre
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param nombre
     * setNombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * getFechaNac
     * @return fechaNac
     */
    public Date getFechaNac() {
        return fechaNac;
    }

    /**
     * 
     * @param fechaNac
     * setFechaNac
     */
    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    /**
     * getFechaIngreso
     * @return fechaIngreso
     */
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * 
     * @param fechaIngreso
     * setFechaIngreso
     */
    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * getCorreo
     * @return correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * 
     * @param correo
     * setCorreo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * getTelefono
     * @return telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * 
     * @param telefono
     * setTelefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * getStatus
     * @return status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     * setStatus
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * getApellidoPaterno
     * @return apellidoPaterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * 
     * @param apellidoPaterno
     * setApellidoPaterno
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * getApellidoMaterno
     * @return apellidoMaterno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * 
     * @param apellidoMaterno
     * setApellidoMaterno
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
    
    /**
     * getInternalId
     * @return internalId
     */
    public String getInternalId() {
        return internalId;
    }
    
    /**
     * 
     * @param internalId
     * setInternalId
     */
    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    /**
     * getEstadoCivil
     * @return estadoCivil
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }
    
    /**
     * 
     * @param estadoCivil
     * setEstadoCivil
     */
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
    
    /**
     * getGenero
     * @return genero
     */
    public String getGenero() {
        return genero;
    }
    
    /**
     * 
     * @param genero
     * setGenero
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    /**
     * getDirecciones
     * @return direcciones
     */
    public List<DireccionCliente> getDirecciones() {
        return direcciones;
    }

    /**
     * 
     * @param direcciones
     * setDirecciones
     */
    public void setDirecciones(List<DireccionCliente> direcciones) {
        this.direcciones = direcciones;
    }
}