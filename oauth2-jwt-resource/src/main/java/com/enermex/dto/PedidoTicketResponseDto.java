package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class PedidoTicketResponseDto {

	
	private boolean  esPorLitros;
	private Double   litroCarga;
	private Double   costoServicio;
	private Double   costoPorLitro;
	private Double   servicioGasolina;
	private Double   precioLavadoAuto;
	private Double   precioRevisionLlantas;
	private Integer  idCombustible;
	private boolean  lavadoAuto;
	private boolean  revisionNeumaticos;
	private Integer  tipoPedidoP;
	private Double   totalImporte;
	private Double   totalConDescuento;
	private Double   iva;
	private Double   subTotal;
	private Double   totalAPagar;
	private String   alcMun;
	
    /**
     *
     * @return esPorLitros
     */
    public boolean isEsPorLitros() {
		return esPorLitros;
	}

    /**
     *
     * @param esPorLitros
     * esPorLitros
     */
    public void setEsPorLitros(boolean esPorLitros) {
		this.esPorLitros = esPorLitros;
	}

    /**
     *
     * @return litroCarga
     */
    public Double getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     * litroCarga
     */
    public void setLitroCarga(Double litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     *
     * @return costoServicio
     */
    public Double getCostoServicio() {
		return costoServicio;
	}

    /**
     *
     * @param costoServicio
     * costoServicio
     */
    public void setCostoServicio(Double costoServicio) {
		this.costoServicio = costoServicio;
	}

    /**
     *
     * @return costoPorLitro
     */
    public Double getCostoPorLitro() {
		return costoPorLitro;
	}

    /**
     *
     * @param costoPorLitro
     * costoPorLitro
     */
    public void setCostoPorLitro(Double costoPorLitro) {
		this.costoPorLitro = costoPorLitro;
	}

    /**
     *
     * @return precioLavadoAuto
     */
    public Double getPrecioLavadoAuto() {
		return precioLavadoAuto;
	}

    /**
     *
     * @param precioLavadoAuto
     * precioLavadoAuto
     */
    public void setPrecioLavadoAuto(Double precioLavadoAuto) {
		this.precioLavadoAuto = precioLavadoAuto;
	}

    /**
     *
     * @return precioRevisionLlantas
     */
    public Double getPrecioRevisionLlantas() {
		return precioRevisionLlantas;
	}

    /**
     *
     * @param precioRevisionLlantas
     * precioRevisionLlantas
     */
    public void setPrecioRevisionLlantas(Double precioRevisionLlantas) {
		this.precioRevisionLlantas = precioRevisionLlantas;
	}

    /**
     *
     * @return idCombustible
     */ 
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return lavadoAuto
     */
    public boolean isLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     * lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     *
     * @return revisionNeumaticos
     */
    public boolean isRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     * revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     *
     * @return tipoPedidoP
     */
    public Integer getTipoPedidoP() {
		return tipoPedidoP;
	}

    /**
     *
     * @param tipoPedidoP
     * tipoPedidoP
     */
    public void setTipoPedidoP(Integer tipoPedidoP) {
		this.tipoPedidoP = tipoPedidoP;
	}

    /**
     *
     * @return totalImporte
     */
    public Double getTotalImporte() {
		return totalImporte;
	}

    /**
     *
     * @param totalImporte
     * totalImporte
     */
    public void setTotalImporte(Double totalImporte) {
		this.totalImporte = totalImporte;
	}

    /**
     *
     * @return totalConDescuento
     */
    public Double getTotalConDescuento() {
		return totalConDescuento;
	}

    /**
     *
     * @param totalConDescuento
     * totalConDescuento
     */
    public void setTotalConDescuento(Double totalConDescuento) {
		this.totalConDescuento = totalConDescuento;
	}

    /**
     *
     * @return iva
     */
    public Double getIva() {
		return iva;
	}

    /**
     *
     * @param iva
     * iva
     */
    public void setIva(Double iva) {
		this.iva = iva;
	}

    /**
     *
     * @return subTotal
     */
    public Double getSubTotal() {
		return subTotal;
	}

    /**
     *
     * @param subTotal
     * subTotal
     */
    public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

    /**
     *
     * @return totalAPagar
     */
    public Double getTotalAPagar() {
		return totalAPagar;
	}

    /**
     *
     * @param totalAPagar
     * totalAPagar
     */
    public void setTotalAPagar(Double totalAPagar) {
		this.totalAPagar = totalAPagar;
	}

    /**
     *
     * @return servicioGasolina
     */
    public Double getServicioGasolina() {
		return servicioGasolina;
	}

    /**
     *
     * @param servicioGasolina
     * servicioGasolina
     */
    public void setServicioGasolina(Double servicioGasolina) {
		this.servicioGasolina = servicioGasolina;
	}

    /**
     *
     * @return alcMun
     */
    public String getAlcMun() {
		return alcMun;
	}

    /**
     *
     * @param alcMun
     * alcMun
     */
    public void setAlcMun(String alcMun) {
		this.alcMun = alcMun;
	}
   
	
	
	
	
	
	
	
}
