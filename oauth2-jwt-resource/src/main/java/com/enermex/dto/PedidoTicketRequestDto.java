package com.enermex.dto;

import java.math.BigInteger;

/**
 * Nombre de clase
 * @author abraham
 */
public class PedidoTicketRequestDto {
	
	private BigInteger  idCliente;
	private Integer     idAutomovil;
	private boolean     esPorLitros;
	private Double      montoCarga;
	private String      litroCarga;
	private String      cp;
	private Integer     idCombustible;
	private boolean     lavadoAuto;
	private boolean     revisionNeumaticos;
	private String      fechaPedido;
	
    /**
     *
     * @return esPorLitros
     */
    public boolean isEsPorLitros() {
		return esPorLitros;
	}

    /**
     *
     * @param esPorLitros
     * esPorLitros
     */
    public void setEsPorLitros(boolean esPorLitros) {
		this.esPorLitros = esPorLitros;
	}

    /**
     *
     * @return montoCarga
     */
    public Double getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     * montoCarga
     */
    public void setMontoCarga(Double montoCarga) {
		this.montoCarga = montoCarga;
	}

    /**
     *
     * @return litroCarga
     */
    public String getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     * litroCarga
     */
    public void setLitroCarga(String litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     *
     * @return cp
     */
    public String getCp() {
		return cp;
	}

    /**
     *
     * @param cp
     * cp
     */
    public void setCp(String cp) {
		this.cp = cp;
	}

    /**
     *
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return fechaPedido
     */
    public String getFechaPedido() {
		return fechaPedido;
	}

    /**
     *
     * @param fechaPedido
     * fechaPedido
     */
    public void setFechaPedido(String fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

    /**
     *
     * @return lavadoAuto
     */
    public boolean isLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     * lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     *
     * @return revisionNeumaticos
     */ 
    public boolean isRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     * revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     *
     * @return idCliente
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     * idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return idAutomovil
     */
    public Integer getIdAutomovil() {
		return idAutomovil;
	}

    /**
     *
     * @param idAutomovil
     * idAutomovil
     */
    public void setIdAutomovil(Integer idAutomovil) {
		this.idAutomovil = idAutomovil;
	}
	
    
}
