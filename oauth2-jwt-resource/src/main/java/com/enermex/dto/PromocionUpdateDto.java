package com.enermex.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.enermex.modelo.CodigoPostalEntity;

/**
 * Nombre de clase
 * @author abraham
 */
public class PromocionUpdateDto {

	Long    idPromocion;
    String  titulo;
    Boolean clientesAlgunos;
    Boolean clientesFrecuentes;
    Boolean clientesTodos;
    Boolean planIndividual;
    Boolean planFamiliar;
    Boolean planEmpresarial;
    Boolean descuentoPorcentaje;
    Boolean descuentoMonto;
    Date    fechaInicio;
    Date    fechaFin;
    Date    horaInicio;
    Date    horaFin;
    String  montoDescuento;
    String  porcentaje;
    String         detalle;
    List<ProductoDto>     productos;
    Integer        idEstado;
    Integer        idAlcaldia;
    List<CodigoPostalEntity>  cps;
    List<ProductoDto> productosSave;
    List<CodigoPostalEntity>  cpsSave;
    Boolean estatus;
    
    /**
     * Contructor de clase
     */
    public PromocionUpdateDto(){
    	
    	productos     = new ArrayList<>();
    	cps           = new ArrayList<>();
    	productosSave = new ArrayList<>();
    	cpsSave       = new ArrayList<>();
    }
    
    /**
     *
     * @return idPromocion
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     * idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     *
     * @return titulo
     */
    public String getTitulo() {
		return titulo;
	}

    /**
     *
     * @param titulo
     * titulo
     */
    public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

    /**
     *
     * @return clientesAlgunos
     */
    public Boolean getClientesAlgunos() {
		return clientesAlgunos;
	}

    /**
     *
     * @param clientesAlgunos
     * clientesAlgunos
     */
    public void setClientesAlgunos(Boolean clientesAlgunos) {
		this.clientesAlgunos = clientesAlgunos;
	}

    /**
     *
     * @return clientesFrecuentes
     */
    public Boolean getClientesFrecuentes() {
		return clientesFrecuentes;
	}

    /**
     *
     * @param clientesFrecuentes
     * clientesFrecuentes
     */
    public void setClientesFrecuentes(Boolean clientesFrecuentes) {
		this.clientesFrecuentes = clientesFrecuentes;
	}

    /**
     *
     * @return clientesTodos
     */
    public Boolean getClientesTodos() {
		return clientesTodos;
	}

    /**
     *
     * @param clientesTodos
     * clientesTodos
     */
    public void setClientesTodos(Boolean clientesTodos) {
		this.clientesTodos = clientesTodos;
	}

    /**
     *
     * @return planIndividual
     */
    public Boolean getPlanIndividual() {
		return planIndividual;
	}

    /**
     *
     * @param planIndividual
     * planIndividual
     */
    public void setPlanIndividual(Boolean planIndividual) {
		this.planIndividual = planIndividual;
	}

    /**
     *
     * @return planFamiliar
     */
    public Boolean getPlanFamiliar() {
		return planFamiliar;
	}

    /**
     *
     * @param planFamiliar
     * planFamiliar
     */
    public void setPlanFamiliar(Boolean planFamiliar) {
		this.planFamiliar = planFamiliar;
	}

    /**
     *
     * @return planEmpresarial
     */
    public Boolean getPlanEmpresarial() {
		return planEmpresarial;
	}

    /**
     *
     * @param planEmpresarial
     * planEmpresarial
     */
    public void setPlanEmpresarial(Boolean planEmpresarial) {
		this.planEmpresarial = planEmpresarial;
	}

    /**
     *
     * @return descuentoPorcentaje
     */
    public Boolean getDescuentoPorcentaje() {
		return descuentoPorcentaje;
	}

    /**
     *
     * @param descuentoPorcentaje
     * descuentoPorcentaje
     */
    public void setDescuentoPorcentaje(Boolean descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}

    /**
     *
     * @return descuentoMonto
     */
    public Boolean getDescuentoMonto() {
		return descuentoMonto;
	}

    /**
     *
     * @param descuentoMonto
     * descuentoMonto
     */
    public void setDescuentoMonto(Boolean descuentoMonto) {
		this.descuentoMonto = descuentoMonto;
	}

    /**
     *
     * @return fechaInicio
     */
    public Date getFechaInicio() {
		return fechaInicio;
	}

    /**
     *
     * @param fechaInicio
     * fechaInicio
     */
    public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

    /**
     *
     * @return fechaFin
     */
    public Date getFechaFin() {
		return fechaFin;
	}

    /**
     *
     * @param fechaFin
     * fechaFin
     */
    public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

    /**
     *
     * @return horaInicio
     */
    public Date getHoraInicio() {
		return horaInicio;
	}

    /**
     *
     * @param horaInicio
     * horaInicio
     */
    public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

    /**
     *
     * @return horaFin
     */
    public Date getHoraFin() {
		return horaFin;
	}

    /**
     *
     * @param horaFin
     * horaFin
     */
    public void setHoraFin(Date horaFin) {
		this.horaFin = horaFin;
	}

    /**
     *
     * @return montoDescuento
     */
    public String getMontoDescuento() {
		return montoDescuento;
	}

    /**
     *
     * @param montoDescuento
     * montoDescuento
     */
    public void setMontoDescuento(String montoDescuento) {
		this.montoDescuento = montoDescuento;
	}

    /**
     *
     * @return porcentaje
     */
    public String getPorcentaje() {
		return porcentaje;
	}

    /**
     *
     * @param porcentaje
     * porcentaje
     */
    public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}

    /**
     *
     * @return detalle
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     *
     * @param detalle
     * detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

    /**
     *
     * @return productos
     */
    public List<ProductoDto> getProductos() {
		return productos;
	}

    /**
     *
     * @param productos
     * productos
     */
    public void setProductos(List<ProductoDto> productos) {
		this.productos = productos;
	}

    /**
     *
     * @return idEstado
     */
    public Integer getIdEstado() {
		return idEstado;
	}

    /**
     *
     * @param idEstado
     * idEstado
     */
    public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

    /**
     *
     * @return idAlcaldia
     */
    public Integer getIdAlcaldia() {
		return idAlcaldia;
	}

    /**
     *
     * @param idAlcaldia
     * idAlcaldia
     */
    public void setIdAlcaldia(Integer idAlcaldia) {
		this.idAlcaldia = idAlcaldia;
	}

    /**
     *
     * @return cps
     */
    public List<CodigoPostalEntity> getCps() {
		return cps;
	}

    /**
     *
     * @param cps
     * cps
     */
    public void setCps(List<CodigoPostalEntity> cps) {
		this.cps = cps;
	}

    /**
     *
     * @return productosSave
     */
    public List<ProductoDto> getProductosSave() {
		return productosSave;
	}

    /**
     *
     * @param productosSave
     * productosSave
     */
    public void setProductosSave(List<ProductoDto> productosSave) {
		this.productosSave = productosSave;
	}

    /**
     *
     * @return cpsSave
     */
    public List<CodigoPostalEntity> getCpsSave() {
		return cpsSave;
	}

    /**
     *
     * @param cpsSave
     * cpsSave
     */
    public void setCpsSave(List<CodigoPostalEntity> cpsSave) {
		this.cpsSave = cpsSave;
	}

    /**
     *
     * @return estatus
     */
    public Boolean getEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}
    
    
}
