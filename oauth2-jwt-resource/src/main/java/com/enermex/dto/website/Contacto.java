package com.enermex.dto.website;

/**
 *
 * @author abraham
 */
public class Contacto {
  private String  correo;
  private String  nombre;
  private String  mensaje;
  private Integer formulario;

    /**
     *
     * @return
     */
    public String getCorreo() {
    return correo;
  }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
    this.correo = correo;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public String getMensaje() {
    return mensaje;
  }

    /**
     *
     * @param mensaje
     */
    public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }

    /**
     *
     * @return
     */
    public Integer getFormulario() {
    return formulario;
  }

    /**
     *
     * @param formulario
     */
    public void setFormulario(Integer formulario) {
    this.formulario = formulario;
  }  
}
