package com.enermex.dto.website;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author abraham
 */
public class WebsiteDto {

    /**
     *
     * @param json
     * @return
     */
    public static WebsiteDto fromJsonString(String json) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      return objectMapper.readValue(json, WebsiteDto.class);
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

    /**
     *
     * @param website
     * @return
     */
    public static String toJsonString(WebsiteDto website) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      return objectMapper.writeValueAsString(website);
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }
    
  private String iosUrl;
  private String androidUrl;

  // Introducción
  private String introTitle;
  private String introContent;

  // Servicios
  private String combustible;
  private String lavado;
  private String presion;

  // Beneficios
  private String b1Title;
  private String b1Subtitle;
  private String b1Content;
  private String b2Title;
  private String b2Subtitle;
  private String b2Content;
  private String b3Title;
  private String b3Subtitle;
  private String b3Content;
  private String b4Title;
  private String b4Subtitle;
  private String b4Content;
  private String b5Title;
  private String b5Subtitle;
  private String b5Content;

  // Planes
  private String planIndividual;
  private String planFamiliar;
  private String planEmpresarial;

  // Correo de contacto
  private String contacto;

  // Preguntas frecuentes
  ArrayList<Faq> faqs = new ArrayList<Faq>();

  // Rutas activas
  ArrayList<Ruta> rutas = new ArrayList<Ruta>();

    /**
     *
     */
    public static class Faq {
    private String pregunta;
    private String respuesta;

        /**
         *
         * @return
         */
        public String getPregunta() {
      return pregunta;
    }

        /**
         *
         * @param pregunta
         */
        public void setPregunta(String pregunta) {
      this.pregunta = pregunta;
    }

      /**
       *
       * @return
       */
      public String getRespuesta() {
      return respuesta;
    }

        /**
         *
         * @param respuesta
         */
        public void setRespuesta(String respuesta) {
      this.respuesta = respuesta;
    }
  }

    /**
     *
     */
    public static class Ruta {
    private String nombre;
    private String lat;
    private String lng;
    private List<Cobertura> cobertura;

      /**
       *
       * @return
       */
      public String getNombre() {
      return nombre;
    }

      /**
       *
       * @param nombre
       */
      public void setNombre(String nombre) {
      this.nombre = nombre;
    }

      /**
       *
       * @return
       */
      public String getLat() {
      return lat;
    }

      /**
       *
       * @param lat
       */
      public void setLat(String lat) {
      this.lat = lat;
    }

      /**
       *
       * @return
       */
      public String getLng() {
      return lng;
    }

        /**
         *
         * @param lng
         */
        public void setLng(String lng) {
      this.lng = lng;
    }

      /**
       *
       * @return
       */
      public List<Cobertura> getCobertura() {
      return cobertura;
    }

        /**
         *
         * @param cobertura
         */
        public void setCobertura(List<Cobertura> cobertura) {
      this.cobertura = cobertura;
    }
  }
  
    /**
     *
     */
    public static class Cobertura {

      /**
       *
       */
      public int idEstado;
    private String estado;

      /**
       *
       */
      public int idMunicipio;
    private String municipio;
    private List<String> cps;

        /**
         *
         * @return
         */
        public String getEstado() {
      return estado;
    }

      /**
       *
       * @param estado
       */
      public void setEstado(String estado) {
      this.estado = estado;
    }

      /**
       *
       * @return
       */
      public String getMunicipio() {
      return municipio;
    }

      /**
       *
       * @param municipio
       */
      public void setMunicipio(String municipio) {
      this.municipio = municipio;
    }

      /**
       *
       * @return
       */
      public List<String> getCps() {
      return cps;
    }

        /**
         *
         * @param cps
         */
        public void setCps(List<String> cps) {
      this.cps = cps;
    }
  }

    /**
     *
     * @return
     */
    public String getIosUrl() {
    return iosUrl;
  }

    /**
     *
     * @param iosUrl
     */
    public void setIosUrl(String iosUrl) {
    this.iosUrl = iosUrl;
  }

    /**
     *
     * @return
     */
    public String getAndroidUrl() {
    return androidUrl;
  }

    /**
     *
     * @param androidUrl
     */
    public void setAndroidUrl(String androidUrl) {
    this.androidUrl = androidUrl;
  }

    /**
     *
     * @return
     */
    public String getIntroTitle() {
    return introTitle;
  }

    /**
     *
     * @param introTitle
     */
    public void setIntroTitle(String introTitle) {
    this.introTitle = introTitle;
  }

    /**
     *
     * @return
     */
    public String getIntroContent() {
    return introContent;
  }

    /**
     *
     * @param introContent
     */
    public void setIntroContent(String introContent) {
    this.introContent = introContent;
  }

    /**
     *
     * @return
     */
    public String getCombustible() {
    return combustible;
  }

    /**
     *
     * @param combustible
     */
    public void setCombustible(String combustible) {
    this.combustible = combustible;
  }

    /**
     *
     * @return
     */
    public String getLavado() {
    return lavado;
  }

    /**
     *
     * @param lavado
     */
    public void setLavado(String lavado) {
    this.lavado = lavado;
  }

    /**
     *
     * @return
     */
    public String getPresion() {
    return presion;
  }

    /**
     *
     * @param presion
     */
    public void setPresion(String presion) {
    this.presion = presion;
  }

    /**
     *
     * @return
     */
    public String getB1Title() {
    return b1Title;
  }

    /**
     *
     * @param b1Title
     */
    public void setB1Title(String b1Title) {
    this.b1Title = b1Title;
  }

    /**
     *
     * @return
     */
    public String getB1Subtitle() {
    return b1Subtitle;
  }

    /**
     *
     * @param b1Subtitle
     */
    public void setB1Subtitle(String b1Subtitle) {
    this.b1Subtitle = b1Subtitle;
  }

    /**
     *
     * @return
     */
    public String getB1Content() {
    return b1Content;
  }

    /**
     *
     * @param b1Content
     */
    public void setB1Content(String b1Content) {
    this.b1Content = b1Content;
  }

    /**
     *
     * @return
     */
    public String getB2Title() {
    return b2Title;
  }

    /**
     *
     * @param b2Title
     */
    public void setB2Title(String b2Title) {
    this.b2Title = b2Title;
  }

    /**
     *
     * @return
     */
    public String getB2Subtitle() {
    return b2Subtitle;
  }

    /**
     *
     * @param b2Subtitle
     */
    public void setB2Subtitle(String b2Subtitle) {
    this.b2Subtitle = b2Subtitle;
  }

    /**
     *
     * @return
     */
    public String getB2Content() {
    return b2Content;
  }

    /**
     *
     * @param b2Content
     */
    public void setB2Content(String b2Content) {
    this.b2Content = b2Content;
  }

    /**
     *
     * @return
     */
    public String getB3Title() {
    return b3Title;
  }

    /**
     *
     * @param b3Title
     */
    public void setB3Title(String b3Title) {
    this.b3Title = b3Title;
  }

    /**
     *
     * @return
     */
    public String getB3Subtitle() {
    return b3Subtitle;
  }

    /**
     *
     * @param b3Subtitle
     */
    public void setB3Subtitle(String b3Subtitle) {
    this.b3Subtitle = b3Subtitle;
  }

    /**
     *
     * @return
     */
    public String getB3Content() {
    return b3Content;
  }

    /**
     *
     * @param b3Content
     */
    public void setB3Content(String b3Content) {
    this.b3Content = b3Content;
  }

    /**
     *
     * @return
     */
    public String getB4Title() {
    return b4Title;
  }

    /**
     *
     * @param b4Title
     */
    public void setB4Title(String b4Title) {
    this.b4Title = b4Title;
  }

    /**
     *
     * @return
     */
    public String getB4Subtitle() {
    return b4Subtitle;
  }

    /**
     *
     * @param b4Subtitle
     */
    public void setB4Subtitle(String b4Subtitle) {
    this.b4Subtitle = b4Subtitle;
  }

    /**
     *
     * @return
     */
    public String getB4Content() {
    return b4Content;
  }

    /**
     *
     * @param b4Content
     */
    public void setB4Content(String b4Content) {
    this.b4Content = b4Content;
  }

    /**
     *
     * @return
     */
    public String getB5Title() {
    return b5Title;
  }

    /**
     *
     * @param b5Title
     */
    public void setB5Title(String b5Title) {
    this.b5Title = b5Title;
  }

    /**
     *
     * @return
     */
    public String getB5Subtitle() {
    return b5Subtitle;
  }

    /**
     *
     * @param b5Subtitle
     */
    public void setB5Subtitle(String b5Subtitle) {
    this.b5Subtitle = b5Subtitle;
  }

    /**
     *
     * @return
     */
    public String getB5Content() {
    return b5Content;
  }

    /**
     *
     * @param b5Content
     */
    public void setB5Content(String b5Content) {
    this.b5Content = b5Content;
  }

    /**
     *
     * @return
     */
    public String getPlanIndividual() {
    return planIndividual;
  }

    /**
     *
     * @param planIndividual
     */
    public void setPlanIndividual(String planIndividual) {
    this.planIndividual = planIndividual;
  }

    /**
     *
     * @return
     */
    public String getPlanFamiliar() {
    return planFamiliar;
  }

    /**
     *
     * @param planFamiliar
     */
    public void setPlanFamiliar(String planFamiliar) {
    this.planFamiliar = planFamiliar;
  }

    /**
     *
     * @return
     */
    public String getPlanEmpresarial() {
    return planEmpresarial;
  }

    /**
     *
     * @param planEmpresarial
     */
    public void setPlanEmpresarial(String planEmpresarial) {
    this.planEmpresarial = planEmpresarial;
  }

    /**
     *
     * @return
     */
    public ArrayList<Faq> getFaqs() {
    return faqs;
  }

    /**
     *
     * @param faqs
     */
    public void setFaqs(ArrayList<Faq> faqs) {
    this.faqs = faqs;
  }

    /**
     *
     * @return
     */
    public ArrayList<Ruta> getRutas() {
    return rutas;
  }

    /**
     *
     * @param rutas
     */
    public void setRutas(ArrayList<Ruta> rutas) {
    this.rutas = rutas;
  }

    /**
     *
     * @return
     */
    public String getContacto() {
    return contacto;
  }

    /**
     *
     * @param contacto
     */
    public void setContacto(String contacto) {
    this.contacto = contacto;
  }
}