package com.enermex.dto;

/**
 *
 * @author abraham
 */
public class CancelacionEmailDto {
	
	
	private String idServicio;
	
	private String fechaServicio;
	
	private String horaServicio;
	
	private String direccion;
	
	private String Automovil;
	
	private String detalleServicio;
	
	private String fechaCancelacion;
	
	private String horaCancelacion;
	
	private String motivoCancelacion;
	
	private String comentarioCancelacion;
	
	private String montoPenalizacion;
	
    private String montoDevuelto;
    
    private boolean penalizacion;
    
    /**
     * Constuctor de clase
     */
    public CancelacionEmailDto() {}
    
    /**
    * 
    * @return idServicio
    * idServicio
    */
	public String getIdServicio() {
		return idServicio;
	}

	/**
	 * 
	 * @param idServicio
	 * idServicio
	 */
	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	/**
	 * 
	 * @return fechaServicio
	 */
	public String getFechaServicio() {
		return fechaServicio;
	}

	/**
	 * 
	 * @param fechaServicio
	 * fechaServicio
	 */
	public void setFechaServicio(String fechaServicio) {
		this.fechaServicio = fechaServicio;
	}

	/**
	 * 
	 * @return horaServicio
	 */
	public String getHoraServicio() {
		return horaServicio;
	}

	/**
	 * 
	 * @param horaServicio
	 * horaServicio
	 */
	public void setHoraServicio(String horaServicio) {
		this.horaServicio = horaServicio;
	}

	/**
	 * 
	 * @return direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * 
	 * @param direccion
	 * direccion
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * 
	 * @return Automovil
	 */
	public String getAutomovil() {
		return Automovil;
	}

	/**
	 * 
	 * @param automovil
	 * automovil
	 */
	public void setAutomovil(String automovil) {
		Automovil = automovil;
	}

	/**
	 * 
	 * @return detalleServicio
	 */
	public String getDetalleServicio() {
		return detalleServicio;
	}

	/**
	 * 
	 * @param detalleServicio
	 * detalleServicio
	 */
	public void setDetalleServicio(String detalleServicio) {
		this.detalleServicio = detalleServicio;
	}

	/**
	 * 
	 * @return fechaCancelacion
	 */
	public String getFechaCancelacion() {
		return fechaCancelacion;
	}

	/**
	 * 
	 * @param fechaCancelacion
	 * fechaCancelacion
	 */
	public void setFechaCancelacion(String fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	/**
	 * 
	 * @return horaCancelacion
	 */
	public String getHoraCancelacion() {
		return horaCancelacion;
	}

	/**
	 * 
	 * @param horaCancelacion
	 * horaCancelacion
	 */
	public void setHoraCancelacion(String horaCancelacion) {
		this.horaCancelacion = horaCancelacion;
	}

	/**
	 * 
	 * @return motivoCancelacion
	 */
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	/**
	 * 
	 * @param motivoCancelacion
	 * motivoCancelacion
	 */
	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	/**
	 * 
	 * @return comentarioCancelacion
	 */
	public String getComentarioCancelacion() {
		return comentarioCancelacion;
	}

	/**
	 * 
	 * @param comentarioCancelacion
	 * comentarioCancelacion
	 */
	public void setComentarioCancelacion(String comentarioCancelacion) {
		this.comentarioCancelacion = comentarioCancelacion;
	}
	
    /**
     * 
     * @return montoPenalizacion
     */
	public String getMontoPenalizacion() {
		return montoPenalizacion;
	}

	/**
	 * 
	 * @param montoPenalizacion
	 * montoPenalizacion
	 */
	public void setMontoPenalizacion(String montoPenalizacion) {
		this.montoPenalizacion = montoPenalizacion;
	}

	/**
	 * 
	 * @return montoDevuelto
	 */
	public String getMontoDevuelto() {
		return montoDevuelto;
	}

	/**
	 * 
	 * @param montoDevuelto
	 * montoDevuelto
	 */
	public void setMontoDevuelto(String montoDevuelto) {
		this.montoDevuelto = montoDevuelto;
	}

   /**
    * 
    * @return penalizacion
    */
	public boolean isPenalizacion() {
		return penalizacion;
	}

    /**
     * 
     * @param penalizacion
     * penalizacion
     */
	public void setPenalizacion(boolean penalizacion) {
		this.penalizacion = penalizacion;
	}
    
    
    
    
	
	
	

}
