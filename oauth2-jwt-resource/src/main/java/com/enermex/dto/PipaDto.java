package com.enermex.dto;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.enermex.modelo.CatCamara;

import com.enermex.modelo.Pipa;

/**
 * Nombre de clase
 * @author abraham
 */
public class PipaDto  extends Pipa{
	
	private Integer idPipa;

	private String nombre;

	private String numSerie;

	private String placas;

	private String estatus;

	private int usuario;

	private Date fechaCreacion;

	private String motivoBaja;

	private boolean selected;

	private boolean diesel;

	private boolean regular;

	private boolean premium;

	private String tamanoTotalTanque;

	private Double  totalPremium;

	private Double  totalRegular;

	private Double  totalDiesel;

	private Double  litrosRestantesDiesel;

	private Double  litrosRestantesRegular;

	private Double  litrosRestantesPremium;

	private Double  litrosCargarDiesel;

	private Double  litrosCargarPremium;

	private Double  litrosCargarRegular;
	
	private Double  litrosProgramadosDiesel;
	
	private Double  litrosProgramadosPremium;
	
	private Double  litrosProgramadosRegular;
	
	private Calendar fechaModificacion;
	
	private List<CatCamara> camarasSelected;
	
    /**
     * Constructor de clase
     * @param pipa
     * pipa entity
     */
    public PipaDto(Pipa pipa) {
	super();
	this.idPipa = pipa.getIdPipa();
	this.nombre = pipa.getNombre();
	this.numSerie = pipa.getNumSerie();
	this.placas = pipa.getPlacas();
	this.estatus = pipa.getEstatus();
	this.usuario = pipa.getUsuario();
	this.fechaCreacion = pipa.getFechaCreacion();
	this.motivoBaja = pipa.getMotivoBaja();
	this.selected = pipa.isSelected();
	this.diesel = pipa.isDiesel();
	this.regular = pipa.isRegular();
	this.premium = pipa.isPremium();
	this.tamanoTotalTanque = pipa.getTamanoTotalTanque();
	this.totalPremium = pipa.getTotalPremium();
	this.totalRegular = pipa.getTotalRegular();
	this.totalDiesel = pipa.getTotalDiesel();
	this.litrosRestantesDiesel = pipa.getLitrosRestantesDiesel();
	this.litrosRestantesRegular = pipa.getLitrosRestantesRegular();
	this.litrosRestantesPremium = pipa.getLitrosRestantesPremium();
	this.fechaModificacion      = pipa.getFechaModificacion();
   }
	
    /**
     * Constructor de clase
     */
    public PipaDto() {}
	
    /**
     *
     * @return camarasSelected
     */
    public List<CatCamara> getCamarasSelected() {
		return camarasSelected;
	}

    /**
     *
     * @param camarasSelected
     * camarasSelected
     */
    public void setCamarasSelected(List<CatCamara> camarasSelected) {
		this.camarasSelected = camarasSelected;
	}

    /**
     *
     * @return litrosCargarDiesel
     */
    public Double getLitrosCargarDiesel() {
		return litrosCargarDiesel;
	}

    /**
     *
     * @param litrosCargarDiesel
     * litrosCargarDiesel
     */
    public void setLitrosCargarDiesel(Double litrosCargarDiesel) {
		this.litrosCargarDiesel = litrosCargarDiesel;
	}

    /**
     *
     * @return litrosCargarPremium
     */
    public Double getLitrosCargarPremium() {
		return litrosCargarPremium;
	}

    /**
     *
     * @param litrosCargarPremium
     * litrosCargarPremium
     */
    public void setLitrosCargarPremium(Double litrosCargarPremium) {
		this.litrosCargarPremium = litrosCargarPremium;
	}

    /**
     *
     * @return litrosCargarRegular
     */
    public Double getLitrosCargarRegular() {
		return litrosCargarRegular;
	}

    /**
     *
     * @param litrosCargarRegular
     * litrosCargarRegular
     */
    public void setLitrosCargarRegular(Double litrosCargarRegular) {
		this.litrosCargarRegular = litrosCargarRegular;
	}

    /**
     *
     * @return litrosProgramadosDiesel
     */
    public Double getLitrosProgramadosDiesel() {
		return litrosProgramadosDiesel;
	}

    /**
     *
     * @param litrosProgramadosDiesel
     * litrosProgramadosDiesel
     */
    public void setLitrosProgramadosDiesel(Double litrosProgramadosDiesel) {
		this.litrosProgramadosDiesel = litrosProgramadosDiesel;
	}

    /**
     *
     * @return litrosProgramadosPremium
     */
    public Double getLitrosProgramadosPremium() {
		return litrosProgramadosPremium;
	}

    /**
     *
     * @param litrosProgramadosPremium
     * litrosProgramadosPremium
     */
    public void setLitrosProgramadosPremium(Double litrosProgramadosPremium) {
		this.litrosProgramadosPremium = litrosProgramadosPremium;
	}

    /**
     *
     * @return litrosProgramadosRegular
     */
    public Double getLitrosProgramadosRegular() {
		return litrosProgramadosRegular;
	}

    /**
     *
     * @param litrosProgramadosRegular
     * litrosProgramadosRegular
     */
    public void setLitrosProgramadosRegular(Double litrosProgramadosRegular) {
		this.litrosProgramadosRegular = litrosProgramadosRegular;
	}

    /**
     *
     * @return idPipa
     */
    public Integer getIdPipa() {
		return idPipa;
	}

    /**
     *
     * @param idPipa
     * idPipa
     */
    public void setIdPipa(Integer idPipa) {
		this.idPipa = idPipa;
	}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return numSerie
     */
    public String getNumSerie() {
		return numSerie;
	}

    /**
     *
     * @param numSerie
     * numSerie
     */
    public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

    /**
     *
     * @return placas
     */
    public String getPlacas() {
		return placas;
	}

    /**
     *
     * @param placas
     * placas
     */
    public void setPlacas(String placas) {
		this.placas = placas;
	}

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

    /**
     *
     * @return usuario
     */
    public int getUsuario() {
		return usuario;
	}

    /**
     *
     * @param usuario
     * usuario
     */
    public void setUsuario(int usuario) {
		this.usuario = usuario;
	}

    /**
     *
     * @return fechaCreacion
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return motivoBaja
     */
    public String getMotivoBaja() {
		return motivoBaja;
	}

    /**
     *
     * @param motivoBaja
     * motivoBaja
     */
    public void setMotivoBaja(String motivoBaja) {
		this.motivoBaja = motivoBaja;
	}

    /**
     *
     * @return selected
     */
    public boolean isSelected() {
		return selected;
	}

    /**
     *
     * @param selected
     * selected
     */
    public void setSelected(boolean selected) {
		this.selected = selected;
	}

    /**
     *
     * @return diesel
     */
    public boolean isDiesel() {
		return diesel;
	}

    /**
     *
     * @param diesel
     * diesel
     */
    public void setDiesel(boolean diesel) {
		this.diesel = diesel;
	}

    /**
     *
     * @return regular
     */
    public boolean isRegular() {
		return regular;
	}

    /**
     *
     * @param regular
     * regular
     */
    public void setRegular(boolean regular) {
		this.regular = regular;
	}

    /**
     *
     * @return premium
     */
    public boolean isPremium() {
		return premium;
	}

    /**
     *
     * @param premium
     * premium
     */
    public void setPremium(boolean premium) {
		this.premium = premium;
	}

    /**
     *
     * @return tamanoTotalTanque
     */
    public String getTamanoTotalTanque() {
		return tamanoTotalTanque;
	}

    /**
     *
     * @param tamanoTotalTanque
     * tamanoTotalTanque
     */
    public void setTamanoTotalTanque(String tamanoTotalTanque) {
		this.tamanoTotalTanque = tamanoTotalTanque;
	}

    /**
     *
     * @return totalPremium
     */
    public Double getTotalPremium() {
		return totalPremium;
	}

    /**
     *
     * @param totalPremium
     * totalPremium
     */
    public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

    /**
     *
     * @return totalRegular
     */
    public Double getTotalRegular() {
		return totalRegular;
	}

    /**
     *
     * @param totalRegular
     * totalRegular
     */
    public void setTotalRegular(Double totalRegular) {
		this.totalRegular = totalRegular;
	}

    /**
     *
     * @return totalDiesel
     */
    public Double getTotalDiesel() {
		return totalDiesel;
	}

    /**
     *
     * @param totalDiesel
     * totalDiesel
     */
    public void setTotalDiesel(Double totalDiesel) {
		this.totalDiesel = totalDiesel;
	}

    /**
     *
     * @return litrosRestantesDiesel
     */
    public Double getLitrosRestantesDiesel() {
		return litrosRestantesDiesel;
	}

    /**
     *
     * @param litrosRestantesDiesel
     * litrosRestantesDiesel
     */
    public void setLitrosRestantesDiesel(Double litrosRestantesDiesel) {
		this.litrosRestantesDiesel = litrosRestantesDiesel;
	}

    /**
     *
     * @return litrosRestantesRegular
     */
    public Double getLitrosRestantesRegular() {
		return litrosRestantesRegular;
	}

    /**
     *
     * @param litrosRestantesRegular
     * litrosRestantesRegular
     */
    public void setLitrosRestantesRegular(Double litrosRestantesRegular) {
		this.litrosRestantesRegular = litrosRestantesRegular;
	}

    /**
     *
     * @return litrosRestantesPremium
     */
    public Double getLitrosRestantesPremium() {
		return litrosRestantesPremium;
	}

    /**
     *
     * @param litrosRestantesPremium
     * litrosRestantesPremium
     */
    public void setLitrosRestantesPremium(Double litrosRestantesPremium) {
		this.litrosRestantesPremium = litrosRestantesPremium;
	}

    /**
     *
     * @return fechaModificacion
     */
    public Calendar getFechaModificacion() {
		return fechaModificacion;
	}

    /**
     *
     * @param fechaModificacion
     * fechaModificacion
     */
    public void setFechaModificacion(Calendar fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	
	
	
	
	
	
}
