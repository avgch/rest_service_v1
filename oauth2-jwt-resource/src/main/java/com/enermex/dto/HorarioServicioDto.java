package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class HorarioServicioDto {
	
	private Integer idHorario;
	private String  from ;
	private String  to;
	
    /**
     * getIdHorario
     * @return idHorario
     */
    public Integer getIdHorario() {
		return idHorario;
	}

    /**
     * 
     * @param idHorario
     * setIdHorario
     */
    public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}

    /**
     * getFrom
     * @return from
     */
    public String getFrom() {
		return from;
	}

    /**
     * 
     * @param from
     * setFrom
     */
    public void setFrom(String from) {
		this.from = from;
	}

    /**
     * getTo
     * @return to
     */
    public String getTo() {
		return to;
	}

    /**
     * 
     * @param to
     * setTo
     */
    public void setTo(String to) {
		this.to = to;
	}
	
	

}
