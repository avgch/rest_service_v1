package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class ValidaPromoDto {
	
	
	private Integer idCombustible;
	private boolean esLavado;
	private boolean esNeumaticos;
	private String  codigo;
	
    /**
     *
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return codigo
     */
    public String getCodigo() {
		return codigo;
	}

    /**
     *
     * @param codigo
     * codigo
     */
    public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

    /**
     *
     * @return esLavado
     */
    public boolean isEsLavado() {
		return esLavado;
	}

    /**
     *
     * @param esLavado
     * esLavado
     */
    public void setEsLavado(boolean esLavado) {
		this.esLavado = esLavado;
	}

    /**
     *
     * @return esNeumaticos
     */
    public boolean isEsNeumaticos() {
		return esNeumaticos;
	}

    /**
     *
     * @param esNeumaticos
     * esNeumaticos
     */
    public void setEsNeumaticos(boolean esNeumaticos) {
		this.esNeumaticos = esNeumaticos;
	}
	
	

}
