package com.enermex.dto;

import java.util.Calendar;

/**
 * Nombre de clase
 * @author abraham
 */
public class PromocionMovilDto {

	
	private Long    idPromocion;
	private String  titulo;
	
	private byte[] imagenPromo;
	
	private String  codigoPromocion;
	
    private Calendar  fechaHoraInicio;
	
	private Calendar  fechaHoraVigencia;
	
	private String  detalle;
	
	private Boolean estatus;
	
	private String colorEstatus;

    /**
     *
     * @return titulo
     */
    public String getTitulo() {
		return titulo;
	}

    /**
     *
     * @param titulo
     * titulo
     */
    public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

    /**
     *
     * @return imagenPromo
     */
    public byte[] getImagenPromo() {
		return imagenPromo;
	}

    /**
     *
     * @param imagenPromo
     * imagenPromo
     */
    public void setImagenPromo(byte[] imagenPromo) {
		this.imagenPromo = imagenPromo;
	}

    /**
     *
     * @return codigoPromocion
     */
    public String getCodigoPromocion() {
		return codigoPromocion;
	}

    /**
     *
     * @param codigoPromocion
     * codigoPromocion
     */
    public void setCodigoPromocion(String codigoPromocion) {
		this.codigoPromocion = codigoPromocion;
	}

    /**
     *
     * @return  fechaHoraInicio
     */
    public Calendar getFechaHoraInicio() {
		return fechaHoraInicio;
	}

    /**
     *
     * @param fechaHoraInicio
     * fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

    /**
     *
     * @return fechaHoraVigencia
     */ 
    public Calendar getFechaHoraVigencia() {
		return fechaHoraVigencia;
	}

    /**
     *
     * @param fechaHoraVigencia
     * fechaHoraVigencia
     */
    public void setFechaHoraVigencia(Calendar fechaHoraVigencia) {
		this.fechaHoraVigencia = fechaHoraVigencia;
	}

    /**
     *
     * @return detalle
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     *
     * @param detalle
     * detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

    /**
     *
     * @return estatus
     */
    public Boolean getEstatus() {
		return estatus;
	}

    /**
     *
     * @param estatus
     * estatus
     */
    public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

    /**
     *
     * @return idPromocion
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     * idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     *
     * @return colorEstatus
     */
    public String getColorEstatus() {
		return colorEstatus;
	}

    /**
     *
     * @param colorEstatus
     * colorEstatus
     */
    public void setColorEstatus(String colorEstatus) {
		this.colorEstatus = colorEstatus;
	}
	
	
	
	
	
}
