package com.enermex.dto.queja;

import java.util.Calendar;
import java.util.List;

import com.enermex.enumerable.QuejaEstatus;
import com.enermex.modelo.Cliente;
import com.enermex.modelo.Pedido;
import com.enermex.modelo.QuejaDetalleEntity;
import com.enermex.modelo.QuejaEvidenciaEntity;
import com.enermex.modelo.UsuarioEntity;

/**
 * QuejaWeb
 */
public class QuejaWeb {

  private Long idQueja;
  private Calendar fechaCreacion;
  private QuejaEstatus estatus;
  private UsuarioEntity usuario;
  private UsuarioEntity despachador;
  private UsuarioEntity atendedor;
  private Cliente cliente;
  private List<QuejaEvidenciaEntity> evidencias;
  private List<QuejaDetalleEntity> detalle;
  private Pedido pedido;

    /**
     *
     * @return
     */
    public Long getIdQueja() {
    return idQueja;
  }

    /**
     *
     * @param idQueja
     */
    public void setIdQueja(Long idQueja) {
    this.idQueja = idQueja;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public QuejaEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(QuejaEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public UsuarioEntity getUsuario() {
    return usuario;
  }

    /**
     *
     * @param usuario
     */
    public void setUsuario(UsuarioEntity usuario) {
    this.usuario = usuario;
  }

    /**
     *
     * @return
     */
    public UsuarioEntity getDespachador() {
    return despachador;
  }

    /**
     *
     * @param despachador
     */
    public void setDespachador(UsuarioEntity despachador) {
    this.despachador = despachador;
  }

    /**
     *
     * @return
     */
    public UsuarioEntity getAtendedor() {
    return atendedor;
  }

    /**
     *
     * @param atendedor
     */
    public void setAtendedor(UsuarioEntity atendedor) {
    this.atendedor = atendedor;
  }

    /**
     *
     * @return
     */
    public Cliente getCliente() {
    return cliente;
  }

    /**
     *
     * @param cliente
     */
    public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }

    /**
     *
     * @return
     */
    public List<QuejaEvidenciaEntity> getEvidencias() {
    return evidencias;
  }

    /**
     *
     * @param evidencias
     */
    public void setEvidencias(List<QuejaEvidenciaEntity> evidencias) {
    this.evidencias = evidencias;
  }

    /**
     *
     * @return
     */
    public List<QuejaDetalleEntity> getDetalle() {
    return detalle;
  }

    /**
     *
     * @param detalle
     */
    public void setDetalle(List<QuejaDetalleEntity> detalle) {
    this.detalle = detalle;
  }

    /**
     *
     * @return
     */
    public Pedido getPedido() {
    return pedido;
  }

    /**
     *
     * @param pedido
     */
    public void setPedido(Pedido pedido) {
    this.pedido = pedido;
  }
}
