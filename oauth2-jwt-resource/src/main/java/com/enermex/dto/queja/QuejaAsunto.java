package com.enermex.dto.queja;

import java.math.BigInteger;

/**
 * PostCliente
 */
public class QuejaAsunto {
  private BigInteger idPedido;
  private String asunto;

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
    return idPedido;
  }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
    this.idPedido = idPedido;
  }

    /**
     *
     * @return
     */
    public String getAsunto() {
    return asunto;
  }

    /**
     *
     * @param asunto
     */
    public void setAsunto(String asunto) {
    this.asunto = asunto;
  }
}
