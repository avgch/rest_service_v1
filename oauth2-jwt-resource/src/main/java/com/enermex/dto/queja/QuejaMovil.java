package com.enermex.dto.queja;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.enermex.enumerable.QuejaEstatus;
import com.enermex.enumerable.QuejaTipoDetalle;
import com.enermex.modelo.DireccionCliente;

/**
 * GetCliente
 */
public class QuejaMovil {
  private Long idQueja;
  private Calendar fechaCreacion;
  private QuejaEstatus estatus;
  private List<String> evidencias;
  private List<Detalle> detalle;
  private Pedido pedido;

    /**
     *
     * @return
     */
    public Long getIdQueja() {
    return idQueja;
  }

    /**
     *
     * @param idQueja
     */
    public void setIdQueja(Long idQueja) {
    this.idQueja = idQueja;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public QuejaEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(QuejaEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public List<String> getEvidencias() {
    return evidencias;
  }

    /**
     *
     * @param evidencias
     */
    public void setEvidencias(List<String> evidencias) {
    this.evidencias = evidencias;
  }

    /**
     *
     * @return
     */
    public List<Detalle> getDetalle() {
    return detalle;
  }

    /**
     *
     * @param detalle
     */
    public void setDetalle(List<Detalle> detalle) {
    this.detalle = detalle;
  }

    /**
     *
     * @return
     */
    public Pedido getPedido() {
    return pedido;
  }

    /**
     *
     * @param pedido
     */
    public void setPedido(Pedido pedido) {
    this.pedido = pedido;
  }

  /**
   * 
   */
  public static class Detalle {
    private Calendar fechaCreacion;
    private QuejaTipoDetalle tipo;
    private String texto;

      /**
       *
       * @param fechaCreacion
       * @param tipo
       * @param texto
       */
      public Detalle(Calendar fechaCreacion, QuejaTipoDetalle tipo, String texto) {
      this.fechaCreacion = fechaCreacion;
      this.tipo  = tipo;
      this.texto = texto;
    }

      /**
       *
       * @return
       */
      public Calendar getFechaCreacion() {
      return fechaCreacion;
    }

      /**
       *
       * @param fechaCreacion
       */
      public void setFechaCreacion(Calendar fechaCreacion) {
      this.fechaCreacion = fechaCreacion;
    }

      /**
       *
       * @return
       */
      public QuejaTipoDetalle getTipo() {
      return tipo;
    }

      /**
       *
       * @param tipo
       */
      public void setTipo(QuejaTipoDetalle tipo) {
      this.tipo = tipo;
    }

      /**
       *
       * @return
       */
      public String getTexto() {
      return texto;
    }

      /**
       *
       * @param texto
       */
      public void setTexto(String texto) {
      this.texto = texto;
    }
  }

  /**
   * 
   */
  public static class Pedido {
    private BigInteger idPedido;
    private Integer    idRuta;
    private String     rutaNombre = "";
    private Integer    idAutomovil;
    private Integer    idCombustible;
    private Integer    idServicio;
    private String     litroCarga;
    private double     montoCarga;
    private boolean    verificaPresionNeumatico;
    private String     llantasDelanterasPresion;
    private String     llantasTraserasPresion;
    private Calendar   fechaCreacion;
    private Calendar   fechaPedido;
    private Long       idPromocion;
    private boolean    cargaGas;
    private boolean    lavadoAuto;
    private boolean    revisionNeumaticos;
    private BigInteger idCliente;
    private Integer    estatusPedido;
    private String     detalle;
    private String     uuid;
    private Double     totalPagar;
    private String     litrosDespachados;
    
    // REQ:  Pablo Pérez - Obtener dierección y no identificador
    private DireccionCliente direccion;

      /**
       *
       * @return
       */
      public BigInteger getIdPedido() {
      return idPedido;
    }

      /**
       *
       * @param idPedido
       */
      public void setIdPedido(BigInteger idPedido) {
      this.idPedido = idPedido;
    }

      /**
       *
       * @return
       */
      public DireccionCliente getDireccion() {
      return direccion;
    }

      /**
       *
       * @param direccion
       */
      public void setDireccion(DireccionCliente direccion) {
      this.direccion = direccion;
    }

      /**
       *
       * @return
       */
      public Integer getIdRuta() {
      return idRuta;
    }

      /**
       *
       * @param idRuta
       */
      public void setIdRuta(Integer idRuta) {
      this.idRuta = idRuta;
    }

      /**
       *
       * @return
       */
      public Integer getIdAutomovil() {
      return idAutomovil;
    }

      /**
       *
       * @param idAutomovil
       */
      public void setIdAutomovil(Integer idAutomovil) {
      this.idAutomovil = idAutomovil;
    }

      /**
       *
       * @return
       */
      public Integer getIdCombustible() {
      return idCombustible;
    }

      /**
       *
       * @param idCombustible
       */
      public void setIdCombustible(Integer idCombustible) {
      this.idCombustible = idCombustible;
    }

      /**
       *
       * @return
       */
      public Integer getIdServicio() {
      return idServicio;
    }

      /**
       *
       * @param idServicio
       */
      public void setIdServicio(Integer idServicio) {
      this.idServicio = idServicio;
    }

      /**
       *
       * @return
       */
      public String getLitroCarga() {
      return litroCarga;
    }

      /**
       *
       * @param litroCarga
       */
      public void setLitroCarga(String litroCarga) {
      this.litroCarga = litroCarga;
    }

      /**
       *
       * @return
       */
      public double getMontoCarga() {
      return montoCarga;
    }

      /**
       *
       * @param montoCarga
       */
      public void setMontoCarga(double montoCarga) {
      this.montoCarga = montoCarga;
    }

      /**
       *
       * @return
       */
      public boolean isVerificaPresionNeumatico() {
      return verificaPresionNeumatico;
    }

      /**
       *
       * @param verificaPresionNeumatico
       */
      public void setVerificaPresionNeumatico(boolean verificaPresionNeumatico) {
      this.verificaPresionNeumatico = verificaPresionNeumatico;
    }

      /**
       *
       * @return
       */
      public String getLlantasDelanterasPresion() {
      return llantasDelanterasPresion;
    }

      /**
       *
       * @param llantasDelanterasPresion
       */
      public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
      this.llantasDelanterasPresion = llantasDelanterasPresion;
    }

      /**
       *
       * @return
       */
      public String getLlantasTraserasPresion() {
      return llantasTraserasPresion;
    }

      /**
       *
       * @param llantasTraserasPresion
       */
      public void setLlantasTraserasPresion(String llantasTraserasPresion) {
      this.llantasTraserasPresion = llantasTraserasPresion;
    }

      /**
       *
       * @return
       */
      public Calendar getFechaCreacion() {
      return fechaCreacion;
    }

      /**
       *
       * @param fechaCreacion
       */
      public void setFechaCreacion(Calendar fechaCreacion) {
      this.fechaCreacion = fechaCreacion;
    }

      /**
       *
       * @return
       */
      public Calendar getFechaPedido() {
      return fechaPedido;
    }

      /**
       *
       * @param fechaPedido
       */
      public void setFechaPedido(Calendar fechaPedido) {
      this.fechaPedido = fechaPedido;
    }

      /**
       *
       * @return
       */
      public Long getIdPromocion() {
      return idPromocion;
    }

      /**
       *
       * @param idPromocion
       */
      public void setIdPromocion(Long idPromocion) {
      this.idPromocion = idPromocion;
    }

      /**
       *
       * @return
       */
      public boolean isCargaGas() {
      return cargaGas;
    }

      /**
       *
       * @param cargaGas
       */
      public void setCargaGas(boolean cargaGas) {
      this.cargaGas = cargaGas;
    }

      /**
       *
       * @return
       */
      public boolean isLavadoAuto() {
      return lavadoAuto;
    }

      /**
       *
       * @param lavadoAuto
       */
      public void setLavadoAuto(boolean lavadoAuto) {
      this.lavadoAuto = lavadoAuto;
    }

      /**
       *
       * @return
       */
      public boolean isRevisionNeumaticos() {
      return revisionNeumaticos;
    }

      /**
       *
       * @param revisionNeumaticos
       */
      public void setRevisionNeumaticos(boolean revisionNeumaticos) {
      this.revisionNeumaticos = revisionNeumaticos;
    }

      /**
       *
       * @return
       */
      public BigInteger getIdCliente() {
      return idCliente;
    }

      /**
       *
       * @param idCliente
       */
      public void setIdCliente(BigInteger idCliente) {
      this.idCliente = idCliente;
    }

      /**
       *
       * @return
       */
      public Integer getEstatusPedido() {
      return estatusPedido;
    }

      /**
       *
       * @param estatusPedido
       */
      public void setEstatusPedido(Integer estatusPedido) {
      this.estatusPedido = estatusPedido;
    }

      /**
       *
       * @return
       */
      public String getDetalle() {
      return detalle;
    }

      /**
       *
       * @param detalle
       */
      public void setDetalle(String detalle) {
      this.detalle = detalle;
    }

      /**
       *
       * @return
       */
      public String getUuid() {
      return uuid;
    }

      /**
       *
       * @param uuid
       */
      public void setUuid(String uuid) {
      this.uuid = uuid;
    }

      /**
       *
       * @return
       */
      public Double getTotalPagar() {
      return totalPagar;
    }

      /**
       *
       * @param totalPagar
       */
      public void setTotalPagar(Double totalPagar) {
      this.totalPagar = totalPagar;
    }

      /**
       *
       * @return
       */
      public String getRutaNombre() {
      return rutaNombre;
    }

      /**
       *
       * @param rutaNombre
       */
      public void setRutaNombre(String rutaNombre) {
      this.rutaNombre = rutaNombre;
    }

      /**
       *
       * @return
       */
      public String getLitrosDespachados() {
      return litrosDespachados;
    }

      /**
       *
       * @param litrosDespachados
       */
      public void setLitrosDespachados(String litrosDespachados) {
      this.litrosDespachados = litrosDespachados;
    }
  }
}
