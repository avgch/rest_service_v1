package com.enermex.dto.queja;

/**
 * QuejaRespuesta
 */
public class QuejaRespuesta {
  private String respuesta;

    /**
     *
     * @return
     */
    public String getRespuesta() {
    return respuesta;
  }

    /**
     *
     * @param respuesta
     */
    public void setRespuesta(String respuesta) {
    this.respuesta = respuesta;
  }
}