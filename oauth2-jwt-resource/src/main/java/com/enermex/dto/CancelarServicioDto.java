package com.enermex.dto;

import java.math.BigInteger;

/**
 * Nombre de clase
 * @author abraham
 */
public class CancelarServicioDto {

	private BigInteger idPedido;
	private Integer    idMotivo;
	private String     detalle;
	
    /**
     * getIdPedido
     * @return idPedido
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     * 
     * @param idPedido
     * setIdPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     * 
     * @return idMotivo
     * getIdMotivo
     */
    public Integer getIdMotivo() {
		return idMotivo;
	}

    /**
     * 
     * @param idMotivo
     * setIdMotivo
     */
    public void setIdMotivo(Integer idMotivo) {
		this.idMotivo = idMotivo;
	}

    /**
     * 
     * @return detalle
     * 
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     * 
     * @param detalle
     * setDetalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	
	
	
}
