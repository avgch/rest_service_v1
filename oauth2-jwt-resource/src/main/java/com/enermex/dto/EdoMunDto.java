package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class EdoMunDto {
	
	private Integer idEdo;
	private Integer idMun;
	
    /**
     * getIdEdo
     * @return idEstado
     */
    public Integer getIdEdo() {
		return idEdo;
	}

    /**
     * 
     * @param idEdo
     * setIdEdo
     */
    public void setIdEdo(Integer idEdo) {
		this.idEdo = idEdo;
	}

    /**
     * getIdMun
     * @return idMunicipio
     */
    public Integer getIdMun() {
		return idMun;
	}

    /**
     * 
     * @param idMun
     * setIdMun
     */
    public void setIdMun(Integer idMun) {
		this.idMun = idMun;
	}
	
	
	

}
