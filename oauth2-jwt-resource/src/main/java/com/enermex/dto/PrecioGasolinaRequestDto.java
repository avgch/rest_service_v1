package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class PrecioGasolinaRequestDto {
	
	private Integer idMunicipio;
	private Integer idEstado;
	private Integer  idCombustible;
	
    /**
     *
     * @return idMunicipio
     */
    public Integer getIdMunicipio() {
		return idMunicipio;
	}

    /**
     *
     * @param idMunicipio
     * idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

    /**
     *
     * @return idEstado
     */
    public Integer getIdEstado() {
		return idEstado;
	}

    /**
     *
     * @param idEstado
     * idEstado
     */
    public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

    /**
     *
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}
	
	
	
	
	

}
