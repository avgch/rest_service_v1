package com.enermex.dto.reporte;

import java.util.Calendar;

/**
 *
 * @author abraham
 */
public class ReporteDespachador {
  private Calendar inicio;
  private Calendar fin;
  private Integer  idRuta;
  private Long     idDespachador;
  private Integer  estatus;

    /**
     *
     * @return
     */
    public Calendar getInicio() {
    return inicio;
  }

    /**
     *
     * @param inicio
     */
    public void setInicio(Calendar inicio) {
    this.inicio = inicio;
  }

    /**
     *
     * @return
     */
    public Calendar getFin() {
    return fin;
  }

    /**
     *
     * @param fin
     */
    public void setFin(Calendar fin) {
    this.fin = fin;
  }

    /**
     *
     * @return
     */
    public Integer getIdRuta() {
    return idRuta;
  }

    /**
     *
     * @param idRuta
     */
    public void setIdRuta(Integer idRuta) {
    this.idRuta = idRuta;
  }

    /**
     *
     * @return
     */
    public Long getIdDespachador() {
    return idDespachador;
  }

    /**
     *
     * @param idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
    this.idDespachador = idDespachador;
  }

    /**
     *
     * @return
     */
    public Integer getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }
}