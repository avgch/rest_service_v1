package com.enermex.dto.reporte;

import java.math.BigInteger;
import java.util.Calendar;

/**
 *
 * @author abraham
 */
public class ReporteServicio {
  private Calendar   inicio;
  private Calendar   fin;
  private Integer    plan;
  private BigInteger idCliente;
  private Integer    idEstado;
  private Integer    idMunicipio;
  private String     cp;
  private Integer    estatus;
  private Boolean    hasLavado;
  private Boolean    hasPresion;
  private Integer    idTipoPedido;

    /**
     *
     * @return
     */
    public Calendar getInicio() {
    return inicio;
  }

    /**
     *
     * @param inicio
     */
    public void setInicio(Calendar inicio) {
    this.inicio = inicio;
  }

    /**
     *
     * @return
     */
    public Calendar getFin() {
    return fin;
  }

    /**
     *
     * @param fin
     */
    public void setFin(Calendar fin) {
    this.fin = fin;
  }

    /**
     *
     * @return
     */
    public Integer getPlan() {
    return plan;
  }

    /**
     *
     * @param plan
     */
    public void setPlan(Integer plan) {
    this.plan = plan;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }

    /**
     *
     * @return
     */
    public String getCp() {
    return cp;
  }

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
    this.cp = cp;
  }

    /**
     *
     * @return
     */
    public Integer getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public Boolean getHasLavado() {
    return hasLavado;
  }

    /**
     *
     * @param hasLavado
     */
    public void setHasLavado(Boolean hasLavado) {
    this.hasLavado = hasLavado;
  }

    /**
     *
     * @return
     */
    public Boolean getHasPresion() {
    return hasPresion;
  }

    /**
     *
     * @param hasPresion
     */
    public void setHasPresion(Boolean hasPresion) {
    this.hasPresion = hasPresion;
  }

    /**
     *
     * @return
     */
    public Integer getIdTipoPedido() {
    return idTipoPedido;
  }

    /**
     *
     * @param idTipoPedido
     */
    public void setIdTipoPedido(Integer idTipoPedido) {
    this.idTipoPedido = idTipoPedido;
  }
}