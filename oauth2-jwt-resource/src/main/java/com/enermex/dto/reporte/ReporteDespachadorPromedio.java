package com.enermex.dto.reporte;

/**
 *
 * @author abraham
 */
public class ReporteDespachadorPromedio {
  private Long idDespachador;
  private Long   cantidadPedidos;
  private Double promedioCalificacion;
  private Double promedioTraslado;
  private Double promedioServicio;

    /**
     *
     * @return
     */
    public Long getIdDespachador() {
    return idDespachador;
  }

    /**
     *
     * @param idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
    this.idDespachador = idDespachador;
  }

    /**
     *
     * @return
     */
    public Long getCantidadPedidos() {
    return cantidadPedidos;
  }

    /**
     *
     * @param cantidadPedidos
     */
    public void setCantidadPedidos(Long cantidadPedidos) {
    this.cantidadPedidos = cantidadPedidos;
  }

    /**
     *
     * @return
     */
    public Double getPromedioCalificacion() {
    return promedioCalificacion;
  }

    /**
     *
     * @param promedioCalificacion
     */
    public void setPromedioCalificacion(Double promedioCalificacion) {
    this.promedioCalificacion = promedioCalificacion;
  }

    /**
     *
     * @return
     */
    public Double getPromedioTraslado() {
    return promedioTraslado;
  }

    /**
     *
     * @param promedioTraslado
     */
    public void setPromedioTraslado(Double promedioTraslado) {
    this.promedioTraslado = promedioTraslado;
  }

    /**
     *
     * @return
     */
    public Double getPromedioServicio() {
    return promedioServicio;
  }

    /**
     *
     * @param promedioServicio
     */
    public void setPromedioServicio(Double promedioServicio) {
    this.promedioServicio = promedioServicio;
  }
}