package com.enermex.dto.reporte;

import java.sql.Timestamp;

/**
 *
 * @author abraham
 */
public class ReporteClienteLineaV2 {
  private long   idCliente;;
  private long   idTitular;
  private String nombre;
  private String apellidoPaterno;
  private String apellidoMaterno;
  private int    idEstatus;
  private boolean    bloqueo;
  private boolean    acceso;
  private Timestamp fechaCreacion;
  private Timestamp fechaBaja;
  private long   idAutomovil;
  private String marca;
  private String modelo;
  private String anio;
  private String color;
  private String placas;
  private long   cantidad;
  private long autos;
  private long familiares;

    /**
     *
     * @return
     */
    public long getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(long idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public long getIdTitular() {
    return idTitular;
  }

    /**
     *
     * @param idTitular
     */
    public void setIdTitular(long idTitular) {
    this.idTitular = idTitular;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public String getApellidoPaterno() {
    return apellidoPaterno;
  }

    /**
     *
     * @param apellidoPaterno
     */
    public void setApellidoPaterno(String apellidoPaterno) {
    this.apellidoPaterno = apellidoPaterno;
  }

    /**
     *
     * @return
     */
    public String getApellidoMaterno() {
    return apellidoMaterno;
  }

    /**
     *
     * @param apellidoMaterno
     */
    public void setApellidoMaterno(String apellidoMaterno) {
    this.apellidoMaterno = apellidoMaterno;
  }

    /**
     *
     * @return
     */
    public int getIdEstatus() {
    return idEstatus;
  }

    /**
     *
     * @param idEstatus
     */
    public void setIdEstatus(int idEstatus) {
    this.idEstatus = idEstatus;
  }

    /**
     *
     * @return
     */
    public boolean getBloqueo() {
    return bloqueo;
  }

    /**
     *
     * @param bloqueo
     */
    public void setBloqueo(boolean bloqueo) {
    this.bloqueo = bloqueo;
  }

    /**
     *
     * @return
     */
    public boolean getAcceso() {
    return acceso;
  }

    /**
     *
     * @param acceso
     */
    public void setAcceso(boolean acceso) {
    this.acceso = acceso;
  }

    /**
     *
     * @return
     */
    public Timestamp getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Timestamp fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public Timestamp getFechaBaja() {
    return fechaBaja;
  }

    /**
     *
     * @param fechaBaja
     */
    public void setFechaBaja(Timestamp fechaBaja) {
    this.fechaBaja = fechaBaja;
  }

    /**
     *
     * @return
     */
    public long getIdAutomovil() {
    return idAutomovil;
  }

    /**
     *
     * @param idAutomovil
     */
    public void setIdAutomovil(long idAutomovil) {
    this.idAutomovil = idAutomovil;
  }

    /**
     *
     * @return
     */
    public String getMarca() {
    return marca;
  }

    /**
     *
     * @param marca
     */
    public void setMarca(String marca) {
    this.marca = marca;
  }

    /**
     *
     * @return
     */
    public String getModelo() {
    return modelo;
  }

    /**
     *
     * @param modelo
     */
    public void setModelo(String modelo) {
    this.modelo = modelo;
  }

    /**
     *
     * @return
     */
    public String getAnio() {
    return anio;
  }

    /**
     *
     * @param anio
     */
    public void setAnio(String anio) {
    this.anio = anio;
  }

    /**
     *
     * @return
     */
    public String getColor() {
    return color;
  }

    /**
     *
     * @param color
     */
    public void setColor(String color) {
    this.color = color;
  }

    /**
     *
     * @return
     */
    public String getPlacas() {
    return placas;
  }

    /**
     *
     * @param placas
     */
    public void setPlacas(String placas) {
    this.placas = placas;
  }

    /**
     *
     * @return
     */
    public long getCantidad() {
    return cantidad;
  }

    /**
     *
     * @param cantidad
     */
    public void setCantidad(long cantidad) {
    this.cantidad = cantidad;
  }

    /**
     *
     * @return
     */
    public long getAutos() {
    return autos;
  }

    /**
     *
     * @param autos
     */
    public void setAutos(long autos) {
    this.autos = autos;
  }

    /**
     *
     * @return
     */
    public long getFamiliares() {
    return familiares;
  }

    /**
     *
     * @param familiares
     */
    public void setFamiliares(long familiares) {
    this.familiares = familiares;
  }
  
}
