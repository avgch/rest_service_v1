package com.enermex.dto.reporte;

/**
 *
 * @author abraham
 */
public class ReporteClienteFiltroV2 {
  private String  inicio;
  private String  fin;
  private Integer plan;
  private Long    idCliente;
  private Long    idAutomovil;
  private Boolean hasLavado;
  private Boolean hasPresion;
  private Integer idTipoPedido;
  private Integer idEstado;
  private Integer idMunicipio;
  private String  cp;
  private Integer estatus;

    /**
     *
     * @return
     */
    public String getInicio() {
    return inicio;
  }

    /**
     *
     * @param inicio
     */
    public void setInicio(String inicio) {
    this.inicio = inicio;
  }

    /**
     *
     * @return
     */
    public String getFin() {
    return fin;
  }

    /**
     *
     * @param fin
     */
    public void setFin(String fin) {
    this.fin = fin;
  }

    /**
     *
     * @return
     */
    public Integer getPlan() {
    return plan;
  }

    /**
     *
     * @param plan
     */
    public void setPlan(Integer plan) {
    this.plan = plan;
  }

    /**
     *
     * @return
     */
    public Long getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(Long idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public Long getIdAutomovil() {
    return idAutomovil;
  }

    /**
     *
     * @param idAutomovil
     */
    public void setIdAutomovil(Long idAutomovil) {
    this.idAutomovil = idAutomovil;
  }

    /**
     *
     * @return
     */
    public Boolean getHasLavado() {
    return hasLavado;
  }

    /**
     *
     * @param hasLavado
     */
    public void setHasLavado(Boolean hasLavado) {
    this.hasLavado = hasLavado;
  }

    /**
     *
     * @return
     */
    public Boolean getHasPresion() {
    return hasPresion;
  }

    /**
     *
     * @param hasPresion
     */
    public void setHasPresion(Boolean hasPresion) {
    this.hasPresion = hasPresion;
  }

    /**
     *
     * @return
     */
    public Integer getIdTipoPedido() {
    return idTipoPedido;
  }

    /**
     *
     * @param idTipoPedido
     */
    public void setIdTipoPedido(Integer idTipoPedido) {
    this.idTipoPedido = idTipoPedido;
  }

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }

    /**
     *
     * @return
     */
    public String getCp() {
    return cp;
  }

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
    this.cp = cp;
  }

    /**
     *
     * @return
     */
    public Integer getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }
}