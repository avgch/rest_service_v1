package com.enermex.dto.reporte;

import java.util.List;

import com.enermex.modelo.Cliente;
import com.enermex.view.ServicioReporteView;

/**
 *
 * @author abraham
 */
public class ReporteClienteLinea {
  private ServicioReporteView servicio;
  private List<Cliente> familiares;
  private Long cantidadServicios;
  private Long cantidadAutos;
  private List<String> planes;

    /**
     *
     * @return
     */
    public ServicioReporteView getServicio() {
    return servicio;
  }

    /**
     *
     * @param servicio
     */
    public void setServicio(ServicioReporteView servicio) {
    this.servicio = servicio;
  }

    /**
     *
     * @return
     */
    public List<Cliente> getFamiliares() {
    return familiares;
  }

    /**
     *
     * @param familiares
     */
    public void setFamiliares(List<Cliente> familiares) {
    this.familiares = familiares;
  }

    /**
     *
     * @return
     */
    public Long getCantidadServicios() {
    return cantidadServicios;
  }

    /**
     *
     * @param cantidadServicios
     */
    public void setCantidadServicios(Long cantidadServicios) {
    this.cantidadServicios = cantidadServicios;
  }

    /**
     *
     * @return
     */
    public Long getCantidadAutos() {
    return cantidadAutos;
  }

    /**
     *
     * @param cantidadAutos
     */
    public void setCantidadAutos(Long cantidadAutos) {
    this.cantidadAutos = cantidadAutos;
  }

    /**
     *
     * @return
     */
    public List<String> getPlanes() {
    return planes;
  }

    /**
     *
     * @param planes
     */
    public void setPlanes(List<String> planes) {
    this.planes = planes;
  }
}