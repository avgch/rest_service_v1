package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class ProductoDto {

	
	
	private Long id;
	private String nombre;
	
    /**
     *
     * @return id
     */
    public Long getId() {
		return id;
	}

    /**
     *
     * @param id
     * id
     */
    public void setId(Long id) {
		this.id = id;
	}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
