package com.enermex.dto.seguimiento;

import java.util.Calendar;

/**
 * FiltroWeb
 */
public class FiltroWeb {
  private Calendar inicio;
  private Calendar fin;
  private Integer ruta;
  private Integer estatus;
  private int pagina;

    /**
     *
     * @return
     */
    public Calendar getInicio() {
    return inicio;
  }

    /**
     *
     * @param inicio
     */
    public void setInicio(Calendar inicio) {
    this.inicio = inicio;
  }

    /**
     *
     * @return
     */
    public Calendar getFin() {
    return fin;
  }

    /**
     *
     * @param fin
     */
    public void setFin(Calendar fin) {
    this.fin = fin;
  }

    /**
     *
     * @return
     */
    public Integer getRuta() {
    return ruta;
  }

    /**
     *
     * @param ruta
     */
    public void setRuta(Integer ruta) {
    this.ruta = ruta;
  }

    /**
     *
     * @return
     */
    public Integer getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public int getPagina() {
    return pagina;
  }

    /**
     *
     * @param pagina
     */
    public void setPagina(int pagina) {
    this.pagina = pagina;
  }
}