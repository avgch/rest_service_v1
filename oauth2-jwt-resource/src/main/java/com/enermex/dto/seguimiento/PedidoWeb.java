package com.enermex.dto.seguimiento;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;

import com.enermex.modelo.Cliente;
import com.enermex.modelo.ClienteDireccionEntity;
import com.enermex.modelo.PedidoFacturaEntity;
import com.enermex.modelo.Pipa;
import com.enermex.modelo.UsuarioEntity;
import com.enermex.modelo.SeguimientoPedidoEntity.SClienteEntity;
import com.enermex.modelo.SeguimientoPedidoEntity.SUsuarioEntity;

/**
 * PedidoWeb
 */
public class PedidoWeb {
  // Datos asociados al pedido
  private BigInteger idPedido;
  private String     uuid;
  private Integer    idRuta;
  private String     rutaNombre;
	private Integer    idAutomovil;
	private Integer    idCombustible;
	private Integer    idServicio;
	private String     litroCarga;
	private double     montoCarga;
	private boolean    verificaPresionNeumatico;
	private String     llantasDelanterasPresion;
	private String     llantasTraserasPresion;
	private Calendar   fechaCreacion;
	private Calendar   fechaPedido;
	private Long       idPromocion;
	private boolean    cargaGas;
	private boolean    lavadoAuto;
	private boolean    revisionNeumaticos;
	private Integer    estatusPedido;
	private String     detalle;
	private Double     subTotal;
	private Double     total;
	private Double     iva;
	private Double     totalPagar;
  private Double     descuento;
  private String     tiempoTraslado;
  private Calendar   fechaHoraInicio;
  private Calendar   fechaHoraFin;


  private Double     precioPorLitroFinal;
  private String     litrosDespachados;
  private Double     precioPorLitro;

  private Double costoServicio;
  private String municipio;
  private Double precioLavadoAuto;
  private Double precioPresionNeumaticos;

  // Cancelación
  private String cancelaMotivo;
  private String cancelaComentario;
  
  
  // Datos del cliente
	private ClienteDireccionEntity direccion;
  private SClienteEntity cliente;
  
  // Datos del despachador
  private SUsuarioEntity despachador;
  
  // Evidencias
  private ArrayList<String> evidenciasAntes   = new ArrayList<>();
  private ArrayList<String> evidenciasDespues = new ArrayList<>();

  // Calificaciones
  private Integer calificacionCliente     = -1;
  private Integer calificacionDespachador = -1;

  // Datos del autotanque
  private Pipa pipa;

  // Datos de facturación
  private PedidoFacturaEntity factura;

  // Tarjeta de la compra
  // TODO: PayPal
  private String tarjeta;


  // Promoción
  private String codigoPromocion;

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
    return idPedido;
  }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
    this.idPedido = idPedido;
  }

    /**
     *
     * @return
     */
    public Integer getIdRuta() {
    return idRuta;
  }

    /**
     *
     * @param idRuta
     */
    public void setIdRuta(Integer idRuta) {
    this.idRuta = idRuta;
  }

    /**
     *
     * @return
     */
    public Integer getIdAutomovil() {
    return idAutomovil;
  }

    /**
     *
     * @param idAutomovil
     */
    public void setIdAutomovil(Integer idAutomovil) {
    this.idAutomovil = idAutomovil;
  }

    /**
     *
     * @return
     */
    public Integer getIdCombustible() {
    return idCombustible;
  }

    /**
     *
     * @param idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
    this.idCombustible = idCombustible;
  }

    /**
     *
     * @return
     */
    public Integer getIdServicio() {
    return idServicio;
  }

    /**
     *
     * @param idServicio
     */
    public void setIdServicio(Integer idServicio) {
    this.idServicio = idServicio;
  }

    /**
     *
     * @return
     */
    public String getLitroCarga() {
    return litroCarga;
  }

    /**
     *
     * @param litroCarga
     */
    public void setLitroCarga(String litroCarga) {
    this.litroCarga = litroCarga;
  }

    /**
     *
     * @return
     */
    public double getMontoCarga() {
    return montoCarga;
  }

    /**
     *
     * @param montoCarga
     */
    public void setMontoCarga(double montoCarga) {
    this.montoCarga = montoCarga;
  }

    /**
     *
     * @return
     */
    public boolean isVerificaPresionNeumatico() {
    return verificaPresionNeumatico;
  }

    /**
     *
     * @param verificaPresionNeumatico
     */
    public void setVerificaPresionNeumatico(boolean verificaPresionNeumatico) {
    this.verificaPresionNeumatico = verificaPresionNeumatico;
  }

    /**
     *
     * @return
     */
    public String getLlantasDelanterasPresion() {
    return llantasDelanterasPresion;
  }

    /**
     *
     * @param llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
    this.llantasDelanterasPresion = llantasDelanterasPresion;
  }

    /**
     *
     * @return
     */
    public String getLlantasTraserasPresion() {
    return llantasTraserasPresion;
  }

    /**
     *
     * @param llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion) {
    this.llantasTraserasPresion = llantasTraserasPresion;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaPedido() {
    return fechaPedido;
  }

    /**
     *
     * @param fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido) {
    this.fechaPedido = fechaPedido;
  }

    /**
     *
     * @return
     */
    public Long getIdPromocion() {
    return idPromocion;
  }

    /**
     *
     * @param idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
    this.idPromocion = idPromocion;
  }

    /**
     *
     * @return
     */
    public boolean isCargaGas() {
    return cargaGas;
  }

    /**
     *
     * @param cargaGas
     */
    public void setCargaGas(boolean cargaGas) {
    this.cargaGas = cargaGas;
  }

    /**
     *
     * @return
     */
    public boolean isLavadoAuto() {
    return lavadoAuto;
  }

    /**
     *
     * @param lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
    this.lavadoAuto = lavadoAuto;
  }

    /**
     *
     * @return
     */
    public boolean isRevisionNeumaticos() {
    return revisionNeumaticos;
  }

    /**
     *
     * @param revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
    this.revisionNeumaticos = revisionNeumaticos;
  }

    /**
     *
     * @return
     */
    public Integer getEstatusPedido() {
    return estatusPedido;
  }

    /**
     *
     * @param estatusPedido
     */
    public void setEstatusPedido(Integer estatusPedido) {
    this.estatusPedido = estatusPedido;
  }

    /**
     *
     * @return
     */
    public String getDetalle() {
    return detalle;
  }

    /**
     *
     * @param detalle
     */
    public void setDetalle(String detalle) {
    this.detalle = detalle;
  }

    /**
     *
     * @return
     */
    public Double getSubTotal() {
    return subTotal;
  }

    /**
     *
     * @param subTotal
     */
    public void setSubTotal(Double subTotal) {
    this.subTotal = subTotal;
  }

    /**
     *
     * @return
     */
    public Double getTotal() {
    return total;
  }

    /**
     *
     * @param total
     */
    public void setTotal(Double total) {
    this.total = total;
  }

    /**
     *
     * @return
     */
    public Double getIva() {
    return iva;
  }

    /**
     *
     * @param iva
     */
    public void setIva(Double iva) {
    this.iva = iva;
  }

    /**
     *
     * @return
     */
    public Double getTotalPagar() {
    return totalPagar;
  }

    /**
     *
     * @param totalPagar
     */
    public void setTotalPagar(Double totalPagar) {
    this.totalPagar = totalPagar;
  }

    /**
     *
     * @return
     */
    public Double getDescuento() {
    return descuento;
  }

    /**
     *
     * @param descuento
     */
    public void setDescuento(Double descuento) {
    this.descuento = descuento;
  }

    /**
     *
     * @return
     */
    public String getTiempoTraslado() {
    return tiempoTraslado;
  }

    /**
     *
     * @param tiempoTraslado
     */
    public void setTiempoTraslado(String tiempoTraslado) {
    this.tiempoTraslado = tiempoTraslado;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaHoraInicio() {
    return fechaHoraInicio;
  }

    /**
     *
     * @param fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio) {
    this.fechaHoraInicio = fechaHoraInicio;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaHoraFin() {
    return fechaHoraFin;
  }

    /**
     *
     * @param fechaHoraFin
     */
    public void setFechaHoraFin(Calendar fechaHoraFin) {
    this.fechaHoraFin = fechaHoraFin;
  }

    /**
     *
     * @return
     */
    public ClienteDireccionEntity getDireccion() {
    return direccion;
  }

    /**
     *
     * @param direccion
     */
    public void setDireccion(ClienteDireccionEntity direccion) {
    this.direccion = direccion;
  }

    /**
     *
     * @return
     */
    public SClienteEntity getCliente() {
    return cliente;
  }

    /**
     *
     * @param cliente
     */
    public void setCliente(SClienteEntity cliente) {
    this.cliente = cliente;
  }

    /**
     *
     * @return
     */
    public SUsuarioEntity getDespachador() {
    return despachador;
  }

    /**
     *
     * @param despachador
     */
    public void setDespachador(SUsuarioEntity despachador) {
    this.despachador = despachador;
  }

    /**
     *
     * @return
     */
    public ArrayList<String> getEvidenciasAntes() {
    return evidenciasAntes;
  }

    /**
     *
     * @param evidenciasAntes
     */
    public void setEvidenciasAntes(ArrayList<String> evidenciasAntes) {
    this.evidenciasAntes = evidenciasAntes;
  }

    /**
     *
     * @return
     */
    public ArrayList<String> getEvidenciasDespues() {
    return evidenciasDespues;
  }

    /**
     *
     * @param evidenciasDespues
     */
    public void setEvidenciasDespues(ArrayList<String> evidenciasDespues) {
    this.evidenciasDespues = evidenciasDespues;
  }

    /**
     *
     * @return
     */
    public Integer getCalificacionCliente() {
    return calificacionCliente;
  }

    /**
     *
     * @param calificacionCliente
     */
    public void setCalificacionCliente(Integer calificacionCliente) {
    this.calificacionCliente = calificacionCliente;
  }

    /**
     *
     * @return
     */
    public Integer getCalificacionDespachador() {
    return calificacionDespachador;
  }

    /**
     *
     * @param calificacionDespachador
     */
    public void setCalificacionDespachador(Integer calificacionDespachador) {
    this.calificacionDespachador = calificacionDespachador;
  }

    /**
     *
     * @return
     */
    public Pipa getPipa() {
    return pipa;
  }

    /**
     *
     * @param pipa
     */
    public void setPipa(Pipa pipa) {
    this.pipa = pipa;
  }

    /**
     *
     * @return
     */
    public PedidoFacturaEntity getFactura() {
    return factura;
  }

    /**
     *
     * @param factura
     */
    public void setFactura(PedidoFacturaEntity factura) {
    this.factura = factura;
  }

    /**
     *
     * @return
     */
    public String getRutaNombre() {
    return rutaNombre;
  }

    /**
     *
     * @param rutaNombre
     */
    public void setRutaNombre(String rutaNombre) {
    this.rutaNombre = rutaNombre;
  }

    /**
     *
     * @return
     */
    public String getUuid() {
    return uuid;
  }

    /**
     *
     * @param uuid
     */
    public void setUuid(String uuid) {
    this.uuid = uuid;
  }

    /**
     *
     * @return
     */
    public Double getCostoServicio() {
    return costoServicio;
  }

    /**
     *
     * @param costoServicio
     */
    public void setCostoServicio(Double costoServicio) {
    this.costoServicio = costoServicio;
  }

    /**
     *
     * @return
     */
    public String getMunicipio() {
    return municipio;
  }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
    this.municipio = municipio;
  }

    /**
     *
     * @return
     */
    public Double getPrecioLavadoAuto() {
    return precioLavadoAuto;
  }

    /**
     *
     * @param precioLavadoAuto
     */
    public void setPrecioLavadoAuto(Double precioLavadoAuto) {
    this.precioLavadoAuto = precioLavadoAuto;
  }

    /**
     *
     * @return
     */
    public Double getPrecioPresionNeumaticos() {
    return precioPresionNeumaticos;
  }

    /**
     *
     * @param precioPresionNeumaticos
     */
    public void setPrecioPresionNeumaticos(Double precioPresionNeumaticos) {
    this.precioPresionNeumaticos = precioPresionNeumaticos;
  }

    /**
     *
     * @return
     */
    public Double getPrecioPorLitroFinal() {
    return precioPorLitroFinal;
  }

    /**
     *
     * @param precioPorLitroFinal
     */
    public void setPrecioPorLitroFinal(Double precioPorLitroFinal) {
    this.precioPorLitroFinal = precioPorLitroFinal;
  }

    /**
     *
     * @return
     */
    public String getLitrosDespachados() {
    return litrosDespachados;
  }

    /**
     *
     * @param litrosDespachados
     */
    public void setLitrosDespachados(String litrosDespachados) {
    this.litrosDespachados = litrosDespachados;
  }

    /**
     *
     * @return
     */
    public Double getPrecioPorLitro() {
    return precioPorLitro;
  }

    /**
     *
     * @param precioPorLitro
     */
    public void setPrecioPorLitro(Double precioPorLitro) {
    this.precioPorLitro = precioPorLitro;
  }

    /**
     *
     * @return
     */
    public String getCancelaMotivo() {
    return cancelaMotivo;
  }

    /**
     *
     * @param cancelaMotivo
     */
    public void setCancelaMotivo(String cancelaMotivo) {
    this.cancelaMotivo = cancelaMotivo;
  }

    /**
     *
     * @return
     */
    public String getCancelaComentario() {
    return cancelaComentario;
  }

    /**
     *
     * @param cancelaComentario
     */
    public void setCancelaComentario(String cancelaComentario) {
    this.cancelaComentario = cancelaComentario;
  }

    /**
     *
     * @return
     */
    public String getCodigoPromocion() {
    return codigoPromocion;
  }

    /**
     *
     * @param codigoPromocion
     */
    public void setCodigoPromocion(String codigoPromocion) {
    this.codigoPromocion = codigoPromocion;
  }

    /**
     *
     * @return
     */
    public String getTarjeta() {
    return tarjeta;
  }

    /**
     *
     * @param tarjeta
     */
    public void setTarjeta(String tarjeta) {
    this.tarjeta = tarjeta;
  }
}
