package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class NotificaDespachadorDto {

	

	private Long    idUsuario ;

	private String  despachador;

    /**
     * Constructor de clase
     * @return idUsuario
     */
    public Long getIdUsuario() {

		return idUsuario;

	}

    /**
     *  
     * @param idUsuario
     * setIdUsuario
     */
    public void setIdUsuario(Long idUsuario) {

		this.idUsuario = idUsuario;

	}

    /**
     * getDespachador
     * @return despachador
     */
    public String getDespachador() {

		return despachador;

	}

    /**
     * 
     * @param despachador
     * setDespachador
     */
    public void setDespachador(String despachador) {

		this.despachador = despachador;

	}



}

