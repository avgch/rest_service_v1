package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class ProductoServicioDto {
	
	private Integer id;
	private String  nombre;
	
    /**
     *
     * @return id
     */
    public Integer getId() {
		return id;
	}

    /**
     *
     * @param id
     * id
     */
    public void setId(Integer id) {
		this.id = id;
	}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	

}
