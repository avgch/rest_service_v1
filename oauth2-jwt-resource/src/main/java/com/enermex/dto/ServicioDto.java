package com.enermex.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Nombre de clase
 * @author abraham
 */
@Entity
@Table(name = "servicios")
public class ServicioDto {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_servicio")
	private Integer idServicios;
	
	
	@Column(name = "servicio")
	private String servicio;
	
	@Column(name = "precio")
	private float precio;
	
    /**
     *
     * @return idServicios
     */
    public Integer getIdServicios() {
		return idServicios;
	}

    /**
     *
     * @param idServicios
     * idServicios
     */
    public void setIdServicios(Integer idServicios) {
		this.idServicios = idServicios;
	}

    /**
     *
     * @return servicio
     */
    public String getServicio() {
		return servicio;
	}

    /**
     *
     * @param servicio
     * servicio
     */
    public void setServicio(String servicio) {
		this.servicio = servicio;
	}

    /**
     *
     * @return precio
     */
    public float getPrecio() {
		return precio;
	}

    /**
     *
     * @param precio
     * precio
     */
    public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	
	
	
	
	
	
	
	
	
	

}
