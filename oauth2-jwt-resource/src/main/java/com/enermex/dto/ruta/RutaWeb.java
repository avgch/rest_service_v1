package com.enermex.dto.ruta;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.enermex.enumerable.RutaEstatus;
import com.enermex.modelo.RutaCodigoEntity;

/**
 *
 * @author abraham
 */
public class RutaWeb {
  Integer idRuta;
  String nombre;
  RutaEstatus estatus;
  Date fechaCreacion;
  Date fechaBaja;
  private ArrayList<Usuario> despachadores = new ArrayList<>(); 
  private List<RutaCodigoEntity> codigos;

    /**
     *
     * @return
     */
    public Integer getIdRuta() {
    return idRuta;
  }

    /**
     *
     * @param idRuta
     */
    public void setIdRuta(Integer idRuta) {
    this.idRuta = idRuta;
  }

    /**
     *
     * @return
     */
    public String getNombre() {
    return nombre;
  }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
    this.nombre = nombre;
  }

    /**
     *
     * @return
     */
    public RutaEstatus getEstatus() {
    return estatus;
  }

    /**
     *
     * @param estatus
     */
    public void setEstatus(RutaEstatus estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return
     */
    public Date getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public List<RutaCodigoEntity> getCodigos() {
    return codigos;
  }

    /**
     *
     * @param codigos
     */
    public void setCodigos(List<RutaCodigoEntity> codigos) {
    this.codigos = codigos;
  }

    /**
     *
     * @return
     */
    public Date getFechaBaja() {
    return fechaBaja;
  }

    /**
     *
     * @param fechaBaja
     */
    public void setFechaBaja(Date fechaBaja) {
    this.fechaBaja = fechaBaja;
  }

    /**
     *
     * @return
     */
    public ArrayList<Usuario> getDespachadores() {
    return despachadores;
  }

    /**
     *
     * @param despachadores
     */
    public void setDespachadores(ArrayList<Usuario> despachadores) {
    this.despachadores = despachadores;
  }

    /**
     *
     */
    public static class Usuario {
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;

      /**
       *
       * @return
       */
      public String getNombre() {
      return nombre;
    }

      /**
       *
       * @param nombre
       */
      public void setNombre(String nombre) {
      this.nombre = nombre;
    }

      /**
       *
       * @return
       */
      public String getApellidoPaterno() {
      return apellidoPaterno;
    }

      /**
       *
       * @param apellidoPaterno
       */
      public void setApellidoPaterno(String apellidoPaterno) {
      this.apellidoPaterno = apellidoPaterno;
    }

      /**
       *
       * @return
       */
      public String getApellidoMaterno() {
      return apellidoMaterno;
    }

      /**
       *
       * @param apellidoMaterno
       */
      public void setApellidoMaterno(String apellidoMaterno) {
      this.apellidoMaterno = apellidoMaterno;
    }

    
  }
}