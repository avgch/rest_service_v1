package com.enermex.dto.reportes;

import java.math.BigInteger;
import java.util.Calendar;

/**
 *
 * @author abraham
 */
public class ReporteHistoricoDto {
	
	private BigInteger id;
	private Integer    idEstado;
	private Integer    idAlcaldia;
	private String     estado;
	private String     alcaldia;
	private String     cp;
	private Integer    idCombustible;
	private Double     promedio;
	private String     fechaRegistro;
	
    /**
     *
     */
    public ReporteHistoricoDto(){
		
		//fechaVigencia = Calendar.getInstance();
	}
	
    /**
     *
     * @return
     */
    public BigInteger getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(BigInteger id) {
		this.id = id;
	}

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
		return idEstado;
	}

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

    /**
     *
     * @return
     */
    public Integer getIdAlcaldia() {
		return idAlcaldia;
	}

    /**
     *
     * @param idAlcaldia
     */
    public void setIdAlcaldia(Integer idAlcaldia) {
		this.idAlcaldia = idAlcaldia;
	}

    /**
     *
     * @return
     */
    public String getEstado() {
		return estado;
	}

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
		this.estado = estado;
	}

    /**
     *
     * @return
     */
    public String getAlcaldia() {
		return alcaldia;
	}

    /**
     *
     * @param alcaldia
     */
    public void setAlcaldia(String alcaldia) {
		this.alcaldia = alcaldia;
	}

    /**
     *
     * @return
     */
    public String getCp() {
		return cp;
	}

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
		this.cp = cp;
	}

    /**
     *
     * @return
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return
     */
    public Double getPromedio() {
		return promedio;
	}

    /**
     *
     * @param promedio
     */
    public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}

    /**
     *
     * @return
     */
    public String getFechaRegistro() {
		return fechaRegistro;
	}

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
    
	
	

}
