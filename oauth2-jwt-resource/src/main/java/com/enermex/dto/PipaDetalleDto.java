package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class PipaDetalleDto {
	
	
	private String numSerie;
	private String placas;
	private String nombre;
	
    /**
     *
     * @return numSerie
     */
    public String getNumSerie() {
		return numSerie;
	}

    /**
     *
     * @param numSerie
     * numSerie
     */
    public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

    /**
     *
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     *
     * @param nombre
     * nombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     *
     * @return placas
     */
    public String getPlacas() {
		return placas;
	}

    /**
     *
     * @param placas
     * placas
     */
    public void setPlacas(String placas) {
		this.placas = placas;
	}
	
	

}
