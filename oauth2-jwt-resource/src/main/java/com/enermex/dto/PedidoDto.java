package com.enermex.dto;

import java.math.BigInteger;

import com.enermex.modelo.Pedido;

/**
 * Nombre de Clase
 * @author abraham
 */
public class PedidoDto extends Pedido{

	
	private BigInteger idCliente;
	
	private boolean esPorLitros;

	private String fechaPedidoS;
	
	private String cp;
	
	private Integer tipoPedidoP;
	
	
	private Double precioLavadoAutoS;
	private Double precioRevicionLLantasS;
	
    /**
     *
     * @return esPorLitros
     */
    public boolean isEsPorLitros() {
		return esPorLitros;
	}

    /**
     *
     * @param esPorLitros
     * esPorLitros
     */
    public void setEsPorLitros(boolean esPorLitros) {
		this.esPorLitros = esPorLitros;
	}

    /**
     *
     * @return fechaPedidoS
     */
    public String getFechaPedidoS() {
		return fechaPedidoS;
	}

    /**
     *
     * @param fechaPedidoS
     * fechaPedidoS
     */
    public void setFechaPedidoS(String fechaPedidoS) {
		this.fechaPedidoS = fechaPedidoS;
	}

    /**
     *
     * @return cp
     */
    public String getCp() {
		return cp;
	}

    /**
     *
     * @param cp
     * cp
     */
    public void setCp(String cp) {
		this.cp = cp;
	}

    /**
     *
     * @return idCliente
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     * idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return precioLavadoAutoS
     */
    public Double getPrecioLavadoAutoS() {
		return precioLavadoAutoS;
	}

    /**
     *
     * @param precioLavadoAutoS
     * precioLavadoAutoS
     */
    public void setPrecioLavadoAutoS(Double precioLavadoAutoS) {
		this.precioLavadoAutoS = precioLavadoAutoS;
	}

    /**
     *
     * @return tipoPedidoP
     */
    public Integer getTipoPedidoP() {
		return tipoPedidoP;
	}

    /**
     *
     * @param tipoPedidoP
     * tipoPedidoP
     */
    public void setTipoPedidoP(Integer tipoPedidoP) {
		this.tipoPedidoP = tipoPedidoP;
	}

    /**
     *
     * @return precioRevicionLLantasS
     */
    public Double getPrecioRevicionLLantasS() {
		return precioRevicionLLantasS;
	}

    /**
     *
     * @param precioRevicionLLantasS
     * precioRevicionLLantasS
     */
    public void setPrecioRevicionLLantasS(Double precioRevicionLLantasS) {
		this.precioRevicionLLantasS = precioRevicionLLantasS;
	}
    
    
    
	
	
	
	
	
	
}
