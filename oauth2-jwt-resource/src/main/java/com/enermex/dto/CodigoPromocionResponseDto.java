package com.enermex.dto;

/**
 * Nombre de clase 
 * @author abraham
 */
public class CodigoPromocionResponseDto {
	
	private Long    idPromocion;
	private String  descuento;
	private Integer idCombustible;
	private boolean esDescuento;
	private double  monto;
	private boolean esMonto;
	private boolean esInvalido;
	private boolean isCombustible;
	private boolean isLavado;
	private boolean isNeumaticos;
	private String  mensaje;
	
    /**
     * getDescuento
     * @return descuento
     */
    public String getDescuento() {
		return descuento;
	}

    /**
     * 
     * @param descuento
     * setDescuento
     */
    public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

    /**
     * isEsDescuento
     * @return esDescuento
     */
    public boolean isEsDescuento() {
		return esDescuento;
	}

    /**
     * 
     * @param esDescuento
     * setEsDescuento
     */
    public void setEsDescuento(boolean esDescuento) {
		this.esDescuento = esDescuento;
	}

    /**
     * getMonto
     * @return monto
     */
    public double getMonto() {
		return monto;
	}

    /**
     * 
     * @param monto
     * setMonto
     */
    public void setMonto(double monto) {
		this.monto = monto;
	}

    /**
     * isEsMonto
     * @return esMonto
     */
    public boolean isEsMonto() {
		return esMonto;
	}

    /**
     * 
     * @param esMonto
     * setEsMonto
     */
    public void setEsMonto(boolean esMonto) {
		this.esMonto = esMonto;
	}

    /**
     * isEsInvalido
     * @return esValido
     */
    public boolean isEsInvalido() {
		return esInvalido;
	}

    /**
     * 
     * @param esInvalido
     * setEsInvalido
     */
    public void setEsInvalido(boolean esInvalido) {
		this.esInvalido = esInvalido;
	}

    /**
     * getMensaje
     * @return mensaje
     */
    public String getMensaje() {
		return mensaje;
	}

    /**
     * 
     * @param mensaje
     * setMensaje
     */
    public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

    /**
     * isCombustible
     * @return isCombustible
     */
    public boolean isCombustible() {
		return isCombustible;
	}

    /**
     * 
     * @param isCombustible
     * setCombustible
     */
    public void setCombustible(boolean isCombustible) {
		this.isCombustible = isCombustible;
	}

    /**
     * isLavado
     * @return isLavado
     */
    public boolean isLavado() {
		return isLavado;
	}

    /**
     * 
     * @param isLavado
     * setLavado
     */
    public void setLavado(boolean isLavado) {
		this.isLavado = isLavado;
	}

    /**
     * isNeumaticos
     * @return isNeumaticos
     */
    public boolean isNeumaticos() {
		return isNeumaticos;
	}

    /**
     * 
     * @param isNeumaticos
     * setNeumaticos
     */
    public void setNeumaticos(boolean isNeumaticos) {
		this.isNeumaticos = isNeumaticos;
	}

    /**
     * getIdPromocion
     * @return idPromocion
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     * 
     * @param idPromocion
     * setIdPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     * getIdCombustible
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     * 
     * @param idCombustible
     * setIdCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}
	
	
	

}
