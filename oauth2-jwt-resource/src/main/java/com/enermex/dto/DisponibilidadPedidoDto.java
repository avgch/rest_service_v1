package com.enermex.dto;

import java.math.BigInteger;

/**
 * Nombre de  clase
 * @author abraham
 */
public class DisponibilidadPedidoDto {

	private BigInteger idHorario;
	private Long       idDespachador;
	private Integer    idPipa;
	private Integer    idRuta;
	private String     hrInc;
	private String     hrFin;
	
    /**
     * getIdHorario
     * @return idHorario
     */
    public BigInteger getIdHorario() {
		return idHorario;
	}

    /**
     * 
     * @param idHorario
     * setIdHorario
     */
    public void setIdHorario(BigInteger idHorario) {
		this.idHorario = idHorario;
	}

    /**
     * getIdDespachador
     * @return idDespachador
     */
    public Long getIdDespachador() {
		return idDespachador;
	}

    /**
     * 
     * @param idDespachador
     * setIdDespachador
     */
    public void setIdDespachador(Long idDespachador) {
		this.idDespachador = idDespachador;
	}

    /**
     * getIdPipa
     * @return idPipa
     */
    public Integer getIdPipa() {
		return idPipa;
	}

    /**
     * 
     * @param idPipa
     * setIdPipa
     */
    public void setIdPipa(Integer idPipa) {
		this.idPipa = idPipa;
	}

    /**
     * getIdRuta
     * @return idRuta
     */
    public Integer getIdRuta() {
		return idRuta;
	}

    /**
     * 
     * @param idRuta
     * setIdRuta
     */
    public void setIdRuta(Integer idRuta) {
		this.idRuta = idRuta;
	}

    /**
     * getHrInc
     * @return hrInc
     */
    public String getHrInc() {
		return hrInc;
	}

    /**
     * 
     * @param hrInc
     * setHrInc
     */
    public void setHrInc(String hrInc) {
		this.hrInc = hrInc;
	}

    /**
     * getHrFin
     * @return hrFin
     */
    public String getHrFin() {
		return hrFin;
	}

    /**
     * 
     * @param hrFin
     * setHrFin
     */
    public void setHrFin(String hrFin) {
		this.hrFin = hrFin;
	}
	
	
	
	
}
