package com.enermex.dto;

/**
 *
 * @author abraham
 */
public class AppStoreDto {
	
	private Integer idAppStore;
	private String tecnologia;
	private String valor;
	
	/**
	 * 
	 * @return idAppStore
	 * getIdAppStore
	 */
	public Integer getIdAppStore() {
		return idAppStore;
	}
	
	/**
	 * 
	 * @param idAppStore
	 * setIdAppStore
	 */
	public void setIdAppStore(Integer idAppStore) {
		this.idAppStore = idAppStore;
	}
	/**
	 * 
	 * @return tecnologia
	 * getTecnologia
	 */
	public String getTecnologia() {
		return tecnologia;
	}
	/**
	 * 
	 * @param tecnologia
	 * setTecnologia
	 */
	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}
	/**
	 * 
	 * @return valor
	 * getValor
	 */
	public String getValor() {
		return valor;
	}
	/**
	 * 
	 * @param valor
	 * setValor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	/**
	 * Constructor de clase
	 * @param idAppStore
	 * idAppStore
	 * @param tecnologia
	 * tecnologia
	 * @param valor
	 * valor
	 */
	public void setTransformer(Integer idAppStore, String tecnologia, String valor)
	{
		this.idAppStore = idAppStore;
		this.tecnologia = tecnologia;
		this.valor = valor;
	}
}
