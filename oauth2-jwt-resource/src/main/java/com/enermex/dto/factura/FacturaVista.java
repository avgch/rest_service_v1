package com.enermex.dto.factura;

import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enermex.enumerable.ClienteFiscalPersona;
import com.enermex.enumerable.PedidoFacturaEstatus;

/**
 *
 * @author abraham
 */
@Entity
@Table(name = "v_factura")
public class FacturaVista {
  @Id
  @Column(name = "id_factura")
  private BigInteger idFactura;

  @Column(name = "factura_estatus")
  private PedidoFacturaEstatus facturaEstatus;

  @Column(name = "uso_codigo")
  private String usoCodigo;

  @Column(name = "uso")
  private String uso;

  @Column(name = "id_pedido")
  private BigInteger idPedido;

  @Column(name = "uuid")
  private String uuid;

  @Column(name = "fecha_pedido")
  private Calendar fechaPedido;

  @Column(name = "id_tipo_pedido")
  private Integer idTipoPedido;

  @Column(name = "id_promocion")
  private BigInteger idPromocion;

  @Column(name = "has_presion")
  private Boolean hasPresion;

  @Column(name = "presion")
  private String presion;

  @Column(name = "has_lavado")
  private Boolean hasLavado;

  @Column(name = "lavado")
  private String lavado;

  @Column(name = "litros")
  private String litros;

  @Column(name = "combustible")
  private String combustible;

  @Column(name = "servicio")
  private String servicio;

  @Column(name = "total_pagar")
  private String total;
  
  @Column(name = "subtotal")
  private String subtotal;

  @Column(name = "iva")
  private String iva;

  @Column(name = "total")
  private String totalsd;

  @Column(name = "descuento")
  private String descuento;

  @Column(name = "id_combustible")
  private Integer idCombustible;

  @Column(name = "id_cliente")
  private BigInteger idCliente;

  @Column(name = "cliente_nombre")
  private String clienteNombre;

  @Column(name = "cliente_apellido_paterno")
  private String clienteApellidoPaterno;

  @Column(name = "cliente_apellido_materno")
  private String clienteApellidoMaterno;

  @Column(name = "cliente_telefono")
  private String clienteTelefono;

  @Column(name = "codigo")
  private String codigo;

  @Column(name = "estado")
  private String estado;

  @Column(name = "municipio")
  private String municipio;

  @Column(name = "id_fiscal")
  private BigInteger idFiscal;

  @Column(name = "rfc")
  private String rfc;

  @Column(name = "persona")
  private ClienteFiscalPersona persona;

  @Column(name = "razon_social")
  private String razonSocial;

  @Column(name = "cp")
  private String cp;

  @Column(name = "calle")
  private String calle;

  @Column(name = "colonia")
  private String colonia;

  @Column(name = "num_interior")
  private String numInterior;

  @Column(name = "num_exterior")
  private String numExterior;

  @Column(name = "fiscal_estado")
  private String fiscalEstado;

  @Column(name = "fiscal_municipio")
  private String fiscalMunicipio;

  @Column(name = "correo")
  private String correo;

  @Column(name = "telefono")
  private String telefono;

  @Column(name = "codigo_promocion")
  private String codigoPromocion;

  @Column(name = "precio_litro")
  private String precioLitro;

    /**
     *
     * @return
     */
    public BigInteger getIdFactura() {
    return idFactura;
  }

    /**
     *
     * @param idFactura
     */
    public void setIdFactura(BigInteger idFactura) {
    this.idFactura = idFactura;
  }

    /**
     *
     * @return
     */
    public PedidoFacturaEstatus getFacturaEstatus() {
    return facturaEstatus;
  }

    /**
     *
     * @param facturaEstatus
     */
    public void setFacturaEstatus(PedidoFacturaEstatus facturaEstatus) {
    this.facturaEstatus = facturaEstatus;
  }

    /**
     *
     * @return
     */
    public String getUsoCodigo() {
    return usoCodigo;
  }

    /**
     *
     * @param usoCodigo
     */
    public void setUsoCodigo(String usoCodigo) {
    this.usoCodigo = usoCodigo;
  }

    /**
     *
     * @return
     */
    public String getUso() {
    return uso;
  }

    /**
     *
     * @param uso
     */
    public void setUso(String uso) {
    this.uso = uso;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
    return idPedido;
  }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
    this.idPedido = idPedido;
  }

    /**
     *
     * @return
     */
    public String getUuid() {
    return uuid;
  }

    /**
     *
     * @param uuid
     */
    public void setUuid(String uuid) {
    this.uuid = uuid;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaPedido() {
    return fechaPedido;
  }

    /**
     *
     * @param fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido) {
    this.fechaPedido = fechaPedido;
  }

    /**
     *
     * @return
     */
    public Integer getIdTipoPedido() {
    return idTipoPedido;
  }

    /**
     *
     * @param idTipoPedido
     */
    public void setIdTipoPedido(Integer idTipoPedido) {
    this.idTipoPedido = idTipoPedido;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdPromocion() {
    return idPromocion;
  }

    /**
     *
     * @param idPromocion
     */
    public void setIdPromocion(BigInteger idPromocion) {
    this.idPromocion = idPromocion;
  }

    /**
     *
     * @return
     */
    public Boolean getHasPresion() {
    return hasPresion;
  }

    /**
     *
     * @param hasPresion
     */
    public void setHasPresion(Boolean hasPresion) {
    this.hasPresion = hasPresion;
  }

    /**
     *
     * @return
     */
    public String getPresion() {
    return presion;
  }

    /**
     *
     * @param presion
     */
    public void setPresion(String presion) {
    this.presion = presion;
  }

    /**
     *
     * @return
     */
    public Boolean getHasLavado() {
    return hasLavado;
  }

    /**
     *
     * @param hasLavado
     */
    public void setHasLavado(Boolean hasLavado) {
    this.hasLavado = hasLavado;
  }

    /**
     *
     * @return
     */
    public String getLavado() {
    return lavado;
  }

    /**
     *
     * @param lavado
     */
    public void setLavado(String lavado) {
    this.lavado = lavado;
  }

    /**
     *
     * @return
     */
    public String getLitros() {
    return litros;
  }

    /**
     *
     * @param litros
     */
    public void setLitros(String litros) {
    this.litros = litros;
  }

    /**
     *
     * @return
     */
    public String getCombustible() {
    return combustible;
  }

    /**
     *
     * @param combustible
     */
    public void setCombustible(String combustible) {
    this.combustible = combustible;
  }

    /**
     *
     * @return
     */
    public String getServicio() {
    return servicio;
  }

    /**
     *
     * @param servicio
     */
    public void setServicio(String servicio) {
    this.servicio = servicio;
  }

    /**
     *
     * @return
     */
    public String getTotal() {
    return total;
  }

    /**
     *
     * @param total
     */
    public void setTotal(String total) {
    this.total = total;
  }

    /**
     *
     * @return
     */
    public String getTotalsd() {
    return totalsd;
  }

    /**
     *
     * @param totalsd
     */
    public void setTotalsd(String totalsd) {
    this.totalsd = totalsd;
  }

    /**
     *
     * @return
     */
    public Integer getIdCombustible() {
    return idCombustible;
  }

    /**
     *
     * @param idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
    this.idCombustible = idCombustible;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdCliente() {
    return idCliente;
  }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
    this.idCliente = idCliente;
  }

    /**
     *
     * @return
     */
    public String getClienteNombre() {
    return clienteNombre;
  }

    /**
     *
     * @param clienteNombre
     */
    public void setClienteNombre(String clienteNombre) {
    this.clienteNombre = clienteNombre;
  }

    /**
     *
     * @return
     */
    public String getClienteApellidoPaterno() {
    return clienteApellidoPaterno;
  }

    /**
     *
     * @param clienteApellidoPaterno
     */
    public void setClienteApellidoPaterno(String clienteApellidoPaterno) {
    this.clienteApellidoPaterno = clienteApellidoPaterno;
  }

    /**
     *
     * @return
     */
    public String getClienteApellidoMaterno() {
    return clienteApellidoMaterno;
  }

    /**
     *
     * @param clienteApellidoMaterno
     */
    public void setClienteApellidoMaterno(String clienteApellidoMaterno) {
    this.clienteApellidoMaterno = clienteApellidoMaterno;
  }

    /**
     *
     * @return
     */
    public String getClienteTelefono() {
    return clienteTelefono;
  }

    /**
     *
     * @param clienteTelefono
     */
    public void setClienteTelefono(String clienteTelefono) {
    this.clienteTelefono = clienteTelefono;
  }

    /**
     *
     * @return
     */
    public String getCodigo() {
    return codigo;
  }

    /**
     *
     * @param codigo
     */
    public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

    /**
     *
     * @return
     */
    public String getEstado() {
    return estado;
  }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
    this.estado = estado;
  }

    /**
     *
     * @return
     */
    public String getMunicipio() {
    return municipio;
  }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
    this.municipio = municipio;
  }

    /**
     *
     * @return
     */
    public BigInteger getIdFiscal() {
    return idFiscal;
  }

    /**
     *
     * @param idFiscal
     */
    public void setIdFiscal(BigInteger idFiscal) {
    this.idFiscal = idFiscal;
  }

    /**
     *
     * @return
     */
    public String getRfc() {
    return rfc;
  }

    /**
     *
     * @param rfc
     */
    public void setRfc(String rfc) {
    this.rfc = rfc;
  }

    /**
     *
     * @return
     */
    public ClienteFiscalPersona getPersona() {
    return persona;
  }

    /**
     *
     * @param persona
     */
    public void setPersona(ClienteFiscalPersona persona) {
    this.persona = persona;
  }

    /**
     *
     * @return
     */
    public String getRazonSocial() {
    return razonSocial;
  }

    /**
     *
     * @param razonSocial
     */
    public void setRazonSocial(String razonSocial) {
    this.razonSocial = razonSocial;
  }

    /**
     *
     * @return
     */
    public String getCp() {
    return cp;
  }

    /**
     *
     * @param cp
     */
    public void setCp(String cp) {
    this.cp = cp;
  }

    /**
     *
     * @return
     */
    public String getCalle() {
    return calle;
  }

    /**
     *
     * @param calle
     */
    public void setCalle(String calle) {
    this.calle = calle;
  }

    /**
     *
     * @return
     */
    public String getColonia() {
    return colonia;
  }

    /**
     *
     * @param colonia
     */
    public void setColonia(String colonia) {
    this.colonia = colonia;
  }

    /**
     *
     * @return
     */
    public String getNumInterior() {
    return numInterior;
  }

    /**
     *
     * @param numInterior
     */
    public void setNumInterior(String numInterior) {
    this.numInterior = numInterior;
  }

    /**
     *
     * @return
     */
    public String getNumExterior() {
    return numExterior;
  }

    /**
     *
     * @param numExterior
     */
    public void setNumExterior(String numExterior) {
    this.numExterior = numExterior;
  }

    /**
     *
     * @return
     */
    public String getFiscalEstado() {
    return fiscalEstado;
  }

    /**
     *
     * @param fiscalEstado
     */
    public void setFiscalEstado(String fiscalEstado) {
    this.fiscalEstado = fiscalEstado;
  }

    /**
     *
     * @return
     */
    public String getFiscalMunicipio() {
    return fiscalMunicipio;
  }

    /**
     *
     * @param fiscalMunicipio
     */
    public void setFiscalMunicipio(String fiscalMunicipio) {
    this.fiscalMunicipio = fiscalMunicipio;
  }

    /**
     *
     * @return
     */
    public String getCorreo() {
    return correo;
  }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
    this.correo = correo;
  }

    /**
     *
     * @return
     */
    public String getDescuento() {
    return descuento;
  }

    /**
     *
     * @param descuento
     */
    public void setDescuento(String descuento) {
    this.descuento = descuento;
  }

    /**
     *
     * @return
     */
    public String getTelefono() {
    return telefono;
  }

    /**
     *
     * @param telefono
     */
    public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
  
    /**
     *
     * @return
     */
    public String getSubtotal() {
    return subtotal;
  }

    /**
     *
     * @param subtotal
     */
    public void setSubtotal(String subtotal) {
    this.subtotal = subtotal;
  }

    /**
     *
     * @return
     */
    public String getIva() {
    return iva;
  }

    /**
     *
     * @param iva
     */
    public void setIva(String iva) {
    this.iva = iva;
  }

    /**
     *
     * @return
     */
    public String getCodigoPromocion() {
    return codigoPromocion;
  }

    /**
     *
     * @param codigoPromocion
     */
    public void setCodigoPromocion(String codigoPromocion) {
    this.codigoPromocion = codigoPromocion;
  }

    /**
     *
     * @return
     */
    public String getPrecioLitro() {
    return precioLitro;
  }

    /**
     *
     * @param precioLitro
     */
    public void setPrecioLitro(String precioLitro) {
    this.precioLitro = precioLitro;
  }
}
