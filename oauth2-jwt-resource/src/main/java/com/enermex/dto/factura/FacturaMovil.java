package com.enermex.dto.factura;

import java.math.BigInteger;
import java.util.Calendar;

import com.enermex.enumerable.PedidoFacturaEstatus;

/**
 *
 * @author abraham
 */
public class FacturaMovil {
   private BigInteger idFactura;
   private BigInteger idPedido;
   private BigInteger idFiscal;
   private PedidoFacturaEstatus estatus;
   private String usoCfdi;
   private String usoCfdiTexto;
   private Double total;
   private Boolean disponible;
   private Calendar fechaPedido;
   private String uuid;

    /**
     *
     * @return
     */
    public BigInteger getIdFactura() {
     return idFactura;
   }

    /**
     *
     * @param idFactura
     */
    public void setIdFactura(BigInteger idFactura) {
     this.idFactura = idFactura;
   }

    /**
     *
     * @return
     */
    public BigInteger getIdPedido() {
     return idPedido;
   }

    /**
     *
     * @param idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
     this.idPedido = idPedido;
   }

    /**
     *
     * @return
     */
    public BigInteger getIdFiscal() {
     return idFiscal;
   }

    /**
     *
     * @param idFiscal
     */
    public void setIdFiscal(BigInteger idFiscal) {
     this.idFiscal = idFiscal;
   }

    /**
     *
     * @return
     */
    public PedidoFacturaEstatus getEstatus() {
     return estatus;
   }

    /**
     *
     * @param estatus
     */
    public void setEstatus(PedidoFacturaEstatus estatus) {
     this.estatus = estatus;
   }

    /**
     *
     * @return
     */
    public String getUsoCfdi() {
     return usoCfdi;
   }

    /**
     *
     * @param usoCfdi
     */
    public void setUsoCfdi(String usoCfdi) {
     this.usoCfdi = usoCfdi;
   }

    /**
     *
     * @return
     */
    public String getUsoCfdiTexto() {
     return usoCfdiTexto;
   }

    /**
     *
     * @param usoCfdiTexto
     */
    public void setUsoCfdiTexto(String usoCfdiTexto) {
     this.usoCfdiTexto = usoCfdiTexto;
   }

    /**
     *
     * @return
     */
    public Double getTotal() {
     return total;
   }

    /**
     *
     * @param total
     */
    public void setTotal(Double total) {
     this.total = total;
   }

    /**
     *
     * @return
     */
    public Boolean getDisponible() {
     return disponible;
   }

    /**
     *
     * @param disponible
     */
    public void setDisponible(Boolean disponible) {
     this.disponible = disponible;
   }

    /**
     *
     * @return
     */
    public Calendar getFechaPedido() {
     return fechaPedido;
   }

    /**
     *
     * @param fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido) {
     this.fechaPedido = fechaPedido;
   }

    /**
     *
     * @return
     */
    public String getUuid() {
     return uuid;
   }

    /**
     *
     * @param uuid
     */
    public void setUuid(String uuid) {
     this.uuid = uuid;
   }
}
