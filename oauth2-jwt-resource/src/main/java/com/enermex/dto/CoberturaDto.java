package com.enermex.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de clase
 * @author abraham
 */
public class CoberturaDto {

	
	private List<Integer> edos;
	
	private List<Integer> municipios;
	
    /**
     * Constructo de clase 
     */
    public CoberturaDto() {
		
		edos       = new ArrayList<Integer>();
		municipios = new ArrayList<Integer>();
		
	}

    /**
     * getEdos
     * @return edos
     */
    public List<Integer> getEdos() {
		return edos;
	}

    /**
     * 
     * @param edos
     * setEdos
     */
    public void setEdos(List<Integer> edos) {
		this.edos = edos;
	}

    /**
     * getMunicipios
     * @return municipios
     */
    public List<Integer> getMunicipios() {
		return municipios;
	}

    /**
     * 
     * @param municipios
     * setMunicipios
     */
    public void setMunicipios(List<Integer> municipios) {
		this.municipios = municipios;
	}
	
	
	
	
	
}
