package com.enermex.dto.tarifa;

import java.util.List;

import com.enermex.modelo.TarifaLavadoEntity;
import com.enermex.modelo.TarifaPresionEntity;

/**
 * TarifaLista
 */
public class TarifaLista {
  private List<TarifaLavadoEntity> lavado;
  private List<TarifaPresionEntity> presion;

    /**
     *
     * @return
     */
    public List<TarifaLavadoEntity> getLavado() {
    return lavado;
  }

    /**
     *
     * @param lavado
     */
    public void setLavado(List<TarifaLavadoEntity> lavado) {
    this.lavado = lavado;
  }

    /**
     *
     * @return
     */
    public List<TarifaPresionEntity> getPresion() {
    return presion;
  }

    /**
     *
     * @param presion
     */
    public void setPresion(List<TarifaPresionEntity> presion) {
    this.presion = presion;
  }
}