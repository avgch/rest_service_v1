package com.enermex.dto.tarifa;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Debe contener toda la información de códigos postales respecto a las tarifas
 * Inmutable
 */
@Entity
@Table(name = "v_tarifario")
public class TarifaVista {
  @Id
  @Column(name = "id_codigo")
  String idCodigo;

  @Column(name = "precio_lavado")
  String precioLavado;

  @Column(name = "precio_presion")
  String precioPresion;

  @Column(name = "fecha_creacion")
  Calendar fechaCreacion;

  @Column(name = "id_estado")
  Integer idEstado;

  @Column(name = "estado")
  String estado;

  @Column(name = "id_municipio")
  Integer idMunicipio;

  @Column(name = "municipio")
  String municipio;

    /**
     *
     * @return
     */
    public String getIdCodigo() {
    return idCodigo;
  }

    /**
     *
     * @param idCodigo
     */
    public void setIdCodigo(String idCodigo) {
    this.idCodigo = idCodigo;
  }

    /**
     *
     * @return
     */
    public String getPrecioLavado() {
    return precioLavado;
  }

    /**
     *
     * @param precioLavado
     */
    public void setPrecioLavado(String precioLavado) {
    this.precioLavado = precioLavado;
  }

    /**
     *
     * @return
     */
    public String getPrecioPresion() {
    return precioPresion;
  }

    /**
     *
     * @param precioPresion
     */
    public void setPrecioPresion(String precioPresion) {
    this.precioPresion = precioPresion;
  }

    /**
     *
     * @return
     */
    public Calendar getFechaCreacion() {
    return fechaCreacion;
  }

    /**
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

    /**
     *
     * @return
     */
    public Integer getIdEstado() {
    return idEstado;
  }

    /**
     *
     * @param idEstado
     */
    public void setIdEstado(Integer idEstado) {
    this.idEstado = idEstado;
  }

    /**
     *
     * @return
     */
    public String getEstado() {
    return estado;
  }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
    this.estado = estado;
  }

    /**
     *
     * @return
     */
    public Integer getIdMunicipio() {
    return idMunicipio;
  }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(Integer idMunicipio) {
    this.idMunicipio = idMunicipio;
  }

    /**
     *
     * @return
     */
    public String getMunicipio() {
    return municipio;
  }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
    this.municipio = municipio;
  }
}