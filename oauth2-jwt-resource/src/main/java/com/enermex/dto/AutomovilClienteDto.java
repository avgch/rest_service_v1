/*##############################################################################
# Nombre del Programa : AutomovilDto.java                                      #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/


package com.enermex.dto;

import com.enermex.modelo.AutomovilCliente;

/**
 *
 * @author abraham
 */
public class AutomovilClienteDto extends  AutomovilCliente{
	

	
	private String marca;
	
	private String modelo;
	
	/**
	 * Constructor de clase
	 */
	public AutomovilClienteDto() {
		
		
	}

	/**
	 * 
     * @return  marca
     * getMarca
	 */
	public String getMarca() {
		return marca;
	}
    
	/**
	 * 
     * @param marca
     * setMarca
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}

	/**
	 * 
     * @return modelo
     * getModelo
	 */
	public String getModelo() {
		return modelo;
	}

	/**
	 * 
     * @param modelo
     * setModelo
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
		
	}
	
	
   
	


}
