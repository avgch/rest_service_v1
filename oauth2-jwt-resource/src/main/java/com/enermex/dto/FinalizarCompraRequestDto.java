package com.enermex.dto;

import java.math.BigInteger;

/**
 * Nombre de clase
 * @author abraham
 */
public class FinalizarCompraRequestDto {
	
	private String litros;
	
	private String monto;
	
	private BigInteger idPedido;

    /**
     * getLitros
     * @return litros
     */
    public String getLitros() {
		return litros;
	}

    /**
     * 
     * @param litros
     * setLitros
     */
    public void setLitros(String litros) {
		this.litros = litros;
	}

    /**
     * getMonto
     * @return monto
     */
    public String getMonto() {
		return monto;
	}

    /**
     * 
     * @param monto
     * setMonto
     */
    public void setMonto(String monto) {
		this.monto = monto;
	}

    /**
     * getIdPedido
     * @return idPedido
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     * 
     * @param idPedido
     * setIdPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}
	
	
	
	
	
	

}
