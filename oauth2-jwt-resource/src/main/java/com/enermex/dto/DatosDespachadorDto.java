package com.enermex.dto;

/**
 * Nombre de clase 
 * @author abraham
 */
public class DatosDespachadorDto {
	
	private Long   id;
	private String nombreCompleto;
	private String uriFoto;
	private String telefono;
	
    /**
     * Constructor de clase 
     */
    public DatosDespachadorDto(){
		
	}

    /**
     * getId
     * @return id
     */
    public Long getId() {
		return id;
	}

    /**
     * 
     * @param id
     * setId
     */
    public void setId(Long id) {
		this.id = id;
	}

    /**
     * getNombreCompleto
     * @return nombreCompleto
     */
    public String getNombreCompleto() {
		return nombreCompleto;
	}

    /**
     * 
     * @param nombreCompleto
     * setNombreCompleto
     */
    public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

    /**
     * getUriFoto
     * @return uriFoto
     */
    public String getUriFoto() {
		return uriFoto;
	}

    /**
     * 
     * @param uriFoto
     * setUriFoto
     */
    public void setUriFoto(String uriFoto) {
		this.uriFoto = uriFoto;
	}

    /**
     * getTelefono
     * @return telefono
     */
    public String getTelefono() {
		return telefono;
	}

    /**
     * 
     * @param telefono
     * setTelefono
     */
    public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	
	

}
