package com.enermex.dto.legales;

/**
 *
 * @author abraham
 */
public class LegalesText {
  private String text;

    /**
     *
     * @return
     */
    public String getText() {
    return text;
  }

    /**
     *
     * @param text
     */
    public void setText(String text) {
    this.text = text;
  }
}