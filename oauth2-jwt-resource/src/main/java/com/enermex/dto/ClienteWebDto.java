package com.enermex.dto;

import java.math.BigInteger;
import java.util.Calendar;

/**
 * Nombre de clase
 * @author abraham
 */
public class ClienteWebDto {
	             

	
	private BigInteger idCliente;
	private String     nombreCompleto;
	private String     email;
	private Calendar   fechaCreacion;
	private String     telefonoCelular;
	private String     tipoPlan;   
	private Boolean    bloqueo;
	
    /**
     * Contructor de clase
     */
    public ClienteWebDto() {
		
		this.fechaCreacion = Calendar.getInstance();
	}
	
    /**
     * getIdCliente
     * @return idCliente
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     * 
     * @param idCliente
     * setIdCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     * getNombreCompleto
     * @return nombreCompleto
     */
    public String getNombreCompleto() {
		return nombreCompleto;
	}

    /**
     * 
     * @param nombreCompleto
     * setNombreCompleto
     */
    public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

    /**
     * getEmail
     * @return email
     */
    public String getEmail() {
		return email;
	}

    /**
     * 
     * @param email
     * setEmail
     */
    public void setEmail(String email) {
		this.email = email;
	}

    /**
     * getFechaCreacion
     * @return fechaCreacion
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     * 
     * @param fechaCreacion
     * setFechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     * getTelefonoCelular
     * @return telefonoCelular
     */
    public String getTelefonoCelular() {
		return telefonoCelular;
	}

    /**
     * 
     * @param telefonoCelular
     * setTelefonoCelular
     */
    public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

    /**
     * getTipoPlan
     * @return tipoPlan
     */
    public String getTipoPlan() {
		return tipoPlan;
	}

    /**
     * 
     * @param tipoPlan
     * setTipoPlan
     */
    public void setTipoPlan(String tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

    /**
     * getBloqueo
     * @return bloqueo
     */
    public Boolean getBloqueo() {
		return bloqueo;
	}

    /**
     * 
     * @param bloqueo
     * setBloqueo
     */
    public void setBloqueo(Boolean bloqueo) {
		this.bloqueo = bloqueo;
	}
	
	
	
	

}
