package com.enermex.dto;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

import com.enermex.modelo.DireccionCliente;

/**
 * Nombre de clase
 * @author abraham
 */
public class PedidoDespachador {
	
	
    private BigInteger idPedido;
    private BigInteger idUsuario;
    private BigInteger idCliente;
    private BigInteger idDireccion;
    private DireccionCliente direccion;
    private String     ruta ;   
    private Calendar    fecha_pedido;
    private double     montoCarga;   
    private String     litroCarga;
    private boolean    verificaPresionNeumatico; 
    private String     llantasDelanterasPresion;
    private String     llantasTraserasPresion;
    private boolean    cargaCas;
    private boolean    lavadoAuto; 
    private boolean    revisionNeumaticos;
    private String     detalle;
    private Integer    idCombustible;
    private String     nombreCliente;
    private Integer    estatusPedido;
    private Double     totalPagar;
    private String     uuid;
    private Double     totalImporte;
    private Double     totalConDescuento;
    
    /**
     *
     * @return idPedido
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     * idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     *
     * @return idUsuario
     */
    public BigInteger getIdUsuario() {
		return idUsuario;
	}

    /**
     *
     * @param idUsuario
     * idUsuario
     */
    public void setIdUsuario(BigInteger idUsuario) {
		this.idUsuario = idUsuario;
	}

    /**
     *
     * @return idCliente
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     * idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return idDireccion
     */
    public BigInteger getIdDireccion() {
		return idDireccion;
	}

    /**
     *
     * @param idDireccion
     * idDireccion
     */
    public void setIdDireccion(BigInteger idDireccion) {
		this.idDireccion = idDireccion;
	}

    /**
     *
     * @return direccion
     */
    public DireccionCliente getDireccion() {
		return direccion;
	}

    /**
     *
     * @param direccion
     * direccion
     */
    public void setDireccion(DireccionCliente direccion) {
		this.direccion = direccion;
	}

    /**
     *
     * @return fecha_pedido
     */
    public Calendar getFecha_pedido() {
		return fecha_pedido;
	}

    /**
     *
     * @param fecha_pedido
     * fecha_pedido
     */
    public void setFecha_pedido(Calendar fecha_pedido) {
		this.fecha_pedido = fecha_pedido;
	}

    /**
     *
     * @return montoCarga
     */
    public double getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     * montoCarga
     */
    public void setMontoCarga(double montoCarga) {
		this.montoCarga = montoCarga;
	}

    /**
     *
     * @return litroCarga
     */
    public String getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     * litroCarga
     */
    public void setLitroCarga(String litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     *
     * @return verificaPresionNeumatico
     */
    public boolean isVerificaPresionNeumatico() {
		return verificaPresionNeumatico;
	}

    /**
     *
     * @param verificaPresionNeumatico
     * verificaPresionNeumatico
     */
    public void setVerificaPresionNeumatico(boolean verificaPresionNeumatico) {
		this.verificaPresionNeumatico = verificaPresionNeumatico;
	}

    /**
     *
     * @return llantasDelanterasPresion
     */
    public String getLlantasDelanterasPresion() {
		return llantasDelanterasPresion;
	}

    /**
     *
     * @param llantasDelanterasPresion
     * llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
		this.llantasDelanterasPresion = llantasDelanterasPresion;
	}

    /**
     *
     * @return llantasTraserasPresion
     */
    public String getLlantasTraserasPresion() {
		return llantasTraserasPresion;
	}

    /**
     *
     * @param llantasTraserasPresion
     * llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion) {
		this.llantasTraserasPresion = llantasTraserasPresion;
	}

    /**
     *
     * @return cargaCas
     */
    public boolean isCargaCas() {
		return cargaCas;
	}

    /**
     *
     * @param cargaCas
     * cargaCas
     */
    public void setCargaCas(boolean cargaCas) {
		this.cargaCas = cargaCas;
	}

    /**
     *
     * @return lavadoAuto
     */
    public boolean isLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     * lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     *
     * @return lavadoAuto
     */
    public boolean isRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     * revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     *
     * @return ruta
     */
    public String getRuta() {
		return ruta;
	}

    /**
     *
     * @param ruta
     * ruta
     */
    public void setRuta(String ruta) {
		this.ruta = ruta;
	}

    /**
     *
     * @return detalle
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     *
     * @param detalle
     * detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
    /**
     *
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return nombreCliente
     */
    public String getNombreCliente() {
		return nombreCliente;
	}

    /**
     *
     * @param nombreCliente
     * nombreCliente
     */
    public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

    /**
     *
     * @return estatusPedido
     */
    public Integer getEstatusPedido() {
		return estatusPedido;
	}

    /**
     *
     * @param estatusPedido
     * estatusPedido
     */
    public void setEstatusPedido(Integer estatusPedido) {
		this.estatusPedido = estatusPedido;
	}

    /**
     *
     * @return totalPagar
     */
    public Double getTotalPagar() {
		return totalPagar;
	}

    /**
     *
     * @param totalPagar
     * totalPagar
     */
    public void setTotalPagar(Double totalPagar) {
		this.totalPagar = totalPagar;
	}

    /**
     *
     * @return uuid
     */
    public String getUuid() {
		return uuid;
	}

    /**
     *
     * @param uuid
     * uuid
     */
    public void setUuid(String uuid) {
		this.uuid = uuid;
	}

    /**
     *
     * @return totalImporte
     */
    public Double getTotalImporte() {
		return totalImporte;
	}

    /**
     *
     * @param totalImporte
     * totalImporte
     */
    public void setTotalImporte(Double totalImporte) {
		this.totalImporte = totalImporte;
	}

    /**
     *
     * @return totalConDescuento
     */
    public Double getTotalConDescuento() {
		return totalConDescuento;
	}

    /**
     *
     * @param totalConDescuento
     * totalConDescuento
     */
    public void setTotalConDescuento(Double totalConDescuento) {
		this.totalConDescuento = totalConDescuento;
	}
	
    
	

}
