package com.enermex.dto;

import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;

import com.enermex.modelo.Pedido;
import com.enermex.modelo.RutaEntity;
import com.enermex.modelo.TipoPedido;

/**
 * Nombre de clase
 * @author abraham
 */
public class PedidoClienteDto extends Pedido {
	
	
	 private BigInteger idPedido;
	 private Integer    idDireccion;
	 private Long       idDespachador;
	 private RutaEntity ruta;
	 private Integer    idAutomovil;
	 private Integer    idCombustible;
	 private String     litroCarga;
	 private Double     montoCarga;
	 private String     llantasDelanterasPresion;
	 private String     llantasTraserasPresion;
	 private Calendar   fechaCreacion;
	 private Calendar   fechaPedido;
	 private Long       idPromocion;
	 private Boolean    cargaGas;
	 private Boolean    lavadoAuto;
	 private Boolean    revisionNeumaticos;
	 private Integer    estatusPedido;
	 private String     detalle;
	 private Double     subTotal;
	 private Double     total;
	 private Double     iva;
	 private Double     totalPagar;
	 private Double     descuento;
	 private Integer    idTarjeta;
	 private BigInteger idHorarioServicio;
	 private String     uuid;
	 private String     tiempoTraslado;
	 private String     distancia;
	 private TipoPedido tipoPedido;
	 private Calendar   fechaHoraInicio;
	 private Calendar   fechaHoraFin;
	 private Double     precioPresionNeumaticos;
	 private Boolean    calificado;
	 private Double     costoServicio;
	 private Integer    idPipa;
	 private String     latitudDespachador;
	 private String     longitudDespachador;
	 private Double     precioPorLitro;
	 private Double     totalImporte;	
     private Double     totalConDescuento;   
     private Double     precioPorLitroFinal;
	 private String     litrosDespachados;
	 private Calendar   horaEstimada;
    
    /**
     * Contructor de clase recibe un objeto pedido
     * @param p
     * pedido
     */
    public PedidoClienteDto(Pedido p) {
		super();
		this.idPedido = p.getIdPedido();
		this.idDireccion = p.getIdDireccion();
		this.idDespachador = p.getIdDespachador();
		this.idAutomovil = p.getIdAutomovil();
		this.idCombustible = p.getIdCombustible();
		this.litroCarga = p.getLitroCarga();
		this.montoCarga = p.getMontoCarga();
		this.llantasDelanterasPresion = p.getLlantasDelanterasPresion();
		this.llantasTraserasPresion = p.getLlantasTraserasPresion();
		this.fechaCreacion = p.getFechaCreacion();
		this.fechaPedido = p.getFechaPedido();
		this.idPromocion = p.getIdPromocion();
		this.cargaGas = p.isCargaGas();
		this.lavadoAuto = p.isLavadoAuto();
		this.revisionNeumaticos = p.isRevisionNeumaticos();
		this.estatusPedido = p.getEstatusPedido();
		this.detalle = p.getDetalle();
		this.subTotal = p.getSubTotal();
		this.iva = p.getIva();
		this.totalPagar = p.getTotalPagar();
		this.descuento = p.getDescuento();
		this.idTarjeta = p.getIdTarjeta();
		this.idHorarioServicio = p.getIdHorarioServicio();
		this.uuid = p.getUuid();
		this.tiempoTraslado = p.getTiempoTraslado();
		this.distancia = p.getDistancia();
		this.tipoPedido = p.getTipoPedido();
		this.fechaHoraInicio = p.getFechaHoraInicio();
		this.fechaHoraFin = p.getFechaHoraFin();
		this.precioPresionNeumaticos = p.getPrecioPresionNeumaticos();
		this.calificado = p.getCalificado();
		this.costoServicio = p.getCostoServicio();
		this.idPipa = p.getIdPipa();
		this.litrosDespachados = p.getLitrosDespachados();
		this.precioPorLitroFinal = p.getPrecioPorLitroFinal();
		this.latitudDespachador = p.getLatitudDespachador();
		this.longitudDespachador = p.getLatitudDespachador();
		this.precioPorLitro = p.getPrecioPorLitro();
		this.totalImporte = p.getTotalImporte();
	    this.totalConDescuento = p.getTotalConDescuento();
		
	}

    /**
     *
     * @return idPedido
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     * idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     *
     * @return idDireccion
     */
    public Integer getIdDireccion() {
		return idDireccion;
	}

    /**
     *
     * @param idDireccion
     * idDireccion
     */
    public void setIdDireccion(Integer idDireccion) {
		this.idDireccion = idDireccion;
	}

    /**
     *
     * @return idDespachador
     */
    public Long getIdDespachador() {
		return idDespachador;
	}

    /**
     *
     * @param idDespachador
     * idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
		this.idDespachador = idDespachador;
	}

    /**
     *
     * @return ruta
     */
    public RutaEntity getRuta() {
		return ruta;
	}

    /**
     *
     * @param ruta
     * ruta
     */
    public void setRuta(RutaEntity ruta) {
		this.ruta = ruta;
	}

    /**
     *
     * @return idAutomovil
     */
    public Integer getIdAutomovil() {
		return idAutomovil;
	}

    /**
     *
     * @param idAutomovil
     * idAutomovil
     */
    public void setIdAutomovil(Integer idAutomovil) {
		this.idAutomovil = idAutomovil;
	}

    /**
     *
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return litroCarga
     */
    public String getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     * litroCarga
     */
    public void setLitroCarga(String litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     *
     * @return montoCarga
     */
    public Double getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     * montoCarga
     */
    public void setMontoCarga(Double montoCarga) {
		this.montoCarga = montoCarga;
	}

    /**
     *
     * @return llantasDelanterasPresion
     */ 
    public String getLlantasDelanterasPresion() {
		return llantasDelanterasPresion;
	}

    /**
     *
     * @param llantasDelanterasPresion
     * llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
		this.llantasDelanterasPresion = llantasDelanterasPresion;
	}

    /**
     *
     * @return llantasTraserasPresion
     */
    public String getLlantasTraserasPresion() {
		return llantasTraserasPresion;
	}

    /**
     *
     * @param llantasTraserasPresion
     * llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion) {
		this.llantasTraserasPresion = llantasTraserasPresion;
	}

    /**
     *
     * @return fechaCreacion
     */
    public Calendar getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     *
     * @return fechaPedido
     */
    public Calendar getFechaPedido() {
		return fechaPedido;
	}

    /**
     *
     * @param fechaPedido
     * fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

    /**
     *
     * @return idPromocion
     */
    public Long getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     * idPromocion
     */
    public void setIdPromocion(Long idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     *
     * @return cargaGas
     */
    public Boolean getCargaGas() {
		return cargaGas;
	}

    /**
     *
     * @param cargaGas
     * cargaGas
     */
    public void setCargaGas(Boolean cargaGas) {
		this.cargaGas = cargaGas;
	}

    /**
     *
     * @return lavadoAuto
     */
    public Boolean getLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     * lavadoAuto
     */
    public void setLavadoAuto(Boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     *
     * @return revisionNeumaticos
     */
    public Boolean getRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     * revisionNeumaticos
     */
    public void setRevisionNeumaticos(Boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     *
     * @return estatusPedido
     */
    public Integer getEstatusPedido() {
		return estatusPedido;
	}

    /**
     *
     * @param estatusPedido
     * estatusPedido
     */
    public void setEstatusPedido(Integer estatusPedido) {
		this.estatusPedido = estatusPedido;
	}

    /**
     *
     * @return detalle
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     *
     * @param detalle
     * detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

    /**
     *
     * @return subTotal
     */
    public Double getSubTotal() {
		return subTotal;
	}

    /**
     *
     * @param subTotal
     * subTotal
     */
    public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

    /**
     *
     * @return total
     */
    public Double getTotal() {
		return total;
	}

    /**
     *
     * @param total
     * total
     */
    public void setTotal(Double total) {
		this.total = total;
	}

    /**
     *
     * @return iva
     */
    public Double getIva() {
		return iva;
	}

    /**
     *
     * @param iva
     * iva
     */
    public void setIva(Double iva) {
		this.iva = iva;
	}

    /**
     *
     * @return totalPagar
     */
    public Double getTotalPagar() {
		return totalPagar;
	}

    /**
     *
     * @param totalPagar
     * totalPagar
     */
    public void setTotalPagar(Double totalPagar) {
		this.totalPagar = totalPagar;
	}

    /**
     *
     * @return descuento
     */
    public Double getDescuento() {
		return descuento;
	}

    /**
     *
     * @param descuento
     * descuento
     */
    public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

    /**
     *
     * @return idTarjeta
     */
    public Integer getIdTarjeta() {
		return idTarjeta;
	}

    /**
     *
     * @param idTarjeta
     * idTarjeta
     */
    public void setIdTarjeta(Integer idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

    /**
     *
     * @return idHorarioServicio
     */
    public BigInteger getIdHorarioServicio() {
		return idHorarioServicio;
	}

    /**
     *
     * @param idHorarioServicio
     * idHorarioServicio
     */
    public void setIdHorarioServicio(BigInteger idHorarioServicio) {
		this.idHorarioServicio = idHorarioServicio;
	}

    /**
     *
     * @return uuid
     */
    public String getUuid() {
		return uuid;
	}

    /**
     *
     * @param uuid
     * uuid
     */
    public void setUuid(String uuid) {
		this.uuid = uuid;
	}

    /**
     *
     * @return tiempoTraslado
     */
    public String getTiempoTraslado() {
		return tiempoTraslado;
	}

    /**
     *
     * @param tiempoTraslado
     * tiempoTraslado
     */
    public void setTiempoTraslado(String tiempoTraslado) {
		this.tiempoTraslado = tiempoTraslado;
	}

    /**
     *
     * @return distancia
     */
    public String getDistancia() {
		return distancia;
	}

    /**
     *
     * @param distancia
     * distancia
     */
    public void setDistancia(String distancia) {
		this.distancia = distancia;
	}

    /**
     *
     * @return tipoPedido
     */ 
    public TipoPedido getTipoPedido() {
		return tipoPedido;
	}

    /**
     *
     * @param tipoPedido
     * tipoPedido
     */
    public void setTipoPedido(TipoPedido tipoPedido) {
		this.tipoPedido = tipoPedido;
	}

    /**
     *
     * @return fechaHoraInicio
     */
    public Calendar getFechaHoraInicio() {
		return fechaHoraInicio;
	}

    /**
     *
     * @param fechaHoraInicio
     * fechaHoraInicio
     */
    public void setFechaHoraInicio(Calendar fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

    /**
     *
     * @return fechaHoraFin
     */
    public Calendar getFechaHoraFin() {
		return fechaHoraFin;
	}

    /**
     *
     * @param fechaHoraFin
     * fechaHoraFin
     */
    public void setFechaHoraFin(Calendar fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

    /**
     *
     * @return precioPresionNeumaticos
     */
    public Double getPrecioPresionNeumaticos() {
		return precioPresionNeumaticos;
	}

    /**
     *
     * @param precioPresionNeumaticos
     * precioPresionNeumaticos
     */
    public void setPrecioPresionNeumaticos(Double precioPresionNeumaticos) {
		this.precioPresionNeumaticos = precioPresionNeumaticos;
	}

    /**
     *
     * @return calificado
     */
    public Boolean getCalificado() {
		return calificado;
	}

    /**
     *
     * @param calificado
     * calificado
     */
    public void setCalificado(Boolean calificado) {
		this.calificado = calificado;
	}

    /**
     *
     * @return costoServicio
     */
    public Double getCostoServicio() {
		return costoServicio;
	}

    /**
     *
     * @param costoServicio
     * costoServicio
     */
    public void setCostoServicio(Double costoServicio) {
		this.costoServicio = costoServicio;
	}

    /**
     *
     * @return idPipa
     */
    public Integer getIdPipa() {
		return idPipa;
	}

    /**
     *
     * @param idPipa
     * idPipa
     */
    public void setIdPipa(Integer idPipa) {
		this.idPipa = idPipa;
	}

    /**
     *
     * @return litrosDespachados
     */
    public String getLitrosDespachados() {
		return litrosDespachados;
	}

    /**
     *
     * @param litrosDespachados
     * litrosDespachados
     */
    public void setLitrosDespachados(String litrosDespachados) {
		this.litrosDespachados = litrosDespachados;
	}

    /**
     *
     * @return latitudDespachador
     */
    public String getLatitudDespachador() {
		return latitudDespachador;
	}

    /**
     *
     * @param latitudDespachador
     * latitudDespachador
     */
    public void setLatitudDespachador(String latitudDespachador) {
		this.latitudDespachador = latitudDespachador;
	}

    /**
     *
     * @return longitudDespachador
     */
    public String getLongitudDespachador() {
		return longitudDespachador;
	}

    /**
     *
     * @param longitudDespachador
     * longitudDespachador
     */
    public void setLongitudDespachador(String longitudDespachador) {
		this.longitudDespachador = longitudDespachador;
	}

    /**
     *
     * @return precioPorLitro
     */
    public Double getPrecioPorLitro() {
		return precioPorLitro;
	}

    /**
     *
     * @param precioPorLitro
     * precioPorLitro
     */
    public void setPrecioPorLitro(Double precioPorLitro) {
		this.precioPorLitro = precioPorLitro;
	}

    /**
     *
     * @return totalImporte
     */
    public Double getTotalImporte() {
		return totalImporte;
	}

    /**
     *
     * @param totalImporte
     * totalImporte
     */
    public void setTotalImporte(Double totalImporte) {
		this.totalImporte = totalImporte;
	}

    /**
     *
     * @return totalConDescuento
     */
    public Double getTotalConDescuento() {
		return totalConDescuento;
	}

    /**
     *
     * @param totalConDescuento
     * totalConDescuento
     */
    public void setTotalConDescuento(Double totalConDescuento) {
		this.totalConDescuento = totalConDescuento;
	}

    /**
     *
     * @return precioPorLitroFinal
     */
    public Double getPrecioPorLitroFinal() {
		return precioPorLitroFinal;
	}

    /**
     *
     * @param precioPorLitroFinal
     * precioPorLitroFinal
     */
    public void setPrecioPorLitroFinal(Double precioPorLitroFinal) {
		this.precioPorLitroFinal = precioPorLitroFinal;
	}

    /**
     *
     * @return horaEstimada
     */
    public Calendar getHoraEstimada() {
		return horaEstimada;
	}

    /**
     *
     * @param horaEstimada
     * horaEstimada
     */
    public void setHoraEstimada(Calendar horaEstimada) {
		this.horaEstimada = horaEstimada;
	}
	 
	 
	
	

}
