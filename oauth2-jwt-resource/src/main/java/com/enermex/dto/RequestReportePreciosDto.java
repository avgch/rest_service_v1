package com.enermex.dto;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * Nombre de clase
 * @author abraham
 */
public class RequestReportePreciosDto {

	
	private Date fechaInc;
	private Date fechaFin;
	
    /**
     *
     * @return fechaInc
     */
    public Date getFechaInc() {
		return fechaInc;
	}

    /**
     *
     * @param fechaInc
     * fechaInc
     */
    public void setFechaInc(Date fechaInc) {
		this.fechaInc = fechaInc;
	}

    /**
     *
     * @return fechaFin
     */
    public Date getFechaFin() {
		return fechaFin;
	}

    /**
     *
     * @param fechaFin
     * fechaFin
     */
    public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	
	
	
	
}
