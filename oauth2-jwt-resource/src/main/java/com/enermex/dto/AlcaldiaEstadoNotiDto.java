package com.enermex.dto;

/**
 *
 * @author abraham
 */
public class AlcaldiaEstadoNotiDto {
	
	
	 private Integer idAlcaldia;

	  private Integer idEstado;

	  private String alcaldia;

	  private String edo;
	  
    /**
     * 
     * @return idAlcaldia
     * getIdAlcaldia
     */
	public Integer getIdAlcaldia() {
		return idAlcaldia;
	}

	/**
	 * 
	 * @param idAlcaldia
	 * setIdAlcaldia
	 */
	public void setIdAlcaldia(Integer idAlcaldia) {
		this.idAlcaldia = idAlcaldia;
	}

	/**
	 * 
	 * @return idEstado
	 * getIdEstado
	 */
	public Integer getIdEstado() {
		return idEstado;
	}

	/**
	 * 
	 * @param idEstado
	 * setIdEstado
	 */
	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}
    /**
     * 
     * @return alcaldia
     * getAlcaldia
     */
	public String getAlcaldia() {
		return alcaldia;
	}
    /**
     * 
     * @param alcaldia
     * setAlcaldia
     */
	public void setAlcaldia(String alcaldia) {
		this.alcaldia = alcaldia;
	}

	/**
	 * 
	 * @return edo
	 * getEdo
	 */
	public String getEdo() {
		return edo;
	}

	/**
	 * 
	 * @param edo
	 * setEdo
	 */
	public void setEdo(String edo) {
		this.edo = edo;
	}
	  
	  
	  

}
