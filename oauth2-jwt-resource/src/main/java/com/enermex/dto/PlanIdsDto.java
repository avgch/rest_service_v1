package com.enermex.dto;

import java.util.List;

/**
 *
 * @author abraham
 */
public class PlanIdsDto {
	
	
   private List<Integer> planes;

    /**
     *
     * @return planes
     */
    public List<Integer> getPlanes() {
		return planes;
	}
	
    /**
     *
     * @param planes
     * planes
     */
    public void setPlanes(List<Integer> planes) {
		this.planes = planes;
	}	
   
   
   


}
