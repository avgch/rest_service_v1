package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class UbicacionDespachadorRequestDto {
	
	
	
	private String latitud;
	private String longitud;
	
    /**
     * latitud
     * @return latitud
     */
    public String getLatitud() {
		return latitud;
	}

    /**
     *
     * @param latitud
     * latitud
     */
    public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

    /**
     * longitud
     * @return longitud
     */
    public String getLongitud() {
		return longitud;
	}

    /**
     *
     * @param longitud
     * longitud
     */
    public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	
	
	

}
