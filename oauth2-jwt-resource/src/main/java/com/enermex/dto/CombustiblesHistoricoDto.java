package com.enermex.dto;

import java.util.Calendar;

/**
 * Nombre de clase 
 * @author abraham
 */
public class CombustiblesHistoricoDto {
	
	private Calendar fechaInc;
	private Calendar fechaFin;
	
    /**
     * getFechaInc
     * @return fechaInc
     */
    public Calendar getFechaInc() {
		return fechaInc;
	}

    /**
     * 
     * @param fechaInc
     * setFechaInc
     */
    public void setFechaInc(Calendar fechaInc) {
		this.fechaInc = fechaInc;
	}

    /**
     * getFechaFin
     * @return fechaFin
     */
    public Calendar getFechaFin() {
		return fechaFin;
	}

    /**
     * 
     * @param fechaFin
     * setFechaFin
     */
    public void setFechaFin(Calendar fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	
	

}
