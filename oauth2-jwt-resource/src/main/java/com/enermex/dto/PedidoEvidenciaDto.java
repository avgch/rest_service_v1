package com.enermex.dto;

import java.math.BigInteger;

/**
 * Nombre de Clase
 * @author abraham
 */
public class PedidoEvidenciaDto {
	
	
	private BigInteger idEvidencia;
	private String     momento;
	
    /**
     *
     * @return idEvidencia
     */
    public BigInteger getIdEvidencia() {
		return idEvidencia;
	}

    /**
     *
     * @param idEvidencia
     * idEvidencia
     */
    public void setIdEvidencia(BigInteger idEvidencia) {
		this.idEvidencia = idEvidencia;
	}

    /**
     *
     * @return momento
     */
    public String getMomento() {
		return momento;
	}

    /**
     *
     * @param momento
     * momento
     */
    public void setMomento(String momento) {
		this.momento = momento;
	}
	
	

}
