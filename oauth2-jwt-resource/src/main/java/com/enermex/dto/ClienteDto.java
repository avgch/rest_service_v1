package com.enermex.dto;

import java.util.List;

import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.Cliente;

/**
 * Nombre de clase
 * @author abraham
 */
public class ClienteDto extends Cliente {
	
	
	
	
	private List<Integer> planesId;
	
	private List<ClienteDto> invitados;
	
	private String pass;
	
	private List<AutomovilCliente> cars;
	
	private int idModelo;
	
	private int idMarca;
	
	private int idPlan;
	
	private String passActual;
	
	private String tokenConekta;
	
	private String idCustumerCnkta;
	
    /**
     * Contructor de clase
     */
    public ClienteDto() {
		
		
	}

    /**
     * getPlanesId
     * @return planesId
     */
    public List<Integer> getPlanesId() {
		return planesId;
	}

    /**
     * 
     * @param planesId
     * setPlanesId
     */
    public void setPlanesId(List<Integer> planesId) {
		this.planesId = planesId;
	}

    /**
     * getIdPlan
     * @return idPlan
     */
    public int getIdPlan() {
		return idPlan;
	}

    /**
     * 
     * @param idPlan
     * setIdPlan
     */
    public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

    /**
     * 
     * @return pass
     * getPass
     */
    public String getPass() {
		return pass;
	}

    /**
     * 
     * @param pass
     * setPass
     */
    public void setPass(String pass) {
		this.pass = pass;
	}

    /**
     * getCars
     * @return cars
     */
    public List<AutomovilCliente> getCars() {
		return cars;
	}

    /**
     * 
     * @param cars
     * setCars
     */
    public void setCars(List<AutomovilCliente> cars) {
		this.cars = cars;
	}

    /**
     * getIdModelo
     * @return idModelo
     */
    public int getIdModelo() {
		return idModelo;
	}

    /**
     * 
     * @param idModelo
     * setIdModelo
     */
    public void setIdModelo(int idModelo) {
		this.idModelo = idModelo;
	}

    /**
     * getIdMarca
     * @return idMarca
     */
    public int getIdMarca() {
		return idMarca;
	}

    /**
     * 
     * @param idMarca
     * setIdMarca
     */
    public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}

    /**
     * getPassActual
     * @return passActual
     */
    public String getPassActual() {
		return passActual;
	}

    /**
     * 
     * @param passActual
     * setPassActual
     */
    public void setPassActual(String passActual) {
		this.passActual = passActual;
	}

    /**
     * getInvitados
     * @return invitados
     */
    public List<ClienteDto> getInvitados() {
		return invitados;
	}

    /**
     * 
     * @param invitados
     * setInvitados
     */
    public void setInvitados(List<ClienteDto> invitados) {
		this.invitados = invitados;
	}

    /**
     * getTokenConekta
     * @return tokenConekta
     */ 
    public String getTokenConekta() {
		return tokenConekta;
	}

    /**
     * 
     * @param tokenConekta
     * setTokenConekta
     */
    public void setTokenConekta(String tokenConekta) {
		this.tokenConekta = tokenConekta;
	}

    /**
     * getIdCustumerCnkta
     * @return idCustumerCnkta
     */
    public String getIdCustumerCnkta() {
		return idCustumerCnkta;
	}

    /**
     * 
     * @param idCustumerCnkta
     * setIdCustumerCnkta
     */
    public void setIdCustumerCnkta(String idCustumerCnkta) {
		this.idCustumerCnkta = idCustumerCnkta;
	}
   
	

	
	
	
	
	
	

}
