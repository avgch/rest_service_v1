package com.enermex.dto;

import java.util.Date;

/**
 * Nombre de clase
 * @author abraham
 */
public class ParametroDto {
	
	
	private Integer idParametro;
	private String nombre;
	private String valor;
	private String descripcion;
	private String tipo;
	private Date fechaCreacion;
	
    /**
     * getIdParametro
     * @return idParametro
     */
    public Integer getIdParametro() {
		return idParametro;
	}

    /**
     * 
     * @param idParametro
     * setIdParametro
     */
    public void setIdParametro(Integer idParametro) {
		this.idParametro = idParametro;
	}

    /**
     * getNombre
     * @return nombre
     */
    public String getNombre() {
		return nombre;
	}

    /**
     * 
     * @param nombre
     * setNombre
     */
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    /**
     * getValor
     * @return valor
     */
    public String getValor() {
		return valor;
	}

    /**
     * 
     * @param valor
     * setValor
     */
    public void setValor(String valor) {
		this.valor = valor;
	}

    /**
     * getDescripcion
     * @return descripcion
     */
    public String getDescripcion() {
		return descripcion;
	}

    /**
     * 
     * @param descripcion
     * setDescripcion
     */
    public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

    /**
     * getTipo
     * @return tipo
     */
    public String getTipo() {
		return tipo;
	}

    /**
     * 
     * @param tipo
     * setTipo
     */
    public void setTipo(String tipo) {
		this.tipo = tipo;
	}

    /**
     * getFechaCreacion
     * @return fechaCreacion
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     * 
     * @param fechaCreacion
     * setFechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	
	
	
	

}
