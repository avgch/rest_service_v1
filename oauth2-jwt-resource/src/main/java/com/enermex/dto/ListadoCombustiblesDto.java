package com.enermex.dto;

import java.math.BigInteger;
import java.util.Calendar;

/**
 * Nombre de clase 
 * @author abraham
 */
public class ListadoCombustiblesDto {
	
	private BigInteger id;
	private Integer    idEstado;
	private Integer    idAlcaldia;
	private String     estado;
	private String     alcaldia;
	private String     cp;
	private Integer    idCombustible;
	private Double     promedio;
	private String   fechaVigencia;
	private Calendar   fechaRegistro;
	
    /**
     * Constructor de clase
     */
    public ListadoCombustiblesDto() {
		
		fechaRegistro = Calendar.getInstance();
	}
	
    /**
     * getId
     * @return id
     */
    public BigInteger getId() {
		return id;
	}

    /**
     * 
     * @param id
     * setId
     */
    public void setId(BigInteger id) {
		this.id = id;
	}

    /**
     * getIdEstado
     * @return idEstado
     */
    public Integer getIdEstado() {
		return idEstado;
	}

    /**
     * 
     * @param idEstado
     * setIdEstado
     */
    public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

    /**
     * getIdAlcaldia
     * @return idAlcaldia
     */
    public Integer getIdAlcaldia() {
		return idAlcaldia;
	}

    /**
     * 
     * @param idAlcaldia
     * setIdAlcaldia
     */
    public void setIdAlcaldia(Integer idAlcaldia) {
		this.idAlcaldia = idAlcaldia;
	}

    /**
     * getEstado
     * @return estado
     */
    public String getEstado() {
		return estado;
	}

    /**
     * 
     * @param estado
     * setEstado
     */
    public void setEstado(String estado) {
		this.estado = estado;
	}

    /**
     * getAlcaldia
     * @return alcaldia
     */
    public String getAlcaldia() {
		return alcaldia;
	}

    /**
     * 
     * @param alcaldia
     * setAlcaldia
     */
    public void setAlcaldia(String alcaldia) {
		this.alcaldia = alcaldia;
	}
	
    /**
     * getCp
     * @return cp
     */
    public String getCp() {
		return cp;
	}

    /**
     * 
     * @param cp
     * setCp
     */
    public void setCp(String cp) {
		this.cp = cp;
	}

    /**
     * getIdCombustible
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     * 
     * @param idCombustible
     * setIdCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     * getPromedio
     * @return promedio
     */
    public Double getPromedio() {
		return promedio;
	}

    /**
     * 
     * @param promedio
     * setPromedio
     */
    public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}

    /**
     * getFechaVigencia
     * @return fechaVigencia
     */
    public String getFechaVigencia() {
		return fechaVigencia;
	}

    /**
     * 
     * @param fechaVigencia
     * setFechaVigencia
     */
    public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

    /**
     * getFechaRegistro
     * @return fechaRegistro
     */
    public Calendar getFechaRegistro() {
		return fechaRegistro;
	}

    /**
     * 
     * @param fechaRegistro
     * setFechaRegistro
     */
    public void setFechaRegistro(Calendar fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	
	
	
	

}
