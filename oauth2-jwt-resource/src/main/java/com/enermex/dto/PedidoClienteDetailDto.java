package com.enermex.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.enermex.modelo.AutomovilCliente;
import com.enermex.modelo.DireccionCliente;
import com.enermex.modelo.Pipa;

/**
 * Nombre de clase
 * @author abraham
 */
public class PedidoClienteDetailDto {
	
	private BigInteger        idPedido;
	private String            ruta;
	private String            nombreDespachador;
	private String            telDespachador;
	private String            fotoUri;
	private Long              idDespachador;
	private String            litroCarga;
	private Double            precioPorLitro;
	private double            montoCarga;
	private Double            costoServicio;
	private Double            costoLavado;
	private Double            costoNeumaticos;
	private boolean           verificaPresionNeumatico;
	private String            llantasDelanterasPresion;
	private String            llantasTraserasPresion;
	private Date              fechaCreacion;
	private Calendar          fechaPedido;
	private String            tiempoTraslado;
	private String            distancia;
	private String            uuid;
	private Calendar          horaLlegada;
	private BigInteger        idPromocion;
	private boolean           cargaGas;
	private boolean           lavadoAuto;
	private boolean           revisionNeumaticos;
	private BigInteger        idCliente;
	private Integer           estatusPedido;
	private String            detalle;
    private int               idCombustible;
    private AutomovilCliente  automovil;
	private DireccionCliente  direccion;
	private Boolean           calificado;
	private Double            subTotal;
	private Double            totalImporte;
	private Double            totalConDescuento;
	private Double            iva;
	private Double            totalPagar;
	private Double            descuento;
	private Integer           calificacionDespachador;
	private PipaDetalleDto    pipa;
	private String            tarjeta;
	private Double            precioPorLitroFinal;
	private String            litrosDespachados;
	private String            alcaldiaMunicipio;
	private Calendar          horaEstimada;
	private String            codigoPromocion;
	
    /**
     * horaEstimada
     * @return horaEstimada
     */
    public Calendar getHoraEstimada() {
		return horaEstimada;
	}

    /**
     *
     * @param horaEstimada
     * horaEstimada
     */
    public void setHoraEstimada(Calendar horaEstimada) {
		this.horaEstimada = horaEstimada;
	}


	List<BigInteger> antes = new ArrayList<>();
	List<BigInteger> despues = new ArrayList<>();
	
    /**
     * Constructor de clase
     */
    public PedidoClienteDetailDto(){
		
		
		pipa = new PipaDetalleDto();
	}
	
    /**
     * idPedido
     * @return idPedido
     */
    public BigInteger getIdPedido() {
		return idPedido;
	}

    /**
     *
     * @param idPedido
     * idPedido
     */
    public void setIdPedido(BigInteger idPedido) {
		this.idPedido = idPedido;
	}

    /**
     * ruta
     * @return ruta
     */
    public String getRuta() {
		return ruta;
	}

    /**
     *
     * @param ruta
     * ruta
     */
    public void setRuta(String ruta) {
		this.ruta = ruta;
	}

    /**
     * nombreDespachador
     * @return nombreDespachador
     */
    public String getNombreDespachador() {
		return nombreDespachador;
	}

    /**
     *
     * @param nombreDespachador
     * nombreDespachador
     */
    public void setNombreDespachador(String nombreDespachador) {
		this.nombreDespachador = nombreDespachador;
	}

    /**
     * idDespachador
     * @return idDespachador
     */
    public Long getIdDespachador() {
		return idDespachador;
	}

    /**
     *
     * @param idDespachador
     * idDespachador
     */
    public void setIdDespachador(Long idDespachador) {
		this.idDespachador = idDespachador;
	}

    /**
     * litroCarga
     * @return litroCarga
     */
    public String getLitroCarga() {
		return litroCarga;
	}

    /**
     *
     * @param litroCarga
     * litroCarga
     */
    public void setLitroCarga(String litroCarga) {
		this.litroCarga = litroCarga;
	}

    /**
     * montoCarga
     * @return montoCarga
     */
    public double getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     * montoCarga
     */
    public void setMontoCarga(double montoCarga) {
		this.montoCarga = montoCarga;
	}

    /**
     * verificaPresionNeumatico
     * @return verificaPresionNeumatico
     */
    public boolean isVerificaPresionNeumatico() {
		return verificaPresionNeumatico;
	}

    /**
     *
     * @param verificaPresionNeumatico
     *  verificaPresionNeumatico
     */
    public void setVerificaPresionNeumatico(boolean verificaPresionNeumatico) {
		this.verificaPresionNeumatico = verificaPresionNeumatico;
	}

    /**
     * llantasDelanterasPresion
     * @return llantasDelanterasPresion
     */
    public String getLlantasDelanterasPresion() {
		return llantasDelanterasPresion;
	}

    /**
     *
     * @param llantasDelanterasPresion
     * llantasDelanterasPresion
     */
    public void setLlantasDelanterasPresion(String llantasDelanterasPresion) {
		this.llantasDelanterasPresion = llantasDelanterasPresion;
	}

    /**
     * llantasTraseraPresion
     * @return llantasTraseraPresion
     */
    public String getLlantasTraserasPresion() {
		return llantasTraserasPresion;
	}

    /**
     *
     * @param llantasTraserasPresion
     * llantasTraserasPresion
     */
    public void setLlantasTraserasPresion(String llantasTraserasPresion) {
		this.llantasTraserasPresion = llantasTraserasPresion;
	}

    /**
     * fechaCreacion
     * @return fechaCreacion
     */
    public Date getFechaCreacion() {
		return fechaCreacion;
	}

    /**
     *
     * @param fechaCreacion
     * fechaCreacion
     */
    public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

    /**
     * fechaPedido
     * @return fechaPedido
     */
    public Calendar getFechaPedido() {
		return fechaPedido;
	}

    /**
     *
     * @param fechaPedido
     * fechaPedido
     */
    public void setFechaPedido(Calendar fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

    /**
     * idPromocion
     * @return idPromocion
     */
    public BigInteger getIdPromocion() {
		return idPromocion;
	}

    /**
     *
     * @param idPromocion
     * idPromocion
     */
    public void setIdPromocion(BigInteger idPromocion) {
		this.idPromocion = idPromocion;
	}

    /**
     * cargaGas
     * @return cargaGas
     */
    public boolean isCargaGas() {
		return cargaGas;
	}

    /**
     *
     * @param cargaGas
     * cargaGas
     */
    public void setCargaGas(boolean cargaGas) {
		this.cargaGas = cargaGas;
	}

    /**
     * lavadoAuto
     * @return lavadoAuto
     */
    public boolean isLavadoAuto() {
		return lavadoAuto;
	}

    /**
     *
     * @param lavadoAuto
     * lavadoAuto
     */
    public void setLavadoAuto(boolean lavadoAuto) {
		this.lavadoAuto = lavadoAuto;
	}

    /**
     * revisionNeumaticos
     * @return revisionNeumaticos
     */
    public boolean isRevisionNeumaticos() {
		return revisionNeumaticos;
	}

    /**
     *
     * @param revisionNeumaticos
     * revisionNeumaticos
     */
    public void setRevisionNeumaticos(boolean revisionNeumaticos) {
		this.revisionNeumaticos = revisionNeumaticos;
	}

    /**
     * idCliente
     * @return idCliente
     */
    public BigInteger getIdCliente() {
		return idCliente;
	}

    /**
     *
     * @param idCliente
     * idCliente
     */
    public void setIdCliente(BigInteger idCliente) {
		this.idCliente = idCliente;
	}

    /**
     *
     * @return estatusPedido
     * estatusPedido
     */
    public Integer getEstatusPedido() {
		return estatusPedido;
	}

    /**
     *
     * @param estatusPedido
     * estatusPedido
     */
    public void setEstatusPedido(Integer estatusPedido) {
		this.estatusPedido = estatusPedido;
	}

    /**
     * detalle
     * @return detalle
     */
    public String getDetalle() {
		return detalle;
	}

    /**
     * 
     * @param detalle
     * detalle
     */
    public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

    /**
     * idCombustible
     * @return idCombustible
     */
    public int getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(int idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     * automovil
     * @return automovil
     */
    public AutomovilCliente getAutomovil() {
		return automovil;
	}

    /**
     *
     * @param automovil
     * automovil
     */
    public void setAutomovil(AutomovilCliente automovil) {
		this.automovil = automovil;
	}

    /**
     * direccion
     * @return direccion
     */
    public DireccionCliente getDireccion() {
		return direccion;
	}

    /**
     *
     * @param direccion
     * direccion
     */
    public void setDireccion(DireccionCliente direccion) {
		this.direccion = direccion;
	}

    /**
     * telDespachador
     * @return telDespachador
     */
    public String getTelDespachador() {
		return telDespachador;
	}

    /**
     *
     * @param telDespachador
     * telDespachador
     */
    public void setTelDespachador(String telDespachador) {
		this.telDespachador = telDespachador;
	}

    /**
     * fotouri
     * @return fotouri
     */
    public String getFotoUri() {
		return fotoUri;
	}

    /**
     *
     * @param fotoUri
     * fotoUri
     */
    public void setFotoUri(String fotoUri) {
		this.fotoUri = fotoUri;
	}

    /**
     * calificado
     * @return calificado
     */
    public Boolean getCalificado() {
		return calificado;
	}

    /**
     *
     * @param calificado
     * calificado
     */
    public void setCalificado(Boolean calificado) {
		this.calificado = calificado;
	}

    /**
     * tiempoTraslado
     * @return tiempoTraslado
     */
    public String getTiempoTraslado() {
		return tiempoTraslado;
	}

    /**
     *
     * @param tiempoTraslado
     * tiempoTraslado
     */
    public void setTiempoTraslado(String tiempoTraslado) {
		this.tiempoTraslado = tiempoTraslado;
	}

    /**
     * uuid
     * @return uuid
     */
    public String getUuid() {
		return uuid;
	}

    /**
     *
     * @param uuid
     * uuid
     */
    public void setUuid(String uuid) {
		this.uuid = uuid;
	}

    /**
     * distancia
     * @return distancia
     */
    public String getDistancia() {
		return distancia;
	}

    /**
     *
     * @param distancia
     * distancia
     */
    public void setDistancia(String distancia) {
		this.distancia = distancia;
	}

    /**
     * subTotal
     * @return subTotal
     */
    public Double getSubTotal() {
		return subTotal;
	}

    /**
     *
     * @param subTotal
     * subTotal
     */
    public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

    /**
     * iva
     * @return iva
     */
    public Double getIva() {
		return iva;
	}

    /**
     *
     * @param iva
     * iva
     */
    public void setIva(Double iva) {
		this.iva = iva;
	}

    /**
     * totalPagar
     * @return totalPagar
     */
    public Double getTotalPagar() {
		return totalPagar;
	}

    /**
     *
     * @param totalPagar
     * totalPagar
     */
    public void setTotalPagar(Double totalPagar) {
		this.totalPagar = totalPagar;
	}

    /**
     * descuento
     * @return descuento
     */
    public Double getDescuento() {
		return descuento;
	}

    /**
     *
     * @param descuento
     * descuento
     */
    public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

    /**
     * calificacionDespachador
     * @return calificacionDespachador
     */
    public Integer getCalificacionDespachador() {
		return calificacionDespachador;
	}

    /**
     *
     * @param calificacionDespachador
     * calificacionDespachador
     */
    public void setCalificacionDespachador(Integer calificacionDespachador) {
		this.calificacionDespachador = calificacionDespachador;
	}

    /**
     * pipa
     * @return pipa
     */
    public PipaDetalleDto getPipa() {
		return pipa;
	}

    /**
     *
     * @param pipa
     * pipa
     */
    public void setPipa(PipaDetalleDto pipa) {
		this.pipa = pipa;
	}

    /**
     * precioPorLitro
     * @return precioPorLitro
     */
    public Double getPrecioPorLitro() {
		return precioPorLitro;
	}

    /**
     *
     * @param precioPorLitro
     * precioPorLitro
     */
    public void setPrecioPorLitro(Double precioPorLitro) {
		this.precioPorLitro = precioPorLitro;
	}

    /**
     * tarjeta
     * @return tarjeta
     */
    public String getTarjeta() {
		return tarjeta;
	}

    /**
     *
     * @param tarjeta
     * tarjeta
     */
    public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

    /**
     * costoServicio
     * @return costoServicio
     */
    public Double getCostoServicio() {
		return costoServicio;
	}

    /**
     *
     * @param costoServicio
     * costoServicio
     */
    public void setCostoServicio(Double costoServicio) {
		this.costoServicio = costoServicio;
	}

    /**
     * costoLavado
     * @return costoLavado
     */
    public Double getCostoLavado() {
		return costoLavado;
	}

    /**
     *
     * @param costoLavado
     * costoLavado
     */
    public void setCostoLavado(Double costoLavado) {
		this.costoLavado = costoLavado;
	}

    /**
     * costoNeumaticos
     * @return costoNeumaticos
     */
    public Double getCostoNeumaticos() {
		return costoNeumaticos;
	}

    /**
     *
     * @param costoNeumaticos
     * costoNeumaticos
     */
    public void setCostoNeumaticos(Double costoNeumaticos) {
		this.costoNeumaticos = costoNeumaticos;
	}

    /**
     * totalImporte
     * @return totalImporte
     */
    public Double getTotalImporte() {
		return totalImporte;
	}

    /**
     *
     * @param totalImporte
     * totalImporte
     */
    public void setTotalImporte(Double totalImporte) {
		this.totalImporte = totalImporte;
	}

    /**
     * totalConDescuento
     * @return totalConDescuento
     */
    public Double getTotalConDescuento() {
		return totalConDescuento;
	}

    /**
     *
     * @param totalConDescuento
     * totalConDescuento
     */
    public void setTotalConDescuento(Double totalConDescuento) {
		this.totalConDescuento = totalConDescuento;
	}

    /**
     * precioPorLitroFinal
     * @return precioPorLitroFinal
     */
    public Double getPrecioPorLitroFinal() {
		return precioPorLitroFinal;
	}

    /**
     * 
     * @param precioPorLitroFinal
     * precioPorLitroFinal
     */
    public void setPrecioPorLitroFinal(Double precioPorLitroFinal) {
		this.precioPorLitroFinal = precioPorLitroFinal;
	}

    /**
     * litrosDespachados
     * @return litrosDespachados
     */
    public String getLitrosDespachados() {
		return litrosDespachados;
	}

    /**
     *
     * @param litrosDespachados
     * litrosDespachados
     */
    public void setLitrosDespachados(String litrosDespachados) {
		this.litrosDespachados = litrosDespachados;
	}

    /**
     * antes
     * @return antes
     */
    public List<BigInteger> getAntes() {
		return antes;
	}

    /**
     *
     * @param antes
     * antes
     */
    public void setAntes(List<BigInteger> antes) {
		this.antes = antes;
	}

    /**
     * despues
     * @return despues
     */
    public List<BigInteger> getDespues() {
		return despues;
	}

    /**
     *
     * @param despues
     * despues
     */
    public void setDespues(List<BigInteger> despues) {
		this.despues = despues;
	}

    /**
     * alcaldiaMunicipio
     * @return alcaldiaMunicipio
     */
    public String getAlcaldiaMunicipio() {
		return alcaldiaMunicipio;
	}

    /**
     *
     * @param alcaldiaMunicipio
     * alcaldiaMunicipio
     */
    public void setAlcaldiaMunicipio(String alcaldiaMunicipio) {
		this.alcaldiaMunicipio = alcaldiaMunicipio;
	}

    /**
     * horaLlegada
     * @return horaLlegada
     */
    public Calendar getHoraLlegada() {
		return horaLlegada;
	}

    /**
     *
     * @param horaLlegada
     * horaLlegada
     */
    public void setHoraLlegada(Calendar horaLlegada) {
		this.horaLlegada = horaLlegada;
	}

    /**
     * codigoPromocion
     * @return codigoPromocion
     */
    public String getCodigoPromocion() {
		return codigoPromocion;
	}

    /**
     *
     * @param codigoPromocion
     * codigoPromocion
     */
    public void setCodigoPromocion(String codigoPromocion) {
		this.codigoPromocion = codigoPromocion;
	}
	
    
	
	
    
    

}
