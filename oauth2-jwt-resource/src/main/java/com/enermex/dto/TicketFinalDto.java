package com.enermex.dto;

/**
 * Nombre de clase
 * @author abraham
 */
public class TicketFinalDto {

	
	private boolean  lavado;
	private boolean  revNeumaticos;
	private double   montoCarga;
	private double   precioPorlitro;
	private String   litros;
	private double   subTotal;
	private double   totalImporte;
	private Double   totalConDescuento;
	private double   ivaPagar;
	private double   costoServicio;
	private double   montoLavado;
	private double   montoNeumaticos;
	private Integer  idCombustible;
	private String   codigoPromo;
	private double   descuento;
	private String   tarjeta;
	private double   totalPagar;
	private String   alcMun;

    /**
     *
     * @return lavado
     */
    public boolean isLavado() {
		return lavado;
	}

    /**
     *
     * @param lavado
     * lavado
     */
    public void setLavado(boolean lavado) {
		this.lavado = lavado;
	}

    /**
     *
     * @return revNeumaticos
     */
    public boolean isRevNeumaticos() {
		return revNeumaticos;
	}

    /**
     *
     * @param revNeumaticos
     * revNeumaticos
     */
    public void setRevNeumaticos(boolean revNeumaticos) {
		this.revNeumaticos = revNeumaticos;
	}

    /**
     *
     * @return montoCarga
     */
    public double getMontoCarga() {
		return montoCarga;
	}

    /**
     *
     * @param montoCarga
     * montoCarga
     */
    public void setMontoCarga(double montoCarga) {
		this.montoCarga = montoCarga;
	}

    /**
     *
     * @return litros
     */
    public String getLitros() {
		return litros;
	}

    /**
     *
     * @param litros
     * litros
     */
    public void setLitros(String litros) {
		this.litros = litros;
	}

    /**
     *
     * @return subTotal
     */
    public double getSubTotal() {
		return subTotal;
	}

    /**
     *
     * @param subTotal
     * subTotal
     */
    public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

    /**
     *
     * @return ivaPagar
     */
    public double getIvaPagar() {
		return ivaPagar;
	}

    /**
     *
     * @param ivaPagar
     * ivaPagar
     */
    public void setIvaPagar(double ivaPagar) {
		this.ivaPagar = ivaPagar;
	}

    /**
     *
     * @return costoServicio
     */
    public double getCostoServicio() {
		return costoServicio;
	}

    /**
     *
     * @param costoServicio
     * costoServicio
     */
    public void setCostoServicio(double costoServicio) {
		this.costoServicio = costoServicio;
	}

    /**
     *
     * @return montoLavado
     */
    public double getMontoLavado() {
		return montoLavado;
	}

    /**
     *
     * @param montoLavado
     * montoLavado
     */
    public void setMontoLavado(double montoLavado) {
		this.montoLavado = montoLavado;
	}

    /**
     *
     * @return idCombustible
     */
    public Integer getIdCombustible() {
		return idCombustible;
	}

    /**
     *
     * @param idCombustible
     * idCombustible
     */
    public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}

    /**
     *
     * @return codigoPromo
     */
    public String getCodigoPromo() {
		return codigoPromo;
	}

    /**
     *
     * @param codigoPromo
     * codigoPromo
     */
    public void setCodigoPromo(String codigoPromo) {
		this.codigoPromo = codigoPromo;
	}

    /**
     *
     * @return descuento
     */
    public double getDescuento() {
		return descuento;
	}

    /**
     *
     * @param descuento
     * descuento
     */
    public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

    /**
     *
     * @return tarjeta
     */
    public String getTarjeta() {
		return tarjeta;
	}

    /**
     *
     * @param tarjeta
     * tarjeta
     */
    public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

    /**
     *
     * @return totalPagar
     */
    public double getTotalPagar() {
		return totalPagar;
	}

    /**
     *
     * @param totalPagar
     * totalPagar
     */
    public void setTotalPagar(double totalPagar) {
		this.totalPagar = totalPagar;
	}

    /**
     *
     * @return montoNeumaticos
     */
    public double getMontoNeumaticos() {
		return montoNeumaticos;
	}

    /**
     *
     * @param montoNeumaticos
     * montoNeumaticos
     */
    public void setMontoNeumaticos(double montoNeumaticos) {
		this.montoNeumaticos = montoNeumaticos;
	}

    /**
     *
     * @return totalImporte
     */
    public double getTotalImporte() {
		return totalImporte;
	}

    /**
     *
     * @param totalImporte
     * totalImporte
     */
    public void setTotalImporte(double totalImporte) {
		this.totalImporte = totalImporte;
	}

    /**
     *
     * @return totalConDescuento
     */
    public Double getTotalConDescuento() {
		return totalConDescuento;
	}

    /**
     * 
     * @param totalConDescuento
     * totalConDescuento
     */
    public void setTotalConDescuento(Double totalConDescuento) {
		this.totalConDescuento = totalConDescuento;
	}

    /**
     *
     * @return precioPorlitro
     */
    public double getPrecioPorlitro() {
		return precioPorlitro;
	}

    /**
     *
     * @param precioPorlitro
     * precioPorlitro
     */
    public void setPrecioPorlitro(double precioPorlitro) {
		this.precioPorlitro = precioPorlitro;
	}

    /**
     *
     * @return alcMun
     */
    public String getAlcMun() {
		return alcMun;
	}

    /**
     *
     * @param alcMun
     * alcMun
     */
    public void setAlcMun(String alcMun) {
		this.alcMun = alcMun;
	}
	
	
	
	
	
	
	
	
}
