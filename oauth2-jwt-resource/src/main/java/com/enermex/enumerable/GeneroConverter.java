package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class GeneroConverter implements AttributeConverter<Genero, String> {
  
    @Override
    public String convertToDatabaseColumn(Genero genero) {
        if (genero == null) {
            return null;
        }
        return genero.getGenero();
    }
 
    @Override
    public Genero convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (Genero genero : Genero.values()) {
          if(genero.getGenero().equals(code)){
            return genero;
          }
        }

        return null;
    }
}
