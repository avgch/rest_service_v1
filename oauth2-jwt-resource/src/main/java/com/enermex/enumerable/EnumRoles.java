package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum EnumRoles {

    /**
     *
     */
    ROLE_CLIENTE("ROLE_CLIENTE"),

    /**
     *
     */
    ROLE_ADMINISTRADOR("ROLE_ADMINISTRADOR"),

    /**
     *
     */
    ROLE_DESPACHADOR("ROLE_DESPACHADOR");
	
	
	 private final String name;

	    /**
	     * @param name
	     */
	    private EnumRoles(final String name) {
	        this.name = name;
	    }

    /**
     *
     * @return name
     */
    public String getName() {
	        return name;
	    }
}
