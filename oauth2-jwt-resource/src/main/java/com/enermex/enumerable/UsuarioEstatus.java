package  com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum UsuarioEstatus {
 
    /**
     *
     */
    ACTIVO("A"), 
 
    /**
     *
     */
    INACTIVO("I"), 
 
    /**
     *
     */
    BLOQUEADO("B"), 

    /**
     *
     */
    POR_VERIFICAR("V");
  
  private String estatus;

  private UsuarioEstatus(String estatus) {
      this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
      return estatus;
  }
}
