package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PedidoMensajes {

    /**
     *
     */
    CANCELAR_SERVICIO_INICIADO("No se puede cancelar un servicio iniciado"),

    /**
     *
     */
    SERVICIO_EN_CAMINO("Estimado cliente la pipa va en camino a su ubicación para atender el ID servicio XXXXXXXXXX, con hora estimada de llegada: YYYYY"),

    /**
     *
     */
    SERVICIO_INICIADO("Estimado cliente la pipa ya se encuentra en la ubicación solicitada, va a iniciar a atender el ID servicio: XXXXXXXXXX"),

    /**
     *
     */
    SERVICIO_TERMINADO("Estimado cliente se ha terminado de atender su servicio ID XXXXXXX, gracias por su confianza"),

    /**
     *
     */
    SERVICIO_CANCELADO_DESPACHADOR_CLIENTE("Estimado cliente se ha cancelado su servicio ID XXXXXXX.");
	
	
	  private String valor;

	  private PedidoMensajes(String valor) {
	      this.valor = valor;
	  }

    /**
     *
     * @return valor
     */
    public String getValor() {
	      return valor;
	  }
}
