package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum DireccionEstatus {

    /**
     *
     */
    ACTIVO(1),

    /**
     *
     */
    ELIMINADO(2);

  private int estatus;

  private DireccionEstatus(int estadoCivil) {
    this.estatus = estadoCivil;
  }

    /**
     *
     * @return estatus
     */
    public int getDireccionEstatus() {
    return estatus;
  }
}
