package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PlanesEnum {

    /**
     *
     */
    PARTICULAR(1),
 
    /**
     *
     */
    FAMILIAR(2), 

    /**
     *
     */
    EMPRESARIAL(3);
	  
	  private int valor;

	  private PlanesEnum(int valor) {
	      this.valor = valor;
	  }

    /**
     *
     * @return valor
     */
    public int getValor() {
	      return valor;
	  }

}
