package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum MsjBloqueCuentaEnum {

    /**
     *
     */
    MSJ_BLOQUEO_CUENTA("Estimado NOMBRE_TITULAR " + 
			" le informamos que en la fecha FECHA_CAMBIO se realizó el BLOQUE_DESBLOQUEO de su invitado NOMBRE_FAMILIAR  y a esta fecha  CUENTA_O_NO_CUENTA con servicios programados."),
	
    /**
     *
     */
    MSJ_BLOQUEO_CUENTA_FAMILIAR("Estimado NOMBRE_FAMILIAR" + 
			" le informamos que en la fecha FECHA_CAMBIO se realizó el BLOQUE_DESBLOQUEO de su cuenta perteneciente a un plan familiar, favor de contactarse con el (ella)  y a esta fecha  CUENTA_O_NO_CUENTA con servicios programados");
	
	 private String valor;

	  private MsjBloqueCuentaEnum(String valor) {
	      this.valor = valor;
	  }

    /**
     *
     * @return valor
     */
    public String getValor() {
	      return valor;
	  }
}
