package  com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PedidoEvidenciaMomento {

    /**
     *
     */
    ANTES("A"),

    /**
     *
     */
    DESPUES("D");
  
  private String momento;

  private PedidoEvidenciaMomento(String momento) {
      this.momento = momento;
  }

    /**
     *
     * @return momento
     */
    public String getMomento() {
      return momento;
  }
}
