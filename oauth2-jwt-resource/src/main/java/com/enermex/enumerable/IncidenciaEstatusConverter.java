package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class IncidenciaEstatusConverter implements AttributeConverter<IncidenciaEstatus, String> {
  
    @Override
    public String convertToDatabaseColumn(IncidenciaEstatus estatus) {
        if (estatus == null) {
            return null;
        }
        return estatus.getEstatus();
    }
 
    @Override
    public IncidenciaEstatus convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (IncidenciaEstatus estatus : IncidenciaEstatus.values()) {
          if(estatus.getEstatus().equals(code)){
            return estatus;
          }
        }

        return null;
    }
}
