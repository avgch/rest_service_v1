package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum CamarasEstatus {
	
    /**
     *
     */ 
    ACTIVA("Activa"), 
 
    /**
     *
     */
    INACTIVA("Inactiva"), 

    /**
     *
     */
    ASIGNADA("Asignada");
	  
	  private String estatus;

	  private CamarasEstatus(String estatus) {
	      this.estatus = estatus;
	  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
	      return estatus;
	  }
	

}
