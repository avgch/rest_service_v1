package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum TipoCombustiblePetroIntelligenceEnum {
	
    /**
     *
     */
    REGULAR(1),

    /**
     *
     */
    PREMIUM(2),

    /**
     *
     */
    DIESEL(3);
	  	 
	  private Integer combustible;

	  private TipoCombustiblePetroIntelligenceEnum(Integer combustible) {
	      this.combustible = combustible;
	  }

    /**
     *
     * @return combustible
     */
    public Integer getCombustible() {
	      return combustible;
	  }

}
