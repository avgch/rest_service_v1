package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class FaqEstatusConverter implements AttributeConverter<FaqEstatus, String> {
  
    @Override
    public String convertToDatabaseColumn(FaqEstatus estatus) {
        if (estatus == null) {
            return null;
        }
        return estatus.getEstatus();
    }
 
    @Override
    public FaqEstatus convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (FaqEstatus estatus : FaqEstatus.values()) {
          if(estatus.getEstatus().equals(code)){
            return estatus;
          }
        }

        return null;
    }
}
