package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum TipoCombustible {
	
    /**
     *
     */
    DIESEL(1),

    /**
     *
     */
    PREMIUM(2),

    /**
     *
     */
    REGULAR(3);
	  	 
	  private Integer combustible;

	  private TipoCombustible(Integer combustible) {
	      this.combustible = combustible;
	  }

    /**
     *
     * @return combustible
     */
    public Integer getCombustible() {
	      return combustible;
	  }

}
