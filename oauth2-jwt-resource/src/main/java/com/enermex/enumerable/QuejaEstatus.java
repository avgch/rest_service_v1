package  com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum QuejaEstatus {

    /**
     *
     */
    ASIGNADA("A"),

    /**
     *
     */
    EN_ATENCION("E"),

    /**
     *
     */
    ATENDIDA("N"),

    /**
     *
     */
    CERRADA("C");
  
  private String estatus;

  private QuejaEstatus(String estatus) {
      this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
      return estatus;
  }
}
