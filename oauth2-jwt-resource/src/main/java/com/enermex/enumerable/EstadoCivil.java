package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum EstadoCivil {

    /**
     *
     */
    SOLTERO("S"),

    /**
     *
     */
    CASADO("C"),

    /**
     *
     */
    DIVORCIADO("D"),

    /**
     *
     */
    UNION_LIBRE("U");

  private String estadoCivil;

  private EstadoCivil(String estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

    /**
     * estadoCivil
     * @return estadoCivil
     */
    public String getEstadoCivil() {
    return estadoCivil;
  }
}
