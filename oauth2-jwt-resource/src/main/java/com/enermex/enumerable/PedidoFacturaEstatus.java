package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PedidoFacturaEstatus {

    /**
     *
     */
    SOLICITADA("S"),

    /**
     *
     */
    PROCESANDO("P"),

    /**
     *
     */
    DISPONIBLE("D"),

    /**
     *
     */
    INACTIVO("I");

  private String estatus;

  private PedidoFacturaEstatus(String estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
    return estatus;
  }

    /**
     *
     * @param name
     * name
     * @return estatus
     */
    public static PedidoFacturaEstatus iterationFindByName(String name) {
    for (PedidoFacturaEstatus estatus : PedidoFacturaEstatus.values()) {
      if (name.equals(estatus.name())) {
        return estatus;
      }
    }

    return null;
  }
}
