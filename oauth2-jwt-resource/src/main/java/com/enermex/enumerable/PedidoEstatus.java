package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PedidoEstatus {
	
    /**
     *
     */
    ASIGNADO(1), 
 
    /**
     *
     */
    EN_CAMINO(2), 

    /**
     *
     */
    TRABAJO_INICIADO(3),

    /**
     *
     */
    TRABAJO_TERMINADO(4),

    /**
     *
     */
    CANCELADO(5);
	  
	  private Integer estatus;

	  private PedidoEstatus(Integer estatus) {
	      this.estatus = estatus;
	  }

    /**
     *
     * @return estatus
     */
    public Integer getEstatus() {
	      return estatus;
	  }
	

}
