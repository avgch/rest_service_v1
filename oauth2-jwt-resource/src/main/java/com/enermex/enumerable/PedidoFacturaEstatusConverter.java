package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class PedidoFacturaEstatusConverter implements AttributeConverter<PedidoFacturaEstatus, String> {

  @Override
  public String convertToDatabaseColumn(PedidoFacturaEstatus estatus) {
    if (estatus == null) {
      return null;
    }
    return estatus.getEstatus();
  }

  @Override
  public PedidoFacturaEstatus convertToEntityAttribute(String code) {
    if (code == null) {
      return null;
    }

    for (PedidoFacturaEstatus estatus : PedidoFacturaEstatus.values()) {
      if (estatus.getEstatus().equals(code)) {
        return estatus;
      }
    }

    return null;
  }
}
