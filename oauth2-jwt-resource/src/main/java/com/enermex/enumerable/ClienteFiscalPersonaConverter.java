package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class ClienteFiscalPersonaConverter implements AttributeConverter<ClienteFiscalPersona, String> {

  @Override
  public String convertToDatabaseColumn(ClienteFiscalPersona persona) {
    if (persona == null) {
      return null;
    }
    return persona.getPersona();
  }

  @Override
  public ClienteFiscalPersona convertToEntityAttribute(String code) {
    if (code == null) {
      return null;
    }

    for (ClienteFiscalPersona persona : ClienteFiscalPersona.values()) {
      if (persona.getPersona().equals(code)) {
        return persona;
      }
    }

    return null;
  }
}
