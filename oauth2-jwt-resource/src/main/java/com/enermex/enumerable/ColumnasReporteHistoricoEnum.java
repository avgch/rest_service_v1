package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum ColumnasReporteHistoricoEnum {
	
    /**
     *
     */
    Column1("IDTarifa"),

    /**
     *
     */
    Column2("Estado"),

    /**
     *
     */
    Column3("Alcaldía/Municipio"),

    /**
     *
     */
    Column4("Cp"),

    /**
     *
     */
    Column5("Combustible"),

    /**
     *
     */
    Column6("Promedio"),

    /**
     *
     */
    Column7("Fecha de registro");
	    
	   String columnName;
	   ColumnasReporteHistoricoEnum(String colName) {
	      columnName = colName;
	   }

    /**
     *
     * @return columnName
     */
    public String getColumnName() {
	      return columnName;
	   } 
	    
	

}
