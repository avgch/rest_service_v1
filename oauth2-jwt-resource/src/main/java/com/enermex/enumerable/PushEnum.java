package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PushEnum {
	
    /**
     *
     */
    PROMOCION("Estimado cliente se tiene la siguiente promoción CODIGO con la siguiente vigencia [Del FECHA_INC -  Hasta FECHA_FIN] con [Hora de inicio: HR_INC   - Hora de término: HR_FIN], lo invitamos para mayor detalle visitar la sección de Promociones de su Aplicación");
	
	
	  private String valor;

	  private PushEnum(String valor) {
	      this.valor = valor;
	  }

    /**
     *
     * @return valor
     */
    public String getValor() {
	      return valor;
	  }

}
