package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class QuejaTipoDetalleConverter implements AttributeConverter<QuejaTipoDetalle, String> {
  
    @Override
    public String convertToDatabaseColumn(QuejaTipoDetalle tipo) {
        if (tipo == null) {
            return null;
        }
        return tipo.getTipoDetalle();
    }
 
    @Override
    public QuejaTipoDetalle convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (QuejaTipoDetalle tipo : QuejaTipoDetalle.values()) {
          if(tipo.getTipoDetalle().equals(code)){
            return tipo;
          }
        }

        return null;
    }
}
