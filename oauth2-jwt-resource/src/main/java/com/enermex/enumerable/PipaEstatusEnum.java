package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PipaEstatusEnum {
	
    /**
     *
     */
    ACTIVO("Activo"), 

    /**
     *
     */
    INACTIVO("Inactivo");
	  
	  private String estatus;

	  private PipaEstatusEnum(String estatus) {
	      this.estatus = estatus;
	  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
	      return estatus;
	  }
	


}
