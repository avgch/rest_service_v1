package  com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum IncidenciaEstatus {
 
    /**
     *
     */
    ACTIVO("A"), 

    /**
     *
     */
    INACTIVO("I");
  
  private String estatus;

  private IncidenciaEstatus(String estatus) {
      this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
      return estatus;
  }
}
