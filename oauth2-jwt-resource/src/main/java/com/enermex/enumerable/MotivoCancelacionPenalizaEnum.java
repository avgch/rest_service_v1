package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum MotivoCancelacionPenalizaEnum {

    /**
     *
     */
    AUTO_CERRADO(12), 

    /**
     *
     */
    NO_HAY_AUTOMOVIL(7);
	  
	  private Integer valor;

	  private MotivoCancelacionPenalizaEnum(Integer valor) {
	      this.valor = valor;
	  }

    /**
     *
     * @return valor
     */
    public Integer getValor() {
	      return valor;
	  }
	
	
}
