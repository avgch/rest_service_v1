package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum TipoPedidoEnum {

    /**
     *
     */
    PROGRAMADO(1), 
 
    /**
     *
     */
    MISMO_DIA(2), 

    /**
     *
     */
    URGENTE(3);
	  
	  private Integer id;

	  private TipoPedidoEnum(Integer id) {
	      this.id = id;
	  }

    /**
     *
     * @return id
     */
    public Integer getId() {
	      return id;
	  }
}
