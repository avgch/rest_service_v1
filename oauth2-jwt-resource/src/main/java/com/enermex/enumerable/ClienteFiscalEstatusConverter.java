package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class ClienteFiscalEstatusConverter implements AttributeConverter<ClienteFiscalEstatus, String> {

  @Override
  public String convertToDatabaseColumn(ClienteFiscalEstatus estatus) {
    if (estatus == null) {
      return null;
    }
    return estatus.getEstatus();
  }

  @Override
  public ClienteFiscalEstatus convertToEntityAttribute(String code) {
    if (code == null) {
      return null;
    }

    for (ClienteFiscalEstatus estatus : ClienteFiscalEstatus.values()) {
      if (estatus.getEstatus().equals(code)) {
        return estatus;
      }
    }

    return null;
  }
}
