package  com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum QuejaTipoDetalle {

    /**
     *
     */
    ASUNTO("A"),

    /**
     *
     */
    RESPUESTA("R");
  
  private String estatus;

  private QuejaTipoDetalle(String estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getTipoDetalle() {
    return estatus;
  }
}
