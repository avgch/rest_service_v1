package  com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class DireccionEstatusConverter implements AttributeConverter<DireccionEstatus, Integer> {
  
    @Override
    public Integer convertToDatabaseColumn(DireccionEstatus estatus) {
        if (estatus == null) {
            return null;
        }
        return estatus.getDireccionEstatus();
    }
 
    @Override
    public DireccionEstatus convertToEntityAttribute(Integer code) {
        if (code == null) {
            return null;
        }
 
        for (DireccionEstatus estatus : DireccionEstatus.values()) {
          if(estatus.getDireccionEstatus() == code){
            return estatus;
          }
        }

        return null;
    }
}
