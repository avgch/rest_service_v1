package  com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum RutaEstatus {
 
    /**
     *
     */
    ACTIVO("A"), 

    /**
     *
     */
    INACTIVO("I");
  
  private String estatus;

  private RutaEstatus(String estatus) {
      this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
      return estatus;
  }
  
    /**
     *
     * @param name
     * name
     * @return name
     */
    public static RutaEstatus iterationFindByName(String name) {
	    for (RutaEstatus suit : RutaEstatus.values()) {
	        if (name.equals(suit.name())) {
	            return suit;
	        }
	    }
	    return null;
	}
  
}
