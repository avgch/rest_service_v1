package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum ProductosEnum {
	 
    /**
     *
     */
    DIESEL(1),

    /**
     *
     */
    PREMIUM(2),

    /**
     *
     */
    REGULAR(3),

    /**
     *
     */
    LAVADO_AUTO(4),

    /**
     *
     */
    REVISION_NEUMATICOS(5);
	  
	  private Integer valor;

	  private ProductosEnum(Integer valor) {
	      this.valor = valor;
	  }

    /**
     *
     * @return valor
     */
    public Integer getValor() {
	      return valor;
	  }

}
