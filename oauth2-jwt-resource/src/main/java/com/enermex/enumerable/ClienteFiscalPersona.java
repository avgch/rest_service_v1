package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum ClienteFiscalPersona {

    /**
     *
     */
    FISICA("F"),

    /**
     *
     */
    MORAL("M");

  private String persona;

  private ClienteFiscalPersona(String persona) {
    this.persona = persona;
  }

    /**
     *
     * @return persona
     */
    public String getPersona() {
    return persona;
  }

    /**
     *
     * @param name
     * name
     * @return persona
     */
    public static ClienteFiscalPersona iterationFindByName(String name) {
    for (ClienteFiscalPersona persona : ClienteFiscalPersona.values()) {
      if (name.equals(persona.name())) {
        return persona;
      }
    }

    return null;
  }
}
