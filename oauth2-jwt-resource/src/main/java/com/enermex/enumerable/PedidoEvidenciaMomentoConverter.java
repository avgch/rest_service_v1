package com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class PedidoEvidenciaMomentoConverter implements AttributeConverter<PedidoEvidenciaMomento, String> {
  
    @Override
    public String convertToDatabaseColumn(PedidoEvidenciaMomento momento) {
        if (momento == null) {
            return null;
        }
        return momento.getMomento();
    }
 
    @Override
    public PedidoEvidenciaMomento convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (PedidoEvidenciaMomento momento : PedidoEvidenciaMomento.values()) {
          if(momento.getMomento().equals(code)){
            return momento;
          }
        }

        return null;
    }
}
