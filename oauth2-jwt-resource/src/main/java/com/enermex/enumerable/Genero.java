package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum Genero {

    /**
     *
     */
    MASCULINO("M"),

    /**
     *
     */
    FEMENINO("F");

  private String genero;

  private Genero(String genero) {
      this.genero = genero;
  }
  
    /**
     *
     * @return genero
     */
    public String getGenero() {
      return genero;
  }
}
