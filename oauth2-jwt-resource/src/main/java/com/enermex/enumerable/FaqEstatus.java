package  com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum FaqEstatus {
 
    /**
     *
     */
    ACTIVO("A"), 

    /**
     *
     */
    INACTIVO("I");
  
  private String estatus;

  private FaqEstatus(String estatus) {
      this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
      return estatus;
  }
}
