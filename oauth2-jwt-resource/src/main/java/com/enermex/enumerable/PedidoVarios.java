package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum PedidoVarios {
	
    /**
     *
     */
    TARIFA_PROGRAMADO("Tarifa programado"), 
 
    /**
     *
     */
    TARIFA_MISMO_DIA("Tarifa mismo día"), 

    /**
     *
     */
    TARIFA_URGENTE("Tarifa urgente"),

    /**
     *
     */
    PEDIDO_PROGRAMADO_EN_TIEMPO("Pedido programado"),

    /**
     *
     */
    PEDIDO_MISMO_DIA_EN_TIEMPO_LI_INF("Pedido mismo día limite inferior"),

    /**
     *
     */
    PEDIDO_MISMO_DIA_EN_TIEMPO_LI_SUP("Pedido mismo día limite superior"),

    /**
     *
     */
    PEDIDO_URGENTE_EN_TIEMPO_LI_INF("Pedido urgente limite inferior"),

    /**
     *
     */
    PEDIDO_URGENTE_EN_TIEMPO_LI_SUP("Pedido urgente limite superior"),

    /**
     *
     */
    PEDIDO_PROGRAMADO_TIEMPO_CANCELACION("Pedido programado cancelación"),

    /**
     *
     */
    PEDIDO_MISMO_DIA_TIEMPO_CANCELACION("Pedido mismo día cancelación"),

    /**
     *
     */
    PEDIDO_URGENTE_TIEMPO_CANCELACION("Pedido urgente cancelación"),

    /**
     *
     */
    TARIFA_CANCELACION_PROGRAMADO("Tarifa de cancelación pedido programado"),

    /**
     *
     */
    TARIFA_CANCELACION_MISMO_DIA("Tarifa de cancelación pedido mismo día"),

    /**
     *
     */
    TARIFA_CANCELACION_URGENTE("Tarifa de cancelación pedido urgente") ;
	  
	  private String valor;

	  private PedidoVarios(String valor) {
	      this.valor = valor;
	  }

    /**
     *
     * @return valor
     */
    public String getValor() {
	      return valor;
	  }
	

}
