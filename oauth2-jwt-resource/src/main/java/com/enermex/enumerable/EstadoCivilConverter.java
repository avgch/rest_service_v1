package  com.enermex.enumerable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abraham
 */
@Converter(autoApply = true)
public class EstadoCivilConverter implements AttributeConverter<EstadoCivil, String> {
  
    @Override
    public String convertToDatabaseColumn(EstadoCivil estadoCivil) {
        if (estadoCivil == null) {
            return null;
        }
        return estadoCivil.getEstadoCivil();
    }
 
    @Override
    public EstadoCivil convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        for (EstadoCivil estadoCivil : EstadoCivil.values()) {
          if(estadoCivil.getEstadoCivil().equals(code)){
            return estadoCivil;
          }
        }

        return null;
    }
}
