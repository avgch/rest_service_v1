package com.enermex.enumerable;

/**
 *
 * @author abraham
 */
public enum ClienteFiscalEstatus {

    /**
     *
     */
    ACTIVO("A"),

    /**
     *
     */
    INACTIVO("I");

  private String estatus;

  private ClienteFiscalEstatus(String estatus) {
    this.estatus = estatus;
  }

    /**
     *
     * @return estatus
     */
    public String getEstatus() {
    return estatus;
  }

    /**
     *
     * @param name
     * name
     * @return estatus
     */
    public static ClienteFiscalEstatus iterationFindByName(String name) {
    for (ClienteFiscalEstatus estatus : ClienteFiscalEstatus.values()) {
      if (name.equals(estatus.name())) {
        return estatus;
      }
    }

    return null;
  }
}
